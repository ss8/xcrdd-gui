# usage: setVersion.sh [<version string>]
#
#        If unspecified, <version string> is set to ISO 8601
#        local date-time, e.g. 20110715-0200.
#
#        Otherwise, <version string> should be of the form:
#            a.b.c.d[_text]
#        , where a, b, c, and d are single-digit numbers,
#        and "_text" is optional text.

# set build number
if [ -z "$1" ]; then
    VERSION="`date '+%Y%m%d-%H%M'`"     # ISO 8601 localtime date-time
else                                    #+string, e.g. 20110715-0200
    VERSION="$1"
fi

DATE="`date '+%a %d %b %Y %H:%M:%S %Z'`"  # Fri 31 May 2019 11:12:21 PM PDT
YEAR="`date '+%Y'`"  # Fri 31 May 2019 11:12:21 PM PDT
RELEASE=`echo $VERSION | cut -d. -f1,2`

echo "ABOUT VERSION           $VERSION"
echo "ABOUT DATE           	$DATE"

#ABOUT_DATA_FILE_DIR=.
ABOUT_DATA_FILE_DIR=./src/setup
ABOUT_DATA_FILE=$ABOUT_DATA_FILE_DIR/about-data.json
ABOUT_DATA_FILE_TEMP=$ABOUT_DATA_FILE_DIR/about-data.json.temp
if [ -f $FILE ]; then
  git checkout $ABOUT_DATA_FILE
  sed 's/discovery_xcipio_build_version/'"$VERSION"'/g; 
       s/discovery_xcipio_build_date/'"$DATE"'/g; 
       s/copy_right_year/'"$YEAR"'/g' $ABOUT_DATA_FILE > $ABOUT_DATA_FILE_TEMP
  mv $ABOUT_DATA_FILE_TEMP $ABOUT_DATA_FILE 
fi
