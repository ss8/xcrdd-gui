{
  "version": "1.0.0",
  "name": "Cisco Virtual PCRF",
  "key": "CISCOVPCRF",
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "liInterface",
            "preprovisioningLead",
            "type"
          ],
          [
            "name",
            "description",
            "tag",
            "insideNat"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "liInterface": {
        "type": "text",
        "hide": "true",
        "initial": "ALL"
      },
      "preprovisioningLead": {
        "type": "text",
        "hide": "true",
        "initial": "000:00"
      },
      "name": {
        "label": "Name",
        "type": "text",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,256}$",
              "errorMessage": "Must be between 1-256 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            },
            { 
              "type": "customFunction",
              "customValidationFunction": "nonRepeatedPropertyValidator",
              "errorMessage": "AF with this name already exists."
            }
          ]
        }
      },
      "description": {
        "label": "Description",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 256
            }
          ]
        }
      },
      "type": {
        "type": "text",
        "hide": "true",
        "initial": "PCRF"
      },
      "tag": {
        "label": "AF Type",
        "type": "select",
        "initial": "CISCOVPCRF",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "insideNat": {
        "label": "Xcipio is behind NAT",
        "type": "checkbox",
        "initial": false
      }
    }
  },
  "systemInformation": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "version",
            "model",
            "serialNumber",
            "contact"
          ],
          [
            "country",
            "stateName",
            "city",
            "timeZone"
          ]
        ]
      }
    },
    "fields": {
      "version": {
        "label": "Software Version",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "model": {
        "label": "Model Number",
        "type": "text",
        "initial": "",
        "disabled": true
      },
      "serialNumber": {
        "label": "Serial Number",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a contact",
            "value": ""
          }
        ]
      },
      "country": {
        "label": "Country",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a country",
            "value": ""
          }
        ]
      },
      "stateName": {
        "label": "State/Province",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a state/province",
            "value": ""
          }
        ]
      },
      "city": {
        "label": "City",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a city",
            "value": ""
          }
        ]
      },

      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "initial": "",
        "validation": {
          "required": true
        },
        "options": [
          {
            "label": "Select a time zone",
            "value": ""
          }
        ]
      }
    }
  },
  "provisioningInterfaces": {
    "metadata": {
      "min": 1,
      "max": 2,
      "layout": {
        "rows": [
          [
            "id",
            "name"
          ],
          [
            "uri",
            "destPort",
            "secinfoid",
            "cdnetid"
          ],
          [
            "ccdestip",
            "reqState",
            "$empty",
            "$empty"
          ],
          [
            "$empty",
            "state",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "ownipaddr",
              "ownIPPort",
              "interval",
              "dscp"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "uri": {
        "label": "AF URL",
        "type": "text",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 256
            },
            {
              "type": "URL",
              "errorMessage": "Must be a valid URL. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "6443",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "cdnetid": {
        "label": "Call Data Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "ccdestip": {
        "label": "Call Content Destination IP Address",
        "type": "text",
        "initial": "::",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 64
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "ownipaddr": {
        "label": "XCP IP Address",
        "type": "text",
        "initial": "0.0.0.0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "invalidIPs": [
                "255.255.255.255"
              ],
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownIPPort": {
        "label": "XCP Port (TCP)",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "interval": {
        "label": "Message Update Interval",
        "type": "text",
        "initial": "30",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[10, 10000]],
              "errorMessage": "Must be between 10 - 10000. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [[0, 63]],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "dataInterfaces": {
    "metadata": {
      "min": 1,
      "max": 2,
      "layout": {
        "rows": [
          [
            "id",
            "name"
          ],
          [
            "destIPAddr",
            "destPort",
            "secinfoid",
            "reqState"
          ],
          [
            "$empty",
            "$empty",
            "$empty",
            "state"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "ownipaddr",
              "ownIPPort",
              "interval",
              "dscp"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "validation": {
          "required": false
        },
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className":"field-current-state"
      },
      "ownipaddr": {
        "label": "XCP IP Address",
        "type": "text",
        "initial": "0.0.0.0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "invalidIPs": [
                "255.255.255.255"
              ],
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownIPPort": {
        "label": "XCP Port (TCP)",
        "type": "text",
        "initial": "51500",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "interval": {
        "label": "Message Update Interval",
        "type": "text",
        "initial": "30",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[10, 10000]],
              "errorMessage": "Must be between 10 - 10000. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [[0, 63]],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  }
}