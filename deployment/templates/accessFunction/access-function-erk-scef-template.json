{
  "version": "1.0.1",
  "name": "Ericsson SCEF",
  "key": "ERK_SCEF",
  "processOptionProcName": "ESCEFAFIM",
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "liInterface",
            "type",
            "preprovisioningLead"
          ],
          [
            "name",
            "description",
            "tag",
            "insideNat"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "liInterface": {
        "type": "text",
        "hide": "true",
        "initial": "ALL"
      },
      "preprovisioningLead": {
        "type": "text",
        "hide": "true",
        "initial": "000:00"
      },
      "name": {
        "label": "Name",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 256
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,256}$",
              "errorMessage": "Must be between 1-256 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            },
            {
              "type": "customFunction",
              "customValidationFunction": "nonRepeatedPropertyValidator",
              "errorMessage": "AF with this name already exists."
            }
          ]
        }
      },
      "description": {
        "label": "Description",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 256
            }
          ]
        }
      },
      "type": {
        "type": "text",
        "hide": "true",
        "initial": "ERK_SCEF"
      },
      "tag": {
        "label": "AF Type",
        "type": "select",
        "initial": "ERK_SCEF",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "insideNat": {
        "label": "Xcipio is behind NAT",
        "type": "checkbox",
        "initial": false
      }
    }
  },
  "systemInformation": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "version",
            "model",
            "serialNumber",
            "contact"
          ],
          [
            "country",
            "stateName",
            "city",
            "timeZone"
          ]
        ]
      }
    },
    "fields": {
      "serialNumber": {
        "label": "Serial Number",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "model": {
        "label": "Model Number",
        "type": "text",
        "initial": "",
        "disabled": true
      },
      "version": {
        "label": "Software Version",
        "type": "textSuggest",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        },
        "options": []
      },
      "country": {
        "label": "Country",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a country",
            "value": ""
          }
        ]
      },
      "stateName": {
        "label": "State/Province",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a state/province",
            "value": ""
          }
        ]
      },
      "city": {
        "label": "City",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a city",
            "value": ""
          }
        ]
      },
      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "initial": "",
        "options": [
          {
            "label": "Select a time zone",
            "value": ""
          }
        ],
        "validation": {
          "required": true
        }
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a contact",
            "value": ""
          }
        ]
      }
    }
  },
  "provisioningInterfaces": {
    "metadata": {
      "min": 0,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "role"
          ],
          [
            "destIPAddr",
            "destPort",
            "secinfoid" ,
            "ownnetid"      
          ],
          [
            "reqState",
            "$empty",
            "$empty",
            "$empty"
          ],
          [
            "state",
            "$empty",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "keepaliveinterval",
              "transport",
              "appProtocol"
            ],
            [              
              "version",
              "$empty",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "keepaliveinterval": {
        "label": "Keepalive Interval",
        "type": "text",
        "initial": "60",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 9
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  0
                ],
                [
                  60,
                  99999999
                ]
              ],
              "errorMessage": "Must be 0 or between 60 - 999999999. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "appProtocol": {
        "label": "AF Protocol",
        "type": "textSuggest",
        "disabled": true,
        "initial": "ERK_SCEF"
      },
      "version": {
        "label": "AF Protocol Version",
        "type": "textSuggest",
        "disabled": true,
        "initial": "1.0"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      },
      "transport": {
        "label": "Transport",
        "type": "text",
        "initial": "TCP",
        "options": [
          {
            "label": "UDP",
            "value": "UDP"
          },
          {
            "label": "TCP",
            "value": "TCP"
          }
        ],
        "disabled": true
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "None",
            "value": "Tk9ORQ"
          }
        ]
      },
      "role": {
        "type": "text",
        "hide": "true",
        "initial": "CLIENT"
      }
    }
  },
  "dataInterfaces": {
    "metadata": {
      "min": 0,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "role"
          ],
          [
            "destIPAddr",
            "destPort",
            "secinfoid" ,
            "ownnetid"           
          ],
          [
            "reqState",
            "$empty",
            "$empty",
            "$empty"
          ],
          [
            "state",
            "$empty",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [              
              "dscp",
              "maxx2connections",
              "tpkt",
              "x224headerpresent"
            ],
            [
              "transport",
              "appProtocol",
              "version",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "maxx2connections": {
        "label": "Maximum Concurrent Connections",
        "type": "text",
        "initial": "10",
        "processOptionParamName": "MAXX2CONNSPERAF",
        "disabled": true
      },
      "tpkt": {
        "label": "TPKT",
        "type": "checkbox",
        "initial": true,
        "affectedFieldsOnChange": [
          "x224headerpresent"
        ]
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "processOptionParamName": "IRILISTENPORT",
        "processOptionLoadParamValueTo": "optionsFormatValue",
        "options": [],
        "optionsFormat": "networkIdIPAddressAndCustomPort",
        "optionsFormatValue": "12010",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "appProtocol": {
        "label": "AF Protocol",
        "type": "textSuggest",
        "disabled": true,
        "initial": "ERK_SCEF"
      },
      "version": {
        "label": "AF Protocol Version",
        "type": "textSuggest",
        "disabled": true,
        "initial": "1.0"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "32",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      },
      "transport": {
        "label": "Transport",
        "type": "text",
        "initial": "TCP",
        "options": [
          {
            "label": "UDP",
            "value": "UDP"
          },
          {
            "label": "TCP",
            "value": "TCP"
          }
        ],
        "disabled": true
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "None",
            "value": "Tk9ORQ"
          }
        ]
      },
      "x224headerpresent": {
        "label": "Embedded X.224 TPKT",
        "type": "checkbox",
        "initial": true
      },
      "role": {
        "type": "text",
        "hide": "true",
        "initial": "SERVER"
      }
    }
  }
}