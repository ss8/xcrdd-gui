{
  "version": "1.0.1",
  "name": "Alcatel-Lucent Web Gateway",
  "key": "LU3G_CSCF",
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "liInterface",
            "type",
            "preprovisioningLead"
          ],
          [
            "name",
            "description",
            "tag",
            "insideNat"
          ],
          [
            "contentDeliveryVia",
            "$empty",
            "$empty",
            "$empty"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "liInterface": {
        "type": "text",
        "hide": "true",
        "initial": "ALL"
      },
      "preprovisioningLead": {
        "type": "text",
        "hide": "true",
        "initial": "000:00"
      },
      "name": {
        "label": "Name",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 256
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,256}$",
              "errorMessage": "Must be between 1-256 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            },
            {
              "type": "customFunction",
              "customValidationFunction": "nonRepeatedPropertyValidator",
              "errorMessage": "AF with this name already exists."
            }
          ]
        }
      },
      "description": {
        "label": "Description",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 256
            }
          ]
        }
      },
      "type": {
        "type": "text",
        "hide": "true",
        "initial": "LU3G_CSCF"
      },
      "tag": {
        "label": "AF Type",
        "type": "select",
        "initial": "LU3G_CSCF",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "insideNat": {
        "label": "Xcipio is behind NAT",
        "type": "checkbox",
        "initial": false
      },
      "contentDeliveryVia": {
        "label": "Configure",
        "type": "select",
        "initial": "directFromAF",
        "options": [
          {
            "label": "without Traffic Manager",
            "value": "directFromAF"
          },
          {
            "label": "with Traffic Manager",
            "value": "trafficManager"
          }
        ]
      }
    }
  },
  "systemInformation": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "version",
            "model",
            "serialNumber",
            "contact"
          ],
          [
            "country",
            "stateName",
            "city",
            "timeZone"
          ]
        ]
      }
    },
    "fields": {
      "serialNumber": {
        "label": "Serial Number",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "model": {
        "label": "Model Number",
        "type": "text",
        "initial": "WebGW",
        "disabled": true
      },
      "version": {
        "label": "Software Version",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "country": {
        "label": "Country",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a country",
            "value": ""
          }
        ]
      },
      "stateName": {
        "label": "State/Province",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a state/province",
            "value": ""
          }
        ]
      },
      "city": {
        "label": "City",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a city",
            "value": ""
          }
        ]
      },
      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "initial": "",
        "validation": {
          "required": true
        },
        "options": [
          {
            "label": "Select a time zone",
            "value": ""
          }
        ]
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a contact",
            "value": ""
          }
        ]
      }
    }
  },
  "provisioningInterfaces_directFromAF": {
    "metadata": {
      "min": 1,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "$empty",
            "$empty"
          ],
          [
            "destIPAddr",
            "destPort",
            "ccnetid",
            "reqState"
          ],
          [
            "$empty",
            "$empty",
            "$empty",
            "state"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "ownIPAddr",
              "ownIPPort",
              "dscp",
              "timeout"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "ownIPAddr": {
        "label": "XCP IP Address",
        "type": "text",
        "initial": "0.0.0.0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 50
            },
            {
              "type": "IPAddress",
              "invalidIPs": [
                "255.255.255.255"
              ],
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownIPPort": {
        "label": "XCP Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 0], [10000, 65535]],
              "errorMessage": "Must be 0 or between 10000 - 65535. Please re-enter."
            }
          ]
        }
      },
      "ccnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "optionsFormat": "networkIdAndIpAddress",
        "options": [
          {
            "label": "Select a Network ID",
            "value": ""
          }
        ]
      },
      "timeout": {
        "label": "Timeout (secs)",
        "type": "text",
        "initial": "10",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 3
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  10,
                  120
                ]
              ],
              "errorMessage": "Must be between 10 - 120. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "dataInterfaces_directFromAF": {
    "metadata": {
      "min": 1,
      "max": 16,
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "$empty",
            "$empty"
          ],
          [
            "destIPAddr",
            "destPort",
            "dscp",
            "reqState"
          ],
          [
            "$empty",
            "$empty",
            "$empty",
            "state"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "contentInterfaces_trafficManager": {
    "metadata": {
      "min": 1,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "type"
          ],
          [
            "lbid",
            "destIPAddr",
            "destPort",
            "ownnetid"
          ],
          [
            "reqState",
            "$empty",
            "$empty",
            "$empty"
          ],
          [
            "state",
            "$empty",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "traceLevel",
              "$empty",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "hide": "true"
      },
      "type": {
        "name": "text",
        "hide": "true",
        "initial": "LU3GCSCF"
      },
      "lbid": {
        "label": "XTP ID",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "traceLevel": {
        "label": "Trace Level",
        "type": "select",
        "initial": "0",
        "options": [
          {
            "label": "None",
            "value": "0"
          },
          {
            "label": "1",
            "value": "1"
          },
          {
            "label": "2",
            "value": "2"
          },
          {
            "label": "3",
            "value": "3"
          },
          {
            "label": "4",
            "value": "4"
          }
        ]
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      }
    }
  }
}