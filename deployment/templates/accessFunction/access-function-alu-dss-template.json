{
  "version": "1.0.0",
  "name": "Alcatel-Lucent Distributed Soft Switch",
  "key": "ALU_DSS",
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "liInterface",
            "type",
            "preprovisioningLead"
          ],
          [
            "name",
            "description",
            "tag",
            "insideNat"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true",
        "initial": ""
      },
      "liInterface": {
        "type": "text",
        "hide": "true",
        "initial": "ALL"
      },
      "preprovisioningLead": {
        "type": "text",
        "hide": "true",
        "initial": "000:00"
      },
      "name": {
        "label": "Name",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 256
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,256}$",
              "errorMessage": "Must be between 1-256 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            },
            {
              "type": "customFunction",
              "customValidationFunction": "nonRepeatedPropertyValidator",
              "errorMessage": "AF with this name already exists."
            }
          ]
        }
      },
      "description": {
        "label": "Description",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 256
            }
          ]
        }
      },
      "type": {
        "type": "text",
        "hide": "true",
        "initial": "ALU_DSS"
      },
      "tag": {
        "label": "AF Type",
        "type": "select",
        "initial": "ALU_DSS",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "insideNat": {
        "label": "Xcipio is behind NAT",
        "type": "checkbox",
        "initial": false
      }
    }
  },
  "systemInformation": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "version",
            "model",
            "serialNumber",
            "contact"
          ],
          [
            "country",
            "stateName",
            "city",
            "timeZone"
          ]
        ]
      }
    },
    "fields": {
      "serialNumber": {
        "label": "Serial Number",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "model": {
        "label": "Model Number",
        "type": "text",
        "initial": "",
        "disabled": true
      },
      "version": {
        "label": "Software Version",
        "type": "select",
        "initial": "",
        "options": []
      },
      "country": {
        "label": "Country",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a country",
            "value": ""
          }
        ]
      },
      "stateName": {
        "label": "State/Province",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a state/province",
            "value": ""
          }
        ]
      },
      "city": {
        "label": "City",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a city",
            "value": ""
          }
        ]
      },
      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "initial": "",
        "options": [
          {
            "label": "Select a time zone",
            "value": ""
          }
        ],
        "validation": {
          "required": true
        }
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a contact",
            "value": ""
          }
        ]
      }
    }
  },
  "provisioningInterfaces": {
    "metadata": {
      "min": 1,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name"
          ],
          [
            "destIPAddr",
            "destPort",
            "ownnetid",
            "ownIPPort"
          ],
          [
            "reqState",
            "$empty",
            "$empty",
            "$empty"
          ],
          [
            "state",
            "$empty",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "timeout",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true",
        "initial": ""
      },
      "name": {
        "type": "text",
        "hide": "true",
        "initial": ""
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "ownIPPort": {
        "label": "XCP Port (TCP)",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [[0, 63]],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      },
      "timeout": {
        "label": "Timeout Interval (secs)",
        "type": "text",
        "initial": "10",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [[1, 99]],
              "errorMessage": "Must be between 1 - 99. Please re-enter."
            }
          ]
        }
      },
      "imsi": {
        "label": "IMSI allowed",
        "type": "checkbox",
        "initial": true,
        "hide": "true"
      },
      "imei": {
        "label": "IMEI allowed",
        "type": "checkbox",
        "initial": true,
        "hide": "true"
      },
      "msisdn": {
        "label": "MSISDN allowed",
        "type": "checkbox",
        "initial": true,
        "hide": "true"
      }
    }
  },
  "dataInterfaces": {
    "metadata": {
      "min": 0,
      "max": 1,
      "layout": {
        "rows": [
          [
            "id",
            "name"
          ],
          [
            "destIPAddr",
            "destPort",
            "ownnetid",
            "ownIPPort"
          ],
          [
            "mscid",
            "reqState",
            "$empty",
            "$empty"
          ],
          [
            "$empty",
            "state",
            "$empty",
            "$empty"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true",
        "initial": ""
      },
      "name": {
        "type": "text",
        "hide": "true",
        "initial": ""
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [[0, 65535]],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "ownIPPort": {
        "label": "XCP Port",
        "type": "text",
        "initial": "11550",
        "disabled": true
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className":"field-current-state"
      },
      "mscid": {
        "label": "MSC ID",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 15
            }
          ]
        }
      }
    }
  }
}