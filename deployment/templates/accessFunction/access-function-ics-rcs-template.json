{
  "version": "1.0.1",
  "name": "ICS RCS Server",
  "key": "ICS_RCS",
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "liInterface",
            "type",
            "preprovisioningLead"
          ],
          [
            "name",
            "description",
            "tag",
            "contentDeliveryVia"
          ]
        ]
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "liInterface": {
        "type": "text",
        "hide": "true",
        "initial": "ALL"
      },
      "preprovisioningLead": {
        "type": "text",
        "hide": "true",
        "initial": "000:00"
      },
      "name": {
        "label": "Name",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 256
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,256}$",
              "errorMessage": "Must be between 1-256 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            },
            {
              "type": "customFunction",
              "customValidationFunction": "nonRepeatedPropertyValidator",
              "errorMessage": "AF with this name already exists."
            }
          ]
        }
      },
      "description": {
        "label": "Description",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 256
            }
          ]
        }
      },
      "type": {
        "type": "text",
        "hide": "true",
        "initial": "ICS_RCS"
      },
      "tag": {
        "label": "AF Type",
        "type": "select",
        "initial": "ICS_RCS",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "contentDeliveryVia": {
        "label": "Configure",
        "type": "select",
        "initial": "directFromAF",
        "options": [
          {
            "label": "without Traffic Manager",
            "value": "directFromAF"
          },
          {
            "label": "with Traffic Manager",
            "value": "trafficManager"
          }
        ]
      }
    }
  },
  "systemInformation": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "version",
            "model",
            "serialNumber",
            "contact"
          ],
          [
            "country",
            "stateName",
            "city",
            "timeZone"
          ]
        ]
      }
    },
    "fields": {
      "serialNumber": {
        "label": "Serial Number",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        }
      },
      "model": {
        "label": "Model Number",
        "type": "text",
        "initial": "",
        "disabled": true
      },
      "version": {
        "label": "Software Version",
        "type": "textSuggest",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 16
            }
          ]
        },
        "options": []
      },
      "country": {
        "label": "Country",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a country",
            "value": ""
          }
        ]
      },
      "stateName": {
        "label": "State/Province",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a state/province",
            "value": ""
          }
        ]
      },
      "city": {
        "label": "City",
        "type": "select",
        "initial": "",
        "disabled": true,
        "options": [
          {
            "label": "Select a city",
            "value": ""
          }
        ]
      },
      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "initial": "",
        "options": [
          {
            "label": "Select a time zone",
            "value": ""
          }
        ],
        "validation": {
          "required": true
        }
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "Select a contact",
            "value": ""
          }
        ]
      }
    }
  },
  "provisioningInterfaces_directFromAF": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "$empty",
            "$empty"
          ],
          [
            "destIPAddr",
            "destPort",
            "uri",
            "ownnetid"
          ],
          [
            "secinfoid",
            "reqState",
            "$empty",
            "$empty"
          ],
          [
            "$empty",
            "state",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "$empty",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "initial": "",
        "hide": "true"
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "uri": {
        "label": "NE URI Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 128
            }
          ]
        }
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "None",
            "value": "Tk9ORQ"
          }
        ]
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "dataInterfaces_directFromAF": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "$empty",
            "$empty"
          ],
          [
            "destIPAddr",
            "ownnetid",
            "userName",
            "password"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "$empty",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "initial": "",
        "hide": "true"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "userName": {
        "label": "User Name",
        "type": "text",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 20
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,20}$",
              "errorMessage": "Must be between 1-20 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            }
          ]
        }
      },
      "password": {
        "label": "Password",
        "type": "text",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 15
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{0,15}$",
              "errorMessage": "Must be between 0-15 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "contentInterfaces_directFromAF": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "$empty",
            "$empty"
          ],
          [
            "destIPAddr",
            "ownnetid",
            "userName",
            "password"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "$empty",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "initial": "",
        "hide": "true"
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "userName": {
        "label": "User Name",
        "type": "text",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 20
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{1,20}$",
              "errorMessage": "Must be between 1-20 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            }
          ]
        }
      },
      "password": {
        "label": "Password",
        "type": "text",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 15
            },
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?]{0,15}$",
              "errorMessage": "Must be between 0-15 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?. Please re-enter."
            }
          ]
        }
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "0",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  63
                ]
              ],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      }
    }
  },
  "contentInterfaces_trafficManager": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "id",
            "name",
            "type"
          ],
          [
            "lbid",
            "ownnetid",
            "destIPAddr",
            "destPort"
          ],
          [
            "secinfoid",
            "reqState",
            "$empty",
            "$empty"
          ],
          [
            "$empty",
            "state",
            "$empty",
            "$empty"
          ]
        ],
        "collapsible": {
          "label": "Advanced Settings",
          "rows": [
            [
              "dscp",
              "traceLevel",
              "$empty",
              "$empty"
            ]
          ]
        }
      }
    },
    "fields": {
      "id": {
        "type": "text",
        "hide": "true"
      },
      "name": {
        "type": "text",
        "initial": "",
        "hide": "true"
      },
      "type": {
        "type": "text",
        "initial": "ICS_RCS",
        "hide": "true"
      },
      "lbid": {
        "label": "XTP ID",
        "type": "select",
        "initial": "",
        "options": [],
        "validation": {
          "required": true
        }
      },
      "ownnetid": {
        "label": "Network ID",
        "type": "select",
        "initial": "",
        "options": [],
        "optionsFormat": "networkIdAndIpAddress",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "customFunction",
              "customValidationFunction": "networkIdNotFoundValidator",
              "errorMessage": "Please select another network ID."
            }
          ]
        }
      },
      "destIPAddr": {
        "label": "AF IP Address",
        "type": "text",
        "initial": "",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "length",
              "length": 42
            },
            {
              "type": "IPAddress",
              "errorMessage": "Must be a valid IP Address. Please re-enter."
            }
          ]
        }
      },
      "destPort": {
        "label": "AF Port",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 5
            },
            {
              "type": "Interval",
              "intervals": [
                [
                  0,
                  65535
                ]
              ],
              "errorMessage": "Must be between 0 - 65535. Please re-enter."
            }
          ]
        }
      },
      "secinfoid": {
        "label": "TLS Profile (Security Info ID)",
        "type": "select",
        "initial": "",
        "options": [
          {
            "label": "None",
            "value": ""
          }
        ]
      },
      "traceLevel": {
        "label": "Trace Level",
        "type": "select",
        "initial": "0",
        "options": [
          {
            "label": "None",
            "value": "0"
          },
          {
            "label": "1",
            "value": "1"
          },
          {
            "label": "2",
            "value": "2"
          },
          {
            "label": "3",
            "value": "3"
          },
          {
            "label": "4",
            "value": "4"
          }
        ]
      },
      "dscp": {
        "label": "DSCP (0-63)",
        "type": "text",
        "initial": "32",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 2
            },
            {
              "type": "Interval",
              "intervals": [[0, 63]],
              "errorMessage": "Must be between 0 - 63. Please re-enter."
            }
          ]
        }
      },
      "reqState": {
        "label": "Configured State",
        "type": "select",
        "initial": "ACTIVE",
        "options": [
          {
            "label": "Active",
            "value": "ACTIVE"
          },
          {
            "label": "Inactive",
            "value": "INACTIVE"
          }
        ]
      },
      "state": {
        "label": "Current State",
        "type": "custom",
        "initial": "",
        "hide": "true",
        "customComponent": "accessFunctionCurrentState",
        "className": "field-current-state"
      }
    }
  }
}