// declarations for 3rd party modules with no defs

declare module 'muuri';
declare module 'react-resizable';
declare module 'react-dimensions';
declare module 'lodash.isequal';
declare module 'react-confirm';
