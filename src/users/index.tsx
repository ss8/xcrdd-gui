import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { getAllRoles } from '../roles/roles-actions';
import {
  getAllUsers,
  createUserAndRefresh,
  updateUserAndRefresh,
  deleteUserAndRefresh,
  clearErrors,
  getUserSettings,
  putUserSettings,
} from './users-actions';
import ModuleRoutes from './users-routes';
import './users.scss';

const Users = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  users: state.users,
  authentication: state.authentication,
  roles: state.roles,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllRoles,
  getAllUsers,
  createUserAndRefresh,
  updateUserAndRefresh,
  deleteUserAndRefresh,
  clearErrors,
  getUserSettings,
  putUserSettings,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));
