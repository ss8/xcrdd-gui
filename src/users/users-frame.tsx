import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import UsersGrid from './grid';
import './users.scss';
import { AppToaster } from '../shared/toaster';
import { toUserMessage } from './users-util';
import convertErrorCodeToMessage from '../shared/errorCodeConvert/convertErrorCodeToMessage';

class UserFrame extends React.Component<any, any> {
  handleChange = (e: any) => {
    this.setState({ selectedTab: e });
  }

  handleViewChange = (e: any) => {
    this.setState({ selectedView: e.currentTarget.id });
  }

  componentDidUpdate() {
    const { error } = this.props.users;
    let message: any;
    if (error.errorCode === 'PASSWORD_POLICY_FAILURE') {
      message = toUserMessage(error.errorMessage, this.props.policies.passwordPolicy);
    } else {
      message = convertErrorCodeToMessage(error);
    }

    if (error.errorCode != null || error.errorMessage != null) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${message}`,
      });
      this.props.clearErrors();
    }
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/authentication/login');
    }
  }

  render() {
    return (
      <Fragment>
        <div className="col-md-12" style={{ height: 'inherit' }}>
          <UsersGrid {...this.props} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state: any) => ({
  policies: state.policies,
});

export default connect(mapStateToProps, null)(UserFrame);
