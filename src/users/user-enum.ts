// eslint-disable-next-line import/prefer-default-export
export enum USER_PRIVILEGE {
    View = 'user_view',
    Edit = 'user_write',
    Delete = 'user_delete',
    Admin = 'user_admin', // Predefined User edit/create/delete permission)
}

export enum USER_ACCOUNT_STATE {
    Active = 'active',
    Deactivated = 'deactivated',
    Pending = 'pending',
    PasswordRequired = 'new-password',
    Locked = 'locked',
}
