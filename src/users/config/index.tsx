import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Button, Label,
} from '@blueprintjs/core';
import isEqual from 'lodash.isequal';
import cloneDeep from 'lodash.clonedeep';
import { AppToaster } from '../../shared/toaster';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';
import DynamicForm from '../../shared/form';
import userConfig from './users-configuration-form.json';
import { USER_PRIVILEGE, USER_ACCOUNT_STATE } from '../user-enum';
import { isSystemAdmin } from '../users-util';
import { getAllRoles } from '../../roles/roles-actions';
import { updateUserStateAndRefresh } from '../users-actions';
import { getPasswordPolicy } from '../../policies/policy-actions';
import { Role } from '../../data/role/role.types';

class UsersConfig extends React.Component<any, any> {
  private ref: any;

  private fields:any;

  private currentUser:any; // current login user

  private previouslyAssignedRoles:string[];

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.props.getPasswordPolicy();
    // makes a copy so the json instance is not modified because it is shared with
    // edit.
    this.fields = JSON.parse(JSON.stringify(userConfig.forms.caseNotes.fields));
    this.currentUser = props.location.state.currentUser;
    this.previouslyAssignedRoles = cloneDeep(props.location.state.data.assignedRoles);
    this.state = {
      data: props.location.state.data, // user data
      currentStateMode: props.location.state.action,
      fieldDirty: false,
    };
  }

  componentDidMount() {
    this.props.getAllRoles();
    this.previouslyAssignedRoles = cloneDeep(this.props.location.state.data.assignedRoles);
    this.setState({
      data: this.props.location.state.data,
      currentStateMode: this.props.location.state.action,
    });
    this.handleStateModeChanged(this.props.location.state.action);
  }

  /**
   * Non-sysadm users can only assign others roles that they have
   */
  getAssignableRoles = (roles: [], user:any) => {
    let assignableRoles:string[] = [];
    if (isSystemAdmin(this.props.authentication.userDBId) === false) {
      const currentUserRoles: string[] = this.currentUser.reporting.assignedRoles.map((role:Role) => (role.name));
      assignableRoles = [...new Set([...currentUserRoles, ...user.assignedRoles])];
    } else {
      assignableRoles = roles.map((role:Role) => (role.name));
    }

    return assignableRoles.map((roleName: string) => {
      return { label: roleName, value: roleName };
    });
  }

  getFields = (roles: [], user: any) => {
    Object.keys(this.fields).forEach((key) => {
      if (key === 'assignedRoles') {
        this.fields[key].options = this.getAssignableRoles(roles, user);
        // Disallow anyone except sysadm to change sysadm roles
        if (isSystemAdmin(user.id) && isSystemAdmin(this.props.authentication.userDBId) === false) {
          this.fields[key].readOnly = true;
        }
      } else if (key === 'password') {
        this.fields[key].initial = user[key];
      // Disallow users from changing their own username and disallow changing sysadm username
      } else if (key === 'userID' && (user.userID === this.props.authentication.userId || isSystemAdmin(user.id))) {
        this.fields[key].disabled = true;
      }
    });
    return this.fields;
  }

  handleClose = () => {
    AppToaster.clear();
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  isRoleChanged = (newAssignedRoles:any) : boolean => {
    return isEqual(newAssignedRoles.sort(), this.previouslyAssignedRoles.sort());
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (this.ref.current.isValid(true) === false) return;

    const userPayload = this.ref.current.getValues();
    userPayload.displayName = `${userPayload.lastName} ${userPayload.firstName}`;
    if (userPayload.displayName.trim() === '') {
      userPayload.displayName = userPayload.userID;
    }
    userPayload.role = ''; // this field not used by server
    userPayload.state = this.state.data.state;

    const { id } = this.props.location.state.data;
    userPayload.id = id;

    let rolePayload = this.ref.current.getValue('assignedRoles');
    if (this.isRoleChanged(rolePayload)) {
      rolePayload = null;
    }

    let { password } = userPayload;
    if (userPayload.password === this.props.location.state.data.password) {
      password = null;
    }

    this.props.updateUserAndRefresh(id, userPayload, rolePayload, password, this.props.history);
  }

  handleFieldUpdate = () => {
    this.setState({ fieldDirty: true }); // todo: delegate this to config-form-popup
  }

  setReadOnly = (readOnly:boolean) => {
    Object.keys(this.fields).forEach((key: any) => {
      (this.fields as any)[key].readOnly = readOnly;
    });
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    this.setState({ currentStateMode: stateMode });
    if (stateMode === CONFIG_FORM_MODE.Edit) {
      this.setReadOnly(false);
    } else {
      this.setReadOnly(true);
    }
  }

  updateUserAcctState = (newState:string) => {
    const newUser = { ...this.state.data, state: newState };
    this.setState({ data: newUser });
  }

  handleClick = (e: any) => {
    const newState = e.currentTarget.id;
    const { id, userID } = this.props.location.state.data;
    this.props.updateUserStateAndRefresh(id, userID, newState)
      .then(() => this.updateUserAcctState(newState));
  }

  getTitle = (currentStateMode : CONFIG_FORM_MODE) => {
    return (currentStateMode === CONFIG_FORM_MODE.Edit) ? 'Edit User' : 'View User';
  }

  toUserAcctStateLabel = (userAcctState: string) => {
    if (userAcctState === null) { return 'unknown'; }
    if (userAcctState === 'new-password') {
      return 'Password Change Required';
    }
    switch (userAcctState) {
      case USER_ACCOUNT_STATE.Active:
        return 'Active';
      case USER_ACCOUNT_STATE.Pending:
        return 'Pending';
      case USER_ACCOUNT_STATE.PasswordRequired:
        return 'Password Change Required';
      case USER_ACCOUNT_STATE.Deactivated:
        return 'Deactivated';
      case USER_ACCOUNT_STATE.Locked:
        return 'Locked';
      default:
        return 'unknown';
    }
  }

  showRequirePasswordButton = (formState:string, userState:string) => {
    return (formState === CONFIG_FORM_MODE.Edit &&
      userState !== USER_ACCOUNT_STATE.Pending &&
      userState !== USER_ACCOUNT_STATE.PasswordRequired &&
      userState !== USER_ACCOUNT_STATE.Deactivated);
  }

  showDeactivateButton = (formState:string, userState:string, user:any) => {
    return (formState === CONFIG_FORM_MODE.Edit &&
        userState !== USER_ACCOUNT_STATE.Deactivated &&
        isSystemAdmin(user.id) === false &&
        user.userID !== this.props.authentication.userId);
  }

  showActiveButton = (formState:string, userState:string, user:any) => {
    if (formState === CONFIG_FORM_MODE.Edit &&
        userState !== USER_ACCOUNT_STATE.Active &&
        userState !== USER_ACCOUNT_STATE.Pending) {
      if (isSystemAdmin(user.id) === true) {
        return true;
      }
      if (userState !== USER_ACCOUNT_STATE.PasswordRequired) {
        return true;
      }
    }
    return false;
  }

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToEdit = privileges !== undefined
      && (privileges.includes(USER_PRIVILEGE.Edit) || privileges.includes(USER_PRIVILEGE.Admin));

    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: this.state.currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: true,
      width: '950px',
      height: 'auto',
      overflow: 'auto',
    };

    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          <div style={{ float: 'left', width: '450px' }}>
            <DynamicForm
              name={userConfig.key}
              ref={this.ref}
              fields={this.getFields(this.props.roles.roles, this.state.data)}
              layout={userConfig.forms.caseNotes.metadata.layout.rows}
              colWidth={6}
              onSubmit={this.handleSubmit}
              defaults={this.state.data}
              fieldUpdateHandler={this.handleFieldUpdate}
            />
          </div>
          <div style={{ float: 'right', width: '450px' }}>
            <div className="form-flex-col-css field-group">
              <Label>State:</Label>
              <div className="form-flex-row-css">
                <Label
                  style={{ width: '200px' }}
                >
                  {this.toUserAcctStateLabel(this.state.data.state)}
                </Label>
                { this.showActiveButton(this.state.currentStateMode, this.state.data.state, this.state.data) ? (
                  <Button
                    id={USER_ACCOUNT_STATE.Active}
                    style={{ marginLeft: '5px', marginBottom: '7px' }}
                    onClick={this.handleClick}
                  >
                    Activate
                  </Button>
                )
                  : null }
                { this.showDeactivateButton(this.state.currentStateMode, this.state.data.state, this.state.data) ? (
                  <Button
                    id={USER_ACCOUNT_STATE.Deactivated}
                    style={{ marginLeft: '5px', marginBottom: '7px' }}
                    onClick={this.handleClick}
                  >
                    Deactivate
                  </Button>
                )
                  : null }
                { this.showRequirePasswordButton(this.state.currentStateMode, this.state.data.state) ? (
                  <Button
                    id={USER_ACCOUNT_STATE.PasswordRequired}
                    style={{ marginLeft: '5px', marginBottom: '7px' }}
                    onClick={this.handleClick}
                  >
                    Force Password Change
                  </Button>
                )
                  : null }
              </div>
            </div>
            <div className="form-flex-col-css field-group">
              <Label>Last Login:</Label>
              <Label>{ (this.state.data.lastlogin) ? this.state.data.lastlogin : 'User never logged in'}</Label>
            </div>
            <div className="form-flex-col-css field-group">
              <Label>Last Login Address:</Label>
              <Label>{ (this.state.data.lastLoginAddress) ? this.state.data.lastLoginAddress : 'No last login address'}</Label>
            </div>
            <div className="form-flex-col-css field-group">
              <Label>Last Password Change:</Label>
              <Label>{(this.state.data.lastPasswordChange) ? this.state.data.lastPasswordChange : 'Password was never changed'}</Label>
            </div>
          </div>
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  roles: state.roles,
  users: state.users,
});
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllRoles,
  updateUserStateAndRefresh,
  getPasswordPolicy,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UsersConfig));
