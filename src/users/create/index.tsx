import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Role } from 'data/role/role.types';
import cloneDeep from 'lodash.clonedeep';
import { FormTemplateFieldMap, FormTemplateOption } from 'data/template/template.types';
import { AppToaster } from '../../shared/toaster';
import DynamicForm from '../../shared/form';
import userCreate from '../config/users-configuration-form.json';
import { getPasswordPolicy } from '../../policies/policy-actions';
import PopUp from '../../shared/popup/dialog';
import { createUserAndRefresh } from '../users-actions';
import { isSystemAdmin } from '../users-util';

class UsersCreate extends React.Component<any, any> {
  private ref: any;

  private currentUser:any; // current login user

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.currentUser = props.location.currentUser;
    this.props.getAllRoles();
    this.props.getPasswordPolicy();

    this.state = {
      isOpen: true,
    };
  }

  /**
   * Users can only assign others roles that they have
   */
  getAssignableRoles = (roles: Role[]): FormTemplateOption[] => {
    const { authentication: { userDBId } } = this.props;
    let assignableRoles:string[] = [];
    if (isSystemAdmin(userDBId) === false) {
      assignableRoles = this.currentUser.reporting.assignedRoles.map((role:Role) => (role.name));
    } else {
      assignableRoles = roles.map((role:Role) => (role.name));
    }

    return assignableRoles.map((roleName: string) => {
      return { label: roleName, value: roleName };
    });
  }

  getFields = (roles: Role[]) => {
    const fields = cloneDeep(userCreate.forms.caseNotes.fields) as FormTemplateFieldMap;
    Object.keys(fields).forEach((key:string) => {
      if (key === 'assignedRoles') {
        fields[key].options = this.getAssignableRoles(roles);

      } else if (key === 'password') {
        fields[key].initial = '';
      }
    });
    return fields;
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (this.ref.current.isValid(true) === false) return;

    const userPayload = this.ref.current.getValues();
    userPayload.displayName = `${userPayload.lastName} ${userPayload.firstName}`;
    if (userPayload.displayName.trim() === '') {
      userPayload.displayName = userPayload.userID;
    }
    userPayload.role = ''; // this field not used by CREST server
    userPayload.userID = this.ref.current.getValue('userID');

    const rolePayload = this.ref.current.getValue('assignedRoles');
    this.props.createUserAndRefresh(userPayload.userID, userPayload, rolePayload, this.props.history);
  }

  handleClose = () => {
    AppToaster.clear();
    this.setState({ isOpen: false });
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  render() {
    return (
      <PopUp title="Create New User" isOpen={this.state.isOpen} onSubmit={this.handleSubmit} handleClose={this.handleClose} width="500px" {...this.props}>
        <DynamicForm
          name={userCreate.key}
          ref={this.ref}
          fields={this.getFields(this.props.roles.roles)}
          layout={userCreate.forms.caseNotes.metadata.layout.rows}
          colWidth={6}
          onSubmit={this.handleSubmit}
          {...this.props}
        />
      </PopUp>
    );
  }
}

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  roles: state.roles,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getPasswordPolicy,
  createUserAndRefresh,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UsersCreate);
