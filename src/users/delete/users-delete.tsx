import React from 'react';
import { connect } from 'react-redux';
import ModalDialog from '../../shared/modal/dialog';

interface DeleteUserArgs {
  wId: any;
  user: string;
}

class UserDeleteUser extends React.Component<any, any> {
  handleDelete = () => {
    const {
      deleteUserAndRefresh,
      externalDashboardIP,
      logiSupervisorUserName,
      logiSupervisorPswd,
      location,
    } = this.props;

    const { selection } = location.state;

    selection
      .map((x: any) => ({ wId: x.id, user: x.userID }))
      .forEach(({ wId, user }: DeleteUserArgs) => {
        deleteUserAndRefresh({
          id: String(wId),
          user,
          externalDashboardIP,
          logiSupervisorUserName,
          logiSupervisorPswd,
        });
      });
  }

  render() {
    const { selection } = this.props.location.state;
    const displayMessage = `Are you sure you want to delete user "${selection[0].userID}" ?`;
    return (
      <ModalDialog
        onSubmit={this.handleDelete}
        width="500px"
        {...this.props}
        actionText="Delete"
        displayMessage={displayMessage}
      />
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      externalDashboardIP,
      logiSupervisorUserName,
      logiSupervisorPswd,
    },
  },
}: any) => ({
  externalDashboardIP,
  logiSupervisorUserName,
  logiSupervisorPswd,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(
  UserDeleteUser,
);
