import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import UsersFrame from './users-frame';
import UsersCreate from './create';
import UsersDelete from './delete/users-delete';
import UsersConfig from './config';

const moduleRoot = '/users';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <UsersFrame {...props} />}
    />
    <Route
      path={`${moduleRoot}/delete`}
      render={() => <UsersDelete {...props} />}
    />
    <Route
      path={`${moduleRoot}/create`}
      render={() => <UsersCreate {...props} />}
    />
    <Route
      path={`${moduleRoot}/edit`}
      render={() => <UsersConfig {...props} />}
    />
    <Route
      path={`${moduleRoot}/view`}
      render={() => <UsersConfig {...props} />}
    />

  </Fragment>
);

export default ModuleRoutes;
