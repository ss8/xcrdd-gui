import {
  GET_ALL_USERS_FULFILLED,
  GET_USER_TEAMS_FULFILLED,
  GET_TIMEZONES_FULFILLED,
  USERS_CLEAR_ERRORS,
  GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
  UPDATE_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
} from './users-actions';

const defaultState = {
  users: [],
  teams: [],
  timezones: [],
  error: {
    errorCode: null,
    errorMessage: null,
  },
  action: '',
  hideIncludeCaseConfirmation: false,
};

const moduleReducer = (state = defaultState, action: any) => {
  if (/USER.*_REJECTED/.test(action.type)) {
    let errorFieldName = '';
    let errorCode = action.payload.response.status;
    let errorMessage = action.payload.response.statusText;
    if (action.payload.response.data && action.payload.response.data !== ''
        && action.payload.response.data.error) {
      errorFieldName = JSON.parse(action.payload.response.config.data).userID;
      errorCode = action.payload.response.data.error.code;
      errorMessage = action.payload.response.data.error.message;
    }
    return { ...state, error: { errorMessage, errorCode, errorFieldName } };
  }

  switch (action.type) {
    case GET_ALL_USERS_FULFILLED:
      return { ...state, users: action.payload.data };
    case USERS_CLEAR_ERRORS:
      return { ...state, error: { errorMessage: null, errorCode: null } };
    case GET_USER_TEAMS_FULFILLED:
      return { ...state, teams: action.payload.data.data };
    case GET_TIMEZONES_FULFILLED: {
      let timezones = [];
      const { data } = action.payload.data;
      if (data && data[0] && data[0].zones) {
        timezones = data[0].zones;
      }
      return { ...state, timezones };
    }
    case GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION: {
      return { ...state, hideIncludeCaseConfirmation: action.payload };
    }
    case UPDATE_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION: {
      return { ...state, hideIncludeCaseConfirmation: action.payload };
    }
    default:
      return state;
  }
};
export default moduleReducer;
