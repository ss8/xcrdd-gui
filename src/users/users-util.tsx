import { AUTH } from 'utils';
import { toPasswordPolicyDescription } from '../policies/policies-util';

/**
 * This is a convenient function to get a list of assigned roles of a user.
 * @param user is the user object returned by REST API
 * @return assigned role array
 */
// eslint-disable-next-line import/prefer-default-export
export const getAssignedRoles = (user:any) => {
  const assignedRoles: string[] = [];
  if (user.reporting && user.reporting.assignedRoles) {
    user.reporting.assignedRoles.forEach((role:any) => {
      assignedRoles.push(role.name);
    });
  }
  return assignedRoles;
};

// eslint-disable-next-line import/prefer-default-export
export const toUserMessage = (errorMessage: string, data:any) => {
  if (!errorMessage) return '';

  if (errorMessage.indexOf('password policy violation') > 0) {
    return `Invalid password format. ${toPasswordPolicyDescription(data)}`;
  }
  return errorMessage;
};

export const isSystemAdmin = (userDBId: string): boolean => {
  return userDBId === '1';
};

export const setLocalStorageHideIncludeCaseConfirmationSetting = (value: boolean): boolean => {

  const userKey = getLocalStorageUserSettingsKey();
  const userSettings = getLocalStorageUserSettings();
  userSettings.hideIncludeCaseConfirmation = value;
  localStorage.setItem(userKey, JSON.stringify(userSettings));

  return value;
};

export const getLocalStorageHideIncludeCaseConfirmationSetting = (): boolean => {
  return getLocalStorageUserSettings().hideIncludeCaseConfirmation;
};

const getLocalStorageUserSettings = (): { [key:string]: any } => {
  const userKey = getLocalStorageUserSettingsKey();
  const userSettings = JSON.parse(localStorage.getItem(userKey) || '{}');
  return userSettings;
};

const getLocalStorageUserSettingsKey = (): string => {
  const userId = localStorage.getItem(AUTH.USERID);
  return `${userId}/settings`;
};
