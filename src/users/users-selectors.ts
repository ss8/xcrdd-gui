import { RootState } from 'data/root.reducers';
import { Team } from 'data/team/team.types';
import { ValueLabelPair } from 'data/types';

export const getTimezones = (state: RootState): ValueLabelPair[] => state.users.timezones;
export const shouldHideIncludeCaseConfirmation = (state: RootState): boolean => state.users.hideIncludeCaseConfirmation;
export const getTeams = (state: RootState): Team[] => state.users.teams;
