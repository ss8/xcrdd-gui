import configureStore, { MockStoreCreator } from 'redux-mock-store';
import rootReducer, { RootState } from 'data/root.reducers';
import { Team } from 'data/team/team.types';
import {
  getTeams,
  getTimezones,
  shouldHideIncludeCaseConfirmation,
} from './users-selectors';
import {
  GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
  GET_TIMEZONES_FULFILLED,
  GET_USER_TEAMS_FULFILLED,
} from './users-actions';

describe('users-selectors', () => {
  let timezones: string[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    timezones = [
      'America/Los_Angeles',
      'America/New_York',
    ];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of timezones', () => {
    const action = {
      type: GET_TIMEZONES_FULFILLED,
      payload: { data: { data: [{ zones: timezones }] } },
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getTimezones(store.getState())).toEqual(timezones);
  });

  it('should return if should hide include case confirmation', () => {
    const action = {
      type: GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
      payload: true,
    };
    store = mockStore(rootReducer(undefined, action));
    expect(shouldHideIncludeCaseConfirmation(store.getState())).toEqual(true);
  });

  it('should return the list of teams', () => {
    const teams: Team[] = [{
      id: '1',
      name: 'team1',
    }];

    const action = {
      type: GET_USER_TEAMS_FULFILLED,
      payload: { data: { data: teams } },
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getTeams(store.getState())).toEqual(teams);
  });
});
