import axios from 'axios';
import { History } from 'history';
import Http from '../shared/http';
import { showSuccessMessage, showError, TOAST_TIMEOUT } from '../shared/toaster';
import { extractErrorMessage } from '../utils';
import { getLocalStorageHideIncludeCaseConfirmationSetting, setLocalStorageHideIncludeCaseConfirmationSetting } from './users-util';
import { decryptText } from '../utils/auth';

const http = Http.getHttp();

interface DeleteLogiUserArgs {
  user: string;
  externalDashboardIP: string;
  logiSupervisorUserName: string;
  logiSupervisorPswd: string;
}

interface DeleteUserArgs extends DeleteLogiUserArgs {
  id: any;
}

// action export constants
export const GET_ALL_USERS = 'GET_ALL_USERS';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';
export const CREATE_USER = 'CREATE_USER';
export const UPDATE_USER_STATE = 'UPDATE_USER_STATE';
export const ASSIGN_ROLES = 'ASSIGN_ROLES';
export const UPDATE_USER_PASSWORD = 'UPDATE_USER_PASSWORD';

export const GET_ALL_USERS_FULFILLED = 'GET_ALL_USERS_FULFILLED';
export const UPDATE_USER_FULFILLED = 'UPDATE_USER_FULFILLED';
export const DELETE_USER_FULFILLED = 'DELETE_USER_FULFILLED';
export const CREATE_USER_FULFILLED = 'CREATE_USER_FULFILLED';
export const USERS_CLEAR_ERRORS = 'USERS_CLEAR_ERRORS';
export const GET_USER_TEAMS = 'GET_USER_TEAMS';
export const GET_USER_TEAMS_FULFILLED = 'GET_USER_TEAMS_FULFILLED';

export const GET_USER_SETTINGS = 'GET_USER_SETTINGS';
export const PUT_USER_SETTINGS = 'PUT_USER_SETTINGS';

export const GET_TIMEZONES = 'GET_TIMEZONES';
export const GET_TIMEZONES_FULFILLED = 'GET_TIMEZONES_FULFILLED';

export const UPDATE_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION = 'UPDATE_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION';
export const GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION = 'GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION';

function goToPreviousPage(history: History): () => void {
  return () => {
    history.goBack();
  };
}

// action creators
export const getAllUsers = (body = '$view=reporting') => ({ type: GET_ALL_USERS, payload: http.get(`/users/?${body}`) });
const createUser = (body: any) => ({ type: CREATE_USER, payload: http.post('/users/', body) });
const assignRoles = (userId: any, roles: any) => ({ type: ASSIGN_ROLES, payload: http.post(`/users/${userId}/roles`, roles) });
export const createUserAndRefresh = (userId: any, userPayload: any, rolePayload: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(createUser(userPayload));
      await dispatch(assignRoles(userId, rolePayload));
      dispatch(goToPreviousPage(history));
      showSuccessMessage('User successfully created.', TOAST_TIMEOUT);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create user: ${errorMessage}.`);

    } finally {
      dispatch(getAllUsers());
    }
  };
};

const updatePassword = (id: number, userId: string, password: string) => {
  if (password == null) {
    return null;
  }
  const body = {
    user: userId,
    action: 'password',
    password,
  };
  return ({ type: UPDATE_USER_PASSWORD, payload: http.put(`/users/${id}/password`, body) });
};

const updateUser = (id: any, body: any) => ({ type: UPDATE_USER, payload: http.put(`/users/${id}`, body) });
export const updateUserAndRefresh = (id: any, userPayload: any, rolePayload: any,
  password: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(updateUser(id, userPayload));
      if (rolePayload !== null) await dispatch(assignRoles(id, rolePayload));
      if (password !== null) await dispatch(updatePassword(id, userPayload.userID, password));
      dispatch(goToPreviousPage(history));
      showSuccessMessage('User successfully updated.', TOAST_TIMEOUT);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update user: ${errorMessage}.`);

    } finally {
      dispatch(getAllUsers());
    }
  };
};

const deleteUser = (id: number) => ({ type: DELETE_USER, payload: http.delete(`/users/${id}`) });
const deleteLogiUser = ({
  user,
  externalDashboardIP,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: DeleteLogiUserArgs) => {
  return async () => {
    const getLogiUsersUrl = `${externalDashboardIP}/zoomdata/api/users?limit=-1&offset=0`;

    let logiUserId = '';

    const { data: logiUsers } = await axios.get(getLogiUsersUrl, {
      headers: {
        Accept: 'application/vnd.zoomdata.v2+json',
      },
      auth: {
        username: decryptText(logiSupervisorUserName),
        password: decryptText(logiSupervisorPswd),
      },
    });

    for (let i = 0; i < logiUsers.length; i += 1) {
      if (logiUsers[i].name === user) {
        logiUserId = logiUsers[i].id;
        break;
      }
    }

    const deleteLogiUserUrl = `${externalDashboardIP}/zoomdata/api/users/${logiUserId}`;

    axios.delete(deleteLogiUserUrl, {
      headers: {
        Accept: 'application/vnd.zoomdata.v2+json',
      },
      auth: {
        username: decryptText(logiSupervisorUserName),
        password: decryptText(logiSupervisorPswd),
      },
    });
  };
};

export const deleteUserAndRefresh = ({
  id,
  user,
  externalDashboardIP,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: DeleteUserArgs) => {
  return async (dispatch: any) => {
    try {
      await dispatch(deleteUser(id));
      dispatch(getAllUsers());
      if (localStorage.getItem('logi_auth_token')) {
        await dispatch(
          deleteLogiUser({
            user,
            externalDashboardIP,
            logiSupervisorUserName,
            logiSupervisorPswd,
          }),
        );
      }
      showSuccessMessage('User successfully deleted.', TOAST_TIMEOUT);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to delete user: ${errorMessage}.`);

    } finally {
      dispatch(getAllUsers());
    }
  };
};

export const getUserTeams = (id: number) =>
  ({ type: GET_USER_TEAMS, payload: http.get(`/users/${id}/teams`) });

const updateUserState = (id: number, userID:string, newState:string) => {
  const requestBody = {
    id,
    state: newState,
    userID,
  };
  return ({ type: UPDATE_USER_STATE, payload: http.put(`/users/${id}/state`, requestBody) });
};
export const updateUserStateAndRefresh = (id: number, userID:string, newState:string) => {
  return async (dispatch: any) => {
    try {
      await dispatch(updateUserState(id, userID, newState));
      dispatch(getAllUsers());
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to change account state: ${errorMessage}.`);
      throw e;
    }
  };
};

export const clearErrors = () => ({ type: USERS_CLEAR_ERRORS, payload: null });

// table column setting
export const getUserSettings = (userSettingCategory: any, userId: string) => {
  return ({ type: GET_USER_SETTINGS, payload: http.get(`/users/${userId}/settings?name=${userSettingCategory}`) });
};
export const putUserSettings = (userSettingCategory: any, userId: any, userSettings:any) => {
  const settingInJson = JSON.stringify(userSettings);
  const userSettingsData = {
    name: userSettingCategory,
    type: 'json',
    user: userId,
    setting: settingInJson,
  };
  const requestBody = [userSettingsData];
  return ({ type: PUT_USER_SETTINGS, payload: http.put(`/users/${userId}/settings`, requestBody) });
};

// User Sessions
export const getTimezones = () => {
  return ({ type: GET_TIMEZONES, payload: http.get('/session/timezones') });
};

export const updateShowRemoveCaseInWarrantConfirmation = (payload: boolean) => {
  return ({
    type: UPDATE_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
    payload: setLocalStorageHideIncludeCaseConfirmationSetting(payload),
  });
};

export const getShowRemoveCaseInWarrantConfirmation = () => {
  return ({
    type: GET_HIDE_INCLUDE_CASE_IN_WARRANT_CONFIRMATION,
    payload: getLocalStorageHideIncludeCaseConfirmationSetting(),
  });
};
