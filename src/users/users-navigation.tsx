import React from 'react';
import {
  Navbar, NavbarGroup, NavbarHeading, Button, Alignment,
} from '@blueprintjs/core';
import { withRouter } from 'react-router-dom';

@(withRouter as any)
class UsersNavigation extends React.Component<any, any> {
  render() {
    return (
      <div data-scroll-to-id="templates">
        <Navbar fixedToTop style={{ marginTop: '85px', background: '#f0f0f0' }}>
          <NavbarGroup align={Alignment.LEFT}>
            <NavbarHeading>Users</NavbarHeading>
          </NavbarGroup>
          <NavbarGroup align={Alignment.RIGHT}>
            <Button
              onClick={() => this.props.history.push({
                pathname: 'users/config',
              })}
              icon="cog"
            />
          </NavbarGroup>
        </Navbar>
      </div>
    );
  }
}

export default UsersNavigation;
