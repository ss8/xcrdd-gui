import React, { Component, Fragment } from 'react';
import { getAssignedRoles } from '../users-util';

export default class AssignedRoleCellRenderer extends Component<any, any> {
  render() {
    return (
      <Fragment>
        { getAssignedRoles(this.props.data).join() }
      </Fragment>
    );
  }
}
