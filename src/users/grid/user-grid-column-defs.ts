import { dateFilterParams } from '../../shared/constants/index';
import { COMMON_GRID_COLUMN_FIELD_NAMES } from '../../shared/commonGrid/grid-enums';
import { stringArrayComparator } from '../../shared/commonGrid/grid-util';

export default [
  {
    headerName: '', field: COMMON_GRID_COLUMN_FIELD_NAMES.Action, cellRenderer: 'actionMenuCellRenderer',
  },
  {
    headerName: 'User ID', field: 'userID', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'userID',
  },
  {
    headerName: 'Email', field: 'email', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'email',
  },
  {
    headerName: 'First', field: 'firstName', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'firstName',
  },
  {
    headerName: 'Last', field: 'lastName', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'lastName',
  },
  {
    headerName: 'Roles', field: 'assignedRoles', sortable: true, filter: 'agTextColumnFilter', resizable: true, filterParams: { textCustomComparator: stringArrayComparator }, tooltipField: 'assignedRoles',
  },
  {
    headerName: 'State', field: 'state', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'state',
  },
  {
    headerName: 'Last Login',
    field: 'lastlogin',
    sortable: true,
    filter: 'agDateColumnFilter',
    resizable: true,
    filterParams: dateFilterParams,
    cellRenderer: 'datetimeCellRenderer',
    tooltipField: 'lastlogin',
  },
  {
    headerName: 'Last Login IP', field: 'lastLoginAddress', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'lastLoginAddress',
  },
];
