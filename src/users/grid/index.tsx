import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import '../users.scss';
import CommonGrid, { ICommonGridProperties } from '../../shared/commonGrid';
import UserGridColumnDefs from './user-grid-column-defs';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../shared/commonGrid/grid-enums';
import { USER_PRIVILEGE } from '../user-enum';
import { CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';
import ActionMenuCellRenderer from '../../shared/commonGrid/action-menu-cell-renderer';
import DateTimeCellRenderer from '../../shared/commonGrid/date-cell-renderer';
import AssignedRoleCellRenderer from './role-cell-renderer';
import { getAssignedRoles } from '../users-util';

class UsersGrid extends React.Component<any, any> {
  USER_GRID_SETTINGS = 'UserGridSettings';

  hasPrivilegesToCreate = true;

  hasPrivilegeToDelete = true;

  hasPrivilegetoView = true;

  constructor(props: any) {
    super(props);
    this.state = {
      selection: [],
    };
  }

  getUsers = (users: any) => {
    if (users === undefined) return undefined;
    return users.map((user:any) => {
      const assignedRoles = getAssignedRoles(user);
      return { ...user, assignedRoles };
    });
  }

  saveGridSettings = (userGridSettings: {[key: string]: any}) => {
    if (userGridSettings !== null) {
      this.props.putUserSettings(this.USER_GRID_SETTINGS, this.props.authentication.userId,
        userGridSettings);
    }
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
  }

  componentDidMount() {
    this.props.getAllUsers();
    const { privileges } = this.props.authentication;
    this.hasPrivilegesToCreate = privileges !== undefined &&
      (privileges.includes(USER_PRIVILEGE.Edit) || privileges.includes(USER_PRIVILEGE.Admin));
    this.hasPrivilegeToDelete = privileges !== undefined &&
      (privileges.includes(USER_PRIVILEGE.Delete) || privileges.includes(USER_PRIVILEGE.Admin));
    this.hasPrivilegetoView = privileges !== undefined &&
      (privileges.includes(USER_PRIVILEGE.View) || privileges.includes(USER_PRIVILEGE.Admin));
  }

  /**
   * @returns the current login user
   */
  getCurrentUser = () => {
    const users = this.props.users.users.data;
    return users.find((user:any) => (user.id === this.props.authentication.userDBId));
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getUserSettings(this.USER_GRID_SETTINGS, this.props.authentication.userId);
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    const user = { ...this.state.selection[0] };
    if (type === COMMON_GRID_ACTION.Delete) {
      this.props.history.push({ pathname: '/users/delete', state: this.state });
    } else if (type === COMMON_GRID_ACTION.Edit) {
      this.props.history.push({ pathname: '/users/edit', state: { action: CONFIG_FORM_MODE.Edit, data: user, currentUser: this.getCurrentUser() } });
    } else {
      this.props.history.push({ pathname: '/users/view', state: { action: CONFIG_FORM_MODE.View, data: user, currentUser: this.getCurrentUser() } });
    }
  }

  showActionMenuItem = (menuId:string, rowData: any, ctx: any) => {
    if (menuId === COMMON_GRID_ACTION.Delete) {
      // disallow user from deleting himself
      if (rowData.userID === ctx.props.authentication.userId) {
        return false;
      }
      return this.hasPrivilegeToDelete;
    }
    return true;
  }

  handleCreateNew = () => { this.props.history.push({ pathname: '/users/create', action: 'create', currentUser: this.getCurrentUser() }); }

  handleRefresh = () => {
    this.props.getAllUsers();
  }

  render() {
    const frameworkComponents = {
      actionMenuCellRenderer: ActionMenuCellRenderer,
      datetimeCellRenderer: DateTimeCellRenderer,
      assignedRoleCellRenderer: AssignedRoleCellRenderer,
    };

    const commonGridProps: ICommonGridProperties = {
      onRefresh: this.handleRefresh,
      onSelectionChanged: this.handleSelection,
      onActionMenuItemClick: this.onActionMenuItemClick,
      rowSelection: ROWSELECTION.SINGLE,
      columnDefs: UserGridColumnDefs,
      rowData: this.getUsers(this.props.users.users.data),
      context: this,
      pagination: true,
      enableSorting: true,
      enableFilter: true,
      enableBrowserTooltips: true,
      handleCreateNew: this.handleCreateNew,
      createNewText: 'New User',
      hasPrivilegeToCreateNew: this.hasPrivilegesToCreate,
      hasPrivilegeToDelete: this.hasPrivilegeToDelete,
      hasPrivilegetoView: this.hasPrivilegetoView,
      showActionMenuItem: this.showActionMenuItem,
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
      frameworkComponents,
      noResults: (!this.props.users || !this.props.users.users ||
        !this.props.users.users.data || !this.props.users.users.data.length),
    };
    return (
      <div className="grid-container" style={{ height: 'inherit' }}>
        <CommonGrid {...commonGridProps} />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
});

export default withRouter(connect(mapStateToProps, null)(UsersGrid));
