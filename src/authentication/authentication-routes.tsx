import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Login from './components/login';
import PasswordUpdate from './components/passwordUpdate';

const moduleRoot = '/authentication';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route path={`${moduleRoot}/login`} render={() => <Login {...props} />} />
    <Route path={`${moduleRoot}/passwordupdate`} render={() => <PasswordUpdate {...props} />} />
  </Fragment>
);

export default ModuleRoutes;
