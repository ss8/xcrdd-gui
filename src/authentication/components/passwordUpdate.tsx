import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Icon } from '@blueprintjs/core';

import { getDXSettingsAndReturn } from '../../xcipio/discovery-xcipio-actions';
import ss8logo from '../../assets/ss8-logo.png';
import { toPasswordPolicyDescription } from '../../policies/policies-util';
import './passwordupdate.scss';

const Login = '/authentication/login';

const defaultErrorMessage = 'Wrong password. Please try again.';
const defaultErrorMessageForAgainPolicy = 'New passwords against the password policy. Please try again';
const defaultMessageForPasswordNotfirmed = 'New passwords are different. Please try again.';

class PasswordUpdate extends React.Component<any, any> {
  private password: any;

  private newpassword: any;

  private confirmpassword: any;

  private submit: any;

  state = {
    error: false,
    userFromLogin: '',
    passwordFromLogin: '',
    password: '',
    newpassword: '',
    confirmpassword: '',
    prePathname: '',
    passwordPolicy: '',
  };

  componentDidMount() {
    if (this.props.authentication.errorStatus === '489') {
      this.setState({
        userFromLogin: this.props.location.username,
        passwordPolicy: toPasswordPolicyDescription(this.props.policies.passwordPolicy),
      });
    } else {
      this.setState({
        userFromLogin: this.props.location.username,
        prePathname: this.props.location.curpath,
        passwordPolicy: toPasswordPolicyDescription(this.props.policies.passwordPolicy),
      });
    }
  }

  async componentDidUpdate(prevProps: any) {
    const {
      isAuthenticated,
      errorStatus,
      privileges,
      userId: userName,
    } = this.props.authentication;
    const passwordUpdateFlag = this.props.authentication.passwordUpdate;
    if (prevProps.authentication.passwordUpdate && !passwordUpdateFlag) {
      this.props.history.push(this.state.prePathname);
    }
    if ((errorStatus === '400' || errorStatus === '401') && prevProps.authentication.errorStatus !== errorStatus) {
      this.props.getPasswordPolicy();
    }
    if ((errorStatus === '487' || errorStatus === '488') && prevProps.authentication.errorStatus !== errorStatus) {
      this.props.doFetchLoginMessage();
      this.props.history.push({
        pathname: Login,
        password: this.state.newpassword,
        user: this.state.userFromLogin,
      });
    }
    if (isAuthenticated) {
      const {
        externalDashboardIP,
        logiOAuthClientId,
        logiOAuthClientName,
        logiAdminAccount,
        logiAdminUserName,
        logiAdminPswd,
        logiSupervisorUserName,
        logiSupervisorPswd,
      } = await this.props.getDXSettingsAndReturn();

      this.props.authenticateLogiUser({
        user: userName,
        password: this.state.confirmpassword,
        privileges,
        externalDashboardIP,
        logiOAuthClientId,
        logiOAuthClientName,
        logiAdminAccount,
        logiAdminUserName,
        logiAdminPswd,
        logiSupervisorUserName,
        logiSupervisorPswd,
      });
    }
  }

  componentWillUnmount() {
    this.props.undoSetPasswordUpdateFlag();
  }

  handleChange = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.clearErrorStatus();
    this.setState({ error: false, passwordPolicy: '' });
    if (this.state.newpassword !== this.state.confirmpassword) {
      this.setState({
        error: true,
        password: '',
        newpassword: '',
        confirmpassword: '',
      });
    } else {
      this.props.clearPasswordPolicy();
      this.props.doLogin({
        action: 'password',
        newPassword: this.state.newpassword,
        password: this.state.password,
        user: this.state.userFromLogin,
      });
    }
  }

  handleCancel = (e: any) => {
    e.preventDefault();
    this.setState({ error: false, passwordPolicy: '' });
    this.props.undoSetPasswordUpdateFlag();
    this.props.clearErrorStatus();
    this.props.clearPasswordPolicy();
    if (this.props.authentication.isAuthenticated) {
      this.props.history.push(this.state.prePathname);
    } else {
      this.props.history.push(Login);
    }
  }

  onKeyDown = (e: any) => {
    if (e.key === 'Enter') {
      this.handleSubmit(e);
    }
  }

  render() {
    const { error } = this.state;
    const { isAuthenticated, errorStatus, passwordUpdate } = this.props.authentication;
    return (
      <div className={isAuthenticated ? 'passwordupdate-authenticated-container' : 'passwordupdate-container'}>
        <div className="passwordupdate-form">
          <div className="passwordupdate-form-header">
            <img className="login-logo" src={ss8logo} alt="SS8 Login" />
            <h1>Discovery</h1>
          </div>
          <h2>Please update password</h2>
          <form>
            <div className="bp3-input-group .modifier">
              <input
                type="password"
                name="password"
                className="bp3-input"
                placeholder="Enter current password..."
                onKeyDown={this.onKeyDown}
                value={this.state.password}
                ref={(input:any) => { this.password = input; }}
                onChange={this.handleChange}
              />

            </div>
            <div className="bp3-input-group .modifier">
              <input
                type="password"
                name="newpassword"
                className="bp3-input"
                placeholder="Enter new password..."
                onKeyDown={this.onKeyDown}
                value={this.state.newpassword}
                ref={(input:any) => { this.newpassword = input; }}
                onChange={this.handleChange}
              />
            </div>
            <div className="bp3-input-group .modifier">
              <input
                type="password"
                name="confirmpassword"
                className="bp3-input"
                placeholder="Enter new password..."
                onKeyDown={this.onKeyDown}
                value={this.state.confirmpassword}
                ref={(input:any) => { this.confirmpassword = input; }}
                onChange={this.handleChange}
              />
            </div>
            {error && (<div className="errorDisplay"><Icon intent="danger" icon="error" iconSize={18} /> {defaultMessageForPasswordNotfirmed}</div>)}
            {errorStatus === '400' && (<div className="errorDisplay"><Icon intent="danger" icon="error" iconSize={18} /> {defaultErrorMessageForAgainPolicy}</div>)}
            {errorStatus === '401' && passwordUpdate && (<div className="errorDisplay"><Icon intent="danger" icon="error" iconSize={18} />{defaultErrorMessage}</div>)}
            {this.state.passwordPolicy.length > 0 && (<div className="passwordPolicyDisplay">{this.state.passwordPolicy}</div>) }
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <button
                type="submit"
                className="bp3-button bp3-large bp3-icon-submit"
                onClick={this.handleCancel}
                value="submit"
              >cancel
              </button> &nbsp; &nbsp;
              <button
                type="submit"
                className="bp3-button bp3-large bp3-icon-submit"
                onClick={this.handleSubmit}
                ref={(input) => { this.submit = input; }}
                value="submit"
              >submit
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bAttachments_enabled: attachmentEnabled,
      isAuditEnabled,
      logiOAuthClientId,
      logiOAuthClientName,
      logiAdminAccount,
      logiAdminUserName,
      logiAdminPswd,
      logiSupervisorUserName,
      logiSupervisorPswd,
    },
  },
}: any) => ({
  attachmentEnabled,
  isAuditEnabled,
  logiOAuthClientId,
  logiOAuthClientName,
  logiAdminAccount,
  logiAdminUserName,
  logiAdminPswd,
  logiSupervisorUserName,
  logiSupervisorPswd,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getDXSettingsAndReturn,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(PasswordUpdate);
