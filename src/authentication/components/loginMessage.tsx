import React, { Fragment } from 'react';
import './login.scss';

const LoginMessage = (props: any) => {
  const { loginMessage, errorStatus } = props.authentication;
  return (
    <Fragment>
      <div className="login-message">
        <h4>Login Message</h4>
        <div className="iframe-container">
          <iframe srcDoc={loginMessage} />
          <div className="button-holder">
            {errorStatus === '487' && <button type="button" onClick={props.handleCancel}>Cancel</button> }&nbsp; &nbsp;
            <button type="button" onClick={props.handleClick}>{ errorStatus === '487' ? 'Accept' : 'Ok'}</button>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default LoginMessage;
