import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Icon } from '@blueprintjs/core';

import { AppToaster } from '../../shared/toaster';
import ss8logo from '../../assets/ss8-logo.png';
import { getDXSettingsAndReturn } from '../../xcipio/discovery-xcipio-actions';
import LoginMessage from './loginMessage';
import './login.scss';

const DEFAULT_PAGE = '/warrants';

const defaultErrorMessage = 'Wrong password or username. Try again.';

class Login extends React.Component<any, any> {
   private userName: any;

   private password: any;

   constructor(props: any) {
     super(props);
     this.state = {
       login: '',
       password: '',
     };
   }

  checkAuthentication = () => {
    // handle existing auth token in local storage
    // should eventually have "isValid" check on server
    const { isAuthenticated } = this.props.authentication;
    const { token } = window.localStorage;
    if (isAuthenticated) {
      AppToaster.clear();
      this.props.history.push(DEFAULT_PAGE);
    } else if (token) {
      this.props.doCheckToken(token);
    }
  }

  componentDidMount() {
    this.checkAuthentication();
    if (this.props.location.password !== null && this.props.location.user !== null) {
      this.setState({
        login: this.props.location.user,
        password: this.props.location.password,
      });
    }
  }

  async componentDidUpdate(prevProps: any) {
    const {
      isAuthenticated,
      errorStatus,
      userId: userName,
      privileges,
    } = this.props.authentication;

    const oldIsAuthenticated = prevProps.authentication.isAuthenticated;
    if (oldIsAuthenticated !== isAuthenticated) this.checkAuthentication();
    if (errorStatus === '401' && prevProps.authentication.errorStatus !== errorStatus) {
      this.setState({ password: '' });
    } else if (errorStatus === '489' && prevProps.authentication.errorStatus !== errorStatus) {
      this.props.setPasswordUpdateFlag();
      this.props.clearLoginMessage();
      this.props.history.push({
        pathname: '/authentication/passwordupdate',
        username: this.state.login,
      });
    } else if ((errorStatus === '488' || errorStatus === '487') &&
    (prevProps.authentication.errorStatus === '' || prevProps.authentication.errorStatus === '489')) {
      this.props.doFetchLoginMessage();
    }

    if (isAuthenticated) {
      const {
        isExternalDashboardEnabled,
        externalDashboardIP,
        logiOAuthClientId,
        logiOAuthClientName,
        logiAdminAccount,
        logiAdminUserName,
        logiAdminPswd,
        logiSupervisorUserName,
        logiSupervisorPswd,
      } = await this.props.getDXSettingsAndReturn();

      if (isExternalDashboardEnabled && externalDashboardIP) {
        await this.props.authenticateLogiUser({
          user: this.state.login,
          password: this.state.password,
          privileges,
          externalDashboardIP,
          logiOAuthClientId,
          logiOAuthClientName,
          logiAdminAccount,
          logiAdminUserName,
          logiAdminPswd,
          logiSupervisorUserName,
          logiSupervisorPswd,
        });
      }
    }
  }

  handleLoginMessageClick = () => {
    const { errorStatus } = this.props.authentication;
    if (errorStatus === '488') {
      this.props.doLogin({
        action: 'message',
        password: this.state.password,
        user: this.state.login,
      });
    } else if (errorStatus === '487') {
      this.props.doLogin({
        action: 'confirm',
        password: this.state.password,
        user: this.state.login,
      });
    }
  }

  handleLoginMessageCancel = () => {
    this.props.clearErrorStatus();
    this.props.clearLoginMessage();
  }

  handleChange = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.doLogin({ user: this.state.login, password: this.state.password });
  }

  onKeyDown = (e: any) => {
    if (e.key === 'Enter') this.handleSubmit(e);
  }

  render() {
    const { errorStatus, loginMessage } = this.props.authentication;
    return (
      <div className="login-container">
        {(errorStatus === '488' || errorStatus === '487') ? (
          <LoginMessage
            loginMessage={loginMessage}
            {...this.props}
            handleClick={this.handleLoginMessageClick}
            handleCancel={this.handleLoginMessageCancel}
          />
        ) :
          (
            <Fragment>
              <div className="login-form">
                <div className="login-form-header">
                  <img className="login-logo" src={ss8logo} alt="SS8 Login" />
                  <h1>Discovery</h1>
                </div>
                <form>
                  <div className="bp3-input-group .modifier">
                    <input
                      type="text"
                      name="login"
                      className="bp3-input"
                      placeholder="Enter your login username..."
                      value={this.state.login}
                      ref={(input:any) => { this.userName = input; }}
                      onChange={this.handleChange}
                      onKeyDown={this.onKeyDown}
                    />

                  </div>
                  <div className="bp3-input-group .modifier">
                    <input
                      type="password"
                      name="password"
                      className="bp3-input"
                      placeholder="Enter your password..."
                      value={this.state.password}
                      ref={(input:any) => { this.password = input; }}
                      onChange={this.handleChange}
                      onKeyDown={this.onKeyDown}
                    />
                  </div>

                  { (errorStatus) && (
                  <div className="errorDisplay">
                    <Icon intent="danger" icon="error" iconSize={18} />&nbsp; &nbsp; <span>{ defaultErrorMessage }</span>
                  </div>
                  )}
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <button
                      type="submit"
                      onClick={this.handleSubmit}
                      className="bp3-button bp3-large bp3-icon-submit"
                      value="submit"
                    >submit
                    </button>
                  </div>
                </form>
              </div>
            </Fragment>
          )}
      </div>
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      externalDashboardIP,
      logiOAuthClientId,
      logiOAuthClientName,
      logiAdminAccount,
      logiAdminUserName,
      logiAdminPswd,
      logiSupervisorUserName,
      logiSupervisorPswd,
    },
  },
}: any) => ({
  externalDashboardIP,
  logiOAuthClientId,
  logiOAuthClientName,
  logiAdminAccount,
  logiAdminUserName,
  logiAdminPswd,
  logiSupervisorUserName,
  logiSupervisorPswd,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getDXSettingsAndReturn,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Login);
