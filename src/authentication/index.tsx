import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  doLogin,
  doCheckToken,
  clearErrorStatus,
  setPasswordUpdateFlag,
  undoSetPasswordUpdateFlag,
  doFetchLoginMessage,
  clearLoginMessage,
  checkLogiToken,
  authenticateLogiUser,
} from 'data/authentication/authentication.actions';
import { getPasswordPolicy, clearPasswordPolicy } from '../policies/policy-actions';
import ModuleRoutes from './authentication-routes';

const Authentication = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  policies: state.policies,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  doLogin,
  doCheckToken,
  clearErrorStatus,
  setPasswordUpdateFlag,
  undoSetPasswordUpdateFlag,
  doFetchLoginMessage,
  clearLoginMessage,
  getPasswordPolicy,
  clearPasswordPolicy,
  checkLogiToken,
  authenticateLogiUser,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Authentication));
