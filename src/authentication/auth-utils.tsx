export default class AuthUtils {
    static hasPermissions = (routePermissions: Array<any>, privileges: Array<string>) => {
      if (!routePermissions || !routePermissions.length) return false;
      if (!privileges || !privileges.length) return false;
      let hasPermissions = false;
      for (let i = 0; i < routePermissions.length; i += 1) {
        if (privileges.indexOf(routePermissions[i]) !== -1) {
          hasPermissions = true;
          break;
        }
      }
      return hasPermissions;
    }
}
