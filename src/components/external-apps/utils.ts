import { NavbarItem, NavbarSubItem } from '../../shared/navbar/types';
import navbarConfig from '../../shared/navbar/navbar-config';

type ExternalAppConfig = NavbarItem | NavbarSubItem | null;

// eslint-disable-next-line import/prefer-default-export
export function findExternalAppConfig(pathname: string, isExternalDashboardEnabled: boolean,
  isSecurityGatewayEnabled: boolean): ExternalAppConfig {
  const navbarConfiguration = navbarConfig(isExternalDashboardEnabled, isSecurityGatewayEnabled);

  let externalAppConfig: ExternalAppConfig = null;

  navbarConfiguration.forEach((itemConfig) => {
    if (itemConfig.children?.length) {
      itemConfig.children.forEach((subitemConfig) => {
        if (subitemConfig.route === pathname) {
          externalAppConfig = subitemConfig;
        }
      });
    } else if (itemConfig.route === pathname) {
      externalAppConfig = itemConfig;
    }
  });

  return externalAppConfig;
}
