import React from 'react';
import { mount, shallow } from 'enzyme';
import { Callout } from '@blueprintjs/core';
import { ExternalApps, ExternalAppsProps } from './index';
import IFrame from '../iframe';
import XMS from './xms';

describe('<ExternalApps />', () => {
  let wrapper;
  let props: ExternalAppsProps;

  beforeEach(() => {
    props = {
      location: {
        pathname: '',
      },
      isExternalDashboardEnabled: true,
      isSecurityGatewayEnabled: true,
      externalDashboardIP: '127.0.0.1',
      logiTokenUpdated: false,
      handleLogout: jest.fn(),
    };
  });

  it('should render an iframe if empty url if no match for the pathname', () => {
    props.location.pathname = '/invalid/path';
    wrapper = mount(<ExternalApps {...props} />);
    expect(wrapper.find(IFrame)).toHaveLength(1);
    expect(wrapper.find(IFrame).prop('url')).toEqual('');
  });

  it('should render the XMS component for paths that include "xms"', () => {
    props.location.pathname = '/afs/xms';
    wrapper = mount(<ExternalApps {...props} />);
    expect(wrapper.find(XMS)).toHaveLength(1);
    expect(wrapper.find(XMS).prop('pathName')).toEqual('/#!af');
  });

  it('should render an iframe for the /external/dashboard path', () => {
    props.location.pathname = '/external/dashboard';
    localStorage.setItem('logi_auth_token', 'some_token');
    wrapper = mount(<ExternalApps {...props} />);
    expect(wrapper.find(IFrame)).toHaveLength(1);
    expect(wrapper.find(IFrame).prop('url')).toEqual('127.0.0.1/zoomdata/visualization/all?access_token=some_token');
  });

  it('should render a Callout component if path is /external/dashboard but logi auth empty', () => {
    props.location.pathname = '/external/dashboard';
    localStorage.removeItem('logi_auth_token');
    wrapper = mount(<ExternalApps {...props} />);
    expect(wrapper.find(Callout)).toHaveLength(1);
  });
});
