import { findExternalAppConfig } from './utils';
import navbarConfig from '../../shared/navbar/navbar-config';
import { AF_ROUTE } from '../../constants';

describe('findExternalAppConfig', () => {
  const navbarConfiguration = navbarConfig(true, true);

  it('should return null if no config found', () => {
    expect(findExternalAppConfig('/invalid/path', true, true)).toBeNull();
  });

  it('should return the NavbarItem for a child pathname', () => {
    expect(findExternalAppConfig('/xms/reports', true, true)).toEqual(navbarConfiguration.find(({ route }) => route === '/xms/reports'));
  });

  it('should return the NavbarSubItem for a child pathname', () => {
    const { children = [] } = navbarConfiguration.find(({ route }) => route === AF_ROUTE) ?? {};
    expect(findExternalAppConfig('/afs/xms', true, true)).toEqual(children[1]);
  });
});
