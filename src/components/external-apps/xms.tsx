import React from 'react';
import { COOKIE, getCookie, setCookie } from '../../utils';
import IFrame from '../iframe';

interface XMSProps {
  pathName: string
  handleLogout: CallableFunction
}

function getClassName(pathname: string) {
  let className = 'xms';

  if (pathname.includes('#!af') || pathname.includes('#!sysconfhome')) {
    className = `${className} as-subtab`;
  }

  return className;
}

const XMS = ({ handleLogout, pathName }: XMSProps) => {
  const jSessionID = getCookie(COOKIE.JSESSIONID);
  if (jSessionID === '') {
    handleLogout();
    return null;
  }

  const XMSIP = process.env.NODE_ENV === 'development' && process.env.REACT_APP_PROXY_TARGET
    ? process.env.REACT_APP_PROXY_TARGET.slice(0, process.env.REACT_APP_PROXY_TARGET?.lastIndexOf(':'))
    : `${window.location.protocol}//${window.location.hostname}`;

  const className = getClassName(pathName);
  setCookie(COOKIE.LOGINFROM, 'DX');

  return (
    <IFrame url={`${XMSIP + pathName}`} key={pathName} addClassName={className} />
  );
};

export default XMS;
