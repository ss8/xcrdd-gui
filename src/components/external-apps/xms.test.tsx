import React from 'react';
import { mount } from 'enzyme';
import XMS from './xms';
import { setCookie, COOKIE, removeCookie } from '../../utils';
import IFrame from '../iframe';

describe('<XMS />', () => {
  let wrapper;

  it('should call handleLogout if JSESSIONID is empty', () => {
    const handleLogout = jest.fn();
    wrapper = mount(<XMS handleLogout={handleLogout} pathName="" />);
    expect(handleLogout).toBeCalledWith();
  });

  it('should render nothing if JSESSIONID is empty', () => {
    wrapper = mount(<XMS handleLogout={jest.fn()} pathName="" />);
    expect(wrapper.children()).toHaveLength(0);
  });

  it('should render an iframe if JSESSIONID exists with proper url and className for afs', () => {
    setCookie(COOKIE.JSESSIONID, 'someValidCookie');
    wrapper = mount(<XMS handleLogout={jest.fn()} pathName="/#!af" />);
    expect(wrapper.find(IFrame)).toHaveLength(1);
    expect(wrapper.find(IFrame).prop('url')).toEqual('http://localhost/#!af');
    expect(wrapper.find(IFrame).prop('addClassName')).toEqual('xms as-subtab');
    removeCookie(COOKIE.JSESSIONID);
  });

  it('should render an iframe if JSESSIONID exists with proper url and className for system settings', () => {
    setCookie(COOKIE.JSESSIONID, 'someValidCookie');
    wrapper = mount(<XMS handleLogout={jest.fn()} pathName="/#!sysconfhome" />);
    expect(wrapper.find(IFrame)).toHaveLength(1);
    expect(wrapper.find(IFrame).prop('url')).toEqual('http://localhost/#!sysconfhome');
    expect(wrapper.find(IFrame).prop('addClassName')).toEqual('xms as-subtab');
    removeCookie(COOKIE.JSESSIONID);
  });

  it('should render an iframe if JSESSIONID exists with proper url and className for reports', () => {
    setCookie(COOKIE.JSESSIONID, 'someValidCookie');
    wrapper = mount(<XMS handleLogout={jest.fn()} pathName="/#!reports" />);
    expect(wrapper.find(IFrame)).toHaveLength(1);
    expect(wrapper.find(IFrame).prop('url')).toEqual('http://localhost/#!reports');
    expect(wrapper.find(IFrame).prop('addClassName')).toEqual('xms');
    removeCookie(COOKIE.JSESSIONID);
  });
});
