import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Callout, Intent } from '@blueprintjs/core';
import { handleLogout as authenticationHandleLogout } from 'data/authentication/authentication.actions';
import IFrame from '../iframe';
import XMS from './xms';
import { findExternalAppConfig } from './utils';

export interface ExternalAppsProps {
  location: {
    pathname: string
  }
  isExternalDashboardEnabled: boolean,
  isSecurityGatewayEnabled: boolean,
  externalDashboardIP: string,
  logiTokenUpdated: boolean,
  handleLogout: CallableFunction,
}

export const ExternalApps = ({
  location,
  isExternalDashboardEnabled,
  isSecurityGatewayEnabled,
  externalDashboardIP,
  logiTokenUpdated,
  handleLogout,
}: ExternalAppsProps) => {
  const [tab, setTab] = useState({ route: '', url: '' });
  const [logiAuthToken, setLogiAuthToken] = useState('');

  useEffect(() => {
    const externalAppConfig = findExternalAppConfig(location.pathname, isExternalDashboardEnabled,
      isSecurityGatewayEnabled);

    if (externalAppConfig == null) {
      return;
    }

    const { route, url = '' } = externalAppConfig;

    setTab({ route, url });

    const logiToken = localStorage.getItem('logi_auth_token') as string;

    if (isExternalDashboardEnabled && logiToken) {
      setLogiAuthToken(logiToken);
    }
  }, [location.pathname, logiTokenUpdated]);

  let displayContent = <IFrame url={tab.url} />;

  if (location.pathname.includes('xms')) {
    displayContent = <XMS pathName={tab.url} handleLogout={handleLogout} />;
  } else if (location.pathname.includes('external/dashboard')) {
    displayContent = (logiAuthToken && logiAuthToken !== '') ? (
      <IFrame
        url={`${externalDashboardIP}/zoomdata/visualization/all?access_token=${logiAuthToken}`}
        addClassName="logi"
      />
    ) : (
      <Callout intent={Intent.WARNING}>
        We are unable to display the content of this page.
        Please contact your administrator for assistance.
      </Callout>
    );
  }

  return displayContent;
};

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isExternalDashboardEnabled,
      isSecurityGatewayEnabled,
      externalDashboardIP,
    },
  },
  authentication: {
    logiTokenUpdated,
  },
}: any) =>
  ({
    isExternalDashboardEnabled,
    isSecurityGatewayEnabled,
    externalDashboardIP,
    logiTokenUpdated,
  });

const mapDispatchToProps = {
  handleLogout: authenticationHandleLogout,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ExternalApps));
