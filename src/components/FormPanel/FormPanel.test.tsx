import React from 'react';
import { mount } from 'enzyme';
import FormPanel from './FormPanel';

describe('<FormPanel />', () => {
  it('should render the title and content', () => {
    const wrapper = mount(
      <FormPanel title="Form title">
        Some Panel content
      </FormPanel>,
    );
    expect(wrapper.find('h2').text()).toEqual('Form title');
    expect(wrapper.find('span').text()).toEqual('Some Panel content');
  });

  it('should render the content if title not available', () => {
    const wrapper = mount(
      <FormPanel>
        Some Panel content
      </FormPanel>,
    );
    expect(wrapper.text()).toEqual('Some Panel content');
  });
});
