import React, { FunctionComponent, ReactNode } from 'react';
import './form-panel.scss';

type Props = {
  title?: string | null,
  children: ReactNode
}

export const FormPanel: FunctionComponent<Props> = ({
  title = '',
  children,
}: Props) => {
  const renderTitle = () => {
    if (title) {
      return <h2 className="formPanelTitle">{title}</h2>;
    }
    return <div className="formPanelTitle" />;
  };

  return (
    <div className="formPanel" data-testid="FormPanel">
      {renderTitle()}
      <span>
        {children}
      </span>
    </div>
  );
};

export default FormPanel;
