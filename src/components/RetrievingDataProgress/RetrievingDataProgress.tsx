import React, { FC } from 'react';
import { Dialog, Classes, ProgressBar } from '@blueprintjs/core';
import './retrieving-data-progress.scss';

type Props = {
    isOpen:boolean,
    message: string
}

const RetrievingDataProgress:FC<Props> = ({ isOpen, message }:Props) => {
  return (
    <Dialog isOpen={isOpen} className="retrieving-data-progress">
      <div className={Classes.DIALOG_BODY}>
        <p className="retrieving-data-progress-title">{message}</p>
        <ProgressBar intent="primary" value={1} />
      </div>
    </Dialog>
  );
};

export default RetrievingDataProgress;
