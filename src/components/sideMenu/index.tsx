import React, {
  useState, FunctionComponent, ReactElement, Fragment,
} from 'react';
import {
  Collapse,
} from '@blueprintjs/core';
import './sideMenu.scss';
import { useHistory } from 'react-router-dom';

type subMenu={
    id:string,
    text:string,
    href?:string,
}
export type sideMenu={
    id:string,
    text:string,
    open:boolean
    children:subMenu[]
}

interface Props{
    activeMenuItem:string
    menuList:sideMenu[],
    setActiveMenuItem:any
}

const CustomCollapse:FunctionComponent<Props> = (props :Props):ReactElement => {
  const { activeMenuItem, menuList, setActiveMenuItem } = props;
  const [menu, setMenu] = useState<sideMenu[]>(menuList);
  const history = useHistory();

  const handleSideMenu = (e:any) => {
    const { id } = e.currentTarget;
    setMenu((prev:any) => {
      const nsm = prev.map((val:any) => {
        if (val.id === id) {
          return { ...val, open: !(val.open) };
        }
        return val;
      });
      return nsm;
    });
  };

  const goTORoute = (path:string) => {
    if (path.length > 0) {
      history.push(path);
    }
  };

  return (
    <Fragment>
      {menu.map((item:any) => {
        return (
          <div key={item.id} className="custom-side-menu">
            <div id={item.id} className="custom-side-menu-items" onClick={handleSideMenu}>
              {item.open ? <span className="bp3-icon bp3-icon-chevron-down" /> : <span className="bp3-icon bp3-icon-chevron-right" />}
              <div>{item.text}</div>
            </div>
            <Collapse
              isOpen={item.open}
              transitionDuration={500}
            >
              <div className="custom-side-submenu-items" onClick={(e) => setActiveMenuItem(e, item.text)}>
                {
                  item.children.map((subitem:any) => (
                    <div
                      className={activeMenuItem === subitem.text ? 'custom-side-submenu-items-active' : ' '}
                      id={subitem.text}
                      key={subitem.id}
                      onClick={() => goTORoute(subitem.href)}
                    >{subitem.text}
                    </div>
                  ))
              }
              </div>
            </Collapse>
          </div>
        );
      })}
    </Fragment>
  );
};

export default CustomCollapse;
