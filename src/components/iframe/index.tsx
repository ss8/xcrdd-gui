import React from 'react';
import Iframe from 'react-iframe';

import './style.scss';

const IFrame = ({ url, addClassName }:any) => {
  return (
    <Iframe
      url={url}
      width="100%"
      height="100%"
      position="relative"
      className={`dynamic-iframe ${addClassName}`}
    />
  );
};

export default IFrame;
