import { Classes, Dialog } from '@blueprintjs/core';
import CProgressBar from 'components/progress-bar';
import React, { FunctionComponent } from 'react';
import './progressDialog.scss';

type Props={
  isOpen:boolean,
  text?:string,
  value?:number|undefined
  handleClose?:any
}

const ProgressDialog:FunctionComponent<Props> = ({
  isOpen, text, value = 1, handleClose,
}:Props) => {
  return (
    <Dialog
      isOpen={isOpen}
      style={{ width: '500px' }}
      onClose={handleClose}
      className="custom-dialog"
    >
      <div className={Classes.DIALOG_CONTAINER}>
        <div className={Classes.DIALOG_BODY}>
          <div className="custom-dialog-text">{text}</div>
          <CProgressBar
            value={value}
          />
        </div>
      </div>
    </Dialog>
  );
};

export default ProgressDialog;
