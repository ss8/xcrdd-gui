import React from 'react';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';
import { stateValueFormatter } from '../../utils/valueFormatters';
import NotApplicableCellRenderer from '../NotApplicableCellRenderer';

type Props = {
  value?: string,
  node?: { group: boolean }
};

export default function FieldStatusCellRenderer({
  value,
  node,
  ...rest
}: Props): JSX.Element {
  if (node?.group) {
    return <span />;
  }

  if (value == null || value === '-' || value === '') {
    return (
      <NotApplicableCellRenderer />
    );
  }

  value = stateValueFormatter({ ...(rest as ICellRendererParams), value });

  return (
    <span className="access-function-status-cell-renderer">
      {value}
    </span>
  );
}
