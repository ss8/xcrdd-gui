import React from 'react';
import {
  Alert, IconName, MaybeElement, Intent,
} from '@blueprintjs/core';

interface IAlertProps {
  cancelButtonText?: string;
  canEscapeKeyCancel?: boolean;
  canOutsideClickCancel?: boolean;
  className?: string;
  confirmButtonText?: string,
  icon?: IconName | MaybeElement,
  intent?: Intent,
  isOpen: boolean,
  handleCancel?: any,
  handleConfirm?: any,
  children?: any
  style?: any

}
const CAlert = (props:IAlertProps) => {
  const {
    canEscapeKeyCancel = false,
    canOutsideClickCancel = false,
    confirmButtonText = 'OK',
    intent = 'primary',
  } = props;

  return (
    <Alert
      canEscapeKeyCancel={canEscapeKeyCancel}
      canOutsideClickCancel={canOutsideClickCancel}
      cancelButtonText={props.cancelButtonText}
      confirmButtonText={confirmButtonText}
      intent={intent}
      isOpen={props.isOpen}
      onCancel={props.handleCancel}
      onConfirm={props.handleConfirm}
      style={props.style}
    >
      {props.children}
    </Alert>
  );
};

export default CAlert;
