import React from 'react';
import {
  DateInput, Classes as ClassesDateInput, TimePrecision,
} from '@blueprintjs/datetime';
import moment from 'moment';
import { getDateTimeFormatTemplate } from '../../utils';

interface IDateInputProps {
  className?: string,
  closeOnSelection?: boolean;
  disabled?: boolean,
  fill?: boolean;
  maxDate?: Date,
  onChange?: any,
  reverseMonthAndYearMenus?: boolean;
  shortcuts?: boolean;
  timePrecision?: TimePrecision | undefined;
  value: Date | null,
}

const CDateInput = (props: IDateInputProps) => {
  const {
    className,
    maxDate,
    value,
    ...spreadProps
  } = props;
  return (
    <DateInput
      value={value ? new Date(value) : null}
      inputProps={{
        autoCorrect: 'off', autoComplete: 'off',
      }}
      className={`${ClassesDateInput.DATEINPUT} ${className}`}
      maxDate={maxDate || moment(new Date()).add(20, 'year').toDate()}
      formatDate={(date) => moment(date).format(getDateTimeFormatTemplate())}
      parseDate={(str) => moment(str, getDateTimeFormatTemplate()).toDate()}
      {...spreadProps}
    />
  );
};

export default CDateInput;
