import React, { FC, useEffect, useState } from 'react';
import { AgGridReact } from '@ag-grid-community/react';
import {
  CellValueChangedEvent,
  ColDef,
  GridApi,
  ICellRendererParams,
  GridReadyEvent,
} from '@ag-grid-enterprise/all-modules';
import { Button } from '@blueprintjs/core';
import ModalDialog from 'shared/modal/dialog';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import ActionCellRenderer from './ActionCellRenderer';
import IPCellEditor from '../../routes/AccessFunction/components/IPCellEditor';
import IpAddressCellRenderer from '../../routes/AccessFunction/components/IpAddressCellRenderer/IpAddressCellRenderer';
import IpAFNameCellRenderer from '../../routes/AccessFunction/components/IpAFNameCellRenderer/IpAFNameCellRenderer';
import * as utils from './AllowedIPsUpdateTable.utils';
import './allowed-ips-update-table.scss';

type Props = {
  rowData: DistributorConfigurationListenerAfWhiteList[],
  isEditing: boolean,
  ipTableColumnDefs: (ColDef)[],
  isDialogSubmitted: boolean,
  onAddNewIp: () => void
  onRowDataUpdated: (
    data: DistributorConfigurationListenerAfWhiteList[],
    validators: { hasAddressFormatError: boolean, hasNonUniqueAFError: boolean }
  ) => void;
  onCellKeyDown: () => void,
}

type AFWhiteListVersioned = {
  rowKey: number,
  actions?: string,
} & DistributorConfigurationListenerAfWhiteList;

const frameworkComponents = {
  actionCellRenderer: ActionCellRenderer,
  iPCellEditor: IPCellEditor,
  ipAddressCellRenderer: IpAddressCellRenderer,
  ipAFNameCellRenderer: IpAFNameCellRenderer,
};

const defaultColDef = {
  flex: 1,
  editable: false,
  floatingFilter: true,
};

let currentWhiteListIpPk = 0;

const AllowedIPsUpdateTable: FC<Props> = ({
  ipTableColumnDefs,
  isEditing,
  rowData,
  isDialogSubmitted,
  onAddNewIp,
  onRowDataUpdated,
  onCellKeyDown,
}: Props) => {
  const [gridApi, setGridApi] = useState<GridApi>();
  const [selectedIpToRemove, setSelectedIpToRemove] = useState<ICellRendererParams>();
  const [displayMessage, setDisplayMessage] = useState<string>('');
  const [rowDataVersioned, setRowDataVersioned] = useState<AFWhiteListVersioned[]>([]);
  const [accessFunctionOccurrences, setAccessFunctionOccurrences] = useState<{ [key: string]: number }>({});
  const [hasAddressFormatError, setAddressFormatError] = useState<boolean>(false);
  const [hasNonUniqueAFError, setNonUniqueAFError] = useState<boolean>(false);

  useEffect(() => {
    if (gridApi) {
      gridApi.setRowData(utils.getRowData(gridApi));
    }
  }, [isDialogSubmitted, gridApi]);

  useEffect(() => {
    const { currentVersionPk, versionedRowData } = utils.toVersionedRowData(rowData, currentWhiteListIpPk);

    currentWhiteListIpPk = currentVersionPk;
    setRowDataVersioned(versionedRowData);
    setAccessFunctionOccurrences(utils.getAccessFunctionOccurrences(versionedRowData));
    return () => {
      currentWhiteListIpPk = 0;
    };
  }, [rowData]);

  const handleAddNewIp = () => {
    if (!gridApi) {
      return;
    }
    const newRowData = utils.addNewItemToGridRowData<AFWhiteListVersioned>(gridApi, {
      afId: '',
      afName: '',
      afAddress: '',
      actions: '',
      rowKey: currentWhiteListIpPk += 1,
    });

    setAddressFormatError(true);
    onRowDataUpdated(newRowData, {
      hasAddressFormatError,
      hasNonUniqueAFError,
    });
    onAddNewIp();
  };

  const handleGridReady = (params: GridReadyEvent) => {
    setGridApi(params.api);
  };

  const handleCellValueChanged = (event: CellValueChangedEvent) => {
    if (!utils.cellValueHasChanged(event) || !gridApi) {
      return;
    }

    if (utils.isChangingColumn('afAddress', event)) {
      const currentRowData = utils.getRowData<AFWhiteListVersioned>(gridApi);

      const hasAddressError = utils.hasAddressFormatError(currentRowData);

      setAddressFormatError(hasAddressError);

      onRowDataUpdated(currentRowData, {
        hasAddressFormatError: hasAddressError,
        hasNonUniqueAFError,
      });
    }

    if (utils.isChangingColumn('afName', event)) {
      const newAccessFunctionOccurrences = utils.updateAccessFunctionOccurrences(
        accessFunctionOccurrences,
        event?.oldValue,
        event?.newValue,
      );

      setAccessFunctionOccurrences(newAccessFunctionOccurrences);

      const hasNonUniqueError = utils.hasRepeatedAccessFunctionOccurrencesError(newAccessFunctionOccurrences);
      event.context.accessFunctionOccurrences = newAccessFunctionOccurrences;

      setNonUniqueAFError(hasNonUniqueError);

      onRowDataUpdated(utils.getRowData<AFWhiteListVersioned>(gridApi), {
        hasAddressFormatError,
        hasNonUniqueAFError: hasNonUniqueError,
      });
    }
  };

  const handleRemoveIp = (params: ICellRendererParams) => {
    setSelectedIpToRemove(params);
    setDisplayMessage(`Are you sure you want to delete IP address ${params?.data?.afAddress} for AF '${params?.data?.afName}' ?`);
  };

  const handleConfirmRemoveIp = () => {
    if (!selectedIpToRemove || !gridApi) { return; }

    const newAccessFunctionOccurrences = utils.updateAccessFunctionOccurrences(
      accessFunctionOccurrences,
      selectedIpToRemove.data.afName,
      '',
    );

    selectedIpToRemove.context.accessFunctionOccurrences = newAccessFunctionOccurrences;
    setAccessFunctionOccurrences(newAccessFunctionOccurrences);

    const newRowData = utils.removeFromRowData<AFWhiteListVersioned>(selectedIpToRemove, gridApi);
    gridApi.setRowData(newRowData);

    const hasAddressError = utils.hasAddressFormatError(newRowData);
    const hasNonUniqueError = utils.hasRepeatedAccessFunctionOccurrencesError(newAccessFunctionOccurrences);
    setAddressFormatError(hasAddressError);
    setNonUniqueAFError(hasNonUniqueError);

    onRowDataUpdated(newRowData, {
      hasAddressFormatError: hasAddressError,
      hasNonUniqueAFError: hasNonUniqueError,
    });
    setSelectedIpToRemove(undefined);
    setDisplayMessage('');
  };

  const handleCancelRemoveIp = () => {
    setSelectedIpToRemove(undefined);
    setDisplayMessage('');
  };

  const handleCellKeyDown = () => {
    onCellKeyDown();
  };

  const renderAddPoiButton = (showButton: boolean) => {
    if (!showButton) {
      return null;
    }

    return (
      <div>
        <Button icon="add" minimal onClick={handleAddNewIp}>Add AF IP Address</Button>
      </div>
    );
  };

  return (
    <div>
      {renderAddPoiButton(isEditing)}
      <div className="ag-theme-balham allowed-ips-update-table allowed-ips-update0-table">
        <AgGridReact
          floatingFilter
          stopEditingWhenGridLosesFocus
          columnDefs={ipTableColumnDefs}
          rowData={rowDataVersioned}
          frameworkComponents={frameworkComponents}
          defaultColDef={defaultColDef}
          context={{
            onRemoveIp: handleRemoveIp,
            accessFunctionOccurrences,
            isDialogSubmitted,
          }}
          onCellValueChanged={handleCellValueChanged}
          onCellKeyDown={handleCellKeyDown}
          onGridReady={handleGridReady}
        />
        <ModalDialog
          isOpen={!!selectedIpToRemove}
          displayMessage={displayMessage}
          title="Delete AF Allowed IP Address"
          actionText="Yes, delete it"
          onSubmit={handleConfirmRemoveIp}
          onClose={handleCancelRemoveIp}
        />
      </div>
    </div>
  );
};

export default AllowedIPsUpdateTable;
