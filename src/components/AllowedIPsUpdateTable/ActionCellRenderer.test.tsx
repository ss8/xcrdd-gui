import React from 'react';
import {
  render,
  RenderResult,
  waitFor,
  fireEvent,
  screen,
} from '@testing-library/react';
import {
  RowNode,
  Column,
  GridApi,
  ColumnApi,
} from '@ag-grid-enterprise/all-modules';
import ActionCellRenderer from './ActionCellRenderer';

describe('<ActionCellRenderer />', () => {
  describe('when ActionCellRenderer receive a list of allowed ips', () => {
    let component: RenderResult;
    const onRemoveIpFn = jest.fn();
    const mockFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <ActionCellRenderer
          context={{
            onRemoveIp: onRemoveIpFn,
          }}
          value={null}
          valueFormatted={null}
          getValue={mockFn}
          setValue={mockFn}
          formatValue={mockFn}
          data={null}
          colDef={{}}
          node={{} as RowNode}
          column={{} as Column}
          api={{} as GridApi}
          $scope={null}
          rowIndex={0}
          columnApi={{} as ColumnApi}
          refreshCell={mockFn}
          eGridCell={{} as HTMLLIElement}
          eParentOfValue={{} as HTMLLIElement}
          addRenderedRowListener={mockFn}
        />,
      );
      await waitFor(() => component?.getByRole('button'));
    });

    it('should click cell renderer button and fire onRemoveIp prop callback', async () => {
      const actionCellRendererButton = await screen?.findByRole('button');
      fireEvent.click(actionCellRendererButton);
      expect(onRemoveIpFn).toHaveBeenCalled();
    });
  });
});
