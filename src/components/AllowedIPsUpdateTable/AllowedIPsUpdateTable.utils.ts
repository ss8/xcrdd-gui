import {
  GridApi,
  RowNode,
  ICellRendererParams,
  CellValueChangedEvent,
} from '@ag-grid-enterprise/all-modules';
import IPAddressValidator from 'shared/form/validators/IPAddressValidator';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { AFWhiteListVersioned } from '../../routes/AccessFunction/AccessFunction.types';

type AccessFunctionOccurrence = { [key: string]: number };

export const getRowData = <T>(api: GridApi): T[] => {
  const currentRowData: T[] = [];
  api.forEachNode((node: RowNode) => {
    currentRowData.push(node.data);
  });
  return currentRowData;
};

export const addNewItemToGridRowData = <T>(api: GridApi, newItem: T): T[] => {
  const newRowData: T[] = getRowData(api);

  newRowData.push(newItem);

  api.setRowData(newRowData);
  api.setFocusedCell(newRowData.length - 1, 'afName');

  api.startEditingCell({
    rowIndex: newRowData.length - 1,
    colKey: 'afName',
  });

  return newRowData;
};

export const removeFromRowData = <T>(
  selectedRow: ICellRendererParams,
  gridApi: GridApi,
): T[] => {
  selectedRow.node.setSelected(true);
  const rowDataAfterRemove: T[] = [];

  gridApi.forEachNode((node) => {
    if (selectedRow?.data?.rowKey !== node?.data?.rowKey) {
      rowDataAfterRemove.push(node.data);
    }
  });

  gridApi.setRowData(rowDataAfterRemove);
  return rowDataAfterRemove;
};

export const cellValueHasChanged = (event: CellValueChangedEvent): boolean =>
  event.newValue !== event.oldValue;

export const toVersionedRowData = (
  rowData: DistributorConfigurationListenerAfWhiteList[],
  versionPk: number,
): { currentVersionPk: number; versionedRowData: AFWhiteListVersioned[] } => {
  const newRowData = rowData.map((item) => {
    versionPk += 1;
    return {
      ...item,
      rowKey: versionPk,
    };
  });

  return {
    currentVersionPk: versionPk,
    versionedRowData: newRowData,
  };
};

export const isChangingColumn = (
  searchedColId: string,
  changingColEvent: CellValueChangedEvent,
): boolean => changingColEvent.column.getColId() === searchedColId;

export const hasRepeatedAccessFunctionOccurrencesError = (accessFunctionOccurrences: {
  [key: string]: number;
}): boolean =>
  Object.keys(accessFunctionOccurrences)
    .filter((key) => !!key)
    .some((key) => accessFunctionOccurrences[key] > 1);

export const getAccessFunctionOccurrences = (
  rowData: DistributorConfigurationListenerAfWhiteList[],
): AccessFunctionOccurrence =>
  rowData
    .map((data) => data.afName)
    .reduce(
      (previousValue, currentValue) => ({
        ...previousValue,
        [currentValue]: 1,
      }),
      {},
    );

export const updateAccessFunctionOccurrences = (
  accessFunctionOccurrences: AccessFunctionOccurrence,
  oldValue: string,
  newValue: string,
): AccessFunctionOccurrence => {
  const oldOccurrences = accessFunctionOccurrences[oldValue] || 0;
  const newOccurrences = accessFunctionOccurrences[newValue] || 0;

  return {
    ...accessFunctionOccurrences,
    [oldValue]: oldOccurrences - 1,
    [newValue]: newOccurrences + 1,
  };
};

export const hasAddressFormatError = (rowData: DistributorConfigurationListenerAfWhiteList[]): boolean => rowData
  .map((data) => IPAddressValidator.isValid(null, null, data.afAddress, null, null, null))
  .some((isValid) => !isValid);
