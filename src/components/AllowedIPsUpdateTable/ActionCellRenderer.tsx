import React, { FC } from 'react';
import { Button } from '@blueprintjs/core';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';

type Props = {
  context: {
    onRemoveIp: (params:Props) => void;
  }
} & ICellRendererParams;

const ActionCellRenderer: FC<Props> = (props: Props) => {
  const handleRemoveIp = () => {
    props.context.onRemoveIp(props);
  };

  return (
    <Button icon="remove" minimal onClick={handleRemoveIp} />
  );
};

export default ActionCellRenderer;
