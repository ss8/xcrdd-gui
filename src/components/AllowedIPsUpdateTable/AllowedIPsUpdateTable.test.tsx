import React from 'react';
import {
  render,
  RenderResult,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { distributorsIPsWhiteListEditableTableColumnDefs } from 'routes/AccessFunction/AccessFunction.types';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import AllowedIPsUpdateTable from './AllowedIPsUpdateTable';

const ALLOWED_IPS_DATA_MOCK: DistributorConfigurationListenerAfWhiteList[] = [
  {
    afName: 'afName1',
    afAddress: '111.111.111.111',
    afId: 'id1',
  },
  {
    afName: 'afName2',
    afAddress: '111.111.111.222',
    afId: 'id2',
  },
  {
    afName: 'afName3',
    afAddress: '11.11.11.11',
    afId: 'id3',
  },
];

describe('<AllowedIPsUpdateTable />', () => {
  describe('when AllowedIPsUpdateTable receive a list of allowed ips', () => {
    let component: RenderResult;
    const onAddNewIpFn = jest.fn();
    const onCellKeyDownFn = jest.fn();
    const onRowDataUpdatedFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <AllowedIPsUpdateTable
          isDialogSubmitted
          isEditing
          rowData={ALLOWED_IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          onAddNewIp={onAddNewIpFn}
          onCellKeyDown={onCellKeyDownFn}
          onRowDataUpdated={onRowDataUpdatedFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should render IPs in AllowedIPsUpdateTable passed rowData', async () => {
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[0].afName)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[1].afName)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[2].afName)).toBeVisible();

      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[0].afAddress)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[1].afAddress)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[2].afAddress)).toBeVisible();
    });

    it('should add a new row data and call add row callback function when click Add AF IP button', async () => {
      expect((await screen.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(ALLOWED_IPS_DATA_MOCK.length);

      const addPoiItemButton = (await screen.findByText('Add AF IP Address')).parentElement;

      fireEvent.click(addPoiItemButton as Element);
      expect(onAddNewIpFn).toHaveBeenCalled();
      expect((await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(ALLOWED_IPS_DATA_MOCK.length + 1);
    });
  });

  describe('when removes allowed IP', () => {
    let component: RenderResult;
    const onAddNewIpFn = jest.fn();
    const onRowDataUpdatedFn = jest.fn();
    const onCellKeyDownFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <AllowedIPsUpdateTable
          isEditing
          isDialogSubmitted
          rowData={ALLOWED_IPS_DATA_MOCK}
          ipTableColumnDefs={[...distributorsIPsWhiteListEditableTableColumnDefs].reverse()}
          onAddNewIp={onAddNewIpFn}
          onCellKeyDown={onCellKeyDownFn}
          onRowDataUpdated={onRowDataUpdatedFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should remove an list element', async () => {
      const rowsBeforeDelete = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row');
      expect(rowsBeforeDelete.length).toBe(3);

      // First remove Button
      const firstItemRemoveButton = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="actions"]')[0].querySelector('button');
      fireEvent.click(firstItemRemoveButton as Element);

      // Confirm deletion
      const confirmDeleteButton = await component.findByText('Yes, delete it');
      fireEvent.click(confirmDeleteButton as Element);
      const rowsAfterDelete = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row');
      expect(rowsAfterDelete.length).toBe(2);
    });
  });

  describe('when update list elements', () => {
    let component: RenderResult;
    const onAddNewIpFn = jest.fn();
    const onCellKeyDownFn = jest.fn();
    const onRowDataUpdatedFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <AllowedIPsUpdateTable
          isDialogSubmitted
          isEditing
          rowData={ALLOWED_IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          onAddNewIp={onAddNewIpFn}
          onCellKeyDown={onCellKeyDownFn}
          onRowDataUpdated={onRowDataUpdatedFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    describe('when updating afName', () => {
      it('should double click afName cell, type new AF name and see the new afName in the table', async () => {
        await waitFor(() => screen.getByText('afName1'));

        act(() => {
          fireEvent.doubleClick(screen.getByText('afName1'));
        });

        const inputField = screen.getByDisplayValue('afName1');
        fireEvent.change(inputField, { target: { value: 'newName' } });
        fireEvent.focusOut(inputField);

        await waitFor(() => expect(screen.getByText('newName')).toBeVisible());
      });
    });

    describe('when updating afAddress', () => {
      it('should double click afAddress cell, type new ip and see the new IP in the table', async () => {
        await waitFor(() => screen.getByText('111.111.111.111'));

        act(() => {
          fireEvent.doubleClick(screen.getByText('111.111.111.111'));
        });

        const inputField = screen.getByDisplayValue('111.111.111.111');

        act(() => {
          fireEvent.change(inputField, { target: { value: '1.2.1.2' } });
          fireEvent.focusOut(inputField);
        });

        await waitFor(() => expect(screen.getByText('1.2.1.2')).toBeVisible());
      });
    });
  });

  describe('when renders AllowedIPsUpdateTable in view mode', () => {
    let component: RenderResult;
    const onAddNewIpFn = jest.fn();
    const onCellKeyDownFn = jest.fn();
    const onRowDataUpdatedFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <AllowedIPsUpdateTable
          isDialogSubmitted
          isEditing={false}
          rowData={ALLOWED_IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          onAddNewIp={onAddNewIpFn}
          onCellKeyDown={onCellKeyDownFn}
          onRowDataUpdated={onRowDataUpdatedFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should render IPs in AllowedIPsUpdateTable passed rowData', async () => {
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[0].afName)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[1].afName)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[2].afName)).toBeVisible();

      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[0].afAddress)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[1].afAddress)).toBeVisible();
      expect(await screen?.findByText(ALLOWED_IPS_DATA_MOCK[2].afAddress)).toBeVisible();
    });

    it('should not render the Add AF IP button in view mode', async () => {
      const addPoiItemButton = (await screen.findByRole('grid')).querySelectorAll('.bp3-button.bp3-minimal');
      expect(addPoiItemButton.length).toBe(0);
    });
  });
});
