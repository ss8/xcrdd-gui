/* eslint-disable react/require-default-props */
import React, {
  forwardRef, Fragment, useEffect,
  useImperativeHandle, useRef, useState,
} from 'react';
import {
  Button, Switch, Label, Dialog, Classes,
} from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { FormTemplate } from 'data/template/template.types';
import { VpnConfigTemplate } from 'data/vpnTemplate/vpnTemplate.types';
import { FieldUpdateHandlerParam } from 'shared/form';
import { VpnConfig, VpnConfigFormData, LiInterface } from 'data/vpnConfig/vpnConfig.types';
import { VPNFormRef } from './VPNForm/VPNForm';
import { VPNTemplate } from './templates/VPNTemplate';
import VPNConfigurationPopup from './VPNConfigurationPopup';
import {
  adaptTemplateOptions,
  updateFormData,
  fillFormDataByTemplate,
} from './utils/vpnTemplate.adapter';
import { createVpnConfigBasedOnTemplate } from './utils/vpn.utils';
import './vpn-configuration-section.scss';

// eslint-disable-next-line react/require-default-props
interface Props {
  liInterface: LiInterface,
  isEditing: boolean,
  data?: VpnConfig,
  vpnConfigTemplates?: VpnConfigTemplate[],
  vpnConfigs?: VpnConfig[],
}

export type VPNConfigurationSectionRef = {
  getValues: () => VpnConfig | null
}

export const VPNConfigurationSection = forwardRef<VPNConfigurationSectionRef, Props>(({
  liInterface,
  isEditing,
  data,
  vpnConfigTemplates,
  vpnConfigs,
}: Props, ref) => {
  const vpnFormRef = useRef<VPNFormRef>(null);
  const [popupOpen, setPopupOpen] = useState<boolean>(false);
  const [confirmPopupOpen, setConfirmPopupOpen] = useState<boolean>(false);
  const [vpnConfig, setVpnConfig] = useState<Partial<VpnConfigFormData>>({});
  const [template, setTemplate] = useState<FormTemplate>();
  const vpnSubmitData = useRef<VpnConfig | null>(null);
  const [removed, setRemoved] = useState<boolean>(false);

  useEffect(() => {
    vpnSubmitData.current = cloneDeep(data) ?? null;

    if (vpnSubmitData.current == null || removed) {
      // create mode
      const initialVpnConfig = createVpnConfigBasedOnTemplate(VPNTemplate, liInterface);
      setVpnConfig(initialVpnConfig);
      const adaptedTemplate = adaptTemplateOptions(
        isEditing, VPNTemplate, initialVpnConfig, vpnConfigTemplates, vpnConfigs,
      );
      setTemplate(adaptedTemplate);
      if (vpnSubmitData.current != null && removed) {
        vpnSubmitData.current.operation = 'DELETE';
      }
    } else {
      // view or edit mode
      const clonedData = cloneDeep(vpnSubmitData.current);
      setVpnConfig(clonedData);
      const adaptedTemplate = adaptTemplateOptions(
        isEditing, VPNTemplate, clonedData, vpnConfigTemplates, vpnConfigs,
      );
      setTemplate(adaptedTemplate);
    }
  }, [data, isEditing, liInterface, removed, vpnConfigTemplates, vpnConfigs]);

  const getValues = () => {
    return vpnSubmitData.current;
  };

  useImperativeHandle(ref, () => ({
    getValues,
  }));

  const handleOpenVPNConfiguration = () => {
    setPopupOpen(true);
  };

  const handleCloseVPNConfiguration = () => {
    let clonedVpnSubmitData = cloneDeep(vpnSubmitData.current);
    if (clonedVpnSubmitData == null || clonedVpnSubmitData.operation === 'DELETE') {
      clonedVpnSubmitData = createVpnConfigBasedOnTemplate(VPNTemplate, liInterface);
    }
    setVpnConfig(clonedVpnSubmitData);
    setPopupOpen(false);
  };

  const handleEnableVPNChange = () => {
    if (vpnConfig.enabled === undefined) {
      return;
    }

    const clonedVpnConfig = cloneDeep(vpnConfig);
    clonedVpnConfig.enabled = !vpnConfig.enabled;

    const clonedVpnSubmitData = cloneDeep(vpnSubmitData.current);
    if (clonedVpnSubmitData != null) {
      clonedVpnSubmitData.enabled = clonedVpnConfig.enabled;
    }
    vpnSubmitData.current = clonedVpnSubmitData;

    setVpnConfig(clonedVpnConfig);

  };

  const onOkThenClose = () => {
    const isFormValid = vpnFormRef?.current?.isValid(true, true) ?? true;
    if (!isFormValid) {
      vpnSubmitData.current = null;
      return;
    }

    const formData = vpnFormRef?.current?.getValues();
    if (!formData) {
      vpnSubmitData.current = null;
      return;
    }
    const clonedFormData = cloneDeep(formData);
    if (!clonedFormData.id) {
      clonedFormData.operation = 'CREATE';
    } else {
      clonedFormData.operation = 'NO_OPERATION';
    }
    setVpnConfig(clonedFormData);
    vpnSubmitData.current = clonedFormData;

    setPopupOpen(false);
  };

  /**
   * Update form data and template based on GUI behavior.
   * Switch 'VPN Mode' to 'IPSec Tunnel', Tunnel Own IP and Dest IP are editable and mandatory.
   * Switch 'VPN Mode' to 'IPSec Transport', Tunnel Own IP and Dest IP are read-only and empty.
   * Switch 'IKE Version' to 'V1', IKE Mode is editable.
   * Switch 'IKE Version' to 'IPSec Tunnel', IKE Mode are read-only and set to 'Main'.
  */
  const handleFieldUpdate = (({ name: fieldName, value }: FieldUpdateHandlerParam) => {
    let filledFormData;
    if (fieldName === 'templateId') {
      filledFormData = fillFormDataByTemplate(vpnConfig, vpnConfigTemplates ?? [], value);
    } else {
      // Update data because adapted template change the template.key for some above scenario.
      filledFormData = updateFormData(vpnConfig, fieldName, value);
    }

    const adaptedTemplate = adaptTemplateOptions(
      true, VPNTemplate, filledFormData, vpnConfigTemplates, vpnConfigs,
    );

    setTemplate(adaptedTemplate);
    setVpnConfig(filledFormData);
  });

  const handleRemoveVPN = (): void => {
    setConfirmPopupOpen(true);
  };

  const confirmPopupCloseHandler = (): void => {
    setConfirmPopupOpen(false);
  };

  const confirmPopupDisableHandler = (): void => {
    vpnConfig.enabled = false;
    const clonedVpnSubmitData = cloneDeep(vpnSubmitData.current);
    if (clonedVpnSubmitData != null) {
      clonedVpnSubmitData.enabled = false;
    }
    vpnSubmitData.current = clonedVpnSubmitData;
    setConfirmPopupOpen(false);
  };

  const confirmPopupRemoveHandler = (): void => {
    if (!vpnConfig.id) {
      vpnSubmitData.current = null;
      setVpnConfig(createVpnConfigBasedOnTemplate(VPNTemplate, liInterface));
    } else {
      const clonedVpnSubmitData = cloneDeep(vpnSubmitData.current);
      if (clonedVpnSubmitData != null) {
        clonedVpnSubmitData.operation = 'DELETE';
      }
      vpnSubmitData.current = clonedVpnSubmitData;
      setRemoved(true);
    }
    setConfirmPopupOpen(false);
  };

  const renderVPNToolBarRemove = () => {
    if (isEditing) {
      return (
        <div className="next-field">
          <Button
            data-testid="vpnToolbarRemove"
            className="vpn-toolbar-remove-button"
            minimal
            intent="primary"
            onClick={handleRemoveVPN}
            text="Remove"
          />
        </div>
      );
    }
    return null;
  };

  const renderVPNDetail = () => {
    if (!vpnConfig || !vpnConfig.name || vpnConfig.operation === 'DELETE') {
      return null;
    }

    return (
      <Fragment>
        <div className="next-field">
          <Label className="first-field">VPN Name:</Label>
          <Label className="label-field-value">{vpnConfig.name}</Label>
        </div>
        <Switch
          className="next-field"
          data-testid="VPNConfigurationEnabledCheckbox"
          label="Enable VPN"
          disabled={isEditing === false}
          onChange={handleEnableVPNChange}
          checked={vpnConfig.enabled}
        />
        {renderVPNToolBarRemove()}
        <Dialog
          title={`Remove ${liInterface} VPN Configuration`}
          isOpen={confirmPopupOpen}
          className="vpn-confirm-popup"
          onClose={confirmPopupCloseHandler}
        >
          <div className={Classes.DIALOG_BODY}>
            <p>
              Are you sure you want to remove {liInterface} VPN Configuration {vpnConfig.name} ?
              <br />
              Alternatively, you can just disable it.
            </p>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button data-testid="vpnConfirmPopupCancel" minimal intent="primary" onClick={confirmPopupCloseHandler} text="Cancel" />
              <Button data-testid="vpnConfirmPopupDisable" intent="none" onClick={confirmPopupDisableHandler} text="Disable" />
              <Button data-testid="vpnConfirmPopupRemove" intent="primary" onClick={confirmPopupRemoveHandler} text="Remove" />
            </div>
          </div>
        </Dialog>
      </Fragment>
    );
  };

  const renderForm = () => {
    if (!template) {
      return null;
    }
    return (
      <Fragment>
        <VPNConfigurationPopup
          ref={vpnFormRef}
          liInterface={liInterface}
          isOpen={popupOpen}
          isEditing={isEditing}
          data={vpnConfig}
          template={template}
          onClose={handleCloseVPNConfiguration}
          onOkThenClose={onOkThenClose}
          onFieldUpdate={handleFieldUpdate}
        />
      </Fragment>
    );
  };

  return (
    <div>
      <div className="vpn-configuration-section" data-testid={`VPNConfiguration-${liInterface}`}>
        <Button className="first-field" onClick={handleOpenVPNConfiguration} intent="none" text="VPN Configuration" />
        {renderVPNDetail()}
      </div>
      {renderForm()}
    </div>
  );
});

export default VPNConfigurationSection;
