import React, { forwardRef } from 'react';
import { LiInterface, VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import { Button, Classes, Dialog } from '@blueprintjs/core';
import { FieldUpdateHandlerParam } from 'shared/form';
import { FormTemplate } from 'data/template/template.types';
import { VPNFormRef, VPNForm } from './VPNForm/VPNForm';
import { getTitleByLiInterface } from './utils/vpn.utils';
import '../FormLayout/form-layout.scss';
import './vpn-configuration-popup.scss';

type Props = {
  liInterface: LiInterface,
  isOpen: boolean,
  isEditing: boolean,
  data: Partial<VpnConfig>,
  template: FormTemplate,
  onClose: () => void,
  onOkThenClose: () => void,
  onFieldUpdate: (param: FieldUpdateHandlerParam, formSection?: string) => void
};

export const VPNConfigurationPopup = forwardRef<VPNFormRef, Props>(({
  liInterface,
  isOpen,
  isEditing,
  data,
  template,
  onClose,
  onOkThenClose,
  onFieldUpdate,
}: Props, ref) => {

  const renderButtons = () => {
    if (isEditing) {
      return (
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button data-testid="FormButtonsCancel" className="actionButton" onClick={onClose} intent="none" text="Cancel" />
          <Button data-testid="FormButtonsSave" className="actionButton" onClick={onOkThenClose} intent="primary" text="Ok" />
        </div>
      );
    }
  };

  const renderForm = () => {
    return (
      <VPNForm
        ref={ref}
        data={data}
        template={template}
        onFieldUpdate={onFieldUpdate}
      />
    );
  };

  return (
    <Dialog
      autoFocus
      title={`VPN Configuration - ${getTitleByLiInterface(liInterface, data.name ?? '')}`}
      onClose={onClose}
      isOpen={isOpen}
      className="vpn-configuration-popup"
    >
      <div className={Classes.DIALOG_BODY}>
        <div data-testid={`VPNConfigurationPopup-${data.name === '' ? liInterface : data.name}`} />
        {renderForm()}
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        {renderButtons()}
      </div>
    </Dialog>
  );
});

export default VPNConfigurationPopup;
