import { FormTemplate } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export const VPNTemplate: FormTemplate = {
  version: '1.0.0',
  name: 'VPN Configuration',
  key: 'vpnconfig',
  main: {
    metadata: {
      layout: {
        rows: [
          [
            'id',
            'mappedId',
            'copiedId',
            '$empty',
            '$empty',
          ],
          [
            'name',
            'vpnOwnIp',
            'vpnDestIp',
            'enabled',
            'templateId',
          ],
          [
            '$empty',
            '$empty',
            '$empty',
            '$empty',
            'templateDesc',
          ],
          [
            'role',
            'tunOwnIp',
            'tunDestIp',
            'keyingTries',
            '$empty',
          ],
        ],
      },
    },
    fields: {
      id: {
        type: 'text',
        hide: 'true',
      },
      mappedId: {
        type: 'text',
        hide: 'true',
      },
      copiedId: {
        type: 'text',
        hide: 'true',
      },
      name: {
        type: 'text',
        label: 'Name',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^[A-Za-z0-9\\-_\\.]{1,16}$',
              errorMessage: 'Must only include letters, digits and the following special characters: -._. Please re-enter.',
            },
            {
              type: 'length',
              length: 16,
            },
          ],
        },
      },
      vpnOwnIp: {
        label: 'VPN Own IP',
        type: 'textSuggest',
        initial: '',
        options: [],
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              invalidIPs: ['255.255.255.255'],
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
          ],
        },
      },
      vpnDestIp: {
        label: 'VPN Dest IP',
        type: 'textSuggest',
        initial: '',
        options: [],
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              invalidIPs: ['255.255.255.255'],
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
          ],
        },
      },
      enabled: {
        label: 'Enable VPN',
        type: 'checkbox',
        initial: true,
      },
      templateId: {
        label: 'VPN Template',
        type: 'select',
        options: [
          {
            label: 'Select a template',
            value: '',
          },
        ],
      },
      templateDesc: {
        type: 'custom',
        initial: 'Below data will be copied from the template',
        customComponent: 'vpnTemplateDesc',
        className: 'field-desc',
      },
      role: {
        label: 'Mode',
        type: 'select',
        initial: 'IPSECTRANS',
        options: [
          {
            label: 'IPSec Transport',
            value: 'IPSECTRANS',
          },
          {
            label: 'IPSec Tunnel',
            value: 'IPSEC',
          },
        ],
        validation: {
          required: true,
        },
      },
      tunOwnIp: {
        label: 'Tunnel Own IP',
        type: 'textSuggest',
        initial: '',
        options: [],
        readOnly: true,
        validation: {
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              invalidIPs: ['255.255.255.255'],
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
          ],
        },
      },
      tunDestIp: {
        label: 'Tunnel Dest IP',
        type: 'textSuggest',
        initial: '',
        options: [],
        readOnly: true,
        validation: {
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              invalidIPs: ['255.255.255.255'],
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
          ],
        },
      },
      keyingTries: {
        label: 'Keying Tries',
        type: 'text',
        initial: 'forever',
        validation: {
          validators: [
            {
              type: 'regexp',
              regexp: '^(forever|[0-9]{0,8})$',
              errorMessage: 'Must only include word (forever) or digits. Please re-enter.',
            },
            {
              type: 'length',
              length: 8,
            },
          ],
        },
      },
    },
  },
  ike: {
    metadata: {
      layout: {
        rows: [
          [
            'ikeVer',
            'p1Mode',
            'p1Encr',
            'p1Oakley',
            'p1LifetimeSecs',
          ],
          [
            'p1Auth',
            'key',
            '$empty',
            '$empty',
            '$empty',
          ],
        ],
      },
    },
    fields: {
      ikeVer: {
        label: 'Version',
        type: 'radio',
        inline: true,
        initial: 'IKEv1',
        options: [
          {
            label: 'V1',
            value: 'IKEv1',
          },
          {
            label: 'V2',
            value: 'IKEv2',
          },
        ],
      },
      p1Mode: {
        label: 'Mode',
        type: 'radio',
        inline: true,
        initial: 'MAIN',
        options: [
          {
            label: 'Main',
            value: 'MAIN',
          },
          {
            label: 'Aggressive',
            value: 'AGGRESSIVE',
          },
        ],
      },
      p1Encr: {
        label: 'Encryption',
        type: 'select',
        initial: 'AES(128)',
        options: [
          {
            label: 'AES(128)',
            value: 'AES(128)',
          },
          {
            label: 'AES(192)',
            value: 'AES(192)',
          },
          {
            label: 'AES(256)',
            value: 'AES(256)',
          },
          {
            label: '3DES',
            value: '3DES',
          },
          {
            label: 'AES',
            value: 'AES',
          },
          {
            label: 'DES',
            value: 'DES',
          },
          {
            label: 'DISABLED',
            value: 'DISABLED',
          },
        ],
      },
      p1Oakley: {
        label: 'Oakley',
        type: 'select',
        initial: '5',
        options: [
          {
            label: 'NA',
            value: 'NA',
          },
          {
            label: '1 (768-bit)',
            value: '1',
          },
          {
            label: '2 (1024-bit)',
            value: '2',
          },
          {
            label: '5 (1536-bit)',
            value: '5',
          },
          {
            label: '14 (2048-bit)',
            value: '14',
          },
          {
            label: '15 (3072-bit)',
            value: '15',
          },
          {
            label: '16 (4096-bit)',
            value: '16',
          },
          {
            label: '19 (256-bit ECP RFC4753)',
            value: '19',
          },
          {
            label: '20 (384-bit ECP RFC4753)',
            value: '20',
          },
          {
            label: '21 (521-bit ECP RFC4753)',
            value: '21',
          },
          {
            label: '22 (1024-bit MODP RFC5114)',
            value: '22',
          },
          {
            label: '23 (2048-bit MODP RFC5114)',
            value: '23',
          },
          {
            label: '24 (4096-bit MODP RFC5114)',
            value: '24',
          },
          {
            label: '25 (192-bit ECP RFC5114)',
            value: '25',
          },
          {
            label: '26 (224-bit ECP RFC5114)',
            value: '26',
          },
        ],
      },
      p1LifetimeSecs: {
        label: 'Lifetime (secs)',
        type: 'text',
        initial: '14400',
        validation: {
          validators: [
            {
              type: 'Interval',
              intervals: [[0, 86400]],
              errorMessage: 'Must be between 0 - 86400. Please re-enter.',
            },
          ],
        },
      },
      p1Auth: {
        label: 'Authentication',
        type: 'select',
        initial: 'MD5',
        options: [
          {
            label: 'MD5',
            value: 'MD5',
          },
          {
            label: 'SHA',
            value: 'SHA',
          },
          {
            label: 'HMAC-SHA256',
            value: 'HMAC-SHA256',
          },
          {
            label: 'HMAC-SHA384',
            value: 'HMAC-SHA384',
          },
          {
            label: 'HMAC-SHA512',
            value: 'HMAC-SHA512',
          },
          {
            label: 'DISABLED',
            value: 'DISABLED',
          },
        ],
      },
      key: {
        label: 'Key (plain text or HEX)',
        type: 'text',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 66,
            },
            {
              type: 'regexp',
              regexp: '(^0x[0-9a-fA-F]{6,66}$)|(^(?!0x)[A-Za-z0-9\\-_@#%]{8,66}$)|([\\*]+)',
              errorMessage: 'Must be between 8-66 characters. Please enter a valid hexadecimal value beginning with 0x, or a plain text where special characters allowed are -_@#%',
            },
          ],
        },
      },
    },
  },
  sa: {
    metadata: {
      layout: {
        rows: [
          [
            'encryption',
            'p2Oakley',
            'leftProtocol',
            'leftProtoPort',
            'rightProtoPort',
          ],
          [
            'authentication',
            'p2LifetimeSecs',
            'pfs',
            'rekey',
            'rekeyMarginSecs',
          ],
        ],
      },
    },
    fields: {
      encryption: {
        label: 'Encryption',
        type: 'select',
        initial: 'AES(128)',
        options: [
          {
            label: 'AES(128)',
            value: 'AES(128)',
          },
          {
            label: 'AES(192)',
            value: 'AES(192)',
          },
          {
            label: 'AES(256)',
            value: 'AES(256)',
          },
          {
            label: '3DES',
            value: '3DES',
          },
          {
            label: 'AES',
            value: 'AES',
          },
          {
            label: 'DES',
            value: 'DES',
          },
          {
            label: 'DISABLED',
            value: 'DISABLED',
          },
        ],
      },
      p2Oakley: {
        label: 'Oakley',
        type: 'select',
        initial: '5',
        options: [
          {
            label: 'NA',
            value: 'NA',
          },
          {
            label: '1 (768-bit)',
            value: '1',
          },
          {
            label: '2 (1024-bit)',
            value: '2',
          },
          {
            label: '5 (1536-bit)',
            value: '5',
          },
          {
            label: '14 (2048-bit)',
            value: '14',
          },
          {
            label: '15 (3072-bit)',
            value: '15',
          },
          {
            label: '16 (4096-bit)',
            value: '16',
          },
          {
            label: '19 (256-bit ECP RFC4753)',
            value: '19',
          },
          {
            label: '20 (384-bit ECP RFC4753)',
            value: '20',
          },
          {
            label: '21 (521-bit ECP RFC4753)',
            value: '21',
          },
          {
            label: '22 (1024-bit MODP RFC5114)',
            value: '22',
          },
          {
            label: '23 (2048-bit MODP RFC5114)',
            value: '23',
          },
          {
            label: '24 (4096-bit MODP RFC5114)',
            value: '24',
          },
          {
            label: '25 (192-bit ECP RFC5114)',
            value: '25',
          },
          {
            label: '26 (224-bit ECP RFC5114)',
            value: '26',
          },
        ],
      },
      leftProtocol: {
        label: 'Protocol Name',
        type: 'select',
        initial: 'UDP',
        options: [
          {
            label: 'TCP',
            value: 'TCP',
          },
          {
            label: 'UDP',
            value: 'UDP',
          },
        ],
      },
      leftProtoPort: {
        label: 'Left Protocol Port',
        type: 'text',
        initial: '0',
        validation: {
          validators: [
            {
              type: 'Interval',
              intervals: [[0, 65535]],
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
          ],
        },
      },
      rightProtoPort: {
        label: 'Right Protocol Port',
        type: 'text',
        initial: '0',
        validation: {
          validators: [
            {
              type: 'Interval',
              intervals: [[0, 65535]],
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
          ],
        },
      },
      authentication: {
        label: 'Authentication',
        type: 'select',
        initial: 'MD5',
        options: [
          {
            label: 'MD5',
            value: 'MD5',
          },
          {
            label: 'SHA',
            value: 'SHA',
          },
          {
            label: 'HMAC-SHA256',
            value: 'HMAC-SHA256',
          },
          {
            label: 'HMAC-SHA384',
            value: 'HMAC-SHA384',
          },
          {
            label: 'HMAC-SHA512',
            value: 'HMAC-SHA512',
          },
          {
            label: 'DISABLED',
            value: 'DISABLED',
          },
        ],
      },
      p2LifetimeSecs: {
        label: 'Lifetime (secs)',
        type: 'text',
        initial: '3600',
        validation: {
          validators: [
            {
              type: 'Interval',
              intervals: [[0, 86400]],
              errorMessage: 'Must be between 0 - 86400. Please re-enter.',
            },
          ],
        },
      },
      pfs: {
        label: 'PFS',
        type: 'checkbox',
        initial: true,
      },
      rekey: {
        label: 'Rekey',
        type: 'checkbox',
        initial: true,
      },
      rekeyMarginSecs: {
        label: 'ReKey Margin (secs)',
        type: 'text',
        initial: '540',
        validation: {
          validators: [
            {
              type: 'Interval',
              intervals: [[0, 86400]],
              errorMessage: 'Must be between 0 - 86400. Please re-enter.',
            },
          ],
        },
      },
    },
  },
};
