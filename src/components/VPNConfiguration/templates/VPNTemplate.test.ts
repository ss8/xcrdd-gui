import { FormTemplateSection } from 'data/template/template.types';
import cloneDeep from 'lodash.clonedeep';
import { VPNTemplate } from './VPNTemplate';

describe('Validate AF template formats', () => {
  const template = cloneDeep(VPNTemplate);
  function getAllFields(rows:string[][]): string[] {
    if (!rows) {
      return [];
    }
    const fields:string[] = [];
    rows.forEach((row:string[]) => {
      row.forEach((fieldName:string) => {
        if (fieldName !== '$empty') {
          fields.push(fieldName);
        }
      });
    });
    return fields;
  }

  it('the fields used in layout.rows should be defined in the fields section', () => {
    const sections = Object.keys(template);
    sections.forEach((section:string) => {
      if (typeof template[section] === 'string') return;
      const templateSection = (template[section] as FormTemplateSection);
      const fieldsInRows = [];
      fieldsInRows.push(...getAllFields(templateSection?.metadata?.layout?.rows || []));
      fieldsInRows.push(...getAllFields(templateSection?.metadata?.layout?.collapsible?.rows || []));
      expect(templateSection?.fields).toBeDefined();
      const definedFields = Object.keys(templateSection?.fields);
      definedFields.push('$empty'); // there could be $empty in rows, so add $empty as a defined field.
      // include afTag in the expect and received so when there is an error we know which template has a problem
      expect(
        {
          afTag: template.key,
          templateSection: section,
          definedFields,
        },
      ).toEqual(
        {
          afTag: template.key,
          templateSection: section,
          definedFields: expect.arrayContaining(fieldsInRows),
        },
      );
    });
  });
});
