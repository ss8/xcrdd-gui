import React from 'react';
import {
  fireEvent,
  render,
  screen,
} from '@testing-library/react';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import VPNConfigurationPopup from './VPNConfigurationPopup';
import { VPNTemplate } from './templates/VPNTemplate';

describe('VPNConfigurationPopup', () => {
  const liInterface = 'x2';
  const title = 'VPN Configuration - Delivery Interface (X2)';
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    vpnConfig = {
      id: '',
      name: '',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '12.12.12.12',
      vpnDestIp: '13.13.13.13',
      tunOwnIp: '14.14.14.14',
      tunDestIp: '15.15.15.15',
      p1Encr: 'AES(256)',
      p1Auth: 'HMAC-SHA256',
      p1Oakley: '14',
      encryption: 'AES(192)',
      authentication: 'MD5',
      p2Oakley: '1',
      p2LifetimeSecs: '23',
      key: '21321312',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '9494',
      rightProtoPort: '9494',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: false,
      rekeyMarginSecs: '23433',
      rekey: false,
      p1LifetimeSecs: '24343',
      p1Mode: 'AGGRESSIVE',
    };
  });

  it('should not display buttons for view mode', async () => {
    const isEditing = false;
    render(
      <VPNConfigurationPopup
        liInterface={liInterface}
        isOpen
        data={vpnConfig}
        isEditing={isEditing}
        template={VPNTemplate}
        onClose={jest.fn()}
        onOkThenClose={jest.fn()}
        onFieldUpdate={jest.fn()}
      />,
    );

    expect(screen.getByText(title)).toBeVisible();
    expect(screen.queryAllByText('Ok')).toHaveLength(0);
    expect(screen.queryAllByText('Cancel')).toHaveLength(0);
  });

  it('should display buttons for create/edit mode', async () => {
    const isEditing = true;
    render(
      <VPNConfigurationPopup
        liInterface={liInterface}
        isOpen
        data={vpnConfig}
        isEditing={isEditing}
        template={VPNTemplate}
        onClose={jest.fn()}
        onOkThenClose={jest.fn()}
        onFieldUpdate={jest.fn()}
      />,
    );

    expect(screen.getByText(title)).toBeVisible();
    expect(screen.queryAllByText('Ok')).toHaveLength(1);
    expect(screen.queryAllByText('Cancel')).toHaveLength(1);
  });

  it('should call the button callback when clicking on them if in edit mode', () => {
    const isEditing = true;
    const closeHandler = jest.fn();
    const okHandler = jest.fn();
    render(
      <VPNConfigurationPopup
        liInterface={liInterface}
        isOpen
        data={vpnConfig}
        isEditing={isEditing}
        template={VPNTemplate}
        onClose={closeHandler}
        onOkThenClose={okHandler}
        onFieldUpdate={jest.fn()}
      />,
    );

    fireEvent.click(screen.queryAllByText('Ok')[0]);
    expect(okHandler).toBeCalled();

    fireEvent.click(screen.queryAllByText('Cancel')[0]);
    expect(closeHandler).toBeCalled();
  });

  it('should call onFieldUpdate if a field is changed in edit mode', () => {
    const isEditing = true;
    const updateHandler = jest.fn();
    render(
      <VPNConfigurationPopup
        liInterface={liInterface}
        isOpen
        data={vpnConfig}
        isEditing={isEditing}
        template={VPNTemplate}
        onClose={jest.fn()}
        onOkThenClose={jest.fn()}
        onFieldUpdate={updateHandler}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'vpn-name-1' } });
    expect(updateHandler).toBeCalledWith({
      label: 'Name',
      name: 'name',
      value: 'vpn-name-1',
      valueLabel: 'vpn-name-1',
    });
  });

  it('should display fields for edit mode', async () => {
    const isEditing = true;
    render(
      <VPNConfigurationPopup
        liInterface={liInterface}
        isOpen
        data={vpnConfig}
        isEditing={isEditing}
        template={VPNTemplate}
        onClose={jest.fn()}
        onOkThenClose={jest.fn()}
        onFieldUpdate={jest.fn()}
      />,
    );
    expect(screen.getByTestId('VPNMain')).toBeVisible();
    expect(screen.getByTestId('VPNIKEParameters')).toBeVisible();
    expect(screen.getByTestId('VPNSAParameters')).toBeVisible();

    expect(screen.getByText('VPN Own IP:')).toBeVisible();
    expect(screen.getByText('VPN Dest IP:')).toBeVisible();
    expect(screen.getByText('Tunnel Own IP:')).toBeVisible();
    expect(screen.getByText('Tunnel Dest IP:')).toBeVisible();
    expect(screen.getByText('Version:')).toBeVisible();
    expect(screen.getByText('Protocol Name:')).toBeVisible();
  });

});
