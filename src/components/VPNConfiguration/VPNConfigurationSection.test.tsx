import React from 'react';
import {
  render,
  screen,
  fireEvent,
} from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import VPNConfigurationSection from './VPNConfigurationSection';

describe('VPNConfigurationSection', () => {
  const liInterface = 'x2';
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    vpnConfig = {
      id: '1',
      name: 'a-vpn-name',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '12.12.12.12',
      vpnDestIp: '13.13.13.13',
      tunOwnIp: '14.14.14.14',
      tunDestIp: '15.15.15.15',
      p1Encr: 'AES(256)',
      p1Auth: 'HMAC-SHA256',
      p1Oakley: '14',
      encryption: 'AES(192)',
      authentication: 'MD5',
      p2Oakley: '1',
      p2LifetimeSecs: '23',
      key: '21321312',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '9494',
      rightProtoPort: '9494',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: false,
      rekeyMarginSecs: '23433',
      rekey: false,
      p1LifetimeSecs: '24343',
      p1Mode: 'AGGRESSIVE',
    };
  });

  it('should display VPN Configuration Popup when button is clicked', async () => {
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        data={vpnConfig}
        isEditing
      />,
    );
    fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
    expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();
  });

  it('should display VPN Name when there is a VpnConfig ', () => {
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        isEditing
        data={vpnConfig}
      />,
    );
    expect(screen.getByText('VPN Name:')).toBeVisible();
    expect(screen.getByText(`${vpnConfig.name}`)).toBeVisible();
  });

  it('should display not VPN Name when there is not a VpnConfig ', () => {
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        isEditing
      />,
    );
    expect(screen.queryByText('VPN Name:')).toBeNull();
    expect(screen.queryByText(`${vpnConfig.name}`)).toBeNull();

  });

  it('should enable switch if VpnConfig is enabled', () => {
    vpnConfig.enabled = true;
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        isEditing
        data={vpnConfig}
      />,
    );
    expect(screen.getByTestId('VPNConfigurationEnabledCheckbox')).toBeChecked();
  });

  it('should disable switch if VpnConfig is not enabled', () => {
    vpnConfig.enabled = false;
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        isEditing
        data={vpnConfig}
      />,
    );
    expect(screen.getByTestId('VPNConfigurationEnabledCheckbox')).not.toBeChecked();
  });

  describe('when user changes the Authentication Key value', () => {

    it('should display required validation message ', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: '' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(screen?.getByText('Please enter a value.')).toBeVisible();
    });

    it('should display validation message when the informed value is na invalid hexadecimal ', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: '0x12TTT35' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(screen?.getByText('Must be between 8-66 characters. Please enter a valid hexadecimal value beginning with 0x, or a plain text where special characters allowed are -_@#%')).toBeVisible();
    });

    it('should display validation message when the informed value is an invalid plan text ', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: 'o1o&' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(screen?.getByText('Must be between 8-66 characters. Please enter a valid hexadecimal value beginning with 0x, or a plain text where special characters allowed are -_@#%')).toBeVisible();
    });

    it('should not show validation message when the informed value is valid hexadecimal', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: '0xafafaf' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(document.querySelectorAll('.field-error-message')).toHaveLength(0);
    });

    it('should not show validation message when the informed value is valid plan text', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: '12345678' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(document.querySelectorAll('.field-error-message')).toHaveLength(0);
    });

    it('should show validation message when the informed value length is less than 8.', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
      expect(screen?.getByTestId(`VPNConfigurationPopup-${vpnConfig.name}`)).toBeVisible();

      fireEvent.change(screen.getByLabelText('Key (plain text or HEX):*'), { target: { value: '1234567' } });
      fireEvent.click(screen.getByText('Ok'));

      expect(document.querySelectorAll('.field-error-message')).toHaveLength(1);
      expect(screen?.getByText('Must be between 8-66 characters. Please enter a valid hexadecimal value beginning with 0x, or a plain text where special characters allowed are -_@#%')).toBeVisible();
    });
  });

  describe('when user click Remove on VPN toolbar', () => {

    it('should display Confirm Popup with three options when vpn is configured.', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing
        />,
      );

      expect(screen?.getByTestId('vpnToolbarRemove')).toBeVisible();
      fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
      expect(screen.getByTestId('vpnConfirmPopupCancel')).toBeVisible();
      expect(screen.getByTestId('vpnConfirmPopupDisable')).toBeVisible();
      expect(screen.getByTestId('vpnConfirmPopupRemove')).toBeVisible();
    });

    it('should not display Remove on VPN toolbar when it is view mode.', () => {
      const viewMode = true;
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          data={vpnConfig}
          isEditing={!viewMode}
        />,
      );

      expect(screen.queryAllByTestId('vpnToolbarRemove')).toHaveLength(0);
    });

    it('should not display Confirm Popup with three options when vpn is not configured.', () => {
      render(
        <VPNConfigurationSection
          liInterface={liInterface}
          isEditing
        />,
      );

      expect(screen.queryAllByTestId('vpnToolbarRemove')).toHaveLength(0);
      expect(screen.queryByTestId('vpnConfirmPopupCancel')).toBeNull();
      expect(screen.queryByTestId('vpnConfirmPopupDisable')).toBeNull();
      expect(screen.queryByTestId('vpnConfirmPopupRemove')).toBeNull();
    });
  });

  it('should set operation as DELETE when click Remove button on VPN Remove-Confirm popup', () => {
    let ref: any;
    const clonedData = cloneDeep(vpnConfig);
    clonedData.operation = 'NO_OPERATION';
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        ref={(instance) => { ref = instance; }}
        data={clonedData}
        isEditing
      />,
    );

    fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
    fireEvent.click(screen?.getByTestId('vpnConfirmPopupRemove'));

    expect(ref?.getValues()).toEqual({
      ...clonedData,
      operation: 'DELETE',
    });
  });

  it('should set enabled as false when click Disable button on VPN Remove-Confirm popup', () => {
    let ref: any;
    const clonedData = cloneDeep(vpnConfig);
    clonedData.enabled = true;
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        ref={(instance) => { ref = instance; }}
        data={clonedData}
        isEditing
      />,
    );

    fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
    fireEvent.click(screen?.getByTestId('vpnConfirmPopupDisable'));

    expect(ref?.getValues()).toEqual({
      ...clonedData,
      enabled: false,
    });
  });

  it('should be no change when click Cancel button on VPN Remove-Confirm popup', () => {
    let ref: any;
    const clonedData = cloneDeep(vpnConfig);
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        ref={(instance) => { ref = instance; }}
        data={clonedData}
        isEditing
      />,
    );

    fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
    fireEvent.click(screen?.getByTestId('vpnConfirmPopupCancel'));

    expect(ref?.getValues()).toEqual({
      ...clonedData,
    });
  });

  it('should reset VPN form on VPN configuration popup when click Remove button on VPN Remove-Confirm popup', () => {
    let ref: any;
    const clonedData = cloneDeep(vpnConfig);
    clonedData.operation = 'NO_OPERATION';
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        ref={(instance) => { ref = instance; }}
        data={clonedData}
        isEditing
      />,
    );

    fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
    fireEvent.click(screen?.getByTestId('vpnConfirmPopupRemove'));

    fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
    expect(screen.getByLabelText('Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Key (plain text or HEX):*')).toHaveValue('');
  });

  /**
   * Steps:
   * Open an AF with an existent vpn
   * Remove the VPN
   * Open the popup to add the vpn and click cancel
   * Open the popup to add the vpn again, the removed data should not be displayed
   */
  it('should reset VPN form on VPN configuration popup for the comment of corner case', () => {
    let ref: any;
    const clonedData = cloneDeep(vpnConfig);
    clonedData.operation = 'NO_OPERATION';
    render(
      <VPNConfigurationSection
        liInterface={liInterface}
        ref={(instance) => { ref = instance; }}
        data={clonedData}
        isEditing
      />,
    );

    fireEvent.click(screen?.getByTestId('vpnToolbarRemove'));
    fireEvent.click(screen?.getByTestId('vpnConfirmPopupRemove'));

    fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
    expect(screen.getByLabelText('Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Key (plain text or HEX):*')).toHaveValue('');
    fireEvent.click(screen.queryAllByText('Cancel')[0]);
    fireEvent.click(screen.getAllByText('VPN Configuration')[0]);
    expect(screen.getByLabelText('Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Key (plain text or HEX):*')).toHaveValue('');
  });

});
