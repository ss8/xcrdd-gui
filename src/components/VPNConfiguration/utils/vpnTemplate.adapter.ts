import {
  FormTemplate,
  FormTemplateOption,
  FormTemplateSection,
} from 'data/template/template.types';
import { VpnConfig, VpnConfigFormData } from 'data/vpnConfig/vpnConfig.types';
import { VpnConfigTemplate } from 'data/vpnTemplate/vpnTemplate.types';
import cloneDeep from 'lodash.clonedeep';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';

export function adaptTemplateOptions(
  isEditing: boolean,
  template: FormTemplate,
  data: Partial<VpnConfig>,
  vpnConfigTemplates?: VpnConfigTemplate[],
  vpnConfigs?: VpnConfig[],
): FormTemplate {
  const templateClone = cloneDeep(template);
  if (!isEditing) {
    adaptTemplateForViewMode(templateClone);
  } else {
    adaptTemplateForVpnConfigTemplates(templateClone, vpnConfigTemplates ?? []);
    adaptTemplateForIPSuggest(templateClone, vpnConfigs ?? []);
    adaptTemplateForVPNMode(templateClone, data.role !== 'IPSEC');
    adaptTemplateForIKEVersion(templateClone, data.ikeVer === 'IKEv2');
  }

  return templateClone;
}

export function adaptToForm(vpnConfig: VpnConfig): VpnConfigFormData {
  const clonedVpnConfig = cloneDeep(vpnConfig);

  const vpnConfigForm: VpnConfigFormData = {
    ...clonedVpnConfig,
  };

  return omitBy<VpnConfigFormData>(vpnConfigForm, isNil) as VpnConfigFormData;
}

export function updateFormData(
  data: Partial<VpnConfig>,
  fieldName: string,
  value: any,
): Partial<VpnConfig> {
  let clonedData = cloneDeep(data);
  clonedData = {
    ...clonedData,
    [fieldName]: value,
  };
  if (fieldName === 'role') {
    clonedData.tunDestIp = '';
    clonedData.tunOwnIp = '';
  } else if (fieldName === 'ikeVer') {
    clonedData.p1Mode = 'MAIN';
  }

  return clonedData;
}

export function fillFormDataByTemplate(
  data: Partial<VpnConfig>,
  vpnConfigTemplates: VpnConfigTemplate[],
  value: string,
): Partial<VpnConfig> {
  const selectedTemplate = vpnConfigTemplates.find((entry) => (entry.id === value));
  let clonedData = cloneDeep(data);
  if (selectedTemplate != null) {
    const {
      id, name, afType, ...replacedFields
    } = selectedTemplate;

    clonedData = {
      ...clonedData,
      ...replacedFields,
    };
  }

  clonedData.templateId = value;

  return clonedData;
}

function adaptTemplateForVpnConfigTemplates(template: FormTemplate, vpnConfigTemplates: VpnConfigTemplate[]): void {
  const vpnTemplateOptions: FormTemplateOption[] = [];
  vpnConfigTemplates.forEach((entry) => {
    vpnTemplateOptions.push({
      label: entry.name,
      value: entry.id,
    });
  });
  const oldLength = (template.main as FormTemplateSection).fields.templateId.options?.length ?? 0;
  const spliceLength = oldLength > 1 ? oldLength - 1 : 0;
  (template.main as FormTemplateSection).fields.templateId.options?.splice(1, spliceLength, ...vpnTemplateOptions);
}

function adaptTemplateForIPSuggest(template: FormTemplate, vpnConfigs: VpnConfig[]): void {
  const vpnOwnIPOptions: {label:string, value:string}[] = [];
  const tunOwnIPOptions: {label:string, value:string}[] = [];
  const tunDestIPOptions: {label:string, value:string}[] = [];
  vpnConfigs.forEach((entry) => {
    if (entry.vpnOwnIp !== '0.0.0.0' && entry.vpnOwnIp !== '' &&
      !vpnOwnIPOptions.find((val) => (val.value === entry.vpnOwnIp))) {
      vpnOwnIPOptions.push({
        label: entry.vpnOwnIp,
        value: entry.vpnOwnIp,
      });
    }
    if (entry.tunOwnIp !== '0.0.0.0' && entry.tunOwnIp !== '' &&
      !tunOwnIPOptions.find((val) => (val.value === entry.tunOwnIp))) {
      tunOwnIPOptions.push({
        label: entry.tunOwnIp,
        value: entry.tunOwnIp,
      });
    }
    if (entry.tunDestIp !== '0.0.0.0' && entry.tunDestIp !== '' &&
      !tunDestIPOptions.find((val) => (val.value === entry.tunDestIp))) {
      tunDestIPOptions.push({
        label: entry.tunDestIp,
        value: entry.tunDestIp,
      });
    }
  });
  (template.main as FormTemplateSection).fields.vpnOwnIp.options = vpnOwnIPOptions;
  (template.main as FormTemplateSection).fields.tunOwnIp.options = tunOwnIPOptions;
  (template.main as FormTemplateSection).fields.tunDestIp.options = tunDestIPOptions;
}

function adaptTemplateForViewMode(template: FormTemplate): void {
  Object.entries(template).forEach(([_key, templateSection]) => {
    const { fields } = (templateSection as FormTemplateSection);
    if (fields != null) {
      Object.values(fields).forEach((field) => {
        field.readOnly = true;
      });
    }
  });
}

export function adaptTemplateForVPNMode(template: FormTemplate, isNotTunnel: boolean): void {
  Object.entries(template).forEach(([_key, templateSection]) => {
    const { fields } = (templateSection as FormTemplateSection);
    if (fields != null) {
      Object.entries(fields).forEach(([fieldKey, fieldEntry]) => {
        if (fieldKey === 'tunOwnIp' || fieldKey === 'tunDestIp') {
          fieldEntry.readOnly = isNotTunnel;
          fieldEntry.validation = {
            ...fieldEntry.validation, required: !isNotTunnel,
          };
        }
      });
    }
  });
}

export function adaptTemplateForIKEVersion(template: FormTemplate, isV2: boolean): void {
  (template.ike as FormTemplateSection).fields.p1Mode.readOnly = isV2;
}
