import { FormTemplateSection, FormTemplate } from 'data/template/template.types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import cloneDeep from 'lodash.clonedeep';
import { mockVpnConfigs } from '__test__/mockVpnConfigs';
import { mockVpnTemplates } from '__test__/mockVpnTemplates';
import { VPNTemplate } from '../templates/VPNTemplate';
import {
  adaptTemplateOptions,
  fillFormDataByTemplate,
  updateFormData,
} from './vpnTemplate.adapter';

describe('adaptTemplateForViewMode', () => {
  let template: FormTemplate;
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    template = cloneDeep(VPNTemplate);
    vpnConfig = {
      id: '',
      name: '',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '',
      enabled: false,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    };
  });

  it('All fields should be readonly', () => {
    const expectedTemplate = adaptTemplateOptions(false, template, vpnConfig);
    expect(expectedTemplate).toEqual({
      ...expectedTemplate,
      main: {
        ...(template.main as FormTemplateSection),
        fields: {
          ...(template.main as FormTemplateSection).fields,
          id: {
            ...(template.main as FormTemplateSection).fields.id,
            readOnly: true,
          },
          mappedId: {
            ...(template.main as FormTemplateSection).fields.mappedId,
            readOnly: true,
          },
          copiedId: {
            ...(template.main as FormTemplateSection).fields.copiedId,
            readOnly: true,
          },
          name: {
            ...(template.main as FormTemplateSection).fields.name,
            readOnly: true,
          },
          vpnOwnIp: {
            ...(template.main as FormTemplateSection).fields.vpnOwnIp,
            readOnly: true,
          },
          vpnDestIp: {
            ...(template.main as FormTemplateSection).fields.vpnDestIp,
            readOnly: true,
          },
          enabled: {
            ...(template.main as FormTemplateSection).fields.enabled,
            readOnly: true,
          },
          templateId: {
            ...(template.main as FormTemplateSection).fields.templateId,
            readOnly: true,
          },
          templateDesc: {
            ...(template.main as FormTemplateSection).fields.templateDesc,
            readOnly: true,
          },
          role: {
            ...(template.main as FormTemplateSection).fields.role,
            readOnly: true,
          },
          tunOwnIp: {
            ...(template.main as FormTemplateSection).fields.tunOwnIp,
            readOnly: true,
          },
          tunDestIp: {
            ...(template.main as FormTemplateSection).fields.tunDestIp,
            readOnly: true,
          },
          keyingTries: {
            ...(template.main as FormTemplateSection).fields.keyingTries,
            readOnly: true,
          },
        },
      },
      ike: {
        ...(template.ike as FormTemplateSection),
        fields: {
          ...(template.ike as FormTemplateSection).fields,
          ikeVer: {
            ...(template.ike as FormTemplateSection).fields.ikeVer,
            readOnly: true,
          },
          p1Mode: {
            ...(template.ike as FormTemplateSection).fields.p1Mode,
            readOnly: true,
          },
          p1Encr: {
            ...(template.ike as FormTemplateSection).fields.p1Encr,
            readOnly: true,
          },
          p1Oakley: {
            ...(template.ike as FormTemplateSection).fields.p1Oakley,
            readOnly: true,
          },
          p1LifetimeSecs: {
            ...(template.ike as FormTemplateSection).fields.p1LifetimeSecs,
            readOnly: true,
          },
          p1Auth: {
            ...(template.ike as FormTemplateSection).fields.p1Auth,
            readOnly: true,
          },
          key: {
            ...(template.ike as FormTemplateSection).fields.key,
            readOnly: true,
          },
        },
      },
      sa: {
        ...(template.sa as FormTemplateSection),
        fields: {
          ...(template.sa as FormTemplateSection).fields,
          encryption: {
            ...(template.sa as FormTemplateSection).fields.encryption,
            readOnly: true,
          },
          p2Oakley: {
            ...(template.sa as FormTemplateSection).fields.p2Oakley,
            readOnly: true,
          },
          leftProtocol: {
            ...(template.sa as FormTemplateSection).fields.leftProtocol,
            readOnly: true,
          },
          leftProtoPort: {
            ...(template.sa as FormTemplateSection).fields.leftProtoPort,
            readOnly: true,
          },
          rightProtoPort: {
            ...(template.sa as FormTemplateSection).fields.rightProtoPort,
            readOnly: true,
          },
          authentication: {
            ...(template.sa as FormTemplateSection).fields.authentication,
            readOnly: true,
          },
          p2LifetimeSecs: {
            ...(template.sa as FormTemplateSection).fields.p2LifetimeSecs,
            readOnly: true,
          },
          pfs: {
            ...(template.sa as FormTemplateSection).fields.pfs,
            readOnly: true,
          },
          rekey: {
            ...(template.sa as FormTemplateSection).fields.rekey,
            readOnly: true,
          },
          rekeyMarginSecs: {
            ...(template.sa as FormTemplateSection).fields.rekeyMarginSecs,
            readOnly: true,
          },
        },
      },
    });
  });
});

describe('fillFormDataByTemplate', () => {
  const vpnConfig: VpnConfig = {
    id: '1',
    name: 'test',
    mappedId: '2',
    enabled: true,
    vpnOwnIp: '',
    vpnDestIp: '',
    tunOwnIp: '',
    tunDestIp: '',
    role: 'IPSEC',
    p1Encr: 'AES',
    p1Auth: 'MD5',
    p1Oakley: '5',
    encryption: 'AES',
    authentication: 'MD5',
    p2Oakley: '5',
    p2LifetimeSecs: '3600',
    key: '',
    ikeVer: 'IKEv1',
    keyingTries: 'forever',
    leftProtoPort: '0',
    rightProtoPort: '0',
    leftProtocol: 'UDP',
    rightProtocol: 'UDP',
    pfs: true,
    rekeyMarginSecs: '540',
    rekey: true,
    p1LifetimeSecs: '14400',
    p1Mode: 'MAIN',
  };

  it('check vpn form data are copied from the selected template', () => {
    const formData = cloneDeep(vpnConfig);
    formData.key = '12345678';
    formData.leftProtoPort = '1234';
    expect(fillFormDataByTemplate(formData, mockVpnTemplates, 'ZnRwMQ')).toEqual({ ...vpnConfig, templateId: 'ZnRwMQ' });
  });
});

describe('adaptTemplateForVpnConfigTemplates', () => {
  const vpnConfig: VpnConfig = {
    id: '1',
    name: 'test',
    mappedId: '2',
    enabled: true,
    vpnOwnIp: '',
    vpnDestIp: '',
    tunOwnIp: '',
    tunDestIp: '',
    role: 'IPSEC',
    p1Encr: 'AES',
    p1Auth: 'MD5',
    p1Oakley: '5',
    encryption: 'AES',
    authentication: 'MD5',
    p2Oakley: '5',
    p2LifetimeSecs: '3600',
    key: '',
    ikeVer: 'IKEv1',
    keyingTries: 'forever',
    leftProtoPort: '0',
    rightProtoPort: '0',
    leftProtocol: 'UDP',
    rightProtocol: 'UDP',
    pfs: true,
    rekeyMarginSecs: '540',
    rekey: true,
    p1LifetimeSecs: '14400',
    p1Mode: 'MAIN',
  };

  it('check vpn template optioins show correct value list', () => {
    const expectedTemplate = adaptTemplateOptions(true, VPNTemplate, vpnConfig, mockVpnTemplates, []);
    expect((expectedTemplate.main as FormTemplateSection).fields.templateId.options).toEqual([
      {
        label: 'Select a template',
        value: '',
      },
      {
        label: 'vpn-x1',
        value: 'ZnRwMQ',
      },
      {
        label: 'vpn-data',
        value: 'ZnRwMg',
      },
    ]);
  });
});

describe('adaptTemplateForIPSuggest', () => {
  const vpnConfig: VpnConfig = {
    id: '1',
    name: 'test',
    mappedId: '2',
    enabled: true,
    vpnOwnIp: '',
    vpnDestIp: '',
    tunOwnIp: '',
    tunDestIp: '',
    role: 'IPSEC',
    p1Encr: 'AES',
    p1Auth: 'MD5',
    p1Oakley: '5',
    encryption: 'AES',
    authentication: 'MD5',
    p2Oakley: '5',
    p2LifetimeSecs: '3600',
    key: '',
    ikeVer: 'IKEv1',
    keyingTries: 'forever',
    leftProtoPort: '0',
    rightProtoPort: '0',
    leftProtocol: 'UDP',
    rightProtocol: 'UDP',
    pfs: true,
    rekeyMarginSecs: '540',
    rekey: true,
    p1LifetimeSecs: '14400',
    p1Mode: 'MAIN',
  };

  it('check VPN Own IP Suggest options show correct value list', () => {
    const expectedTemplate = adaptTemplateOptions(true, VPNTemplate, vpnConfig, [], mockVpnConfigs);
    expect((expectedTemplate.main as FormTemplateSection).fields.vpnOwnIp.options).toEqual([
      {
        label: '1.1.1.1',
        value: '1.1.1.1',
      },
    ]);
  });

  it('check Tunnel Own IP Suggest options show correct value list', () => {
    const expectedTemplate = adaptTemplateOptions(true, VPNTemplate, vpnConfig, [], mockVpnConfigs);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunOwnIp.options).toEqual([
      {
        label: '1.1.1.2',
        value: '1.1.1.2',
      },
    ]);
  });

  it('check Tunnel Dest IP Suggest options show correct value list', () => {
    const expectedTemplate = adaptTemplateOptions(true, VPNTemplate, vpnConfig, [], mockVpnConfigs);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunDestIp.options).toEqual([
      {
        label: '1.1.1.3',
        value: '1.1.1.3',
      },
    ]);
  });
});

describe('updateFormData', () => {
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    vpnConfig = {
      id: '1',
      name: 'test',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    };
  });

  it('p1Mode set as "MAIN" when switch ikeVer to V2', () => {
    const clonedData = cloneDeep(vpnConfig);
    expect(updateFormData(clonedData, 'ikeVer', 'IKEv2').p1Mode).toEqual('MAIN');
  });

  it('Tunnel IP set as empty when switch VPN Mode to IPSECTRANS', () => {
    const clonedData = cloneDeep(vpnConfig);
    expect(updateFormData(clonedData, 'role', 'IPSECTRANS').tunDestIp).toEqual('');
    expect(updateFormData(clonedData, 'role', 'IPSECTRANS').tunOwnIp).toEqual('');
  });
});

describe('adaptTemplateForVPNMode', () => {
  let template: FormTemplate;
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    template = cloneDeep(VPNTemplate);
    vpnConfig = {
      id: '',
      name: '',
      mappedId: '2',
      role: 'IPSECTRANS',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    };
  });
  it('Tunnel IP should be readonly and not mandatory when Mode is IPSec Tunnel.', () => {
    const expectedTemplate = adaptTemplateOptions(true, template, vpnConfig, []);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunDestIp.readOnly).toEqual(true);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunOwnIp.readOnly).toEqual(true);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunDestIp.validation?.required).toEqual(false);
    expect((expectedTemplate.main as FormTemplateSection).fields.tunOwnIp.validation?.required).toEqual(false);
  });
});

describe('adaptTemplateForIKEVersion', () => {
  let template: FormTemplate;
  let vpnConfig: VpnConfig;
  beforeEach(() => {
    template = cloneDeep(VPNTemplate);
    vpnConfig = {
      id: '',
      name: '',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv2',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    };
  });
  it('IKE Mode should be readonly when IKE version is V2', () => {
    const expectedTemplate = adaptTemplateOptions(true, template, vpnConfig, []);
    expect((expectedTemplate.ike as FormTemplateSection).fields.p1Mode.readOnly).toEqual(true);
  });
});
