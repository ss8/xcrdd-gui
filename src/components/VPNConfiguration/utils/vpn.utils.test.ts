import { FormTemplate } from 'data/template/template.types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import cloneDeep from 'lodash.clonedeep';
import { VPNTemplate } from '../templates/VPNTemplate';
import {
  createVpnConfigBasedOnTemplate,
  getTitleByLiInterface,
  populateLiInterfaceId,
  populateMappedId,
} from './vpn.utils';

describe('vpn.factory', () => {
  let template: FormTemplate;
  beforeEach(() => {
    template = cloneDeep(VPNTemplate);
  });

  it('createVpnConfigBasedOnTemplate on provisioningInterfaces', () => {
    const vpnConfig: VpnConfig = createVpnConfigBasedOnTemplate(template, 'x1');
    expect(vpnConfig).toEqual({
      name: vpnConfig.name,
      mappedId: '1',
      role: 'IPSECTRANS',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES(128)',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES(128)',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    });
  });

  it('createVpnConfigBasedOnTemplate on dataInterfaces', () => {
    const vpnConfig: VpnConfig = createVpnConfigBasedOnTemplate(template, 'x2');
    expect(vpnConfig).toEqual({
      name: vpnConfig.name,
      mappedId: '2',
      role: 'IPSECTRANS',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES(128)',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES(128)',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    });
  });

  it('createVpnConfigBasedOnTemplate on contentInterfaces', () => {
    const vpnConfig: VpnConfig = createVpnConfigBasedOnTemplate(template, 'x3');
    expect(vpnConfig).toEqual({
      name: vpnConfig.name,
      mappedId: '3',
      role: 'IPSECTRANS',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES(128)',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES(128)',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'MAIN',
    });
  });

  it('populateMappedId', () => {
    expect(populateMappedId('provisioningInterfaces')).toEqual('1');
    expect(populateMappedId('dataInterfaces')).toEqual('2');
    expect(populateMappedId('contentInterfaces')).toEqual('3');
    expect(populateMappedId('hi2Interfaces')).toEqual('2');
    expect(populateMappedId('hi3Interfaces')).toEqual('3');
  });

  it('populateLiInterfaceId', () => {
    expect(populateLiInterfaceId('provisioningInterfaces')).toEqual('x1');
    expect(populateLiInterfaceId('dataInterfaces')).toEqual('x2');
    expect(populateLiInterfaceId('contentInterfaces')).toEqual('x3');
    expect(populateLiInterfaceId('hi2Interfaces')).toEqual('hi2');
    expect(populateLiInterfaceId('hi3Interfaces')).toEqual('hi3');
  });

  it('getTitleByLiInterface', () => {
    expect(getTitleByLiInterface('x1', '')).toEqual('Provisioning Interface (X1)');
    expect(getTitleByLiInterface('x2', '')).toEqual('Delivery Interface (X2)');
    expect(getTitleByLiInterface('x3', '')).toEqual('Content Interface (X3)');
    expect(getTitleByLiInterface('hi2', '')).toEqual('Intercept Related Information (IRI)');
    expect(getTitleByLiInterface('hi3', '')).toEqual('Communication Content (CC)');
    expect(getTitleByLiInterface('NA', 'vpn-name-a')).toEqual('vpn-name-a');
  });

});
