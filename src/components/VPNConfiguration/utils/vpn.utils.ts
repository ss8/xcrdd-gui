import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { VpnConfig, LiInterface } from 'data/vpnConfig/vpnConfig.types';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';

export function createVpnConfigBasedOnTemplate(
  template: FormTemplate,
  liInterface: LiInterface,
): VpnConfig {
  const liInterfaceIndex = liInterface as ('x1' | 'x2' | 'x3' | 'hi2' | 'hi3');
  const mappedId = MAP_VPN_MAPPEDID_BY_INTERFACE_ID[liInterfaceIndex];
  const vpnConfigForm: Partial<VpnConfig> = {
    ...getFieldValuesFromSection(template.main as FormTemplateSection),
    ...getFieldValuesFromSection(template.ike as FormTemplateSection),
    ...getFieldValuesFromSection(template.sa as FormTemplateSection),
    mappedId,
  };

  return omitBy(vpnConfigForm, isNil) as unknown as VpnConfig;
}

const MAP_VPN_LI_INTERFACE_ID_BY_INTERFACE_NAME = {
  provisioningInterfaces: 'x1',
  dataInterfaces: 'x2',
  contentInterfaces: 'x3',
  hi2Interfaces: 'hi2',
  hi3Interfaces: 'hi3',
};

const MAP_VPN_MAPPEDID_BY_INTERFACE_NAME = {
  provisioningInterfaces: '1',
  dataInterfaces: '2',
  contentInterfaces: '3',
  hi2Interfaces: '2',
  hi3Interfaces: '3',
};

const MAP_VPN_MAPPEDID_BY_INTERFACE_ID = {
  x1: '1',
  x2: '2',
  x3: '3',
  hi2: '2',
  hi3: '3',
};

const MAP_TITLE_BY_LI_INTERFACE = {
  x1: 'Provisioning Interface (X1)',
  x2: 'Delivery Interface (X2)',
  x3: 'Content Interface (X3)',
  hi2: 'Intercept Related Information (IRI)',
  hi3: 'Communication Content (CC)',
};

export function getTitleByLiInterface(
  liInterface: LiInterface,
  name: string,
): string {
  // For the 'NA' LiInterface, give the default title with name.
  if (liInterface === 'NA') {
    return name;
  }
  const title = MAP_TITLE_BY_LI_INTERFACE[liInterface];
  return title;
}

export function populateMappedId(
  interfaceName: 'provisioningInterfaces' | 'dataInterfaces' | 'contentInterfaces' | 'hi2Interfaces' | 'hi3Interfaces',
): string {
  return MAP_VPN_MAPPEDID_BY_INTERFACE_NAME[interfaceName] ?? '0';
}

export function populateLiInterfaceId(
  interfaceName: 'provisioningInterfaces' | 'dataInterfaces' | 'contentInterfaces' | 'hi2Interfaces' | 'hi3Interfaces',
): LiInterface {
  return MAP_VPN_LI_INTERFACE_ID_BY_INTERFACE_NAME[interfaceName] as LiInterface;
}

function getFieldValuesFromSection(
  templateSection: FormTemplateSection,
): Partial<VpnConfig> {
  return Object.entries(templateSection?.fields)
    .filter(([fieldName, field]) => field.initial != null && fieldName !== 'templateDesc')
    .reduce((previousValue, [fieldName, field]) => ({
      ...previousValue,
      [fieldName]: field.initial,
    }), {});
}
