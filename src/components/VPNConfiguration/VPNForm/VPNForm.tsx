import React, {
  forwardRef, useRef, useImperativeHandle,
} from 'react';
import {
  FormTemplate, FormTemplateSection,
} from 'data/template/template.types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import Form, { FieldUpdateHandlerParam } from 'shared/form';
import FieldDesc from 'components/FieldDesc';
import '../../FormLayout/form-layout.scss';

type Props = {
  data: Partial<VpnConfig>
  template: FormTemplate
  onFieldUpdate: (param: FieldUpdateHandlerParam, formSection?: string) => void
}

const customFieldComponentMap = {
  vpnTemplateDesc: FieldDesc,
};

export type VPNFormRef = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => VpnConfig
}

export const VPNForm = forwardRef<VPNFormRef, Props>(({
  data,
  template,
  onFieldUpdate = () => null,
}: Props, ref) => {
  const mainRef = useRef<Form>(null);
  const ikeRef = useRef<Form>(null);
  const saRef = useRef<Form>(null);

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isMainValid = mainRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isIKEValid = ikeRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isSAValid = saRef.current?.isValid(forceValidation, forceTouched) ?? true;

    return isMainValid && isIKEValid && isSAValid;
  };

  const getValues = () => {
    const formValues: VpnConfig = {
      ...mainRef.current?.getValues(),
      ...ikeRef.current?.getValues(),
      ...saRef.current?.getValues(),
    };
    return formValues;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  return (
    <div className="formLayout">
      <div data-testid="VPNMain">
        <Form
          ref={mainRef}
          data-test="main-form"
          name={`main_${new Date().getTime()}`}
          defaults={data}
          fields={(template.main as FormTemplateSection).fields}
          layout={(template.main as FormTemplateSection).metadata.layout?.rows}
          customFieldComponentMap={customFieldComponentMap}
          fieldUpdateHandler={onFieldUpdate}
        />
      </div>
      <div data-testid="VPNIKEParameters">
        <h3>IKE Parameters</h3>
        <Form
          ref={ikeRef}
          data-test="ike-form"
          name={`ike_${new Date().getTime()}`}
          defaults={data}
          fields={(template.ike as FormTemplateSection).fields}
          layout={(template.ike as FormTemplateSection).metadata.layout?.rows}
          fieldUpdateHandler={onFieldUpdate}
        />
      </div>
      <div data-testid="VPNSAParameters">
        <h3>SA Parameters</h3>
        <Form
          ref={saRef}
          data-test="sa-form"
          name={`sa_${new Date().getTime()}`}
          defaults={data}
          fields={(template.sa as FormTemplateSection).fields}
          layout={(template.sa as FormTemplateSection).metadata.layout?.rows}
          fieldUpdateHandler={onFieldUpdate}
        />
      </div>
    </div>
  );
});

export default VPNForm;
