import React from 'react';
import DefaultContainer from './default-container';

const AutoForm = ({ formInputs }:any) => {
  return (
    <div className="box-row">
      {Object.keys(formInputs).map((key: any, index: number) => {
        return <DefaultContainer key={index} formInput={formInputs[key]} />;
      })}
    </div>
  );
};
export default AutoForm;
