import { Intent } from '@blueprintjs/core';
import { checkGivenTimeWithCurrentTime }
  from '../../shared/constants/checkTimeValidation';
import DateTimeRangeValidator from '../../shared/form/validators/dateTimeRangeValidator';

export const SupportedTypes = {
  SWITCH: 'Switch',
  INPUTGROUP: 'InputGroup',
  INPUT: 'Input',
  HTMLSelect: 'HTMLSelect',
  DatePicker: 'DatePicker',
  DateInput: 'DateInput',
};

export const updateAutoFormState = (el: any, state: any) => {
  const inputName = el.target.name;
  const stateSnapshot = { ...state };
  stateSnapshot.fieldDirty = true;
  const {
    type, isRequired, mustNotInclude,
  } = stateSnapshot.formInputs[inputName];

  const inputValue =
  type === SupportedTypes.SWITCH ? el.target.checked
    : type === SupportedTypes.HTMLSelect ? el.currentTarget.value
      : el.target.value;

  stateSnapshot.formInputs[inputName].value = inputValue;
  stateSnapshot.formInputs[inputName].intent = Intent.NONE;

  if (isRequired && inputValue === '') {
    stateSnapshot.formInputs[inputName].intent = Intent.WARNING;
  }

  if (isRequired && inputValue !== '') {
    stateSnapshot.formInputs[inputName].helperText = '';
  }

  if (mustNotInclude) {
    if (mustNotInclude.includes(inputValue)) {
      stateSnapshot.formInputs[inputName].intent = Intent.DANGER;
      stateSnapshot.formInputs[
        inputName
      ].helperText = `${stateSnapshot.formInputs[inputName].label} "${inputValue}" is taken`;
    } else {
      stateSnapshot.formInputs[inputName].helperText = ' ';
    }
  }

  return stateSnapshot;
};

export const resetAutoForm = (formInputs: any, initialStateFormInputs: any) => {
  return Object.keys(formInputs).map((key: any) => {
    return Object.keys(formInputs[key]).forEach((property: any) => {
      formInputs[key][property] = initialStateFormInputs[key][property];
    });
  });
};

export const getAutoFormValue = (formInputs: any) => {
  const res: any = {};
  Object.keys(formInputs).forEach((key: any) => {
    res[key] = formInputs[key].value;
  });
  return res;
};

export const validateAutoForm = (formInputs: any) => {
  let isValid = true;
  let errorMessage = null;

  // eslint-disable-next-line array-callback-return
  Object.keys(formInputs).map((key: any) => {
    let intent = '';
    let helperText = '';
    const {
      mustNotInclude, isRequired, value, originalValue, label, autoValidate,
    } = formInputs[key];

    if (isRequired && (value === undefined || value.toString().trim() === '')) {
      intent = Intent.DANGER;
      helperText = 'Please enter a value';
      isValid = false;
    }

    if (mustNotInclude) {
      if (mustNotInclude.includes(value)) {
        intent = Intent.DANGER;
        isValid = false;
      }
    }

    if (autoValidate && autoValidate.length > 0) {
      autoValidate.forEach((validator: any) => {
        if (validator.type === 'mustBeBiggerThan') {
          if (new Date(value).getTime() < new Date(formInputs[validator.field].value).getTime()) {
            helperText = validator.errorMessage;
            intent = Intent.DANGER;
            isValid = false;
          }
        } else if (validator.type === 'checkGivenTimeWithCurrentTime') {
          if (checkGivenTimeWithCurrentTime(value, formInputs[validator.field].value)) {
            helperText = validator.errorMessage;
            intent = Intent.DANGER;
            isValid = false;
          }
        } else {
          const validatorClasses = [DateTimeRangeValidator];
          const validatorClass = validatorClasses.find((aValidatorClass) => {
            return (aValidatorClass.isType(validator.type));
          });
          if (validatorClass && validatorClass.isValid(null, null,
            value, originalValue, validator, null) === false) {
            helperText = validator.errorMessage;
            errorMessage = `${label} Field: ${validator.errorMessage}`;
            intent = Intent.DANGER;
            isValid = false;
          }
        }
      });
    }

    formInputs[key].intent = intent;
    formInputs[key].helperText = helperText;
  });
  return { isValid, formInputs, errorMessage };
};
