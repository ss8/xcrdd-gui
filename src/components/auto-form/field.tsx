import React, { Fragment } from 'react';
import {
  InputGroup, Checkbox, Classes, Intent,
} from '@blueprintjs/core';

import { SupportedTypes } from './utils';
import { DatePicker, DateInput, HTMLSelect } from '../index';

interface IAutoFieldProps {
  formInput: {
    helperText: string,
    id: string,
    intent: Intent,
    isRequired: boolean,
    type: string,
    value: any,
    [propName: string]: any;
  }
}

const AutoField = ({ formInput }:IAutoFieldProps) => {
  const {
    type,
    id,
    value,
    fill,
    ...formInputSpread
  } = formInput;

  const fieldId = {
    id,
    name: id,
  };
  return (
    <Fragment>
      {type === SupportedTypes.INPUTGROUP && (
      <InputGroup
        {...fieldId}
        {...formInputSpread}
        value={value}
      />
      )}
      {type === SupportedTypes.INPUT && (
      <input
        className={Classes.INPUT}
        {...fieldId}
        {...formInputSpread}
        value={value}
      />
      )}
      {type === SupportedTypes.SWITCH && (
      <Checkbox
        {...fieldId}
        {...formInputSpread}
        checked={value}
      />
      )}
      {type === SupportedTypes.HTMLSelect && (
      <HTMLSelect
        {...formInput}
      />
      )}
      {type === SupportedTypes.DatePicker && (
      <DatePicker
        {...formInput}
      />
      )}
      {type === SupportedTypes.DateInput && (
      <DateInput
        {...formInput}
      />
      )}

    </Fragment>
  );
};

export default AutoField;
