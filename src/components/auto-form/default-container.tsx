import React from 'react';
import { Label, Intent } from '@blueprintjs/core';
import AutoField from './field';
import './style.scss';

const DefaultContainer = ({ formInput }:any) => {
  const {
    label,
    id,
    isRequired,
    intent,
    helperText,
  } = formInput;

  return (
    <div className="box-column">
      <Label htmlFor={id}>{label}: { isRequired && <span className="intent-danger-message">*</span>} </Label>
      <AutoField formInput={formInput} />
      {helperText !== '' && <Label className={intent === Intent.DANGER ? 'intent-danger-message' : ''}>{helperText}</Label>}
    </div>
  );
};

export default DefaultContainer;
