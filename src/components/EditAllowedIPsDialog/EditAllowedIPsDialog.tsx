import React, {
  FunctionComponent,
  useState,
  Fragment,
  useEffect,
} from 'react';
import {
  Button, Classes, Dialog,
} from '@blueprintjs/core';
import { ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { AppToaster, showError } from 'shared/toaster';
import './edit-allowed-ips-dialog.scss';
import PointsOfInterceptionProperty from '../PointsOfInterceptionProperty';
import AllowedIPsUpdateTable from '../AllowedIPsUpdateTable';

type Props = {
  isEditing: boolean,
  isOpen: boolean,
  ipsWhiteList: DistributorConfigurationListenerAfWhiteList[],
  ipTableColumnDefs: (ColDef | ColGroupDef)[],
  distributorName?: string;
  listeningIpAddress?: string;
  listeningPort?: string;
  onSubmit: (data: DistributorConfigurationListenerAfWhiteList[]) => void,
  onCancel: () => void,
}

const EditAllowedIPsDialog: FunctionComponent<Props> = ({
  isOpen,
  distributorName,
  ipsWhiteList,
  ipTableColumnDefs,
  listeningIpAddress,
  listeningPort,
  isEditing,
  onCancel,
  onSubmit,
}: Props) => {
  const [ipsWhiteListChanged, setIpsWhiteListChanged] = useState(false);
  const [isDialogValid, setDialogValid] = useState(true);
  const [isDialogSubmitted, setDialogSubmitted] = useState(false);
  const [
    currentAFWhiteListData,
    setCurrentAFWhiteListData,
  ] = useState<DistributorConfigurationListenerAfWhiteList[]>([]);

  useEffect(() => {
    setIpsWhiteListChanged(false);
    return () => {
      AppToaster.clear();
    };
  }, [isOpen]);

  const handleCancelAddNewIp = () => {
    setIpsWhiteListChanged(true);
  };

  const handleCellKeyDown = () => {
    setIpsWhiteListChanged(true);
  };

  const handleRowDataUpdated = (
    data: DistributorConfigurationListenerAfWhiteList[],
    { hasAddressFormatError, hasNonUniqueAFError }: { hasAddressFormatError: boolean, hasNonUniqueAFError: boolean },
  ) => {

    AppToaster.clear();
    if (hasNonUniqueAFError) {
      showError('AF must have an unique entry in the list. Please review entries.');
    }

    if (hasAddressFormatError || hasNonUniqueAFError) {
      setDialogValid(false);

    } else {
      setCurrentAFWhiteListData(data);
      setIpsWhiteListChanged(true);
      setDialogValid(true);
    }
  };

  const handleSubmit = () => {
    setDialogSubmitted(true);
    if (isDialogValid) {
      setIpsWhiteListChanged(false);
      onSubmit(currentAFWhiteListData);
    }
  };

  const handleCancel = () => {
    setIpsWhiteListChanged(false);
    onCancel();
    setDialogSubmitted(false);
  };

  const renderFooterActions = (hasChanged: boolean) => {
    if (hasChanged && isEditing) {
      return (
        <Fragment>
          <Button data-testid="EditAllowedIPsDialogCancelButton" onClick={handleCancel}>Cancel</Button>
          <Button data-testid="EditAllowedIPsDialogConfirmButton" intent="primary" onClick={handleSubmit}>Confirm</Button>
        </Fragment>
      );
    }

    return <Button intent="primary" onClick={handleCancel}>Close</Button>;
  };

  return (
    <Dialog
      autoFocus
      isOpen={isOpen}
      title="SS8 Distributor Listener - Allowed AF IP List"
      className="edit-allowed-ips-dialog"
      onClose={handleCancel}
    >
      <div className={Classes.DIALOG_BODY}>
        <div className="edit-allowed-ips-dialog-points-of-interception-properties">
          <PointsOfInterceptionProperty label="SS8 Distributor Configuration" value={distributorName} />
          <PointsOfInterceptionProperty label="Listening IP Address" value={listeningIpAddress} />
          <PointsOfInterceptionProperty label="Listening Port" value={listeningPort} />
        </div>
        <div>
          <p><strong>Allowed AF IPs List</strong></p>
          <p className="edit-allowed-ips-dialog-listener-explanation">
            This listener will only listen to the IP addresses in the list below.
            When the list is empty it will listen to all Access Functions.
          </p>
        </div>
        <div>
          <AllowedIPsUpdateTable
            isEditing={isEditing}
            isDialogSubmitted={isDialogSubmitted}
            rowData={ipsWhiteList}
            ipTableColumnDefs={ipTableColumnDefs}
            onAddNewIp={handleCancelAddNewIp}
            onRowDataUpdated={handleRowDataUpdated}
            onCellKeyDown={handleCellKeyDown}
          />
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            {renderFooterActions(ipsWhiteListChanged)}
          </div>
        </div>
      </div>
    </Dialog>
  );
};

export default EditAllowedIPsDialog;
