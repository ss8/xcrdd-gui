import React from 'react';
import {
  render,
  RenderResult,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { distributorsIPsWhiteListEditableTableColumnDefs } from 'routes/AccessFunction/AccessFunction.types';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import EditAllowedIPsDialog from './EditAllowedIPsDialog';

const IPS_DATA_MOCK: DistributorConfigurationListenerAfWhiteList[] = [
  {
    afName: 'afName1',
    afAddress: '111.111.111.111',
    afId: 'id1',
  },
  {
    afName: 'afName2',
    afAddress: '111.111.111.222',
    afId: 'id2',
  },
  {
    afName: 'afName3',
    afAddress: '11.11.11.11',
    afId: 'id3',
  },
];

const DISTRIBUTOR_MOCK = {
  distributorName: 'SX3DIST_CONF_1',
  listeningIpAddress: '12.12.12.12',
  listeningPort: '12345',
};

describe('<EditAllowedIPsDialog />', () => {
  const onSubmitFn = jest.fn();
  const onCancelFn = jest.fn();

  describe('when EditAllowedIPsDialog isOpened and user click Close, Cancel or Confirm buttons', () => {
    let component: RenderResult;
    beforeEach(async () => {
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitFn}
          onCancel={onCancelFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should calls the close callback function passed as props by clicking close top button', async () => {
      const closeButton = component?.getByText('SS8 Distributor Listener - Allowed AF IP List').parentElement?.querySelector('.bp3-button.bp3-minimal.bp3-dialog-close-button');
      await act(() => {
        fireEvent.click(closeButton as Element);
        // Wait for the animation
        return new Promise<void>((resolve) => setTimeout(resolve, 100));
      });

      expect(onCancelFn).toHaveBeenCalled();
    });

    it('should calls the close callback function passed as props by clicking close button', async () => {
      const closeButton = component?.getByText('Close');
      await act(() => {
        fireEvent.click(closeButton as Element);
        // Wait for the animation
        return new Promise<void>((resolve) => setTimeout(resolve, 100));
      });

      expect(onCancelFn).toHaveBeenCalled();
    });

    it('should calls the close callback function passed as props by clicking editing mode cancel button', () => {
      // First click the Add IP button to make Cancel button be visible
      const addItemButton = component.getByText('Add AF IP Address').parentElement;
      act(() => {
        fireEvent.click(addItemButton as Element);
      });

      const closeButton = component?.getByText('Cancel');
      act(() => {
        fireEvent.click(closeButton);
      });

      expect(onCancelFn).toHaveBeenCalled();
    });
  });

  describe('when EditAllowedIPsDialog isOpened and  ipsWhiteList has a list of IPs', () => {
    let component: RenderResult;
    beforeEach(async () => {
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitFn}
          onCancel={onCancelFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should render the modal containing the distributors parameters passed', async () => {
      expect(await screen?.findByText(DISTRIBUTOR_MOCK.distributorName)).toBeVisible();
      expect(await screen?.findByText(DISTRIBUTOR_MOCK.listeningIpAddress)).toBeVisible();
      expect(await screen?.findByText(DISTRIBUTOR_MOCK.listeningPort.toString())).toBeVisible();
    });

    it('should render the table containing the passed white list IPs', async () => {
      expect(await screen.findByText(IPS_DATA_MOCK[0].afAddress)).toBeVisible();
      expect(await screen.findByText(IPS_DATA_MOCK[1].afAddress)).toBeVisible();
      expect(await screen.findByText(IPS_DATA_MOCK[2].afAddress)).toBeVisible();
    });
  });

  describe('when user Add a new IP', () => {
    const onSubmitToWatchAddFn = jest.fn();

    let component: RenderResult;

    beforeEach(async () => {
      jest.spyOn(global.Math, 'random').mockReturnValue(8);
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={[]}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitToWatchAddFn}
          onCancel={onCancelFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should increased IP list by clicking ADD AF button', () => {
      const addItemButton = screen.getByText('Add AF IP Address').parentElement;

      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(0);

      fireEvent.click(addItemButton as Element);
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(1);

      fireEvent.click(addItemButton as Element);
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(2);
    });
  });

  describe('when user Remove a new IP', () => {
    const onSubmitToWatchAddFn = jest.fn();

    let component: RenderResult;

    beforeEach(async () => {
      /*
        TODO: The remove tests needs a more specific attention because for some reason
        the render method is not rendering the delete items button in the test DOM,
        so it must be investigated why to have effective remove item tests
      */
      const reverseColDefs = [...distributorsIPsWhiteListEditableTableColumnDefs].reverse();
      jest.spyOn(global.Math, 'random').mockReturnValue(8);
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={IPS_DATA_MOCK}
          ipTableColumnDefs={reverseColDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitToWatchAddFn}
          onCancel={onCancelFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should receive a decreased IP list by clicking ADD AF button and then Confirm button', async () => {
      const rowsBeforeDelete = await (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row');
      expect(rowsBeforeDelete.length).toBe(3);

      // First remove Button
      const firstItemRemoveButton = await (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="actions"]')[0].querySelector('button');
      fireEvent.click(firstItemRemoveButton as Element);

      expect(await component.findByText(`Are you sure you want to delete IP address ${IPS_DATA_MOCK[0].afAddress} for AF '${IPS_DATA_MOCK[0].afName}' ?`)).toBeVisible();

      // Confirm deletion
      const confirmDeleteButton = await component.findByText('Yes, delete it');
      fireEvent.click(confirmDeleteButton as Element);

      const rowsAfterDelete = await (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row');
      expect(rowsAfterDelete.length).toBe(2);
    });
  });

  describe('when user Remove two IPs', () => {
    const onSubmitToWatchAddFn = jest.fn();

    let component: RenderResult;

    beforeEach(async () => {
      /*
        TODO: The remove tests needs a more specific attention because for some reason
        the render method is not rendering the delete items button in the test DOM,
        so it must be investigated why to have effective remove item tests
      */
      const reverseColDefs = [...distributorsIPsWhiteListEditableTableColumnDefs].reverse();
      jest.spyOn(global.Math, 'random').mockReturnValue(8);
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={IPS_DATA_MOCK}
          ipTableColumnDefs={reverseColDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitToWatchAddFn}
          onCancel={onCancelFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should receive a decreased IP list by clicking ADD AF button and then Confirm button', async () => {
      expect((await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(3);

      // First remove Button
      const firstItemRemoveButton = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="actions"]')[0].querySelector('button');
      fireEvent.click(firstItemRemoveButton as Element);

      expect(await component.findByText(`Are you sure you want to delete IP address ${IPS_DATA_MOCK[0].afAddress} for AF '${IPS_DATA_MOCK[0].afName}' ?`)).toBeVisible();

      // Confirm deletion
      fireEvent.click(await component.findByText('Yes, delete it') as Element);

      expect((await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(2);

      // First remove Button
      const newFirstItemRemoveButton = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="actions"]')[0].querySelector('button');
      fireEvent.click(newFirstItemRemoveButton as Element);

      expect(await component.findByText(`Are you sure you want to delete IP address ${IPS_DATA_MOCK[1].afAddress} for AF '${IPS_DATA_MOCK[1].afName}' ?`)).toBeVisible();

      // Confirm deletion
      fireEvent.click(await component.findByText('Yes, delete it') as Element);

      expect((await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(1);

      fireEvent.click((await component?.findByText('Confirm')).parentElement as Element);

      expect(onSubmitToWatchAddFn).toHaveBeenCalledWith([{
        afAddress: '11.11.11.11', afId: 'id3', afName: 'afName3', rowKey: 3,
      }]);
    });
  });

  describe('when update list elements', () => {
    let component: RenderResult;
    const onSubmitCallbackFn = jest.fn();
    const onCancelCallbackFn = jest.fn();

    beforeEach(async () => {
      component = render(
        <EditAllowedIPsDialog
          isEditing
          isOpen
          ipsWhiteList={IPS_DATA_MOCK}
          ipTableColumnDefs={distributorsIPsWhiteListEditableTableColumnDefs}
          distributorName={DISTRIBUTOR_MOCK.distributorName}
          listeningIpAddress={DISTRIBUTOR_MOCK.listeningIpAddress}
          listeningPort={DISTRIBUTOR_MOCK.listeningPort}
          onSubmit={onSubmitCallbackFn}
          onCancel={onCancelCallbackFn}
        />,
      );
      await waitFor(() => component?.getByRole('grid'));
    });

    it('should change the afAddress to valid values and call onSubmit callback function', async () => {
      const afIdCell = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="afName"]')[0];
      act(() => {
        fireEvent.doubleClick(afIdCell);
      });

      act(() => {
        fireEvent.change(afIdCell.querySelector('input') as Element, { target: { value: 'afNameNew' } });
        fireEvent.focusOut(afIdCell.querySelector('input') as Element);
      });

      const afAddressCell = (await component.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row [col-id="afAddress"]')[0];
      act(() => {
        fireEvent.doubleClick(afAddressCell);
      });

      act(() => {
        fireEvent.change(afAddressCell.querySelector('input') as Element, { target: { value: '22.22.11.22' } });
        fireEvent.focusOut(afAddressCell.querySelector('input') as Element);
      });

      await waitFor(() => expect(screen.getByText('Confirm')).toBeVisible());

      fireEvent.click((await component?.findByText('Confirm')).parentElement as Element);

      const dataAfterEditingTable = [{
        afAddress: '22.22.11.22', afId: 'id1', afName: 'afNameNew', rowKey: 1,
      }, {
        afAddress: '111.111.111.222', afId: 'id2', afName: 'afName2', rowKey: 2,
      }, {
        afAddress: '11.11.11.11', afId: 'id3', afName: 'afName3', rowKey: 3,
      }];

      expect(onSubmitCallbackFn).toHaveBeenCalledWith(dataAfterEditingTable);
    });
  });
});
