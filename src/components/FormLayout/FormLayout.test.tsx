import React from 'react';
import { mount } from 'enzyme';
import { FormLayout, FormLayoutContent, FormLayoutFooter } from './FormLayout';

describe('<FormLayout />', () => {
  it('should render the child content for the layout', () => {
    const wrapper = mount(
      <FormLayout>
        Some Layout
      </FormLayout>,
    );
    expect(wrapper.text()).toEqual('Some Layout');
  });
});

describe('<FormLayoutContent />', () => {
  it('should render the child content for the content', () => {
    const wrapper = mount(
      <FormLayoutContent>
        Some layout content
      </FormLayoutContent>,
    );
    expect(wrapper.text()).toEqual('Some layout content');
  });
});

describe('<FormLayoutFooter />', () => {
  it('should render the child content for the footer', () => {
    const wrapper = mount(
      <FormLayoutFooter>
        Some layout footer
      </FormLayoutFooter>,
    );
    expect(wrapper.text()).toEqual('Some layout footer');
  });
});
