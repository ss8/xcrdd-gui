import React, { FunctionComponent, ReactNode } from 'react';
import './form-layout.scss';

type Props = {
  children: ReactNode
}

type FormLayoutContentProps = Props & {
  paddingTop?: string
}

type FormLayoutFixedHeaderProps = Props & {
  height: string
}

export const FormLayoutContent: FunctionComponent<FormLayoutContentProps> = (
  { children, paddingTop }: FormLayoutContentProps,
) => (
  <div className="formLayoutContent" data-testid="FormLayoutContent" style={{ paddingTop: paddingTop ?? '0' }}>
    {children}
  </div>
);

export const FormLayoutFixedHeader: FunctionComponent<FormLayoutFixedHeaderProps> = (
  { children, height }: FormLayoutFixedHeaderProps,
) => (
  <div className="formLayoutFixedHeader" data-testid="FormLayoutFixedHeader" style={{ height }}>
    {children}
  </div>
);

export const FormLayoutFooter: FunctionComponent<Props> = ({ children }: Props) => (
  <div className="formLayoutFooter" data-testid="FormLayoutFooter">
    {children}
  </div>
);

export const FormLayout: FunctionComponent<Props> = ({ children }: Props) => (
  <div className="formLayout" data-testid="FormLayout">
    {children}
  </div>
);
