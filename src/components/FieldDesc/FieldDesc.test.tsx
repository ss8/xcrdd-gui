import React from 'react';
import {
  render,
  screen,
} from '@testing-library/react';
import FieldDesc from './FieldDesc';

describe('FieldDesc', () => {
  it('should render component', () => {

    render(
      <FieldDesc value="Test the field description" />,
    );

    expect(screen.getByTestId('FieldDesc')).toBeVisible();
    expect(screen.getByText('Test the field description')).toBeVisible();

  });

  it('should render field description with css field-desc-container-value', () => {
    const { container } = render(
      <FieldDesc value="Test the field description" />,
    );

    expect(container.querySelectorAll('.field-desc-container-value')).toHaveLength(1);
  });

});
