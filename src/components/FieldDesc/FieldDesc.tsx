import React from 'react';
import './field-desc.scss';

type Props = {
  value: string,
};

const FieldDesc = ({ value } : Props): JSX.Element => {

  return (
    <div data-testid="FieldDesc" className="field-desc-container ">
      <span className="field-desc-container-value">
        {value}
      </span>
    </div>
  );
};

export default FieldDesc;
