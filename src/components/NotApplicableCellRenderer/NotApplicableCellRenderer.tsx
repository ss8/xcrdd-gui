import React, { Fragment } from 'react';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';

import './not-applicable-cell-renderer.scss';

type Props = Partial<ICellRendererParams>;

const NotApplicableCellRenderer = ({ value, node }:Props): JSX.Element => {

  if (node?.group) {
    return <span />;
  }

  if (value != null && value !== '') {
    return <Fragment>{value}</Fragment>;
  }

  return (
    <span className="not-applicable-cell-renderer">
      not applicable
    </span>
  );
};

export default NotApplicableCellRenderer;
