import React from 'react';
import { stateValue } from 'utils/valueFormatters/stateValueFormatter';
import './field-current-state.scss';

type Props = {
  value: 'ACTIVE' | 'INACTIVE' |'PARTIALLY_ACTIVE' | 'FAILED',
}

const FieldCurrentState = ({ value }: Props): JSX.Element => {

  const bulletClassName = (state: string) => {
    return `field-current-state-container-bullet field-current-state-container-bullet-${state.toLowerCase()}`;
  };

  return (
    <div data-testid="FieldCurrentState" className="field-current-state-container">
      <div className={bulletClassName(value)} />
      <span className="field-current-state-container-value">
        {stateValue(value)}
      </span>
    </div>
  );
};

export default FieldCurrentState;
