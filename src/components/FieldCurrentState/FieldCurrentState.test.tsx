import React from 'react';
import {
  render,
  screen,
} from '@testing-library/react';
import FieldCurrentState from './FieldCurrentState';

describe('FieldCurrentState', () => {
  it('should render component state in ACTIVE mode', () => {

    const { container } = render(
      <FieldCurrentState value="ACTIVE" />,
    );

    expect(screen.getByTestId('FieldCurrentState')).toBeVisible();
    expect(screen.getByText('Active')).toBeVisible();
    expect(container.querySelectorAll('.field-current-state-container-bullet-active')).toHaveLength(1);

  });

  it('should render component state in FAILED mode', () => {
    const { container } = render(
      <FieldCurrentState value="FAILED" />,
    );

    expect(screen.getByTestId('FieldCurrentState')).toBeVisible();
    expect(screen.getByText('Failed')).toBeVisible();
    expect(container.querySelectorAll('.field-current-state-container-bullet-failed')).toHaveLength(1);

  });

  it('should render component state in INACTIVE mode', () => {
    const { container } = render(
      <FieldCurrentState value="INACTIVE" />,
    );

    expect(screen.getByTestId('FieldCurrentState')).toBeVisible();
    expect(screen.getByText('Inactive')).toBeVisible();
    expect(container.querySelectorAll('.field-current-state-container-bullet-inactive')).toHaveLength(1);

  });

  it('should render component state in PARTIALLY_ACTIVE mode', () => {
    const { container } = render(
      <FieldCurrentState value="PARTIALLY_ACTIVE" />,
    );

    expect(screen.getByTestId('FieldCurrentState')).toBeVisible();
    expect(screen.getByText('Partially Active')).toBeVisible();
    expect(container.querySelectorAll('.field-current-state-container-bullet-partially_active')).toHaveLength(1);

  });
});
