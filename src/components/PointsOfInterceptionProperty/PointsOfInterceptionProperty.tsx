import React, { FC } from 'react';

type Props = {
  label: string,
  value?: string | number,
}

const PointsOfInterceptionProperty: FC<Props> = ({ label, value }: Props) => (
  <div className="points-of-interception-property">
    <p><strong>{label}</strong></p>
    <p>{value}</p>
  </div>
);

export default PointsOfInterceptionProperty;
