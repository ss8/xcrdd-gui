import React from 'react';
import { ProgressBar, Intent } from '@blueprintjs/core';

interface IProgressBarProps {
  animate?: boolean;
  className?: string;
  intent?: Intent;
  stripes?: boolean;
  value: number;
}
const CProgressBar = (props: IProgressBarProps) => {
  const { intent = 'primary' } = props;

  return (
    <ProgressBar
      intent={intent}
      value={props.value}
      animate={props.animate}
      stripes={props.stripes}
      className={props.className}
    />
  );
};

export default CProgressBar;
