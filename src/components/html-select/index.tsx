import React from 'react';
import {
  HTMLSelect,
} from '@blueprintjs/core';

interface IHTMLSelectProps {
  id: string,
  disabled?: boolean,
  fill?: boolean,
  className?: string,
  onChange?: any,
  options?: Array<string | number> | {label:string, value:string}[]
  value: string | number,
  iconProps:any,
}

const CHTMLSelect = (props: IHTMLSelectProps) => {
  const {
    id,
    fill,
    value,
    onChange,
    options,
    iconProps,
  } = props;
  return (
    <HTMLSelect
      data-test={`input-${id}`}
      name={id}
      fill={fill}
      value={value}
      onChange={onChange}
      options={options && options.length ? options : []}
      iconProps={iconProps}
    />
  );
};

export default CHTMLSelect;
