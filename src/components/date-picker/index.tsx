import React from 'react';
import {
  DatePicker, TimePrecision,
} from '@blueprintjs/datetime';
import moment from 'moment';

interface IDateInputProps {
  className?: string,
  closeOnSelection?: boolean;
  date?: Date | null;
  maxDate?: Date,
  minDate?: Date,
  onChange?: any,
  reverseMonthAndYearMenus?: boolean;
  shortcuts?: boolean;
  timePrecision?: TimePrecision | undefined;
  value: Date,
}

const CDatePicker = (props: IDateInputProps) => {
  const {
    value,
    maxDate,
    ...spreadProps
  } = props;
  return (
    <DatePicker
      maxDate={maxDate || moment(new Date()).add(20, 'year').toDate()}
      value={value ? new Date(value) : undefined}
      {...spreadProps}
    />
  );
};

export default CDatePicker;
