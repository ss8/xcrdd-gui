import React, { Component, ReactNode } from 'react';
import { Alert, ProgressBar } from '..';

import ShowStatusMessage from '../../xcipio/warrants/forms/updateWarrant/show-status';
import './style.scss';

interface IProgressBarIndicatorProps {
  counter: number,
  errors: Array<string>,
  handleConfirm: any,
  inProgress: boolean,
  isOpen: boolean,
  totalSelected: number,
  totalProcessed: number,
}

class ProgressBarIndicator extends Component<IProgressBarIndicatorProps> {
  renderProgress(): ReactNode {
    const { totalProcessed, totalSelected } = this.props;

    if (totalProcessed < totalSelected) {
      return (
        <React.Fragment><b>{totalProcessed}</b> out of <b>{totalSelected}</b> processed</React.Fragment>
      );
    }

    return 'Completed';
  }

  renderDetails(): ReactNode {
    const { counter, errors } = this.props;

    if (counter === 0 && errors.length === 0) {
      return null;
    }

    if (counter > 0 && errors.length === 0) {
      return <React.Fragment> - <b>{counter}</b> succeeded</React.Fragment>;
    }

    if (counter === 0 && errors.length > 0) {
      return <React.Fragment> - <b>{errors.length}</b> failed</React.Fragment>;
    }

    return <React.Fragment> - <b>{counter}</b> succeeded and <b>{errors.length}</b> failed</React.Fragment>;
  }

  render(): ReactNode {
    const {
      isOpen,
      handleConfirm,
      inProgress,
      errors,
      totalSelected,
      totalProcessed,
    } = this.props;

    return (
      <Alert
        isOpen={isOpen}
        handleConfirm={handleConfirm}
        confirmButtonText={inProgress ? 'Cancel' : 'OK'}
        style={{
          width: '550px',
          minWidth: '550px',
          minHeight: errors.length ? '350px' : 'auto',
        }}
      >
        <div className="bulk-content-container">
          <div>
            <p>
              {this.renderProgress()}
              {this.renderDetails()}
            </p>
          </div>
          <ProgressBar
            value={totalProcessed / totalSelected}
            key={totalProcessed}
            animate={inProgress}
            className="bulk-progress-bar"
          />
          {errors.length !== 0 && (
            <ShowStatusMessage
              isDialog
              hasErrors
              msgs={errors}
              className="bulk-action"
            />
          )}
        </div>
      </Alert>
    );
  }
}

export default ProgressBarIndicator;
