import React from 'react';
import { shallow } from 'enzyme';
import ProgressBarIndicator from './index';

describe('<ProgressBarIndicator />', () => {
  let testMocks: any;
  let wrapper: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.props = {
      counter: 0,
      isOpen: true,
      handleConfirm: jest.fn(),
      inProgress: true,
      errors: [],
      totalSelected: 10,
      totalProcessed: 0,
    };

    wrapper = shallow(<ProgressBarIndicator {...testMocks.props} />);
  });

  describe('renderProgress()', () => {
    it('should render the in progress state', () => {
      expect(wrapper.instance().renderProgress()).toMatchSnapshot();
    });

    it('should render the completed state', () => {
      wrapper.setProps({
        inProgress: false,
        totalProcessed: testMocks.props.totalSelected,
      });
      expect(wrapper.instance().renderProgress()).toMatchSnapshot();
    });
  });

  describe('renderDetails()', () => {
    it('should not render anything if no success or failure', () => {
      expect(wrapper.instance().renderDetails()).toMatchSnapshot();
    });

    it('should render the succeeded number if only success', () => {
      wrapper.setProps({
        counter: 5,
        totalProcessed: 5,
      });
      expect(wrapper.instance().renderDetails()).toMatchSnapshot();
    });

    it('should render the failed number if only failure', () => {
      wrapper.setProps({
        errors: ['error 1', 'error 2', 'error 3'],
        totalProcessed: 5,
      });
      expect(wrapper.instance().renderDetails()).toMatchSnapshot();
    });

    it('should render both if success and failure count', () => {
      wrapper.setProps({
        counter: 5,
        errors: ['error 1', 'error 2', 'error 3'],
        totalProcessed: 8,
      });
      expect(wrapper.instance().renderDetails()).toMatchSnapshot();
    });
  });

  describe('render()', () => {
    it('should render as expected if in progress', () => {
      wrapper.setProps({
        counter: 5,
        totalProcessed: 5,
        isProgress: true,
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render as expected if completed', () => {
      wrapper.setProps({
        counter: 7,
        errors: ['error 1', 'error 2', 'error 3'],
        totalProcessed: 10,
        inProgress: false,
      });
      expect(wrapper).toMatchSnapshot();
    });
  });
});
