import React from 'react';
import {
  fireEvent,
  getByText,
  queryAllByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import UserMenu from './UserMenu';

describe('<UserMenu />', () => {
  it('should render the user menu icon', () => {
    const { container } = render(
      <UserMenu
        username="username"
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    expect(container.querySelector('[icon="user"]')).toBeVisible();
  });

  it('should render the user menu as first and last name credentials', () => {
    render(
      <UserMenu
        username="username"
        firstName="First"
        lastName="Last"
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    expect(screen.getByText('FL')).toBeVisible();
  });

  it('should render the menu as expected when just username', async () => {
    const { container } = render(
      <UserMenu
        username="username"
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(container.querySelector('[icon="user"]') as HTMLElement);

    await waitFor(() => expect(getByText(document.body, '(username)')).toBeVisible());
    expect(getByText(document.body, 'Change Password')).toBeVisible();
    expect(getByText(document.body, 'Help')).toBeVisible();
    expect(getByText(document.body, 'About')).toBeVisible();
    expect(getByText(document.body, 'Logout')).toBeVisible();
  });

  it('should render the menu as expected when first name, last name and username', async () => {
    render(
      <UserMenu
        username="username"
        firstName="First"
        lastName="Last"
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(screen.getByText('FL'));

    await waitFor(() => expect(getByText(document.body, 'First Last')).toBeVisible());
    expect(getByText(document.body, '(username)')).toBeVisible();
    expect(getByText(document.body, 'Change Password')).toBeVisible();
    expect(getByText(document.body, 'Help')).toBeVisible();
    expect(getByText(document.body, 'About')).toBeVisible();
    expect(getByText(document.body, 'Logout')).toBeVisible();
  });

  it('should call passwordUpdate on clicking in Change Password', async () => {
    const passwordUpdate = jest.fn();
    const { container } = render(
      <UserMenu
        username="username"
        onLogout={jest.fn()}
        passwordUpdate={passwordUpdate}
      />,
    );

    fireEvent.click(container.querySelector('[icon="user"]') as HTMLElement);
    await waitFor(() => expect(getByText(document.body, 'Change Password')).toBeVisible());

    fireEvent.click(getByText(document.body, 'Change Password'));
    expect(passwordUpdate).toHaveBeenCalled();
  });

  it('should show and close the About popup when clicking in About', async () => {
    const { container } = render(
      <UserMenu
        username="username"
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(container.querySelector('[icon="user"]') as HTMLElement);
    await waitFor(() => expect(getByText(document.body, 'About')).toBeVisible());

    fireEvent.click(getByText(document.body, 'About'));
    await waitFor(() => expect(getByText(document.body, 'About Discovery Xcipio')).toBeVisible());

    fireEvent.click(getByText(document.body, 'Ok'));
    await waitFor(() => expect(queryAllByText(document.body, 'About Discovery Xcipio')).toHaveLength(0));
  });

  it('should call onLogout on clicking in Logout', async () => {
    const onLogout = jest.fn();
    const { container } = render(
      <UserMenu
        username="username"
        onLogout={onLogout}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(container.querySelector('[icon="user"]') as HTMLElement);
    await waitFor(() => expect(getByText(document.body, 'Logout')).toBeVisible());

    fireEvent.click(getByText(document.body, 'Logout'));
    expect(onLogout).toHaveBeenCalled();
  });
});
