import React, {
  FunctionComponent,
  ReactElement,
  useState,
} from 'react';
import {
  Popover,
  Position,
} from '@blueprintjs/core';
import UserMenuContent from './components/UserMenuContent';
import About from './components/About';
import UserMenuIcon from './components/UserMenuIcon';

type PreferenceProps = {
  username: string
  firstName?: string
  lastName?: string
  passwordUpdate: () => void
  onLogout: () => void
}

const UserMenu: FunctionComponent<PreferenceProps> = ({
  username,
  firstName,
  lastName,
  passwordUpdate,
  onLogout,
}: PreferenceProps): ReactElement => {
  const [isAboutOpen, setAboutOpen] = useState(false);

  const handleAbout = () => setAboutOpen(true);
  const handleClose = () => setAboutOpen(false);

  return (
    <div data-testid="UserMenu">
      <Popover
        inheritDarkTheme={false}
        content={(
          <UserMenuContent
            username={username}
            firstName={firstName}
            lastName={lastName}
            passwordUpdate={passwordUpdate}
            onLogout={onLogout}
            onAboutClick={handleAbout}
          />
        )}
        position={Position.BOTTOM}
      >
        <UserMenuIcon firstName={firstName} lastName={lastName} />
      </Popover>
      <About isOpen={isAboutOpen} onClose={handleClose} />
    </div>
  );
};

export default UserMenu;
