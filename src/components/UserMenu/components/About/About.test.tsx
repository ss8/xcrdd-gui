import { fireEvent, getByText, render } from '@testing-library/react';
import React from 'react';
import About from './About';

describe('<About />', () => {
  it('should render the about popup as expected when open', () => {
    render(<About isOpen onClose={jest.fn()} />);

    expect(document.body).toMatchInlineSnapshot(`
      <body
        class="bp3-overlay-open"
      >
        <div />
        <div
          class="bp3-portal"
        >
          <div
            class="bp3-overlay bp3-overlay-open bp3-overlay-scroll-container"
          >
            <div
              class="bp3-overlay-backdrop bp3-overlay-appear bp3-overlay-appear-active"
              tabindex="0"
            />
            <div
              class="bp3-dialog-container bp3-overlay-content bp3-overlay-appear bp3-overlay-appear-active"
              tabindex="0"
            >
              <div
                class="bp3-dialog"
                style="width: 400px;"
              >
                <div
                  class="bp3-dialog-header"
                >
                  <h4
                    class="bp3-heading"
                  >
                    About Discovery Xcipio
                  </h4>
                  <button
                    aria-label="Close"
                    class="bp3-button bp3-minimal bp3-dialog-close-button"
                    type="button"
                  >
                    <span
                      class="bp3-icon bp3-icon-small-cross"
                      icon="small-cross"
                    >
                      <svg
                        data-icon="small-cross"
                        height="20"
                        viewBox="0 0 20 20"
                        width="20"
                      >
                        <desc>
                          small-cross
                        </desc>
                        <path
                          d="M11.41 10l3.29-3.29c.19-.18.3-.43.3-.71a1.003 1.003 0 00-1.71-.71L10 8.59l-3.29-3.3a1.003 1.003 0 00-1.42 1.42L8.59 10 5.3 13.29c-.19.18-.3.43-.3.71a1.003 1.003 0 001.71.71l3.29-3.3 3.29 3.29c.18.19.43.3.71.3a1.003 1.003 0 00.71-1.71L11.41 10z"
                          fill-rule="evenodd"
                        />
                      </svg>
                    </span>
                  </button>
                </div>
                <div
                  class="bp3-dialog-body"
                >
                  <div
                    style="margin-bottom: 10px;"
                  >
                    <label
                      class="bp3-label"
                      style="font-size: large;"
                    >
                      SS8 Discovery Xcipio
                    </label>
                    <label
                      class="bp3-label"
                    >
                      Version: 
                      discovery_xcipio_build_version
                    </label>
                    <label
                      class="bp3-label"
                    >
                      Date: 
                      discovery_xcipio_build_date
                    </label>
                  </div>
                  <label
                    class="bp3-label"
                  >
                    (c) 
                    copy_right_year
                     
                    SS8 Networks, Inc. All rights reserved
                  </label>
                  <label
                    class="bp3-label"
                  >
                    Home page:
                    <a
                      href="http://www.ss8.com"
                    >
                       
                      http://www.ss8.com
                       
                    </a>
                  </label>
                </div>
                <div
                  class="bp3-dialog-footer-actions"
                  style="padding-right: 20px;"
                >
                  <button
                    class="bp3-button bp3-intent-primary"
                    type="button"
                  >
                    <span
                      class="bp3-button-text"
                    >
                      Ok
                    </span>
                    <span
                      class="bp3-icon bp3-icon-tick"
                      icon="tick"
                    >
                      <svg
                        data-icon="tick"
                        height="16"
                        viewBox="0 0 16 16"
                        width="16"
                      >
                        <desc>
                          tick
                        </desc>
                        <path
                          d="M14 3c-.28 0-.53.11-.71.29L6 10.59l-3.29-3.3a1.003 1.003 0 00-1.42 1.42l4 4c.18.18.43.29.71.29s.53-.11.71-.29l8-8A1.003 1.003 0 0014 3z"
                          fill-rule="evenodd"
                        />
                      </svg>
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </body>
    `);
  });

  it('should render the about popup as expected when not open', () => {
    render(<About isOpen={false} onClose={jest.fn()} />);

    expect(document.body).toMatchInlineSnapshot(`
      <body
        class=""
      >
        <div />
      </body>
    `);
  });

  it('should call onClose callback when clicking to close', () => {
    const onClose = jest.fn();

    render(<About isOpen onClose={onClose} />);

    fireEvent.click(getByText(document.body, 'Ok'));
    expect(onClose).toHaveBeenCalled();
  });
});
