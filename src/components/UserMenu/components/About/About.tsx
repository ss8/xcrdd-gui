import React, { FunctionComponent, ReactElement } from 'react';
import {
  Button, Dialog, Classes, Label,
} from '@blueprintjs/core';
import aboutData from 'setup/about-data.json';

type AboutProps = {
  isOpen: boolean
  onClose: () => void
}

const About: FunctionComponent<AboutProps> = ({
  isOpen,
  onClose,
}: AboutProps): ReactElement => {
  // licensedProducts should be gotten from server. but hardcoded for now.
  // Example: licensedProducts = ['discovery_xcipio', 'fusion']
  const licensedProducts = ['discovery_xcipio'];

  const aboutContent = licensedProducts.map((licensed:string) => {
    const { productName, version, date } = (aboutData.products as any)[licensed];
    return (
      <div key={productName} style={{ marginBottom: '10px' }}>
        <Label style={{ fontSize: 'large' }}>{productName}</Label>
        <Label>Version: {version}</Label>
        <Label>Date: {date}</Label>
      </div>
    );
  });

  const dialogTitle = 'About Discovery Xcipio';

  return (
    <Dialog
      onClose={onClose}
      isOpen={isOpen}
      style={{ width: '400px' }}
      title={dialogTitle}
    >
      <div className={Classes.DIALOG_BODY}>
        {aboutContent}
        <Label>(c) {aboutData.ss8.copyRightYear} {aboutData.ss8.copyRightText}</Label>
        <Label>Home page:<a href={`${aboutData.ss8.homePage}`}> {aboutData.ss8.homePage} </a></Label>
      </div>
      <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px' }}>
        <Button onClick={onClose} rightIcon="tick" intent="primary" text="Ok" />
      </div>
    </Dialog>
  );
};

export default About;
