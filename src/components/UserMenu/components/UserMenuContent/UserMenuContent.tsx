import React, {
  Fragment,
  FunctionComponent,
  ReactElement,
} from 'react';
import {
  Menu,
  MenuItem,
  MenuDivider,
} from '@blueprintjs/core';
import './user-menu-content.scss';

type UserMenuContentProps = {
  firstName?: string
  lastName?: string
  username: string
  passwordUpdate: () => void
  onLogout: () => void
  onAboutClick: () => void
}

const UserMenuContent: FunctionComponent<UserMenuContentProps> = ({
  firstName,
  lastName,
  username,
  passwordUpdate,
  onLogout,
  onAboutClick,
}: UserMenuContentProps): ReactElement => {
  const getTitle = () => {
    const name = [firstName ?? '', lastName ?? ''].join(' ').trim();

    return (
      <Fragment>
        {name}{' '}<span className="user-menu-content-username">({username})</span>
      </Fragment>
    );
  };

  return (
    <Menu>
      <MenuDivider title={getTitle()} />
      <MenuDivider />
      <MenuItem icon="key" text="Change Password" onClick={passwordUpdate} />
      <MenuItem icon="help" text="Help" href={`${process.env.PUBLIC_URL}/help/index.html`} target="_blank" />
      <MenuItem icon="info-sign" text="About" onClick={onAboutClick} />
      <MenuItem
        icon="log-out"
        text="Logout"
        onClick={onLogout}
      />
    </Menu>
  );
};

export default UserMenuContent;
