import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import UserMenuContent from './UserMenuContent';

describe('<UserMenuContent />', () => {
  it('should render the menu as expected', () => {
    render(
      <UserMenuContent
        username="username"
        onAboutClick={jest.fn()}
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    expect(screen.getByText('(username)')).toBeVisible();
    expect(screen.getByText('Change Password')).toBeVisible();
    expect(screen.getByText('Help')).toBeVisible();
    expect(screen.getByText('About')).toBeVisible();
    expect(screen.getByText('Logout')).toBeVisible();
  });

  it('should render the menu as expected when there is first and last name', () => {
    render(
      <UserMenuContent
        username="username"
        firstName="First"
        lastName="Last"
        onAboutClick={jest.fn()}
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    expect(screen.getByText('First Last')).toBeVisible();
    expect(screen.getByText('(username)')).toBeVisible();
    expect(screen.getByText('Change Password')).toBeVisible();
    expect(screen.getByText('Help')).toBeVisible();
    expect(screen.getByText('About')).toBeVisible();
    expect(screen.getByText('Logout')).toBeVisible();
  });

  it('should call passwordUpdate on clicking in Change Password', () => {
    const passwordUpdate = jest.fn();
    render(
      <UserMenuContent
        username="username"
        onAboutClick={jest.fn()}
        onLogout={jest.fn()}
        passwordUpdate={passwordUpdate}
      />,
    );

    fireEvent.click(screen.getByText('Change Password'));
    expect(passwordUpdate).toHaveBeenCalled();
  });

  it('should call onAboutClick on clicking in About', () => {
    const onAboutClick = jest.fn();
    render(
      <UserMenuContent
        username="username"
        onAboutClick={onAboutClick}
        onLogout={jest.fn()}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(screen.getByText('About'));
    expect(onAboutClick).toHaveBeenCalled();
  });

  it('should call onLogout on clicking in Logout', () => {
    const onLogout = jest.fn();
    render(
      <UserMenuContent
        username="username"
        onAboutClick={jest.fn()}
        onLogout={onLogout}
        passwordUpdate={jest.fn()}
      />,
    );

    fireEvent.click(screen.getByText('Logout'));
    expect(onLogout).toHaveBeenCalled();
  });
});
