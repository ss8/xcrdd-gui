import React, {
  FunctionComponent,
  ReactElement,
} from 'react';
import {
  Button,
  IconName,
  MaybeElement,
} from '@blueprintjs/core';
import './user-menu-icon.scss';
import { getNameInitials } from './getNameInitials';

type UserMenuIconProps = {
  firstName?: string
  lastName?: string
}

const UserMenuIcon: FunctionComponent<UserMenuIconProps> = ({
  firstName,
  lastName,
}: UserMenuIconProps): ReactElement => {
  const nameInitials = getNameInitials(firstName, lastName);

  let icon: IconName | MaybeElement = 'user';
  if (nameInitials) {
    icon = <span className="user-menu-icon-initials">{nameInitials}</span>;
  }

  return (
    <Button className="user-menu-icon" minimal icon={icon} />
  );
};

export default UserMenuIcon;
