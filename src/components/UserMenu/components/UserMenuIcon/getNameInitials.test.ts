import { getNameInitials } from './getNameInitials';

describe('getNameInitials()', () => {
  it('should return the initials for the following cases', () => {
    expect(getNameInitials()).toEqual('');
    expect(getNameInitials('First')).toEqual('F');
    expect(getNameInitials('', 'Last')).toEqual('L');
    expect(getNameInitials('First', 'Last')).toEqual('FL');
    expect(getNameInitials('First', 'Some Last')).toEqual('FL');
    expect(getNameInitials('  First  Any ', '  Some   Last  ')).toEqual('FL');
  });
});
