// eslint-disable-next-line import/prefer-default-export
export function getNameInitials(firstName?: string, lastName?: string): string {
  const nameSplit = [firstName ?? '', lastName ?? ''].join(' ').trim().split(' ');

  let initials = nameSplit.shift()?.charAt(0).toLocaleUpperCase() ?? '';
  initials += nameSplit.pop()?.charAt(0).toLocaleUpperCase() ?? '';

  return initials;
}
