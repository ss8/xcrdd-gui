import React, { FunctionComponent, Fragment } from 'react';
import { Button } from '@blueprintjs/core';

type ClickCallback =
((event: React.MouseEvent<HTMLElement, MouseEvent>) => void) &
((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void);

type Props = {
  isEditing: boolean,
  onClickCancel: ClickCallback
  onClickSave?: ClickCallback
  onClickEdit?: ClickCallback
  onClickClose: ClickCallback
  onClickSaveAndCopy?: ClickCallback
  onClickCopy?: ClickCallback
  onClickDelete?: ClickCallback
}

export const FormButtons: FunctionComponent<Props> = ({
  isEditing,
  onClickSave,
  onClickCancel,
  onClickEdit,
  onClickClose,
  onClickSaveAndCopy,
  onClickCopy,
  onClickDelete,
}: Props) => {

  const renderSaveAndCopyButton = () => {
    if (!onClickSaveAndCopy) {
      return null;
    }

    return (
      <Button
        data-testid="FormButtonsSaveAndCopy"
        className="actionButton"
        text="Save and Copy"
        intent="none"
        onClick={onClickSaveAndCopy}
      />
    );
  };

  const renderButtons = () => {
    if (isEditing) {
      return (
        <Fragment>
          <Button
            data-testid="FormButtonsCancel"
            className="actionButton"
            text="Cancel"
            intent="none"
            onClick={onClickCancel}
          />
          {renderSaveAndCopyButton()}
          <Button
            data-testid="FormButtonsSave"
            className="actionButton"
            text="Save"
            intent="primary"
            onClick={onClickSave}
            disabled={onClickSave == null}
          />
        </Fragment>
      );
    }

    return (
      <Fragment>

        {onClickDelete != null && (
          <Button
            data-testid="FormButtonsDelete"
            className="actionButton"
            text="Delete"
            intent="none"
            onClick={onClickDelete}
          />
        )}
        {onClickCopy != null && (
          <Button
            data-testid="FormButtonsCopy"
            className="actionButton"
            text="Copy"
            intent="none"
            onClick={onClickCopy}
          />
        )}

        <Button
          data-testid="FormButtonsEdit"
          className="actionButton"
          text="Edit"
          intent="none"
          onClick={onClickEdit}
        />
        <Button
          data-testid="FormButtonsBack"
          className="actionButton"
          text="Back"
          intent="primary"
          onClick={onClickClose}
        />
      </Fragment>
    );
  };

  return (
    <Fragment>
      {renderButtons()}
    </Fragment>
  );
};

export default FormButtons;
