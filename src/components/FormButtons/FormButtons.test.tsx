import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import FormButtons from './FormButtons';

describe('<FormButtons />', () => {
  let wrapper: ReactWrapper;
  let mockCallback: jest.Mock;

  beforeEach(() => {
    mockCallback = jest.fn();
  });

  it('should render the Cancel and Save buttons if editing', () => {
    wrapper = mount(
      <FormButtons
        isEditing
        onClickCancel={() => false}
        onClickSave={() => false}
        onClickEdit={() => false}
        onClickClose={() => false}
      />,
    );

    expect(wrapper.find({ text: 'Cancel' })).toHaveLength(1);
    expect(wrapper.find({ text: 'Save' })).toHaveLength(1);
  });

  it('should render the Edit and Close buttons if viewing', () => {
    wrapper = mount(
      <FormButtons
        isEditing={false}
        onClickCancel={() => false}
        onClickSave={() => false}
        onClickEdit={() => false}
        onClickClose={() => false}
      />,
    );

    expect(wrapper.find({ text: 'Edit' })).toHaveLength(1);
    expect(wrapper.find({ text: 'Back' })).toHaveLength(1);
  });

  it('should render the Save button as disabled if callback not defined', () => {
    wrapper = mount(
      <FormButtons
        isEditing
        onClickCancel={() => false}
        onClickClose={() => false}
      />,
    );

    expect(wrapper.find({ text: 'Cancel' })).toHaveLength(1);
    expect(wrapper.find({ text: 'Save' })).toHaveLength(1);
    expect(wrapper.find({ text: 'Save' }).prop('disabled')).toBe(true);
  });

  it('should call onClickCancel if clicking in Cancel', () => {
    wrapper = mount(
      <FormButtons
        isEditing
        onClickCancel={mockCallback}
        onClickSave={() => false}
        onClickEdit={() => false}
        onClickClose={() => false}
      />,
    );
    const onClick = wrapper.find({ text: 'Cancel' }).prop('onClick');
    onClick({} as React.MouseEvent);
    expect(mockCallback).toBeCalledTimes(1);
  });

  it('should call onClickSave if clicking in Save', () => {
    wrapper = mount(
      <FormButtons
        isEditing
        onClickCancel={() => false}
        onClickSave={mockCallback}
        onClickEdit={() => false}
        onClickClose={() => false}
      />,
    );
    const onClick = wrapper.find({ text: 'Save' }).prop('onClick');
    onClick({} as React.MouseEvent);
    expect(mockCallback).toBeCalledTimes(1);
  });

  it('should call onClickEdit if clicking in Edit', () => {
    wrapper = mount(
      <FormButtons
        isEditing={false}
        onClickCancel={() => false}
        onClickSave={() => false}
        onClickEdit={mockCallback}
        onClickClose={() => false}
      />,
    );
    const onClick = wrapper.find({ text: 'Edit' }).prop('onClick');
    onClick({} as React.MouseEvent);
    expect(mockCallback).toBeCalledTimes(1);
  });

  it('should call onClickClose if clicking in Close', () => {
    wrapper = mount(
      <FormButtons
        isEditing={false}
        onClickCancel={() => false}
        onClickSave={() => false}
        onClickEdit={() => false}
        onClickClose={mockCallback}
      />,
    );
    const onClick = wrapper.find({ text: 'Back' }).prop('onClick');
    onClick({} as React.MouseEvent);
    expect(mockCallback).toBeCalledTimes(1);
  });

  it('should call onClickSaveAndCopy if clicking in Save and Copy', () => {
    wrapper = mount(
      <FormButtons
        isEditing
        onClickCancel={() => false}
        onClickSave={() => false}
        onClickEdit={() => false}
        onClickClose={() => false}
        onClickSaveAndCopy={mockCallback}
      />,
    );
    const onClick = wrapper.find({ text: 'Save and Copy' }).prop('onClick');
    onClick({} as React.MouseEvent);
    expect(mockCallback).toBeCalledTimes(1);
  });
});
