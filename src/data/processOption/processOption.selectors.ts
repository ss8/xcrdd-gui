import { RootState } from '../root.reducers';
import { ProcessOption } from './processOption.types';

// eslint-disable-next-line import/prefer-default-export
export const getProcessOptions = (state: RootState): ProcessOption[] => state.processOption.entities;
