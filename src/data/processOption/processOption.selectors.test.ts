import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import {
  getProcessOptions,
} from './processOption.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './processOption.types';

describe('processOption.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of processOptions', () => {
    const action: types.ProcessOptionActionTypes = {
      type: types.GET_PROCESS_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockProcessOptions),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getProcessOptions(store.getState())).toEqual(mockProcessOptions);
  });
});
