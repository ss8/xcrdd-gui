import * as api from './processOption.api';
import { GET_PROCESS_OPTIONS, GetProcessOptionAction } from './processOption.types';

// eslint-disable-next-line import/prefer-default-export
export function getProcessOptions(deliveryFunctionId: string): GetProcessOptionAction {
  return {
    type: GET_PROCESS_OPTIONS,
    payload: api.getProcessOptions(deliveryFunctionId),
  };
}
