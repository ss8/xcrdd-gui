import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  ProcessOption,
  API_PATH_PROCESS_OPTIONS,
} from './processOption.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getProcessOptions(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<ProcessOption>> {
  return http.get(`${API_PATH_PROCESS_OPTIONS}/${deliveryFunctionId}`);
}
