import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './processOption.actions';
import * as types from './processOption.types';
import * as api from './processOption.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('processOption.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the processOptions', () => {
    const expectedAction = {
      type: types.GET_PROCESS_OPTIONS,
      payload: api.getProcessOptions('0'),
    };

    expect(actions.getProcessOptions('0')).toEqual(expectedAction);
  });
});
