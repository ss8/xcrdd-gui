import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_PROCESS_OPTIONS = '/process-options';

export const GET_PROCESS_OPTIONS = 'GET_PROCESS_OPTIONS';
export const GET_PROCESS_OPTIONS_PENDING = 'GET_PROCESS_OPTIONS_PENDING';
export const GET_PROCESS_OPTIONS_FULFILLED = 'GET_PROCESS_OPTIONS_FULFILLED';
export const GET_PROCESS_OPTIONS_REJECTED = 'GET_PROCESS_OPTIONS_REJECTED';

export interface ProcessOption {
  id: string
  procName: string
  paramName: string
  paramValue: string
  comments: string
}

export interface ProcessOptionState {
  entities: ProcessOption[]
}

export interface GetProcessOptionAction extends AnyAction {
  type: typeof GET_PROCESS_OPTIONS
  payload: AxiosPromise<ServerResponse<ProcessOption>>
}

export interface GetProcessOptionPendingAction extends AnyAction {
  type: typeof GET_PROCESS_OPTIONS_PENDING
  payload: AxiosResponse<ServerResponse<ProcessOption>>
}

export interface GetProcessOptionFulfilledAction extends AnyAction {
  type: typeof GET_PROCESS_OPTIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<ProcessOption>>
}

export interface GetProcessOptionRejectedAction extends AnyAction {
  type: typeof GET_PROCESS_OPTIONS_REJECTED
  payload: AxiosResponse<ServerResponse<ProcessOption>>
}

export type ProcessOptionActionTypes =
  GetProcessOptionAction |
  GetProcessOptionPendingAction |
  GetProcessOptionFulfilledAction |
  GetProcessOptionRejectedAction;
