import {
  GET_PROCESS_OPTIONS_FULFILLED,
  ProcessOptionActionTypes,
  ProcessOptionState,
} from './processOption.types';

const initialState: ProcessOptionState = {
  entities: [],
};

export default function processOptionReducer(
  state = initialState,
  action: ProcessOptionActionTypes,
): ProcessOptionState {
  switch (action.type) {
    case GET_PROCESS_OPTIONS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
