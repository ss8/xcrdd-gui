import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './processOption.api';
import { API_PATH_PROCESS_OPTIONS } from './processOption.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('processOption.api', () => {
  it('should request the list for processOptions', async () => {
    const response = buildAxiosServerResponse(mockProcessOptions);
    axiosMock.onGet(`${API_PATH_PROCESS_OPTIONS}/0`).reply(200, response);
    await expect(api.getProcessOptions('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_PROCESS_OPTIONS}/0` },
      status: 200,
    });
  });
});
