import { mockProcessOptions } from '__test__/mockProcessOptions';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './processOption.reducer';
import * as types from './processOption.types';

describe('processOption.reducer', () => {
  it('should handle GET_PROCESS_OPTIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_PROCESS_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockProcessOptions),
    })).toEqual({
      entities: mockProcessOptions,
    });
  });
});
