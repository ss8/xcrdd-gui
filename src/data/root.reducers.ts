import { combineReducers } from 'redux';
import dashboardReducer from '../dashboard/dashboard-reducer';
import authenticationReducer from './authentication/authentication.reducer';
import warrantsReducer from '../xcipio/warrants/warrants-reducer';
import usersReducer from '../users/users-reducer';
import teamsReducer from '../teams/teams-reducer';
import roleReducer from '../roles/roles-reducer';
import policiesReducer from '../policies/policy-reducer';
import auditReducer from '../audit/general-audit/audit-reducer';
import userAuditReducer from '../audit/user-audit/user-audit-reducer';
import accessFunctionReducer from './accessFunction/accessFunction.reducer';
import collectionFunctionReducer from './collectionFunction/collectionFunction.reducer';
import interceptsReducer from '../xcipio/intercepts/intercept-reducer';
import casesReducer from '../xcipio/cases/case-reducer';
import targetReducer from '../xcipio/targets/targets-reducer';
import attachmentsReducer from '../xcipio/attachments/attachments-reducer';
import wiretapsReducer from '../xcipio/wiretaps/wiretap-reducer';
import contactsReducer from '../xcipio/contacts/contacts-reducer';
import distributorConfigurationReducer from './distributorConfiguration/distributorConfiguration.reducer';
import globalReducer from '../global/global-reducer';
import discoveryXcipioReducer from '../xcipio/discovery-xcipio-reducer';
import distributorReducer from './distributor/distributor.reducer';
import nodesReducer from './nodes/nodes.reducer';
import networkReducer from './network/network.reducer';
import securityInfoReducer from './securityInfo/securityInfo.reducer';
import sshSecurityInfoReducer from './sshSecurityInfo/sshSecurityInfo.reducer';
import ftpInfoReducer from './ftpInfo/ftpInfo.reducer';
import locationReducer from './location/location.reducer';
import tlsProxyReducer from './tlsProxy/tlsProxy.reducer';
import ss8ProxyDefaultReducer from './ss8ProxyDefault/ss8ProxyDefault.reducer';
import fqdnGroupReducer from './fqdnGroup/fqdnGroup.reducer';
import vpnConfigReducer from './vpnConfig/vpnConfig.reducer';
import vpnConfigTemplateReducer from './vpnTemplate/vpnTemplate.reducer';
import xtpReducer from './xtp/xtp.reducer';
import xdpReducer from './xdp/xdp.reducer';
import processOptionReducer from './processOption/processOption.reducer';
import alertNotificationReducer from './alertNotification/alertNotification.reducer';
import xcrddSettingsReducer from './xcrddSettings/settings.reducer';
import xcrddLEAReducer from './xcrddLea/lea.reducer';
import xcrddDataSourcesReducer from './xcrddDataSource/dataSource.reducer';

const rootReducer = combineReducers({
  dashboard: dashboardReducer,
  authentication: authenticationReducer,
  warrants: warrantsReducer,
  users: usersReducer,
  teams: teamsReducer,
  roles: roleReducer,
  policies: policiesReducer,
  audit: auditReducer,
  userAudit: userAuditReducer,
  afs: accessFunctionReducer,
  cfs: collectionFunctionReducer,
  intercepts: interceptsReducer,
  discoveryXcipio: discoveryXcipioReducer,
  cases: casesReducer,
  targets: targetReducer,
  attachments: attachmentsReducer,
  wiretaps: wiretapsReducer,
  contacts: contactsReducer,
  distributorConfiguration: distributorConfigurationReducer,
  global: globalReducer,
  distributor: distributorReducer,
  nodes: nodesReducer,
  network: networkReducer,
  securityInfo: securityInfoReducer,
  sshSecurityInfo: sshSecurityInfoReducer,
  ftpInfo: ftpInfoReducer,
  vpnConfig: vpnConfigReducer,
  fqdnGroup: fqdnGroupReducer,
  vpnConfigTemplate: vpnConfigTemplateReducer,
  location: locationReducer,
  tlsProxy: tlsProxyReducer,
  ss8ProxyDefault: ss8ProxyDefaultReducer,
  xtp: xtpReducer,
  xdp: xdpReducer,
  processOption: processOptionReducer,
  alertNotification: alertNotificationReducer,
  xcrddSettings: xcrddSettingsReducer,
  xcrddLea: xcrddLEAReducer,
  xcrddDataSources: xcrddDataSourcesReducer,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>
