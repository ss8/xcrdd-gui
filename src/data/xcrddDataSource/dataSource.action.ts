import Http from 'shared/http';
import { GET_LIINTERCEPT, SELECT_LIITERCEPT } from './dataSource.types';

const http = Http.getHttp();

export function getSelectedLIIntercept(liintercept:any):any {
  return {
    type: SELECT_LIITERCEPT,
    payload: liintercept,
  };
}

export const getLIIntercept = (body = ''):any => ({ type: GET_LIINTERCEPT, payload: http.get(`/liintercept/${body}`) });
