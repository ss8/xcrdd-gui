import {
  GET_LIINTERCEPT_FULFILLED, SELECT_LIITERCEPT,
} from './dataSource.types';

const initilaState:any = {
  liIntercepts: [],
  selected: [],
};

const xcrddDataSourcesReducer = (state = initilaState, action:any) => {
  switch (action.type) {
    case GET_LIINTERCEPT_FULFILLED:
      return { ...state, liIntercepts: action.payload.data.data };
    case SELECT_LIITERCEPT:
      return { ...state, selected: action.payload || [] };
    default:
      return state;
  }
};

export default xcrddDataSourcesReducer;
