export type Permission = {
  id: number,
  name: string | null,
}

export type Role = {
  id?: number,
  name: string,
  description: string | null,
  dateCreated?: string,
  dateUpdated?: string,
  permissions?: Permission[],
  permissionLabels?: string[],
  isActive?: boolean,
  reporting?: string | null,
}
