import { Role } from 'data/role/role.types';

export type UserReporting = {
    assignedRoles? : Role [],
}

export type User = {
    id: string,
    userID: string,
    password: string,
    role: string,
    firstName: string,
    middleName: string | null,
    lastName: string,
    displayName: string,
    office: string,
    dept: string,
    email: string,
    phone: string,
    state: 'pending' | 'active' | 'locked' | 'new-password' | 'deactivated',
    superior: number,
    datecreated: string,
    dateupdated: string,
    lastlogin: string | null,
    lastLoginAddress: string | null,
    lastPasswordChange: string | null,
    lastLoginMessageSeen: string | null,
    agreements: number,
    reporting: UserReporting,
}
