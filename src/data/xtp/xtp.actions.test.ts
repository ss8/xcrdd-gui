import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './xtp.actions';
import * as types from './xtp.types';
import * as api from './xtp.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('xtp.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the xtps', () => {
    const expectedAction = {
      type: types.GET_XTPS,
      payload: api.getXtps('0'),
    };

    expect(actions.getXtps('0')).toEqual(expectedAction);
  });
});
