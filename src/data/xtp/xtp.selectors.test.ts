import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockXtps } from '__test__/mockXtps';
import {
  getXtps,
} from './xtp.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './xtp.types';

describe('xtp.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of xtps', () => {
    const action: types.XtpActionTypes = {
      type: types.GET_XTPS_FULFILLED,
      payload: buildAxiosServerResponse(mockXtps),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getXtps(store.getState())).toEqual(mockXtps);
  });
});
