import {
  GET_XTPS_FULFILLED,
  XtpActionTypes,
  XtpState,
} from './xtp.types';

const initialState: XtpState = {
  entities: [],
};

export default function xtpReducer(
  state = initialState,
  action: XtpActionTypes,
): XtpState {
  switch (action.type) {
    case GET_XTPS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
