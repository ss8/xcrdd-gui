import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockXtps } from '__test__/mockXtps';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './xtp.api';
import { API_PATH_XTPS } from './xtp.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('xtp.api', () => {
  it('should request the list for xtps', async () => {
    const response = buildAxiosServerResponse(mockXtps);
    axiosMock.onGet(`${API_PATH_XTPS}/0`).reply(200, response);
    await expect(api.getXtps('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_XTPS}/0` },
      status: 200,
    });
  });
});
