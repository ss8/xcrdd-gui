import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  Xtp,
  API_PATH_XTPS,
} from './xtp.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getXtps(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<Xtp>> {
  return http.get(`${API_PATH_XTPS}/${deliveryFunctionId}`);
}
