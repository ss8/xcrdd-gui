import { RootState } from '../root.reducers';
import { Xtp } from './xtp.types';

// eslint-disable-next-line import/prefer-default-export
export const getXtps = (state: RootState): Xtp[] => state.xtp.entities;
