import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_XTPS = '/xtps';

export const GET_XTPS = 'GET_XTPS';
export const GET_XTPS_PENDING = 'GET_XTPS_PENDING';
export const GET_XTPS_FULFILLED = 'GET_XTPS_FULFILLED';
export const GET_XTPS_REJECTED = 'GET_XTPS_REJECTED';

export interface Xtp {
  id: string
  lbId: string
  ipAddr: string
}

export interface XtpState {
  entities: Xtp[]
}

export interface GetXtpAction extends AnyAction {
  type: typeof GET_XTPS
  payload: AxiosPromise<ServerResponse<Xtp>>
}

export interface GetXtpPendingAction extends AnyAction {
  type: typeof GET_XTPS_PENDING
  payload: AxiosResponse<ServerResponse<Xtp>>
}
export interface GetXtpFulfilledAction extends AnyAction {
  type: typeof GET_XTPS_FULFILLED
  payload: AxiosResponse<ServerResponse<Xtp>>
}

export interface GetXtpRejectedAction extends AnyAction {
  type: typeof GET_XTPS_REJECTED
  payload: AxiosResponse<ServerResponse<Xtp>>
}

export type XtpActionTypes =
  GetXtpAction |
  GetXtpPendingAction |
  GetXtpFulfilledAction |
  GetXtpRejectedAction;
