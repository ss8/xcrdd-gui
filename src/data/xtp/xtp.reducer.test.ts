import { mockXtps } from '__test__/mockXtps';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './xtp.reducer';
import * as types from './xtp.types';

describe('xtp.reducer', () => {
  it('should handle GET_XTPS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_XTPS_FULFILLED,
      payload: buildAxiosServerResponse(mockXtps),
    })).toEqual({
      entities: mockXtps,
    });
  });
});
