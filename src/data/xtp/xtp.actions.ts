import * as api from './xtp.api';
import { GET_XTPS, GetXtpAction } from './xtp.types';

// eslint-disable-next-line import/prefer-default-export
export function getXtps(deliveryFunctionId: string): GetXtpAction {
  return {
    type: GET_XTPS,
    payload: api.getXtps(deliveryFunctionId),
  };
}
