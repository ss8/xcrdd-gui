export const GET_INSTANCE_CONFIGURATIONS = 'GET_INSTANCE_CONFIGURATIONS';
export const SET_RETENTION_CONFIGURATIONS = 'SET_RETENTION_CONFIGURATIONS';
export const GET_INSTANCE_CONFIGURATIONS_FULFILLED = 'GET_INSTANCE_CONFIGURATIONS_FULFILLED';
export const SET_RETENTION_CONFIGURATIONS_FULFILLED = 'SET_RETENTION_CONFIGURATIONS_FULFILLED';
export const GET_RDHI_CONFIGURATIONS = 'GET_RDHI_CONFIGURATIONS';
export const SET_RDHI_CONFIGURATIONS = 'SET_RDHI_CONFIGURATIONS';
export const GET_RDHI_CONFIGURATIONS_FULFILLED = 'GET_RDHI_CONFIGURATIONS_FULFILLED';
export const SET_RDHI_CONFIGURATIONS_FULFILLED = 'SET_RDHI_CONFIGURATIONS_FULFILLED';
export const SELECT_INGEST_LISTENER = 'SELECT_INGEST_LISTENER';
export const GET_INGEST_LISTENERS = 'GET_INGEST_LISTENERS';
export const GET_INGEST_LISTENERS_FULFILLED = 'GET_INGEST_LISTENERS_FULFILLED';

export interface instanceState {
    instanceId: number | null,
    name: string,
    provisioningServer: string,
    retentionTime: number | null,
    updateRetention?: {
      fetching:boolean,
      status:string
    },
    retentionTimeUnit: string,
    rdhiEnabled: boolean,
    diskCapDefaultLevel1:number
    diskCapUpdatedLevel1?:number|null
    diskCapDefaultLevel2:number
    diskCapUpdatedLevel2?:number|null
  }

export interface rdhiState{
  rdhiId ?:number|null
  serverName:string
  serverIP:string
  port:number
  pkFileName:string
  filepassword:string
  certFileName:string
  authorityName:string
  directory:string
}

export interface ingestListener{
  id: number
  ipAddress: string
  keepAliveRetries?: number
  keepAliveTimeout: number
  numberOfConnections?: number
  maxConcurrentConnection?:number
  numberOfParsers?:number
  port: number
  allowedIPAddresses:[]
  protocol: string
  secInfoId: string
  state: string
  status: string
  transport: string
  synchronizationHeader?:string
}

export interface ingestListeners{
  instanceId:number
  moduleName:string
  name:string
  nodeName:string
  listener:ingestListener
  destination:ingestListener
}

export interface defaultState{
    instanceConfig:instanceState
    rdhiConfig:rdhiState
    selected:[],
    ingestListeners:[]
}
