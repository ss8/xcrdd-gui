import { History } from 'history';
import Http from '../../shared/http';

import {
  GET_INSTANCE_CONFIGURATIONS,
  SET_RETENTION_CONFIGURATIONS,
  GET_RDHI_CONFIGURATIONS,
  SET_RDHI_CONFIGURATIONS,
  SELECT_INGEST_LISTENER,
  GET_INGEST_LISTENERS,
} from './settings.types';

const http = Http.getHttp();

export const getInstanceConfigurations = () => ({ type: GET_INSTANCE_CONFIGURATIONS, payload: http.get('/xcrddinstance') });
export const setRetentionConfiguration = (instanceId:number, body:any) => ({
  type: SET_RETENTION_CONFIGURATIONS, payload: http.put(`/xcrddinstance/${instanceId}`, body),
});

export const getRDHIConfigurations = () => ({ type: GET_RDHI_CONFIGURATIONS, payload: http.get('/rdhiserver') });
export const setRDHIConfiguration = (body:any) => ({
  type: SET_RDHI_CONFIGURATIONS, payload: http.put('/rdhiserver/update', body),
});

export const getIngestListenersList = ():any => ({ type: GET_INGEST_LISTENERS, payload: http.get('/ingestlistener') });

export function selectIngestListener(ingestListner:any[]): any {
  return {
    type: SELECT_INGEST_LISTENER,
    payload: ingestListner,
  };
}

export function goToIngestListenersList(history: History): () => void {
  return () => {
    history.push('/xcrdd/settings/ingest');
  };
}

export function goToIngestListenersView(history: History): () => void {
  return () => {
    history.push('/xcrdd/settings/ingest/view');
  };
}
