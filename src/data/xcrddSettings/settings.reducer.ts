import {
  GET_INSTANCE_CONFIGURATIONS_FULFILLED,
  GET_RDHI_CONFIGURATIONS_FULFILLED,
  SELECT_INGEST_LISTENER,
  GET_INGEST_LISTENERS_FULFILLED,
  defaultState,
} from './settings.types';

const initilaState:defaultState = {
  instanceConfig: {
    instanceId: null,
    name: '',
    provisioningServer: '',
    retentionTime: 30,
    retentionTimeUnit: 'DAYS',
    rdhiEnabled: false,
    diskCapDefaultLevel1: 0,
    diskCapDefaultLevel2: 0,
  },
  rdhiConfig: {
    rdhiId: null,
    serverName: '',
    serverIP: '',
    port: 0,
    pkFileName: '',
    filepassword: '',
    certFileName: '',
    authorityName: '',
    directory: '',
  },
  selected: [],
  ingestListeners: [],
};

const xcrddSettingsReducer = (state = initilaState, action:any) => {
  switch (action.type) {
    case GET_INSTANCE_CONFIGURATIONS_FULFILLED:
      return {
        ...state,
        instanceConfig: {
          ...action.payload.data.data[0],
        },
      };
      // remove diskCapDefaultLevel1, diskCapUpdatedLevel1,  diskCapDefaultLevel2 once API supports

    case GET_RDHI_CONFIGURATIONS_FULFILLED:
      return { ...state, rdhiConfig: action.payload.data.data[0] };

    case SELECT_INGEST_LISTENER:
      return { ...state, selected: action.payload || [] };

    case GET_INGEST_LISTENERS_FULFILLED:
      return { ...state, ingestListeners: action.payload.data.data };

    default:
      return state;
  }
};

export default xcrddSettingsReducer;
