import cloneDeep from 'lodash.clonedeep';
import { createSelector } from 'reselect';
import { RootState } from 'data/root.reducers';
import { CaseModel } from './case.types';
import { getTargetsById } from '../target/target.selectors';

export const getCases = (state: RootState): CaseModel[] => state.cases.cases;

/**
 * This function is going to replace the targets only if they are not already set.
 */
export const getCasesWithTargetsIfNotSet = createSelector(
  [getCases, getTargetsById],
  (cases, targetsById) => {
    const clonedCases = cloneDeep(cases);
    clonedCases.forEach((caseEntry) => {
      caseEntry.targets = caseEntry.targets.map((target) => {
        if (target.caseId == null && target.id != null && targetsById[target.id] != null) {
          return targetsById[target.id];
        }

        return target;
      });
    });

    return clonedCases;
  },
);
