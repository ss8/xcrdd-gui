import moment from 'moment-timezone';
import cloneDeep from 'lodash.clonedeep';
import { CaseModel } from './case.types';

/**
 * Fields from the warrantCase to convert to the timezone
 */
const WARRANT_DATE_TIME_FIELDS_TO_CONVERT = [
  'receivedDateTime',
  'startDateTime',
  'stopDateTime',
];

/**
 * Fields from the case to convert to the timezone.
 */
const CASE_DATE_TIME_FIELDS_TO_CONVERT = [
  'startDateTime',
  'stopDateTime',
];

/**
 * Fields from the audit to convert to the timezone.
 */
const AUDIT_DATE_TIME_FIELDS_TO_CONVERT = [
  'date1',
  'date2',
];

/**
 * Internal date format used when converting. Should not be used outside this class.
 */
const DATE_TIME_FORMAT_WITHOUT_TIMEZONE = 'YYYY-MM-DD HH:mm';

/**
 * Class responsible for adapting the warrantCase form data
 * before loading and before submitting.
 *
 * It has two separate parts, the first one is *ToLoad methods, which
 * are used before loading any data from the server. And *ToSubmit methods
 * which are used before submitting the date to the server.
 *
 * *ToLoad logic:
 * It will convert the UTC time which represents a date/time in the warrantCase timezone
 * to a UTC time which visually will represents the date/time in the computer timezone.
 * For instance.
 * Consider the following:
 * - computer timezone as America/Los_Angeles (GMT-7)
 * - warrantCase timezone as America/New_York (GTM-4)
 * - warrantCase startTime as '2020-07-29 16:00' in America/New_York time
 *
 * The UTC value for '2020-07-29 16:00' in America/New_York is 2020-07-29T20:00:00.000Z
 * This is converted to a UTC in computer timezone (America/Los_Angeles) that will be
 * displayed in the UI as '2020-07-29 16:00', which is 2020-07-29T23:00:00.000Z
 *
 * *toSubmit logic:
 * It is basically the same, but in this case it will convert the back the America/New_York
 * timezone.
 */
export default class CaseAdapter {
  /**
   * This function is going to ignore the timezone in the date and
   * use just the year, month, day, hour and minute to create a new
   * date in the specified timezone.
   *
   * @param date the date to convert
   * @param timezone the timezone to use
   * @return the new date in the specified timezone
   */
  static convertFromComputerTimezoneToGivenTimezone(date: string, timezone: string): string {
    const dateWithoutTimezoneInfo = moment(date).format(DATE_TIME_FORMAT_WITHOUT_TIMEZONE);
    const convertedDate = moment.tz(dateWithoutTimezoneInfo, timezone).toDate();
    return convertedDate.toISOString();
  }

  /**
   * This function is going to create the date using the timezone and
   * create a new date using just the year, month, day, hour and minute
   * to create a new date in the specified timezone.
   *
   * @param date the date to convert
   * @param timezone the timezone to use
   * @return the new date in the specified timezone
   */
  static convertFromGivenTimezoneToComputerTimezone(date: string, timezone: string): string {
    const dateWithoutTimezoneInfo = moment.tz(date, timezone).format(DATE_TIME_FORMAT_WITHOUT_TIMEZONE);
    const revertedDate = moment(dateWithoutTimezoneInfo).toDate();
    return revertedDate.toISOString();
  }

  static applyConvertFunctionToField(
    data: any,
    field: string,
    timezone: string,
    convert: CallableFunction,
  ): void {
    const dateTime = data[field];

    if (!timezone || !dateTime) {
      return;
    }

    data[field] = convert(dateTime, timezone);
  }

  static adaptCaseDateTimeFieldsToTimezone(warrantCase: any, convertFunction: CallableFunction): any {
    const {
      timeZone,
    } = warrantCase;

    WARRANT_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
      CaseAdapter.applyConvertFunctionToField(
        warrantCase, field, timeZone, convertFunction,
      );
    });

    return warrantCase;
  }

  static adaptCasesDateTimeFieldsToTimezone(cases: Array<any> = [], convertFunction: CallableFunction): Array<any> {
    cases.forEach((caseEntry: any) => {
      const { timeZone } = caseEntry;
      CASE_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
        CaseAdapter.applyConvertFunctionToField(
          caseEntry, field, timeZone, convertFunction,
        );
      });
    });

    return cases;
  }

  static adaptAuditDateTimeFieldsToTimezone(
    userAudit: any = {},
    timezone: string,
    convertFunction: CallableFunction,
  ): any {
    AUDIT_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
      CaseAdapter.applyConvertFunctionToField(
        userAudit, field, timezone, convertFunction,
      );
    });

    return userAudit;
  }

  /**
   * Use this function to adapt the warrantCase from the form and convert the
   * dates considering the warrantCase timezone.
   *
   * @param warrantCase warrantCase data
   * @return the adapted data
   */
  static adaptCaseDateTimeFieldsToSubmit(warrantCase: any): any {
    let warrantCaseClone = cloneDeep(warrantCase);
    const convertFunction = CaseAdapter.convertFromComputerTimezoneToGivenTimezone;
    warrantCaseClone = CaseAdapter.adaptCaseDateTimeFieldsToTimezone(warrantCaseClone, convertFunction);
    return warrantCaseClone;
  }

  static adaptCaseToSubmit(warrantCase: CaseModel): CaseModel {
    let warrantCaseClone = cloneDeep(warrantCase);
    warrantCaseClone = CaseAdapter.adaptCaseDateTimeFieldsToSubmit(warrantCaseClone);
    return warrantCaseClone;
  }

  /**
   * Use this function to adapt the user audit from the form and convert the date
   * considering the warrantCase timezone.
   *
   * @param userAudit the user audit data
   * @param timezone the timezone to use
   */
  static adaptAuditToSubmit(userAudit: any, timezone: string): any {
    let userAuditClone = cloneDeep(userAudit);
    const convertFunction = CaseAdapter.convertFromComputerTimezoneToGivenTimezone;
    userAuditClone = CaseAdapter.adaptAuditDateTimeFieldsToTimezone(userAuditClone, timezone, convertFunction);
    return userAuditClone;
  }

}
