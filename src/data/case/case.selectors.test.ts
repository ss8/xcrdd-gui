import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import rootReducer, { RootState } from 'data/root.reducers';
import * as caseActions from 'xcipio/cases/case-actions';
import * as targetActions from 'xcipio/targets/targets-actions';
import { mockCases } from '__test__/mockCases';
import { mockTargets } from '__test__/mockTargets';
import {
  getCases,
} from './case.selectors';

describe('case.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    const caseAction: AnyAction = {
      type: caseActions.GET_ALL_CASES_BY_CASEIDS_FULFILLED,
      payload: buildAxiosServerResponse(mockCases),
    };

    const targetAction: AnyAction = {
      type: targetActions.GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED,
      payload: buildAxiosServerResponse(mockTargets),
    };

    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, caseAction));
    store = mockStore(rootReducer(store.getState(), targetAction));
  });

  it('should return the cases', () => {
    expect(getCases(store.getState())).toEqual(mockCases);
  });
});
