import { CollectionFunction, HI2Interface, HI3Interface } from 'data/collectionFunction/collectionFunction.types';
import { Identity } from '../types';
import { AccessFunctionMap } from '../accessFunction/accessFunction.types';
import { TargetModel } from '../target/target.types';
import { FormTemplateOption } from '../template/template.types';

export const INTERCEPT_TYPE_CD = 'CD';

// Objects to handle all Referring variables for HI2/3 component
// TBD: Refactor HI2/3 component and will add more variables associated.
export type CaseInterfacesRefs = {
  dataOnly: boolean,
  casePHIR: boolean,
}

export interface CaseState {
  cases: CaseModel[],
  selectedIndex: number,
  error: string,
  selectedCases: CaseModel[],
  selectionLastCleared: any,
}

export type CaseModelMap = {
  [caseId: string]: CaseModel
}

export type CaseModel = {
  id: string
  targets: TargetModel[]
  name: string
  warrantId: Identity
  dxOwner: string
  dxAccess: string | null
  liId: string
  device: string
  services: string[]
  cfId: {
    id: string
    name: string
    cfId?: string
    tag?: string
  }
  coId?: string
  entryDateTime: string
  type?: string
  startDateTime: string
  stopDateTime: string
  timeZone: string
  signature?: string
  cfOptions?: FormTemplateOption[]
  options?: CaseOptions
  templateUsed?: string | null
  genccc?: {
    leaphone1?: string,
    leaphone2?: string
  } | null
  cfs?: CollectionFunction[]
  hi2Interfaces?: HI2Interface[] | null
  hi3Interfaces?: HI3Interface[] | null
  hi2Data?: HI2Interface[] | null
  hi3Data?: HI3Interface[] | null
  allTargetAfs?: TargetModel[]
  afMapping?: AccessFunctionMap
  validAfs?: Set<string>
  includeInWarrant?: boolean
  hideFromView?: boolean
  subStatus?: string
  status?: string
  reporting?: unknown
  warrantStartDateTime?: string
  warrantStopDateTime?: string
  caseIndex?: number
}

export type CaseOptions = {
  kpi: boolean
  traceLevel: number
  configureOptions: boolean
  packetEnvelopeMessage: boolean
  packetEnvelopeContent: boolean
  realTimeText: boolean
  callPartyNumberDisplay: boolean
  directDigitExtractionData: boolean
  monitoringReplacementPartiesAllowed: boolean
  circuitSwitchedVoiceCombined: boolean
  includeInBandOutBoundSignalling: boolean
  encryptionType: string
  encryptionKey: string
  provideTargetIdentityInCcChannel: string
  cccBillingType: string
  cccBillingNumber: string
  deactivationMode: string
  location: boolean
  target: boolean
  sms: boolean
  callIndependentSupplementaryServicesContent: boolean
  packetSummaryCount: number
  packetSummaryTimer: number
  generatePacketDataHeaderReport: boolean
  packetHeaderInformationReportFilter: string
  callTraceLogging: number
  featureStatusInterval: number
  surveillanceStatusInterval: number
  options?: boolean
  headersAF?: boolean
  summaryAF?: boolean
}
