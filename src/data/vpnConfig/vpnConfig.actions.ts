import * as api from './vpnConfig.api';
import { GET_VPNCONFIGS, GetVpnConfigAction } from './vpnConfig.types';

// eslint-disable-next-line import/prefer-default-export
export function getVpnConfigs(deliveryFunctionId: string): GetVpnConfigAction {
  return {
    type: GET_VPNCONFIGS,
    payload: api.getVpnConfigs(deliveryFunctionId),
  };
}
