import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import { Operation } from '../types';

export const API_PATH_VPNCONFIGS = '/vpn-configs';

export const GET_VPNCONFIGS = 'GET_VPNCONFIGS';
export const GET_VPNCONFIGS_PENDING = 'GET_VPNCONFIGS_PENDING';
export const GET_VPNCONFIGS_FULFILLED = 'GET_VPNCONFIGS_FULFILLED';
export const GET_VPNCONFIGS_REJECTED = 'GET_VPNCONFIGS_REJECTED';

export type VpnEncryption = 'AES(128)' | 'AES(192)' | 'AES(256)' | '3DES' | 'AES' | 'DES' | 'DISABLED';

export type VpnAuthentication = 'MD5' | 'SHA' | 'HMAC-SHA256' | 'HMAC-SHA384' | 'HMAC-SHA512' | 'DISABLED';

export type VpnOakley = 'NA' | '1' | '2' | '5' | '14' | '15' | '16' | '19' | '20' | '21' | '22' | '23' | '24' | '25' | '26';

export type VpnProtocol = 'UDP' | 'TCP';

export type LiInterface = 'x1' | 'x2' | 'x3' | 'hi2' | 'hi3' | 'NA';

export type VpnConfig = {
  readonly id?: string
  name: string
  mappedId: string
  mappedAfid?: string
  mappedCfid?: string
  role: 'MF' | 'DF' | 'IPSEC' | 'IPSECTRANS'
  vpnOwnIp: string
  vpnDestIp: string
  tunOwnIp: string
  tunDestIp: string
  p1Encr: VpnEncryption
  p1Auth: VpnAuthentication
  p1Oakley: VpnOakley
  encryption: VpnEncryption
  authentication: VpnAuthentication
  p2Oakley: VpnOakley
  p2LifetimeSecs: string
  key: string
  enabled: boolean
  ikeVer: 'IKEv1' | 'IKEv2'
  keyingTries: string | 'forever'
  leftProtoPort: string
  rightProtoPort: string
  leftProtocol: VpnProtocol
  rightProtocol: VpnProtocol
  pfs: boolean
  rekeyMarginSecs: string
  rekey: boolean
  p1LifetimeSecs: string
  p1Mode: 'MAIN' | 'AGGRESSIVE'
  templateId?: string
  copiedId?: string
  operation?: Operation
}

export interface VpnConfigFormData extends Omit<VpnConfig, 'mappedAfid' | 'mappedCfid'> {
  mappedAfid?: string
  mappedCfid?: string
}

export interface VpnConfigState {
  entities: VpnConfig[]
}

export interface GetVpnConfigAction extends AnyAction {
  type: typeof GET_VPNCONFIGS
  payload: AxiosPromise<ServerResponse<VpnConfig>>
}

export interface GetVpnConfigPendingAction extends AnyAction {
  type: typeof GET_VPNCONFIGS_PENDING
  payload: AxiosResponse<ServerResponse<VpnConfig>>
}
export interface GetVpnConfigFulfilledAction extends AnyAction {
  type: typeof GET_VPNCONFIGS_FULFILLED
  payload: AxiosResponse<ServerResponse<VpnConfig>>
}

export interface GetVpnConfigRejectedAction extends AnyAction {
  type: typeof GET_VPNCONFIGS_REJECTED
  payload: AxiosResponse<ServerResponse<VpnConfig>>
}

export type VpnConfigActionTypes =
  GetVpnConfigAction |
  GetVpnConfigPendingAction |
  GetVpnConfigFulfilledAction |
  GetVpnConfigRejectedAction;
