import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockVpnConfigs } from '__test__/mockVpnConfigs';
import {
  getVpnConfigs,
} from './vpnConfig.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './vpnConfig.types';

describe('vpnConfig.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of vpnConfigs', () => {
    const action: types.VpnConfigActionTypes = {
      type: types.GET_VPNCONFIGS_FULFILLED,
      payload: buildAxiosServerResponse(mockVpnConfigs),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getVpnConfigs(store.getState())).toEqual(mockVpnConfigs);
  });
});
