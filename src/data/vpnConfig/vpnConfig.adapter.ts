import isEqual from 'lodash.isequal';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import omitBy from 'lodash.omitby';
import isNil from 'lodash.isnil';

export function setVpnConfigsOperation(
  vpnConfigs: VpnConfig[] | undefined,
  origVpnConfigs: VpnConfig[] | undefined,
): VpnConfig[] | undefined {
  if (vpnConfigs === undefined) {
    return;
  }

  if (origVpnConfigs === undefined) {
    vpnConfigs.forEach((vpnConfig) => { vpnConfig.operation = 'CREATE'; });
  }

  setInterfaceOperationValueBasedInOriginal(
    vpnConfigs,
    origVpnConfigs,
  );
}

export function adaptVPNToCopy(
  vpnConfigs: VpnConfig[] | undefined,
): VpnConfig [] | undefined {
  return vpnConfigs?.map((entry) => {
    const { id, ...rest } = entry;
    if (id != null) {
      rest.copiedId = id;
      return rest;
    }

    return omitBy<VpnConfig>(entry, isNil) as VpnConfig;
  });
}

function setInterfaceOperationValueBasedInOriginal(
  vpnConfigs: VpnConfig[] | undefined,
  originalVpnConfigs: VpnConfig[] | undefined,
) {
  // Check which interfaces will be marked as CREATE, UPDATE or NO_OPERATION
  vpnConfigs?.forEach((vpnConfig) => {
    const originalVpnConfig = originalVpnConfigs?.find(({ id }) => id === vpnConfig.id);
    if (originalVpnConfig == null) {
      vpnConfig.operation = 'CREATE';
    } else if (vpnConfig.operation !== 'DELETE') {
      if (!isEqual(vpnConfig, originalVpnConfig)) {
        vpnConfig.operation = 'UPDATE';
      } else {
        vpnConfig.operation = 'NO_OPERATION';
      }
    }
  });

  // Check which interfaces will be marked as DELETE
  const vpnConfigsToDelete =
    originalVpnConfigs?.filter((originalVpnConfig) => {
      return !vpnConfigs?.find(({ id }) => id === originalVpnConfig.id);
    })
      .map((originalVpnConfig) => ({
        ...originalVpnConfig,
        operation: 'DELETE',
      }));

  if (vpnConfigsToDelete) {
    vpnConfigs?.push(...(vpnConfigsToDelete as VpnConfig[]));
  }
}

const ENCRYPTED_PLACEHOLDER = '*****';

export function adaptVPNToSubmit(
  vpnConfigsForm: VpnConfig[] | undefined,
): VpnConfig[] | undefined {
  if (!vpnConfigsForm) {
    return undefined;
  }

  return vpnConfigsForm.map((entry) => {
    if (entry.key === ENCRYPTED_PLACEHOLDER) {
      entry.key = '';
    }

    return omitBy<VpnConfig>(entry, isNil) as VpnConfig;
  });
}
