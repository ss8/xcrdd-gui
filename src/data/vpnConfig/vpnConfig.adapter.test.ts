import cloneDeep from 'lodash.clonedeep';
import {
  setVpnConfigsOperation,
  adaptVPNToSubmit,
  adaptVPNToCopy,
} from 'data/vpnConfig/vpnConfig.adapter';
import { VpnConfig } from './vpnConfig.types';

describe('setVpnConfigsOperation', () => {
  const vpnConfigs: VpnConfig[] = [];
  const originalVpnConfigs: VpnConfig [] = [];
  beforeEach(() => {
    vpnConfigs.push({
      id: '',
      name: 'vpn-x1',
      mappedId: '1',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    },
    {
      id: '',
      name: 'vpn-x2',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    });
  });

  it('Operation set as CREATE for create mode', () => {
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);
    vpnConfigs.forEach((vpnConfig) => {
      expect(vpnConfig.operation).toEqual('CREATE');
    });
  });

  it('Operation set as UPDATE on the second one which has value changed for Edit mode', () => {
    const id = 'EEAABB22';
    vpnConfigs[1] = {
      ...vpnConfigs[1],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[1]);
    originalVpnConfigs.push(clonedVpnConfig);
    vpnConfigs[1].tunOwnIp = '10.0.1.123';
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[1].operation).toEqual('UPDATE');
  });

  it('Operation set as NO_OPERATION on the second one for Edit mode', () => {
    const id = 'EEAABB11';
    vpnConfigs[1] = {
      ...vpnConfigs[1],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[1]);
    originalVpnConfigs.push(clonedVpnConfig);
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[1].operation).toEqual('NO_OPERATION');
  });

  it('Operation set as CREATE on the first one which is not existing for Edit mode', () => {
    const id = 'ECDABB11';
    vpnConfigs[1] = {
      ...vpnConfigs[1],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[1]);
    originalVpnConfigs.push(clonedVpnConfig);
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[0].operation).toEqual('CREATE');
  });

  it('Operation set as DELETE on the first one when the associated interface removed for Edit mode', () => {
    const id = '1ERRABB11';
    vpnConfigs[0] = {
      ...vpnConfigs[0],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[0]);
    originalVpnConfigs.push(clonedVpnConfig);
    vpnConfigs.length = 0;
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[0].operation).toEqual('DELETE');
  });

  it('Operation is kept as DELETE when vpn object is set as DELETE on operation.', () => {
    const id = '1ERRABB11';
    vpnConfigs[0] = {
      ...vpnConfigs[0],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[0]);
    originalVpnConfigs.push(clonedVpnConfig);
    vpnConfigs[0].key = '11112222';
    vpnConfigs[0].operation = 'DELETE';
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[0].operation).toEqual('DELETE');
  });

  it('Operation set as UPDATE when vpn object is set as NO_OPERATION on operation.', () => {
    const id = '1ERRABB11';
    vpnConfigs[0] = {
      ...vpnConfigs[0],
      id,
    };
    const clonedVpnConfig = cloneDeep(vpnConfigs[0]);
    originalVpnConfigs.push(clonedVpnConfig);
    vpnConfigs[0].key = '11112222';
    vpnConfigs[0].operation = 'NO_OPERATION';
    setVpnConfigsOperation(vpnConfigs, originalVpnConfigs);

    expect(vpnConfigs[0].operation).toEqual('UPDATE');
  });

});

describe('adaptVPNToCopy', () => {
  const vpnConfigs: VpnConfig[] = [];
  const originalVpnConfigs: VpnConfig [] = [];
  beforeEach(() => {
    vpnConfigs.push({
      id: '',
      name: 'vpn-x1',
      mappedId: '1',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    },
    {
      id: '',
      name: 'vpn-x2',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    });
  });

  it('Should set copiedId as the value from vpn.id when copy AF with VPN.', () => {
    const vpnId = '1ERRABB11';
    vpnConfigs[0] = {
      ...vpnConfigs[0],
      id: vpnId,
    };
    originalVpnConfigs.length = 0;
    const receivedVpnConfigs = adaptVPNToCopy(vpnConfigs) ?? [];

    expect(receivedVpnConfigs[0].copiedId).toEqual(vpnId);
    const { id, ...copiedVpn } = vpnConfigs[0];
    copiedVpn.copiedId = vpnId;
    expect(receivedVpnConfigs[0]).toEqual(copiedVpn);
  });
});

describe('adaptVPNToSubmit', () => {
  const vpnConfigs: VpnConfig[] = [];
  beforeEach(() => {
    vpnConfigs.push({
      id: '',
      name: 'vpn-x1',
      mappedId: '1',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '*****',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    },
    {
      id: '',
      name: 'vpn-x2',
      mappedId: '2',
      role: 'IPSEC',
      vpnOwnIp: '',
      vpnDestIp: '',
      tunOwnIp: '',
      tunDestIp: '',
      p1Encr: 'AES',
      p1Auth: 'MD5',
      p1Oakley: '5',
      encryption: 'AES',
      authentication: 'MD5',
      p2Oakley: '5',
      p2LifetimeSecs: '3600',
      key: '12345678',
      enabled: true,
      ikeVer: 'IKEv1',
      keyingTries: 'forever',
      leftProtoPort: '0',
      rightProtoPort: '0',
      leftProtocol: 'UDP',
      rightProtocol: 'UDP',
      pfs: true,
      rekeyMarginSecs: '540',
      rekey: true,
      p1LifetimeSecs: '14400',
      p1Mode: 'AGGRESSIVE',
    });
  });

  it('Should set key field as empty ', () => {
    const vpnConfigsForSubmit = adaptVPNToSubmit(vpnConfigs);
    vpnConfigsForSubmit?.forEach((vpnConfig) => {
      expect(vpnConfig).toEqual({
        ...vpnConfig,
        key: vpnConfig.key === '*****' ? '' : vpnConfig.key,
      });
    });
  });
});
