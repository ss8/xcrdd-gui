import { mockVpnConfigs } from '__test__/mockVpnConfigs';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './vpnConfig.reducer';
import * as types from './vpnConfig.types';

describe('vpnConfig.reducer', () => {
  it('should handle GET_FTP_INFOS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_VPNCONFIGS_FULFILLED,
      payload: buildAxiosServerResponse(mockVpnConfigs),
    })).toEqual({
      entities: mockVpnConfigs,
    });
  });
});
