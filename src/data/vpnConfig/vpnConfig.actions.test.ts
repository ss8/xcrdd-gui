import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './vpnConfig.actions';
import * as types from './vpnConfig.types';
import * as api from './vpnConfig.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('vpnConfig.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the vpnConfigs', () => {
    const expectedAction = {
      type: types.GET_VPNCONFIGS,
      payload: api.getVpnConfigs('0'),
    };

    expect(actions.getVpnConfigs('0')).toEqual(expectedAction);
  });
});
