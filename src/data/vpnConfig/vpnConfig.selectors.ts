import { RootState } from '../root.reducers';
import { VpnConfig } from './vpnConfig.types';

// eslint-disable-next-line import/prefer-default-export
export const getVpnConfigs = (state: RootState): VpnConfig[] => state.vpnConfig.entities;
