import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  VpnConfig,
  API_PATH_VPNCONFIGS,
} from './vpnConfig.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getVpnConfigs(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<VpnConfig>> {
  return http.get(`${API_PATH_VPNCONFIGS}/${deliveryFunctionId}`);
}
