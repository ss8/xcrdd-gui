import {
  GET_VPNCONFIGS_FULFILLED,
  VpnConfigActionTypes,
  VpnConfigState,
} from './vpnConfig.types';

const initialState: VpnConfigState = {
  entities: [],
};

export default function vpnConfigReducer(
  state = initialState,
  action: VpnConfigActionTypes,
): VpnConfigState {
  switch (action.type) {
    case GET_VPNCONFIGS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
