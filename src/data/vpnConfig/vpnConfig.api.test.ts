import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './vpnConfig.api';
import { API_PATH_VPNCONFIGS } from './vpnConfig.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('vpnConfig.api', () => {
  it('should request the list for vpnConfigs', async () => {
    const response = buildAxiosServerResponse(mockFtpInfos);
    axiosMock.onGet(`${API_PATH_VPNCONFIGS}/0`).reply(200, response);
    await expect(api.getVpnConfigs('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_VPNCONFIGS}/0` },
      status: 200,
    });
  });
});
