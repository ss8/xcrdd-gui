export type Customization = {
  derivedTargets: CustomizationDerivedTargetMap
  collectionFunctionName?: CustomizationCollectionFunctionName
  targetDefaultAFSelection?: TargetDefaultAFSelection
}

export type CustomizationDerivedTargetMap = {
  [target: string]: CustomizationDerivedTargetService
}

export type CustomizationDerivedTargetService = {
  [service: string]: {
    parent: string
    pattern: string
    target?: string
  }
}

export type CustomizationCollectionFunctionName = {
  template?: string
  tagMap?: {
    [collectionFunctionTag: string]: string
  }
}

// Only SelectNoneMethod is supported now
export type TargetDefaultAFSelectionMethod = SelectNoneMethod |
  SelectAFGroupMatchingTargetValueMethod |
  SelectAFGroupsMethod |
  SelectAFsMethod;

export type SelectNoneMethod = {
  method: 'selectNone'
}

// preparing for DX-2595
export type SelectAFGroupMatchingTargetValueMethod = {
  method: 'selectAFGroupMatchingTargetValue'
  regExp: string
}

// preparing for DX-2492
export type SelectAFGroupsMethod = {
  method: 'selectAFGroups'
  afGroupNames: string[]
}

// preparing for DX-2492
export type SelectAFsMethod = {
  method: 'selectAFs'
  afNames: string[]
}

export type TargetDefaultAFSelection = {
  [service: string]: {
    [target:string]: {
      selectionMethods: TargetDefaultAFSelectionMethod[],
      selectionRequired?: boolean
    }
  }
}
