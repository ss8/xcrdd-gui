import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  Location,
  API_PATH_LOCATIONS,
} from './location.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

export function getLocationList(): AxiosPromise<ServerResponse<Location>> {
  return http.get(`${API_PATH_LOCATIONS}`);
}

export function getLocationByFilter(filter: string): AxiosPromise<ServerResponse<Location>> {
  return http.get(`${API_PATH_LOCATIONS}`, {
    params: {
      $filter: filter,
    },
  });
}

export function getLocationByFilterWithViewCities(filter: string): AxiosPromise<ServerResponse<Location>> {
  return http.get(`${API_PATH_LOCATIONS}`, {
    params: {
      $filter: filter,
      $view: 'cities',
    },
  });
}
