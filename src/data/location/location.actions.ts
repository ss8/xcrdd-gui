import * as api from './location.api';
import {
  FETCH_LOCATION_LIST,
  FETCH_LOCATION_LIST_BY_FILTER,
  FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES,
  FetchLocationListAction,
  FetchLocationListByFilterAction,
  FetchLocationListByFilterWithViewCitiesAction,
} from './location.types';

export function fetchLocationList(): FetchLocationListAction {
  return {
    type: FETCH_LOCATION_LIST,
    payload: api.getLocationList(),
  };
}

export function fetchLocationByFilter(filter: string): FetchLocationListByFilterAction {
  return {
    type: FETCH_LOCATION_LIST_BY_FILTER,
    payload: api.getLocationByFilter(filter),
  };
}

export function fetchLocationByFilterWithViewCities(
  filter: string,
): FetchLocationListByFilterWithViewCitiesAction {
  return {
    type: FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES,
    payload: api.getLocationByFilterWithViewCities(filter),
  };
}
