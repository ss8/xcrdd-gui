import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './location.reducer';
import * as types from './location.types';

describe('location.reducer', () => {
  let locations: types.Location[];

  beforeEach(() => {
    locations = [{
      id: 'id1',
      country: 'United States',
      stateCode: 'CA',
      stateName: 'California',
      cities: [{
        id: 'id1',
        city: 'San Francisco',
        latitude: 37.7749,
        longitude: 122.4194,
      }],
    }];
  });

  it('should handle FETCH_LOCATION_LIST_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.FETCH_LOCATION_LIST_FULFILLED,
      payload: buildAxiosServerResponse(locations),
    })).toEqual({
      entities: locations,
      filteredEntities: [],
    });
  });

  it('should handle FETCH_LOCATION_LIST_BY_FILTER_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.FETCH_LOCATION_LIST_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(locations),
    })).toEqual({
      entities: locations,
      filteredEntities: [],
    });
  });

  it('should handle FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED,
      payload: buildAxiosServerResponse(locations),
    })).toEqual({
      entities: [],
      filteredEntities: locations,
    });
  });
});
