import { createSelector } from 'reselect';
import { RootState } from '../root.reducers';
import { Location } from './location.types';

export const getLocationList = (state: RootState): Location[] => state.location.entities;
export const getFilteredLocationList = (state: RootState): Location[] => state.location.filteredEntities;

export const getCountryList = createSelector(
  [getLocationList],
  (locationList) => {
    const countryMap = locationList.reduce((previousValue, { country }) => ({
      ...previousValue,
      [country]: true,
    }), {});

    return Object.keys(countryMap);
  },
);

export const getStateListByCountry = createSelector(
  [getLocationList],
  (locationList) => {
    const stateListByCountry: {[country: string]: { stateName: string, stateCode: string }[]} = {};

    locationList.forEach(({ country, stateName, stateCode }) => {
      if (stateListByCountry[country] == null) {
        stateListByCountry[country] = [];
      }

      stateListByCountry[country].push({ stateName, stateCode });
    });

    return stateListByCountry;
  },
);

export const getFilteredCityList = createSelector(
  [getFilteredLocationList],
  (locationList) => {
    const filteredCities: string[] = [];

    locationList.forEach(({ country, stateName, cities }) => {
      if (cities) {
        const cityNames = cities.map(({ city }) => city);
        filteredCities.push(...cityNames);
      }
    });

    return filteredCities;
  },
);
