import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getLocationList,
} from './location.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './location.types';

describe('location.selectors', () => {
  let locations: types.Location[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    locations = [{
      id: 'id1',
      country: 'United States',
      stateCode: 'CA',
      stateName: 'California',
      cities: [{
        id: 'id1',
        city: 'San Francisco',
        latitude: 37.7749,
        longitude: 122.4194,
      }],
    }];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of locations', () => {
    const action: types.LocationActionTypes = {
      type: types.FETCH_LOCATION_LIST_FULFILLED,
      payload: buildAxiosServerResponse(locations),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getLocationList(store.getState())).toEqual(locations);
  });
});
