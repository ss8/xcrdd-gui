import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './location.api';
import {
  Location,
} from './location.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('location.api', () => {
  let locations: Location[];

  beforeEach(() => {
    locations = [{
      id: 'id1',
      country: 'United States',
      stateCode: 'CA',
      stateName: 'California',
      cities: [{
        id: 'id1',
        city: 'San Francisco',
        latitude: 37.7749,
        longitude: 122.4194,
      }],
    }];
  });

  it('should request the list for locations', async () => {
    const response = buildAxiosServerResponse(locations);
    axiosMock.onGet('/locations').reply(200, response);
    await expect(api.getLocationList()).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/locations' },
      status: 200,
    });
  });

  it('should request the list for locations filtered', async () => {
    const response = buildAxiosServerResponse(locations);
    axiosMock.onGet('/locations').reply(200, response);
    await expect(api.getLocationByFilter('country eq \'US\'')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/locations' },
      status: 200,
    });
  });

  it('should request the list for locations filtered with cities', async () => {
    const response = buildAxiosServerResponse(locations);
    axiosMock.onGet('/locations').reply(200, response);
    await expect(api.getLocationByFilterWithViewCities('country eq \'US\'')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/locations' },
      status: 200,
    });
  });
});
