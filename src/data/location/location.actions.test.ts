import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './location.actions';
import * as types from './location.types';
import * as api from './location.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('location.actions', () => {
  beforeAll(() => {
    axiosMock.onAny().reply(200);
  });

  it('should create an action to get the locations', () => {
    const expectedAction = {
      type: types.FETCH_LOCATION_LIST,
      payload: api.getLocationList(),
    };

    expect(actions.fetchLocationList()).toEqual(expectedAction);
  });

  it('should create an action to get the locations filtered', () => {
    const expectedAction = {
      type: types.FETCH_LOCATION_LIST_BY_FILTER,
      payload: api.getLocationByFilter('country eq \'US\''),
    };

    expect(actions.fetchLocationByFilter('country eq \'US\'')).toEqual(expectedAction);
  });

  it('should create an action to get the locations filtered with cities', () => {
    const expectedAction = {
      type: types.FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES,
      payload: api.getLocationByFilterWithViewCities('country eq \'US\''),
    };

    expect(actions.fetchLocationByFilterWithViewCities('country eq \'US\'')).toEqual(expectedAction);
  });
});
