import { Action } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import { AsyncAction } from 'redux-promise-middleware';

export const API_PATH_LOCATIONS = '/locations';

export const FETCH_LOCATION_LIST = 'FETCH_LOCATION_LIST';
export const FETCH_LOCATION_LIST_PENDING = 'FETCH_LOCATION_LIST_PENDING';
export const FETCH_LOCATION_LIST_FULFILLED = 'FETCH_LOCATION_LIST_FULFILLED';
export const FETCH_LOCATION_LIST_REJECTED = 'FETCH_LOCATION_LIST_REJECTED';
export const FETCH_LOCATION_LIST_BY_FILTER = 'FETCH_LOCATION_LIST_BY_FILTER';
export const FETCH_LOCATION_LIST_BY_FILTER_PENDING = 'FETCH_LOCATION_LIST_BY_FILTER_PENDING';
export const FETCH_LOCATION_LIST_BY_FILTER_FULFILLED = 'FETCH_LOCATION_LIST_BY_FILTER_FULFILLED';
export const FETCH_LOCATION_LIST_BY_FILTER_REJECTED = 'FETCH_LOCATION_LIST_BY_FILTER_REJECTED';
export const FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES = 'FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES';
export const FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_PENDING = 'FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_PENDING';
export const FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED = 'FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED';
export const FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_REJECTED = 'FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_REJECTED';

export interface Location {
  id: string
  country: string
  stateCode: string
  stateName: string
  cities: {
    id: string
    city: string
    latitude: number
    longitude: number
  }[] | null
}

export interface LocationState {
  entities: Location[]
  filteredEntities: Location[]
}

export interface FetchLocationListAction extends AsyncAction {
  type: typeof FETCH_LOCATION_LIST
  payload: AxiosPromise<ServerResponse<Location>>
}

export interface FetchLocationListFulfilledAction extends Action {
  type: typeof FETCH_LOCATION_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<Location>>
}

export interface FetchLocationListByFilterAction extends AsyncAction {
  type: typeof FETCH_LOCATION_LIST_BY_FILTER
  payload: AxiosPromise<ServerResponse<Location>>
}

export interface FetchLocationListByFilterFulfilledAction extends Action {
  type: typeof FETCH_LOCATION_LIST_BY_FILTER_FULFILLED
  payload: AxiosResponse<ServerResponse<Location>>
}

export interface FetchLocationListByFilterWithViewCitiesAction extends AsyncAction {
  type: typeof FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES
  payload: AxiosPromise<ServerResponse<Location>>
}

export interface FetchLocationListByFilterWithViewCitiesFulfilledAction extends Action {
  type: typeof FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED
  payload: AxiosResponse<ServerResponse<Location>>
}

export type LocationActionTypes =
  FetchLocationListAction |
  FetchLocationListFulfilledAction |
  FetchLocationListByFilterAction |
  FetchLocationListByFilterFulfilledAction |
  FetchLocationListByFilterWithViewCitiesAction |
  FetchLocationListByFilterWithViewCitiesFulfilledAction;
