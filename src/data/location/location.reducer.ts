import {
  LocationState,
  LocationActionTypes,
  FETCH_LOCATION_LIST_FULFILLED,
  FETCH_LOCATION_LIST_BY_FILTER_FULFILLED,
  FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED,
} from './location.types';

const initialState: LocationState = {
  entities: [],
  filteredEntities: [],
};

export default function locationReducer(
  state = initialState,
  action: LocationActionTypes,
): LocationState {
  switch (action.type) {
    case FETCH_LOCATION_LIST_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    case FETCH_LOCATION_LIST_BY_FILTER_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    case FETCH_LOCATION_LIST_BY_FILTER_WITH_VIEW_CITIES_FULFILLED:
      return {
        ...state,
        filteredEntities: action.payload.data.data || [],
      };

    default:
      return state;
  }
}
