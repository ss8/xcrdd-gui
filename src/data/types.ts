import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';

export type YesOrNo = 'YES' | 'NO';

export type Operation = 'CREATE' | 'UPDATE' | 'DELETE' | 'NO_OPERATION';

export type Deployment = 'andromeda' | 'endeavor';

export interface Contact {
  id: string
  name: string
}

export type Identity = {
  id?: string
  name?: string
}

export interface Metadata {
  recordCount: number
  recordsInView: number
  idCreated: string
  uriStatus: string | null
}

interface Error {
  status: number
  code: string
  message: string
}

export interface ServerResponse<T> {
  success: boolean
  metadata: Metadata | null
  data: T[] | null
  error: Error | null
  errors?: string[]
  message?: string
  status?: string
}

export interface UserSettings {
  readonly id?: string
  dateUpdated?: string
  name: string
  setting: string
  type: 'json'
  user: string
}

export interface GridSettings {
  columnSetting: {
    colId: string
    hide?: boolean
    aggFunc?: string | null
    width?: number
    pivotIndex?: number | null
    pinned?: boolean | string | 'left' | 'right'
    rowGroupIndex?: number | null
  }[]
  sortSetting: {
    colId: string
    sort: string
  }[]
}

export type AuditDetails = {
  auditEnabled: boolean
  userId: string
  userName: string
  fieldDetails: string
  recordName: string
  service: AuditService
  actionType?: AuditActionType
}

export type WithAuditDetails<T> = {
  data: T
  auditDetails?: AuditDetails
}

export type ValueLabelPair = {
  value: string,
  label: string,
}

export type DeliveryFunctionId = {
  deliveryFunctionId: string
}
