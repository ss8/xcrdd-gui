import * as api from './network.api';
import { GET_NETWORKS, GetNetworkAction } from './network.types';

// eslint-disable-next-line import/prefer-default-export
export function getNetworks(deliveryFunctionId: string): GetNetworkAction {
  return {
    type: GET_NETWORKS,
    payload: api.getNetworks(deliveryFunctionId),
  };
}
