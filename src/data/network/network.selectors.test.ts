import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getNetworks,
} from './network.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './network.types';

describe('network.selectors', () => {
  let networks: types.Network[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    networks = [{
      id: 'id1',
      networkId: 'networkId1',
      ownIPAddress: '12.12.12.12',
      startPortNumber: '1000',
      portNumberRange: '100',
    }];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of networks', () => {
    const action: types.NetworkActionTypes = {
      type: types.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(networks),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getNetworks(store.getState())).toEqual(networks);
  });
});
