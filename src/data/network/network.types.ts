import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_NETWORKS = '/networks';

export const GET_NETWORKS = 'GET_NETWORKS';
export const GET_NETWORKS_FULFILLED = 'GET_NETWORKS_FULFILLED';
export const GET_NETWORKS_REJECTED = 'GET_NETWORKS_REJECTED';

export interface Network {
  id: string
  networkId: string
  ownIPAddress: string
  startPortNumber: string
  portNumberRange: string
}

export interface NetworkState {
  entities: Network[]
}

export interface GetNetworkAction extends AnyAction {
  type: typeof GET_NETWORKS
  payload: AxiosPromise<ServerResponse<Network>>
}

export interface GetNetworkFulfilledAction extends AnyAction {
  type: typeof GET_NETWORKS_FULFILLED
  payload: AxiosResponse<ServerResponse<Network>>
}

export type NetworkActionTypes =
  GetNetworkAction |
  GetNetworkFulfilledAction;
