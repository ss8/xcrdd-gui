import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './network.api';
import {
  Network,
} from './network.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('network.api', () => {
  let network: Network;

  beforeEach(() => {
    network = {
      id: 'id1',
      networkId: 'networkId1',
      ownIPAddress: '12.12.12.12',
      startPortNumber: '1000',
      portNumberRange: '100',
    };
  });

  it('should request the list for networks', async () => {
    const response = buildAxiosServerResponse([network]);
    axiosMock.onGet('/networks/0').reply(200, response);
    await expect(api.getNetworks('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/networks/0' },
      status: 200,
    });
  });
});
