import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './network.reducer';
import * as types from './network.types';

describe('network.reducer', () => {
  let networks: types.Network[];

  beforeEach(() => {
    networks = [{
      id: 'id1',
      networkId: 'networkId1',
      ownIPAddress: '12.12.12.12',
      startPortNumber: '1000',
      portNumberRange: '100',
    }];
  });

  it('should handle GET_NETWORKS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(networks),
    })).toEqual({
      entities: networks,
    });
  });
});
