import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './network.actions';
import * as types from './network.types';
import * as api from './network.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('network.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the networks', () => {
    const expectedAction = {
      type: types.GET_NETWORKS,
      payload: api.getNetworks('0'),
    };

    expect(actions.getNetworks('0')).toEqual(expectedAction);
  });
});
