import { RootState } from '../root.reducers';
import { Network } from './network.types';

// eslint-disable-next-line import/prefer-default-export
export const getNetworks = (state: RootState): Network[] => state.network.entities;
