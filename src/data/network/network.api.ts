import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  Network,
  API_PATH_NETWORKS,
} from './network.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getNetworks(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<Network>> {
  return http.get(`${API_PATH_NETWORKS}/${deliveryFunctionId}`);
}
