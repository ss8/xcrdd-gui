import {
  GET_NETWORKS_FULFILLED,
  NetworkActionTypes,
  NetworkState,
} from './network.types';

const initialState: NetworkState = {
  entities: [],
};

export default function networkReducer(
  state = initialState,
  action: NetworkActionTypes,
): NetworkState {
  switch (action.type) {
    case GET_NETWORKS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
