import { CustomizationCollectionFunctionName, CustomizationDerivedTargetMap, TargetDefaultAFSelection } from 'data/customization/customization.types';
import { RootState } from 'data/root.reducers';
import { RawTemplate, Template } from 'data/template/template.types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import { CaseModel } from 'data/case/case.types';
import { Warrant } from './warrant.types';

export const getSelectedWarrant = (state: RootState): Warrant | null => state.warrants.selectedWarrants[0];
export const getWarrant = (state: RootState): Warrant | null => state.warrants.warrant;
export const getTemplateUsed = (state: RootState): Template | null => state.warrants.templateUsed;
export const getRawTemplates = (state: RootState): RawTemplate[] => state.warrants.templates;
export const getChangeTrackerInstance = (state: RootState): ChangeTracker | null => state.warrants.changeTracker;
export const getWarrantConfigurationInstance = (state: RootState): WarrantConfiguration | null =>
  state.warrants.warrantConfigInstance;
export const getCustomizationDerivedTargets = (state: RootState): CustomizationDerivedTargetMap =>
  state.warrants.customizationconfig?.derivedTargets;
export const getCustomizationCollectionFunctionName =
  (state: RootState): CustomizationCollectionFunctionName | undefined =>
    state.warrants.customizationconfig?.collectionFunctionName;
export const getTargetDefaultAFSelection = (state: RootState): TargetDefaultAFSelection | undefined =>
    state.warrants.customizationconfig?.targetDefaultAFSelection;
export const getWarrantUserAudit = (state: RootState): unknown => state.warrants.userAudit;
export const getSelectedWarrantCase = (state: RootState): CaseModel | null => state.warrants.selectedCase;
