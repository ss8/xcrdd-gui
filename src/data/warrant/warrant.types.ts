import { CaseOptions, CaseModel } from 'data/case/case.types';
import { Customization } from 'data/customization/customization.types';
import { SupportedFeatures } from 'data/supportedFeatures/supportedFeatures.types';
import { RawTemplate, Template } from 'data/template/template.types';
import { Contact, Identity } from 'data/types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';

export const WARRANT_LEA_ANY = 'Any';

export interface WarrantState {
  warrant: Warrant | null
  warrants: unknown[]
  selectedWarrants: Warrant[]
  error: unknown
  cfs: unknown[]
  action: string
  config: SupportedFeatures | undefined
  customizationconfig: Customization
  templates: RawTemplate[],
  template: unknown,
  changeTracker: ChangeTracker | null
  warrantConfigInstance: WarrantConfiguration | null
  templateUsed: Template | null
  userAudit: unknown
  selectionLastCleared: unknown
  displayTrashWarrants: boolean
  selectedCase: CaseModel | null,
}

export type Warrant = {
  id: string
  attachments: unknown[]
  caseOptTemplateUsed?: string
  caseOptions?: CaseOptions
  caseType?: string
  cases: Identity[]
  cfGroup?: Identity
  cfPool?: unknown[]
  city: string
  comments: string
  contact: Contact | null
  department: string
  dxAccess: string
  dxOwner: string
  judge: string
  lea?: string
  name: string
  province: string
  receivedDateTime: string
  region: string
  startDateTime: string
  stopDateTime: string
  templateUsed: string
  timeZone: string
  state?: string
}
