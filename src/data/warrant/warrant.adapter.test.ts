import cloneDeep from 'lodash.clonedeep';
import { mockWarrants } from '__test__/mockWarrants';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { warrantModel } from 'xcipio/warrants/warrant-model';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import { CaseModel } from 'data/case/case.types';
import WarrantAdapter, { CF_GROUP_NONE_ID } from './warrant.adapter';
import { Warrant } from './warrant.types';

describe('WarrantAdapter', () => {
  let testMocks: any;

  beforeEach(() => {
    testMocks = {};

    // As per configuration set in setupTest.js, the default timezone is:
    // America/Los_Angeles
    // ===================

    // This is the date converted to computer timezone
    testMocks.warrantInComputerTimezone = {
      ...mockWarrants[0],
      timeZone: 'America/New_York',
      receivedDateTime: '2020-07-29T15:20:00.000Z', // 2020-07-29 08:00 in America/Los_Angeles
      startDateTime: '2020-07-29T16:20:00.000Z', // 2020-07-29 09:20 in America/Los_Angeles
      stopDateTime: '2020-07-30T12:20:00.000Z', // 2020-07-30 05:20 in America/Los_Angeles
    };

    // This is what will be submitted
    testMocks.warrantInGivenTimezone = {
      ...mockWarrants[0],
      timeZone: 'America/New_York',
      receivedDateTime: '2020-07-29T12:20:00.000Z', // 2020-07-29 08:00 in America/New_York
      startDateTime: '2020-07-29T13:20:00.000Z', // 2020-07-29 09:20 in America/New_York
      stopDateTime: '2020-07-30T09:20:00.000Z', // 2020-07-30 05:20 in America/New_York
    };

    testMocks.casesInComputerTimezone = [{
      ...mockWarrants[0],
      timeZone: 'America/New_York',
      entryDateTime: '2020-07-29T07:00:00.000Z', // 2020-07-29 00:00 in America/Los_Angeles
      startDateTime: '2020-07-29T16:20:00.000Z', // 2020-07-29 09:20 in America/Los_Angeles
      stopDateTime: '2020-07-30T12:20:00.000Z', // 2020-07-30 05:20 in America/Los_Angeles
    }];

    testMocks.casesInGivenTimezone = [{
      ...mockWarrants[0],
      timeZone: 'America/New_York',
      entryDateTime: '2020-07-29T07:00:00.000Z', // 2020-07-29 03:00 in America/New_York
      startDateTime: '2020-07-29T13:20:00.000Z', // 2020-07-29 09:20 in America/New_York
      stopDateTime: '2020-07-30T09:20:00.000Z', // 2020-07-30 05:20 in America/New_York
    }];

    testMocks.warrantWithCasesInComputerTimezone = {
      ...testMocks.warrantInComputerTimezone,
      cases: testMocks.casesInComputerTimezone,
    };

    testMocks.warrantWithCasesInGivenTimezone = {
      ...testMocks.warrantInGivenTimezone,
      cases: testMocks.casesInGivenTimezone,
    };

    testMocks.userAuditInComputerTimezone = {
      signer1: 'test1',
      date1: '2020-07-30T12:20:00.000Z', // 2020-07-29 09:20 in America/Los_Angeles
      signer2: 'test2',
      date2: '2020-07-30T12:20:00.000Z', // 2020-07-29 09:20 in America/Los_Angeles
      warrantNumber: '123',
    };

    testMocks.userAuditInGivenTimezone = {
      signer1: 'test1',
      date1: '2020-07-30T09:20:00.000Z', // 2020-07-30 05:20 in America/New_York
      signer2: 'test2',
      date2: '2020-07-30T09:20:00.000Z', // 2020-07-30 05:20 in America/New_York
      warrantNumber: '123',
    };
  });

  describe('convertFromComputerTimezoneToGivenTimezone()', () => {
    it('should convert the date to the given timezone', () => {
      expect(WarrantAdapter.convertFromComputerTimezoneToGivenTimezone('2020-07-30T12:20:00.000Z', 'America/New_York'))
        .toEqual('2020-07-30T09:20:00.000Z');
    });
  });

  describe('convertFromGivenTimezoneToComputerTimezone()', () => {
    it('should revert the date back to the given timezone', () => {
      expect(WarrantAdapter.convertFromGivenTimezoneToComputerTimezone('2020-07-30T09:20:00.000Z', 'America/New_York'))
        .toEqual('2020-07-30T12:20:00.000Z');
    });
  });

  describe('applyConvertFunctionToField()', () => {
    beforeEach(() => {
      testMocks.data = {
        date: '2020-07-30T12:20:00.000Z',
      };
      testMocks.convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
    });

    it('should convert the field to the given timezone using convert function', () => {
      WarrantAdapter.applyConvertFunctionToField(testMocks.data, 'date', 'America/New_York', testMocks.convertFunction);
      expect(testMocks.data.date).toEqual('2020-07-30T09:20:00.000Z');
    });

    it('should do nothing if field is not available', () => {
      testMocks.data.date = null;
      WarrantAdapter.applyConvertFunctionToField(testMocks.data, 'date', 'America/New_York', testMocks.convertFunction);
      expect(testMocks.data.date).toEqual(null);
    });

    it('should do nothing if timezone is not defined', () => {
      WarrantAdapter.applyConvertFunctionToField(testMocks.data, 'date', '', testMocks.convertFunction);
      expect(testMocks.data.date).toEqual('2020-07-30T12:20:00.000Z');
    });
  });

  describe('adaptWarrantDateTimeFieldsToTimezone()', () => {
    it('should adapt the date time fields to the timezone', () => {
      const convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
      expect(WarrantAdapter.adaptWarrantDateTimeFieldsToTimezone(testMocks.warrantInComputerTimezone, convertFunction))
        .toEqual(testMocks.warrantInGivenTimezone);
    });
  });

  describe('adaptCasesDateTimeFieldsToTimezone()', () => {
    it('should adapt the date time fields to the timezone', () => {
      const convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
      expect(WarrantAdapter.adaptCasesDateTimeFieldsToTimezone(testMocks.casesInComputerTimezone, convertFunction))
        .toEqual(testMocks.casesInGivenTimezone);
    });
  });

  describe('adaptAuditDateTimeFieldsToTimezone()', () => {
    it('should adapt the date time fields to the timezone', () => {
      const timezone = 'America/New_York';
      const convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
      const actual = WarrantAdapter
        .adaptAuditDateTimeFieldsToTimezone(testMocks.userAuditInComputerTimezone, timezone, convertFunction);
      expect(actual).toEqual(testMocks.userAuditInGivenTimezone);
    });
  });

  describe('adaptWarrantDateTimeFieldsToSubmit()', () => {
    it('should adapt the data to be submitted', () => {
      expect(WarrantAdapter.adaptWarrantDateTimeFieldsToSubmit(testMocks.warrantWithCasesInComputerTimezone))
        .toEqual({
          ...testMocks.warrantWithCasesInGivenTimezone,
        });
    });
  });

  describe('adaptWarrantToSubmit()', () => {
    it('should adapt the warrant to be submitted', () => {
      expect(WarrantAdapter.adaptWarrantToSubmit(testMocks.warrantWithCasesInComputerTimezone))
        .toEqual({
          ...testMocks.warrantWithCasesInGivenTimezone,
          cfGroup: { id: 'MA' },
        });
    });

    it('should adapt the warrant to be submitted with the lea value', () => {
      testMocks.warrantWithCasesInComputerTimezone.lea = 'id1';

      expect(WarrantAdapter.adaptWarrantToSubmit(testMocks.warrantWithCasesInComputerTimezone))
        .toEqual({
          ...testMocks.warrantWithCasesInGivenTimezone,
          lea: 'id1',
          cfGroup: { id: 'id1' },
        });
    });
  });

  describe('adaptWarrantToLoad()', () => {
    it('should adapt the warrant to be loaded', () => {
      expect(WarrantAdapter.adaptWarrantToLoad(testMocks.warrantInGivenTimezone))
        .toEqual(testMocks.warrantInComputerTimezone);
    });
  });

  describe('adaptWarrantToLoad()', () => {
    it('should adapt the cases to be loaded', () => {
      expect(WarrantAdapter.adaptCasesToLoad(testMocks.casesInGivenTimezone))
        .toEqual(testMocks.casesInComputerTimezone);
    });
  });

  describe('adaptWarrantToForm()', () => {
    let dateMock: jest.SpyInstance;

    beforeEach(() => {
      dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));
    });

    afterEach(() => {
      dateMock.mockRestore();
    });

    it('should not change the lea value if it is already set', () => {
      const warrant = cloneDeep(mockWarrants[0]);
      warrant.lea = 'something';

      expect(WarrantAdapter.adaptWarrantToForm(warrant, CONFIG_FORM_MODE.Edit, false, undefined, null, warrant.id))
        .toEqual({
          ...warrant,
          lea: 'something',
        });
    });

    it('should set lea to the cf group if it is defined in warrant', () => {
      const warrant = cloneDeep(mockWarrants[0]);
      warrant.cfGroup = {
        id: 'id2',
        name: 'CookCounty',
      };

      expect(WarrantAdapter.adaptWarrantToForm(warrant, CONFIG_FORM_MODE.Edit, false, undefined, null, warrant.id))
        .toEqual({
          ...warrant,
          lea: 'id2',
        });
    });

    it('should set lea to Any if cf group is set to none in warrant', () => {
      const warrant = cloneDeep(mockWarrants[0]);
      warrant.cfGroup = {
        id: CF_GROUP_NONE_ID,
        name: '',
      };

      expect(WarrantAdapter.adaptWarrantToForm(warrant, CONFIG_FORM_MODE.Edit, false, undefined, null, warrant.id))
        .toEqual({
          ...warrant,
          lea: 'Any',
        });
    });

    it('should set the default stop time if enabled is configured and in Create mode', () => {
      expect(WarrantAdapter.adaptWarrantToForm(null, CONFIG_FORM_MODE.Create, true, '60', null, 'newId'))
        .toEqual({
          ...warrantModel,
          stopDateTime: '2021-06-28T06:59:00.000Z',
        });
    });

    it('should not set default stop time if it is already set', () => {
      const warrant = cloneDeep(mockWarrants[0]);

      expect(WarrantAdapter.adaptWarrantToForm(warrant, CONFIG_FORM_MODE.Create, true, '60', null, 'newId'))
        .toEqual({
          ...warrant,
        });
    });

    it('should not set default stop time if preference is not defined', () => {
      expect(WarrantAdapter.adaptWarrantToForm(null, CONFIG_FORM_MODE.Create, true, undefined, null, 'newId'))
        .toEqual({
          ...warrantModel,
        });
    });

    it('should call addUpdated from changeTracker when setting the stop time', () => {
      const changeTracker = new ChangeTracker({ idDF: '0', idWarrant: 'newId' });
      const changeTrackerSpy = jest.spyOn(changeTracker, 'addUpdated');

      WarrantAdapter.adaptWarrantToForm(null, CONFIG_FORM_MODE.Create, true, '60', changeTracker, 'newId');

      expect(changeTrackerSpy).toBeCalledWith('warrant.newId.stopDateTime', new Date('2021-06-28T06:59:00.000Z'), 'Stop Time');
    });
  });

  describe('setWarrantDefaultStopTime()', () => {
    let dateMock: jest.SpyInstance;
    let warrant: Partial<Warrant>;

    beforeEach(() => {
      dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));
      warrant = { stopDateTime: '2021-04-28T12:00:00.000Z' };
    });

    afterEach(() => {
      dateMock.mockRestore();
    });

    it('should set the stopDateTime if preference is configured', () => {
      expect(WarrantAdapter.setWarrantDefaultStopTime(warrant, '60')).toEqual({
        stopDateTime: '2021-06-28T06:59:00.000Z',
      });
    });

    it('should not set the stopDateTime if preference is not configured', () => {
      expect(WarrantAdapter.setWarrantDefaultStopTime(warrant, undefined)).toEqual({
        stopDateTime: '2021-04-28T12:00:00.000Z',
      });
    });
  });

  describe('setCaseDefaultStopTime()', () => {
    let dateMock: jest.SpyInstance;
    let caseModel: Partial<CaseModel>;

    beforeEach(() => {
      dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));
      caseModel = { stopDateTime: '2021-04-28T12:00:00.000Z' };
    });

    afterEach(() => {
      dateMock.mockRestore();
    });

    it('should set the stopDateTime if preference is configured', () => {
      expect(WarrantAdapter.setCaseDefaultStopTime(caseModel, '60')).toEqual({
        stopDateTime: '2021-06-28T06:59:00.000Z',
      });
    });

    it('should not set the stopDateTime if preference is not configured', () => {
      expect(WarrantAdapter.setCaseDefaultStopTime(caseModel)).toEqual({
        stopDateTime: '2021-04-28T12:00:00.000Z',
      });
    });
  });

  describe('setInterceptDefaultStopTime()', () => {
    let dateMock: jest.SpyInstance;
    let intercept: { stopDateTime: string };

    beforeEach(() => {
      dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));
      intercept = { stopDateTime: '2021-04-28T12:00:00.000Z' };
    });

    afterEach(() => {
      dateMock.mockRestore();
    });

    it('should set the stopDateTime if preference is configured', () => {
      expect(WarrantAdapter.setInterceptDefaultStopTime(intercept, '60')).toEqual({
        stopDateTime: '2021-06-28T06:59:00.000Z',
      });
    });

    it('should not set the stopDateTime if preference is not configured', () => {
      expect(WarrantAdapter.setInterceptDefaultStopTime(intercept, '')).toEqual({
        stopDateTime: '2021-04-28T12:00:00.000Z',
      });
    });
  });
});
