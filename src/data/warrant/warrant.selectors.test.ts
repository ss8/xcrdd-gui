import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import rootReducer, { RootState } from 'data/root.reducers';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { mockWarrants } from '__test__/mockWarrants';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockCustomization } from '__test__/mockCustomization';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import {
  getSelectedWarrant,
  getWarrant,
  getRawTemplates,
  getChangeTrackerInstance,
  getWarrantConfigurationInstance,
  getTemplateUsed,
  getCustomizationDerivedTargets,
  getCustomizationCollectionFunctionName,
} from './warrant.selectors';

describe('warrant.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;
  const changeTracker = new ChangeTracker({ idDF: '1', idWarrant: '1' });
  const warrantConfiguration = new WarrantConfiguration({
    warrants: {
      config: mockSupportedFeatures,
    },
    fetchCollectionFunctionListByFilter: jest.fn(),
    getAllAFsByFilter: jest.fn(),
    global: {
      selectedDF: '0',
    },
    discoveryXcipio: {
      discoveryXcipioSettings: {
        deployment: 'andromeda',
      },
    },
  });

  beforeEach(() => {
    const selectedWarrantAction: AnyAction = {
      type: warrantActions.BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: mockWarrants,
    };

    const warrantAction: AnyAction = {
      type: warrantActions.GET_WARRANT_BY_ID_FULFILLED,
      payload: buildAxiosServerResponse([mockWarrants[0]]),
    };

    const templateUsedAction: AnyAction = {
      type: warrantActions.UPDATE_TEMPLATE_USED,
      payload: mockTemplates[1],
    };

    const templateAction: AnyAction = {
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    };

    const changeTrackerAction: AnyAction = {
      type: warrantActions.SET_WARRANTEDIT_CHANGETRACKER,
      payload: changeTracker,
    };

    const warrantConfigurationAction: AnyAction = {
      type: warrantActions.SET_WARRANT_FORM_CONFIG_INSTANCE,
      payload: warrantConfiguration,
    };

    const customizationAction: AnyAction = {
      type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockCustomization]),
    };

    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, selectedWarrantAction));
    store = mockStore(rootReducer(store.getState(), warrantAction));
    store = mockStore(rootReducer(store.getState(), templateUsedAction));
    store = mockStore(rootReducer(store.getState(), templateAction));
    store = mockStore(rootReducer(store.getState(), changeTrackerAction));
    store = mockStore(rootReducer(store.getState(), warrantConfigurationAction));
    store = mockStore(rootReducer(store.getState(), customizationAction));
  });

  it('should return the selected warrant', () => {
    expect(getSelectedWarrant(store.getState())).toEqual(mockWarrants[0]);
  });

  it('should return the retrieved warrant', () => {
    expect(getWarrant(store.getState())).toEqual(mockWarrants[0]);
  });

  it('should return the selected template', () => {
    expect(getTemplateUsed(store.getState())).toEqual(mockTemplates[1]);
  });

  it('should return the templates', () => {
    expect(getRawTemplates(store.getState())).toEqual(mockTemplates);
  });

  it('should return the change tracker instance', () => {
    expect(getChangeTrackerInstance(store.getState())).toEqual(changeTracker);
  });

  it('should return the warrant configuration instance', () => {
    expect(getWarrantConfigurationInstance(store.getState())).toEqual(warrantConfiguration);
  });

  it('should return the customization derived target', () => {
    expect(getCustomizationDerivedTargets(store.getState())).toEqual(mockCustomization.derivedTargets);
  });

  it('should return the customization collection function name', () => {
    expect(getCustomizationCollectionFunctionName(store.getState())).toEqual(mockCustomization.collectionFunctionName);
  });
});
