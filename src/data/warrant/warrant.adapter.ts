import moment from 'moment-timezone';
import cloneDeep from 'lodash.clonedeep';
import { warrantModel } from 'xcipio/warrants/warrant-model';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { CaseModel } from 'data/case/case.types';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import { Warrant, WARRANT_LEA_ANY } from './warrant.types';

/**
 * Fields from the warrant to convert to the timezone
 */
const WARRANT_DATE_TIME_FIELDS_TO_CONVERT = [
  'receivedDateTime',
  'startDateTime',
  'stopDateTime',
];

/**
 * Fields from the case to convert to the timezone.
 */
const CASE_DATE_TIME_FIELDS_TO_CONVERT = [
  'startDateTime',
  'stopDateTime',
];

/**
 * Fields from the audit to convert to the timezone.
 */
const AUDIT_DATE_TIME_FIELDS_TO_CONVERT = [
  'date1',
  'date2',
];

/**
 * Internal date format used when converting. Should not be used outside this class.
 */
const DATE_TIME_FORMAT_WITHOUT_TIMEZONE = 'YYYY-MM-DD HH:mm';

/**
 * Internal date format. Should not be used outside this class.
 */
const DATE_FORMAT_WITHOUT_TIMEZONE = 'YYYY-MM-DD';

/**
 * MA is the base64 value for 0.
 * If cfGroup.id is not send to the server, it is going to set it as '0'.
 */
export const CF_GROUP_NONE_ID = 'MA';

const END_OF_DAY = '23:59';

/**
 * Class responsible for adapting the warrant form data
 * before loading and before submitting.
 *
 * It has two separate parts, the first one is *ToLoad methods, which
 * are used before loading any data from the server. And *ToSubmit methods
 * which are used before submitting the date to the server.
 *
 * *ToLoad logic:
 * It will convert the UTC time which represents a date/time in the warrant timezone
 * to a UTC time which visually will represents the date/time in the computer timezone.
 * For instance.
 * Consider the following:
 * - computer timezone as America/Los_Angeles (GMT-7)
 * - warrant timezone as America/New_York (GTM-4)
 * - warrant startTime as '2020-07-29 16:00' in America/New_York time
 *
 * The UTC value for '2020-07-29 16:00' in America/New_York is 2020-07-29T20:00:00.000Z
 * This is converted to a UTC in computer timezone (America/Los_Angeles) that will be
 * displayed in the UI as '2020-07-29 16:00', which is 2020-07-29T23:00:00.000Z
 *
 * *toSubmit logic:
 * It is basically the same, but in this case it will convert the back the America/New_York
 * timezone.
 */
export default class WarrantAdapter {
  /**
   * This function is going to ignore the timezone in the date and
   * use just the year, month, day, hour and minute to create a new
   * date in the specified timezone.
   *
   * @param date the date to convert
   * @param timezone the timezone to use
   * @return the new date in the specified timezone
   */
  static convertFromComputerTimezoneToGivenTimezone(date: string, timezone: string): string {
    const dateWithoutTimezoneInfo = moment(date).format(DATE_TIME_FORMAT_WITHOUT_TIMEZONE);
    const convertedDate = moment.tz(dateWithoutTimezoneInfo, timezone).toDate();
    return convertedDate.toISOString();
  }

  /**
   * This function is going to create the date using the timezone and
   * create a new date using just the year, month, day, hour and minute
   * to create a new date in the specified timezone.
   *
   * @param date the date to convert
   * @param timezone the timezone to use
   * @return the new date in the specified timezone
   */
  static convertFromGivenTimezoneToComputerTimezone(date: string, timezone: string): string {
    const dateWithoutTimezoneInfo = moment.tz(date, timezone).format(DATE_TIME_FORMAT_WITHOUT_TIMEZONE);
    const revertedDate = moment(dateWithoutTimezoneInfo).toDate();
    return revertedDate.toISOString();
  }

  static applyConvertFunctionToField(
    data: any,
    field: string,
    timezone: string,
    convert: CallableFunction,
  ): void {
    const dateTime = data[field];

    if (!timezone || !dateTime) {
      return;
    }

    data[field] = convert(dateTime, timezone);
  }

  static adaptWarrantDateTimeFieldsToTimezone(warrant: any, convertFunction: CallableFunction): any {
    const {
      timeZone,
    } = warrant;

    WARRANT_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
      WarrantAdapter.applyConvertFunctionToField(
        warrant, field, timeZone, convertFunction,
      );
    });

    return warrant;
  }

  static adaptCasesDateTimeFieldsToTimezone(cases: Array<any> = [], convertFunction: CallableFunction): Array<any> {
    cases.forEach((caseEntry: any) => {
      const { timeZone } = caseEntry;
      CASE_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
        WarrantAdapter.applyConvertFunctionToField(
          caseEntry, field, timeZone, convertFunction,
        );
      });
    });

    return cases;
  }

  static adaptAuditDateTimeFieldsToTimezone(
    userAudit: any = {},
    timezone: string,
    convertFunction: CallableFunction,
  ): any {
    AUDIT_DATE_TIME_FIELDS_TO_CONVERT.forEach((field) => {
      WarrantAdapter.applyConvertFunctionToField(
        userAudit, field, timezone, convertFunction,
      );
    });

    return userAudit;
  }

  /**
   * Use this function to adapt the warrant from the form and convert the
   * dates considering the warrant timezone.
   *
   * @param warrant warrant data
   * @return the adapted data
   */
  static adaptWarrantDateTimeFieldsToSubmit(warrant: any): any {
    let warrantClone = cloneDeep(warrant);
    const { cases } = warrantClone;
    const convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
    warrantClone = WarrantAdapter.adaptWarrantDateTimeFieldsToTimezone(warrantClone, convertFunction);
    WarrantAdapter.adaptCasesDateTimeFieldsToTimezone(cases, convertFunction);
    return warrantClone;
  }

  static adaptWarrantToSubmit(warrant: Warrant): Warrant {
    let warrantClone = cloneDeep(warrant);

    warrantClone = WarrantAdapter.adaptWarrantDateTimeFieldsToSubmit(warrantClone);
    warrantClone = WarrantAdapter.adaptWarrantLeaFieldToCfGroup(warrantClone);

    return warrantClone;
  }

  /**
   * Use this function to adapt the user audit from the form and convert the date
   * considering the warrant timezone.
   *
   * @param userAudit the user audit data
   * @param timezone the timezone to use
   */
  static adaptAuditToSubmit(userAudit: any, timezone: string): any {
    let userAuditClone = cloneDeep(userAudit);
    const convertFunction = WarrantAdapter.convertFromComputerTimezoneToGivenTimezone;
    userAuditClone = WarrantAdapter.adaptAuditDateTimeFieldsToTimezone(userAuditClone, timezone, convertFunction);
    return userAuditClone;
  }

  /**
   * Use this function to adapt the warrant from the server and convert the
   * dates so it can be displayed in the warrant timezone.
   *
   * @param warrant data
   * @return the adapted data
   */
  static adaptWarrantToLoad(warrant: Warrant): Warrant {
    let warrantClone = cloneDeep(warrant);
    const convertFunction = WarrantAdapter.convertFromGivenTimezoneToComputerTimezone;
    warrantClone = WarrantAdapter.adaptWarrantDateTimeFieldsToTimezone(warrantClone, convertFunction);
    return warrantClone;
  }

  /**
   * Use this function to adapt the cases from the server and do the following
   * - convert the dates so it can be displayed in the cases timezone.
   * - set isRealHI3 flag in all the cases' HI3 Interfaces so it can be used in HI2/HI3 Interface popup
   *
   * @param cases array of cases
   * @return the adapted cases
   */
  static adaptCasesToLoad(cases: Array<any>): Array<any> {
    let casesClone = cloneDeep(cases);
    const convertFunction = WarrantAdapter.convertFromGivenTimezoneToComputerTimezone;
    casesClone = WarrantAdapter.adaptCasesDateTimeFieldsToTimezone(casesClone, convertFunction);
    casesClone = WarrantAdapter.adaptHI3InterfacesForHI2HI3InterfacePopup(casesClone);
    return casesClone;
  }

  static adaptHI3InterfacesForHI2HI3InterfacePopup(cases: Array<CaseModel>):Array<CaseModel> {
    if (cases) {
      cases.forEach((aCase) => {
          aCase.hi3Interfaces?.forEach((hi3Interface) => {
            hi3Interface.isRealHI3 = true;
            hi3Interface.cfId = aCase.cfId.id;
          });
      });
    }
    return cases;
  }

  static adaptWarrantLeaFieldToCfGroup(warrant: Warrant): Warrant {
    const warrantClone = cloneDeep(warrant);

    if (warrant.lea === WARRANT_LEA_ANY || warrant.lea == null) {
      warrantClone.cfGroup = {
        id: CF_GROUP_NONE_ID,
      };
    } else {
      warrantClone.cfGroup = {
        id: warrant.lea,
      };
    }

    return warrantClone;
  }

  static adaptWarrantToForm(
    warrant: Warrant | null,
    mode: string,
    isWarrantDefaultExpireEnabled?: boolean,
    warrantDefaultExpireDays?: string,
    changeTracker?: ChangeTracker | null,
    warrantId?: string | null,
  ): Warrant {
    if (warrant == null) {
      warrant = warrantModel;
    }

    const clonedWarrant = cloneDeep(warrant);

    WarrantAdapter.setWarrantLea(clonedWarrant, mode);

    if (mode === CONFIG_FORM_MODE.Create && !warrant.stopDateTime && isWarrantDefaultExpireEnabled) {
      WarrantAdapter.setWarrantDefaultStopTime(clonedWarrant, warrantDefaultExpireDays, changeTracker, warrantId);
    }

    return clonedWarrant;
  }

  static setWarrantLea(warrant: Warrant, mode: string): void {
    if (warrant.lea != null || mode === CONFIG_FORM_MODE.Create) {
      // LEA is already defined, there is no need to set it
      return;
    }

    if (warrant.cfGroup?.id != null && warrant.cfGroup.id !== CF_GROUP_NONE_ID) {
      // cfGroup id is defined in the warrant, use its value as lea
      warrant.lea = warrant.cfGroup.id;
      return;
    }

    // Everything else should fallback to 'Any'
    warrant.lea = WARRANT_LEA_ANY;
  }

  static setWarrantDefaultStopTime(
    warrant: Partial<Warrant>,
    defaultStopTimeInDays?: string,
    changeTracker?: ChangeTracker | null,
    warrantId?: string | null,
  ): Partial<Warrant> {
    WarrantAdapter.setInterceptDefaultStopTime(warrant, defaultStopTimeInDays);

    if (changeTracker && warrant.stopDateTime != null) {
      changeTracker.addUpdated(`warrant.${warrantId}.stopDateTime`, new Date(warrant.stopDateTime), 'Stop Time');
    }

    return warrant;
  }

  static setCaseDefaultStopTime(
    caseModel: Partial<CaseModel>,
    defaultStopTimeInDays?: string,
  ): Partial<CaseModel> {
    return WarrantAdapter.setInterceptDefaultStopTime(caseModel, defaultStopTimeInDays);
  }

  static setInterceptDefaultStopTime(
    intercept: { stopDateTime?: string },
    defaultStopTimeInDays?: string,
  ): { stopDateTime?: string } {
    if (defaultStopTimeInDays == null || defaultStopTimeInDays === '') {
      return intercept;
    }

    const currentDate = moment().format(DATE_FORMAT_WITHOUT_TIMEZONE);

    const stopDateTime = moment(`${currentDate} ${END_OF_DAY}`, DATE_TIME_FORMAT_WITHOUT_TIMEZONE)
      .add(defaultStopTimeInDays, 'days')
      .toISOString();

    intercept.stopDateTime = stopDateTime;

    return intercept;
  }
}
