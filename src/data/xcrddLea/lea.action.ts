import { History } from 'history';
import Http from 'shared/http';
import {
  SELECT_LEA, GET_ALL_LEA, CREATE_LEA, UPDATE_LEA, DELETE_LEA,
} from './lea.types';

const http = Http.getHttp();

export function goToLeaNew(history: History): () => void {
  return () => {
    history.push('/xcrdd/lea/new');
  };
}

export function goToLeaEdit(history: History): () => void {
  return () => {
    history.push('/xcrdd/lea/edit');
  };
}

export function goToLeaView(history: History): () => void {
  return () => {
    history.push('/xcrdd/lea/view');
  };
}

export function goToLeaList(history: History): () => void {
  return () => {
    history.push('/xcrdd/lea');
  };
}

export function getSelectedLEA(lea:any):any {
  return {
    type: SELECT_LEA,
    payload: lea,
  };
}

export const getAllLEAs = (body = ''):any => ({ type: GET_ALL_LEA, payload: http.get(`/lea/${body}`) });
export const updateLea = (body:any):any => ({ type: UPDATE_LEA, payload: http.put('/lea/update', body) });
export const createLea = (body:any):any => ({ type: CREATE_LEA, payload: http.post('/lea/post', body) });
export const deleteLea = (leaId:string):any => ({ type: DELETE_LEA, payload: http.delete(`/lea/delete/${leaId}`) });
