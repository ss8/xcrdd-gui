import cloneDeep from 'lodash.clonedeep';

export const adaptToViewOnly = (template:any) => {
  const clonedTemplate = cloneDeep(template);
  const filedTitle = ['general', 'contacts'];
  delete clonedTemplate.general.fields.leaName.footNote;
  delete clonedTemplate.general.fields.ipAddress.footNote;
  filedTitle.forEach((item) => {
    Object.keys(clonedTemplate[item].fields).forEach((val) => {
      clonedTemplate[item].fields[val].type = 'textPlain';
    });
  });

  return clonedTemplate;
};

export const adaptToEdit = (template:any) => {
  const clonedTemplate = cloneDeep(template);
  delete clonedTemplate.general.fields.leaName.footNote;
  clonedTemplate.general.fields.leaName.readOnly = true;
  return clonedTemplate;
};
