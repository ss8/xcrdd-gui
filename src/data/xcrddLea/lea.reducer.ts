import {
  SELECT_LEA, GET_ALL_LEA_FULFILLED, UPDATE_LEA_FULFILLED, CREATE_LEA_FULFILLED, DELETE_LEA_FULFILLED,
} from './lea.types';

const initilaState:any = {
  leaList: [],
  selected: [],
};

const xcrddLEAReducer = (state = initilaState, action:any) => {
  switch (action.type) {
    case GET_ALL_LEA_FULFILLED:
      return { ...state, leaList: action.payload.data.data };
    case SELECT_LEA:
      return { ...state, selected: action.payload || [] };
    default:
      return state;
  }
};

export default xcrddLEAReducer;
