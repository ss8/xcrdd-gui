import MockAdapter from 'axios-mock-adapter';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import { WithAuditDetails } from 'data/types';
import Http from 'shared/http';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import { History } from 'history';
import * as actions from './distributorConfiguration.actions';
import * as types from './distributorConfiguration.types';
import * as api from './distributorConfiguration.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const mockStore = configureMockStore([thunk]);

describe('distributorConfiguration.actions', () => {
  let distributorConfiguration: types.DistributorConfiguration;
  let distributorConfigurationWithAudit: WithAuditDetails<types.DistributorConfiguration>;
  let history: History;
  let auditOnly: WithAuditDetails<void>;
  let store: MockStoreEnhanced<unknown>;

  beforeEach(() => {
    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'type1',
      listeners: [{
        id: '1',
        afProtocol: 'afProtocol',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }],
      destinations: [{
        id: '1',
        ipAddress: '',
        port: '1234',
        requiredState: 'ACTIVE',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        numOfConnections: '1',
        status: 'ACTIVE',
        secInfoId: '',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      }],
    };

    distributorConfigurationWithAudit = {
      data: distributorConfiguration,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.DISTRIBUTOR_CONFIGS,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.DISTRIBUTOR_CONFIGURATION_ADD,
      },
    };

    history = {
      location: {
        pathname: '',
        hash: '',
        search: '',
        state: '',
      },
      length: 1,
      action: 'PUSH',
      listen: jest.fn(),
      go: jest.fn(),
      push: jest.fn(),
      block: jest.fn(),
      replace: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      createHref: jest.fn(),
    };

    store = mockStore({
      afs: {
        afs: [],
        afGroups: [],
        selected: [],
        isDeleting: false,
      },
    });
    axiosMock.onAny().reply((config) => {
      if (config.url === '/distributorConfigs/0' && config.method === 'post') {
        return [200, {
          success: true,
          metadata: {
            recordCount: 0,
            recordsInView: 0,
            idCreated: 'NEW_IP_CREATED',
            uriStatus: null,
          },
          error: null,
          data: [],
        }];
      }

      return [200, { data: [] }];
    });

  });

  it('should create an action to get the distributor configurations', () => {
    const expectedAction = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS,
      payload: api.getDistributorConfigurations('0'),
    };

    expect(actions.getDistributorConfigurations('0')).toEqual(expectedAction);
  });

  it('should create an action to get the distributor configurations with view', () => {
    const expectedAction = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW,
      payload: api.getDistributorConfigurationsWithView('0', 'view'),
    };

    expect(actions.getDistributorConfigurationsWithView('0', 'view')).toEqual(expectedAction);
  });

  it('should create an action to get the distributor configurations by filter', () => {
    const expectedAction = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER,
      payload: api.getDistributorConfigurationsByFilter('0', 'filter'),
    };

    expect(actions.getDistributorConfigurationsByFilter('0', 'filter')).toEqual(expectedAction);
  });

  it('should create an action to get the distributor configuration', () => {
    const expectedAction = {
      type: types.GET_DISTRIBUTOR_CONFIGURATION,
      payload: api.getDistributorConfiguration('0', 'id1'),
    };

    expect(actions.getDistributorConfiguration('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to create the distributor configuration', () => {
    const expectedAction = [{
      type: types.CREATE_DISTRIBUTOR_CONFIGURATION,
      payload: api.createDistributorConfiguration('0', distributorConfigurationWithAudit),
    }];

    expect.assertions(1);
    return store.dispatch(actions.createDistributorConfiguration(history, '0', distributorConfigurationWithAudit) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to update the distributor configuration', () => {
    const expectedAction = [{
      type: types.UPDATE_DISTRIBUTOR_CONFIGURATION,
      payload: api.updateDistributorConfiguration('0', 'id1', distributorConfigurationWithAudit),
    }];

    expect.assertions(1);
    return store.dispatch(actions.updateDistributorConfiguration(history, '0', 'id1', distributorConfigurationWithAudit) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to delete the distributor configuration', () => {
    const expectedAction = [
      {
        type: types.DELETE_DISTRIBUTOR_CONFIGURATION,
        payload: api.deleteDistributorConfiguration('0', 'id1', auditOnly),
      },
      {
        type: types.GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW,
        payload: api.getDistributorConfigurationsWithView('0'),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.deleteDistributorConfiguration('0', 'id1', auditOnly) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  describe('updateAndCopyDistributorConfiguration()', () => {
    it('should dispatch an action to update a distributor configuration then redirect to copy path', () => {
      const newDistributorConfigRef = { id: distributorConfigurationWithAudit.data.id };

      const expectedAction = [
        {
          type: types.UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION,
          payload: api.updateDistributorConfiguration('0', 'id1', distributorConfigurationWithAudit),
        },
        {
          type: types.SELECT_DISTRIBUTOR_CONFIGURATION,
          payload: [distributorConfigurationWithAudit.data],
        },
      ];

      expect.assertions(2);
      return store.dispatch(actions.updateAndCopyDistributorConfiguration(history, '0', 'id1', distributorConfigurationWithAudit) as any).then(() => {
        expect(store.getActions()).toEqual(expectedAction);
        expect(history.push).toBeCalledWith({ pathname: '/systemSettings/distributors/copy', state: newDistributorConfigRef });
      });
    });
  });
  describe('createAndCopyDistributorConfiguration()', () => {
    const mockStoreWithPromiseMiddleware = configureMockStore([thunk, promiseMiddleware]);

    beforeEach(() => {
      store = mockStoreWithPromiseMiddleware({
        afs: {
          afs: [],
          afGroups: [],
          selected: [],
          isDeleting: false,
        },
      });
    });

    it('should dispatch an action to create a distributor configuration then redirect to copy path', () => {

      const newDistributorConfigRef = { id: 'NEW_IP_CREATED' };

      const expectedAction = [
        {
          type: 'CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_PENDING',
        },
        {
          type: types.SELECT_DISTRIBUTOR_CONFIGURATION,
          payload: [newDistributorConfigRef],
        },
      ];

      expect.assertions(3);
      return store.dispatch(actions.createAndCopyDistributorConfiguration(history, '0', distributorConfigurationWithAudit) as any).then(() => {
        expect(store.getActions()[0]).toEqual(expectedAction[0]);
        expect(store.getActions()[2]).toEqual(expectedAction[1]);
        expect(history.push).toBeCalledWith({ pathname: '/systemSettings/distributors/copy', state: newDistributorConfigRef });
      });
    });
  });
});
