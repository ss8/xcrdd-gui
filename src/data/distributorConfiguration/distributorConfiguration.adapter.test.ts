import cloneDeep from 'lodash.clonedeep';
import {
  adaptToTableData,
  adaptToSubmitFromTableData,
  setDistributorConfigurationOperationValuesInAccessFunction,
} from './distributorConfiguration.adapter';
import { DistributorConfigurationTableData, DistributorConfiguration } from './distributorConfiguration.types';

describe('distributorConfiguration.adapter', () => {
  let distributorConfigs: DistributorConfiguration[];
  let distributorConfigsTableData: DistributorConfigurationTableData[];

  beforeEach(() => {
    distributorConfigs = [{
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      listeners: [{
        id: '2',
        afProtocol: 'afProtocol1',
        ipAddress: '11.11.11.11',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '1111',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId1',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }, {
          afId: 'afId2',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }, {
        id: '3',
        afProtocol: 'afProtocol2',
        ipAddress: '22.22.22.22',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '2222',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId1',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId3',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }],
      destinations: [{
        id: '4',
        ipAddress: '',
        port: '1234',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        numOfConnections: '1',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        secInfoId: 'NONE',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      }],
    }, {
      id: 'id2',
      name: 'name2',
      instanceId: '5',
      moduleName: 'moduleName2',
      afType: 'afType2',
      listeners: [],
      destinations: [],
    }];

    const {
      listeners,
      ...rest
    } = cloneDeep(distributorConfigs[0]);

    distributorConfigsTableData = [{
      ...rest,
      id: '0.id1',
      afProtocol: 'afProtocol1',
      ipAddress: '11.11.11.11',
      port: '1111',
      listener: listeners[0],
    }, {
      ...rest,
      id: '1.id1',
      afProtocol: 'afProtocol2',
      ipAddress: '22.22.22.22',
      port: '2222',
      listener: listeners[1],
    }];
  });

  describe('adaptToTableData()', () => {
    it('should return an empty array if distributor configs are empty', () => {
      expect(adaptToTableData([])).toEqual([]);
    });

    it('should return the distributor configs adapted to be used in the table', () => {
      expect(adaptToTableData(distributorConfigs)).toEqual(distributorConfigsTableData);
    });
  });

  describe('adaptToSubmitFromTableData()', () => {
    it('should return an empty array if distributor configs are empty', () => {
      expect(adaptToSubmitFromTableData([], [])).toEqual([]);
    });

    it('should return an empty array if there is no change to be submitted', () => {
      expect(adaptToSubmitFromTableData(distributorConfigsTableData, distributorConfigs)).toEqual([]);
    });

    it('should return the distributor config adapted to be submitted', () => {
      distributorConfigsTableData[0].listener.afWhitelist[0].afAddress = '11.11.11.11';
      expect(adaptToSubmitFromTableData(distributorConfigsTableData, distributorConfigs)).toEqual([
        {
          ...distributorConfigs[0],
          listeners: [{
            ...distributorConfigs[0].listeners[0],
            afWhitelist: [{
              ...distributorConfigs[0].listeners[0].afWhitelist[0],
              afAddress: '11.11.11.11',
            }, {
              ...distributorConfigs[0].listeners[0].afWhitelist[1],
            }],
          }, {
            ...distributorConfigs[0].listeners[1],
          }],
        },
      ]);
    });

    it('should set the operation fields values as expected', () => {
      const originalDistributorConfigs = cloneDeep(distributorConfigs);

      originalDistributorConfigs[0].listeners[0].afWhitelist = [{
        afId: 'afId1',
        afAddress: '13.13.13.13',
        afName: 'afName',
      }];

      originalDistributorConfigs[0].listeners[1].afWhitelist.push({
        afId: 'afId4',
        afAddress: '55.55.55.55',
        afName: 'afName',
      });

      const expectedDistributorConfig = {
        ...distributorConfigs[0],
        listeners: [{
          ...distributorConfigs[0].listeners[0],
          operation: 'UPDATE',
          afWhitelist: [{
            ...originalDistributorConfigs[0].listeners[0].afWhitelist[0],
            afAddress: '12.12.12.12',
            operation: 'UPDATE',
          }, {
            ...distributorConfigs[0].listeners[0].afWhitelist[1],
            operation: 'CREATE',
          }],
        }, {
          ...distributorConfigs[0].listeners[1],
          operation: 'UPDATE',
          afWhitelist: [{
            ...distributorConfigs[0].listeners[1].afWhitelist[0],
            operation: 'NO_OPERATION',
          }, {
            ...originalDistributorConfigs[0].listeners[1].afWhitelist[1],
            operation: 'DELETE',
          }],
        }],
      };

      expect(
        setDistributorConfigurationOperationValuesInAccessFunction(
          distributorConfigs[0],
          originalDistributorConfigs[0],
        ),
      ).toEqual(expectedDistributorConfig);
    });
  });
});
