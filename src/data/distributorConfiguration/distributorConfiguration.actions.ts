import lodashGet from 'lodash.get';
import { WithAuditDetails } from 'data/types';
import { History } from 'history';
import { Dispatch, Action } from 'redux';
import { RootState } from 'data/root.reducers';
import { showSuccessMessage, showError, TOAST_TIMEOUT } from 'shared/toaster';
import { ThunkDispatch } from 'redux-thunk';
import * as api from './distributorConfiguration.api';
import { extractErrorMessage } from '../../utils';
import {
  GET_DISTRIBUTOR_CONFIGURATIONS,
  GET_DISTRIBUTOR_CONFIGURATION,
  CREATE_DISTRIBUTOR_CONFIGURATION,
  UPDATE_DISTRIBUTOR_CONFIGURATION,
  DELETE_DISTRIBUTOR_CONFIGURATION,
  DistributorConfiguration,
  GetDistributorConfigurationsAction,
  GetDistributorConfigurationAction,
  DeleteDistributorConfigurationThunkAction,
  GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER,
  GetDistributorConfigurationsByFilterAction,
  SELECT_DISTRIBUTOR_CONFIGURATION,
  SelectDistributorConfigurationAction,
  GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW,
  GetDistributorConfigurationsWithViewAction,
  CreateDistributorConfigurationThunkAction,
  UpdateDistributorConfigurationThunkAction,
  UpdateAndCopyDistributorConfigurationThunkAction,
  UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION,
  CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION,
  CreateAndCopyDistributorConfigurationThunkAction,
} from './distributorConfiguration.types';
import { SYSTEM_SETTINGS_ROUTE, DISTRIBUTORS_ROUTE } from '../../constants';

export function goToDistributorConfigList(history: History): () => void {
  return () => {
    history.push(`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}`);
  };
}

export function goToDistributorConfigCreate(history: History): () => void {
  return () => {
    history.push(`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/create`);
  };
}

export function goToDistributorConfigEdit(history: History, id:string): () => void {
  return () => {
    history.push({ pathname: `${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/edit`, state: { id } });
  };
}

export function goToDistributorConfigCopy(history: History, id:string): () => void {
  return () => {
    history.push({ pathname: `${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/copy`, state: { id } });
  };
}

export function goToDistributorConfigView(history: History, id:string): () => void {
  return () => {
    history.push({ pathname: `${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/view`, state: { id } });
  };
}

export function getDistributorConfigurations(deliveryFunctionId: string): GetDistributorConfigurationsAction {
  return {
    type: GET_DISTRIBUTOR_CONFIGURATIONS,
    payload: api.getDistributorConfigurations(deliveryFunctionId),
  };
}
export function getDistributorConfigurationsWithView(
  deliveryFunctionId: string,
  view = 'listeners',
): GetDistributorConfigurationsWithViewAction {
  return {
    type: GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW,
    payload: api.getDistributorConfigurationsWithView(deliveryFunctionId, view),
  };
}

export function getDistributorConfigurationsByFilter(
  deliveryFunctionId: string,
  filter: string,
): GetDistributorConfigurationsByFilterAction {
  return {
    type: GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER,
    payload: api.getDistributorConfigurationsByFilter(deliveryFunctionId, filter),
  };
}

export function getDistributorConfiguration(deliveryFunctionId: string, id: string): GetDistributorConfigurationAction {
  return {
    type: GET_DISTRIBUTOR_CONFIGURATION,
    payload: api.getDistributorConfiguration(deliveryFunctionId, id),
  };
}

export function createDistributorConfiguration(
  history:History,
  deliveryFunctionId: string,
  body: WithAuditDetails<DistributorConfiguration>,
): CreateDistributorConfigurationThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: CREATE_DISTRIBUTOR_CONFIGURATION,
        payload: api.createDistributorConfiguration(deliveryFunctionId, body),
      });
      dispatch(goToDistributorConfigList(history));
      showSuccessMessage('Distributor configuration successfully created.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create Distributor Configuration: ${errorMessage}.`, TOAST_TIMEOUT);
    }
  };
}

export function createAndCopyDistributorConfiguration(
  history:History,
  deliveryFunctionId: string,
  body: WithAuditDetails<DistributorConfiguration>,
): CreateAndCopyDistributorConfigurationThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {

      const result: unknown = await dispatch({
        type: CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION,
        payload: api.createDistributorConfiguration(deliveryFunctionId, body),
      });

      const createdDistributorConfigurationId = lodashGet(result, ['action', 'payload', 'data', 'metadata', 'idCreated']);

      dispatch(
        selectDistributorConfiguration([{ id: createdDistributorConfigurationId } as DistributorConfiguration]),
      );
      dispatch(goToDistributorConfigCopy(history, createdDistributorConfigurationId));
      showSuccessMessage('Distributor configuration successfully created.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create Distributor Configuration: ${errorMessage}.`, TOAST_TIMEOUT);
    }
  };
}

export function updateDistributorConfiguration(
  history:History,
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<DistributorConfiguration>,
): UpdateDistributorConfigurationThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: UPDATE_DISTRIBUTOR_CONFIGURATION,
        payload: api.updateDistributorConfiguration(deliveryFunctionId, id, body),
      });
      dispatch(goToDistributorConfigList(history));
      showSuccessMessage('Distributor Configuration successfully updated.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update Distributor Configuration: ${errorMessage}.`, TOAST_TIMEOUT);
    }
  };
}

export function updateAndCopyDistributorConfiguration(
  history:History,
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<DistributorConfiguration>,
): UpdateAndCopyDistributorConfigurationThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {

      await dispatch({
        type: UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION,
        payload: api.updateDistributorConfiguration(deliveryFunctionId, id, body),
      });

      dispatch(selectDistributorConfiguration([body.data]));
      dispatch(goToDistributorConfigCopy(history, id));
      showSuccessMessage('Distributor Configuration successfully updated.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update Distributor Configuration: ${errorMessage}.`, TOAST_TIMEOUT);
    }
  };
}

export function deleteDistributorConfiguration(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): DeleteDistributorConfigurationThunkAction {
  return async (dispatch: Dispatch, getState: ()=> RootState) => {
    const selectedDistributorConfiguration = getState()?.distributorConfiguration?.selected[0];
    try {
      await dispatch({
        type: DELETE_DISTRIBUTOR_CONFIGURATION,
        payload: api.deleteDistributorConfiguration(deliveryFunctionId, id, body),
      });
      showSuccessMessage(`${selectedDistributorConfiguration?.name} successfully deleted.`, TOAST_TIMEOUT);
    } catch (e) {
      showError(`Unable to delete ${selectedDistributorConfiguration?.name}.`, TOAST_TIMEOUT);
    } finally {
      // Refresh the list of dcs
      dispatch(getDistributorConfigurationsWithView(deliveryFunctionId));
    }
  };
}

export function selectDistributorConfiguration(
  distributorConfiguration: DistributorConfiguration[],
): SelectDistributorConfigurationAction {
  return {
    type: SELECT_DISTRIBUTOR_CONFIGURATION,
    payload: distributorConfiguration,
  };
}
