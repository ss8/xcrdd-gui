import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getDistributorConfigurations,
  getDistributorConfiguration,
  getFilteredDistributorConfigurations,
  getDistributorConfigurationsByAccessFunctionType,
} from './distributorConfiguration.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './distributorConfiguration.types';

describe('distributorConfiguration.selectors', () => {
  let distributorConfiguration: types.DistributorConfiguration;
  let distributorConfigurations: types.DistributorConfiguration[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'ERK_SMF',
      listeners: [{
        id: '2',
        afProtocol: 'afProtocol',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'status1',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }],
      destinations: [{
        id: '3',
        ipAddress: '',
        port: '1234',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        numOfConnections: '1',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        secInfoId: 'NONE',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      }],
    };

    distributorConfigurations = [
      distributorConfiguration,
      {
        id: 'id2',
        name: 'name2',
        instanceId: '2',
        moduleName: 'moduleName1',
        afType: 'ERK_AMF',
        listeners: [],
        destinations: [],
      },
      {
        id: 'id3',
        name: 'name4',
        instanceId: '4',
        moduleName: 'moduleName1',
        afType: 'ERK_AMF',
        listeners: [],
        destinations: [],
      },
    ];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of distributor configurations', () => {
    const action: types.DistributorConfigurationActionTypes = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED,
      payload: buildAxiosServerResponse(distributorConfigurations),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getDistributorConfigurations(store.getState())).toEqual(distributorConfigurations);
  });

  it('should return the list of filtered distributor configurations', () => {
    const action: types.DistributorConfigurationActionTypes = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(distributorConfigurations),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getFilteredDistributorConfigurations(store.getState())).toEqual(distributorConfigurations);
  });

  it('should return the single distributor configuration', () => {
    const action: types.DistributorConfigurationActionTypes = {
      type: types.GET_DISTRIBUTOR_CONFIGURATION_FULFILLED,
      payload: buildAxiosServerResponse([distributorConfiguration]),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getDistributorConfiguration(store.getState())).toEqual(distributorConfiguration);
  });

  it('should return the distributors configuration by access function type', () => {
    const action: types.DistributorConfigurationActionTypes = {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED,
      payload: buildAxiosServerResponse(distributorConfigurations),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getDistributorConfigurationsByAccessFunctionType(store.getState())).toEqual({
      ERK_SMF: [distributorConfigurations[0]],
      ERK_AMF: [distributorConfigurations[1], distributorConfigurations[2]],
    });
  });
});
