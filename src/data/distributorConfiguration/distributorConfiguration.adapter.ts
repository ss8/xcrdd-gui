import cloneDeep from 'lodash.clonedeep';
import isEqual from 'lodash.isequal';
import {
  FormTemplateOption,
  FormTemplateSection,
} from 'data/template/template.types';
import { isNullOrUndefined } from 'util';
import {
  DistributorConfiguration,
  DistributorConfigurationTableData,
  DistributorConfigurationDestination,
  DistributorConfigurationListenerAfWhiteList,
  DistributorConfigurationListener,
} from './distributorConfiguration.types';

const ID_SEPARATOR = '.';

const compare = <T extends { id: string }>(a: T, b: T) => a.id.localeCompare(b.id);

export function initializeForCopy(distributorConfig: DistributorConfiguration): DistributorConfiguration {
  // move this to adaptToCopy
  const clonedDistributorConfig = cloneDeep(distributorConfig);
  clonedDistributorConfig.name = `Copy of ${clonedDistributorConfig.name}`;
  clonedDistributorConfig.id = '';
  clonedDistributorConfig.destinations = clonedDistributorConfig.destinations?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationDestination),
    id: '',
    port: '',
  }));
  clonedDistributorConfig.listeners = clonedDistributorConfig.listeners?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationListener),
    id: '',
    port: '',
  }));

  return clonedDistributorConfig;
}

export function adaptToCreate(distributorConfig: DistributorConfiguration): DistributorConfiguration {
  // move this to adaptToCopy
  const clonedDistributorConfig = cloneDeep(distributorConfig);
  clonedDistributorConfig.id = '';
  clonedDistributorConfig.destinations = clonedDistributorConfig.destinations?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationDestination),
    operation: 'CREATE',
  }));
  clonedDistributorConfig.listeners = clonedDistributorConfig.listeners?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationListener),
    operation: 'CREATE',
  }));
  clonedDistributorConfig.listeners?.forEach((listener: DistributorConfigurationListener) => {
    listener.afWhitelist = listener.afWhitelist?.map((afWhitelist) => ({
      ...afWhitelist,
      operation: 'CREATE',
    }));
  });
  clonedDistributorConfig.operation = 'CREATE';

  return clonedDistributorConfig;
}

export function adaptToCopy(distributorConfig: DistributorConfiguration): DistributorConfiguration {
  // move this to adaptToCopy
  const clonedDistributorConfig = cloneDeep(distributorConfig);
  clonedDistributorConfig.id = '';
  // remove the deleted destinations
  clonedDistributorConfig.destinations = clonedDistributorConfig.destinations.filter(({ operation }) => (operation !== 'DELETE'));

  // remove the deleted listeners
  clonedDistributorConfig.listeners = clonedDistributorConfig.listeners.filter(({ operation }) => (operation !== 'DELETE'));

  // set operations
  clonedDistributorConfig.destinations = clonedDistributorConfig.destinations?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationDestination),
    id: '',
    operation: 'CREATE',
  }));
  clonedDistributorConfig.listeners = clonedDistributorConfig.listeners?.map(({ ...rest }) => ({
    ...(rest as DistributorConfigurationListener),
    id: '',
    operation: 'CREATE',
  }));
  clonedDistributorConfig.listeners?.forEach((listener: DistributorConfigurationListener) => {
    listener.afWhitelist = listener.afWhitelist?.map((afWhitelist) => ({
      ...afWhitelist,
      operation: 'CREATE',
    }));
  });
  clonedDistributorConfig.operation = 'CREATE';

  return clonedDistributorConfig;
}

export function adaptToUpdate(
  distributorConfig: DistributorConfiguration,
  originalDistributorConfig: DistributorConfiguration | null,
): DistributorConfiguration {
  // move this to adaptToCopy
  const clonedDistributorConfig = cloneDeep(distributorConfig);
  // On Edit form: when updating existing listener operation="UPDATE", but adding a new listener operation="CREATE"
  clonedDistributorConfig.destinations = clonedDistributorConfig.destinations?.map(({ operation, ...rest }) => ({
    ...(rest as DistributorConfigurationDestination),
    operation: isNullOrUndefined(operation) ? 'UPDATE' : operation,
  }));
  clonedDistributorConfig.listeners = clonedDistributorConfig.listeners?.map(({ operation, ...rest }) => ({
    ...(rest as DistributorConfigurationListener),
    operation: isNullOrUndefined(operation) ? 'UPDATE' : operation,
  }));
  clonedDistributorConfig.listeners?.forEach((listener: DistributorConfigurationListener, listenerIndex) => {
    const originalListener = originalDistributorConfig?.listeners[listenerIndex];
    setWhiteListOperationValueBasedInOriginal(listener.afWhitelist, originalListener?.afWhitelist);
  });
  clonedDistributorConfig.operation = 'UPDATE';

  return clonedDistributorConfig;
}

export function adaptToTableData(
  distributorConfigs: DistributorConfiguration[],
): DistributorConfigurationTableData[] {
  const clonedDistributorConfigs = cloneDeep(distributorConfigs);
  const distributorConfigsTableData: DistributorConfigurationTableData[] = [];

  clonedDistributorConfigs.forEach((distributorConfig) => {
    const {
      listeners,
      ...rest
    } = distributorConfig;

    listeners.forEach((listener, index) => {
      distributorConfigsTableData.push({
        ...rest,
        id: `${index}${ID_SEPARATOR}${rest.id}`,
        afProtocol: listener.afProtocol,
        ipAddress: listener.ipAddress,
        port: listener.port,
        listener,
      });
    });
  });

  return distributorConfigsTableData;
}

export function adaptToSubmitFromTableData(
  distributorConfigsTableData: DistributorConfigurationTableData[],
  originalDistributorConfigs: DistributorConfiguration[],
): DistributorConfiguration[] {
  const clonedDistributorConfigsTableData = cloneDeep(distributorConfigsTableData);

  let distributorConfigs: DistributorConfiguration[] = [];
  const distributorConfigsMap: {[key: string]: DistributorConfiguration} = {};

  clonedDistributorConfigsTableData.forEach((distributorConfig) => {
    const {
      id,
      listener,
      ipAddress,
      afProtocol,
      port,
      ...rest
    } = distributorConfig;

    const distributorConfigId = id.split(ID_SEPARATOR)[1];

    if (distributorConfigsMap[distributorConfigId] == null) {
      distributorConfigsMap[distributorConfigId] = {
        ...rest,
        id: distributorConfigId,
        listeners: [],
      };

      distributorConfigs.push(distributorConfigsMap[distributorConfigId]);
    }

    distributorConfigsMap[distributorConfigId].listeners.push(listener);
  });

  // Ensure both configs are in the same order
  distributorConfigs.sort(compare);
  originalDistributorConfigs.sort(compare);

  distributorConfigs = distributorConfigs.filter((distributorConfig, index) => {
    const originalDistributorConfig = originalDistributorConfigs[index];
    return !isEqual(distributorConfig, originalDistributorConfig);
  });

  return distributorConfigs;
}

export function setDistributorConfigurationOperationValuesInAccessFunction(
  distributorConfig: DistributorConfiguration,
  originalDistributorConfig: DistributorConfiguration | undefined,
): DistributorConfiguration {
  const clonedDistributorConfig = cloneDeep(distributorConfig);

  setListenerWhiteListOperation(clonedDistributorConfig, originalDistributorConfig);

  return clonedDistributorConfig;
}

function setListenerWhiteListOperation(
  distributorConfig: DistributorConfiguration,
  originalDistributorConfig: DistributorConfiguration | undefined,
) {
  // Ensure both listeners are in the same order
  distributorConfig.listeners.sort(compare);
  originalDistributorConfig?.listeners.sort(compare);

  distributorConfig.listeners.forEach((listener, listenerIndex) => {
    const originalListener = originalDistributorConfig?.listeners[listenerIndex];
    setWhiteListOperationValueBasedInOriginal(listener.afWhitelist, originalListener?.afWhitelist);
    setListenerOperationBasedInWhiteListChanges(listener);
  });
}

function setWhiteListOperationValueBasedInOriginal(
  afWhiteList: DistributorConfigurationListenerAfWhiteList[] | undefined,
  originalAfWhiteList: DistributorConfigurationListenerAfWhiteList[] | undefined,
) {
  // Check which afWhiteList will be marked as CREATE, UPDATE or NO_OPERATION
  afWhiteList?.forEach((afWhiteListEntry) => {
    const originalAfWhiteListEntry = originalAfWhiteList?.find(({ afId }) => afId === afWhiteListEntry.afId);
    if (originalAfWhiteListEntry == null) {
      afWhiteListEntry.operation = 'CREATE';
    } else if (!isEqual(afWhiteListEntry, originalAfWhiteListEntry)) {
      afWhiteListEntry.operation = 'UPDATE';
    } else {
      afWhiteListEntry.operation = 'NO_OPERATION';
    }
  });

  // Check which afWhiteList entry will be marked as DELETE
  const afWhiteListEntriesToDelete =
    originalAfWhiteList?.filter((originalAfWhiteListEntry) => {
      return !afWhiteList?.find(({ afId }) => afId === originalAfWhiteListEntry.afId);
    })
      .map((originalAfWhiteListEntry) => ({
        ...originalAfWhiteListEntry,
        operation: 'DELETE',
      }));

  if (afWhiteListEntriesToDelete) {
    afWhiteList?.push(...(afWhiteListEntriesToDelete as DistributorConfigurationListenerAfWhiteList[]));
  }
}

export function setFieldOptions(
  template: FormTemplateSection,
  options: FormTemplateOption[],
  field: 'moduleName' | 'instanceId' | 'afType' | 'nodeName' | 'afProtocol' | 'secInfoId',
): void {
  const firstOption = template.fields[field].options?.[0];
  if (firstOption) {
    template.fields[field].options = [firstOption];
  }

  if (options.length > 0) {
    template.fields[field].options?.push(...options);
    template.fields[field].disabled = false;
  } else {
    template.fields[field].disabled = true;
  }
}

function setListenerOperationBasedInWhiteListChanges(listener: DistributorConfigurationListener) {
  const hasAnyChange = listener.afWhitelist.some(({ operation }) => operation === 'CREATE' || operation === 'DELETE' || operation === 'UPDATE');

  if (hasAnyChange) {
    listener.operation = 'UPDATE';
  } else {
    listener.operation = 'NO_OPERATION';
  }
}

/* ensures that only required keys are introduced in the object */
export const toDistributorConfigurationListenerWhiteList = ({
  afAddress,
  afId,
  afName,
}: DistributorConfigurationListenerAfWhiteList): DistributorConfigurationListenerAfWhiteList => ({
  afAddress,
  afId,
  afName,
});
