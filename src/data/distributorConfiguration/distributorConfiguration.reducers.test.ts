import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './distributorConfiguration.reducer';
import * as types from './distributorConfiguration.types';
import * as api from './distributorConfiguration.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('distributorConfiguration.reducer', () => {
  let distributorConfiguration: types.DistributorConfiguration;
  let distributorConfigurations: types.DistributorConfiguration[];

  beforeEach(() => {
    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'ERK_UPF',
      listeners: [{
        id: '2',
        afProtocol: 'afProtocul1',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '10',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName1',
        }],
      }],
      destinations: [{
        id: '3',
        ipAddress: '',
        port: '1234',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        numOfConnections: '1',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        secInfoId: 'NONE',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      }],
    };

    distributorConfigurations = [distributorConfiguration];

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED,
      payload: buildAxiosServerResponse(distributorConfigurations),
    })).toEqual({
      entities: distributorConfigurations,
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED,
      payload: buildAxiosServerResponse(distributorConfigurations),
    })).toEqual({
      entities: distributorConfigurations,
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [distributorConfiguration],
      selected: [],
      isDeleting: false,
    }, {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER,
      payload: api.getDistributorConfigurationsByFilter('0', 'filter'),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    }, {
      type: types.GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse([distributorConfiguration]),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [distributorConfiguration],
      selected: [],
      isDeleting: false,
    });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATION', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    }, {
      type: types.GET_DISTRIBUTOR_CONFIGURATION,
      payload: api.getDistributorConfiguration('0', 'id1'),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    });
  });

  it('should handle GET_DISTRIBUTOR_CONFIGURATION_FULFILLED', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    }, {
      type: types.GET_DISTRIBUTOR_CONFIGURATION_FULFILLED,
      payload: buildAxiosServerResponse([distributorConfiguration]),
    })).toEqual({
      entities: [],
      single: distributorConfiguration,
      filteredEntities: [],
      selected: [],
      isDeleting: false,
    });
  });
});
