import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse, Operation } from 'data/types';
import { ThunkAction } from 'redux-thunk';
import { RootState } from 'data/root.reducers';

export const API_PATH_DISTRIBUTOR_CONFIGURATIONS = '/distributorConfigs';

export const DISTRIBUTOR_CONFIGURATION_USER_CATEGORY_SETTINGS = 'DistributorConfigurationSettings';

export const GO_TO_DISTRIBUTOR_CONFIGURATION_CREATE = 'GO_TO_DISTRIBUTOR_CONFIGURATION_CREATE';
export const GO_TO_DISTRIBUTOR_CONFIGURATION_EDIT = 'GO_TO_DISTRIBUTOR_CONFIGURATION_EDIT';
export const GO_TO_DISTRIBUTOR_CONFIGURATION_VIEW = 'GO_TO_DISTRIBUTOR_CONFIGURATION_VIEW';
export const GO_TO_DISTRIBUTOR_CONFIGURATION_COPY = 'GO_TO_DISTRIBUTOR_CONFIGURATION_COPY';
export const GET_DISTRIBUTOR_CONFIGURATIONS = 'GET_DISTRIBUTOR_CONFIGURATIONS';
export const GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED = 'GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED';
export const GET_DISTRIBUTOR_CONFIGURATIONS_REJECTED = 'GET_DISTRIBUTOR_CONFIGURATIONS_REJECTED';
export const GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW = 'GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW';
export const GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED = 'GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED';
export const GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_REJECTED = 'GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_REJECTED';
export const GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER = 'GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER';
export const GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED = 'GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED';
export const GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_REJECTED = 'GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_REJECTED';
export const GET_DISTRIBUTOR_CONFIGURATION = 'GET_DISTRIBUTOR_CONFIGURATION';
export const GET_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'GET_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const GET_DISTRIBUTOR_CONFIGURATION_REJECTED = 'GET_DISTRIBUTOR_CONFIGURATION_REJECTED';
export const CREATE_DISTRIBUTOR_CONFIGURATION = 'CREATE_DISTRIBUTOR_CONFIGURATION';
export const CREATE_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'CREATE_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const CREATE_DISTRIBUTOR_CONFIGURATION_REJECTED = 'CREATE_DISTRIBUTOR_CONFIGURATION_REJECTED';
export const UPDATE_DISTRIBUTOR_CONFIGURATION = 'UPDATE_DISTRIBUTOR_CONFIGURATION';
export const UPDATE_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'UPDATE_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const UPDATE_DISTRIBUTOR_CONFIGURATION_REJECTED = 'UPDATE_DISTRIBUTOR_CONFIGURATION_REJECTED';
export const DELETE_DISTRIBUTOR_CONFIGURATION = 'DELETE_DISTRIBUTOR_CONFIGURATION';
export const DELETE_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'DELETE_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const DELETE_DISTRIBUTOR_CONFIGURATION_REJECTED = 'DELETE_DISTRIBUTOR_CONFIGURATION_REJECTED';
export const SELECT_DISTRIBUTOR_CONFIGURATION = 'SELECT_DISTRIBUTOR_CONFIGURATION';
export const CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION = 'CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION';
export const CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED = 'CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED';
export const UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION = 'UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION';
export const UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED = 'UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED';
export const UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED = 'UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED';

export enum Transport {
  TCP = 'TCP',
  UDP = 'UDP'
}

export enum RequiredState {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export enum Encryption {
  NONE = 'NONE',
  TPKT = 'TPKT'
}
export enum DistributorConfigurationPrivileges {
  View = 'dc_access',
  Edit = 'dc_access',
  Delete = 'dc_access',
}

export interface DistributorConfigurationListenerAfWhiteList {
  afId: string
  afName: string
  afAddress: string
  operation?: Operation
}

export interface DistributorConfiguration {
  id: string
  name: string
  moduleName: string
  instanceId: string
  afType: string
  nodeName?: string
  listeners: DistributorConfigurationListener[]
  destinations: DistributorConfigurationDestination[]
  owner?: string
  operation?: Operation
}

export interface DistributorConfigurationListener {
  id: string
  afProtocol: string
  ipAddress: string
  port: string
  keepaliveTimeout: string
  maxConcurrentConnection: string
  secInfoId: string
  transport: 'TCP' | 'UDP'
  requiredState: 'ACTIVE' | 'INACTIVE'
  status?: string
  synchronizationHeader: 'NONE' | 'TPKT'
  numberOfParsers: string
  afWhitelist: DistributorConfigurationListenerAfWhiteList[]
  operation?: Operation
}

export interface DistributorConfigurationDestination {
  id: string
  ipAddress: string
  port: string
  keepaliveInterval: string
  keepaliveRetries: string
  numOfConnections: string
  requiredState: 'ACTIVE' | 'INACTIVE'
  status?: string
  secInfoId: string
  transport: string
  protocol: string
  operation?: Operation
}

export interface MainFormData {
  id: string,
  name: string
  moduleName: string
  instanceId: string
  afType: string
  nodeName?: string
}

export interface DistributorConfigurationState {
  entities: DistributorConfiguration[]
  filteredEntities: DistributorConfiguration[]
  single: DistributorConfiguration | null
  selected: DistributorConfiguration[],
  isDeleting: boolean,
}

export interface GoToDistributorConfigurationCreateAction extends AnyAction {
  type: typeof GO_TO_DISTRIBUTOR_CONFIGURATION_CREATE
}

export interface GoToDistributorConfigurationEditAction extends AnyAction {
  type: typeof GO_TO_DISTRIBUTOR_CONFIGURATION_EDIT
}

export interface GoToDistributorConfigurationViewAction extends AnyAction {
  type: typeof GO_TO_DISTRIBUTOR_CONFIGURATION_VIEW
}

export interface GoToDistributorConfigurationCopyAction extends AnyAction {
  type: typeof GO_TO_DISTRIBUTOR_CONFIGURATION_COPY
}

export interface DistributorConfigurationTableData extends Omit<DistributorConfiguration, 'listeners'> {
  listener: DistributorConfigurationListener
  afProtocol: string
  ipAddress: string
  port: string
  moduleName: string
}

export interface GetDistributorConfigurationsAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS
  payload: AxiosPromise<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationsFulfilledAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationsWithViewAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW
  payload: AxiosPromise<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationsWithViewFulfilledAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED
  payload: AxiosResponse<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationsByFilterAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER
  payload: AxiosPromise<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationsByFilterFulfilledAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED
  payload: AxiosResponse<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<DistributorConfiguration>>
}

export interface GetDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof GET_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<DistributorConfiguration>>
}

export interface CreateDistributorConfigurationThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface CreateDistributorConfigurationAction extends AnyAction {
  type: typeof CREATE_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface CreateDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof CREATE_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface CreateDistributorConfigurationRejectedAction extends AnyAction {
  type: typeof CREATE_DISTRIBUTOR_CONFIGURATION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface CreateAndCopyDistributorConfigurationThunkAction
  extends ThunkAction<void, RootState, void, AnyAction> { }

export interface CreateAndCopyDistributorConfigurationAction extends AnyAction {
  type: typeof CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface CreateAndCopyDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface CreateAndCopyDistributorConfigurationRejectedAction extends AnyAction {
  type: typeof CREATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface UpdateDistributorConfigurationThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface UpdateDistributorConfigurationAction extends AnyAction {
  type: typeof UPDATE_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface UpdateDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof UPDATE_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}
export interface UpdateDistributorConfigurationRejectedAction extends AnyAction {
  type: typeof UPDATE_DISTRIBUTOR_CONFIGURATION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface UpdateAndCopyDistributorConfigurationThunkAction
  extends ThunkAction<void, RootState, void, AnyAction> { }

export interface UpdateAndCopyDistributorConfigurationAction extends AnyAction {
  type: typeof UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface UpdateAndCopyDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface UpdateAndCopyDistributorConfigurationRejectedAction extends AnyAction {
  type: typeof UPDATE_AND_COPY_DISTRIBUTOR_CONFIGURATION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteDistributorConfigurationThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface DeleteDistributorConfigurationAction extends AnyAction {
  type: typeof DELETE_DISTRIBUTOR_CONFIGURATION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface DeleteDistributorConfigurationFulfilledAction extends AnyAction {
  type: typeof DELETE_DISTRIBUTOR_CONFIGURATION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteDistributorConfigurationRejectedAction extends AnyAction {
  type: typeof DELETE_DISTRIBUTOR_CONFIGURATION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface SelectDistributorConfigurationAction extends AnyAction {
  type: typeof SELECT_DISTRIBUTOR_CONFIGURATION
  payload: DistributorConfiguration[]
}

export type DistributorConfigurationActionTypes =
  GoToDistributorConfigurationCreateAction |
  GoToDistributorConfigurationEditAction |
  GoToDistributorConfigurationViewAction |
  GoToDistributorConfigurationCopyAction |
  GetDistributorConfigurationsAction |
  GetDistributorConfigurationsFulfilledAction |
  GetDistributorConfigurationsWithViewAction |
  GetDistributorConfigurationsWithViewFulfilledAction |
  GetDistributorConfigurationsByFilterAction |
  GetDistributorConfigurationsByFilterFulfilledAction |
  GetDistributorConfigurationAction |
  GetDistributorConfigurationFulfilledAction |
  CreateDistributorConfigurationAction |
  CreateDistributorConfigurationFulfilledAction |
  UpdateDistributorConfigurationAction |
  UpdateDistributorConfigurationFulfilledAction |
  DeleteDistributorConfigurationAction |
  DeleteDistributorConfigurationFulfilledAction |
  DeleteDistributorConfigurationRejectedAction |
  SelectDistributorConfigurationAction |
  CreateAndCopyDistributorConfigurationAction |
  CreateAndCopyDistributorConfigurationFulfilledAction |
  UpdateAndCopyDistributorConfigurationAction |
  UpdateAndCopyDistributorConfigurationFulfilledAction;
