import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import { WithAuditDetails } from 'data/types';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import * as api from './distributorConfiguration.api';
import {
  DistributorConfiguration,
  API_PATH_DISTRIBUTOR_CONFIGURATIONS,
} from './distributorConfiguration.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('distributorConfiguration.api', () => {
  let distributorConfiguration: DistributorConfiguration;
  let distributorConfigurationWithAudit: WithAuditDetails<DistributorConfiguration>;
  let auditOnly: WithAuditDetails<void>;

  beforeEach(() => {
    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      listeners: [{
        id: '2',
        afProtocol: '',
        ipAddress: '',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: 'NONE',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }],
      destinations: [{
        id: '3',
        ipAddress: '',
        port: '1234',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        numOfConnections: '1',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        secInfoId: 'NONE',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      }],
    };

    distributorConfigurationWithAudit = {
      data: distributorConfiguration,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.DISTRIBUTOR_CONFIGS,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.DISTRIBUTOR_CONFIGURATION_ADD,
      },
    };
  });

  it('should request the list for distributor configuration', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);
    await expect(api.getDistributorConfigurations('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0` },
      status: 200,
    });
  });

  it('should request the list for distributor configuration with view', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);
    await expect(api.getDistributorConfigurationsWithView('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0` },
      status: 200,
    });
  });

  it('should request the list for distributor configuration by filter', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0?$filter=filter1&$view=listeners`).reply(200, response);
    await expect(api.getDistributorConfigurationsByFilter('0', 'filter1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0` },
      status: 200,
    });
  });

  it('should request a distributor configuration', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1`).reply(200, response);
    await expect(api.getDistributorConfiguration('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1` },
      status: 200,
    });
  });

  it('should request to create a distributor configuration', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onPost(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);
    await expect(api.createDistributorConfiguration('0', distributorConfigurationWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0` },
      status: 200,
    });
  });

  it('should request to update a distributor configuration', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onPut(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1`).reply(200, response);
    await expect(api.updateDistributorConfiguration('0', 'id1', distributorConfigurationWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1` },
      status: 200,
    });
  });

  it('should request to delete a distributor configuration', async () => {
    const response = buildAxiosServerResponse([distributorConfiguration]);
    axiosMock.onDelete(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1`).reply(200, response);
    await expect(api.deleteDistributorConfiguration('0', 'id1', auditOnly)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0/id1` },
      status: 200,
    });
  });
});
