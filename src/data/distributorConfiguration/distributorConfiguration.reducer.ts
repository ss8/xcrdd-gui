import {
  GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED,
  GET_DISTRIBUTOR_CONFIGURATION_FULFILLED,
  GET_DISTRIBUTOR_CONFIGURATION,
  DistributorConfigurationActionTypes,
  DistributorConfigurationState,
  GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED,
  GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER,
  DELETE_DISTRIBUTOR_CONFIGURATION,
  DELETE_DISTRIBUTOR_CONFIGURATION_FULFILLED,
  DELETE_DISTRIBUTOR_CONFIGURATION_REJECTED,
  SELECT_DISTRIBUTOR_CONFIGURATION,
  GET_DISTRIBUTOR_CONFIGURATIONS,
  GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED,
} from './distributorConfiguration.types';

const initialState: DistributorConfigurationState = {
  entities: [],
  filteredEntities: [],
  single: null,
  selected: [],
  isDeleting: false,
};

export default function distributorConfigurationReducer(
  state = initialState,
  action: DistributorConfigurationActionTypes,
): DistributorConfigurationState {
  switch (action.type) {
    case GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED:
    case GET_DISTRIBUTOR_CONFIGURATIONS_WITH_VIEW_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    case GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER:
      return {
        ...state,
        filteredEntities: [],
      };

    case GET_DISTRIBUTOR_CONFIGURATIONS_BY_FILTER_FULFILLED:
      return {
        ...state,
        filteredEntities: action.payload.data.data || [],
      };

    case GET_DISTRIBUTOR_CONFIGURATION:
      return {
        ...state,
        single: null,
      };
    case GET_DISTRIBUTOR_CONFIGURATION_FULFILLED:
      return {
        ...state,
        single: (action.payload.data.data || [])[0] || null,
      };
    case GET_DISTRIBUTOR_CONFIGURATIONS:
      return {
        ...state,
        selected: [],
      };
    case SELECT_DISTRIBUTOR_CONFIGURATION:
      return {
        ...state,
        selected: action.payload || [],
      };

    case DELETE_DISTRIBUTOR_CONFIGURATION:
      return {
        ...state,
        isDeleting: true,
      };

    case DELETE_DISTRIBUTOR_CONFIGURATION_FULFILLED:
    case DELETE_DISTRIBUTOR_CONFIGURATION_REJECTED:
      return {
        ...state,
        isDeleting: false,
      };

    default:
      return state;
  }
}
