import { createSelector } from 'reselect';
import { RootState } from '../root.reducers';
import { DistributorConfiguration } from './distributorConfiguration.types';

export const getDistributorConfigurations =
  (state: RootState): DistributorConfiguration[] => state.distributorConfiguration.entities;
export const getFilteredDistributorConfigurations =
  (state: RootState): DistributorConfiguration[] => state.distributorConfiguration.filteredEntities;
export const getDistributorConfiguration =
  (state: RootState): DistributorConfiguration | null => state.distributorConfiguration.single;
export const getSelectedDistributorConfiguration =
  (state: RootState): (DistributorConfiguration[]) => state.distributorConfiguration.selected;
export const isDeletingDistributorConfiguration =
  (state: RootState): boolean => state.distributorConfiguration.isDeleting;

export const getDistributorConfigurationsByAccessFunctionType = createSelector(
  [getDistributorConfigurations],
  (distributors) => {
    const distributorsByAccessFunctionType: {[key: string]: DistributorConfiguration[]} = {};

    distributors.forEach((distributor) => {
      if (!distributorsByAccessFunctionType[distributor.afType]) {
        distributorsByAccessFunctionType[distributor.afType] = [];
      }
      distributorsByAccessFunctionType[distributor.afType].push(distributor);
    });

    return distributorsByAccessFunctionType;
  },
);
