import { AxiosPromise } from 'axios';
import Http from 'shared/http';
import {
  DistributorConfiguration,
  API_PATH_DISTRIBUTOR_CONFIGURATIONS,
} from './distributorConfiguration.types';
import { ServerResponse, WithAuditDetails } from '../types';

const http = Http.getHttp();

export function getDistributorConfigurations(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<DistributorConfiguration>> {
  return http.get(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}`);
}

export function getDistributorConfigurationsWithView(
  deliveryFunctionId: string,
  view = 'listeners',
): AxiosPromise<ServerResponse<DistributorConfiguration>> {
  return http.get(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}`, {
    params: {
      $view: view,
    },
  });
}

export function getDistributorConfigurationsByFilter(
  deliveryFunctionId: string,
  filter: string,
): AxiosPromise<ServerResponse<DistributorConfiguration>> {
  return http.get(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}`, {
    params: {
      $filter: filter,
      $view: 'listeners',
    },
  });
}

export function getDistributorConfiguration(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<DistributorConfiguration>> {
  return http.get(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}/${id}`);
}

export function createDistributorConfiguration(
  deliveryFunctionId: string,
  body: WithAuditDetails<DistributorConfiguration>,
): AxiosPromise<ServerResponse<void>> {
  try {
    return http.post(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}`, body);
  } catch (e) {
    console.error(e.response.status);
    return Promise.reject(e.response);
  }
}

export function updateDistributorConfiguration(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<DistributorConfiguration>,
): AxiosPromise<ServerResponse<void>> {
  try {
    return http.put(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}/${id}`, body);
  } catch (e) {
    console.error(e.response.status);
    return Promise.reject(e.response);
  }
}

export function deleteDistributorConfiguration(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): AxiosPromise<ServerResponse<void>> {
  return http.delete(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/${deliveryFunctionId}/${id}`, { data: body.auditDetails });
}
