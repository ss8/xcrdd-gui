import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  NodeData,
  API_PATH_NODES,
} from './nodes.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getNodes(deliveryFunctionId: string): AxiosPromise<ServerResponse<NodeData>> {
  return http.get(`${API_PATH_NODES}/${deliveryFunctionId}`);
}
