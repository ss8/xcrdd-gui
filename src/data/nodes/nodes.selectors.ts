import { createSelector } from 'reselect';
import { RootState } from '../root.reducers';
import { NodeData } from './nodes.types';

// eslint-disable-next-line import/prefer-default-export
export const getNodes = (state: RootState): NodeData[] => state.nodes.entities;

export const getCcpagNodes = createSelector(
  [getNodes],
  (xcipioNodes) => xcipioNodes.filter((node) => node.nodeType === 'CCPAG') ?? [],
);
