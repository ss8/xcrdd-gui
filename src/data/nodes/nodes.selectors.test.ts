import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getCcpagNodes,
  getNodes,
} from './nodes.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './nodes.types';

describe('nodes.selectors', () => {
  const nodeDatas = [{
    id: 'Tm9kZS1EdW1teS0x',
    nodeName: 'Node-Dummy-1',
    nodeType: 'XCF3',
    topology: 'HAPAIR',
    ipAddress: '10.0.1.11',
  },
  {
    id: 'Tm9kZS1EdW1teS0y',
    nodeName: 'Node-Dummy-2',
    nodeType: 'XCF3',
    topology: 'STANDALONE',
    ipAddress: '10.0.2.22',
  },
  {
    id: 'Tm9kZS1EdW1teS0z',
    nodeName: 'Node-Dummy-3',
    nodeType: 'CCPAG',
    topology: 'STANDALONE',
    ipAddress: '10.0.2.33',
  }];

  const ccpagNodeData = [{
    id: 'Tm9kZS1EdW1teS0z',
    nodeName: 'Node-Dummy-3',
    nodeType: 'CCPAG',
    topology: 'STANDALONE',
    ipAddress: '10.0.2.33',
  }];

  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of nodes', () => {
    const action: types.NodeActionTypes = {
      type: types.GET_NODES_FULFILLED,
      payload: buildAxiosServerResponse(nodeDatas),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getNodes(store.getState())).toEqual(nodeDatas);
  });

  it('should return the list of ccpag nodes', () => {
    expect(getCcpagNodes(store.getState())).toEqual(ccpagNodeData);
  });

});
