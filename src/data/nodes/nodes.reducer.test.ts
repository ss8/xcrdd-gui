import { buildAxiosServerResponse } from '__test__/utils';
import nodesReducer from './nodes.reducer';
import * as types from './nodes.types';

describe('nodes.reducer', () => {
  let xNodes: types.NodeData[];

  beforeEach(() => {
    xNodes = [{
      id: 'Tm9kZS1EdW1teS0x',
      nodeName: 'Node-Dummy-1',
      nodeType: 'XCF3',
      topology: 'HAPAIR',
      ipAddress: '10.0.1.11',
    },
    {
      id: 'Tm9kZS1EdW1teS0y',
      nodeName: 'Node-Dummy-2',
      nodeType: 'XCF3',
      topology: 'STANDALONE',
      ipAddress: '10.0.2.22',
    }];
  });

  it('should handle GET_NODES_FULFILLED', () => {
    expect(nodesReducer(undefined, {
      type: types.GET_NODES_FULFILLED,
      payload: buildAxiosServerResponse(xNodes),
    })).toEqual({
      entities: xNodes,
    });
  });
});
