import {
  NodeState,
  GET_NODES_FULFILLED,
  NodeActionTypes,
} from './nodes.types';

const xNodes: NodeState = {
  entities: [],
};

// eslint-disable-next-line import/prefer-default-export
export default function nodesReducer(
  state = xNodes,
  action: NodeActionTypes,
): NodeState {
  switch (action.type) {
    case GET_NODES_FULFILLED:
      return { entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
