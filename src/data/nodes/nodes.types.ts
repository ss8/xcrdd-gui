import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_NODES = '/xcipioNodes';
export const GET_NODES = 'GET_NODES';
export const GET_NODES_FULFILLED = 'GET_NODES_FULFILLED';
export const GET_NODES_REJECTED = 'GET_NODES_REJECTED';

export interface NodeData {
  id: string
  nodeName: string
  nodeType: string
  topology: string
  ipAddress: string
  destNodeName?: string
}

export interface NodeState {
  entities: NodeData[]
}

export interface GetNodeAction extends AnyAction {
  type: typeof GET_NODES
  payload: AxiosPromise<ServerResponse<NodeData>>
}

export interface GetNodeFulfilledAction extends AnyAction {
  type: typeof GET_NODES_FULFILLED
  payload: AxiosResponse<ServerResponse<NodeData>>
}

export type NodeActionTypes =
  GetNodeAction |
  GetNodeFulfilledAction;
