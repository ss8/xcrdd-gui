import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './nodes.actions';
import * as types from './nodes.types';
import * as api from './nodes.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('nodes.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the nodes', () => {
    const expectedAction = {
      type: types.GET_NODES,
      payload: api.getNodes('0'),
    };

    expect(actions.getNodes('0')).toEqual(expectedAction);
  });
});
