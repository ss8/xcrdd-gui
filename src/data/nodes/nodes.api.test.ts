import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './nodes.api';
import {
  NodeData,
  API_PATH_NODES,
} from './nodes.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('nodes.api', () => {
  const xNodes: NodeData[] = [{
    id: 'Tm9kZS1EdW1teS0x',
    nodeName: 'Node-Dummy-1',
    nodeType: 'XCF3',
    topology: 'HAPAIR',
    ipAddress: '10.0.1.11',
  },
  {
    id: 'Tm9kZS1EdW1teS0y',
    nodeName: 'Node-Dummy-2',
    nodeType: 'XCF3',
    topology: 'STANDALONE',
    ipAddress: '10.0.2.22',
  }];

  it('should request the list for nodes', async () => {
    const response = buildAxiosServerResponse(xNodes);
    const apiUrl = `${API_PATH_NODES}/0`;
    axiosMock.onGet(apiUrl).reply(200, response);
    await expect(api.getNodes('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: apiUrl },
      status: 200,
    });
  });
});
