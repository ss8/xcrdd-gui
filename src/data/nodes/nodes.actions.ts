import * as api from './nodes.api';
import {
  GET_NODES, GetNodeAction,
} from './nodes.types';

// eslint-disable-next-line import/prefer-default-export
export function getNodes(deliveryFunctionId: string): GetNodeAction {
  return {
    type: GET_NODES,
    payload: api.getNodes(deliveryFunctionId),
  };
}
