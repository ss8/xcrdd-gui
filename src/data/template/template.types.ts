import { Position } from '@blueprintjs/core';

export const TEMPLATE_CATEGORY_ACCESS_FUNCTION = 'access-function';
export const TEMPLATE_CATEGORY_COLLECTION_FUNCTION = 'collection-function';
export const TEMPLATE_CATEGORY_XCRDD_FUNCTION = 'xcrdd-function';

export type RawTemplate = {
  category: string
  categoryDefault: boolean
  dateUpdated: string
  id: string
  name: string
  owner: unknown
  publicTemplate: boolean
  template: string
}

export type Template = {
  category: string
  categoryDefault: boolean
  dateUpdated: string
  id: string
  name: string
  owner: unknown
  publicTemplate: boolean
  template: FormTemplate
}

interface FormTemplateSectionMap {
  [key: string]: string | FormTemplateSection
}

export type FormTemplate = FormTemplateSectionMap & {
  version: string,
  name: string,
  key: string, // this is AF tag or CF tag
  formId?: string, // use in the name of dynamic form
}

export enum SectionType {
  simpleForm = 'simpleForm',
  distributorConfiguration = 'distributorConfiguration',
  headerSelectionPanel = 'headerSelectionPanel',
}

export type FormTemplateSection = {
  metadata: {
    sectionType?: SectionType,
    sectionTypeInstructionalText?:string
    min?: number
    max?: number
    tabTitle?: string
    layout?: FormTemplateSectionMetadataLayout,
    expandByDefault?: boolean
  }
  fields: FormTemplateFieldMap
}

export type FormTemplateSectionMetadataLayout = {
  rows: string[][]
  collapsible?: CollapsibleSection
}

export type CollapsibleSection = {
  label: string,
  rows: string[][],
}

export type FormTemplateMapByKey = {
  [templateKey: string]: FormTemplate
}

export type FormTemplateFieldMap = {
  [fieldName: string]: FormTemplateField
}

export type ValueToAffectedField = {
  [fieldName: string]: Omit<FormTemplateField, 'type'>
};

export type ValueToAffectedFieldDefinitionMap = {
  [value: string]: {
    layoutOperations?: LayoutOperation[]
    fields: ValueToAffectedField
  }
}

export type LayoutOperation = {
  operation: 'setLayoutOrder'
  parameters: string[]
}

export type FormTemplateValidation = {
  required?: boolean
  validators?: FormTemplateValidator[]
}
export interface FormTemplateField {
  label?: string
  type: string
  options?: FormTemplateOption[]
  optionsFormat?: string
  optionsFormatValue?: string
  allOptions?: FormTemplateOption[]
  initial?: string | number | boolean | string[]
  readOnly?: boolean
  disabled?: boolean
  hide?: 'true' | 'false'
  renderAsEmptyIfHidden?: boolean
  className?: string
  readOnlyIfOneOption?: boolean
  timePrecision?: 'minute' | 'second'
  position?: Position
  validation?: FormTemplateValidation,
  placeholder?: string
  intent?: string
  minimal?: boolean
  icon?: string
  handleClick?: () => void
  customComponent?: string
  affectedFieldsOnChange?: string[]
  affectedFieldsOnChangeExtended?:ValueToAffectedFieldDefinitionMap
  processOptionParamName?: string
  processOptionLoadParamValueTo?: 'optionsFormatValue'
  optionsBasedOn?: string
}

export type FormTemplateValidator = {
  type: string
  length?: number
  regexp?: string
  operator?: string
  fieldName?: string
  value?: string | number
  intervals?: [number, number][]
  customValidationFunction?: string
  invalidIPs?: [string]
  theOtherField?: string
  errorMessage?: string
  errorMessages?: {
    [key: string]: string
  }
  formSectionName?: string
}

export type FormTemplateOption = {
  label: string
  value: string
}

export enum AFFormTemplateFormSectionName {
  provisioningInterfaces = 'provisioningInterfaces',
  dataInterfaces = 'dataInterfaces',
  contentInterfaces = 'contentInterfaces',
}

export enum AFFormTemplateFormSubSectionName {
  trafficManager = 'trafficManager',
  distributorConfiguration = 'distributorConfiguration',
}
