import { AnyAction } from 'redux';
import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getMockTemplatesAccessFunctionStartIndex,
  getMockTemplatesAccessFunctionEndIndex,
  getMockTemplatesCollectionFunctionStartIndex,
  getMockTemplatesCollectionFunctionEndIndex,
  mockTemplates,
  getMockTemplateByKey,
} from '__test__/mockTemplates';
import rootReducer, { RootState } from 'data/root.reducers';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import cloneDeep from 'lodash.clonedeep';
import {
  getRawTemplates,
  getTemplates,
  getAccessFunctionTemplates,
  getAccessFunctionFormTemplates,
  getAccessFunctionFormTemplatesByKey,
  getCollectionFunctionTemplates,
  getCollectionFunctionFormTemplates,
  getCollectionFunctionFormTemplatesByKey,
  getEnabledAccessFunctionFormTemplatesByKey,
  getEnabledCollectionFunctionFormTemplatesByKey,
} from './template.selectors';
import { RawTemplate } from './template.types';

describe('template.selectors', () => {
  let rawTemplates: RawTemplate[] = [];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;
  let accessFunctionStartIndex: number;
  let accessFunctionEndIndex: number;
  let collectionFunctionStartIndex: number;
  let collectionFunctionEndIndex: number;

  beforeEach(() => {
    rawTemplates = cloneDeep(mockTemplates);

    mockStore = configureStore<RootState>([]);

    const action: AnyAction = {
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(rawTemplates),
    };
    store = mockStore(rootReducer(undefined, action));

    accessFunctionStartIndex = getMockTemplatesAccessFunctionStartIndex();
    accessFunctionEndIndex = getMockTemplatesAccessFunctionEndIndex();
    collectionFunctionStartIndex = getMockTemplatesCollectionFunctionStartIndex();
    collectionFunctionEndIndex = getMockTemplatesCollectionFunctionEndIndex();
  });

  describe('getRawTemplates()', () => {
    it('should return the raw templates', () => {
      expect(getRawTemplates(store.getState())).toEqual(rawTemplates);
    });
  });

  describe('getTemplates()', () => {
    it('should return the template with parsed template fields', () => {
      const expected = rawTemplates.map((rawTemplate) => {
        return {
          ...rawTemplate,
          template: JSON.parse(rawTemplate.template),
        };
      });

      expect(getTemplates(store.getState())).toEqual(expected);
    });
  });

  describe('getAccessFunctionTemplates()', () => {
    it('should return access function templates with parsed template fields', () => {
      const expected = [];
      let i: number;
      for (i = accessFunctionStartIndex; i <= accessFunctionEndIndex; i += 1) {
        expected.push({
          ...rawTemplates[i],
          template: JSON.parse(rawTemplates[i].template),
        });
      }
      expect(getAccessFunctionTemplates(store.getState())).toEqual(expected);
    });
  });

  describe('getAccessFunctionFormTemplates()', () => {
    it('should return access function form templates', () => {
      const expected = [];
      let i: number;
      for (i = accessFunctionStartIndex; i <= accessFunctionEndIndex; i += 1) {
        expected.push(JSON.parse(rawTemplates[i].template));
      }
      expect(getAccessFunctionFormTemplates(store.getState())).toEqual(expected);
    });
  });

  describe('getAccessFunctionFormTemplatesByKey()', () => {
    it('should return access function form templates by key', () => {
      expect(getAccessFunctionFormTemplatesByKey(store.getState())).toEqual({
        ERK_UPF: JSON.parse(rawTemplates[accessFunctionStartIndex].template),
        ERK_SMF: JSON.parse(rawTemplates[accessFunctionStartIndex + 1].template),
        PCRF: JSON.parse(rawTemplates[accessFunctionStartIndex + 2].template),
        STA_PGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 3].template),
        NSN_CSCF: JSON.parse(rawTemplates[accessFunctionStartIndex + 4].template),
        ERK_MCPTT: JSON.parse(rawTemplates[accessFunctionStartIndex + 5].template),
        ERK_SGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 6].template),
        ALU_MME: JSON.parse(rawTemplates[accessFunctionStartIndex + 7].template),
        NSNHLR_SDM_PRGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 8].template),
        ALU_DSS: JSON.parse(rawTemplates[accessFunctionStartIndex + 9].template),
        ERK_PGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 10].template),
        MV_SMSIWF: JSON.parse(rawTemplates[accessFunctionStartIndex + 11].template),
        ACME_X123: JSON.parse(rawTemplates[accessFunctionStartIndex + 12].template),
        MVNR_PGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 13].template),
        NSNHLR: JSON.parse(rawTemplates[accessFunctionStartIndex + 14].template),
        NSN_SAEGW_PGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 15].template),
        LU3G_CSCF: JSON.parse(rawTemplates[accessFunctionStartIndex + 16].template),
        CISCOVPCRF: JSON.parse(rawTemplates[accessFunctionStartIndex + 17].template),
        NSN_SAEGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 18].template),
        MVNR_SBC: JSON.parse(rawTemplates[accessFunctionStartIndex + 19].template),
        ERK_VMME: JSON.parse(rawTemplates[accessFunctionStartIndex + 20].template),
        MVNR_CSCF: JSON.parse(rawTemplates[accessFunctionStartIndex + 21].template),
        ICS_RCS: JSON.parse(rawTemplates[accessFunctionStartIndex + 22].template),
        ALU_SGW: JSON.parse(rawTemplates[accessFunctionStartIndex + 23].template),
        SONUS_SBC: JSON.parse(rawTemplates[accessFunctionStartIndex + 24].template),
        ETSI103221: JSON.parse(rawTemplates[accessFunctionStartIndex + 25].template),
        GIZMO: JSON.parse(rawTemplates[accessFunctionStartIndex + 26].template),
        STREAMWIDE: JSON.parse(rawTemplates[accessFunctionStartIndex + 27].template),
        ERK_5GAMF: JSON.parse(rawTemplates[accessFunctionStartIndex + 28].template),
        NOK_5GUDM: JSON.parse(rawTemplates[accessFunctionStartIndex + 29].template),
        NSN_SDM: JSON.parse(rawTemplates[accessFunctionStartIndex + 30].template),
        NOK_5GAMF: JSON.parse(rawTemplates[accessFunctionStartIndex + 31].template),
        NOK_5GSMF: JSON.parse(rawTemplates[accessFunctionStartIndex + 32].template),
        NOK_5GSMSF: JSON.parse(rawTemplates[accessFunctionStartIndex + 33].template),
        MVNR_5GSMSC: JSON.parse(rawTemplates[accessFunctionStartIndex + 34].template),
        ETSI_GIZMO: JSON.parse(rawTemplates[accessFunctionStartIndex + 35].template),
        NKSR: JSON.parse(rawTemplates[accessFunctionStartIndex + 36].template),
        NOKIA_SBC: JSON.parse(rawTemplates[accessFunctionStartIndex + 37].template),
        GPVRADIUS: JSON.parse(rawTemplates[accessFunctionStartIndex + 38].template),
      });
    });
  });

  describe('getCollectionFunctionTemplates()', () => {
    it('should return collection function templates with parsed template fields', () => {
      const expected = [];
      let i: number;
      for (i = collectionFunctionStartIndex; i <= collectionFunctionEndIndex; i += 1) {
        expected.push({
          ...rawTemplates[i],
          template: JSON.parse(rawTemplates[i].template),
        });
      }

      expect(getCollectionFunctionTemplates(store.getState())).toEqual(expected);
    });
  });

  describe('getCollectionFunctionFormTemplates()', () => {
    it('should return collection function form templates', () => {
      const expected = [];
      let i: number;
      for (i = collectionFunctionStartIndex; i <= collectionFunctionEndIndex; i += 1) {
        expected.push(JSON.parse(rawTemplates[i].template));
      }
      expect(getCollectionFunctionFormTemplates(store.getState())).toEqual(expected);
    });
  });

  describe('getCollectionFunctionFormTemplatesByKey()', () => {
    it('should return collection function form templates by key', () => {
      expect(getCollectionFunctionFormTemplatesByKey(store.getState())).toEqual({
        'CF-3GPP-33108': JSON.parse(rawTemplates[collectionFunctionStartIndex].template),
        'CF-ETSI-102232': JSON.parse(rawTemplates[collectionFunctionStartIndex + 1].template),
        'CF-TIA-1072': JSON.parse(rawTemplates[collectionFunctionStartIndex + 2].template),
        'CF-TIA-1066': JSON.parse(rawTemplates[collectionFunctionStartIndex + 3].template),
        'CF-JSTD025A': JSON.parse(rawTemplates[collectionFunctionStartIndex + 4].template),
        'CF-JSTD025B': JSON.parse(rawTemplates[collectionFunctionStartIndex + 5].template),
        'CF-ATIS-1000678': JSON.parse(rawTemplates[collectionFunctionStartIndex + 6].template),
      });
    });
  });

  describe('getEnabledAccessFunctionFormTemplatesByKey()', () => {
    beforeEach(() => {
      const supportedFeatures = {
        id: 'XCP',
        version: '8.12.0',
        description: 'Some description text',
        features: {
          customer: 'UT',
          version: 1,
          nodes: [{
            ip: '12.12.12.12',
            type: 'HA',
            features: [
              'AF-ERK-UPF',
              'AF-ERK-SMF-X2DIST',
              'CF-3GPP-33108',
            ],
          }, {
            ip: '13.13.13.13',
            type: 'HA2',
            features: [
              'AF-ERK-SMF-X1',
              'AF-ERK-SGW',
              'CF-ETSI-102232-TCP',
            ],
          }],
        },
        config: {
          services: {
            'LTE-4G-DATA': {
              description: '4G LTE data',
              title: '4G PD',
            },
            '5G-DATA': {
              description: '5G mobile data',
              title: '5G data',
            },
          },
          accessFunctions: {
            ERK_UPF: {
              requiredTargets: [],
              collectionFunctions: [
                'CF-3GPP-33128',
              ],
              features: [
                'AF-ERK-UPF',
              ],
              autowire: true,
              title: 'Ericsson UPF',
              models: [],
              versions: [],
              genccc: false,
              services: [
                '5G-DATA',
              ],
              optionalTargets: [],
              x3deliveryOptions: null,
              xcipioAFType: 'ERK_UPF',
              description: 'Ericsson User Plane Function Point of Interception',
            },
            ERK_SMF: {
              services: {
                '5G-DATA': {
                  requiredTargets: [
                    'MSISDN',
                  ],
                  optionalTargets: [
                    'IMEI',
                  ],
                  collectionFunctions: [
                    'CF-ETSI-102232',
                  ],
                },
                'LTE-4G-DATA': {
                  requiredTargets: [
                    'IMSI',
                  ],
                  collectionFunctions: [
                    'CF-3GPP-33108',
                  ],
                },
              },
              features: [
                'AF-ERK-SMF-X1',
                'AF-ERK-SMF-X2DIST',
              ],
              autowire: true,
              title: 'Ericsson SMF',
              models: [],
              versions: [],
              genccc: false,
              x3deliveryOptions: null,
              xcipioAFType: 'ERK_SMF',
              description: 'Ericsson Session Management Function Point of Interception',
            },
            ERK_PGW: {
              requiredTargets: [],
              collectionFunctions: [
                'CF-3GPP-33108',
              ],
              features: [
                'AF-ERK-SGW',
              ],
              autowire: false,
              title: 'Ericsson SAEGW/PGW',
              models: [],
              versions: [],
              genccc: false,
              services: [
                'LTE-4G-DATA',
                'VOLTE-4G-VOICE',
              ],
              optionalTargets: [],
              x3deliveryOptions: null,
              xcipioAFType: 'ERK_PGW',
              description: 'Ericsson System Architecture Evolution (SAE) SGW/PGW Access Function',
            },
          },
        },
      };

      const action: AnyAction = {
        type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([supportedFeatures]),
      };
      store = mockStore(rootReducer(undefined, action));
    });

    it('should return only enabled access function form templates', () => {
      const state = {
        ...store.getState(),
        warrants: {
          ...store.getState().warrants,
          templates: cloneDeep(mockTemplates),
        },
      };
      expect(getEnabledAccessFunctionFormTemplatesByKey(state)).toEqual({
        ERK_PGW: getMockTemplateByKey('ERK_PGW'),
        ERK_SMF: getMockTemplateByKey('ERK_SMF'),
        ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      });
    });
  });

  describe('getEnabledCollectionFunctionFormTemplatesByKey()', () => {
    it('should return enabled collection function form templates by key', () => {
      const supportedFeatures = {
        id: 'XCP',
        version: '8.12.0',
        description: 'Some description text',
        features: {
          customer: 'UT',
          version: 1,
          nodes: [{
            ip: '12.12.12.12',
            type: 'HA',
            features: [
              'CF-3GPP-33108',
              'CF-ETSI-102232-TCP',
            ],
          }],
        },
        config: {
          services: {},
          accessFunctions: {},
          collectionFunctions: {
            'CF-3GPP-33108': {
              services: [
                'VOLTE-RCS',
                'VOLTE-4G-VOICE',
                'GSMCDMA-2G-VOICE',
                'GSMCDMA-2G-DATA',
                'LTE-4G-DATA',
                'LTE-4G-DATA-ROAMING',
                'LTE-GIZMO',
                'VZ-VISIBLE',
                'MCPTT',
                'LTE-IOT',
              ],
              versions: [
                '11.1',
                '12.7',
                '12.8',
                '12.12',
                '15.4',
                '15.6',
              ],
              features: [
                'CF-3GPP-33108',
              ],
              description: '3GPP 33.108 Collection Function with TCP or FTP interfaces',
              title: 'US 3GPP LI (TS 33.108) over TCP or FTP',
            },
            'CF-ETSI-102232': {
              services: [
                'VOLTE-RCS',
                'IP',
                'VOLTE-4G-VOICE',
                '5G-DATA',
                'LTE-4G-DATA',
              ],
              versions: [
                '1.4.1',
                '2.2.1',
                '3.1.1',
                '3.3.1',
                '3.10.1',
                '3.15.1',
                '3.21.1',
              ],
              features: [
                'CF-ETSI-102232-TCP',
                'CF-ETSI-102232-FTP',
              ],
              description: 'ETSI 102 232 Collection Function',
              title: 'ETSI IP (TS 102 232)',
            },
            'CF-3GPP-33108ROSE': {
              services: [
                'GSMCDMA-2G-VOICE',
                'GSMCDMA-2G-DATA',
                'LTE-4G-DATA',
                'LTE-GIZMO',
                'VZ-VISIBLE',
                'MCPTT',
                'LTE-IOT',
              ],
              versions: [],
              features: [
                'CF-3GPP-33108',
              ],
              description: 'ETSI 133.108 Collection Function with ROSE interface',
              title: 'ETSI LI (TS 133.108) over ROSE',
            },
          },
          targets: {},
        },
      };

      const action: AnyAction = {
        type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([supportedFeatures]),
      };
      store = mockStore(rootReducer(store.getState(), action));

      expect(getEnabledCollectionFunctionFormTemplatesByKey(store.getState())).toEqual({
        'CF-3GPP-33108': JSON.parse(rawTemplates[collectionFunctionStartIndex].template),
        'CF-ETSI-102232': JSON.parse(rawTemplates[collectionFunctionStartIndex + 1].template),
      });
    });
  });
});
