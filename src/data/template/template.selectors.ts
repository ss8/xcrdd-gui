import { createSelector } from 'reselect';
import { RootState } from 'data/root.reducers';
import { getEnabledAccessFunction, getEnabledCollectionFunctions } from 'data/supportedFeatures/supportedFeatures.selectors';
import { getConfiguredAccessFunctions } from 'data/accessFunction/accessFunction.selectors';
import { SupportedFeaturesAccessFunctions, SupportedFeaturesCollectionFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { FormTemplateMapByKey } from 'data/template/template.types';
import {
  RawTemplate,
  Template,
  FormTemplate,
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  TEMPLATE_CATEGORY_XCRDD_FUNCTION,
} from './template.types';

export const getRawTemplates = (state: RootState): RawTemplate[] => state.warrants.templates ?? [];

export const getTemplates = createSelector(
  [getRawTemplates],
  (templates: RawTemplate[]): Template[] => {
    return templates.map((entry) => ({
      ...entry,
      template: JSON.parse(entry.template),
    }));
  },
);

export const getAccessFunctionTemplates = createSelector(
  [getTemplates],
  (templates: Template[]): Template[] => {
    return templates.filter((template) => template.category === TEMPLATE_CATEGORY_ACCESS_FUNCTION);
  },
);

export const getAccessFunctionFormTemplates = createSelector(
  [getAccessFunctionTemplates],
  (templates: Template[]): FormTemplate[] => {
    return templates.map(({ template }) => template);
  },
);

export const getAccessFunctionFormTemplatesByKey = createSelector(
  [getAccessFunctionFormTemplates],
  (formTemplates: FormTemplate[]): FormTemplateMapByKey => {
    return formTemplates.reduce((previousValue, template) => ({
      ...previousValue,
      [template.key]: template,
    }), {});
  },
);

export const getEnabledAccessFunctionFormTemplatesByKey = createSelector(
  [getAccessFunctionFormTemplates, getEnabledAccessFunction],
  (
    formTemplates: FormTemplate[],
    enabledAccessFunction: SupportedFeaturesAccessFunctions,
  ): FormTemplateMapByKey => {
    return formTemplates.filter((template) => {
      return enabledAccessFunction[template.key] != null;
    }).reduce((previousValue, template) => ({
      ...previousValue,
      [template.key]: template,
    }), {});
  },
);

export const getEnabledAndConfiguredAccessFunctionFormTemplatesByKey = createSelector(
  [getAccessFunctionFormTemplates, getEnabledAccessFunction, getConfiguredAccessFunctions],
  (
    formTemplates: FormTemplate[],
    enabledAccessFunction: SupportedFeaturesAccessFunctions,
    configuredAccessFunction: SupportedFeaturesAccessFunctions,
  ): FormTemplateMapByKey => {
    return formTemplates.filter((template) => {
      return enabledAccessFunction[template.key] != null;
    }).filter((template) => {
      return configuredAccessFunction[template.key] != null;
    }).reduce((previousValue, template) => ({
      ...previousValue,
      [template.key]: template,
    }), {});
  },
);

export const getCollectionFunctionTemplates = createSelector(
  [getTemplates],
  (templates: Template[]): Template[] => {
    return templates.filter((template) => template.category === TEMPLATE_CATEGORY_COLLECTION_FUNCTION);
  },
);

export const getCollectionFunctionFormTemplates = createSelector(
  [getCollectionFunctionTemplates],
  (templates: Template[]): FormTemplate[] => {
    return templates.map(({ template }) => template);
  },
);

export const getCollectionFunctionFormTemplatesByKey = createSelector(
  [getCollectionFunctionFormTemplates],
  (formTemplates: FormTemplate[]): FormTemplateMapByKey => {
    return formTemplates.reduce((previousValue, template) => ({
      ...previousValue,
      [template.key]: template,
    }), {});
  },
);

export const getEnabledCollectionFunctionFormTemplatesByKey = createSelector(
  [getCollectionFunctionFormTemplates, getEnabledCollectionFunctions], (
    formTemplates: FormTemplate[],
    enabledCollectionFunction: SupportedFeaturesCollectionFunctions,
  ): FormTemplateMapByKey => {
    return formTemplates.filter(({ key }) => enabledCollectionFunction[key] != null)
      .reduce((previousValue, template) => ({
        ...previousValue,
        [template.key]: template,
      }), {});
  },
);

export const getXCRDDFunctionTemplates = createSelector(
  [getTemplates],
  (templates: Template[]): Template[] => {
    return templates.filter((template) => template.category === TEMPLATE_CATEGORY_XCRDD_FUNCTION);
  },
);

export const getXCRDDFunctionFormTemplates = createSelector(
  [getXCRDDFunctionTemplates],
  (templates: Template[]): FormTemplate[] => {
    return templates.map(({ template }) => template);
  },
);

export const getXCRDDFunctionFormTemplatesByKey = createSelector(
  [getXCRDDFunctionFormTemplates],
  (formTemplates: FormTemplate[]): FormTemplateMapByKey => {
    return formTemplates.reduce((previousValue, template) => ({
      ...previousValue,
      [template.key]: template,
    }), {});
  },
);
