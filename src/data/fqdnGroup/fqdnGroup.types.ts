import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { Operation, ServerResponse } from 'data/types';

export const API_PATH_FQDN_GROUPS = '/fqdn-groups';

export const GET_FQDN_GROUPS = 'GET_FQDN_GROUPS';
export const GET_FQDN_GROUPS_PENDING = 'GET_FQDN_GROUPS_PENDING';
export const GET_FQDN_GROUPS_FULFILLED = 'GET_FQDN_GROUPS_FULFILLED';
export const GET_FQDN_GROUPS_REJECTED = 'GET_FQDN_GROUPS_REJECTED';

export interface FqdnGroup {
  id: string
  groupName: string
  fqdn: {
    fId: string
    domainName: string
    state: 'INACTIVE' | 'ACTIVE' | 'FAILED'
    operation?: Operation
  }[]
}

export interface FqdnGroupState {
  entities: FqdnGroup[]
}

export interface GetFqdnGroupAction extends AnyAction {
  type: typeof GET_FQDN_GROUPS
  payload: AxiosPromise<ServerResponse<FqdnGroup>>
}

export interface GetFqdnGroupPendingAction extends AnyAction {
  type: typeof GET_FQDN_GROUPS_PENDING
  payload: AxiosResponse<ServerResponse<FqdnGroup>>
}
export interface GetFqdnGroupFulfilledAction extends AnyAction {
  type: typeof GET_FQDN_GROUPS_FULFILLED
  payload: AxiosResponse<ServerResponse<FqdnGroup>>
}

export interface GetFqdnGroupRejectedAction extends AnyAction {
  type: typeof GET_FQDN_GROUPS_REJECTED
  payload: AxiosResponse<ServerResponse<FqdnGroup>>
}

export type FqdnGroupActionTypes =
  GetFqdnGroupAction |
  GetFqdnGroupPendingAction |
  GetFqdnGroupFulfilledAction |
  GetFqdnGroupRejectedAction;
