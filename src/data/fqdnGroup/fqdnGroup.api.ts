import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  FqdnGroup,
  API_PATH_FQDN_GROUPS,
} from './fqdnGroup.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getFqdnGroups(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<FqdnGroup>> {
  return http.get(`${API_PATH_FQDN_GROUPS}/${deliveryFunctionId}`);
}
