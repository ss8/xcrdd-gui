import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './fqdnGroup.api';
import { API_PATH_FQDN_GROUPS } from './fqdnGroup.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('fqdnGroup.api', () => {
  it('should request the list for fqdnGroups', async () => {
    const response = buildAxiosServerResponse(mockFqdnGroups);
    axiosMock.onGet(`${API_PATH_FQDN_GROUPS}/0`).reply(200, response);
    await expect(api.getFqdnGroups('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_FQDN_GROUPS}/0` },
      status: 200,
    });
  });
});
