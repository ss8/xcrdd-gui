import {
  GET_FQDN_GROUPS_FULFILLED,
  FqdnGroupActionTypes,
  FqdnGroupState,
} from './fqdnGroup.types';

const initialState: FqdnGroupState = {
  entities: [],
};

export default function fqdnGroupReducer(
  state = initialState,
  action: FqdnGroupActionTypes,
): FqdnGroupState {
  switch (action.type) {
    case GET_FQDN_GROUPS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
