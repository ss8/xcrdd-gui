import { RootState } from '../root.reducers';
import { FqdnGroup } from './fqdnGroup.types';

// eslint-disable-next-line import/prefer-default-export
export const getFqdnGroups = (state: RootState): FqdnGroup[] => state.fqdnGroup.entities;
