import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './fqdnGroup.reducer';
import * as types from './fqdnGroup.types';

describe('fqdnGroup.reducer', () => {
  it('should handle GET_FQDN_GROUPS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    })).toEqual({
      entities: mockFqdnGroups,
    });
  });
});
