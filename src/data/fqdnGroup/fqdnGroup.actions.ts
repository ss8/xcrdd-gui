import * as api from './fqdnGroup.api';
import { GET_FQDN_GROUPS, GetFqdnGroupAction } from './fqdnGroup.types';

// eslint-disable-next-line import/prefer-default-export
export function getFqdnGroups(deliveryFunctionId: string): GetFqdnGroupAction {
  return {
    type: GET_FQDN_GROUPS,
    payload: api.getFqdnGroups(deliveryFunctionId),
  };
}
