import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import {
  getFqdnGroups,
} from './fqdnGroup.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './fqdnGroup.types';

describe('fqdnGroup.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of fqdnGroups', () => {
    const action: types.FqdnGroupActionTypes = {
      type: types.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getFqdnGroups(store.getState())).toEqual(mockFqdnGroups);
  });
});
