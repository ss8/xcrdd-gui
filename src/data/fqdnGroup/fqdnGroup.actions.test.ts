import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './fqdnGroup.actions';
import * as types from './fqdnGroup.types';
import * as api from './fqdnGroup.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('fqdnGroup.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the fqdnGroups', () => {
    const expectedAction = {
      type: types.GET_FQDN_GROUPS,
      payload: api.getFqdnGroups('0'),
    };

    expect(actions.getFqdnGroups('0')).toEqual(expectedAction);
  });
});
