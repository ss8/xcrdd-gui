import { mockAlertNotifications } from '__test__/mockAlertNotifications';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './alertNotification.reducer';
import * as types from './alertNotification.types';

describe('alertNotification.reducer', () => {
  it('should handle GET_ALERT_NOTIFICATIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    })).toEqual({
      entities: mockAlertNotifications,
    });
  });
});
