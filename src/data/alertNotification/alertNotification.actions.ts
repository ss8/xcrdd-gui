import { AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import * as api from './alertNotification.api';
import {
  GET_ALERT_NOTIFICATIONS,
  GetAlertNotificationAction,
  POLL_ALERT_NOTIFICATIONS,
  POLL_ALERT_NOTIFICATIONS_PENDING,
  POLL_ALERT_NOTIFICATIONS_FULFILLED,
  POLL_ALERT_NOTIFICATIONS_REJECTED,
  PollAlertNotificationAction,
  PollAlertNotificationPendingAction,
  PollAlertNotificationFulfilledAction,
  PollAlertNotificationRejectedAction,
  PollAlertNotificationDelayingAction,
  POLL_ALERT_NOTIFICATIONS_DELAYING,
  AlarmType,
  GET_ALERT_NOTIFICATIONS_FULFILLED,
  GET_ALERT_NOTIFICATIONS_REJECTED,
  GetAlertNotificationFulfilledAction,
  GetAlertNotificationRejectedAction,
  AlertNotificationModel,
} from './alertNotification.types';

export function getAlertNotifications(
  deliveryFunctionId: string,
  alarmTypes?: AlarmType[],
  top?: string,
  orderBy?: 'utcDateTime desc',
): GetAlertNotificationAction {
  return {
    type: GET_ALERT_NOTIFICATIONS,
    payload: api.getAlertNotifications(deliveryFunctionId, alarmTypes, top, orderBy),
  };
}

export function getAlertNotificationsFulfilled(
  response: AxiosResponse<ServerResponse<AlertNotificationModel>>,
): GetAlertNotificationFulfilledAction {
  return {
    type: GET_ALERT_NOTIFICATIONS_FULFILLED,
    payload: response,
  };
}

export function getAlertNotificationsRejected(
  response: AxiosResponse<ServerResponse<AlertNotificationModel>>,
): GetAlertNotificationRejectedAction {
  return {
    type: GET_ALERT_NOTIFICATIONS_REJECTED,
    payload: response,
  };
}

export function pollAlertNotifications(
  deliveryFunctionId: string,
  pollingTime: string,
  alarmTypes?: AlarmType[],
  top?: string,
  orderBy?: 'utcDateTime desc',
): PollAlertNotificationAction {
  return {
    type: POLL_ALERT_NOTIFICATIONS,
    payload: {
      deliveryFunctionId,
      pollingTime,
      alarmTypes,
      top,
      orderBy,
    },
  };
}

export function pollAlertNotificationsPending(): PollAlertNotificationPendingAction {
  return {
    type: POLL_ALERT_NOTIFICATIONS_PENDING,
  };
}

export function pollAlertNotificationsFulfilled(): PollAlertNotificationFulfilledAction {
  return {
    type: POLL_ALERT_NOTIFICATIONS_FULFILLED,
  };
}

export function pollAlertNotificationsRejected(error: Error): PollAlertNotificationRejectedAction {
  return {
    type: POLL_ALERT_NOTIFICATIONS_REJECTED,
    payload: { error },
  };
}

export function pollAlertNotificationsDelaying(): PollAlertNotificationDelayingAction {
  return {
    type: POLL_ALERT_NOTIFICATIONS_DELAYING,
  };
}
