import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './alertNotification.actions';
import * as types from './alertNotification.types';
import * as api from './alertNotification.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('alertNotification.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the alertNotifications', () => {
    const expectedAction = {
      type: types.GET_ALERT_NOTIFICATIONS,
      payload: api.getAlertNotifications('0', ['CCBUFUSAGE', 'IRIBUFUSAGE']),
    };

    expect(actions.getAlertNotifications('0', ['CCBUFUSAGE', 'IRIBUFUSAGE'])).toEqual(expectedAction);
  });

  it('should create an action to poll the alertNotifications', () => {
    const expectedAction = {
      type: types.POLL_ALERT_NOTIFICATIONS,
      payload: {
        deliveryFunctionId: '0',
        pollingTime: '30',
        alarmTypes: ['CCBUFUSAGE', 'IRIBUFUSAGE'],
        top: '10',
        orderBy: 'utcDateTime desc',
      },
    };

    expect(actions.pollAlertNotifications('0', '30', ['CCBUFUSAGE', 'IRIBUFUSAGE'], '10', 'utcDateTime desc')).toEqual(expectedAction);
  });

  it('should create an action for pending poll alertNotifications', () => {
    const expectedAction = {
      type: types.POLL_ALERT_NOTIFICATIONS_PENDING,
    };

    expect(actions.pollAlertNotificationsPending()).toEqual(expectedAction);
  });

  it('should create an action for fulfilled poll alertNotifications', () => {
    const expectedAction = {
      type: types.POLL_ALERT_NOTIFICATIONS_FULFILLED,
    };

    expect(actions.pollAlertNotificationsFulfilled()).toEqual(expectedAction);
  });

  it('should create an action for rejected poll alertNotifications', () => {
    const expectedAction = {
      type: types.POLL_ALERT_NOTIFICATIONS_REJECTED,
      payload: { error: new Error('some error') },
    };

    expect(actions.pollAlertNotificationsRejected(new Error('some error'))).toEqual(expectedAction);
  });

  it('should create an action for delaying poll alertNotifications', () => {
    const expectedAction = {
      type: types.POLL_ALERT_NOTIFICATIONS_DELAYING,
    };

    expect(actions.pollAlertNotificationsDelaying()).toEqual(expectedAction);
  });
});
