import {
  all,
  takeLeading,
} from 'redux-saga/effects';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { recordSaga } from '__test__/recordSaga';
import alertNotificationSaga, * as alertNotificationSagas from './alertNotification.saga';
import * as alertNotificationActions from './alertNotification.actions';

import { POLL_ALERT_NOTIFICATIONS } from './alertNotification.types';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('alertNotification.saga', () => {
  describe('alertNotificationSaga()', () => {
    it('should handle the related sagas', () => {
      const saga = alertNotificationSaga();

      expect(saga.next().value)
        .toEqual(all([takeLeading(POLL_ALERT_NOTIFICATIONS, alertNotificationSagas.pollAlertNotification)]));
    });
  });

  describe('pollAlertNotification()', () => {
    it('should call api and dispatch pending, fulfilled actions', async () => {
      axiosMock.onGet().reply(200, { data: [] });

      const dispatched = await recordSaga(
        alertNotificationSagas.pollAlertNotification,
        alertNotificationActions.pollAlertNotifications('0', '1', ['CCBUFUSAGE', 'IRIBUFUSAGE'], '10', 'utcDateTime desc'),
      );

      expect(dispatched).toEqual([
        alertNotificationActions.pollAlertNotificationsPending(),
        alertNotificationActions.getAlertNotificationsFulfilled(expect.any(Object)),
        alertNotificationActions.pollAlertNotificationsFulfilled(),
        alertNotificationActions.pollAlertNotificationsDelaying(),
      ]);
    });

    it('should call api and dispatch pending, rejected actions', async () => {
      axiosMock.onGet().reply(500, { data: [] });

      const dispatched = await recordSaga(
        alertNotificationSagas.pollAlertNotification,
        alertNotificationActions.pollAlertNotifications('0', '1', ['CCBUFUSAGE', 'IRIBUFUSAGE'], '10', 'utcDateTime desc'),
      );

      expect(dispatched).toEqual([
        alertNotificationActions.pollAlertNotificationsPending(),
        alertNotificationActions.pollAlertNotificationsRejected(new Error('Request failed with status code 500')),
        alertNotificationActions.pollAlertNotificationsDelaying(),
      ]);
    });
  });
});
