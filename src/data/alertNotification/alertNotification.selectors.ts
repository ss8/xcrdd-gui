import { RootState } from '../root.reducers';
import { AlertNotificationModel } from './alertNotification.types';

// eslint-disable-next-line import/prefer-default-export
export const getAlertNotifications = (state: RootState): AlertNotificationModel[] => state.alertNotification.entities;
