import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockAlertNotifications } from '__test__/mockAlertNotifications';
import {
  getAlertNotifications,
} from './alertNotification.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './alertNotification.types';

describe('alertNotification.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of alertNotifications', () => {
    const action: types.AlertNotificationActionTypes = {
      type: types.GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getAlertNotifications(store.getState())).toEqual(mockAlertNotifications);
  });
});
