import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  AlertNotificationModel,
  AlarmType,
  API_PATH_ALERT_NOTIFICATIONS,
} from './alertNotification.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getAlertNotifications(
  deliveryFunctionId: string,
  alarmTypes?: AlarmType[],
  top?: string,
  orderBy?: 'utcDateTime desc',
): AxiosPromise<ServerResponse<AlertNotificationModel>> {
  const queryList: string[] = [];

  if (alarmTypes != null) {
    const filter = alarmTypes.map((alarm) => `(alarmType eq '${alarm}')`).join(' or ');
    queryList.push(`$filter=${filter}`);
  }

  if (top != null) {
    queryList.push(`$top=${top}`);
  }

  if (orderBy != null) {
    queryList.push(`$orderBy=${orderBy}`);
  }

  let query = '';
  if (queryList.length) {
    query = `?${queryList.join('&')}`;
  }

  return http.get(`${API_PATH_ALERT_NOTIFICATIONS}/${deliveryFunctionId}${query}`);
}
