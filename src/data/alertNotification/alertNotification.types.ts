import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse, DeliveryFunctionId } from 'data/types';

export const API_PATH_ALERT_NOTIFICATIONS = '/key-alarms';

export const GET_ALERT_NOTIFICATIONS = 'GET_ALERT_NOTIFICATIONS';
export const GET_ALERT_NOTIFICATIONS_PENDING = 'GET_ALERT_NOTIFICATIONS_PENDING';
export const GET_ALERT_NOTIFICATIONS_FULFILLED = 'GET_ALERT_NOTIFICATIONS_FULFILLED';
export const GET_ALERT_NOTIFICATIONS_REJECTED = 'GET_ALERT_NOTIFICATIONS_REJECTED';

export const POLL_ALERT_NOTIFICATIONS = 'POLL_ALERT_NOTIFICATIONS';
export const POLL_ALERT_NOTIFICATIONS_PENDING = 'POLL_ALERT_NOTIFICATIONS_PENDING';
export const POLL_ALERT_NOTIFICATIONS_FULFILLED = 'POLL_ALERT_NOTIFICATIONS_FULFILLED';
export const POLL_ALERT_NOTIFICATIONS_REJECTED = 'POLL_ALERT_NOTIFICATIONS_REJECTED';
export const POLL_ALERT_NOTIFICATIONS_DELAYING = 'POLL_ALERT_NOTIFICATIONS_DELAYING';

export interface AlertNotificationModel {
  id: string
  procName: string
  alarmType: AlarmType
  message: string
  utcDateTime: string
}

export type AlarmType = 'NEWIP' | 'CCBUFUSAGE' | 'IRIBUFUSAGE' | 'AUDITDISC' | 'DYNIPLKPFAILURE';

export interface AlertNotificationState {
  entities: AlertNotificationModel[]
}

export interface GetAlertNotificationAction extends AnyAction {
  type: typeof GET_ALERT_NOTIFICATIONS
  payload: AxiosPromise<ServerResponse<AlertNotificationModel>>
}

export interface GetAlertNotificationPendingAction extends AnyAction {
  type: typeof GET_ALERT_NOTIFICATIONS_PENDING
  payload: AxiosResponse<ServerResponse<AlertNotificationModel>>
}

export interface GetAlertNotificationFulfilledAction extends AnyAction {
  type: typeof GET_ALERT_NOTIFICATIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<AlertNotificationModel>>
}

export interface GetAlertNotificationRejectedAction extends AnyAction {
  type: typeof GET_ALERT_NOTIFICATIONS_REJECTED
  payload: AxiosResponse<ServerResponse<AlertNotificationModel>>
}

export interface PollAlertNotificationAction extends AnyAction {
  type: typeof POLL_ALERT_NOTIFICATIONS
  payload: DeliveryFunctionId & {
    pollingTime: string,
    alarmTypes?: AlarmType[],
    top?: string,
    orderBy?: 'utcDateTime desc',
  }
}

export interface PollAlertNotificationPendingAction extends AnyAction {
  type: typeof POLL_ALERT_NOTIFICATIONS_PENDING
}

export interface PollAlertNotificationFulfilledAction extends AnyAction {
  type: typeof POLL_ALERT_NOTIFICATIONS_FULFILLED
}

export interface PollAlertNotificationRejectedAction extends AnyAction {
  type: typeof POLL_ALERT_NOTIFICATIONS_REJECTED
  payload: { error: Error }
}

export interface PollAlertNotificationDelayingAction extends AnyAction {
  type: typeof POLL_ALERT_NOTIFICATIONS_DELAYING
}

export type AlertNotificationActionTypes =
  GetAlertNotificationAction |
  GetAlertNotificationPendingAction |
  GetAlertNotificationFulfilledAction |
  GetAlertNotificationRejectedAction |
  PollAlertNotificationAction |
  PollAlertNotificationPendingAction |
  PollAlertNotificationFulfilledAction |
  PollAlertNotificationRejectedAction |
  PollAlertNotificationDelayingAction;
