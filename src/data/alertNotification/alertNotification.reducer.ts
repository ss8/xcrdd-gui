import {
  GET_ALERT_NOTIFICATIONS_FULFILLED,
  AlertNotificationActionTypes,
  AlertNotificationState,
} from './alertNotification.types';

const initialState: AlertNotificationState = {
  entities: [],
};

export default function alertNotificationReducer(
  state = initialState,
  action: AlertNotificationActionTypes,
): AlertNotificationState {
  switch (action.type) {
    case GET_ALERT_NOTIFICATIONS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
