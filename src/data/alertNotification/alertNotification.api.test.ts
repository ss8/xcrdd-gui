import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockAlertNotifications } from '__test__/mockAlertNotifications';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './alertNotification.api';
import { API_PATH_ALERT_NOTIFICATIONS } from './alertNotification.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('alertNotification.api', () => {
  it('should request the list for alertNotifications', async () => {
    const response = buildAxiosServerResponse(mockAlertNotifications);
    axiosMock.onGet(`${API_PATH_ALERT_NOTIFICATIONS}/0`).reply(200, response);
    await expect(api.getAlertNotifications('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_ALERT_NOTIFICATIONS}/0` },
      status: 200,
    });
  });

  it('should request the list for alertNotifications with filter', async () => {
    const response = buildAxiosServerResponse(mockAlertNotifications);
    axiosMock.onGet(`${API_PATH_ALERT_NOTIFICATIONS}/0?$filter=(alarmType eq 'CCBUFUSAGE') or (alarmType eq 'IRIBUFUSAGE')`).reply(200, response);
    await expect(api.getAlertNotifications('0', ['CCBUFUSAGE', 'IRIBUFUSAGE'])).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_ALERT_NOTIFICATIONS}/0?$filter=(alarmType eq 'CCBUFUSAGE') or (alarmType eq 'IRIBUFUSAGE')` },
      status: 200,
    });
  });

  it('should request the list for alertNotifications with top', async () => {
    const response = buildAxiosServerResponse(mockAlertNotifications);
    axiosMock.onGet(`${API_PATH_ALERT_NOTIFICATIONS}/0?$top=10`).reply(200, response);
    await expect(api.getAlertNotifications('0', undefined, '10')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_ALERT_NOTIFICATIONS}/0?$top=10` },
      status: 200,
    });
  });

  it('should request the list for alertNotifications with orderBy', async () => {
    const response = buildAxiosServerResponse(mockAlertNotifications);
    axiosMock.onGet(`${API_PATH_ALERT_NOTIFICATIONS}/0?$orderBy=utcDateTime desc`).reply(200, response);
    await expect(api.getAlertNotifications('0', undefined, undefined, 'utcDateTime desc')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_ALERT_NOTIFICATIONS}/0?$orderBy=utcDateTime desc` },
      status: 200,
    });
  });

  it('should request the list for alertNotifications with all', async () => {
    const response = buildAxiosServerResponse(mockAlertNotifications);
    axiosMock.onGet(`${API_PATH_ALERT_NOTIFICATIONS}/0?$filter=(alarmType eq 'CCBUFUSAGE') or (alarmType eq 'IRIBUFUSAGE')&$top=10&$orderBy=utcDateTime desc`).reply(200, response);
    await expect(api.getAlertNotifications('0', ['CCBUFUSAGE', 'IRIBUFUSAGE'], '10', 'utcDateTime desc')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_ALERT_NOTIFICATIONS}/0?$filter=(alarmType eq 'CCBUFUSAGE') or (alarmType eq 'IRIBUFUSAGE')&$top=10&$orderBy=utcDateTime desc` },
      status: 200,
    });
  });
});
