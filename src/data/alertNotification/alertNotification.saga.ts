import { AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import {
  call,
  put,
  takeLeading,
  all,
  delay,
} from 'redux-saga/effects';
import {
  getAlertNotificationsFulfilled,
  pollAlertNotificationsDelaying,
  pollAlertNotificationsFulfilled,
  pollAlertNotificationsPending,
  pollAlertNotificationsRejected,
} from './alertNotification.actions';
import * as notificationApi from './alertNotification.api';
import {
  AlertNotificationModel,
  PollAlertNotificationAction,
  POLL_ALERT_NOTIFICATIONS,
} from './alertNotification.types';

const DEFAULT_POLLING_TIME = '30';

export function* pollAlertNotification(action: PollAlertNotificationAction): Generator {
  const {
    deliveryFunctionId,
    pollingTime = DEFAULT_POLLING_TIME,
    alarmTypes,
    top,
    orderBy,
  } = action.payload;

  let loop = true;
  while (loop) {
    try {
      yield put(pollAlertNotificationsPending());
      const response = yield call(notificationApi.getAlertNotifications, deliveryFunctionId, alarmTypes, top, orderBy);
      yield put(getAlertNotificationsFulfilled(response as AxiosResponse<ServerResponse<AlertNotificationModel>>));
      yield put(pollAlertNotificationsFulfilled());

    } catch (err) {
      yield put(pollAlertNotificationsRejected(err));

    } finally {
      yield put(pollAlertNotificationsDelaying());
      yield delay(+pollingTime * 1000);
    }

    if (process.env.NODE_ENV === 'test') {
      loop = false;
    }
  }
}

function* alertNotificationSaga(): Generator {
  yield all([
    takeLeading(POLL_ALERT_NOTIFICATIONS, pollAlertNotification),
  ]);
}

export default alertNotificationSaga;
