import { spawn } from 'redux-saga/effects';
import alertNotificationSaga from './alertNotification/alertNotification.saga';
import authenticationSaga from './authentication/authentication.saga';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function* rootSaga() {
  yield spawn(alertNotificationSaga);
  yield spawn(authenticationSaga);
}
