import AuthActions, { AuthenticationState, SET_AUTHENTICATION_EXPIRED } from './authentication.types';

import {
  COOKIE, setCookie, removeCookie, AUTH, setAuth, removeAuth, clearAuth, getAuth,
} from '../../utils';

const defaultState: AuthenticationState = {
  isAuthenticated: getAuth(AUTH.TOKEN) !== null,
  isAuthenticationExpired: false,
  userId: '',
  userDBId: '',
  firstName: '',
  lastName: '',
  privileges: [],
  errorStatus: '',
  beforeLogout: false,
  passwordUpdate: false,
  loginMessage: '',
  logiTokenUpdated: false,
  sessionTimeout: '',
};

const authenticationReducer = (state = defaultState, action: any): AuthenticationState => {
  if (/_REJECTED/.test(action.type) && action.payload.status === 401) {
    return {
      ...state,
      isAuthenticated: false,
      isAuthenticationExpired: false,
      sessionTimeout: '',
    };
  }
  if (/(AUTH|LOGIN|LOGOUT).*_REJECTED/.test(action.type)) {
    const errorState = action.payload.message.match(/\d+/);

    if (state.passwordUpdate) {
      return { ...state, errorStatus: errorState === null ? '' : errorState[0] };
    }
    removeAuth(AUTH.TOKEN);
    removeAuth(AUTH.PRIVILEGES);
    // do not display error message
    if (action.type === AuthActions.CHECK_TOKEN_FULFILLED) return state;
    return {
      ...state,
      isAuthenticated: false,
      isAuthenticationExpired: false,
      sessionTimeout: '',
      errorStatus: errorState === null ? '' : errorState[0],
    };
  }
  let token: any;
  if (action.payload && action.payload.headers && action.payload.headers['x-auth-token']) {
    token = action.payload.headers['x-auth-token'];
  } else {
    token = getAuth(AUTH.TOKEN);
  }

  switch (action.type) {
    case AuthActions.CHECK_TOKEN_FULFILLED:
      setAuth(AUTH.PRIVILEGES, action.payload.data.data[0].privileges, true);
      setAuth(AUTH.USERID, action.payload.data.data[0].userID, true);
      setAuth(AUTH.USERDBID, action.payload.data.data[0].id, true);
      return {
        ...state,
        ...defaultState,
        isAuthenticated: true,
        isAuthenticationExpired: false,
        userId: action.payload.data.data[0].userID,
        userDBId: action.payload.data.data[0].id,
        privileges: action.payload.data.data[0].privileges,
        firstName: action.payload.data.data[0].firstName,
        lastName: action.payload.data.data[0].lastName,
        sessionTimeout: action.payload.data.data[0].sessionTimeout,
      };
    case AuthActions.LOGIN_FULFILLED:
      if (!token) return state;
      const {
        privileges,
        userID,
        jwtToken,
        id,
        firstName,
        lastName,
        sessionTimeout,
      } = action.payload.data;
      setAuth(AUTH.PRIVILEGES, privileges, true);
      setAuth(AUTH.USERID, userID, true);
      setAuth(AUTH.USERDBID, id, true);
      setAuth(AUTH.TOKEN, token);
      setAuth(AUTH.JWTTOKEN, jwtToken);

      // We need the following cookies to display XMS in DX
      setCookie(COOKIE.USERNAME, userID);
      setCookie(COOKIE.USEPOLICY, 'Accept');

      return {
        ...state,
        ...defaultState,
        isAuthenticated: true,
        isAuthenticationExpired: false,
        userId: userID,
        userDBId: id,
        privileges,
        firstName,
        lastName,
        sessionTimeout,
      };
    case AuthActions.LOGINMESSAGE_FULFILLED:
      const message = action.payload.data.data[0].contents.replace(/\n/, '');
      return { ...state, loginMessage: message };
    case AuthActions.LOGOUT_FULFILLED:
      if (state.passwordUpdate) {
        return { ...state };
      }

      clearAuth();
      removeCookie(COOKIE.JSESSIONID);
      removeCookie(COOKIE.USERNAME);
      removeCookie(COOKIE.USEPOLICY);
      return {
        ...state,
        ...defaultState,
        isAuthenticated: false,
        isAuthenticationExpired: false,
        sessionTimeout: '',
      };
    case AuthActions.BEFORE_LOGOUT:
      return { ...state, beforeLogout: true };
    case AuthActions.PASSWORD_UPDATE:
      return { ...state, passwordUpdate: true };
    case AuthActions.UNDOPASSWORD_UPDATE:
      return { ...state, passwordUpdate: false };
    case AuthActions.LOGINMESSAGE_CLEAR:
      return { ...state, loginMessage: '' };
    case AuthActions.AUTH_CLEAR_ERRORSTATUS:
      return { ...state, errorStatus: '' };
    case AuthActions.LOGI_TOKEN_UPDATE:
      return { ...state, logiTokenUpdated: true };

    case SET_AUTHENTICATION_EXPIRED:
      return {
        ...state,
        isAuthenticationExpired: true,
      };

    default:
      return state;
  }
};
export default authenticationReducer;
