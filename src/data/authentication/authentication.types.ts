import { AnyAction } from 'redux';

const AuthActions = {
  LOGIN: 'LOGIN',
  LOGIN_FULFILLED: 'LOGIN_FULFILLED',
  BEFORE_LOGOUT: 'BEFORE_LOGOUT',
  LOGOUT: 'LOGOUT',
  LOGOUT_FULFILLED: 'LOGOUT_FULFILLED',
  CHECK_TOKEN: 'CHECK_TOKEN',
  CHECK_TOKEN_FULFILLED: 'CHECK_TOKEN_FULFILLED',
  AUTH_CLEAR_ERRORSTATUS: 'AUTH_CLEAR_ERRORSTATUS',
  PASSWORD_UPDATE: 'PASSWORD_UPDATE',
  UNDOPASSWORD_UPDATE: 'UNDOPASSWORD_UPDATE',
  LOGINMESSAGE: 'LOGINMESSAGE',
  LOGINMESSAGE_FULFILLED: 'LOGINMESSAGE_FULFILLED',
  LOGINMESSAGE_CLEAR: 'LOGINMESSAGE_CLEAR',
  CHECK_IF_LOGI_USER_EXISTS: 'CHECK_IF_LOGI_USER_EXISTS',
  CREATE_LOGI_USER: 'CREATE_LOGI_USER',
  LOGI_TOKEN_UPDATE: 'LOGI_TOKEN_UPDATE',
};

export const DEFAULT_AUTHENTICATION_TIMEOUT_SECONDS = 1800;

export const START_AUTHENTICATION_TIMEOUT = 'START_AUTHENTICATION_TIMEOUT';
export const RENEW_AUTHENTICATION_TIMEOUT = 'RENEW_AUTHENTICATION_TIMEOUT';
export const SET_AUTHENTICATION_EXPIRED = 'SET_AUTHENTICATION_EXPIRED';

export interface AuthenticationState {
  isAuthenticated: boolean
  isAuthenticationExpired: boolean
  sessionTimeout: string,
  userId: string
  userDBId: string
  firstName?: string
  lastName?: string
  privileges: string[],
  errorStatus: string
  beforeLogout: boolean,
  passwordUpdate: boolean,
  loginMessage: string
  logiTokenUpdated: boolean,
}

export default AuthActions;

export interface StartAuthenticationTimeoutAction extends AnyAction {
  type: typeof START_AUTHENTICATION_TIMEOUT
  payload: { authenticationTimeout: number }
}

export interface RenewAuthenticationTimeoutAction extends AnyAction {
  type: typeof RENEW_AUTHENTICATION_TIMEOUT
}

export interface SetAuthenticationExpiredAction extends AnyAction {
  type: typeof SET_AUTHENTICATION_EXPIRED
}
