import axios from 'axios';

import Http from 'shared/http';
import { getLogiGroupIdsAndRoles, generateLogiToken, decryptText } from 'utils/auth';
import { dashboardPrivileges } from 'shared/constants';
import AuthActions, {
  RenewAuthenticationTimeoutAction,
  RENEW_AUTHENTICATION_TIMEOUT,
  SetAuthenticationExpiredAction,
  SET_AUTHENTICATION_EXPIRED,
  StartAuthenticationTimeoutAction,
  START_AUTHENTICATION_TIMEOUT,
} from './authentication.types';

interface CreateLogiUserArgs {
  user: string;
  password: string;
  privileges: string[];
  externalDashboardIP: string;
  logiAdminAccount: string;
  logiAdminUserName: string;
  logiAdminPswd: string;
  logiSupervisorUserName: string;
  logiSupervisorPswd: string;
}

interface LogiAuthArgs extends CreateLogiUserArgs {
  logiOAuthClientId: string;
  logiOAuthClientName: string;
}

interface LogiTokenAuthArgs {
  user: string;
  externalDashboardIP: string;
  logiOAuthClientId: string;
  logiOAuthClientName: string;
  logiSupervisorUserName: string;
  logiSupervisorPswd: string;
}

interface LogiUserPrivilegesArgs {
  privileges: string[];
  externalDashboardIP: string;
  logiAdminAccount: string;
  logiAdminUserName: string;
  logiAdminPswd: string;
  logiSupervisorUserName: string;
  logiSupervisorPswd: string;
  logiUserId: string;
  user: string;
  password: string;
}

const http = Http.getHttp();

// action creators
export const doLogin = (body: any) => ({ type: AuthActions.LOGIN, payload: http.post('/users/authenticate', body) });
export const doFetchLoginMessage = () => ({ type: AuthActions.LOGINMESSAGE, payload: http.get('/prelogin/latest') });
export const clearLoginMessage = () => ({ type: AuthActions.LOGINMESSAGE_CLEAR });

const doLogout = () => {
  return ({
    type: AuthActions.LOGOUT,
    payload: http.post('/logout'),
  });
};

const doBeforeLogout = () => ({ type: AuthActions.BEFORE_LOGOUT });

/**
 * This function dispatch logout events and when the event handling is completed,
 * it performs logout.
 */
export const handleLogout = () => {
  return async (dispatch: any) => {
    await dispatch(doBeforeLogout());
    // Let all the BEFORE_LOGOUT listeners completed their actions
    // (request + response) before calling doLogout.  Calling doLogout
    // too early will cause REST API server unable to complete the outstanding
    // before_logout requests.
    setTimeout(() => {
      dispatch(doLogout());
    }, 1000);
  };
};

export const setPasswordUpdateFlag = () => ({ type: AuthActions.PASSWORD_UPDATE });
export const undoSetPasswordUpdateFlag = () => ({ type: AuthActions.UNDOPASSWORD_UPDATE });
export const doCheckToken = (token: string) =>
  ({ type: AuthActions.CHECK_TOKEN, payload: http.get(`/session?x-auth-token=${token}`) });
export const clearErrorStatus = () => ({ type: AuthActions.AUTH_CLEAR_ERRORSTATUS });

export function startAuthenticationTimeout(
  authenticationTimeout: number,
): StartAuthenticationTimeoutAction {
  return {
    type: START_AUTHENTICATION_TIMEOUT,
    payload: { authenticationTimeout },
  };
}

export function renewAuthenticationTimeout(): RenewAuthenticationTimeoutAction {
  return {
    type: RENEW_AUTHENTICATION_TIMEOUT,
  };
}

export function setAuthenticationExpired(): SetAuthenticationExpiredAction {
  return {
    type: SET_AUTHENTICATION_EXPIRED,
  };
}

// TODO: Refactor Logi login code - move to separate file?
export const checkLogiToken = ({
  user,
  externalDashboardIP,
  logiOAuthClientId,
  logiOAuthClientName,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: LogiTokenAuthArgs) => {
  return async () => {
    if (localStorage.getItem('logi_auth_token')) {
      if (
        new Date().getTime() >
        new Date(
          localStorage.getItem('logi_auth_token_expiration') as string,
        ).getTime()
      ) {
        const { tokenValue, expiration } = await generateLogiToken({
          user,
          externalDashboardIP,
          logiOAuthClientId,
          logiOAuthClientName,
          logiSupervisorUserName,
          logiSupervisorPswd,
        });

        localStorage.setItem('logi_auth_token', tokenValue);
        localStorage.setItem('logi_auth_token_expiration', expiration);
      }
    }
  };
};

export const createLogiUser = ({
  user,
  password,
  privileges,
  externalDashboardIP,
  logiAdminAccount,
  logiAdminUserName,
  logiAdminPswd,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: CreateLogiUserArgs) => {
  return async () => {
    try {
      const {
        groupIds,
      } = await getLogiGroupIdsAndRoles({
        externalDashboardIP,
        logiAdminAccount,
        logiAdminPswd,
        logiAdminUserName,
        privileges,
      });

      await axios.post(
        `${externalDashboardIP}/zoomdata/api/users`,
        {},
        {
          headers: {
            Accept: 'application/vnd.zoomdata.v2+json',
            'Content-Type': 'application/vnd.zoomdata.v2+json',
          },
          auth: {
            username: decryptText(logiAdminUserName),
            password: decryptText(logiAdminPswd),
          },
          data: {
            accountId: logiAdminAccount,
            accounts: [
              {
                accountDisabled: false,
                accountId: logiAdminAccount,
                accountName: 'company',
                groups: groupIds,
                disabled: false,
                userAttributes: [],
              },
            ],
            activeGroups: groupIds,
            availableAttributeKeys: [],
            deletable: false,
            external: false,
            forcePasswordChange: false,
            fullname: user,
            languageLocaleId: 'en_US',
            localeSettingsId: 'en_US',
            name: user,
            password,
            showQuickstart: false,
            version: 1,
          },
        },
      );
    } catch (err) {
      // TODO: log error
      // console.log('Error while creating user: ', err);
    }
  };
};

const editLogiUserPrivileges = ({
  privileges,
  externalDashboardIP,
  logiAdminAccount,
  logiAdminUserName,
  logiAdminPswd,
  logiSupervisorUserName,
  logiSupervisorPswd,
  logiUserId,
  user,
  password,
}: LogiUserPrivilegesArgs) => {
  return async () => {
    try {
      const {
        // roles,
        groupIds,
      } = await getLogiGroupIdsAndRoles({
        externalDashboardIP,
        logiAdminAccount,
        logiAdminPswd,
        logiAdminUserName,
        privileges,
      });

      const {
        data: { version },
      } = await axios.get(
        `${externalDashboardIP}/zoomdata/api/users/${logiUserId}`,
        {
          headers: {
            Accept: 'application/vnd.zoomdata.v2+json',
          },
          auth: {
            username: decryptText(logiSupervisorUserName),
            password: decryptText(logiSupervisorPswd),
          },
        },
      );

      await axios.put(
        `${externalDashboardIP}/zoomdata/api/users/${logiUserId}`,
        {},
        {
          headers: {
            Accept: 'application/vnd.zoomdata.v2+json',
            'Content-Type': 'application/vnd.zoomdata.v2+json',
          },
          auth: {
            username: decryptText(logiAdminUserName),
            password: decryptText(logiAdminPswd),
          },
          data: {
            accountId: logiAdminAccount,
            accounts: [
              {
                accountDisabled: false,
                accountId: logiAdminAccount,
                accountName: 'company',
                groups: groupIds,
                disabled: false,
                userAttributes: [],
              },
            ],
            activeGroups: groupIds,
            availableAttributeKeys: [],
            deletable: false,
            external: false,
            forcePasswordChange: false,
            fullname: user,
            languageLocaleId: 'en_US',
            localeSettingsId: 'en_US',
            name: user,
            password,
            showQuickstart: false,
            version,
          },
        },
      );
    } catch (err) {
      // TODO: log error
      // console.log('Error while editing Logi user privileges: ', err);
    }
  };
};

export const authenticateLogiUser = ({
  user,
  password,
  privileges,
  externalDashboardIP,
  logiOAuthClientId,
  logiOAuthClientName,
  logiAdminAccount,
  logiAdminUserName,
  logiAdminPswd,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: LogiAuthArgs) => {
  return async (dispatch: any) => {
    let logiUser: any;

    const hasDashboardPrivilege = privileges.some(
      (r) => dashboardPrivileges.indexOf(r) >= 0,
    );

    if (hasDashboardPrivilege) {
      const logiUsersUrl = `${externalDashboardIP}/zoomdata/api/users?limit=-1&offset=0`;

      try {
        const { data: logiUsers } = await axios.get(logiUsersUrl, {
          headers: {
            Accept: 'application/vnd.zoomdata.v2+json',
          },
          auth: {
            username: decryptText(logiSupervisorUserName),
            password: decryptText(logiSupervisorPswd),
          },
        });

        const logiUserExists = logiUsers.some((currLogiUser: any) => {
          logiUser = currLogiUser;
          return currLogiUser.name === user;
        });

        if (!logiUserExists) {
          await dispatch(
            createLogiUser({
              user,
              password,
              privileges,
              externalDashboardIP,
              logiAdminAccount,
              logiAdminUserName,
              logiAdminPswd,
              logiSupervisorUserName,
              logiSupervisorPswd,
            }),
          );
        } else if (logiUser) {
          await dispatch(
            editLogiUserPrivileges({
              privileges,
              externalDashboardIP,
              logiAdminAccount,
              logiAdminUserName,
              logiAdminPswd,
              logiSupervisorUserName,
              logiSupervisorPswd,
              logiUserId: logiUser.id,
              user,
              password,
            }),
          );
        }

        const { tokenValue, expiration } = await generateLogiToken({
          user,
          externalDashboardIP,
          logiOAuthClientId,
          logiOAuthClientName,
          logiSupervisorUserName,
          logiSupervisorPswd,
        });

        localStorage.setItem('logi_auth_token', tokenValue);
        localStorage.setItem('logi_auth_token_expiration', expiration);

        await dispatch({
          type: 'LOGI_TOKEN_UPDATE',
        });
      } catch (err) {
        // TODO: Implement some sort of error log here
        // console.log('Error while authenticating Logi user: ', err);
      }
    }
  };
};
