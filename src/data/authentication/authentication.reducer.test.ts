import { DELETE_ACCESS_FUNCTION_REJECTED } from 'data/accessFunction/accessFunction.types';
import authenticationReducer from './authentication.reducer';
import AuthActions, { AuthenticationState, SET_AUTHENTICATION_EXPIRED } from './authentication.types';

describe('nodes.reducer', () => {
  let defaultState: AuthenticationState;

  beforeEach(() => {
    defaultState = {
      isAuthenticated: false,
      isAuthenticationExpired: false,
      userId: '',
      userDBId: '',
      firstName: '',
      lastName: '',
      privileges: [],
      errorStatus: '',
      beforeLogout: false,
      passwordUpdate: false,
      loginMessage: '',
      logiTokenUpdated: false,
      sessionTimeout: '',
    };
  });

  it('should handle *_REJECTED with error as 401', () => {
    defaultState.isAuthenticated = true;
    defaultState.isAuthenticationExpired = true;

    expect(authenticationReducer(defaultState, {
      type: DELETE_ACCESS_FUNCTION_REJECTED,
      payload: {
        status: 401,
        data: {},
      },
    })).toEqual({
      ...defaultState,
      isAuthenticated: false,
      isAuthenticationExpired: false,
    });
  });

  it('should handle LOGIN_REJECTED', () => {
    defaultState.isAuthenticated = true;
    defaultState.isAuthenticationExpired = true;

    expect(authenticationReducer(defaultState, {
      type: DELETE_ACCESS_FUNCTION_REJECTED,
      payload: {
        status: 401,
        data: {},
      },
    })).toEqual({
      ...defaultState,
      isAuthenticated: false,
      isAuthenticationExpired: false,
    });
  });

  it('should handle CHECK_TOKEN_FULFILLED', () => {
    defaultState.isAuthenticated = true;
    defaultState.isAuthenticationExpired = true;

    expect(authenticationReducer(defaultState, {
      type: 'LOGOUT_REJECTED',
      payload: {
        message: '',
        data: {},
      },
    })).toEqual({
      ...defaultState,
      isAuthenticated: false,
      isAuthenticationExpired: false,
    });
  });

  it('should handle LOGIN_FULFILLED', () => {
    expect(authenticationReducer(defaultState, {
      type: AuthActions.LOGIN_FULFILLED,
      payload: {
        headers: {
          'x-auth-token': 'x-token',
        },
        data: {
          privileges: ['some'],
          userID: 'userId',
          jwtToken: 'token',
          id: 'userDBId',
          firstName: 'firstName',
          lastName: 'lastName',
          sessionTimeout: '1800',
        },
      },
    })).toEqual({
      ...defaultState,
      isAuthenticated: true,
      isAuthenticationExpired: false,
      userId: 'userId',
      userDBId: 'userDBId',
      firstName: 'firstName',
      lastName: 'lastName',
      privileges: ['some'],
      sessionTimeout: '1800',
    });
  });

  it('should handle LOGOUT_FULFILLED', () => {
    const state = {
      ...defaultState,
      isAuthenticated: true,
      isAuthenticationExpired: true,
      userId: 'userId',
      userDBId: 'userDBId',
      firstName: 'firstName',
      lastName: 'lastName',
      privileges: ['some'],
      errorStatus: 'error',
      beforeLogout: true,
      loginMessage: 'message',
      logiTokenUpdated: true,
      sessionTimeout: '1800',
    };

    expect(authenticationReducer(state, {
      type: AuthActions.LOGOUT_FULFILLED,
    })).toEqual({
      ...defaultState,
    });
  });

  it('should handle SET_AUTHENTICATION_EXPIRED', () => {
    expect(authenticationReducer(defaultState, {
      type: SET_AUTHENTICATION_EXPIRED,
    })).toEqual({
      ...defaultState,
      isAuthenticationExpired: true,
    });
  });
});
