import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import rootReducer, { RootState } from 'data/root.reducers';
import {
  getAuthenticationPrivileges,
  getAuthenticationUserId,
  getAuthenticationUserName,
  getAuthenticationBeforeLogout,
  isAuthenticated,
  isAuthenticationExpired,
  getAuthenticationTimeout,
} from './authentication.selectors';
import AuthActions from './authentication.types';

describe('authentication.selectors', () => {
  let checkTokenResponse: any;
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    checkTokenResponse = {
      userID: 'userId1',
      id: 10,
      privileges: [
        'privileges1',
        'privileges2',
      ],
      sessionTimeout: '1800',
    };

    const action: AnyAction = {
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    };
    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, action));
  });

  it('should return if authenticated', () => {
    expect(isAuthenticated(store.getState())).toEqual(true);
  });

  it('should return if authentication expired', () => {
    expect(isAuthenticationExpired(store.getState())).toEqual(false);
  });

  it('should return the value for authenticationTimeout', () => {
    expect(getAuthenticationTimeout(store.getState())).toBe('1800');
  });

  it('should return the user id', () => {
    expect(getAuthenticationUserId(store.getState())).toEqual(10);
  });

  it('should return the user name', () => {
    expect(getAuthenticationUserName(store.getState())).toEqual('userId1');
  });

  it('should return the privileges', () => {
    expect(getAuthenticationPrivileges(store.getState())).toEqual(['privileges1', 'privileges2']);
  });

  it('should return the privileges', () => {
    expect(getAuthenticationBeforeLogout(store.getState())).toEqual(false);
  });
});
