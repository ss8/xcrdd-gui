import { RootState } from 'data/root.reducers';

export const isAuthenticated = (state: RootState): boolean => state.authentication.isAuthenticated;
export const isAuthenticationExpired = (state: RootState): boolean => state.authentication.isAuthenticationExpired;
export const getAuthenticationTimeout = (state: RootState): string => state.authentication.sessionTimeout;
export const getAuthenticationUserId = (state: RootState): string => state.authentication.userDBId;
export const getAuthenticationUserName = (state: RootState): string => state.authentication.userId;
export const getAuthenticationPrivileges = (state: RootState): string[] => state.authentication.privileges;
export const getAuthenticationBeforeLogout = (state: RootState): boolean => state.authentication.beforeLogout;
