import {
  all,
  delay,
  put,
  race,
  take,
  takeEvery,
} from 'redux-saga/effects';
import authenticationSaga, * as authenticationSagas from './authentication.saga';
import * as authenticationActions from './authentication.actions';
import { RENEW_AUTHENTICATION_TIMEOUT, START_AUTHENTICATION_TIMEOUT } from './authentication.types';

describe('authentication.saga', () => {
  describe('authenticationSaga()', () => {
    it('should handle the related sagas', () => {
      const saga = authenticationSaga();

      expect(saga.next().value).toEqual(
        all([
          takeEvery(
            START_AUTHENTICATION_TIMEOUT,
            authenticationSagas.startAuthenticationTimeout,
          ),
        ]),
      );
    });
  });

  describe('startAuthenticationTimeout()', () => {
    it('should dispatch start authentication timeout on renew', async () => {
      const initialAction = authenticationActions.startAuthenticationTimeout(1800);
      const renewAction = authenticationActions.renewAuthenticationTimeout();

      const gen = authenticationSagas.startAuthenticationTimeout(initialAction);

      expect(gen.next().value).toEqual(
        race({
          renewAction: take(RENEW_AUTHENTICATION_TIMEOUT),
          timeout: delay(1800000),
        }),
      );

      expect(gen.next({ renewAction }).value).toEqual(
        put(authenticationActions.startAuthenticationTimeout(1800)),
      );
    });

    it('should dispatch logout on timeout', async () => {
      const initialAction = authenticationActions.startAuthenticationTimeout(1);

      const gen = authenticationSagas.startAuthenticationTimeout(initialAction);

      expect(gen.next().value).toEqual(
        race({
          renewAction: take(RENEW_AUTHENTICATION_TIMEOUT),
          timeout: delay(1000),
        }),
      );

      await new Promise((resolve) => setTimeout(resolve, 1000));

      expect(gen.next({}).value).toEqual(
        put(authenticationActions.setAuthenticationExpired()),
      );
    });

    it('should use default timeout if not defined', () => {
      const initialAction = authenticationActions.startAuthenticationTimeout(0);

      const gen = authenticationSagas.startAuthenticationTimeout(initialAction);

      expect(gen.next().value).toEqual(
        race({
          renewAction: take(RENEW_AUTHENTICATION_TIMEOUT),
          timeout: delay(1800000),
        }),
      );
    });
  });
});
