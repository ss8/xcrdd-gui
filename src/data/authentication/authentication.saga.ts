import {
  all,
  delay,
  put,
  race,
  take,
  takeEvery,
} from 'redux-saga/effects';
import * as authenticationActions from './authentication.actions';
import {
  DEFAULT_AUTHENTICATION_TIMEOUT_SECONDS,
  RenewAuthenticationTimeoutAction,
  RENEW_AUTHENTICATION_TIMEOUT,
  StartAuthenticationTimeoutAction,
  START_AUTHENTICATION_TIMEOUT,
} from './authentication.types';

type RaceType = {
  renewAction?: RenewAuthenticationTimeoutAction,
  timeout?: boolean,
};

export function* startAuthenticationTimeout(action: StartAuthenticationTimeoutAction): Generator {
  let {
    authenticationTimeout,
  } = action.payload;

  if (authenticationTimeout == null || authenticationTimeout === 0) {
    authenticationTimeout = DEFAULT_AUTHENTICATION_TIMEOUT_SECONDS;
  }

  const result = yield race({
    renewAction: take(RENEW_AUTHENTICATION_TIMEOUT),
    timeout: delay(authenticationTimeout * 1000),
  });

  const {
    renewAction,
  } = result as RaceType;

  if (renewAction) {
    yield put(authenticationActions.startAuthenticationTimeout(action.payload.authenticationTimeout));

  } else {
    yield put(authenticationActions.setAuthenticationExpired());
  }
}

function* authenticationSaga(): Generator {
  yield all([
    takeEvery(START_AUTHENTICATION_TIMEOUT, startAuthenticationTimeout),
  ]);
}

export default authenticationSaga;
