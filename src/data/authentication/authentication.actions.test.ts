import { renewAuthenticationTimeout, setAuthenticationExpired, startAuthenticationTimeout } from './authentication.actions';
import { RENEW_AUTHENTICATION_TIMEOUT, SET_AUTHENTICATION_EXPIRED, START_AUTHENTICATION_TIMEOUT } from './authentication.types';

describe('authentication.actions', () => {

  it('should create an action to start the authentication timeout', () => {
    expect(startAuthenticationTimeout(1800)).toEqual({
      type: START_AUTHENTICATION_TIMEOUT,
      payload: { authenticationTimeout: 1800 },
    });
  });

  it('should create and action to renew the authentication timeout', () => {
    expect(renewAuthenticationTimeout()).toEqual({
      type: RENEW_AUTHENTICATION_TIMEOUT,
    });
  });

  it('should create and action to set the authentication as expired', () => {
    expect(setAuthenticationExpired()).toEqual({
      type: SET_AUTHENTICATION_EXPIRED,
    });
  });
});
