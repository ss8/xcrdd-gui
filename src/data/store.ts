import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import promiseMiddleware from 'redux-promise-middleware';
import reduxThunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import reducers from './root.reducers';
import rootSaga from './root.saga';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, composeWithDevTools(
  applyMiddleware(reduxThunkMiddleware, promiseMiddleware, sagaMiddleware),
));

sagaMiddleware.run(rootSaga);

export default store;
