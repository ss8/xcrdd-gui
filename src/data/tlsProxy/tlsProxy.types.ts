import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { Operation, ServerResponse } from 'data/types';
import { ThunkAction } from 'redux-thunk';
import { RootState } from 'data/root.reducers';

export const API_PATH_TLS_PROXIES = '/ss8Proxies';

export const TLS_PROXY_USER_CATEGORY_SETTINGS = 'TlsProxySettings';

export const GO_TO_TLS_PROXY_LIST = 'GO_TO_TLS_PROXY_LIST';
export const GO_TO_TLS_PROXY_CREATE = 'GO_TO_TLS_PROXY_CREATE';
export const GO_TO_TLS_PROXY_EDIT = 'GO_TO_TLS_PROXY_EDIT';
export const GO_TO_TLS_PROXY_VIEW = 'GO_TO_TLS_PROXY_VIEW';
export const GO_TO_TLS_PROXY_COPY = 'GO_TO_TLS_PROXY_COPY';
export const GET_TLS_PROXY_LIST = 'GET_TLS_PROXY_LIST';
export const GET_TLS_PROXY_LIST_PENDING = 'GET_TLS_PROXY_LIST_PENDING';
export const GET_TLS_PROXY_LIST_FULFILLED = 'GET_TLS_PROXY_LIST_FULFILLED';
export const GET_TLS_PROXY_LIST_REJECTED = 'GET_TLS_PROXY_LIST_REJECTED';
export const GET_TLS_PROXY_LIST_WITH_VIEW = 'GET_TLS_PROXY_LIST_WITH_VIEW';
export const GET_TLS_PROXY_LIST_WITH_VIEW_PENDING = 'GET_TLS_PROXY_LIST_WITH_VIEW_PENDING';
export const GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED = 'GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED';
export const GET_TLS_PROXY_LIST_WITH_VIEW_REJECTED = 'GET_TLS_PROXY_LIST_WITH_VIEW_REJECTED';
export const GET_TLS_PROXY_LIST_BY_FILTER = 'GET_TLS_PROXY_LIST_BY_FILTER';
export const GET_TLS_PROXY_LIST_BY_FILTER_PENDING = 'GET_TLS_PROXY_LIST_BY_FILTER_PENDING';
export const GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED = 'GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED';
export const GET_TLS_PROXY_LIST_BY_FILTER_REJECTED = 'GET_TLS_PROXY_LIST_BY_FILTER_REJECTED';
export const GET_TLS_PROXY = 'GET_TLS_PROXY';
export const GET_TLS_PROXY_PENDING = 'GET_TLS_PROXY_PENDING';
export const GET_TLS_PROXY_FULFILLED = 'GET_TLS_PROXY_FULFILLED';
export const GET_TLS_PROXY_REJECTED = 'GET_TLS_PROXY_REJECTED';
export const CREATE_TLS_PROXY = 'CREATE_TLS_PROXY';
export const CREATE_TLS_PROXY_PENDING = 'CREATE_TLS_PROXY_PENDING';
export const CREATE_TLS_PROXY_FULFILLED = 'CREATE_TLS_PROXY_FULFILLED';
export const CREATE_TLS_PROXY_REJECTED = 'CREATE_TLS_PROXY_REJECTED';
export const UPDATE_TLS_PROXY = 'UPDATE_TLS_PROXY';
export const UPDATE_TLS_PROXY_PENDING = 'UPDATE_TLS_PROXY_PENDING';
export const UPDATE_TLS_PROXY_FULFILLED = 'UPDATE_TLS_PROXY_FULFILLED';
export const UPDATE_TLS_PROXY_REJECTED = 'UPDATE_TLS_PROXY_REJECTED';
export const DELETE_TLS_PROXY = 'DELETE_TLS_PROXY';
export const DELETE_TLS_PROXY_PENDING = 'DELETE_TLS_PROXY_PENDING';
export const DELETE_TLS_PROXY_FULFILLED = 'DELETE_TLS_PROXY_FULFILLED';
export const DELETE_TLS_PROXY_REJECTED = 'DELETE_TLS_PROXY_REJECTED';
export const SELECT_TLS_PROXY = 'SELECT_TLS_PROXY';

export enum TlsProxyPrivileges {
  View = 'tp_access',
  Edit = 'tp_access',
  Delete = 'tp_access',
}

export interface TlsProxyListener {
  id?: string
  ipAddress: string
  port: string
  requiredState: 'ACTIVE' | 'INACTIVE'
  keepaliveTimeout: string
  maxConcurrentConnection: string
  secInfoId: string
  transport: 'TCP' | 'UDP'
  routeRule: 'SRCADDR' | 'INTERCEPT' | 'SESSION'
  status?: string
  operation?: Operation
}

export interface TlsProxyDestination {
  id?: string
  ipAddress: string
  port: string
  requiredState: 'ACTIVE' | 'INACTIVE'
  keepaliveInterval: string
  numOfConnections: string
  secInfoId: string
  transport: 'TCP' | 'ZPUSH'
  status?: string
  operation?: Operation
  keepaliveRetries?: string
}

export interface TlsProxy {
  id?: string
  name: string
  nodeName: string
  instanceId: string
  listeners: TlsProxyListener[]
  destinations: TlsProxyDestination[]
  operation?: Operation
}

export interface TlsProxyState {
  entities: TlsProxy[]
  filteredEntities: TlsProxy[]
  single: TlsProxy | null
  selected: TlsProxy[]
}

export interface GoToTlsProxyCreateAction extends AnyAction {
  type: typeof GO_TO_TLS_PROXY_CREATE
}

export interface GoToTlsProxyEditAction extends AnyAction {
  type: typeof GO_TO_TLS_PROXY_EDIT
}

export interface GoToTlsProxyViewAction extends AnyAction {
  type: typeof GO_TO_TLS_PROXY_VIEW
}

export interface GoToTlsProxyCopyAction extends AnyAction {
  type: typeof GO_TO_TLS_PROXY_COPY
}

export interface GetTlsProxyListAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST
  payload: AxiosPromise<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyListFulfilledAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyListWithViewAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST_WITH_VIEW
  payload: AxiosPromise<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyListWithViewFulfilledAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED
  payload: AxiosResponse<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyListByFilterAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST_BY_FILTER
  payload: AxiosPromise<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyListByFilterFulfilledAction extends AnyAction {
  type: typeof GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED
  payload: AxiosResponse<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyAction extends AnyAction {
  type: typeof GET_TLS_PROXY
  payload: AxiosPromise<ServerResponse<TlsProxy>>
}

export interface GetTlsProxyFulfilledAction extends AnyAction {
  type: typeof GET_TLS_PROXY_FULFILLED
  payload: AxiosResponse<ServerResponse<TlsProxy>>
}

export interface CreateTlsProxyThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}

export interface CreateTlsProxyAction extends AnyAction {
  type: typeof CREATE_TLS_PROXY
  payload: AxiosPromise<ServerResponse<void>>
}

export interface CreateTlsProxyFulfilledAction extends AnyAction {
  type: typeof CREATE_TLS_PROXY_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface CreateTlsProxyRejectedAction extends AnyAction {
  type: typeof CREATE_TLS_PROXY_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface UpdateTlsProxyThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}
export interface UpdateTlsProxyAction extends AnyAction {
  type: typeof UPDATE_TLS_PROXY
  payload: AxiosPromise<ServerResponse<void>>
}

export interface UpdateTlsProxyFulfilledAction extends AnyAction {
  type: typeof UPDATE_TLS_PROXY_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}
export interface UpdateTlsProxyRejectedAction extends AnyAction {
  type: typeof UPDATE_TLS_PROXY_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteTlsProxyThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}

export interface DeleteTlsProxyAction extends AnyAction {
  type: typeof DELETE_TLS_PROXY
  payload: AxiosPromise<ServerResponse<void>>
}

export interface DeleteTlsProxyFulfilledAction extends AnyAction {
  type: typeof DELETE_TLS_PROXY_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteTlsProxyRejectedAction extends AnyAction {
  type: typeof DELETE_TLS_PROXY_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface SelectTlsProxyAction extends AnyAction{
  type: typeof SELECT_TLS_PROXY
  payload: TlsProxy[]
}

export type TlsProxyActionTypes =
  GoToTlsProxyCreateAction |
  GoToTlsProxyEditAction |
  GoToTlsProxyViewAction |
  GoToTlsProxyCopyAction |
  GetTlsProxyListAction |
  GetTlsProxyListFulfilledAction |
  GetTlsProxyListWithViewAction |
  GetTlsProxyListWithViewFulfilledAction |
  GetTlsProxyListByFilterAction |
  GetTlsProxyListByFilterFulfilledAction |
  GetTlsProxyAction |
  GetTlsProxyFulfilledAction |
  CreateTlsProxyAction |
  CreateTlsProxyFulfilledAction |
  UpdateTlsProxyAction |
  UpdateTlsProxyFulfilledAction |
  DeleteTlsProxyAction |
  DeleteTlsProxyFulfilledAction |
  DeleteTlsProxyRejectedAction |
  SelectTlsProxyAction;
