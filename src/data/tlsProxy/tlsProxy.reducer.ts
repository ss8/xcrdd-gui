import {
  TlsProxyActionTypes,
  TlsProxyState,
  GET_TLS_PROXY_LIST,
  GET_TLS_PROXY_LIST_FULFILLED,
  GET_TLS_PROXY,
  GET_TLS_PROXY_FULFILLED,
  SELECT_TLS_PROXY,
  GO_TO_TLS_PROXY_CREATE,
  GO_TO_TLS_PROXY_EDIT,
  GO_TO_TLS_PROXY_COPY,
  GO_TO_TLS_PROXY_VIEW,
  GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED,
  GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED,
  GET_TLS_PROXY_LIST_BY_FILTER,
} from './tlsProxy.types';

const initialState: TlsProxyState = {
  entities: [],
  filteredEntities: [],
  single: null,
  selected: [],
};

export default function tlsProxyReducer(
  state = initialState,
  action: TlsProxyActionTypes,
): TlsProxyState {
  switch (action.type) {
    case GET_TLS_PROXY_LIST:
      return {
        ...state,
        selected: [],
      };

    case GET_TLS_PROXY_LIST_FULFILLED:
    case GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    case GET_TLS_PROXY_LIST_BY_FILTER:
      return {
        ...state,
        filteredEntities: [],
      };

    case GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED:
      return {
        ...state,
        filteredEntities: action.payload.data.data || [],
      };

    case GET_TLS_PROXY:
      return {
        ...state,
        single: null,
      };

    case GET_TLS_PROXY_FULFILLED:
      return {
        ...state,
        single: (action.payload.data.data ?? [])[0],
      };

    case SELECT_TLS_PROXY:
      return {
        ...state,
        selected: action.payload || [],
      };

    case GO_TO_TLS_PROXY_CREATE:
    case GO_TO_TLS_PROXY_EDIT:
    case GO_TO_TLS_PROXY_COPY:
    case GO_TO_TLS_PROXY_VIEW:
      return {
        ...state,
        single: null,
      };

    default:
      return state;
  }
}
