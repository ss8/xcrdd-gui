import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './tlsProxy.reducer';
import * as types from './tlsProxy.types';
import * as api from './tlsProxy.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('tlsProxy.reducer', () => {
  beforeAll(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should handle GET_TLS_PROXY_LIST', () => {
    expect(reducer(undefined, {
      type: types.GET_TLS_PROXY_LIST,
      payload: api.getTlsProxyList('0'),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY_LIST_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_TLS_PROXY_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockTlsProxies),
    })).toEqual({
      entities: mockTlsProxies,
      single: null,
      filteredEntities: [],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_TLS_PROXY_LIST_WITH_VIEW_FULFILLED,
      payload: buildAxiosServerResponse(mockTlsProxies),
    })).toEqual({
      entities: mockTlsProxies,
      single: null,
      filteredEntities: [],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY_LIST_BY_FILTER', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [mockTlsProxies[0]],
      selected: [],
    }, {
      type: types.GET_TLS_PROXY_LIST_BY_FILTER,
      payload: api.getTlsProxyListByFilter('0', 'filter'),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    }, {
      type: types.GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse([mockTlsProxies[0]]),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [mockTlsProxies[0]],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    }, {
      type: types.GET_TLS_PROXY,
      payload: api.getTlsProxy('0', 'id1'),
    })).toEqual({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    });
  });

  it('should handle GET_TLS_PROXY_FULFILLED', () => {
    expect(reducer({
      entities: [],
      single: null,
      filteredEntities: [],
      selected: [],
    }, {
      type: types.GET_TLS_PROXY_FULFILLED,
      payload: buildAxiosServerResponse([mockTlsProxies[0]]),
    })).toEqual({
      entities: [],
      single: mockTlsProxies[0],
      filteredEntities: [],
      selected: [],
    });
  });
});
