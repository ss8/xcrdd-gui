import { AxiosPromise } from 'axios';
import Http from 'shared/http';
import {
  TlsProxy,
  API_PATH_TLS_PROXIES,
} from './tlsProxy.types';
import { ServerResponse, WithAuditDetails } from '../types';

const http = Http.getHttp();

export function getTlsProxyList(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<TlsProxy>> {
  return http.get(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}`);
}

export function getTlsProxyListWithView(
  deliveryFunctionId: string,
  view = 'listeners',
): AxiosPromise<ServerResponse<TlsProxy>> {
  return http.get(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}`, {
    params: {
      $view: view,
    },
  });
}

export function getTlsProxyListByFilter(
  deliveryFunctionId: string,
  filter: string,
): AxiosPromise<ServerResponse<TlsProxy>> {
  return http.get(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}`, {
    params: {
      $filter: filter,
      $view: 'listeners',
    },
  });
}

export function getTlsProxy(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<TlsProxy>> {
  return http.get(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}/${id}`);
}

export function createTlsProxy(
  deliveryFunctionId: string,
  body: WithAuditDetails<TlsProxy>,
): AxiosPromise<ServerResponse<void>> {
  return http.post(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}`, body);
}

export function updateTlsProxy(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<TlsProxy>,
): AxiosPromise<ServerResponse<void>> {
  return http.put(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}/${id}`, body);
}

export function deleteTlsProxy(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): AxiosPromise<ServerResponse<void>> {
  return http.delete(`${API_PATH_TLS_PROXIES}/${deliveryFunctionId}/${id}`, { data: body.auditDetails });
}
