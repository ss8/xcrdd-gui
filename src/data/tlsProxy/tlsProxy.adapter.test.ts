import cloneDeep from 'lodash.clonedeep';
import * as types from './tlsProxy.types';
import {
  adaptToCopy,
  setTlsProxyOperationValues,
} from './tlsProxy.adapter';

describe('tlsProxy.adapter', () => {
  let tlsProxy: types.TlsProxy;

  beforeEach(() => {
    tlsProxy = {
      id: 'id1',
      name: 'name1',
      nodeName: 'node-1',
      instanceId: '',
      listeners: [{
        id: 'listenerId1',
        ipAddress: '1.1.1.1',
        port: '1',
        requiredState: 'ACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '256',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: 'destinationId1',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'INACTIVE',
        keepaliveInterval: '60',
        numOfConnections: '5',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
      }],
    };
  });

  describe('when creating a new tls proxy', () => {
    let expectedTlsProxy: types.TlsProxy;

    it('should add the operation fields values as expected', () => {
      expectedTlsProxy = cloneDeep(tlsProxy);

      expectedTlsProxy.listeners[0].operation = 'CREATE';
      expectedTlsProxy.destinations[0].operation = 'CREATE';
      expect(setTlsProxyOperationValues(tlsProxy)).toEqual(
        expectedTlsProxy,
      );
    });
  });

  describe('when editing a tls proxy', () => {
    let originalTlsProxy: types.TlsProxy;
    let expectedTlsProxy: types.TlsProxy;

    beforeEach(() => {
      originalTlsProxy = cloneDeep(tlsProxy);
    });

    it('should add the operation fields as expected', () => {
      expectedTlsProxy = cloneDeep(tlsProxy);
      expectedTlsProxy.operation = 'UPDATE';
      expectedTlsProxy.listeners[0].operation = 'NO_OPERATION';
      expectedTlsProxy.destinations[0].operation = 'NO_OPERATION';
      expect(setTlsProxyOperationValues(tlsProxy, originalTlsProxy, 'UPDATE')).toEqual(expectedTlsProxy);
    });

    it('should add the operation fields as expected when there are changes', () => {
      originalTlsProxy.listeners[0].ipAddress = '1.1.1.2';
      originalTlsProxy.listeners[1] = {
        id: 'listenerId2',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'ACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '256',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      };

      expectedTlsProxy = cloneDeep(tlsProxy);
      expectedTlsProxy.operation = 'UPDATE';
      expectedTlsProxy.listeners[0].operation = 'UPDATE';
      expectedTlsProxy.listeners[1] = {
        ...originalTlsProxy.listeners[1],
        operation: 'DELETE',
      };
      expectedTlsProxy.destinations[0].operation = 'NO_OPERATION';

      expect(
        setTlsProxyOperationValues(tlsProxy, originalTlsProxy, 'UPDATE'),
      ).toEqual(expectedTlsProxy);
    });
  });

  describe('when copying a tls proxy', () => {
    it('should adapt the tls proxy to be copied', () => {
      const expectedTlsProxy = cloneDeep(tlsProxy);
      expectedTlsProxy.id = '';
      expectedTlsProxy.name = 'Copy of name1';
      delete expectedTlsProxy.listeners[0].id;
      expectedTlsProxy.listeners[0].port = '';
      delete expectedTlsProxy.destinations[0].id;
      expectedTlsProxy.destinations[0].port = '';

      expect(adaptToCopy(tlsProxy)).toEqual(expectedTlsProxy);
    });
  });
});
