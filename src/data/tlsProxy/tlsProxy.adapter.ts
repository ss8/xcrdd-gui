import cloneDeep from 'lodash.clonedeep';
import omitBy from 'lodash.omitby';
import isNil from 'lodash.isnil';
import isEqual from 'lodash.isequal';
import { Operation } from 'data/types';
import {
  TlsProxy,
  TlsProxyListener,
  TlsProxyDestination,
} from './tlsProxy.types';

export function setTlsProxyOperationValues(
  tlsProxy: TlsProxy,
  originalTlsProxy: TlsProxy | null = null,
  operation: Operation = 'CREATE',
): TlsProxy {
  const clonedTlsProxy = cloneDeep(tlsProxy);

  if (operation !== 'CREATE') {
    clonedTlsProxy.operation = operation;
  }

  setInterfacesOperation(clonedTlsProxy, originalTlsProxy);

  return clonedTlsProxy;
}

export function adaptToCopy(tlsProxyForm: TlsProxy): TlsProxy {
  const clonedTlsProxy = cloneDeep(tlsProxyForm);
  clonedTlsProxy.name = `Copy of ${clonedTlsProxy.name}`;
  clonedTlsProxy.id = '';

  const tlsProxyToCopy: TlsProxy = {
    ...clonedTlsProxy,
    listeners: adaptInterfaceToCopy<TlsProxyListener>(clonedTlsProxy.listeners),
    destinations: adaptInterfaceToCopy<TlsProxyDestination>(clonedTlsProxy.destinations),
  };

  return omitBy<TlsProxy>(tlsProxyToCopy, isNil) as TlsProxy;
}

function setInterfacesOperation(
  tlsProxy: TlsProxy,
  originalTlsProxy: TlsProxy | null = null,
): void {
  setInterfaceOperationValueBasedInOriginal<TlsProxyListener>(
    tlsProxy.listeners,
    originalTlsProxy?.listeners,
  );
  setInterfaceOperationValueBasedInOriginal<TlsProxyDestination>(
    tlsProxy.destinations,
    originalTlsProxy?.destinations,
  );
}

function setInterfaceOperationValueBasedInOriginal<T extends { id?: string, operation?: Operation }>(
  interfaces: T[] | undefined,
  originalInterfaces: T[] | undefined,
) {
  // Check which interfaces will be marked as CREATE, UPDATE or NO_OPERATION
  interfaces?.forEach((interfaceEntry) => {
    const originalInterfaceEntry = originalInterfaces?.find(({ id }) => id === interfaceEntry.id);
    if (originalInterfaceEntry == null) {
      interfaceEntry.operation = 'CREATE';
    } else if (!isEqual(interfaceEntry, originalInterfaceEntry)) {
      interfaceEntry.operation = 'UPDATE';
    } else {
      interfaceEntry.operation = 'NO_OPERATION';
    }
  });

  // Check which interfaces will be marked as DELETE
  const interfacesToDelete =
    originalInterfaces?.filter((originalInterfaceEntry) => {
      return !interfaces?.find(({ id }) => id === originalInterfaceEntry.id);
    })
      .map((originalInterfaceEntry) => ({
        ...originalInterfaceEntry,
        operation: 'DELETE',
      }));

  if (interfacesToDelete) {
    interfaces?.push(...(interfacesToDelete as T[]));
  }
}

function adaptInterfaceToCopy<T extends { id?: string, ipAddress: string, port: string }>(
  tlsProxyInterfaceFormData: T[],
): T[] {
  return tlsProxyInterfaceFormData.map(({
    id,
    port,
    ...rest
  }) => ({
    ...(rest as T),
    port: '',
  }));
}
