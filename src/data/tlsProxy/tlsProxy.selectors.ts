import { RootState } from '../root.reducers';
import { TlsProxy } from './tlsProxy.types';

export const getTlsProxyList = (state: RootState): TlsProxy[] => state.tlsProxy.entities;
export const getFilteredTlsProxyList = (state: RootState): TlsProxy[] =>
  state.tlsProxy.filteredEntities;
export const getTlsProxy = (state: RootState): TlsProxy | null => state.tlsProxy.single;
export const getSelectedTlsProxy = (state: RootState): (TlsProxy[]) => state.tlsProxy.selected;
