import { tlsProxyTemplate } from 'routes/TlsProxy/templates';
import { createTlsProxyBasedOnTemplate } from './tlsProxy.factory';
import { TlsProxy } from './tlsProxy.types';

describe('tlsProxy.factory', () => {
  describe('createTlsProxyBasedOnTemplate', () => {
    it('should return a TlsProxy instance based in the template', () => {
      const expected: TlsProxy = {
        name: '',
        nodeName: '',
        instanceId: '',
        listeners: [{
          id: '',
          ipAddress: '',
          port: '',
          requiredState: 'ACTIVE',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          routeRule: 'SRCADDR',
        }],
        destinations: [{
          id: '',
          ipAddress: '',
          port: '',
          requiredState: 'ACTIVE',
          keepaliveInterval: '60',
          numOfConnections: '1',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          keepaliveRetries: '6',
        }],
      };

      expect(createTlsProxyBasedOnTemplate(tlsProxyTemplate)).toEqual(expected);
    });
  });
});
