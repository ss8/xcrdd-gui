import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import { WithAuditDetails } from 'data/types';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import * as api from './tlsProxy.api';
import {
  TlsProxy,
  API_PATH_TLS_PROXIES,
} from './tlsProxy.types';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('tlsProxy.api', () => {
  let tlsProxy: TlsProxy;
  let tlsProxyWithAudit: WithAuditDetails<TlsProxy>;
  let auditOnly: WithAuditDetails<void>;

  beforeEach(() => {
    [tlsProxy] = mockTlsProxies;

    tlsProxyWithAudit = {
      data: tlsProxy,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.SECURITY_GATEWAY,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.SECURITY_GATEWAY_ADD,
      },
    };
  });

  it('should request the list for TLS proxy', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onGet(`${API_PATH_TLS_PROXIES}/0`).reply(200, response);
    await expect(api.getTlsProxyList('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0` },
      status: 200,
    });
  });

  it('should request the list for TLS proxy with view', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onGet(`${API_PATH_TLS_PROXIES}/0`).reply(200, response);
    await expect(api.getTlsProxyListWithView('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0` },
      status: 200,
    });
  });

  it('should request the list for TLS proxy by filter', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onGet(`${API_PATH_TLS_PROXIES}/0?$filter=filter1&$view=listeners`).reply(200, response);
    await expect(api.getTlsProxyListByFilter('0', 'filter1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0` },
      status: 200,
    });
  });

  it('should request a TLS proxy', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onGet(`${API_PATH_TLS_PROXIES}/0/id1`).reply(200, response);
    await expect(api.getTlsProxy('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0/id1` },
      status: 200,
    });
  });

  it('should request to create a TLS proxy', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onPost(`${API_PATH_TLS_PROXIES}/0`).reply(200, response);
    await expect(api.createTlsProxy('0', tlsProxyWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0` },
      status: 200,
    });
  });

  it('should request to update a TLS proxy', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onPut(`${API_PATH_TLS_PROXIES}/0/id1`).reply(200, response);
    await expect(api.updateTlsProxy('0', 'id1', tlsProxyWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0/id1` },
      status: 200,
    });
  });

  it('should request to delete a TLS proxy', async () => {
    const response = buildAxiosServerResponse([tlsProxy]);
    axiosMock.onDelete(`${API_PATH_TLS_PROXIES}/0/id1`).reply(200, response);
    await expect(api.deleteTlsProxy('0', 'id1', auditOnly)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_TLS_PROXIES}/0/id1` },
      status: 200,
    });
  });
});
