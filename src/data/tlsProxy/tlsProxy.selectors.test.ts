import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import {
  getFilteredTlsProxyList,
  getSelectedTlsProxy,
  getTlsProxy,
  getTlsProxyList,
} from './tlsProxy.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './tlsProxy.types';

describe('tsProxy.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of TLS Proxies', () => {
    const action: types.TlsProxyActionTypes = {
      type: types.GET_TLS_PROXY_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockTlsProxies),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getTlsProxyList(store.getState())).toEqual(mockTlsProxies);
  });

  it('should return the list of filtered TLS Proxies', () => {
    const action: types.TlsProxyActionTypes = {
      type: types.GET_TLS_PROXY_LIST_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(mockTlsProxies),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getFilteredTlsProxyList(store.getState())).toEqual(mockTlsProxies);
  });

  it('should return the single TLS Proxy', () => {
    const action: types.TlsProxyActionTypes = {
      type: types.GET_TLS_PROXY_FULFILLED,
      payload: buildAxiosServerResponse([mockTlsProxies[0]]),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getTlsProxy(store.getState())).toEqual(mockTlsProxies[0]);
  });

  it('should return the selected TLS Proxy', () => {
    const action: types.TlsProxyActionTypes = {
      type: types.SELECT_TLS_PROXY,
      payload: mockTlsProxies,
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSelectedTlsProxy(store.getState())).toEqual(mockTlsProxies);
  });
});
