import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';
import { TlsProxy, TlsProxyDestination, TlsProxyListener } from './tlsProxy.types';

// eslint-disable-next-line import/prefer-default-export
export function createTlsProxyBasedOnTemplate(
  template: FormTemplate,
): TlsProxy {
  const tlsProxy: Partial<TlsProxy> = {
    ...getFieldValuesFromSection(template.main as FormTemplateSection),
    listeners: getInterfaceFieldValuesFromSection(
      template.listeners as FormTemplateSection,
    ) as TlsProxyListener[],
    destinations: getInterfaceFieldValuesFromSection(
      template.destinations as FormTemplateSection,
    ) as TlsProxyDestination[],
  };

  return omitBy(tlsProxy, isNil) as unknown as TlsProxy;
}

function getFieldValuesFromSection(
  templateSection: FormTemplateSection,
): Partial<TlsProxy> {
  return Object.entries(templateSection?.fields)
    .filter(([_fieldName, field]) => field.initial != null)
    .reduce((previousValue, [fieldName, field]) => ({
      ...previousValue,
      [fieldName]: field.initial,
    }), {});
}

function getInterfaceFieldValuesFromSection(
  templateSection: FormTemplateSection,
): (TlsProxyListener | TlsProxyDestination)[] {
  const fields: Partial<TlsProxyListener | TlsProxyDestination> = {
    ...getFieldValuesFromSection(templateSection),
  };

  return [fields as TlsProxyListener | TlsProxyDestination];
}
