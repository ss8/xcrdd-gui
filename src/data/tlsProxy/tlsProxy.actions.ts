import { WithAuditDetails } from 'data/types';
import { History } from 'history';
import { Dispatch, Action } from 'redux';
import { RootState } from 'data/root.reducers';
import { showSuccessMessage, showError } from 'shared/toaster';
import { ThunkDispatch } from 'redux-thunk';
import * as api from './tlsProxy.api';
import { extractErrorMessage } from '../../utils';
import {
  GO_TO_TLS_PROXY_LIST,
  GO_TO_TLS_PROXY_CREATE,
  GO_TO_TLS_PROXY_EDIT,
  GO_TO_TLS_PROXY_COPY,
  GO_TO_TLS_PROXY_VIEW,
  GET_TLS_PROXY_LIST,
  GET_TLS_PROXY_LIST_BY_FILTER,
  GET_TLS_PROXY_LIST_WITH_VIEW,
  GET_TLS_PROXY,
  CREATE_TLS_PROXY,
  UPDATE_TLS_PROXY,
  DELETE_TLS_PROXY,
  SELECT_TLS_PROXY,
  TlsProxy,
  GetTlsProxyListAction,
  GetTlsProxyAction,
  DeleteTlsProxyThunkAction,
  GetTlsProxyListByFilterAction,
  SelectTlsProxyAction,
  GetTlsProxyListWithViewAction,
  CreateTlsProxyThunkAction,
  UpdateTlsProxyThunkAction,
} from './tlsProxy.types';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../constants';

export function goToTlsProxyList(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_TLS_PROXY_LIST,
    });

    history.push(`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`);
  };
}

export function goToTlsProxyCreate(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_TLS_PROXY_CREATE,
    });

    history.push(`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/create`);
  };
}

export function goToTlsProxyEdit(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_TLS_PROXY_EDIT,
    });

    history.push(`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/edit`);
  };
}

export function goToTlsProxyCopy(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_TLS_PROXY_COPY,
    });

    history.push(`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/copy`);
  };
}

export function goToTlsProxyView(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_TLS_PROXY_VIEW,
    });

    history.push(`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/view`);
  };
}

export function fetchTlsProxyList(deliveryFunctionId: string): GetTlsProxyListAction {
  return {
    type: GET_TLS_PROXY_LIST,
    payload: api.getTlsProxyList(deliveryFunctionId),
  };
}
export function fetchTlsProxyListWithView(
  deliveryFunctionId: string,
  view = 'listeners',
): GetTlsProxyListWithViewAction {
  return {
    type: GET_TLS_PROXY_LIST_WITH_VIEW,
    payload: api.getTlsProxyListWithView(deliveryFunctionId, view),
  };
}

export function fetchTlsProxyListByFilter(
  deliveryFunctionId: string,
  filter: string,
): GetTlsProxyListByFilterAction {
  return {
    type: GET_TLS_PROXY_LIST_BY_FILTER,
    payload: api.getTlsProxyListByFilter(deliveryFunctionId, filter),
  };
}

export function fetchTlsProxy(deliveryFunctionId: string, id: string): GetTlsProxyAction {
  return {
    type: GET_TLS_PROXY,
    payload: api.getTlsProxy(deliveryFunctionId, id),
  };
}

export function createTlsProxy(
  deliveryFunctionId: string,
  body: WithAuditDetails<TlsProxy>,
  history: History,
): CreateTlsProxyThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: CREATE_TLS_PROXY,
        payload: api.createTlsProxy(deliveryFunctionId, body),
      });

      dispatch(goToTlsProxyList(history));
      showSuccessMessage(`${body.data.name} successfully created.`);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create ${body.data.name}: ${errorMessage}.`);
    }
  };
}

export function updateTlsProxy(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<TlsProxy>,
  history: History,
): UpdateTlsProxyThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: UPDATE_TLS_PROXY,
        payload: api.updateTlsProxy(deliveryFunctionId, id, body),
      });

      dispatch(goToTlsProxyList(history));
      showSuccessMessage(`${body.data.name} successfully updated.`);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update ${body.data.name}: ${errorMessage}.`);
    }
  };
}

export function deleteTlsProxy(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): DeleteTlsProxyThunkAction {
  return async (dispatch: Dispatch, getState: () => RootState) => {
    const selectedTlsProxy = getState()?.tlsProxy?.selected[0];

    try {
      await dispatch({
        type: DELETE_TLS_PROXY,
        payload: api.deleteTlsProxy(deliveryFunctionId, id, body),
      });

      showSuccessMessage(`${selectedTlsProxy?.name} successfully deleted.`);

    } catch (e) {
      showError(`Unable to delete ${selectedTlsProxy?.name}.`);

    } finally {
      dispatch(fetchTlsProxyListWithView(deliveryFunctionId));
    }
  };
}

export function selectTlsProxy(
  tlsProxy: TlsProxy[],
): SelectTlsProxyAction {
  return {
    type: SELECT_TLS_PROXY,
    payload: tlsProxy,
  };
}
