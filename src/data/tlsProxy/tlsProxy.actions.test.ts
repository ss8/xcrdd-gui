import thunk from 'redux-thunk';
import { History } from 'history';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import { WithAuditDetails } from 'data/types';
import Http from 'shared/http';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import * as actions from './tlsProxy.actions';
import * as types from './tlsProxy.types';
import * as api from './tlsProxy.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const mockStore = configureMockStore([thunk]);

describe('tlsProxy.actions', () => {
  let tlsProxy: types.TlsProxy;
  let tlsProxyWithAudit: WithAuditDetails<types.TlsProxy>;
  let history: History;
  let auditOnly: WithAuditDetails<void>;
  let store: MockStoreEnhanced<unknown>;

  beforeEach(() => {
    [tlsProxy] = mockTlsProxies;

    tlsProxyWithAudit = {
      data: tlsProxy,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.SECURITY_GATEWAY,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.SECURITY_GATEWAY_ADD,
      },
    };

    history = {
      location: {
        pathname: '',
        hash: '',
        search: '',
        state: '',
      },
      length: 1,
      action: 'PUSH',
      listen: jest.fn(),
      go: jest.fn(),
      push: jest.fn(),
      block: jest.fn(),
      replace: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      createHref: jest.fn(),
    };

    store = mockStore({
      tlsProxy: {
        entities: [],
        filteredEntities: [],
        selected: [],
      },
    });

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to go to the tls proxy list', () => {
    actions.goToTlsProxyList(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/systemSettings/securityGateway');
  });

  it('should create an action to go to the tls proxy create', () => {
    actions.goToTlsProxyCreate(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/systemSettings/securityGateway/create');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_TLS_PROXY_CREATE,
    }]);
  });

  it('should create an action to go to the tls proxy edit', () => {
    actions.goToTlsProxyEdit(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/systemSettings/securityGateway/edit');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_TLS_PROXY_EDIT,
    }]);
  });

  it('should create an action to go to the tls proxy view', () => {
    actions.goToTlsProxyView(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/systemSettings/securityGateway/view');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_TLS_PROXY_VIEW,
    }]);
  });

  it('should create an action to go to the tls proxy copy', () => {
    actions.goToTlsProxyCopy(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/systemSettings/securityGateway/copy');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_TLS_PROXY_COPY,
    }]);
  });

  it('should create an action to get the tls proxies', () => {
    const expectedAction = {
      type: types.GET_TLS_PROXY_LIST,
      payload: api.getTlsProxyList('0'),
    };

    expect(actions.fetchTlsProxyList('0')).toEqual(expectedAction);
  });

  it('should create an action to get the tls proxies with view', () => {
    const expectedAction = {
      type: types.GET_TLS_PROXY_LIST_WITH_VIEW,
      payload: api.getTlsProxyListWithView('0', 'view'),
    };

    expect(actions.fetchTlsProxyListWithView('0', 'view')).toEqual(expectedAction);
  });

  it('should create an action to get the tls proxies by filter', () => {
    const expectedAction = {
      type: types.GET_TLS_PROXY_LIST_BY_FILTER,
      payload: api.getTlsProxyListByFilter('0', 'filter'),
    };

    expect(actions.fetchTlsProxyListByFilter('0', 'filter')).toEqual(expectedAction);
  });

  it('should create an action to get the tls proxy', () => {
    const expectedAction = {
      type: types.GET_TLS_PROXY,
      payload: api.getTlsProxy('0', 'id1'),
    };

    expect(actions.fetchTlsProxy('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to create the tls proxy', () => {
    const expectedAction = [{
      type: types.CREATE_TLS_PROXY,
      payload: api.createTlsProxy('0', tlsProxyWithAudit),
    }, {
      type: types.GO_TO_TLS_PROXY_LIST,
    }];

    expect.assertions(1);
    return store.dispatch(actions.createTlsProxy('0', tlsProxyWithAudit, history) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to update the tls proxy', () => {
    const expectedAction = [{
      type: types.UPDATE_TLS_PROXY,
      payload: api.updateTlsProxy('0', 'id1', tlsProxyWithAudit),
    }, {
      type: types.GO_TO_TLS_PROXY_LIST,
    }];

    expect.assertions(1);
    return store.dispatch(actions.updateTlsProxy('0', 'id1', tlsProxyWithAudit, history) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to delete the tls proxy', () => {
    const expectedAction = [
      {
        type: types.DELETE_TLS_PROXY,
        payload: api.deleteTlsProxy('0', 'id1', auditOnly),
      },
      {
        type: types.GET_TLS_PROXY_LIST_WITH_VIEW,
        payload: api.getTlsProxyListWithView('0'),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.deleteTlsProxy('0', 'id1', auditOnly) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });
});
