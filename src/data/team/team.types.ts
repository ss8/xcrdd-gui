export type Member = {
    id?: number,
    belongsTo: string,
    user: string,
    team: number,
    defaultTeam: boolean,
}

export type Team = {
    id: string,
    name: string,
    description?: string,
    dateCreated?: string,
    members?: Member[],
    reporting?: string | null,
}

export type TeamFormData = {
    id?: string,
    name: string,
    description: string,
    members: string[],
}
