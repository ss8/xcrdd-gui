export const SUPPORTED_FEATURE_5GC_VPC = 'XC-CNFVCP';

export type SupportedFeaturesAccessFunction = {
  requiredTargets?: string[]
  collectionFunctions?: string[]
  features: string[]
  autowire: boolean
  title: string
  models: string[]
  versions: string[]
  genccc: boolean
  services: AFServiceSpecs | string[]
  optionalTargets?: string[]
  x3deliveryOptions?: null | string,
  x3TargetAssociations?: SupportedFeaturesX3TargetAssociations
  xcipioAFType: string
  description: string
  supportedActions?: string[]
  httpVersions?: string[]
  httpDefaultVersion?: string
}

export type SupportedFeaturesCollectionFunction = {
  services: string[]
  versions: string[]
  features: string[]
  description: string
  title: string
  supersedes?: string[]
}

export type AFServiceSpecs = {
  [serviceKey: string]: {
    requiredTargets?: string[]
    optionalTargets?: string[]
    collectionFunctions?: string[]
  }
}

export type SupportedFeaturesX3TargetAssociations = {
  targetInfoType: string
  targets: string[]
}

export type SupportedFeaturesAccessFunctions = {
  [accessFunctionTag: string]: SupportedFeaturesAccessFunction
}

export type SupportedFeaturesCollectionFunctions = {
  [collectionFunctionTag: string]: SupportedFeaturesCollectionFunction
}

export type Ontology = {
  services: {
    [serviceKey: string]: {
      title: string
      description: string
      deployments?: string[]
    }
  },
  accessFunctions: SupportedFeaturesAccessFunctions
  collectionFunctions: SupportedFeaturesCollectionFunctions
  targets: SupportedFeaturesTargets
};

export type SupportedFeaturesTargets = {
  [targetKey: string]: SupportedFeaturesTarget
}

export type SupportedFeaturesTarget = {
  description: string
  title: string
  type: string
  pattern?: string
  maxLength?: number
  minLength?: number
  example?: string
  enum?: {
    value: string
    title: string
  }[],
  items?: {
    [itemName: string]: SupportedFeaturesTargetItem
  }
}

export type SupportedFeaturesTargetItem = {
  type: string
  title: string
  enum?: {
    value: string
    title: string
  }[],
  pattern?: string,
  example?: string,
  maxLength?: number,
  minLength?: number,
}

export type SupportedFeatures = {
  id: string
  description: string
  features: {
    customer: string
    version: number
    nodes: SupportedFeaturesNode[]
  }
  version: string
  config: Ontology
}

export type SupportedFeaturesNode = {
  ip: string
  type: string
  features: string[]
}
