import { createSelector } from 'reselect';
import { RootState } from 'data/root.reducers';
import {
  SUPPORTED_FEATURE_5GC_VPC,
  SupportedFeaturesAccessFunctions,
  SupportedFeaturesNode,
  SupportedFeaturesCollectionFunctions,
} from './supportedFeatures.types';

export const getFeatureNodes = (state: RootState): SupportedFeaturesNode[] =>
  state.warrants.config?.features.nodes ?? [];

export const getAvailableAccessFunction = (state: RootState): SupportedFeaturesAccessFunctions =>
  state.warrants.config?.config.accessFunctions ?? {};

export const getAvailableCollectionFunction = (state: RootState): SupportedFeaturesCollectionFunctions =>
  state.warrants.config?.config.collectionFunctions ?? {};

export const getEnabledFeatures = createSelector(
  [getFeatureNodes],
  (nodes) => {
    const enabledFeaturesSet: Set<string> = new Set();
    nodes.forEach((node) => {
      node.features.forEach((feature) => {
        enabledFeaturesSet.add(feature);
      });
    });
    return Array.from(enabledFeaturesSet);
  },
);

export const getEnabledAccessFunction = createSelector(
  [getAvailableAccessFunction, getEnabledFeatures],
  (availableAccessFunctions, enabledFeatures) => {
    const enabledAccessFunctions: SupportedFeaturesAccessFunctions = {};

    Object.entries(availableAccessFunctions).forEach(([accessFunctionTag, accessFunctionConfig]) => {
      if (accessFunctionConfig.features.some((feature) => enabledFeatures.includes(feature))) {
        enabledAccessFunctions[accessFunctionTag] = accessFunctionConfig;
      }
    });

    return enabledAccessFunctions;
  },
);

export const getEnabledCollectionFunctions = createSelector(
  [getAvailableCollectionFunction, getEnabledFeatures],
  (availableCollectionFunctions, enabledFeatures) => {
    const enabledCollectionFunctions: SupportedFeaturesCollectionFunctions = {};

    Object.entries(availableCollectionFunctions).forEach(([collectionFunctionTag, collectionFunctionConfig]) => {
      if (collectionFunctionConfig.features.some((feature) => enabledFeatures.includes(feature))) {
        enabledCollectionFunctions[collectionFunctionTag] = collectionFunctionConfig;
      }
    });

    return enabledCollectionFunctions;
  },
);

export const is5gcVcpEnabled = createSelector(
  [getEnabledFeatures],
  (enabledFeatures) => {
    return (enabledFeatures.includes(SUPPORTED_FEATURE_5GC_VPC));
  },
);
