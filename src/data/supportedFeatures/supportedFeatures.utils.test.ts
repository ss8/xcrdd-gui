import cloneDeep from 'lodash.clonedeep';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getDeploymentServices } from './supportedFeatures.utils';

describe('getDeploymentServices', () => {
  it('should return services that matches endeavor deployment', () => {
    expect(getDeploymentServices(mockSupportedFeatures.config, 'endeavor')).toEqual([
      'VOICE',
      'MOBILE',
      'VOICE-AND-IP-SMS',
      'DATA',
      'SMS',
      'ROAMING',
      'SUPPLEMENTARY',
      'OFFNET-VOICE',
      'VOICEMAIL',
      'MOBILE-DATA',
      'BROADBAND',
    ]);

    expect(getDeploymentServices(mockSupportedFeatures.config, 'Endeavor')).toEqual([
      'VOICE',
      'MOBILE',
      'VOICE-AND-IP-SMS',
      'DATA',
      'SMS',
      'ROAMING',
      'SUPPLEMENTARY',
      'OFFNET-VOICE',
      'VOICEMAIL',
      'MOBILE-DATA',
      'BROADBAND',
    ]);
  });

  it('should return services that matches andromeda deployment', () => {
    expect(getDeploymentServices(mockSupportedFeatures.config, 'andromeda')).toEqual([
      'PTT',
      'VOIP-VOICE',
      'LTE-IOT',
      'GSMCDMA-2G-DATA',
      'VOLTE-4G-VOICE',
      'IP',
      'VZ-VISIBLE',
      'VOLTE-RCS',
      'CDMA2000-3G-DATA',
      'EMAIL',
      'CDMA2000-3G-VOICE',
      'MCPTT',
      'LTE-4G-DATA',
      'CELLCRYPT',
      '5G-DATA',
      'LTE-4G-DATA-ROAMING',
      'UMTS-3G-VOICE',
      'GSMCDMA-2G-VOICE',
      'LTE-GIZMO',
      'ETSI-GIZMO-SERVICE',
    ]);

    expect(getDeploymentServices(mockSupportedFeatures.config, 'Andromeda')).toEqual([
      'PTT',
      'VOIP-VOICE',
      'LTE-IOT',
      'GSMCDMA-2G-DATA',
      'VOLTE-4G-VOICE',
      'IP',
      'VZ-VISIBLE',
      'VOLTE-RCS',
      'CDMA2000-3G-DATA',
      'EMAIL',
      'CDMA2000-3G-VOICE',
      'MCPTT',
      'LTE-4G-DATA',
      'CELLCRYPT',
      '5G-DATA',
      'LTE-4G-DATA-ROAMING',
      'UMTS-3G-VOICE',
      'GSMCDMA-2G-VOICE',
      'LTE-GIZMO',
      'ETSI-GIZMO-SERVICE',
    ]);
  });

  it('should return all services when deployment string is empty', () => {
    expect(getDeploymentServices(mockSupportedFeatures.config, '')).toEqual([
      'PTT',
      'VOIP-VOICE',
      'LTE-IOT',
      'GSMCDMA-2G-DATA',
      'VOLTE-4G-VOICE',
      'IP',
      'VZ-VISIBLE',
      'VOLTE-RCS',
      'CDMA2000-3G-DATA',
      'EMAIL',
      'CDMA2000-3G-VOICE',
      'MCPTT',
      'LTE-4G-DATA',
      'CELLCRYPT',
      '5G-DATA',
      'LTE-4G-DATA-ROAMING',
      'UMTS-3G-VOICE',
      'GSMCDMA-2G-VOICE',
      'LTE-GIZMO',
      'ETSI-GIZMO-SERVICE',
      'VOICE',
      'MOBILE',
      'VOICE-AND-IP-SMS',
      'DATA',
      'SMS',
      'ROAMING',
      'SUPPLEMENTARY',
      'OFFNET-VOICE',
      'VOICEMAIL',
      'MOBILE-DATA',
      'BROADBAND',
    ]);
  });

  it('should return all services when ontology does not have deployments attribute in services', () => {
    const clonedOntology = cloneDeep(mockSupportedFeatures.config);
    Object.keys(clonedOntology.services).forEach((service) => {
      clonedOntology.services[service].deployments = undefined;
    });

    expect(getDeploymentServices(clonedOntology, 'andromeda')).toEqual([
      'PTT',
      'VOIP-VOICE',
      'LTE-IOT',
      'GSMCDMA-2G-DATA',
      'VOLTE-4G-VOICE',
      'IP',
      'VZ-VISIBLE',
      'VOLTE-RCS',
      'CDMA2000-3G-DATA',
      'EMAIL',
      'CDMA2000-3G-VOICE',
      'MCPTT',
      'LTE-4G-DATA',
      'CELLCRYPT',
      '5G-DATA',
      'LTE-4G-DATA-ROAMING',
      'UMTS-3G-VOICE',
      'GSMCDMA-2G-VOICE',
      'LTE-GIZMO',
      'ETSI-GIZMO-SERVICE',
      'VOICE',
      'MOBILE',
      'VOICE-AND-IP-SMS',
      'DATA',
      'SMS',
      'ROAMING',
      'SUPPLEMENTARY',
      'OFFNET-VOICE',
      'VOICEMAIL',
      'MOBILE-DATA',
      'BROADBAND',
    ]);
  });

});
