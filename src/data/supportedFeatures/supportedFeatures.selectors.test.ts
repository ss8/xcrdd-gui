import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { AnyAction } from 'redux';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import {
  getEnabledFeatures,
  getAvailableAccessFunction,
  getEnabledAccessFunction,
  getEnabledCollectionFunctions,
  is5gcVcpEnabled,
} from './supportedFeatures.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './supportedFeatures.types';

describe('supportedFeatures.selectors', () => {
  let supportedFeatures: types.SupportedFeatures;
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    supportedFeatures = {
      id: 'XCP',
      version: '8.12.0',
      description: 'Some description text',
      features: {
        customer: 'UT',
        version: 1,
        nodes: [{
          ip: '12.12.12.12',
          type: 'HA',
          features: [
            'AF-ERK-5GAMF',
            'AF-NKSR',
            'AF-NOK-5GAMF',
            'AF-NOK-5GSMF',
            'AF-NOK-5GSMSF',
            'AF-NOK-5GUDM',
            'AF-NSN-SDM',
            'AF-ETSI-GIZMO',
            'AF-MVNR-5GSMSC',
            'XC-CNFVCP',
            'AF-ERK-UPF',
            'AF-ERK-SMF-X2DIST',
            'CF-3GPP-33108',
          ],
        }, {
          ip: '13.13.13.13',
          type: 'HA2',
          features: [
            'AF-ERK-SMF-X1',
            'AF-ERK-SGW',
            'CF-ETSI-102232-TCP',
          ],
        }],
      },
      config: {
        services: {
          'LTE-4G-DATA': {
            description: '4G LTE data',
            title: '4G PD',
            deployments: ['andromeda'],
          },
          '5G-DATA': {
            description: '5G mobile data',
            title: '5G data',
            deployments: ['andromeda'],
          },
        },
        accessFunctions: {
          ERK_UPF: {
            requiredTargets: [],
            collectionFunctions: [
              'CF-3GPP-33128',
            ],
            features: [
              'AF-ERK-UPF',
            ],
            autowire: true,
            title: 'Ericsson UPF',
            models: [],
            versions: [],
            genccc: false,
            services: [
              '5G-DATA',
            ],
            optionalTargets: [],
            x3deliveryOptions: null,
            xcipioAFType: 'ERK_UPF',
            description: 'Ericsson User Plane Function Point of Interception',
          },
          ERK_SMF: {
            features: [
              'AF-ERK-SMF-X1',
              'AF-ERK-SMF-X2DIST',
            ],
            autowire: true,
            title: 'Ericsson SMF',
            models: [],
            versions: [],
            genccc: false,
            services: {
              '5G-DATA': {
                requiredTargets: [
                  'MSISDN',
                ],
                optionalTargets: [
                  'IMEI',
                ],
                collectionFunctions: [
                  'CF-ETSI-102232',
                ],
              },
              'LTE-4G-DATA': {
                requiredTargets: [
                  'IMSI',
                ],
                collectionFunctions: [
                  'CF-3GPP-33108',
                ],
              },
            },
            x3deliveryOptions: null,
            xcipioAFType: 'ERK_SMF',
            description: 'Ericsson Session Management Function Point of Interception',
          },
          ERK_PGW: {
            requiredTargets: [],
            collectionFunctions: [
              'CF-3GPP-33108',
            ],
            features: [
              'AF-ERK-SGW',
            ],
            autowire: false,
            title: 'Ericsson SAEGW/PGW',
            models: [],
            versions: [],
            genccc: false,
            services: [
              'LTE-4G-DATA',
              'VOLTE-4G-VOICE',
            ],
            optionalTargets: [],
            x3deliveryOptions: null,
            xcipioAFType: 'ERK_PGW',
            description: 'Ericsson System Architecture Evolution (SAE) SGW/PGW Access Function',
          },
          STA_PGW: {
            requiredTargets: [],
            collectionFunctions: [
              'CF-TIA-1066',
              'CF-ATIS-0700005',
              'CF-3GPP-33108',
            ],
            features: [
              'AF-STARENT-PGW',
            ],
            autowire: false,
            title: 'Cisco/Starent PGW',
            models: [],
            versions: [],
            genccc: false,
            services: [
              'LTE-4G-DATA',
            ],
            optionalTargets: [],
            x3deliveryOptions: null,
            xcipioAFType: 'STA_PGW',
            description: 'Cisco/Starent Packet Data Network Gateway (PGW) Access Function',
          },
        },
        collectionFunctions: {
          'CF-3GPP-33108': {
            services: [
              'VOLTE-RCS',
              'VOLTE-4G-VOICE',
              'GSMCDMA-2G-VOICE',
              'GSMCDMA-2G-DATA',
              'LTE-4G-DATA',
              'LTE-4G-DATA-ROAMING',
              'LTE-GIZMO',
              'VZ-VISIBLE',
              'MCPTT',
              'LTE-IOT',
            ],
            versions: [
              '11.1',
              '12.7',
              '12.8',
              '12.12',
              '15.4',
              '15.6',
            ],
            features: [
              'CF-3GPP-33108',
            ],
            description: '3GPP 33.108 Collection Function with TCP or FTP interfaces',
            title: 'US 3GPP LI (TS 33.108) over TCP or FTP',
          },
          'CF-ETSI-102232': {
            services: [
              'VOLTE-RCS',
              'IP',
              'VOLTE-4G-VOICE',
              '5G-DATA',
              'LTE-4G-DATA',
            ],
            versions: [
              '1.4.1',
              '2.2.1',
              '3.1.1',
              '3.3.1',
              '3.10.1',
              '3.15.1',
              '3.21.1',
            ],
            features: [
              'CF-ETSI-102232-TCP',
              'CF-ETSI-102232-FTP',
            ],
            description: 'ETSI 102 232 Collection Function',
            title: 'ETSI IP (TS 102 232)',
          },
          'CF-3GPP-33108ROSE': {
            services: [
              'GSMCDMA-2G-VOICE',
              'GSMCDMA-2G-DATA',
              'LTE-4G-DATA',
              'LTE-GIZMO',
              'VZ-VISIBLE',
              'MCPTT',
              'LTE-IOT',
            ],
            versions: [],
            features: [
              'CF-3GPP-33108',
            ],
            description: 'ETSI 133.108 Collection Function with ROSE interface',
            title: 'ETSI LI (TS 133.108) over ROSE',
          },
        },
        targets: {
        },
      },
    };

    mockStore = configureStore<RootState>([]);

    const action: AnyAction = {
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([supportedFeatures]),
    };
    store = mockStore(rootReducer(undefined, action));
  });

  it('should return the enabled features', () => {
    expect(getEnabledFeatures(store.getState())).toEqual([
      'AF-ERK-5GAMF',
      'AF-NKSR',
      'AF-NOK-5GAMF',
      'AF-NOK-5GSMF',
      'AF-NOK-5GSMSF',
      'AF-NOK-5GUDM',
      'AF-NSN-SDM',
      'AF-ETSI-GIZMO',
      'AF-MVNR-5GSMSC',
      'XC-CNFVCP',
      'AF-ERK-UPF',
      'AF-ERK-SMF-X2DIST',
      'CF-3GPP-33108',
      'AF-ERK-SMF-X1',
      'AF-ERK-SGW',
      'CF-ETSI-102232-TCP',
    ]);
  });

  it('should return the available access functions', () => {
    expect(getAvailableAccessFunction(store.getState())).toEqual({
      ERK_UPF: supportedFeatures.config.accessFunctions.ERK_UPF,
      ERK_SMF: supportedFeatures.config.accessFunctions.ERK_SMF,
      ERK_PGW: supportedFeatures.config.accessFunctions.ERK_PGW,
      STA_PGW: supportedFeatures.config.accessFunctions.STA_PGW,
    });
  });

  it('should return the enabled access functions', () => {
    expect(getEnabledAccessFunction(store.getState())).toEqual({
      ERK_UPF: supportedFeatures.config.accessFunctions.ERK_UPF,
      ERK_SMF: supportedFeatures.config.accessFunctions.ERK_SMF,
      ERK_PGW: supportedFeatures.config.accessFunctions.ERK_PGW,
    });
  });

  it('should return the enabled collection functions', () => {
    expect(getEnabledCollectionFunctions(store.getState())).toEqual({
      'CF-3GPP-33108': supportedFeatures.config.collectionFunctions['CF-3GPP-33108'],
      'CF-ETSI-102232': supportedFeatures.config.collectionFunctions['CF-ETSI-102232'],
      'CF-3GPP-33108ROSE': supportedFeatures.config.collectionFunctions['CF-3GPP-33108ROSE'],
    });
  });

  it('should return the 5gc VCP enabled true', () => {
    expect(is5gcVcpEnabled(store.getState())).toEqual(true);
  });

});
