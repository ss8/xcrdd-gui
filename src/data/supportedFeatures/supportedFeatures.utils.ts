import {
  Ontology,
  SupportedFeaturesAccessFunction,
  AFServiceSpecs,
} from 'data/supportedFeatures/supportedFeatures.types';

/**
 * @return services are services applicable to the deployment
 */
export function getDeploymentServices(
  ontology: Ontology,
  deployment: string,
): string[] {
  const { services } = ontology;

  // if no deployment specified, return all services
  if (!deployment || deployment.trim().length === 0) {
    return Object.keys(services);
  }

  // if deployment is specified, return only the services matching the deployment
  const aDeployment = deployment.trim();
  const deploymentServices: string[] = Object.keys(services).filter((service) => {
    if (!services[service].deployments) {
      return true;
    }
    // this test whether the service is part of the current deployment using case insensitive includes
    const regex = new RegExp(services[service].deployments?.join('|') ?? '', 'i');
    return regex.test(aDeployment);
  });
  return deploymentServices;
}

/**
 * @Return AF services applicable to this deployment
 */
export function getAFSupportedServices(
  afOntology: SupportedFeaturesAccessFunction,
  deploymentServices: string[],
): string[] {
  let afServices: string[];
  if (Array.isArray(afOntology.services)) {
    afServices = afOntology.services as string[];
  } else {
    const afServiceSpec = afOntology.services as AFServiceSpecs;
    afServices = Array.from(Object.keys(afServiceSpec));
  }
  return afServices.filter((afService) =>
    (deploymentServices.includes(afService)));
}

export function getAFRequiredTargets(
  afOntology: SupportedFeaturesAccessFunction,
  services: string[] | '*',
): string[] {
  let requiredTargets: string[] = [];

  if (afOntology.requiredTargets !== undefined) {
    requiredTargets = afOntology.requiredTargets;
  } else if (Array.isArray(afOntology.services) === false) {
    Object.entries(afOntology.services).forEach(([aService, afServiceSpec]) => {
      if (services === '*' || services.includes(aService)) {
        if (requiredTargets) {
          requiredTargets = requiredTargets.concat(afServiceSpec.requiredTargets);
        } else {
          requiredTargets = afServiceSpec.requiredTargets;
        }
      }
    });
  }
  return requiredTargets;
}

export function getAFOptionalTargets(
  afOntology: SupportedFeaturesAccessFunction,
  services: string[] | '*',
): string[] {
  let optionalTargets: string[] = [];

  if (afOntology.optionalTargets !== undefined) {
    optionalTargets = afOntology.optionalTargets;
  } else if (Array.isArray(afOntology.services) === false) {
    Object.entries(afOntology.services).forEach(([aService, afServiceSpec]) => {
      if (services === '*' || services.includes(aService)) {
        if (optionalTargets) {
          optionalTargets = optionalTargets.concat(afServiceSpec.optionalTargets);
        } else {
          optionalTargets = afServiceSpec.optionalTargets;
        }
      }
    });
  }
  return optionalTargets;
}
