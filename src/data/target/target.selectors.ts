import { RootState } from 'data/root.reducers';
import { TargetModel, TargetModelMap } from './target.types';

// eslint-disable-next-line import/prefer-default-export
export const getTargets = (state: RootState): TargetModel[] => state.targets.targets;
export const getTargetsById = (state: RootState): TargetModelMap => state.targets.byId;
