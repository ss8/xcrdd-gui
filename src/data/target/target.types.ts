import { Identity } from '../types';

export interface TargetState {
  targets: TargetModel[],
  target: TargetModel | null,
  byId: TargetModelMap
}

export type TargetModelWrapper = {
  display: boolean
  ids: string[]
  synthesized?: boolean
  target: TargetModel
}

export type TargetModelArrayMap = {
  [targetId: string]: TargetModel[]
}

export type TargetModelMap = {
  [targetId: string]: TargetModel
}

export type TargetModel = {
  id: string | null
  name?: string
  caseId?: Identity
  dxAccess?: string
  targetInfoHash?: {
    afType: string
    options: unknown[]
  }[]
  dxOwner?: string
  afs?: TargetModelAccessFunctions
  isRequired?: boolean
  options?: unknown[]
  primaryType?: string
  primaryValue?: string
  secondaryType?: string
  secondaryValue?: string
  synthesized?: boolean
}

export type TargetModelAccessFunctions = {
  afIds: {
    bAFAssociation?: boolean
    x3TargetAssociations?: unknown
    contentInterfaces?: unknown
    id?: string
    model?: string
    name: string
    tag?: string
    type?: string
  }[]
  afGroups: unknown[]
  afAutowireTypes: {
    name?: string
    id?: string
    tag?: string
  }[]
  afAutowireGroups: unknown[]
  afccc: {
    afType: string
    afSettings: {
      afids: string[]
      autowired: boolean
      options: {
        key: string
        value: string
      }[]
    }[]
  }[]
}
