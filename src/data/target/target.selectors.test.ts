import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import rootReducer, { RootState } from 'data/root.reducers';
import * as targetActions from 'xcipio/targets/targets-actions';
import { mockTargets } from '__test__/mockTargets';
import {
  getTargets,
  getTargetsById,
} from './target.selectors';

describe('target.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    const targetAction: AnyAction = {
      type: targetActions.GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED,
      payload: buildAxiosServerResponse(mockTargets),
    };

    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, targetAction));
  });

  it('should return the targets', () => {
    expect(getTargets(store.getState())).toEqual(mockTargets);
  });

  it('should return the targets by id', () => {
    expect(getTargetsById(store.getState())).toEqual({
      [mockTargets[0].id as string]: mockTargets[0],
    });
  });
});
