import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import { WithAuditDetails } from 'data/types';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import { AuditActionType, AuditService } from 'shared/userAudit/audit-constants';
import * as api from './accessFunction.api';

import {
  AccessFunction,
  AccessFunctionGroup,
  AccessFunctionDataInterface,
  AccessFunctionDataActionDetails,
} from './accessFunction.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('accessFunction.api', () => {
  let accessFunctionEntry: AccessFunction;
  let accessFunction: AccessFunction;
  let accessFunctionWithAudit: WithAuditDetails<AccessFunction>;
  let accessFunctionGroup: AccessFunctionGroup;
  let accessFunctionDataInterface: AccessFunctionDataInterface;
  let auditOnly: WithAuditDetails<void>;

  beforeEach(() => {
    accessFunctionEntry = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
    };

    accessFunction = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      options: [],
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    };

    accessFunctionGroup = {
      id: 'id2',
      dxOwner: 'dxOwner2',
      dxAccess: 'dxAccess2',
      name: 'name2',
      afIds: [{
        id: 'id1',
        name: 'name1',
      }],
    };

    accessFunctionDataInterface = {
      id: 'id1',
      name: 'name1',
      options: [{
        key: 'key1',
        value: 'value1',
      }],
    };

    accessFunctionWithAudit = {
      data: accessFunction,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: JSON.stringify([{
          field: 'field1',
          value: 'value1',
          before: 'before1',
          after: 'after',
        }]),
        recordName: 'recordName1',
        service: AuditService.AF,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.ACCESS_FUNCTION_ADD,
      },
    };
  });

  it('should request the list for access functions', async () => {
    const response = buildAxiosServerResponse([accessFunctionEntry]);
    axiosMock.onGet('/afs/0').reply(200, response);
    await expect(api.getAccessFunctions('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0' },
      status: 200,
    });
  });

  it('should request the list of access functions filtered', async () => {
    const response = buildAxiosServerResponse([accessFunctionEntry]);
    axiosMock.onGet('/afs/0?$filter=filter1').reply(200, response);
    await expect(api.getAccessFunctionsByFilter('0', 'filter1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0?$filter=filter1' },
      status: 200,
    });
  });

  it('should request the list for access functions with view options', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onGet('/afs/0?$view=options').reply(200, response);
    await expect(api.getAccessFunctionsWithViewOptions('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0?$view=options' },
      status: 200,
    });
  });

  it('should get the list of access function groups', async () => {
    const response = buildAxiosServerResponse([accessFunctionGroup]);
    axiosMock.onGet('/af-groups/0').reply(200, response);
    await expect(api.getAccessFunctionGroups('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/af-groups/0' },
      status: 200,
    });
  });

  it('should request a access function', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onGet('/afs/0/id1').reply(200, response);
    await expect(api.getAccessFunction('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1' },
      status: 200,
    });
  });

  it('should request a access function with view options', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onGet('/afs/0/id1').reply(200, response);
    await expect(api.getAccessFunctionWithViewOptions('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1' },
      status: 200,
    });
  });

  it('should request to create a access function', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onPost('/afs/0').reply(200, response);
    await expect(api.createAccessFunction('0', accessFunctionWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0' },
      status: 200,
    });
  });

  it('should request to update a access function', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onPut('/afs/0/id1').reply(200, response);
    await expect(api.updateAccessFunction('0', 'id1', accessFunctionWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1' },
      status: 200,
    });
  });

  it('should request to delete a access function', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onDelete('/afs/0/id1').reply(200, response);
    await expect(api.deleteAccessFunction('0', 'id1', auditOnly)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1' },
      status: 200,
    });
  });

  it('should request the access function data interfaces', async () => {
    const response = buildAxiosServerResponse([accessFunctionDataInterface]);
    axiosMock.onGet('/afs/0/id1/data-interface?$view=options').reply(200, response);
    await expect(api.getAccessFunctionDataInterfaces('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1/data-interface?$view=options' },
      status: 200,
    });
  });

  it('should request to trigger an action in the access function', async () => {
    const response = buildAxiosServerResponse([accessFunction]);
    axiosMock.onPut('/afs/0/id1/action').reply(200, response);
    const body: WithAuditDetails<AccessFunctionDataActionDetails> = { data: { action: 'AUDIT' } };

    await expect(api.triggerActionAccessFunction('0', 'id1', body)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/id1/action' },
      status: 200,
    });
  });

  it('should get the list of configured access functions', async () => {
    const response = buildAxiosServerResponse(configuredAfs);
    axiosMock.onGet('/afs/0/configuredAfs').reply(200, response);
    await expect(api.getConfiguredAccessFunctions('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/afs/0/configuredAfs' },
      status: 200,
    });
  });
});
