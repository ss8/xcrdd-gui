import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import reducer from './accessFunction.reducer';
import * as types from './accessFunction.types';
import * as api from './accessFunction.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('accessFunction.reducer', () => {
  let accessFunctions: types.AccessFunction[];
  let accessFunctionsWithViewOptions: types.AccessFunction[];
  let accessFunctionGroups: types.AccessFunctionGroup[];

  beforeEach(() => {
    accessFunctions = [{
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
    }];

    accessFunctionsWithViewOptions = [{
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      options: [],
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    }];

    accessFunctionGroups = [{
      id: 'id2',
      dxOwner: 'dxOwner2',
      dxAccess: 'dxAccess2',
      name: 'name2',
      afIds: [{
        id: 'id1',
        name: 'name1',
      }],
    }];

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should handle GET_ALL_AFS', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: accessFunctions, isDeleting: false, single: null, configuredAfs: [],
    }, {
      type: types.GET_ALL_AFS,
      payload: api.getAccessFunctions('0'),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_ALL_AFS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_ALL_AFS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    })).toEqual({
      afs: accessFunctions,
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_ALL_AFS_BY_FILTER_FULFILLED', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: null, configuredAfs: [],
    }, {
      type: types.GET_ALL_AFS_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    })).toEqual({
      afs: accessFunctions,
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: null, configuredAfs: [],
    }, {
      type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctionsWithViewOptions),
    })).toEqual({
      afs: accessFunctionsWithViewOptions,
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_ALL_AFGROUPS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_ALL_AFGROUPS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctionGroups),
    })).toEqual({
      afs: [],
      afGroups: accessFunctionGroups,
      single: null,
      isDeleting: false,
      selected: [],
      configuredAfs: [],
    });
  });

  it('should handle GET_ACCESS_FUNCTION', () => {
    expect(reducer(undefined, {
      type: types.GET_ACCESS_FUNCTION,
      payload: api.getAccessFunction('0', 'id1'),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      isDeleting: false,
      selected: [],
      configuredAfs: [],
    });
  });

  it('should handle GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: accessFunctions[0], configuredAfs: [],
    }, {
      type: types.GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS,
      payload: api.getAccessFunctionWithViewOptions('0', 'id1'),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      isDeleting: false,
      selected: [],
      configuredAfs: [],
    });
  });

  it('should handle GET_ACCESS_FUNCTION_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: accessFunctions[0],
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctionsWithViewOptions),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: accessFunctionsWithViewOptions[0],
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle SELECT_ACCESS_FUNCTION', () => {
    expect(reducer(undefined, {
      type: types.SELECT_ACCESS_FUNCTION,
      payload: accessFunctions,
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: accessFunctions,
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle DELETE_ACCESS_FUNCTION', () => {
    expect(reducer(undefined, {
      type: types.DELETE_ACCESS_FUNCTION,
      payload: api.deleteAccessFunction('0', 'id1', { data: undefined }),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: true,
      configuredAfs: [],
    });
  });

  it('should handle DELETE_ACCESS_FUNCTION_FULFILLED', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: true, single: null, configuredAfs: [],
    }, {
      type: types.DELETE_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse(),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle DELETE_ACCESS_FUNCTION_REJECTED', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: true, single: null, configuredAfs: [],
    }, {
      type: types.DELETE_ACCESS_FUNCTION_REJECTED,
      payload: buildAxiosServerResponse(),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GO_TO_ACCESS_FUNCTION_CREATE', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: accessFunctions[0], configuredAfs: [],
    }, {
      type: types.GO_TO_ACCESS_FUNCTION_CREATE,
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GO_TO_ACCESS_FUNCTION_EDIT', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: accessFunctions[0], configuredAfs: [],
    }, {
      type: types.GO_TO_ACCESS_FUNCTION_EDIT,
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GO_TO_ACCESS_FUNCTION_VIEW', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: accessFunctions[0], configuredAfs: [],
    }, {
      type: types.GO_TO_ACCESS_FUNCTION_VIEW,
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GO_TO_ACCESS_FUNCTION_COPY', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: accessFunctions[0], configuredAfs: [],
    }, {
      type: types.GO_TO_ACCESS_FUNCTION_COPY,
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_CONFIGURED_ACCESS_FUNCTION', () => {
    expect(reducer({
      afs: [], afGroups: [], selected: [], isDeleting: false, single: null, configuredAfs,
    }, {
      type: types.GET_CONFIGURED_ACCESS_FUNCTION,
      payload: api.getConfiguredAccessFunctions('0'),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs: [],
    });
  });

  it('should handle GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([JSON.stringify(configuredAfs)]),
    })).toEqual({
      afs: [],
      afGroups: [],
      single: null,
      selected: [],
      isDeleting: false,
      configuredAfs,
    });
  });
});
