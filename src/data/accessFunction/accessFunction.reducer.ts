import {
  GET_ACCESS_FUNCTION,
  GET_ALL_AFS_FULFILLED,
  GET_ALL_AFS_BY_FILTER_FULFILLED,
  GET_ALL_AFGROUPS_FULFILLED,
  AccessFunctionActionTypes,
  AccessFunctionState,
  GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
  GET_ACCESS_FUNCTION_FULFILLED,
  SELECT_ACCESS_FUNCTION,
  DELETE_ACCESS_FUNCTION_REJECTED,
  DELETE_ACCESS_FUNCTION_FULFILLED,
  DELETE_ACCESS_FUNCTION,
  GET_ALL_AFS,
  GO_TO_ACCESS_FUNCTION_CREATE,
  GO_TO_ACCESS_FUNCTION_EDIT,
  GO_TO_ACCESS_FUNCTION_COPY,
  GO_TO_ACCESS_FUNCTION_VIEW,
  GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS,
  GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED,
  GET_CONFIGURED_ACCESS_FUNCTION,
  GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED,
  ConfiguredAccessFunction,
} from './accessFunction.types';

const initialState: AccessFunctionState = {
  afs: [],
  afGroups: [],
  single: null,
  selected: [],
  isDeleting: false,
  configuredAfs: [],
};

export default function accessFunctionReducer(
  state = initialState,
  action: AccessFunctionActionTypes,
): AccessFunctionState {
  switch (action.type) {
    case GET_ALL_AFS:
      return { ...state, selected: [] };

    case GET_ALL_AFS_FULFILLED:
      return { ...state, afs: action.payload.data.data || [] };

    case GET_ALL_AFS_BY_FILTER_FULFILLED:
      return { ...state, afs: action.payload.data.data || [] };

    case GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED:
      return { ...state, afs: action.payload.data.data || [] };

    case GET_ALL_AFGROUPS_FULFILLED:
      return { ...state, afGroups: action.payload.data.data || [] };

    case GET_ACCESS_FUNCTION:
    case GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS:
      return {
        ...state,
        single: null,
      };

    case GET_ACCESS_FUNCTION_FULFILLED:
    case GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED:
      return {
        ...state,
        single: (action.payload.data.data ?? [])[0],
      };

    case SELECT_ACCESS_FUNCTION:
      return {
        ...state,
        selected: action.payload || [],
      };

    case DELETE_ACCESS_FUNCTION:
      return {
        ...state,
        isDeleting: true,
      };

    case DELETE_ACCESS_FUNCTION_FULFILLED:
    case DELETE_ACCESS_FUNCTION_REJECTED:
      return {
        ...state,
        isDeleting: false,
      };

    case GO_TO_ACCESS_FUNCTION_CREATE:
    case GO_TO_ACCESS_FUNCTION_EDIT:
    case GO_TO_ACCESS_FUNCTION_COPY:
    case GO_TO_ACCESS_FUNCTION_VIEW:
      return {
        ...state,
        single: null,
      };

    case GET_CONFIGURED_ACCESS_FUNCTION:
    case GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED:
      let configuredAccessFunctions: ConfiguredAccessFunction[] = [];
      try {
        configuredAccessFunctions = JSON.parse(action.payload.data?.data[0] ?? []) as ConfiguredAccessFunction[];

      } catch (error) {
        // TODO: Log exception
        // console.log('Error while getting configured AFs: ', error);
      }
      return {
        ...state,
        configuredAfs: configuredAccessFunctions,
      };

    default:
      return state;
  }
}
