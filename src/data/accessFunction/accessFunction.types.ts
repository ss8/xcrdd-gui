import { AxiosResponse, AxiosPromise } from 'axios';
import { AnyAction, Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { AsyncAction } from 'redux-promise-middleware';
import {
  ServerResponse,
  Contact,
  Operation,
} from 'data/types';
import { RootState } from 'data/root.reducers';
import { FieldUpdateHandlerParam } from 'shared/form';
import { VpnConfig } from '../vpnConfig/vpnConfig.types';

export const API_PATH_AFS = '/afs';
export const API_PATH_AF_GROUPS = '/af-groups';

export const AF_USER_CATEGORY_SETTINGS = 'AccessFunctionSettings';

export const GO_TO_ACCESS_FUNCTION_CREATE = 'GO_TO_ACCESS_FUNCTION_CREATE';
export const GO_TO_ACCESS_FUNCTION_EDIT = 'GO_TO_ACCESS_FUNCTION_EDIT';
export const GO_TO_ACCESS_FUNCTION_VIEW = 'GO_TO_ACCESS_FUNCTION_VIEW';
export const GO_TO_ACCESS_FUNCTION_COPY = 'GO_TO_ACCESS_FUNCTION_COPY';
export const GET_ALL_AFS = 'GET_ALL_AFS';
export const GET_ALL_AFS_FULFILLED = 'GET_ALL_AFS_FULFILLED';
export const GET_ALL_AFS_WITH_VIEW_OPTIONS = 'GET_ALL_AFS_WITH_VIEW_OPTIONS';
export const GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED = 'GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED';
export const GET_ALL_AFS_BY_FILTER = 'GET_ALL_AFS_BY_FILTER';
export const GET_ALL_AFS_BY_FILTER_FULFILLED = 'GET_ALL_AFS_BY_FILTER_FULFILLED';
export const GET_ACCESS_FUNCTION = 'GET_ACCESS_FUNCTION';
export const GET_ACCESS_FUNCTION_FULFILLED = 'GET_ACCESS_FUNCTION_FULFILLED';
export const GET_ACCESS_FUNCTION_REJECTED = 'GET_ACCESS_FUNCTION_REJECTED';
export const GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS = 'GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS';
export const GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED = 'GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED';
export const GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_REJECTED = 'GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_REJECTED';
export const CREATE_ACCESS_FUNCTION = 'CREATE_ACCESS_FUNCTION';
export const CREATE_ACCESS_FUNCTION_FULFILLED = 'CREATE_ACCESS_FUNCTION_FULFILLED';
export const CREATE_ACCESS_FUNCTION_REJECTED = 'CREATE_ACCESS_FUNCTION_REJECTED';
export const UPDATE_ACCESS_FUNCTION = 'UPDATE_ACCESS_FUNCTION';
export const UPDATE_ACCESS_FUNCTION_FULFILLED = 'UPDATE_ACCESS_FUNCTION_FULFILLED';
export const UPDATE_ACCESS_FUNCTION_REJECTED = 'UPDATE_ACCESS_FUNCTION_REJECTED';
export const DELETE_ACCESS_FUNCTION = 'DELETE_ACCESS_FUNCTION';
export const DELETE_ACCESS_FUNCTION_FULFILLED = 'DELETE_ACCESS_FUNCTION_FULFILLED';
export const DELETE_ACCESS_FUNCTION_REJECTED = 'DELETE_ACCESS_FUNCTION_REJECTED';
export const GET_ALL_AFGROUPS = 'GET_ALL_AFGROUPS';
export const GET_ALL_AFGROUPS_FULFILLED = 'GET_ALL_AFGROUPS_FULFILLED';
export const GET_AF_DI = 'GET_AF_DI';
export const GET_AF_DI_FULFILLED = 'GET_AF_DI_FULFILLED';
export const SELECT_ACCESS_FUNCTION = 'SELECT_ACCESS_FUNCTION';
export const AUDIT_ACCESS_FUNCTION = 'AUDIT_ACCESS_FUNCTION';
export const RESET_ACCESS_FUNCTION = 'RESET_ACCESS_FUNCTION';
export const REFRESH_ACCESS_FUNCTION = 'REFRESH_ACCESS_FUNCTION';
export const GET_CONFIGURED_ACCESS_FUNCTION = 'GET_CONFIGURED_ACCESS_FUNCTION';
export const GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED = 'GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED';

export const ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID = 'accessFunctionInterface';

export const ACCESS_FUNCTION_REQ_STATE_FIELD = 'reqState';
export const ACCESS_FUNCTION_STATE_FIELD = 'state';

export const ANY_ADDRESSES = ['ANY_ADDR', 'ADDR_ANY'];

export type ConfiguredAccessFunction = {
  afTag: string
  xcipioAfType: string
  model: string
}

export type AccessFunctionMap = {
  [accessFunctionKey: string]: AccessFunction[]
}

export type AfcfByServiceMap = {
  [service: string]: {
    collectionFunctions: {
      [collectionFunctionKey: string]: {
        accessFunctions: string[]
      }
    }
    accessFunctions: string[]
  }
}

export enum AccessFunctionPrivileges {
  View = 'af_view',
  Edit = 'af_write',
  Delete = 'af_delete',
}

export type AccessFunctionFieldUpdateCallback = (
  param: FieldUpdateHandlerParam,
  formSectionName: AccessFunctionFormSection,
  interfaceId?: string | number
) => void

export interface AccessFunctionInterface {
  id: string
  name?: string
  deliveryVia?: string
  options: {
    key: string
    value: string | boolean
  }[]
  operation?: Operation
}

export interface AccessFunctionGridRowData {
  id: string,
  name: string,
  tag: string,
  subType: string,
  ipAddress: string,
  port: string,
  destination:string,
  destinationToolTip: string,
  configuredState: string,
  state: string,
  tagName: string
  afid: string
  provisioningInterfaces?: AccessFunctionInterface[],
}

export interface AccessFunctionDataActionDetails { action: 'AUDIT' | 'DWNLD' | 'REBOOT' | 'SHUTDOWN' | 'RESET' | 'RELOAD' | 'NONE' }

export type ContentDeliveryVia = 'directFromAF' | 'trafficManager' | 'ss8Distributor';
export const POSSIBLE_CONTENT_DELIVERY_VIA = ['directFromAF', 'trafficManager', 'ss8Distributor'];
export type DataDeliveryVia = 'directFromAF' | 'ss8Distributor';
export const POSSIBLE_DATA_DELIVERY_VIA = ['directFromAF', 'ss8Distributor'];
export interface AccessFunction {
  id?: string
  tag: string
  dxOwner?: string
  dxAccess?: string
  name: string
  description?: string
  contact?: Contact
  insideNat?: boolean
  liInterface?: 'INI1' | 'INI2' | 'INI3' | 'ALL'
  model?: string
  preprovisioningLead?: string
  serialNumber?: string
  timeZone: string
  type: string
  version?: string
  country?: string,
  stateName?: string,
  city?: string,
  longitude?: string
  latitude?: string
  status?: 'ACTIVE' | 'INACTIVE' | 'PARTIALLY_ACTIVE'
  options?: {
    key: string,
    value: string | boolean
  }[],
  provisioningInterfaces?: AccessFunctionInterface[]
  dataInterfaces?: AccessFunctionInterface[]
  contentInterfaces?: AccessFunctionInterface[]
  vpnConfig?: VpnConfig[]
  operation?: Operation
  contentDeliveryVia?: ContentDeliveryVia
  dataDeliveryVia?: DataDeliveryVia
}

export interface AccessFunctionInterfaceFormData extends Omit<AccessFunctionInterface, 'options'> {
  destIPAddr?: string
  destPort?: string
  dscp?: string
  ownnetid?: string
  reqState?: 'ACTIVE' | 'INACTIVE'
  secinfoid?: string
  state?: string
  tpkt?: boolean
  x224headerpresent?: boolean
  ownIPPort?: string
  mscid?: string
  imei?: boolean
  imsi?: boolean
  msisdn?: boolean
  timeout?: string
  netid?: string
  transport?: string
  maxx2connsperaf?: string,
  maxx2connections?: string,
  appProtocol?: string
  version?: string
  iri_payload_encoding?: string // eslint-disable-line camelcase
  keepaliveinterval?: string
  litargetnamespace?: string
  uri?: string
  lbid?: string
  traceLevel?: string
  type?: string
  litype?: string
  cdnetid?: string
  cdport?: string
  interval?: string
  ipduid?: string
  command?: string
  dfid?: string,
  deliveryVia?: string
  role?: 'CLIENT' | 'SERVER'
  sharedsecret?: string
  ownIPAddr?: string
  ownipaddr?: string
  password?: string
  rmtpassword?: string
  userPassword?: string
  persistent?: boolean
  ftpType?: string
  userName?: string
  serverName?: string
  destPath?: string
  ifid?: string
  httpversion?: string
  processOptionSecInfo?: string
  serviceid?: string
  sshInfoId?: string
  ccnodename?: string
  mdf2NodeName?: string
  mdf3NodeName?: string
  dataDeliveryVia?: string
  contentDeliveryVia?: string
  connection?: string
}

export type AccessFunctionInterfaceCompositeFormData = {
  afInterfaceFormData: AccessFunctionInterfaceFormData[] | null;
  vpnConfigData: VpnConfig | null;
}

export type ContentInterfaceFormData = AccessFunctionInterfaceFormData
export type ProvisioningInterfaceFormData = AccessFunctionInterfaceFormData
export type DataInterfaceFormData = AccessFunctionInterfaceFormData

export type AccessFunctionFormSection = 'general' | 'systemInformation' | AccessFunctionInterfaceFormSectionType;
export type AccessFunctionInterfaceFormSectionType = 'provisioningInterfaces' | 'dataInterfaces' | 'contentInterfaces';

export interface AccessFunctionFormData extends Omit<AccessFunction, 'contact' | 'provisioningInterfaces' | 'dataInterfaces' | 'contentInterfaces'> {
  contact?: string,
  provisioningInterfaces?: AccessFunctionInterfaceFormData[]
  dataInterfaces?: AccessFunctionInterfaceFormData[]
  contentInterfaces?: AccessFunctionInterfaceFormData[]
}
export interface AccessFunctionGroup {
  readonly id: string,
  dxOwner: string
  dxAccess: string
  name: string
  afIds: {
    id: string
    name: string
  }[]
}

export type AccessFunctionDataInterface = AccessFunctionInterface;

export type AccessFunctionContentInterface = AccessFunctionInterface;

export type AccessFunctionProvisioningInterface = AccessFunctionInterface;

export interface AccessFunctionState {
  afs: AccessFunction[]
  afGroups: AccessFunctionGroup[]
  single: AccessFunction | null
  selected: AccessFunction[],
  isDeleting: boolean,
  configuredAfs: ConfiguredAccessFunction[],
}

export interface GoToAccessFunctionCreateAction extends AnyAction {
  type: typeof GO_TO_ACCESS_FUNCTION_CREATE
}

export interface GoToAccessFunctionEditAction extends AnyAction {
  type: typeof GO_TO_ACCESS_FUNCTION_EDIT
}

export interface GoToAccessFunctionViewAction extends AnyAction {
  type: typeof GO_TO_ACCESS_FUNCTION_VIEW
}

export interface GoToAccessFunctionCopyAction extends AnyAction {
  type: typeof GO_TO_ACCESS_FUNCTION_COPY
}

export interface GetAccessFunctionsAction extends AsyncAction {
  type: typeof GET_ALL_AFS
  payload: AxiosPromise<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionsFulfilledAction extends AnyAction {
  type: typeof GET_ALL_AFS_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionsByFilterAction extends AnyAction {
  type: typeof GET_ALL_AFS_BY_FILTER
  payload: AxiosPromise<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionsByFilterFulfilledAction extends AnyAction {
  type: typeof GET_ALL_AFS_BY_FILTER_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionsWithViewOptionsAction extends AnyAction {
  type: typeof GET_ALL_AFS_WITH_VIEW_OPTIONS
  payload: AxiosPromise<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionsWithViewOptionsFulfilledAction extends AnyAction {
  type: typeof GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionGroupsAction extends AnyAction {
  type: typeof GET_ALL_AFGROUPS
  payload: AxiosPromise<ServerResponse<AccessFunctionGroup>>
}

export interface GetAccessFunctionGroupsFulfilledAction extends AnyAction {
  type: typeof GET_ALL_AFGROUPS_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunctionGroup>>
}

export interface GetAccessFunctionAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION
  payload: AxiosPromise<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionFulfilledAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionRejectedAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface GetAccessFunctionWithViewOptionsAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS
  payload: AxiosPromise<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionWithViewOptionsFulfilledAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunction>>
}

export interface GetAccessFunctionWithViewOptionsRejectedAction extends AnyAction {
  type: typeof GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface CreateAccessFunctionThunkAction extends ThunkAction<void, RootState, unknown, Action<string>> { }

export interface CreateAccessFunctionAction extends AsyncAction {
  type: typeof CREATE_ACCESS_FUNCTION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface CreateAccessFunctionFulfilledAction extends AnyAction {
  type: typeof CREATE_ACCESS_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface UpdateAccessFunctionThunkAction extends ThunkAction<void, RootState, unknown, Action<string>> { }

export interface UpdateAccessFunctionAction extends AnyAction {
  type: typeof UPDATE_ACCESS_FUNCTION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface UpdateAccessFunctionFulfilledAction extends AnyAction {
  type: typeof UPDATE_ACCESS_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteAccessFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface DeleteAccessFunctionAction extends AnyAction {
  type: typeof DELETE_ACCESS_FUNCTION
  payload: AxiosPromise<ServerResponse<void>>
}

export interface DeleteAccessFunctionFulfilledAction extends AnyAction {
  type: typeof DELETE_ACCESS_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface DeleteAccessFunctionRejectedAction extends AnyAction {
  type: typeof DELETE_ACCESS_FUNCTION_REJECTED
  payload: AxiosResponse<ServerResponse<void>>
}

export interface GetAccessFunctionDataInterface extends AnyAction {
  type: typeof GET_AF_DI
  payload: AxiosPromise<ServerResponse<AccessFunctionDataInterface>>
}

export interface GetAccessFunctionDataFulfilledInterface extends AnyAction {
  type: typeof GET_AF_DI_FULFILLED
  payload: AxiosResponse<ServerResponse<AccessFunctionDataInterface>>
}

export interface SelectAccessFunctionAction extends AnyAction {
  type: typeof SELECT_ACCESS_FUNCTION
  payload: AccessFunction[]
}

export interface GetConfiguredAccessFunctions extends AnyAction {
  type: typeof GET_CONFIGURED_ACCESS_FUNCTION
}

export interface GetConfiguredAccessFunctionsFulfilled extends AnyAction {
  type: typeof GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<string>>
}

export interface AuditAccessFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface ResetAccessFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export interface RefreshAccessFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> { }

export type AccessFunctionActionTypes =
  GoToAccessFunctionCreateAction |
  GoToAccessFunctionEditAction |
  GoToAccessFunctionViewAction |
  GoToAccessFunctionCopyAction |
  GetAccessFunctionsAction |
  GetAccessFunctionsFulfilledAction |
  GetAccessFunctionsByFilterAction |
  GetAccessFunctionsByFilterFulfilledAction |
  GetAccessFunctionsWithViewOptionsAction |
  GetAccessFunctionsWithViewOptionsFulfilledAction |
  GetAccessFunctionGroupsAction |
  GetAccessFunctionGroupsFulfilledAction |
  GetAccessFunctionAction |
  GetAccessFunctionFulfilledAction |
  GetAccessFunctionRejectedAction |
  GetAccessFunctionWithViewOptionsAction |
  GetAccessFunctionWithViewOptionsFulfilledAction |
  GetAccessFunctionWithViewOptionsRejectedAction |
  CreateAccessFunctionAction |
  CreateAccessFunctionFulfilledAction |
  UpdateAccessFunctionAction |
  UpdateAccessFunctionFulfilledAction |
  DeleteAccessFunctionAction |
  DeleteAccessFunctionFulfilledAction |
  DeleteAccessFunctionRejectedAction |
  GetAccessFunctionDataInterface |
  GetAccessFunctionDataFulfilledInterface |
  SelectAccessFunctionAction |
  GetConfiguredAccessFunctions |
  GetConfiguredAccessFunctionsFulfilled;
