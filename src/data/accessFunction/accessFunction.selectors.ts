import { createSelector } from 'reselect';
import { RootState } from 'data/root.reducers';
import { getEnabledAccessFunction } from 'data/supportedFeatures/supportedFeatures.selectors';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { AccessFunction, AccessFunctionGroup, ConfiguredAccessFunction } from './accessFunction.types';

export const getAccessFunctions = (state: RootState): AccessFunction[] => state.afs.afs;
export const getAccessFunctionGroups = (state: RootState): AccessFunctionGroup[] => state.afs.afGroups;
export const getAccessFunction = (state: RootState): AccessFunction | null => state.afs.single;
export const getSelectedAccessFunction = (state: RootState): (AccessFunction[]) => state.afs.selected;
export const isDeletingAccessFunction = (state: RootState): boolean => state.afs.isDeleting;
export const getAllConfiguredAccessFunctions
  = (state: RootState) : ConfiguredAccessFunction[] => state.afs.configuredAfs;

export const getConfiguredAccessFunctions = createSelector(
  [getEnabledAccessFunction, getAllConfiguredAccessFunctions],
  (enabledAccessFunction, configuredAccessFunction) => {
    const enabledPoiAccessFunctions: SupportedFeaturesAccessFunctions = {};
    const configuredAFTags : string[] = configuredAccessFunction?.map((caf) => caf.afTag);
    Object.entries(enabledAccessFunction).forEach(([accessFunctionName, accessFunctionConfig]) => {
      if (configuredAFTags?.includes(accessFunctionName)) {
        enabledPoiAccessFunctions[accessFunctionName] = accessFunctionConfig;
      }
    });
    return enabledPoiAccessFunctions;
  },
);

export const getPoiAccessFunctions = createSelector(
  [getAccessFunctions, getConfiguredAccessFunctions],
  (accessFunctions, enabledPoiAccessFunctions) => {
    return accessFunctions.filter(({ tag }) => !!enabledPoiAccessFunctions[tag]);
  },
);

export const getPoiAccessFunctionByType = createSelector(
  [getPoiAccessFunctions],
  (accessFunctions) => {
    const poiAccessFunctionByType: {[key: string]: AccessFunction[]} = {};

    accessFunctions.forEach((af) => {
      if (!poiAccessFunctionByType[af.type]) {
        poiAccessFunctionByType[af.type] = [];
      }
      poiAccessFunctionByType[af.type].push(af);
    });

    return poiAccessFunctionByType;
  },
);

export const getAccessFunctionByType = createSelector(
  [getAccessFunctions],
  (accessFunctions) => {
    const accessFunctionByType: {[key: string]: AccessFunction[]} = {};

    accessFunctions.forEach((af) => {
      if (!accessFunctionByType[af.type]) {
        accessFunctionByType[af.type] = [];
      }
      accessFunctionByType[af.type].push(af);
    });

    return accessFunctionByType;
  },
);
