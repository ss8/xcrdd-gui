import MockAdapter from 'axios-mock-adapter';
import { History } from 'history';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import thunk from 'redux-thunk';
import Http from 'shared/http';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { WithAuditDetails } from 'data/types';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import * as actions from './accessFunction.actions';
import * as types from './accessFunction.types';
import * as api from './accessFunction.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const mockStore = configureMockStore([thunk]);

describe('accessFunction.actions', () => {
  let accessFunction: types.AccessFunction;
  let accessFunctionWithAudit: WithAuditDetails<types.AccessFunction>;
  let auditOnly: WithAuditDetails<void>;
  let history: History;
  let distributorConfiguration: DistributorConfiguration;
  let distributorConfigurationWithAudit: WithAuditDetails<DistributorConfiguration>;
  let store: MockStoreEnhanced<unknown>;

  beforeEach(() => {
    accessFunction = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      options: [],
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    };

    accessFunctionWithAudit = {
      data: accessFunction,
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.AF,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.ACCESS_FUNCTION_ADD,
      },
    };

    history = {
      location: {
        pathname: '',
        hash: '',
        search: '',
        state: '',
      },
      length: 1,
      action: 'PUSH',
      listen: jest.fn(),
      go: jest.fn(),
      push: jest.fn(),
      block: jest.fn(),
      replace: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      createHref: jest.fn(),
    };

    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'type1',
      listeners: [{
        id: 'listeners1',
        afProtocol: 'afProtocol',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        transport: 'TCP',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }],
      destinations: [{
        id: 'destinations1',
        ipAddress: '12.12.12.12',
        keepaliveInterval: '30',
        keepaliveRetries: '20',
        port: '1234',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        numOfConnections: '10',
        protocol: 'afProtocol',
        secInfoId: '1234',
        transport: 'TCP',
      }],
    };

    distributorConfigurationWithAudit = {
      data: distributorConfiguration,
    };

    store = mockStore({
      afs: {
        afs: [],
        afGroups: [],
        selected: [],
        isDeleting: false,
      },
    });

    axiosMock.onAny().reply(200);
  });

  it('should create an action to go to the access function list', () => {
    actions.goToAccessFunctionList(history)();
    expect(history.push).toBeCalledWith('/afs');
  });

  it('should create an action to go to the access function create', () => {
    actions.goToAccessFunctionCreate(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/afs/create');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_ACCESS_FUNCTION_CREATE,
    }]);
  });

  it('should create an action to go to the access function edit', () => {
    actions.goToAccessFunctionEdit(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/afs/edit');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_ACCESS_FUNCTION_EDIT,
    }]);
  });

  it('should create an action to go to the access function view', () => {
    actions.goToAccessFunctionView(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/afs/view');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_ACCESS_FUNCTION_VIEW,
    }]);
  });

  it('should create an action to go to the access function copy', () => {
    actions.goToAccessFunctionCopy(history)(store.dispatch);
    expect(history.push).toBeCalledWith('/afs/copy');
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_ACCESS_FUNCTION_COPY,
    }]);
  });

  it('should create an action to get the access functions', () => {
    const expectedAction = {
      type: types.GET_ALL_AFS,
      payload: api.getAccessFunctions('0'),
    };

    expect(actions.getAllAFs('0')).toEqual(expectedAction);
  });

  it('should create an action to get the access functions filtered', () => {
    const expectedAction = {
      type: types.GET_ALL_AFS_BY_FILTER,
      payload: api.getAccessFunctionsByFilter('0', 'some_filter'),
    };

    expect(actions.getAllAFsByFilter('0', 'some_filter')).toEqual(expectedAction);
  });

  it('should create an action to get the access functions with view options', () => {
    const expectedAction = {
      type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS,
      payload: api.getAccessFunctionsWithViewOptions('0'),
    };

    expect(actions.getAccessFunctionsWithViewOptions('0')).toEqual(expectedAction);
  });

  it('should create an action to get the access function groups', () => {
    const expectedAction = {
      type: types.GET_ALL_AFGROUPS,
      payload: api.getAccessFunctionGroups('0'),
    };

    expect(actions.getAllAFGroups('0')).toEqual(expectedAction);
  });

  it('should create an action to get the access function', () => {
    const expectedAction = {
      type: types.GET_ACCESS_FUNCTION,
      payload: api.getAccessFunction('0', 'id1'),
    };

    expect(actions.getAccessFunction('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to get the access function with options', () => {
    const expectedAction = {
      type: types.GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS,
      payload: api.getAccessFunctionWithViewOptions('0', 'id1'),
    };

    expect(actions.getAccessFunctionWithViewOptions('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to create the access function', async () => {
    const expectedActions = [
      {
        type: types.CREATE_ACCESS_FUNCTION,
        payload: api.createAccessFunction('0', accessFunctionWithAudit),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.createAccessFunction('0', accessFunctionWithAudit, history, []) as any).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should create an action to create the access function and update the distributor configuration', async () => {
    distributorConfiguration.listeners[0].afWhitelist[0].afId = '';
    const expectedActions = [
      {
        type: types.CREATE_ACCESS_FUNCTION,
        payload: api.createAccessFunction('0', accessFunctionWithAudit),
      },
    ];

    expect.assertions(1);
    return store.dispatch(
      actions.createAccessFunction('0', accessFunctionWithAudit, history, [distributorConfigurationWithAudit]) as any,
    )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('should create an action to update the access function', async () => {
    const expectedActions = [
      {
        type: types.UPDATE_ACCESS_FUNCTION,
        payload: api.updateAccessFunction('0', 'id1', accessFunctionWithAudit),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.updateAccessFunction('0', 'id1', accessFunctionWithAudit, history, []) as any).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should create an action to update the access function and update the distributor configuration', async () => {
    const expectedActions = [
      {
        type: types.UPDATE_ACCESS_FUNCTION,
        payload: api.updateAccessFunction('0', 'id1', accessFunctionWithAudit),
      },
    ];

    expect.assertions(1);
    return store.dispatch(
      actions.updateAccessFunction('0', 'id1', accessFunctionWithAudit, history, [distributorConfigurationWithAudit]) as any,
    )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('should create an action to delete the access function', () => {
    const expectedAction = [
      {
        type: types.DELETE_ACCESS_FUNCTION,
        payload: api.deleteAccessFunction('0', 'id1', auditOnly),
      },
      {
        type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS,
        payload: api.getAccessFunctions('0'),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.deleteAccessFunction('0', 'id1', auditOnly) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to get the access function data interfaces', () => {
    const expectedAction = {
      type: types.GET_AF_DI,
      payload: api.getAccessFunctionDataInterfaces('0', 'id1'),
    };

    expect(actions.getAFDataInterfaces('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to audit the access function', () => {
    const body: WithAuditDetails<types.AccessFunctionDataActionDetails> = { data: { action: 'AUDIT' } };
    const expectedAction = [
      {
        type: types.AUDIT_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction('0', 'id1', body),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.auditAccessFunction('0', 'id1', body) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to reset the access function', () => {
    const body: WithAuditDetails<types.AccessFunctionDataActionDetails> = { data: { action: 'AUDIT' } };
    const expectedAction = [
      {
        type: types.RESET_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction('0', 'id1', body),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.resetAccessFunction('0', 'id1', body) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to refresh the access function', () => {
    const body: WithAuditDetails<types.AccessFunctionDataActionDetails> = { data: { action: 'AUDIT' } };
    const expectedAction = [
      {
        type: types.REFRESH_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction('0', 'id1', body),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.refreshAccessFunction('0', 'id1', body) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to get configured access functions list', () => {
    const expectedAction = {
      type: types.GET_CONFIGURED_ACCESS_FUNCTION,
      payload: api.getConfiguredAccessFunctions('0'),
    };

    expect(actions.getConfiguredAccessFunctions('0')).toEqual(expectedAction);
  });
});
