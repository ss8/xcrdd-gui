import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import rootReducer, { RootState } from 'data/root.reducers';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import {
  getAccessFunctions,
  getAccessFunctionGroups,
  getAccessFunction,
  getSelectedAccessFunction,
  isDeletingAccessFunction,
  getPoiAccessFunctions,
  getPoiAccessFunctionByType,
} from './accessFunction.selectors';
import * as types from './accessFunction.types';

describe('accessFunction.selectors', () => {
  let accessFunctions: types.AccessFunction[];
  let accessFunctionGroups: types.AccessFunctionGroup[];
  let accessFunction: types.AccessFunction;
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    accessFunctions = [{
      id: 'id1',
      tag: 'ERK_UPF',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'ERK_UPF',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
    }, {
      id: 'id1',
      tag: 'ERK_SMF',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'ERK_SMF',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
    }, {
      id: 'id3',
      tag: 'ERK_SWG',
      dxOwner: 'dxOwner3',
      dxAccess: 'dxAccess3',
      name: 'name3',
      contact: { id: 'contactId3', name: 'contact3' },
      insideNat: false,
      liInterface: 'INI3',
      model: 'model3',
      preprovisioningLead: 'preprovisioningLead3',
      serialNumber: 'serialNumber3',
      timeZone: 'timeZone3',
      type: 'ERK_SWG',
      version: 'version3',
      latitude: 'latitude3',
      longitude: 'longitude3',
      status: 'ACTIVE',
    }];

    accessFunctionGroups = [{
      id: 'id2',
      dxOwner: 'dxOwner2',
      dxAccess: 'dxAccess2',
      name: 'name2',
      afIds: [{
        id: 'id1',
        name: 'name1',
      }],
    }];

    accessFunction = {
      id: 'id1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'contactId1', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      contentInterfaces: [],
      dataInterfaces: [],
      provisioningInterfaces: [],
      options: [],
      vpnConfig: [],
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      tag: 'tag1',
    };

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of access function', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.GET_ALL_AFS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getAccessFunctions(store.getState())).toEqual(accessFunctions);
  });

  it('should return the list of AF access function', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.GET_ALL_AFS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    };
    const supportedFeaturesAction: AnyAction = {
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    };
    const configuredAF: types.AccessFunctionActionTypes = {
      type: types.GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([JSON.stringify(configuredAfs)]),
    };
    const intermediaryState = rootReducer(undefined, action);
    store = mockStore(rootReducer(intermediaryState, configuredAF));
    store = mockStore(rootReducer(store.getState(), supportedFeaturesAction));
    expect(getPoiAccessFunctions(store.getState())).toEqual([accessFunctions[0], accessFunctions[1]]);
  });

  it('should return the list of access function groups', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.GET_ALL_AFGROUPS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctionGroups),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getAccessFunctionGroups(store.getState())).toEqual(accessFunctionGroups);
  });

  it('should return the single access function', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.GET_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([accessFunction]),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getAccessFunction(store.getState())).toEqual(accessFunction);
  });

  it('should return the selected access function', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [accessFunction],
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSelectedAccessFunction(store.getState())).toEqual([accessFunction]);
  });

  it('should return two selected access function if a multi selections is done', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [accessFunction, accessFunction],
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSelectedAccessFunction(store.getState())).toEqual([accessFunction, accessFunction]);
  });

  it('should change "isDeleting" dispatches DELETE_ACCESS_FUNCTION', () => {
    const action = {
      type: types.DELETE_ACCESS_FUNCTION,
      payload: [accessFunction],
    };

    // before dispatch DELETE_ACCESS_FUNCTION isDeleting should be false
    store = mockStore(rootReducer(undefined, {}));
    expect(isDeletingAccessFunction(store.getState())).toEqual(false);

    // after dispatch DELETE_ACCESS_FUNCTION isDeleting should be false
    store = mockStore(rootReducer(undefined, action));
    expect(isDeletingAccessFunction(store.getState())).toEqual(true);
  });

  it('should return the list of AF access function by type', () => {
    const action: types.AccessFunctionActionTypes = {
      type: types.GET_ALL_AFS_FULFILLED,
      payload: buildAxiosServerResponse(accessFunctions),
    };
    const supportedFeaturesAction: AnyAction = {
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    };
    const configuredAF: types.AccessFunctionActionTypes = {
      type: types.GET_CONFIGURED_ACCESS_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([JSON.stringify(configuredAfs)]),
    };
    const intermediaryState = rootReducer(undefined, action);
    store = mockStore(rootReducer(intermediaryState, configuredAF));
    store = mockStore(rootReducer(store.getState(), supportedFeaturesAction));
    expect(getPoiAccessFunctionByType(store.getState())).toEqual({
      ERK_SMF: [accessFunctions[1]], ERK_UPF: [accessFunctions[0]],
    });
  });
});
