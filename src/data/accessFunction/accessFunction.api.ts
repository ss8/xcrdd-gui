import { AxiosPromise } from 'axios';
import Http from 'shared/http';
import {
  AccessFunction,
  AccessFunctionGroup,
  AccessFunctionDataInterface,
  API_PATH_AFS,
  API_PATH_AF_GROUPS,
  AccessFunctionDataActionDetails,
} from './accessFunction.types';
import { ServerResponse, WithAuditDetails } from '../types';

const http = Http.getHttp();

export function getAccessFunctions(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<AccessFunction>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}`);
}

export function getAccessFunctionsByFilter(
  deliveryFunctionId: string,
  filter: string,
): AxiosPromise<ServerResponse<AccessFunction>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}?$filter=${filter}`);
}

export function getAccessFunctionsWithViewOptions(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<AccessFunction>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}?$view=options`);
}

export function getAccessFunctionGroups(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<AccessFunctionGroup>> {
  return http.get(`${API_PATH_AF_GROUPS}/${deliveryFunctionId}`);
}

export function getAccessFunction(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<AccessFunction>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}/${id}`);
}

export function getAccessFunctionWithViewOptions(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<AccessFunction>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}/${id}`, { params: { $view: 'options' } });
}

export function createAccessFunction(
  deliveryFunctionId: string,
  body: WithAuditDetails<AccessFunction>,
): AxiosPromise<ServerResponse<void>> {
  return http.post(`${API_PATH_AFS}/${deliveryFunctionId}`, body);
}

export function updateAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunction>,
): AxiosPromise<ServerResponse<void>> {
  return http.put(`${API_PATH_AFS}/${deliveryFunctionId}/${id}`, body);
}

export function deleteAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): AxiosPromise<ServerResponse<void>> {
  return http.delete(`${API_PATH_AFS}/${deliveryFunctionId}/${id}`, { data: body.auditDetails });
}

export function getAccessFunctionDataInterfaces(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<AccessFunctionDataInterface>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}/${id}/data-interface?$view=options`);
}

export function triggerActionAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunctionDataActionDetails>,
): AxiosPromise<ServerResponse<void>> {
  return http.put(`${API_PATH_AFS}/${deliveryFunctionId}/${id}/action`, body);
}

export function getConfiguredAccessFunctions(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<string[]>> {
  return http.get(`${API_PATH_AFS}/${deliveryFunctionId}/configuredAfs`);
}
