import cloneDeep from 'lodash.clonedeep';
import * as types from './accessFunction.types';
import {
  adaptToForm,
  adaptToSubmit,
  adaptToCopy,
  setAccessFunctionOperationValues,
} from './accessFunction.adapter';

describe('accessFunction.adapter', () => {
  let accessFunction: types.AccessFunction;
  let accessFunctionWithInterfaces: types.AccessFunction;
  let accessFunctionForm: types.AccessFunctionFormData;
  let accessFunctionFormWithInterfaces: types.AccessFunctionFormData;
  let accessFunctionMVNRPGW: types.AccessFunction;
  let accessFunctionFormMVNRPGW: types.AccessFunctionFormData;
  let accessFunctionFormSONUSSBC: types.AccessFunctionFormData;
  let accessFunctionFormKodiakPTT: types.AccessFunctionFormData;
  let genericAccessFunctionForGroup: types.AccessFunction;
  let genericAccessFunctionFormForGroup: types.AccessFunctionFormData;

  beforeEach(() => {
    accessFunction = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'Y29udGFjdDE=', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      options: [],
      provisioningInterfaces: [],
      contentInterfaces: [],
      dataInterfaces: [],
    };

    accessFunctionWithInterfaces = {
      ...accessFunction,
      provisioningInterfaces: [{
        id: 'provisioningInterfacesId1',
        name: 'provisioningInterfacesName1',
        options: [
          {
            key: 'destIPAddr',
            value: 'destIPAddr1',
          },
          {
            key: 'destPort',
            value: 'destPort1',
          },
          {
            key: 'dscp',
            value: 'dscp1',
          },
          {
            key: 'ownnetid',
            value: 'ownnetid1',
          },
          {
            key: 'reqState',
            value: 'ACTIVE',
          },
          {
            key: 'secinfoid',
            value: 'secinfoid1',
          },
          {
            key: 'tpkt',
            value: 'Y',
          },
          {
            key: 'imei',
            value: 'Y',
          },
          {
            key: 'imsi',
            value: 'N',
          },
          {
            key: 'msisdn',
            value: 'Y',
          },
          {
            key: 'x224headerpresent',
            value: 'Y',
          },
          {
            key: 'persistent',
            value: 'N',
          },
        ],
      }],
      dataInterfaces: [{
        id: 'dataInterfacesId1',
        name: 'dataInterfacesName1',
        options: [],
      }],
      contentInterfaces: [{
        id: 'contentInterfacesId1',
        name: 'contentInterfacesName1',
        options: [],
      }],
    };

    accessFunctionForm = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: 'Y29udGFjdDE=',
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      options: [],
      provisioningInterfaces: [],
      contentInterfaces: [],
      dataInterfaces: [],
    };

    accessFunctionFormWithInterfaces = {
      ...accessFunctionForm,
      provisioningInterfaces: [{
        id: 'provisioningInterfacesId1',
        name: 'provisioningInterfacesName1',
        destIPAddr: 'destIPAddr1',
        destPort: 'destPort1',
        dscp: 'dscp1',
        ownnetid: 'ownnetid1',
        reqState: 'ACTIVE',
        secinfoid: 'secinfoid1',
        state: 'ACTIVE',
        tpkt: true,
        imei: true,
        imsi: false,
        msisdn: true,
        x224headerpresent: true,
        persistent: false,
      }],
      dataInterfaces: [{
        id: 'dataInterfacesId1',
        name: 'dataInterfacesName1',
      }],
      contentInterfaces: [{
        id: 'contentInterfacesId1',
        name: 'contentInterfacesName1',
      }],
    };

    accessFunctionMVNRPGW = {
      ...accessFunction,
      version: '',
      type: 'MVNR_PGW',
      contentDeliveryVia: 'trafficManager',
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        options: [
          { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
          { key: 'lbid', value: 'MQ' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'ownnetid', value: 'U1M4XzMwNzA' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'traceLevel', value: '0' },
          { key: 'type', value: 'MVNRPGW_TCP' },
          { key: 'dscp', value: '10' },
        ],
      }, {
        id: '2',
        name: 'name2',
        options: [
          { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
          { key: 'lbid', value: 'MQ' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '2' },
          { key: 'ownnetid', value: 'U1M4XzMwNzA' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'traceLevel', value: '0' },
          { key: 'type', value: 'MVNRPGW_TCP' },
          { key: 'dscp', value: '10' },
        ],
      }],
    };

    accessFunctionFormMVNRPGW = {
      ...accessFunctionMVNRPGW,
      contact: 'Y29udGFjdDE=',
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        appProtocol: 'MVRN_UAG_PGW',
        lbid: 'MQ',
        destIPAddr: '1.1.1.1',
        destPort: '1,2',
        ownnetid: 'U1M4XzMwNzA',
        reqState: 'ACTIVE',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
        dscp: '10',
        maxx2connsperaf: '2',
      }],
    };

    accessFunctionFormSONUSSBC = {
      ...accessFunctionForm,
      tag: 'SONUS_SBC',
      dataInterfaces: [{
        id: 'data interface id',
        name: 'data interface name',
        dscp: '0',
        state: '',
        version: '1.5_101',
        destPort: '0',
        reqState: 'ACTIVE',
        secinfoid: 'Tk9ORQ',
        destIPAddr: '10.0.1.2',
        deliveryVia: 'PacketCable',
        sharedsecret: 'shared_secret_test',
      }],
    };

    accessFunctionFormKodiakPTT = {
      ...accessFunctionForm,
      tag: 'KODIAK_PTT',
      dataInterfaces: [{
        id: 'id1',
        name: 'name1',
        persistent: true,
      }, {
        id: 'id2',
        name: 'name2',
        persistent: true,
      }],
    };

    genericAccessFunctionForGroup = {
      ...accessFunctionMVNRPGW,
      type: '',
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        options: [
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
        ],
      }, {
        id: '2',
        name: 'name2',
        options: [
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '2' },
        ],
      }],
    };

    genericAccessFunctionFormForGroup = {
      ...accessFunctionFormMVNRPGW,
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        destIPAddr: '1.1.1.1',
        destPort: '1,2',
        maxx2connsperaf: '2',
      }],
    };
  });

  it('should adapt the access function data from server to be loaded in the form', () => {
    expect(adaptToForm(accessFunction)).toEqual(accessFunctionForm);
  });

  it('should group the interfaces for MVNR_PGW', () => {
    expect(adaptToForm(accessFunctionMVNRPGW)).toEqual(accessFunctionFormMVNRPGW);
  });

  it('should group the interfaces for MVNR_SBC', () => {
    genericAccessFunctionForGroup.type = 'MVNR_SBC';
    genericAccessFunctionFormForGroup.type = 'MVNR_SBC';
    expect(adaptToForm(genericAccessFunctionForGroup)).toEqual(genericAccessFunctionFormForGroup);
  });

  it('should group the interfaces for MVNR_CSCF', () => {
    genericAccessFunctionForGroup.type = 'MVNR_CSCF';
    genericAccessFunctionFormForGroup.type = 'MVNR_CSCF';
    expect(adaptToForm(genericAccessFunctionForGroup)).toEqual(genericAccessFunctionFormForGroup);
  });

  it('should group the interfaces for MVNR_RCS_AS', () => {
    genericAccessFunctionForGroup.type = 'MVNR_RCS_AS';
    genericAccessFunctionFormForGroup.type = 'MVNR_RCS_AS';
    expect(adaptToForm(genericAccessFunctionForGroup)).toEqual(genericAccessFunctionFormForGroup);
  });

  it('should group the interfaces for MVNR_RCS_MSTORE', () => {
    genericAccessFunctionForGroup.type = 'MVNR_RCS_MSTORE';
    genericAccessFunctionFormForGroup.type = 'MVNR_RCS_MSTORE';
    expect(adaptToForm(genericAccessFunctionForGroup)).toEqual(genericAccessFunctionFormForGroup);
  });

  it('should adapt the access function form data to be copied', () => {
    const {
      id,
      name,
      ...provisioningInterfacesToCopy
    } = (accessFunctionFormWithInterfaces.provisioningInterfaces as types.AccessFunctionInterfaceFormData[])[0];

    expect(adaptToCopy(accessFunctionFormWithInterfaces)).toEqual({
      ...accessFunctionFormWithInterfaces,
      id: '',
      name: 'Copy of name1',
      provisioningInterfaces: [{
        ...provisioningInterfacesToCopy,
        id: '1.accessFunctionInterface',
      }],
      dataInterfaces: [{ id: '2.accessFunctionInterface' }],
      contentInterfaces: [{ id: '3.accessFunctionInterface' }],
    });
  });

  describe('when creating a new access function', () => {
    let expectedAccessFunction: types.AccessFunction;
    let expectedAccessFunctionWithInterfaces: types.AccessFunction;

    it('should adapt the access function data from the form to be sent to the server', () => {
      expect(adaptToSubmit(accessFunctionForm)).toEqual(accessFunction);
    });

    it('should not add the operation fields values if not required', () => {
      expectedAccessFunction = cloneDeep(accessFunction);
      expect(setAccessFunctionOperationValues(accessFunction)).toEqual(expectedAccessFunction);
    });

    it('should adapt the access function data from the form to be sent to the server with interfaces', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);
      expect(adaptToSubmit(accessFunctionFormWithInterfaces)).toEqual(expectedAccessFunctionWithInterfaces);
    });

    it('should add the operation fields values', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);

      (expectedAccessFunctionWithInterfaces.provisioningInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAccessFunctionWithInterfaces.dataInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAccessFunctionWithInterfaces.contentInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      expect(setAccessFunctionOperationValues(accessFunctionWithInterfaces)).toEqual(
        expectedAccessFunctionWithInterfaces,
      );
    });

    it('should create the expected interfaces for MVNR_PGW', () => {
      expectedAccessFunction = cloneDeep(accessFunctionMVNRPGW);
      (expectedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).id = '';
      (expectedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).name = '';
      (expectedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).id = '';
      (expectedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).name = '';

      expect(adaptToSubmit(accessFunctionFormMVNRPGW)).toEqual(expectedAccessFunction);
    });

    it('should add the operation fields to the created interfaces for MVNR_PGW', () => {
      const adaptedAccessFunction = adaptToSubmit(accessFunctionFormMVNRPGW);

      const expectedAdaptedAccessFunction = cloneDeep(adaptedAccessFunction);
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).name = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).name = '';

      expect(setAccessFunctionOperationValues(adaptedAccessFunction)).toEqual(
        expectedAdaptedAccessFunction,
      );
    });

    it('should remove the temporary ids from the interfaces', () => {
      (accessFunctionFormMVNRPGW.contentInterfaces?.[0] as types.AccessFunctionInterface).id = `1.${types.ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID}`;

      const adaptedAccessFunction = adaptToSubmit(accessFunctionFormMVNRPGW);

      const expectedAdaptedAccessFunction = cloneDeep(adaptedAccessFunction);
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).name = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).name = '';

      expect(setAccessFunctionOperationValues(adaptedAccessFunction)).toEqual(
        expectedAdaptedAccessFunction,
      );
    });

    it('should remove unexpected fields for Sonus SBC before submitting to server', () => {
      expect(adaptToSubmit(accessFunctionFormSONUSSBC)).toEqual({
        ...accessFunctionFormSONUSSBC,
        contact: {
          id: 'Y29udGFjdDE=',
          name: 'contact1',
        },
        dataInterfaces: [{
          id: 'data interface id',
          name: 'data interface name',
          deliveryVia: 'PacketCable',
          options: [
            { key: 'version', value: '1.5_101' },
            { key: 'destPort', value: '0' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'destIPAddr', value: '10.0.1.2' },
            { key: 'sharedsecret', value: 'shared_secret_test' },
          ],
        }],
      });
    });

    it('should work as expected for Sonus SBC before submitting to server if dataInterfaces is empty', () => {
      accessFunctionFormSONUSSBC.dataInterfaces = [];

      expect(adaptToSubmit(accessFunctionFormSONUSSBC)).toEqual({
        ...accessFunctionFormSONUSSBC,
        contact: {
          id: 'Y29udGFjdDE=',
          name: 'contact1',
        },
      });
    });

    it('should remove ownipaddr fields from type PCRF before submitting to server when they are empty', () => {
      accessFunctionForm = {
        ...accessFunctionForm,
        type: 'PCRF',
        tag: 'CISCOVPCRF',
        provisioningInterfaces: [{
          id: 'id1',
          name: 'name1',
          uri: 'http://1.1.1.1',
          ownipaddr: '',
        }],
        dataInterfaces: [{
          id: 'id2',
          name: 'name2',
          uri: 'http://2.2.2.2',
          ownipaddr: '2.2.2.2',
        }],
      };

      expect(adaptToSubmit(accessFunctionForm)).toEqual({
        ...accessFunction,
        type: 'PCRF',
        tag: 'CISCOVPCRF',
        provisioningInterfaces: [{
          id: 'id1',
          name: 'name1',
          options: [
            { key: 'uri', value: 'http://1.1.1.1' },
          ],
        }],
        dataInterfaces: [{
          id: 'id2',
          name: 'name2',
          options: [
            { key: 'uri', value: 'http://2.2.2.2' },
            { key: 'ownipaddr', value: '2.2.2.2' },
          ],
        }],
      });
    });

    it('should set the persistent field value as expected for KODIAK_PTT', () => {
      expect(adaptToSubmit(accessFunctionFormKodiakPTT)).toEqual({
        ...accessFunctionFormKodiakPTT,
        contact: {
          id: 'Y29udGFjdDE=',
          name: 'contact1',
        },
        dataInterfaces: [{
          id: 'id1',
          name: 'name1',
          options: [
            { key: 'persistent', value: 'Y' },
          ],
        }, {
          id: 'id2',
          name: 'name2',
          options: [
            { key: 'persistent', value: 'N' },
          ],
        }],
      });
    });

    it('should set the persistent field value as expected for KODIAK_PTT', () => {
      accessFunctionFormKodiakPTT.dataInterfaces = [{
        ...accessFunctionFormKodiakPTT.dataInterfaces?.[0],
        id: 'id1',
        name: 'name1',
      }];
      expect(adaptToSubmit(accessFunctionFormKodiakPTT)).toEqual({
        ...accessFunctionFormKodiakPTT,
        contact: {
          id: 'Y29udGFjdDE=',
          name: 'contact1',
        },
        dataInterfaces: [{
          id: 'id1',
          name: 'name1',
          options: [
            { key: 'persistent', value: 'Y' },
          ],
        }],
      });
    });
  });

  describe('when editing an access function', () => {
    let originalAccessFunction: types.AccessFunction;
    let originalAccessFunctionWithInterfaces: types.AccessFunction;
    let expectedAccessFunction: types.AccessFunction;
    let expectedAccessFunctionWithInterfaces: types.AccessFunction;

    beforeEach(() => {
      originalAccessFunction = {
        ...accessFunction,
      };

      originalAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);
      (originalAccessFunctionWithInterfaces.provisioningInterfaces?.[0] as types.AccessFunctionDataInterface).name = 'name changed';
    });

    it('should adapt the access function data from the form to be sent to the server', () => {
      expect(adaptToSubmit(accessFunctionForm)).toEqual(accessFunction);
    });

    it('should add the operation fields as expected', () => {
      expectedAccessFunction = cloneDeep(accessFunction);
      expectedAccessFunction.operation = 'UPDATE';
      expect(setAccessFunctionOperationValues(accessFunction, originalAccessFunction, 'UPDATE')).toEqual(expectedAccessFunction);
    });

    it('should adapt the access function data from the form to be sent to the server with interfaces', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);
      expect(adaptToSubmit(accessFunctionFormWithInterfaces)).toEqual(expectedAccessFunctionWithInterfaces);
    });

    it('should remove the password field if it is has *****', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);

      accessFunctionFormWithInterfaces.dataInterfaces = [{
        id: 'dataInterfacesId1',
        name: 'dataInterfacesName1',
        password: '*****',
        rmtpassword: '*****',
        userPassword: '*****',
      }];

      expect(adaptToSubmit(accessFunctionFormWithInterfaces)).toEqual(expectedAccessFunctionWithInterfaces);
    });

    it('should keep the password fields if it is a new value', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);

      accessFunctionFormWithInterfaces.dataInterfaces = [{
        id: 'dataInterfacesId1',
        name: 'dataInterfacesName1',
        password: '123',
        rmtpassword: '456',
        userPassword: '789',
      }];
      expectedAccessFunctionWithInterfaces.dataInterfaces?.[0].options.push({ key: 'password', value: '123' });
      expectedAccessFunctionWithInterfaces.dataInterfaces?.[0].options.push({ key: 'rmtpassword', value: '456' });
      expectedAccessFunctionWithInterfaces.dataInterfaces?.[0].options.push({ key: 'userPassword', value: '789' });
      expect(adaptToSubmit(accessFunctionFormWithInterfaces)).toEqual(expectedAccessFunctionWithInterfaces);
    });

    it('should add the operation fields as expected when there are interfaces', () => {
      expectedAccessFunctionWithInterfaces = cloneDeep(accessFunctionWithInterfaces);

      expectedAccessFunctionWithInterfaces.operation = 'UPDATE';
      (expectedAccessFunctionWithInterfaces.provisioningInterfaces?.[0] as types.AccessFunctionInterface).operation = 'UPDATE';
      expectedAccessFunctionWithInterfaces.dataInterfaces = [{
        ...(expectedAccessFunctionWithInterfaces.dataInterfaces?.[0] as types.AccessFunctionDataInterface),
        operation: 'DELETE',
      }];
      (expectedAccessFunctionWithInterfaces.contentInterfaces?.[0] as types.AccessFunctionInterface).operation = 'NO_OPERATION';

      accessFunctionWithInterfaces.dataInterfaces = [];

      expect(
        setAccessFunctionOperationValues(accessFunctionWithInterfaces, originalAccessFunctionWithInterfaces, 'UPDATE'),
      ).toEqual(expectedAccessFunctionWithInterfaces);
    });

    it('should create the expected interfaces for MVNR_PGW', () => {
      expectedAccessFunction = cloneDeep(accessFunctionMVNRPGW);
      (expectedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).id = '';
      (expectedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).name = '';
      (expectedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).id = '';
      (expectedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).name = '';

      expect(adaptToSubmit(accessFunctionFormMVNRPGW)).toEqual(expectedAccessFunction);
    });

    it('should add the operation fields to the created interfaces for MVNR_PGW', () => {
      const adaptedAccessFunction = adaptToSubmit(accessFunctionFormMVNRPGW);

      const expectedAdaptedAccessFunction = cloneDeep(adaptedAccessFunction);
      expectedAdaptedAccessFunction.operation = 'UPDATE';
      expectedAdaptedAccessFunction.contentInterfaces?.push({
        ...(accessFunctionMVNRPGW.contentInterfaces?.[0] as types.AccessFunctionDataInterface),
        operation: 'DELETE',
      });
      expectedAdaptedAccessFunction.contentInterfaces?.push({
        ...(accessFunctionMVNRPGW.contentInterfaces?.[1] as types.AccessFunctionDataInterface),
        operation: 'DELETE',
      });
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[0] as types.AccessFunctionInterface).name = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).operation = 'CREATE';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).id = '';
      (expectedAdaptedAccessFunction.contentInterfaces?.[1] as types.AccessFunctionInterface).name = '';

      expect(
        setAccessFunctionOperationValues(adaptedAccessFunction, accessFunctionMVNRPGW, 'UPDATE'),
      ).toEqual(expectedAdaptedAccessFunction);
    });
  });
});
