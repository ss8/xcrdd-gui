import cloneDeep from 'lodash.clonedeep';
import omitBy from 'lodash.omitby';
import isNil from 'lodash.isnil';
import isEqual from 'lodash.isequal';
import { Contact, Operation } from 'data/types';
import {
  setVpnConfigsOperation,
  adaptVPNToSubmit,
  adaptVPNToCopy,
} from 'data/vpnConfig/vpnConfig.adapter';

import {
  AccessFunction,
  AccessFunctionFormData,
  AccessFunctionInterface,
  AccessFunctionInterfaceFormData,
  ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID,
  AccessFunctionInterfaceFormSectionType,
  ANY_ADDRESSES,
} from './accessFunction.types';

let nextId = 0;

const YES_NO_FIELDS = [
  'tpkt',
  'x224headerpresent',
  'imei',
  'imsi',
  'msisdn',
  'persistent',
];

const ENCRYPTED_FIELDS = [
  'password',
  'rmtpassword',
  'userPassword',
];

const ENCRYPTED_PLACEHOLDER = '*****';

type GroupInterfaceConfiguration = {
  [accessFunctionType: string]: {
    fieldName: keyof AccessFunctionFormData,
    fieldValue: string,
    interfaces: AccessFunctionInterfaceFormSectionType[],
  },
}

/**
 * The configuration below will be used to group the interfaces
 * for the Access Function tag used as the key.
 * The grouping is only done in the listed interface when a
 * field match the specified value.
 *
 * fieldName - the field name to check
 * fieldValue - the field value to match to the field specified in the fieldName
 * interfaces - list of interface name to group
 */
const GROUP_INTERFACES_FOR_AFS: GroupInterfaceConfiguration = {
  MVNR_PGW: {
    fieldName: 'contentDeliveryVia',
    fieldValue: 'trafficManager',
    interfaces: ['contentInterfaces'],
  },
  MVNR_SBC: {
    fieldName: 'contentDeliveryVia',
    fieldValue: 'trafficManager',
    interfaces: ['contentInterfaces'],
  },
  MVNR_CSCF: {
    fieldName: 'contentDeliveryVia',
    fieldValue: 'trafficManager',
    interfaces: ['contentInterfaces'],
  },
  MVNR_RCS_AS: {
    fieldName: 'contentDeliveryVia',
    fieldValue: 'trafficManager',
    interfaces: ['contentInterfaces'],
  },
  MVNR_RCS_MSTORE: {
    fieldName: 'contentDeliveryVia',
    fieldValue: 'trafficManager',
    interfaces: ['contentInterfaces'],
  },
};

type KeyValueString = {
  key: string
  value: string
}

type KeyValueNull = {
  key: string
  value: null
}

type KeyValueUnknown = {
  key: string
  value: unknown
}

export function adaptToForm(accessFunction: AccessFunction): AccessFunctionFormData {
  const clonedAccessFunction = cloneDeep(accessFunction);

  const accessFunctionForm: AccessFunctionFormData = {
    ...clonedAccessFunction,
    contact: adaptContactToForm(clonedAccessFunction.contact),
    provisioningInterfaces: adaptInterfaceToForm(clonedAccessFunction.provisioningInterfaces),
    dataInterfaces: adaptInterfaceToForm(clonedAccessFunction.dataInterfaces),
    contentInterfaces: adaptInterfaceToForm(clonedAccessFunction.contentInterfaces),
  };

  groupInterfacesToForm(accessFunctionForm);
  deliveryViaToForm(accessFunctionForm);
  return omitBy<AccessFunctionFormData>(accessFunctionForm, isNil) as AccessFunctionFormData;
}

export function adaptToCopy(accessFunctionForm: AccessFunctionFormData): AccessFunctionFormData {
  const clonedAccessFunction = cloneDeep(accessFunctionForm);
  clonedAccessFunction.name = `Copy of ${clonedAccessFunction.name}`;
  clonedAccessFunction.id = '';

  const accessFunctionToCopy: AccessFunctionFormData = {
    ...clonedAccessFunction,
    provisioningInterfaces: adaptInterfaceToCopy(clonedAccessFunction.provisioningInterfaces),
    dataInterfaces: adaptInterfaceToCopy(clonedAccessFunction.dataInterfaces),
    contentInterfaces: adaptInterfaceToCopy(clonedAccessFunction.contentInterfaces),
    vpnConfig: adaptVPNToCopy(clonedAccessFunction.vpnConfig),
  };

  return omitBy<AccessFunctionFormData>(accessFunctionToCopy, isNil) as AccessFunctionFormData;
}

export function adaptToSubmit(
  accessFunctionForm: AccessFunctionFormData,
): AccessFunction {
  const clonedAccessFunctionForm = cloneDeep(accessFunctionForm);
  ungroupInterfacesToSubmit(clonedAccessFunctionForm);
  removeDeliveryViaFields(clonedAccessFunctionForm);
  removeUnexpectedFields(clonedAccessFunctionForm);
  adaptKodiakPTTFields(clonedAccessFunctionForm);

  const accessFunction: AccessFunction = {
    ...clonedAccessFunctionForm,
    contact: adaptContactToSubmit(clonedAccessFunctionForm.contact),
    provisioningInterfaces: adaptInterfaceToSubmit(clonedAccessFunctionForm.provisioningInterfaces),
    dataInterfaces: adaptInterfaceToSubmit(clonedAccessFunctionForm.dataInterfaces),
    contentInterfaces: adaptInterfaceToSubmit(clonedAccessFunctionForm.contentInterfaces),
    vpnConfig: adaptVPNToSubmit(clonedAccessFunctionForm.vpnConfig),
  };
  // Remove any undefined entry
  return omitBy<AccessFunction>(accessFunction, isNil) as AccessFunction;
}

function adaptKodiakPTTFields(accessFunctionForm: AccessFunctionFormData): void {
  if (accessFunctionForm.tag !== 'KODIAK_PTT') {
    return;
  }

  const { dataInterfaces } = accessFunctionForm;
  if (dataInterfaces == null || dataInterfaces.length < 2) {
    return;
  }

  // For Kodiak PTT, there is a hidden field called persistent which the first
  // dataInterface has value set as true and the second value set as false.
  // Because the template can only have one initial value (true), the false
  // value is manually set here for the second interface if it exists.
  dataInterfaces[1].persistent = false;
}

function removeUnexpectedFields(accessFunctionForm: AccessFunctionFormData) {
  if (accessFunctionForm.tag === 'SONUS_SBC') {
    const dataInterface = accessFunctionForm.dataInterfaces?.[0];
    if (dataInterface == null) {
      return;
    }

    const { deliveryVia } = dataInterface;

    if (deliveryVia === 'VoIP') {
      delete dataInterface.sharedsecret;
      delete dataInterface.version;
    } else {
      delete dataInterface.secinfoid;
      delete dataInterface.dscp;
    }

  } else if (accessFunctionForm.type === 'PCRF') {
    deletePropertyIfEqualToFieldValue(accessFunctionForm.provisioningInterfaces, 'ownipaddr', '');
    deletePropertyIfEqualToFieldValue(accessFunctionForm.dataInterfaces, 'ownipaddr', '');
  }
}

function deletePropertyIfEqualToFieldValue(
  accessFunctionInterfaces: AccessFunctionInterfaceFormData[] | undefined,
  fieldName: keyof AccessFunctionInterfaceFormData,
  fieldValue: unknown,
): void {
  if (accessFunctionInterfaces == null) {
    return;
  }

  accessFunctionInterfaces.forEach((entry) => {
    if (entry[fieldName] === fieldValue) {
      delete entry[fieldName];
    }
  });
}

export function setAccessFunctionOperationValues(
  accessFunction: AccessFunction,
  originalAccessFunction: AccessFunction | null = null,
  operation: Operation = 'CREATE',
): AccessFunction {
  const clonedAccessFunction = cloneDeep(accessFunction);

  if (operation !== 'CREATE') {
    clonedAccessFunction.operation = operation;
  }

  setInterfacesOperation(clonedAccessFunction, originalAccessFunction);

  setVpnConfigsOperation(clonedAccessFunction.vpnConfig, originalAccessFunction?.vpnConfig);

  return clonedAccessFunction;
}

function setInterfacesOperation(
  accessFunction: AccessFunction,
  originalAccessFunction: AccessFunction | null = null,
): void {
  setInterfaceOperationValueBasedInOriginal(
    accessFunction.provisioningInterfaces,
    originalAccessFunction?.provisioningInterfaces,
  );
  setInterfaceOperationValueBasedInOriginal(
    accessFunction.contentInterfaces,
    originalAccessFunction?.contentInterfaces,
  );
  setInterfaceOperationValueBasedInOriginal(
    accessFunction.dataInterfaces,
    originalAccessFunction?.dataInterfaces,
  );
}

function setInterfaceOperationValueBasedInOriginal(
  interfaces: AccessFunctionInterface[] | undefined,
  originalInterfaces: AccessFunctionInterface[] | undefined,
) {
  // Check which interfaces will be marked as CREATE, UPDATE or NO_OPERATION
  interfaces?.forEach((interfaceEntry) => {
    const originalInterfaceEntry = originalInterfaces?.find(({ id }) => id === interfaceEntry.id);
    if (originalInterfaceEntry == null) {
      interfaceEntry.operation = 'CREATE';
    } else if (!isEqual(interfaceEntry, originalInterfaceEntry)) {
      interfaceEntry.operation = 'UPDATE';
    } else {
      interfaceEntry.operation = 'NO_OPERATION';
    }
  });

  // Check which interfaces will be marked as DELETE
  const interfacesToDelete =
    originalInterfaces?.filter((originalInterfaceEntry) => {
      return !interfaces?.find(({ id }) => id === originalInterfaceEntry.id);
    })
      .map((originalInterfaceEntry) => ({
        ...originalInterfaceEntry,
        operation: 'DELETE',
      }));

  if (interfacesToDelete) {
    interfaces?.push(...(interfacesToDelete as AccessFunctionInterface[]));
  }
}

function adaptContactToForm(
  contact: Contact | undefined,
): string | undefined {
  if (!contact) {
    return '';
  }

  return contact.id;
}

function adaptInterfaceToForm(
  accessFunctionInterfaces: AccessFunctionInterface[] | undefined,
): AccessFunctionInterfaceFormData[] | undefined {
  if (!accessFunctionInterfaces) {
    return accessFunctionInterfaces;
  }

  return accessFunctionInterfaces.map(({ id, name, options }) => ({
    id,
    name,
    ...options?.map(convertYNValuesToBoolean)
      .map(convertDefaultAnyAddressToEmptyString)
      .reduce((previousValue, currentValue) => ({
        ...previousValue,
        [currentValue.key]: currentValue.value,
      }), {}),
  }));
}

function adaptInterfaceToCopy(
  accessFunctionInterfaceFormData: AccessFunctionInterfaceFormData[] | undefined,
): AccessFunctionInterfaceFormData[] | undefined {
  if (accessFunctionInterfaceFormData == null) {
    return accessFunctionInterfaceFormData;
  }

  return accessFunctionInterfaceFormData?.map(({ id, name, ...rest }) => {
    nextId += 1;

    return {
      ...rest,
      id: `${nextId}.${ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID}`,
    } as AccessFunctionInterfaceFormData;

  });
}

function adaptContactToSubmit(
  contact: string | undefined,
): Contact | undefined {
  if (!contact) {
    return {
      id: '',
      name: '',
    };
  }

  return {
    id: contact,
    name: atob(contact),
  };
}

function adaptInterfaceToSubmit(
  accessFunctionInterfacesForm: AccessFunctionInterfaceFormData[] | undefined,
): AccessFunctionInterface[] {
  if (!accessFunctionInterfacesForm) {
    return [];
  }

  return accessFunctionInterfacesForm.map((entry) => {
    let { id } = entry;

    const {
      name,
      id: _id,
      state: _state,
      deliveryVia,
      ...everythingElse
    } = entry;

    if (id.includes(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID)) {
      id = '';
    }

    // Convert everything else to options and remove the null/undefined ones
    const options = Object.entries(everythingElse)
      .map(([key, value]) => ({ key, value }))
      .map(convertEncryptedValues)
      .map(convertBooleanValuesToYN)
      .filter(({ value }) => value != null);

    const accessFunctionInterface: AccessFunctionInterface = {
      name,
      id,
      deliveryVia,
      options,
    };

    // Remove any undefined entry
    return omitBy<AccessFunctionInterface>(accessFunctionInterface, isNil) as AccessFunctionInterface;
  });
}

function deliveryViaToForm(
  accessFunctionForm: AccessFunctionFormData,
): void {
  const {
    tag,
  } = accessFunctionForm;

  if (tag === 'ETSI103221') {
    if (accessFunctionForm.dataInterfaces && accessFunctionForm.dataInterfaces.length > 0) {
      accessFunctionForm.dataDeliveryVia = 'directFromAF';
    } else {
      accessFunctionForm.dataDeliveryVia = 'ss8Distributor';
    }

    if (accessFunctionForm.contentInterfaces && accessFunctionForm.contentInterfaces.length > 0) {
      accessFunctionForm.contentDeliveryVia = 'directFromAF';
    } else {
      accessFunctionForm.contentDeliveryVia = 'ss8Distributor';
    }
  }
}

function removeDeliveryViaFields(
  accessFunctionForm: AccessFunctionFormData,
): void {
  const {
    tag,
  } = accessFunctionForm;

  if (tag === 'ETSI103221') {
    delete accessFunctionForm.dataDeliveryVia;
    delete accessFunctionForm.contentDeliveryVia;
  }
}

function groupInterfacesToForm(
  accessFunctionForm: AccessFunctionFormData,
): void {
  const {
    type,
  } = accessFunctionForm;

  const configuration = GROUP_INTERFACES_FOR_AFS[type];
  if (configuration == null) {
    return;
  }

  const {
    fieldName,
    fieldValue,
    interfaces,
  } = configuration;

  if (accessFunctionForm[fieldName] === fieldValue) {
    interfaces.forEach((interfaceName) => {
      const interfaceData = accessFunctionForm[interfaceName];

      if (interfaceData == null || interfaceData.length === 0) {
        return;
      }

      const numberOfInterfaces = interfaceData.length;

      accessFunctionForm[interfaceName] = [{
        ...interfaceData[0],
        destPort: getPortForGroupInterfaces(interfaceData),
        maxx2connsperaf: numberOfInterfaces.toString(),
      }];
    });
  }
}

function getPortForGroupInterfaces(interfaceData: AccessFunctionInterfaceFormData[]): string {
  if (interfaceData.length > 4 || interfaceData[0].destPort === '0') {
    return '0';
  }

  return interfaceData.map(({ destPort }) => destPort).join(',');
}

function ungroupInterfacesToSubmit(
  accessFunctionForm: AccessFunctionFormData,
): void {

  const {
    type,
  } = accessFunctionForm;

  const configuration = GROUP_INTERFACES_FOR_AFS[type];
  if (configuration == null) {
    return;
  }

  const {
    fieldName,
    fieldValue,
    interfaces,
  } = configuration;

  if (accessFunctionForm[fieldName] === fieldValue) {
    interfaces.forEach((interfaceName) => {
      const interfaceData = accessFunctionForm[interfaceName];

      if (interfaceData == null || interfaceData.length === 0) {
        return;
      }

      const {
        id,
        name,
        maxx2connsperaf: numberOfInterfaces = '0',
        ...remainingFields
      } = interfaceData[0];

      const newInterfaces: AccessFunctionInterfaceFormData[] = [];
      for (let i = 0; i < +numberOfInterfaces; i += 1) {
        newInterfaces.push({
          ...remainingFields,
          id: '',
          name: '',
          destPort: getPortForInterface(remainingFields, i),
        });
      }

      accessFunctionForm[interfaceName] = newInterfaces;
    });
  }
}

function getPortForInterface(interfaceData: Partial<AccessFunctionInterfaceFormData>, index: number): string {
  const { destPort } = interfaceData;

  if (destPort == null || destPort === '0') {
    return '0';
  }

  return destPort.split(',').map((entry) => entry.trim())[index];
}

function convertYNValuesToBoolean({ key, value }: KeyValueUnknown): KeyValueUnknown {
  if (!YES_NO_FIELDS.includes(key)) {
    return { key, value };
  }

  return { key, value: value === 'Y' };
}

function convertDefaultAnyAddressToEmptyString({ key, value }: KeyValueUnknown): KeyValueUnknown {
  if (key.toLowerCase() === 'ownIPAddr'.toLowerCase() && ANY_ADDRESSES.includes(value as string)) {
    return { key, value: '' };
  }

  return { key, value };
}

function convertBooleanValuesToYN({ key, value }: KeyValueUnknown): KeyValueString {
  if (!YES_NO_FIELDS.includes(key)) {
    return { key, value: value as string };
  }

  if (value === true) {
    return { key, value: 'Y' };
  }

  return { key, value: 'N' };
}

function convertEncryptedValues({ key, value }: KeyValueUnknown): KeyValueUnknown | KeyValueNull {
  if (ENCRYPTED_FIELDS.includes(key) && value === ENCRYPTED_PLACEHOLDER) {
    return { key, value: null };
  }

  return { key, value };
}
