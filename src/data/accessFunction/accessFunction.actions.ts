import { Dispatch, Action } from 'redux';
import { History } from 'history';
import { showSuccessMessage, showError } from 'shared/toaster';
import { WithAuditDetails } from 'data/types';
import { RootState } from 'data/root.reducers';
import { ThunkDispatch } from 'redux-thunk';
import * as distributorConfigTypes from 'data/distributorConfiguration/distributorConfiguration.types';
import {
  GET_ALL_AFS,
  GET_ACCESS_FUNCTION,
  GET_ALL_AFS_BY_FILTER,
  GET_ALL_AFGROUPS,
  CREATE_ACCESS_FUNCTION,
  UPDATE_ACCESS_FUNCTION,
  DELETE_ACCESS_FUNCTION,
  GO_TO_ACCESS_FUNCTION_CREATE,
  GO_TO_ACCESS_FUNCTION_EDIT,
  GO_TO_ACCESS_FUNCTION_COPY,
  GO_TO_ACCESS_FUNCTION_VIEW,
  GET_AF_DI,
  AccessFunctionActionTypes,
  AccessFunction,
  GET_ALL_AFS_WITH_VIEW_OPTIONS,
  API_PATH_AFS,
  SELECT_ACCESS_FUNCTION,
  SelectAccessFunctionAction,
  GetAccessFunctionGroupsAction,
  GetAccessFunctionDataInterface,
  DeleteAccessFunctionThunkAction,
  GetAccessFunctionsAction,
  CreateAccessFunctionThunkAction,
  UpdateAccessFunctionThunkAction,
  GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS,
  CreateAccessFunctionFulfilledAction,
  AUDIT_ACCESS_FUNCTION,
  RESET_ACCESS_FUNCTION,
  REFRESH_ACCESS_FUNCTION,
  AuditAccessFunctionThunkAction,
  AccessFunctionDataActionDetails,
  GetConfiguredAccessFunctions,
  GET_CONFIGURED_ACCESS_FUNCTION,
  ResetAccessFunctionThunkAction,
  RefreshAccessFunctionThunkAction,
} from './accessFunction.types';
import * as api from './accessFunction.api';
import * as distributorConfigurationApi from '../distributorConfiguration/distributorConfiguration.api';
import { extractErrorMessage } from '../../utils';

export function goToAccessFunctionList(history: History): () => void {
  return () => {
    history.push(`${API_PATH_AFS}`);
  };
}

export function goToAccessFunctionCreate(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_ACCESS_FUNCTION_CREATE,
    });

    history.push(`${API_PATH_AFS}/create`);
  };
}

export function goToAccessFunctionEdit(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_ACCESS_FUNCTION_EDIT,
    });

    history.push(`${API_PATH_AFS}/edit`);
  };
}

export function goToAccessFunctionCopy(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_ACCESS_FUNCTION_COPY,
    });

    history.push(`${API_PATH_AFS}/copy`);
  };
}

export function goToAccessFunctionView(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_ACCESS_FUNCTION_VIEW,
    });

    history.push(`${API_PATH_AFS}/view`);
  };
}

export function getAllAFs(deliveryFunctionId: string): GetAccessFunctionsAction {
  return {
    type: GET_ALL_AFS,
    payload: api.getAccessFunctions(deliveryFunctionId),
  };
}

export function getAllAFsByFilter(deliveryFunctionId: string, filter: string): AccessFunctionActionTypes {
  return {
    type: GET_ALL_AFS_BY_FILTER,
    payload: api.getAccessFunctionsByFilter(deliveryFunctionId, filter),
  };
}

export function getAccessFunctionsWithViewOptions(deliveryFunctionId: string): AccessFunctionActionTypes {
  return {
    type: GET_ALL_AFS_WITH_VIEW_OPTIONS,
    payload: api.getAccessFunctionsWithViewOptions(deliveryFunctionId),
  };
}

export function getAllAFGroups(deliveryFunctionId: string): GetAccessFunctionGroupsAction {
  return {
    type: GET_ALL_AFGROUPS,
    payload: api.getAccessFunctionGroups(deliveryFunctionId),
  };
}

export function getAccessFunction(deliveryFunctionId: string, id: string): AccessFunctionActionTypes {
  return {
    type: GET_ACCESS_FUNCTION,
    payload: api.getAccessFunction(deliveryFunctionId, id),
  };
}

export function getAccessFunctionWithViewOptions(deliveryFunctionId: string, id: string): AccessFunctionActionTypes {
  return {
    type: GET_ACCESS_FUNCTION_WITH_VIEW_OPTIONS,
    payload: api.getAccessFunctionWithViewOptions(deliveryFunctionId, id),
  };
}

export function createAccessFunction(
  deliveryFunctionId: string,
  body: WithAuditDetails<AccessFunction>,
  history: History,
  distributorConfigurationBodies: WithAuditDetails<distributorConfigTypes.DistributorConfiguration>[],
): CreateAccessFunctionThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: CREATE_ACCESS_FUNCTION,
        payload: api.createAccessFunction(deliveryFunctionId, body),
      });

      if (distributorConfigurationBodies.length > 0) {
        for (let i = 0; i < distributorConfigurationBodies.length; i += 1) {
          const distributorConfigurationBody = distributorConfigurationBodies[i];
          if (distributorConfigurationBody && distributorConfigurationBody.data) {
            await distributorConfigurationApi.updateDistributorConfiguration(
              deliveryFunctionId,
              distributorConfigurationBody.data.id,
              distributorConfigurationBody,
            );
          }
        }
      }

      dispatch(goToAccessFunctionList(history));
      showSuccessMessage('Access Function successfully created.');

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create Access Function: ${errorMessage}. Try again later.`);
    }
  };
}

export function updateAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunction>,
  history: History,
  distributorConfigurationBodies: WithAuditDetails<distributorConfigTypes.DistributorConfiguration>[],
): UpdateAccessFunctionThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: UPDATE_ACCESS_FUNCTION,
        payload: api.updateAccessFunction(deliveryFunctionId, id, body),
      });

      if (distributorConfigurationBodies.length > 0) {
        for (let i = 0; i < distributorConfigurationBodies.length; i += 1) {
          const distributorConfigurationBody = distributorConfigurationBodies[i];
          if (distributorConfigurationBody && distributorConfigurationBody.data) {
            await distributorConfigurationApi.updateDistributorConfiguration(
              deliveryFunctionId,
              distributorConfigurationBody.data.id,
              distributorConfigurationBody,
            );
          }
        }
      }

      dispatch(goToAccessFunctionList(history));
      showSuccessMessage('Access Function successfully updated.');

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update Access Function: ${errorMessage}. Try again later.`);
    }
  };
}

export function deleteAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): DeleteAccessFunctionThunkAction {
  return async (dispatch: Dispatch, getState: ()=> RootState) => {
    const selectedAccessFunction = getState()?.afs?.selected[0];

    try {
      await dispatch({
        type: DELETE_ACCESS_FUNCTION,
        payload: api.deleteAccessFunction(deliveryFunctionId, id, body),
      });
      showSuccessMessage(`${selectedAccessFunction?.name} successfully deleted.`);
    } catch (e) {
      showError(`Unable to delete ${selectedAccessFunction?.name}. Try again later.`);
    } finally {
      // Refresh the list of afs
      dispatch(getAccessFunctionsWithViewOptions(deliveryFunctionId));
    }
  };
}

export function getAFDataInterfaces(
  deliveryFunctionId: string,
  id: string,
): GetAccessFunctionDataInterface {
  return {
    type: GET_AF_DI,
    payload: api.getAccessFunctionDataInterfaces(deliveryFunctionId, id),
  };
}

export function selectAccessFunction(accessFunction:AccessFunction[]): SelectAccessFunctionAction {
  return {
    type: SELECT_ACCESS_FUNCTION,
    payload: accessFunction,
  };
}

export function auditAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunctionDataActionDetails>,
): AuditAccessFunctionThunkAction {
  return async (dispatch: Dispatch, getState: ()=> RootState) => {
    const selectedAccessFunction = getState()?.afs?.selected[0];

    try {
      await dispatch({
        type: AUDIT_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction(deliveryFunctionId, id, body),
      });
      showSuccessMessage(`Audit on ${selectedAccessFunction?.name} successfully triggered. Check Alarms for result.`);
    } catch (e) {
      showError(`Unable to trigger audit on ${selectedAccessFunction?.name}. Try again later.`);
    }
  };
}

export function resetAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunctionDataActionDetails>,
): ResetAccessFunctionThunkAction {
  return async (dispatch: Dispatch, getState: ()=> RootState) => {
    const selectedAccessFunction = getState()?.afs?.selected[0];

    try {
      await dispatch({
        type: RESET_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction(deliveryFunctionId, id, body),
      });
      showSuccessMessage(`Reset on ${selectedAccessFunction?.name} successfully triggered. Check Alarms for result.`);
    } catch (e) {
      showError(`Unable to trigger reset on ${selectedAccessFunction?.name}. Try again later.`);
    }
  };
}

export function refreshAccessFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<AccessFunctionDataActionDetails>,
): RefreshAccessFunctionThunkAction {
  return async (dispatch: Dispatch, getState: ()=> RootState) => {
    const selectedAccessFunction = getState()?.afs?.selected[0];

    try {
      await dispatch({
        type: REFRESH_ACCESS_FUNCTION,
        payload: api.triggerActionAccessFunction(deliveryFunctionId, id, body),
      });
      showSuccessMessage(`Refresh on ${selectedAccessFunction?.name} successfully triggered. Check Alarms for result.`);
    } catch (e) {
      showError(`Unable to trigger refresh on ${selectedAccessFunction?.name}. Try again later.`);
    }
  };
}

export function getConfiguredAccessFunctions(
  deliveryFunctionId: string,
): GetConfiguredAccessFunctions {
  return {
    type: GET_CONFIGURED_ACCESS_FUNCTION,
    payload: api.getConfiguredAccessFunctions(deliveryFunctionId),
  };
}
