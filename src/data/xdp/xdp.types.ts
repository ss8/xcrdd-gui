import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_XDPS = '/xdps';

export const GET_XDPS = 'GET_XDPS';
export const GET_XDPS_PENDING = 'GET_XDPS_PENDING';
export const GET_XDPS_FULFILLED = 'GET_XDPS_FULFILLED';
export const GET_XDPS_REJECTED = 'GET_XDPS_REJECTED';

export interface Xdp {
  id: string
  inPortEnd: string
  inPortStart: string
  insideNat: boolean
  ipAddr: string
  ipduId: string
  outPortEnd: string
  outPortStart: string
  port: string
  state: string
  switchTech: string
}

export interface XdpState {
  entities: Xdp[]
}

export interface GetXdpAction extends AnyAction {
  type: typeof GET_XDPS
  payload: AxiosPromise<ServerResponse<Xdp>>
}

export interface GetXdpPendingAction extends AnyAction {
  type: typeof GET_XDPS_PENDING
  payload: AxiosResponse<ServerResponse<Xdp>>
}
export interface GetXdpFulfilledAction extends AnyAction {
  type: typeof GET_XDPS_FULFILLED
  payload: AxiosResponse<ServerResponse<Xdp>>
}

export interface GetXdpRejectedAction extends AnyAction {
  type: typeof GET_XDPS_REJECTED
  payload: AxiosResponse<ServerResponse<Xdp>>
}

export type XdpActionTypes =
  GetXdpAction |
  GetXdpPendingAction |
  GetXdpFulfilledAction |
  GetXdpRejectedAction;
