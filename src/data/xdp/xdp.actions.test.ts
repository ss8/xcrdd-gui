import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './xdp.actions';
import * as types from './xdp.types';
import * as api from './xdp.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('xdp.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the xdps', () => {
    const expectedAction = {
      type: types.GET_XDPS,
      payload: api.getXdps('0'),
    };

    expect(actions.getXdps('0')).toEqual(expectedAction);
  });
});
