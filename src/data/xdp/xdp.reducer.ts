import {
  GET_XDPS_FULFILLED,
  XdpActionTypes,
  XdpState,
} from './xdp.types';

const initialState: XdpState = {
  entities: [],
};

export default function xdpReducer(
  state = initialState,
  action: XdpActionTypes,
): XdpState {
  switch (action.type) {
    case GET_XDPS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
