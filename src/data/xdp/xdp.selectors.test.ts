import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockXdps } from '__test__/mockXdps';
import {
  getXdps,
} from './xdp.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './xdp.types';

describe('xdp.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of xdps', () => {
    const action: types.XdpActionTypes = {
      type: types.GET_XDPS_FULFILLED,
      payload: buildAxiosServerResponse(mockXdps),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getXdps(store.getState())).toEqual(mockXdps);
  });
});
