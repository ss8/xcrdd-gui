import { RootState } from '../root.reducers';
import { Xdp } from './xdp.types';

// eslint-disable-next-line import/prefer-default-export
export const getXdps = (state: RootState): Xdp[] => state.xdp.entities;
