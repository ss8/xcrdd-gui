import * as api from './xdp.api';
import { GET_XDPS, GetXdpAction } from './xdp.types';

// eslint-disable-next-line import/prefer-default-export
export function getXdps(deliveryFunctionId: string): GetXdpAction {
  return {
    type: GET_XDPS,
    payload: api.getXdps(deliveryFunctionId),
  };
}
