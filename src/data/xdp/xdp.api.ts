import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  Xdp,
  API_PATH_XDPS,
} from './xdp.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getXdps(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<Xdp>> {
  return http.get(`${API_PATH_XDPS}/${deliveryFunctionId}`);
}
