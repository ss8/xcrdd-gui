import { mockXdps } from '__test__/mockXdps';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './xdp.reducer';
import * as types from './xdp.types';

describe('xdp.reducer', () => {
  it('should handle GET_XDPS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_XDPS_FULFILLED,
      payload: buildAxiosServerResponse(mockXdps),
    })).toEqual({
      entities: mockXdps,
    });
  });
});
