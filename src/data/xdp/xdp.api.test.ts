import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockXdps } from '__test__/mockXdps';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './xdp.api';
import { API_PATH_XDPS } from './xdp.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('xdp.api', () => {
  it('should request the list for xdps', async () => {
    const response = buildAxiosServerResponse(mockXdps);
    axiosMock.onGet(`${API_PATH_XDPS}/0`).reply(200, response);
    await expect(api.getXdps('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_XDPS}/0` },
      status: 200,
    });
  });
});
