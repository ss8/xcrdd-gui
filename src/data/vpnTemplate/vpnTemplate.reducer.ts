import {
  GET_VPN_TEMPLATES_FULFILLED,
  VpnConfigTemplateActionTypes,
  VpnConfigTemplateState,
} from './vpnTemplate.types';

const initialState: VpnConfigTemplateState = {
  entities: [],
};

export default function vpnConfigTemplateReducer(
  state = initialState,
  action: VpnConfigTemplateActionTypes,
): VpnConfigTemplateState {
  switch (action.type) {
    case GET_VPN_TEMPLATES_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
