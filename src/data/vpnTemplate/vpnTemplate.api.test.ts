import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './vpnTemplate.api';
import { API_PATH_VPN_TEMPLATES } from './vpnTemplate.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('vpnTemplate.api', () => {
  it('should request the list for vpn Template', async () => {
    const response = buildAxiosServerResponse(mockFtpInfos);
    axiosMock.onGet(`${API_PATH_VPN_TEMPLATES}/0`).reply(200, response);
    await expect(api.getVpnConfigTemplates('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_VPN_TEMPLATES}/0` },
      status: 200,
    });
  });
});
