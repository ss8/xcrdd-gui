import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockVpnTemplates } from '__test__/mockVpnTemplates';
import {
  getVpnConfigTemplates,
} from './vpnTemplate.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './vpnTemplate.types';

describe('vpnTemplate.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of vpn Templates', () => {
    const action: types.GetVpnConfigTemplateFulfilledAction = {
      type: types.GET_VPN_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockVpnTemplates),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getVpnConfigTemplates(store.getState())).toEqual(mockVpnTemplates);
  });
});
