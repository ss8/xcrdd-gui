import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './vpnTemplate.actions';
import * as types from './vpnTemplate.types';
import * as api from './vpnTemplate.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('vpnTemplate.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the ftpInfos', () => {
    const expectedAction = {
      type: types.GET_VPN_TEMPLATES,
      payload: api.getVpnConfigTemplates('0'),
    };

    expect(actions.GetVpnConfigTemplates('0')).toEqual(expectedAction);
  });
});
