import { mockVpnTemplates } from '__test__/mockVpnTemplates';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './vpnTemplate.reducer';
import * as types from './vpnTemplate.types';

describe('vpnTemplate.reducer', () => {
  it('should handle GET_VPN_TEMPLATES_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_VPN_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockVpnTemplates),
    })).toEqual({
      entities: mockVpnTemplates,
    });
  });
});
