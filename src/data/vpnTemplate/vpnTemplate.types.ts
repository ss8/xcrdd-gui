import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import {
  VpnAuthentication,
  VpnEncryption,
  VpnOakley,
  VpnProtocol,
} from 'data/vpnConfig/vpnConfig.types';

export const API_PATH_VPN_TEMPLATES = '/vpn-templates';

export const GET_VPN_TEMPLATES = 'GET_VPN_TEMPLATES';
export const GET_VPN_TEMPLATES_PENDING = 'GET_VPN_TEMPLATES_PENDING';
export const GET_VPN_TEMPLATES_FULFILLED = 'GET_VPN_TEMPLATES_FULFILLED';
export const GET_VPN_TEMPLATES_REJECTED = 'GET_VPN_TEMPLATES_REJECTED';

export interface VpnConfigTemplate {
  id: string
  name: string
  afType: string
  role: 'MF' | 'DF' | 'IPSEC' | 'IPSECTRANS'
  p1Encr: VpnEncryption
  p1Auth: VpnAuthentication
  p1Oakley: VpnOakley
  encryption: VpnEncryption
  authentication: VpnAuthentication
  p2Oakley: VpnOakley
  p2LifetimeSecs: string
  key: string
  ikeVer: 'IKEv1' | 'IKEv2'
  keyingTries: string | 'forever'
  leftProtoPort: string
  rightProtoPort: string
  leftProtocol: VpnProtocol
  rightProtocol: VpnProtocol
  pfs: boolean
  rekeyMarginSecs: string
  rekey: boolean
  p1LifetimeSecs: string
  p1Mode: 'MAIN' | 'AGGRESSIVE'
}

export interface VpnConfigTemplateState {
  entities: VpnConfigTemplate[]
}

export interface GetVpnConfigTemplateAction extends AnyAction {
  type: typeof GET_VPN_TEMPLATES
  payload: AxiosPromise<ServerResponse<VpnConfigTemplate>>
}

export interface GetVpnConfigTemplatePendingAction extends AnyAction {
  type: typeof GET_VPN_TEMPLATES_PENDING
  payload: AxiosResponse<ServerResponse<VpnConfigTemplate>>
}
export interface GetVpnConfigTemplateFulfilledAction extends AnyAction {
  type: typeof GET_VPN_TEMPLATES_FULFILLED
  payload: AxiosResponse<ServerResponse<VpnConfigTemplate>>
}

export interface GetVpnConfigTemplateRejectedAction extends AnyAction {
  type: typeof GET_VPN_TEMPLATES_REJECTED
  payload: AxiosResponse<ServerResponse<VpnConfigTemplate>>
}

export type VpnConfigTemplateActionTypes =
  GetVpnConfigTemplateAction |
  GetVpnConfigTemplatePendingAction |
  GetVpnConfigTemplateFulfilledAction |
  GetVpnConfigTemplateRejectedAction;
