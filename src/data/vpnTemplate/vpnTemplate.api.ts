import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  VpnConfigTemplate,
  API_PATH_VPN_TEMPLATES,
} from './vpnTemplate.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getVpnConfigTemplates(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<VpnConfigTemplate>> {
  return http.get(`${API_PATH_VPN_TEMPLATES}/${deliveryFunctionId}`);
}
