import { RootState } from '../root.reducers';
import { VpnConfigTemplate } from './vpnTemplate.types';

// eslint-disable-next-line import/prefer-default-export
export const getVpnConfigTemplates = (state: RootState): VpnConfigTemplate[] => {
  return state.vpnConfigTemplate.entities;
};
