import * as api from './vpnTemplate.api';
import { GET_VPN_TEMPLATES, GetVpnConfigTemplateAction } from './vpnTemplate.types';

// eslint-disable-next-line import/prefer-default-export
export function GetVpnConfigTemplates(deliveryFunctionId: string): GetVpnConfigTemplateAction {
  return {
    type: GET_VPN_TEMPLATES,
    payload: api.getVpnConfigTemplates(deliveryFunctionId),
  };
}
