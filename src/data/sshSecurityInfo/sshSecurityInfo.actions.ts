import * as api from './sshSecurityInfo.api';
import { GET_SSH_SECURITY_INFOS, GetSshSecurityInfoAction } from './sshSecurityInfo.types';

// eslint-disable-next-line import/prefer-default-export
export function getSshSecurityInfos(deliveryFunctionId: string): GetSshSecurityInfoAction {
  return {
    type: GET_SSH_SECURITY_INFOS,
    payload: api.getSshSecurityInfos(deliveryFunctionId),
  };
}
