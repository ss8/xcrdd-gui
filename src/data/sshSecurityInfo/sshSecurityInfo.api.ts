import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  SshSecurityInfo,
  API_PATH_SSH_SECURITY_INFOS,
} from './sshSecurityInfo.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getSshSecurityInfos(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<SshSecurityInfo>> {
  return http.get(`${API_PATH_SSH_SECURITY_INFOS}/${deliveryFunctionId}`);
}
