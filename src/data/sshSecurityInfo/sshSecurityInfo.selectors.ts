import { RootState } from '../root.reducers';
import { SshSecurityInfo } from './sshSecurityInfo.types';

// eslint-disable-next-line import/prefer-default-export
export const getSshSecurityInfos = (state: RootState): SshSecurityInfo[] => state.sshSecurityInfo.entities;
