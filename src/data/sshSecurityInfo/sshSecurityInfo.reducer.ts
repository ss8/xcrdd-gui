import {
  GET_SSH_SECURITY_INFOS_FULFILLED,
  SshSecurityInfoActionTypes,
  SshSecurityInfoState,
} from './sshSecurityInfo.types';

const initialState: SshSecurityInfoState = {
  entities: [],
};

export default function sshSecurityInfoReducer(
  state = initialState,
  action: SshSecurityInfoActionTypes,
): SshSecurityInfoState {
  switch (action.type) {
    case GET_SSH_SECURITY_INFOS_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    default:
      return state;
  }
}
