import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './sshSecurityInfo.actions';
import * as types from './sshSecurityInfo.types';
import * as api from './sshSecurityInfo.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('sshSecurityInfo.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the sshSecurityInfos', () => {
    const expectedAction = {
      type: types.GET_SSH_SECURITY_INFOS,
      payload: api.getSshSecurityInfos('0'),
    };

    expect(actions.getSshSecurityInfos('0')).toEqual(expectedAction);
  });
});
