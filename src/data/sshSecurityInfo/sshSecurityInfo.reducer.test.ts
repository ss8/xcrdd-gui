import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './sshSecurityInfo.reducer';
import * as types from './sshSecurityInfo.types';

describe('sshSecurityInfo.reducer', () => {
  it('should handle GET_SSH_SECURITY_INFOS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_SSH_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSshSecurityInfos),
    })).toEqual({
      entities: mockSshSecurityInfos,
    });
  });
});
