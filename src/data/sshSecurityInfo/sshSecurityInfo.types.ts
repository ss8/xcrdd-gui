import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_SSH_SECURITY_INFOS = '/ssh-infos';

export const GET_SSH_SECURITY_INFOS = 'GET_SSH_SECURITY_INFOS';
export const GET_SSH_SECURITY_INFOS_FULFILLED = 'GET_SSH_SECURITY_INFOS_FULFILLED';
export const GET_SSH_SECURITY_INFOS_REJECTED = 'GET_SSH_SECURITY_INFOS_REJECTED';

export interface SshSecurityInfo {
  id: string
  sshType: 'CLIENT' | 'SERVER'
  authType: 'PASSWORD' | 'SSHKEYS'
  userName: string
  password: string
  privKeyFile: string
  passphrase: string
  pubKeyFile: string
  sshDir: string
  hostKeyType: 'RSA' | 'DSA' | 'ECDSA' | 'ED25519'
}

export interface SshSecurityInfoState {
  entities: SshSecurityInfo[]
}

export interface GetSshSecurityInfoAction extends AnyAction {
  type: typeof GET_SSH_SECURITY_INFOS
  payload: AxiosPromise<ServerResponse<SshSecurityInfo>>
}

export interface GetSshSecurityInfoFulfilledAction extends AnyAction {
  type: typeof GET_SSH_SECURITY_INFOS_FULFILLED
  payload: AxiosResponse<ServerResponse<SshSecurityInfo>>
}

export type SshSecurityInfoActionTypes =
  GetSshSecurityInfoAction |
  GetSshSecurityInfoFulfilledAction;
