import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './sshSecurityInfo.api';
import { API_PATH_SSH_SECURITY_INFOS } from './sshSecurityInfo.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('sshSecurityInfo.api', () => {
  it('should request the list for sshSecurityInfos', async () => {
    const response = buildAxiosServerResponse(mockSshSecurityInfos);
    axiosMock.onGet(`${API_PATH_SSH_SECURITY_INFOS}/0`).reply(200, response);
    await expect(api.getSshSecurityInfos('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_SSH_SECURITY_INFOS}/0` },
      status: 200,
    });
  });
});
