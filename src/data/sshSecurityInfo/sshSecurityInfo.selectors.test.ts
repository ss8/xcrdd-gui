import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import {
  getSshSecurityInfos,
} from './sshSecurityInfo.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './sshSecurityInfo.types';

describe('sshSecurityInfo.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of sshSecurityInfos', () => {
    const action: types.SshSecurityInfoActionTypes = {
      type: types.GET_SSH_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSshSecurityInfos),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSshSecurityInfos(store.getState())).toEqual(mockSshSecurityInfos);
  });
});
