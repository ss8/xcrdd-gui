import {
  GET_SECURITY_INFOS_FULFILLED,
  SecurityInfoActionTypes,
  SecurityInfoState,
} from './securityInfo.types';

const initialState: SecurityInfoState = {
  entities: [],
};

export default function securityInfoReducer(
  state = initialState,
  action: SecurityInfoActionTypes,
): SecurityInfoState {
  switch (action.type) {
    case GET_SECURITY_INFOS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
