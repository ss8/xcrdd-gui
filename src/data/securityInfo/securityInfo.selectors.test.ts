import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getSecurityInfos, getSecurityInfosFilteredByTlsAndDtls,
} from './securityInfo.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './securityInfo.types';

describe('securityInfo.selectors', () => {
  let securityInfos: types.SecurityInfo[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    securityInfos = [{
      id: 'VExTMTJfQ0xJRU5U',
      name: 'TLS12_CLIENT',
      intfType: 'TLS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Client',
      authenType: 'MUTUAL',
      privateKeyFile: 'server.key',
      ownCertFile: 'server.crt',
      certAuthority: 'root_cacert.pem',
      certDirectory: '/SS8/Keys_and_Certs',
      description: '',
    }, {
      id: 'RFRMU1NlcnZlcg',
      name: 'DTLSServer',
      intfType: 'DTLS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Server',
      authenType: 'SIMPLE',
      privateKeyFile: 'server.key',
      ownCertFile: 'server.crt',
      certAuthority: 'cacert.pem',
      certDirectory: '/SS8/XCIPIO/8.0.0/RUN/config/CERT/',
      description: '',
    },
    {
      id: 'SFRUUFM',
      name: 'HTTPS',
      intfType: 'HTTPS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Client',
      authenType: 'MUTUAL',
      privateKeyFile: 'client.key',
      ownCertFile: 'client.crt',
      certAuthority: 'ca.crt',
      certDirectory: '/usr/local/exec/cert',
      description: '',
    }];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of securityInfos', () => {
    const action: types.SecurityInfoActionTypes = {
      type: types.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(securityInfos),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSecurityInfos(store.getState())).toEqual(securityInfos);
  });

  it('should return the TLS and DTLS list of securityInfos', () => {
    const action: types.SecurityInfoActionTypes = {
      type: types.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(securityInfos),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSecurityInfosFilteredByTlsAndDtls(store.getState())).toEqual([
      securityInfos[0],
      securityInfos[1],
    ]);
  });
});
