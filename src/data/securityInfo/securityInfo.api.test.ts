import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './securityInfo.api';
import {
  SecurityInfo,
} from './securityInfo.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('securityInfo.api', () => {
  let securityInfo: SecurityInfo;

  beforeEach(() => {
    securityInfo = {
      id: 'VExTMTJfQ0xJRU5U',
      name: 'TLS12_CLIENT',
      intfType: 'TLS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Client',
      authenType: 'MUTUAL',
      privateKeyFile: 'server.key',
      ownCertFile: 'server.crt',
      certAuthority: 'root_cacert.pem',
      certDirectory: '/SS8/Keys_and_Certs',
      description: '',
    };
  });

  it('should request the list for securityInfos', async () => {
    const response = buildAxiosServerResponse([securityInfo]);
    axiosMock.onGet('/securityInfos/0').reply(200, response);
    await expect(api.getSecurityInfos('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/securityInfos/0' },
      status: 200,
    });
  });
});
