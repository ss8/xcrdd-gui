import { createSelector } from 'reselect';
import { RootState } from '../root.reducers';
import { SecurityInfo } from './securityInfo.types';

// eslint-disable-next-line import/prefer-default-export
export const getSecurityInfos = (state: RootState): SecurityInfo[] => state.securityInfo.entities;

export const getSecurityInfosFilteredByTlsAndDtls = createSelector(
  [getSecurityInfos],
  (securityInfos) => securityInfos?.filter(({ intfType }) => intfType === 'TLS' || intfType === 'DTLS') ?? [],
);
