import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  SecurityInfo,
  API_PATH_SECURITY_INFOS,
} from './securityInfo.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getSecurityInfos(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<SecurityInfo>> {
  return http.get(`${API_PATH_SECURITY_INFOS}/${deliveryFunctionId}`);
}
