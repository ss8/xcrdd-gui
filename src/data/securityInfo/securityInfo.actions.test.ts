import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './securityInfo.actions';
import * as types from './securityInfo.types';
import * as api from './securityInfo.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('securityInfo.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the securityInfos', () => {
    const expectedAction = {
      type: types.GET_SECURITY_INFOS,
      payload: api.getSecurityInfos('0'),
    };

    expect(actions.getSecurityInfos('0')).toEqual(expectedAction);
  });
});
