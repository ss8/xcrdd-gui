import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './securityInfo.reducer';
import * as types from './securityInfo.types';

describe('securityInfo.reducer', () => {
  let securityInfos: types.SecurityInfo[];

  beforeEach(() => {
    securityInfos = [{
      id: 'VExTMTJfQ0xJRU5U',
      name: 'TLS12_CLIENT',
      intfType: 'TLS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Client',
      authenType: 'MUTUAL',
      privateKeyFile: 'server.key',
      ownCertFile: 'server.crt',
      certAuthority: 'root_cacert.pem',
      certDirectory: '/SS8/Keys_and_Certs',
      description: '',
    }];
  });

  it('should handle GET_SECURITY_INFOS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(securityInfos),
    })).toEqual({
      entities: securityInfos,
    });
  });
});
