import * as api from './securityInfo.api';
import { GET_SECURITY_INFOS, GetSecurityInfoAction } from './securityInfo.types';

// eslint-disable-next-line import/prefer-default-export
export function getSecurityInfos(deliveryFunctionId: string): GetSecurityInfoAction {
  return {
    type: GET_SECURITY_INFOS,
    payload: api.getSecurityInfos(deliveryFunctionId),
  };
}
