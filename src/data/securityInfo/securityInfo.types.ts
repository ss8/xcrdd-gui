import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_SECURITY_INFOS = '/securityInfos';

export const GET_SECURITY_INFOS = 'GET_SECURITY_INFOS';
export const GET_SECURITY_INFOS_FULFILLED = 'GET_SECURITY_INFOS_FULFILLED';
export const GET_SECURITY_INFOS_REJECTED = 'GET_SECURITY_INFOS_REJECTED';

export const ACCESS_FUNCTION_SECURITY_INFO_FILTER: SecurityInfoFilterByAccessFunctionAndInterface = {
  ERK_SMF: {
    provisioningInterfaces: [
      { field: 'intfType', value: 'TLS' },
      { field: 'tlsType', value: 'Client' },
      { field: 'tlsVersion', value: 'TLSv1.2' },
    ],
  },
  MV_SMSIWF: {
    provisioningInterfaces: [
      { field: 'intfType', value: 'TLS' },
      { field: 'tlsType', value: 'Client' },
      { field: 'tlsVersion', value: 'TLSv1.2' },
    ],
    dataInterfaces: [
      { field: 'intfType', value: 'TLS' },
      { field: 'tlsType', value: 'Server' },
      { field: 'tlsVersion', value: 'TLSv1.2' },
    ],
  },
};

export interface SecurityInfo {
  id: string
  name: string
  intfType: 'HTTPS' | 'FTPS' | 'TLS' | 'DTLS'
  tlsVersion: 'NONE' | 'SSLv2' | 'SSLv3' | 'TLSv1' | 'TLSv1.1' | 'TLSv1.2' | 'TLSv1.3' | 'DTLSv1' | 'DTLSv1.2' | 'DTLSv1.3'
  tlsType: 'Client' | 'Server'
  authenType: 'SIMPLE' | 'MUTUAL'
  privateKeyFile: string
  ownCertFile: string
  certAuthority: string
  certDirectory: string
  description: string
}

export type SecurityInfoFilterByAccessFunctionAndInterface = {
  [accessFunctionTag: string]: SecurityInfoFilterByInterface
}

export type SecurityInfoFilterByInterface = {
  [accessFunctionInterface: string]: SecurityInfoFilter[]
}

export type SecurityInfoFilter = {
  field: keyof SecurityInfo,
  value: string
}

export interface SecurityInfoState {
  entities: SecurityInfo[]
}

export interface GetSecurityInfoAction extends AnyAction {
  type: typeof GET_SECURITY_INFOS
  payload: AxiosPromise<ServerResponse<SecurityInfo>>
}

export interface GetSecurityInfoFulfilledAction extends AnyAction {
  type: typeof GET_SECURITY_INFOS_FULFILLED
  payload: AxiosResponse<ServerResponse<SecurityInfo>>
}

export type SecurityInfoActionTypes =
  GetSecurityInfoAction |
  GetSecurityInfoFulfilledAction;
