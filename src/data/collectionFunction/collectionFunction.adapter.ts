import cloneDeep from 'lodash.clonedeep';
import omitBy from 'lodash.omitby';
import isNil from 'lodash.isnil';
import isEqual from 'lodash.isequal';
import { Contact, Operation } from 'data/types';
import {
  CollectionFunction,
  CollectionFunctionFormData,
  CollectionFunctionInterface,
  CollectionFunctionInterfaceFormData,
  COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID,
} from './collectionFunction.types';

let nextId = 1;

const YES_NO_FIELDS = [
  'tpkt',
  'useIpdu',
  'useHi3',
];

type KeyValueString = {
  key: string
  value: string
}

type KeyValueUnknown = {
  key: string
  value: unknown
}

const ANY_ADDRESSES = ['ANY_ADDR', 'ADDR_ANY'];
const ENCRYPTED_PLACEHOLDER = '*****';

export function adaptToForm(collectionFunction: CollectionFunction): CollectionFunctionFormData {
  const clonedCollectionFunction = cloneDeep(collectionFunction);

  if (clonedCollectionFunction.tag === 'CF-ATIS-0700005') {
    clonedCollectionFunction.tag = 'CF-ATIS-1000678';
  }

  let collectionFunctionForm: CollectionFunctionFormData = {
    ...clonedCollectionFunction,
    deliveryThru: adaptDeliveryThruToForm(clonedCollectionFunction),
    contact: adaptContactToForm(clonedCollectionFunction.contact),
    cfGroup: adaptCfGroupToForm(clonedCollectionFunction.cfGroup),
    hi2Interfaces: adaptInterfaceToForm(clonedCollectionFunction.hi2Interfaces),
    hi3Interfaces: adaptInterfaceToForm(clonedCollectionFunction.hi3Interfaces),
  };

  if (collectionFunctionForm.tag === 'CF-ETSI-102232') {
    collectionFunctionForm = adaptDestinationTypeToForm(collectionFunctionForm);
    collectionFunctionForm = adaptMdfToForm(collectionFunctionForm);
  }

  return omitBy<CollectionFunctionFormData>(collectionFunctionForm, isNil) as CollectionFunctionFormData;
}

export function adaptToCopy(collectionFunctionForm: CollectionFunctionFormData): CollectionFunctionFormData {
  const clonedCollectionFunction = cloneDeep(collectionFunctionForm);
  clonedCollectionFunction.name = `Copy of ${clonedCollectionFunction.name}`;
  clonedCollectionFunction.id = '';

  const collectionFunctionToCopy: CollectionFunctionFormData = {
    ...clonedCollectionFunction,
    hi2Interfaces: adaptInterfaceToCopy(clonedCollectionFunction.hi2Interfaces),
    hi3Interfaces: adaptInterfaceToCopy(clonedCollectionFunction.hi3Interfaces),
  };

  return omitBy<CollectionFunctionFormData>(collectionFunctionToCopy, isNil) as CollectionFunctionFormData;
}

export function adaptToSubmit(
  collectionFunctionForm: CollectionFunctionFormData,
): CollectionFunction {
  let clonedCollectionFunctionForm = cloneDeep(collectionFunctionForm);

  if (clonedCollectionFunctionForm.tag === 'CF-ETSI-102232') {
    clonedCollectionFunctionForm = adaptETSICollectionFunctionToSubmit(clonedCollectionFunctionForm);
  }

  const {
    deliveryThru,
    ...remainingCollectionFunctionForm
  } = clonedCollectionFunctionForm;

  const collectionFunction: CollectionFunction = {
    ...remainingCollectionFunctionForm,
    contact: adaptContactToSubmit(clonedCollectionFunctionForm.contact),
    cfGroup: adaptCfGroupToSubmit(clonedCollectionFunctionForm.cfGroup),
    hi2Interfaces: adaptInterfaceToSubmit(clonedCollectionFunctionForm.hi2Interfaces),
    hi3Interfaces: adaptInterfaceToSubmit(clonedCollectionFunctionForm.hi3Interfaces),
  };

  // Remove any undefined entry
  return omitBy<CollectionFunction>(collectionFunction, isNil) as CollectionFunction;
}

export function setCollectionFunctionOperationValues(
  collectionFunction: CollectionFunction,
  originalCollectionFunction: CollectionFunction | null = null,
  operation: Operation = 'CREATE',
): CollectionFunction {
  const clonedCollectionFunction = cloneDeep(collectionFunction);

  if (operation !== 'CREATE') {
    clonedCollectionFunction.operation = operation;
  }

  setInterfacesOperation(clonedCollectionFunction, originalCollectionFunction);

  return clonedCollectionFunction;
}

function setInterfacesOperation(
  collectionFunction: CollectionFunction,
  originalCollectionFunction: CollectionFunction | null = null,
): void {
  setInterfaceOperationValueBasedInOriginal(
    collectionFunction.hi2Interfaces,
    originalCollectionFunction?.hi2Interfaces,
  );
  setInterfaceOperationValueBasedInOriginal(
    collectionFunction.hi3Interfaces,
    originalCollectionFunction?.hi3Interfaces,
  );
}

function setInterfaceOperationValueBasedInOriginal(
  interfaces: CollectionFunctionInterface[] | undefined,
  originalInterfaces: CollectionFunctionInterface[] | undefined,
) {
  // Check which interfaces will be marked as CREATE, UPDATE or NO_OPERATION
  interfaces?.forEach((interfaceEntry) => {
    const originalInterfaceEntry = originalInterfaces?.find(({ id }) => id === interfaceEntry.id);
    if (originalInterfaceEntry == null) {
      interfaceEntry.operation = 'CREATE';
    } else if (!isEqual(interfaceEntry, originalInterfaceEntry)) {
      interfaceEntry.operation = 'UPDATE';
    } else {
      interfaceEntry.operation = 'NO_OPERATION';
    }
  });

  // Check which interfaces will be marked as DELETE
  const interfacesToDelete =
    originalInterfaces?.filter((originalInterfaceEntry) => {
      return !interfaces?.find(({ id }) => id === originalInterfaceEntry.id);
    })
      .map((originalInterfaceEntry) => ({
        ...originalInterfaceEntry,
        operation: 'DELETE',
      }));

  if (interfacesToDelete) {
    interfaces?.push(...(interfacesToDelete as CollectionFunctionInterface[]));
  }
}

function adaptDeliveryThruToForm(
  collectionFunction: CollectionFunction,
): 'TCP_OR_FTP' | 'AF_DELIVERY' {
  // TODO
  return 'TCP_OR_FTP';
}

function adaptContactToForm(
  contact: Contact | undefined,
): string | undefined {
  if (!contact) {
    return '';
  }

  return contact.id;
}

function adaptCfGroupToForm(
  cfGroup: { id: string, name: string } | undefined,
): string | undefined {
  if (!cfGroup) {
    return;
  }

  return cfGroup.id;
}

function adaptInterfaceToForm(
  collectionFunctionInterfaces: CollectionFunctionInterface[] | undefined,
): CollectionFunctionInterfaceFormData[] {
  if (!collectionFunctionInterfaces) {
    return [];
  }

  return collectionFunctionInterfaces.map(({
    id,
    name,
    transportType,
    mdf2NodeName,
    mdf3NodeName,
    options,
  }) => ({
    id,
    name,
    transportType,
    mdf2NodeName,
    mdf3NodeName,
    ...options?.map(convertYNValuesToBoolean)
      .map(convertStringEncTypeToBoolean)
      .map(convertDefaultAnyAddressToEmptyString)
      .reduce((previousValue, currentValue) => ({
        ...previousValue,
        [currentValue.key]: currentValue.value,
      }), {}),
  }));
}

function adaptInterfaceToCopy(
  collectionFunctionInterfaceFormData: CollectionFunctionInterfaceFormData[] | undefined,
): CollectionFunctionInterfaceFormData[] | undefined {

  if (!collectionFunctionInterfaceFormData) {
    return [];
  }

  return collectionFunctionInterfaceFormData.map(({ name, ...collectionFunctionInterface }) => {

    if (collectionFunctionInterface.destPort != null) {
      collectionFunctionInterface.destPort = '';
    }

    nextId += 1;

    return {
      ...collectionFunctionInterface,
      id: `${nextId}.${COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`,
    } as CollectionFunctionInterfaceFormData;

  });
}

function adaptContactToSubmit(
  contact: string | undefined,
): Contact | undefined {
  if (!contact) {
    return {
      id: '',
      name: '',
    };
  }

  return {
    id: contact,
    name: atob(contact),
  };
}

function adaptCfGroupToSubmit(
  cfGroup: string | undefined,
): { id: string, name: string } | undefined {
  if (!cfGroup) {
    return;
  }

  return {
    id: cfGroup,
    name: '',
  };
}

function adaptInterfaceToSubmit(
  collectionFunctionInterfacesForm: CollectionFunctionInterfaceFormData[] | undefined,
): CollectionFunctionInterface[] {
  if (!collectionFunctionInterfacesForm) {
    return [];
  }

  return collectionFunctionInterfacesForm.map((entry) => {
    let { id } = entry;

    const {
      name,
      id: _id,
      transportType,
      mdf2NodeName,
      mdf3NodeName,
      ...everythingElse
    } = entry;

    if (id.includes(COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID)) {
      id = '';
    }

    // Convert everything else to options and remove the null/undefined ones
    const options = Object.entries(everythingElse)
      .map(([key, value]) => ({ key, value }))
      .map(convertBooleanValuesToYN)
      .map(convertBooleanEncTypeToString)
      .filter(({ value }) => value != null);

    const collectionFunctionInterface: CollectionFunctionInterface = {
      name,
      id,
      transportType,
      options,
      mdf2NodeName,
      mdf3NodeName,
    };

    // Remove any undefined entry
    return omitBy<CollectionFunctionInterface>(collectionFunctionInterface, isNil) as CollectionFunctionInterface;
  });
}

function convertYNValuesToBoolean({ key, value }: KeyValueUnknown): KeyValueUnknown {
  if (!YES_NO_FIELDS.includes(key)) {
    return { key, value };
  }

  return { key, value: value === 'Y' };
}

function convertBooleanValuesToYN({ key, value }: KeyValueUnknown): KeyValueString {
  if (!YES_NO_FIELDS.includes(key)) {
    return { key, value: value as string };
  }

  if (value === true) {
    return { key, value: 'Y' };
  }

  return { key, value: 'N' };
}

function convertStringEncTypeToBoolean({ key, value }: KeyValueUnknown): KeyValueUnknown {
  if (key !== 'pldEncType') {
    return { key, value };
  }

  return { key, value: value !== 'NONE' };
}

function convertBooleanEncTypeToString({ key, value }: KeyValueUnknown): KeyValueString {
  if (key !== 'pldEncType') {
    return { key, value: value as string };
  }

  if (value === true) {
    return { key, value: 'AES-192-CBC' };
  }

  return { key, value: 'NONE' };
}

function convertDefaultAnyAddressToEmptyString({ key, value }: KeyValueUnknown): KeyValueUnknown {
  if (key === 'ownIPAddr' && ANY_ADDRESSES.includes(value as string)) {
    return { key, value: '' };
  }

  return { key, value };
}

function adaptDestinationTypeToForm(
  collectionFunctionForm: CollectionFunctionFormData,
): CollectionFunctionFormData {
  return {
    ...collectionFunctionForm,
    hi3Interfaces: adaptInterfaceDestinationTypeToForm(collectionFunctionForm.hi3Interfaces),
  };
}

function adaptInterfaceDestinationTypeToForm(
  collectionFunctionInterfaces: CollectionFunctionInterfaceFormData[] | undefined,
): CollectionFunctionInterfaceFormData[] {
  if (!collectionFunctionInterfaces) {
    return [];
  }

  return collectionFunctionInterfaces.map((collectionFunctionInterface) => {
    const {
      fsid,
    } = collectionFunctionInterface;

    let destType: 'ipAddress' | 'domainGroup' = 'ipAddress';
    if (fsid != null && fsid !== '') {
      destType = 'domainGroup';
    }

    return {
      ...collectionFunctionInterface,
      destType,
    };
  });
}

function adaptMdfToForm(
  collectionFunctionForm: CollectionFunctionFormData,
): CollectionFunctionFormData {
  const { mdf2NodeName, mdf3NodeName } = collectionFunctionForm;
  collectionFunctionForm?.hi2Interfaces?.forEach((collectionFunctionInterface) => {
    collectionFunctionInterface.mdf2NodeName = mdf2NodeName;
  });

  collectionFunctionForm?.hi3Interfaces?.forEach((collectionFunctionInterface) => {
    collectionFunctionInterface.mdf3NodeName = mdf3NodeName;
  });

  return collectionFunctionForm;
}

function adaptETSICollectionFunctionToSubmit(
  collectionFunctionForm: CollectionFunctionFormData,
): CollectionFunctionFormData {
  return {
    ...collectionFunctionForm,
    hi2Interfaces: adaptETSIInterfaceToSubmit(collectionFunctionForm.hi2Interfaces),
    hi3Interfaces: adaptETSIInterfaceToSubmit(collectionFunctionForm.hi3Interfaces),
  };
}

function adaptETSIInterfaceToSubmit(
  collectionFunctionInterfaces: CollectionFunctionInterfaceFormData[] | undefined,
): CollectionFunctionInterfaceFormData[] {
  if (!collectionFunctionInterfaces) {
    return [];
  }

  return collectionFunctionInterfaces.map((collectionFunctionInterface) => {
    const {
      destType,
      destIPAddr,
      fsid,
      pldEncKey,
      ...remainingCollectionFunctionInterface
    } = collectionFunctionInterface;

    let interfaceData: CollectionFunctionInterfaceFormData = {
      ...remainingCollectionFunctionInterface,
    };

    if (destType != null) {
      if (destType === 'ipAddress') {
        interfaceData = {
          ...interfaceData,
          destIPAddr,
        };

      } else if (destType === 'domainGroup') {
        interfaceData = {
          ...interfaceData,
          destIPAddr: '',
          fsid,
        };
      }

    } else {
      interfaceData = {
        destIPAddr,
        ...interfaceData,
      };
    }

    if (pldEncKey !== ENCRYPTED_PLACEHOLDER && pldEncKey !== '') {
      interfaceData = {
        ...interfaceData,
        pldEncKey,
      };
    }

    return interfaceData;
  });
}
