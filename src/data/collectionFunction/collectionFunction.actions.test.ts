import thunk from 'redux-thunk';
import { History } from 'history';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import { WithAuditDetails } from 'data/types';
import Http from 'shared/http';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import * as actions from './collectionFunction.actions';
import * as types from './collectionFunction.types';
import * as api from './collectionFunction.api';
import { CF_ROUTE } from '../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const mockStore = configureMockStore([thunk]);

describe('collectionFunction.actions', () => {
  let collectionFunction: types.CollectionFunction;
  let collectionFunctionWithAudit: WithAuditDetails<types.CollectionFunction>;
  let history: History;
  let collectionFunctionBody: WithAuditDetails<void>;
  let store: MockStoreEnhanced<unknown>;

  beforeEach(() => {
    [collectionFunction] = mockCollectionFunctions;

    collectionFunctionWithAudit = {
      data: collectionFunction,
    };

    collectionFunctionBody = {
      data: undefined,
    };

    history = {
      location: {
        pathname: '',
        hash: '',
        search: '',
        state: '',
      },
      length: 1,
      action: 'PUSH',
      listen: jest.fn(),
      go: jest.fn(),
      push: jest.fn(),
      block: jest.fn(),
      replace: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      createHref: jest.fn(),
    };

    store = mockStore({
      cfs: {
        cfs: [],
        cfGroups: [],
        single: null,
        selected: [],
      },
    });

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to go to the collection function list', () => {
    actions.goToCollectionFunctionList(history)(store.dispatch);
    expect(history.push).toBeCalledWith(`${types.API_PATH_COLLECTION_FUNCTION}`);
  });

  it('should create an action to go to the collection function create', () => {
    actions.goToCollectionFunctionCreate(history)(store.dispatch);
    expect(history.push).toBeCalledWith(`${CF_ROUTE}/create`);
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_COLLECTION_FUNCTION_CREATE,
    }]);
  });

  it('should create an action to go to the collection function edit', () => {
    actions.goToCollectionFunctionEdit(history)(store.dispatch);
    expect(history.push).toBeCalledWith(`${CF_ROUTE}/edit`);
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_COLLECTION_FUNCTION_EDIT,
    }]);
  });

  it('should create an action to go to the collection function view', () => {
    actions.goToCollectionFunctionView(history)(store.dispatch);
    expect(history.push).toBeCalledWith(`${CF_ROUTE}/view`);
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_COLLECTION_FUNCTION_VIEW,
    }]);
  });

  it('should create an action to go to the collection function copy', () => {
    actions.goToCollectionFunctionCopy(history)(store.dispatch);
    expect(history.push).toBeCalledWith(`${CF_ROUTE}/copy`);
    expect(store.getActions()).toEqual([{
      type: types.GO_TO_COLLECTION_FUNCTION_COPY,
    }]);
  });

  it('should create an action to get the collection functions', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_LIST,
      payload: api.getCollectionFunctionList('0'),
    };

    expect(actions.fetchCollectionFunctionList('0')).toEqual(expectedAction);
  });

  it('should create an action to get the collection functions filtered', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_FILTER,
      payload: api.getCollectionFunctionListByFilter('0', 'some_filter'),
    };

    expect(actions.fetchCollectionFunctionListByFilter('0', 'some_filter')).toEqual(expectedAction);
  });

  it('should create an action to get the collection functions by ids', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_IDS,
      payload: api.getCollectionFunctionListByFilter('0', 'id1,id2'),
    };

    expect(actions.fetchCollectionFunctionListByIds('0', 'id1,id2')).toEqual(expectedAction);
  });

  it('should create an action to get the the collection function groups', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_GROUP_LIST,
      payload: api.getCollectionFunctionGroups('0'),
    };

    expect(actions.fetchCollectionFunctionGroupList('0')).toEqual(expectedAction);
  });

  it('should create an action to get the collection function', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION,
      payload: api.getCollectionFunction('0', 'id1'),
    };

    expect(actions.fetchCollectionFunction('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to create the collection function', () => {
    const expectedAction = [{
      type: types.CREATE_COLLECTION_FUNCTION,
      payload: api.createCollectionFunction('0', collectionFunctionWithAudit),
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_LIST,
    }];

    expect.assertions(1);
    return store.dispatch(actions.createCollectionFunction('0', collectionFunctionWithAudit, history) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to update the collection function', () => {
    const expectedAction = [{
      type: types.UPDATE_COLLECTION_FUNCTION,
      payload: api.updateCollectionFunction('0', 'id1', collectionFunctionWithAudit),
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_LIST,
    }];

    expect.assertions(1);
    return store.dispatch(actions.updateCollectionFunction('0', 'id1', collectionFunctionWithAudit, history) as any).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to delete the Collection Function', () => {

    const expectedAction = [
      {
        type: types.DELETE_COLLECTION_FUNCTION,
        payload: api.deleteCollectionFunction('0', 'id1', collectionFunctionBody),
      },
      {
        type: types.GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
        payload: api.getCollectionFunctionList('0'),
      },
    ];

    expect.assertions(1);
    return store.dispatch(actions.deleteCollectionFunction('0', 'id1', collectionFunctionBody) as any).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to get the collection function HI2', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST,
      payload: api.getCollectionFunctionHi2InterfaceList('0', 'id1'),
    };

    expect(actions.fetchCollectionFunctionHI2InterfaceList('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to get the collection function HI3', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST,
      payload: api.getCollectionFunctionHi3InterfaceList('0', 'id1'),
    };

    expect(actions.fetchCollectionFunctionHI3InterfaceList('0', 'id1')).toEqual(expectedAction);
  });

  it('should create an action to clear the collection function state', () => {
    const expectedAction = {
      type: types.CLEAR_COLLECTION_FUNCTION_STATE,
      payload: null,
    };

    expect(actions.clearCollectionFunctionState()).toEqual(expectedAction);
  });

  it('should create an action to select a collection function', () => {
    const expectedAction = {
      type: types.SELECT_COLLECTION_FUNCTION,
      payload: mockCollectionFunctions,
    };

    expect(actions.selectCollectionFunction(mockCollectionFunctions)).toEqual(expectedAction);
  });

  it('should create an action to get the collection functions (with view mode version)', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
      payload: api.getCollectionFunctionListWithViewOptions('0'),
    };

    expect(actions.fetchCollectionFunctionListViewOptions('0')).toEqual(expectedAction);
  });

  it('should create an action to get the collection functions (with view mode version)', () => {
    const expectedAction = {
      type: types.GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS,
      payload: api.getCollectionFunctionWithViewOptions('0', 'CF_ID'),
    };

    expect(actions.fetchCollectionFunctionWithViewOptions('0', 'CF_ID')).toEqual(expectedAction);
  });
});
