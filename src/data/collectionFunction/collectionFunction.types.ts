import {
  Contact, Identity, Operation, ServerResponse,
} from 'data/types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ThunkAction } from 'redux-thunk';
import { RootState } from 'data/root.reducers';

export const API_PATH_COLLECTION_FUNCTION = '/cfs';
export const API_PATH_COLLECTION_FUNCTION_GROUP = '/cf-groups';

export const GO_TO_COLLECTION_FUNCTION_LIST = 'GO_TO_COLLECTION_FUNCTION_LIST';
export const GO_TO_COLLECTION_FUNCTION_CREATE = 'GO_TO_COLLECTION_FUNCTION_CREATE';
export const GO_TO_COLLECTION_FUNCTION_EDIT = 'GO_TO_COLLECTION_FUNCTION_EDIT';
export const GO_TO_COLLECTION_FUNCTION_VIEW = 'GO_TO_COLLECTION_FUNCTION_VIEW';
export const GO_TO_COLLECTION_FUNCTION_COPY = 'GO_TO_COLLECTION_FUNCTION_COPY';
export const GET_COLLECTION_FUNCTION_LIST = 'GET_COLLECTION_FUNCTION_LIST';
export const GET_COLLECTION_FUNCTION_LIST_FULFILLED = 'GET_COLLECTION_FUNCTION_LIST_FULFILLED';
export const GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS = 'GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS';
export const GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED = 'GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED';
export const GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_PENDING = 'GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_PENDING';
export const GET_COLLECTION_FUNCTION_LIST_BY_FILTER = 'GET_COLLECTION_FUNCTION_LIST_BY_FILTER';
export const GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED = 'GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED';
export const GET_COLLECTION_FUNCTION_LIST_BY_IDS = 'GET_COLLECTION_FUNCTION_LIST_BY_IDS';
export const GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED = 'GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED';
export const GET_COLLECTION_FUNCTION = 'GET_COLLECTION_FUNCTION';
export const GET_COLLECTION_FUNCTION_PENDING = 'GET_COLLECTION_FUNCTION_PENDING';
export const GET_COLLECTION_FUNCTION_FULFILLED = 'GET_COLLECTION_FUNCTION_FULFILLED';
export const GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS = 'GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS';
export const GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED = 'GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED';
export const CREATE_COLLECTION_FUNCTION = 'CREATE_COLLECTION_FUNCTION';
export const CREATE_COLLECTION_FUNCTION_FULFILLED = 'CREATE_COLLECTION_FUNCTION_FULFILLED';
export const CREATE_COLLECTION_FUNCTION_REJECTED = 'CREATE_COLLECTION_FUNCTION_REJECTED';
export const UPDATE_COLLECTION_FUNCTION = 'UPDATE_COLLECTION_FUNCTION';
export const UPDATE_COLLECTION_FUNCTION_FULFILLED = 'UPDATE_COLLECTION_FUNCTION_FULFILLED';
export const UPDATE_COLLECTION_FUNCTION_REJECTED = 'UPDATE_COLLECTION_FUNCTION_REJECTED';
export const DELETE_COLLECTION_FUNCTION = 'DELETE_COLLECTION_FUNCTION';
export const DELETE_COLLECTION_FUNCTION_FULFILLED = 'DELETE_COLLECTION_FUNCTION_FULFILLED';
export const DELETE_COLLECTION_FUNCTION_REJECTED = 'DELETE_COLLECTION_FUNCTION_REJECTED';
export const GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST = 'GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST';
export const GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST_FULFILLED = 'GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST_FULFILLED';
export const GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST = 'GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST';
export const GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST_FULFILLED = 'GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST_FULFILLED';
export const GET_COLLECTION_FUNCTION_GROUP_LIST = 'GET_COLLECTION_FUNCTION_GROUP_LIST';
export const GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED = 'GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED';
export const CLEAR_COLLECTION_FUNCTION_STATE = 'CLEAR_COLLECTION_FUNCTION_STATE';
export const SELECT_COLLECTION_FUNCTION = 'SELECT_COLLECTION_FUNCTION';

export type CFTag = string;

export enum CollectionFunctionPrivileges {
  View = 'cf_view',
  Edit = 'cf_write',
  Delete = 'cf_delete',
}

export const COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID = 'collectionFunctionInterface';

export type CollectionFunctionTag =
  'CF-3GPP-33108' |
  'CF-ETSI-102232' |
  'CF-TIA-1072' |
  'CF-TIA-1066' |
  'CF-JSTD025A' |
  'CF-JSTD025B' |
  'CF-ATIS-1000678' |
  'CF-ATIS-0700005';

// Partial definition. It will be completed in the future
export type CollectionFunction = {
  id: string
  name: string
  tag: CollectionFunctionTag
  type: 'CALEA' | 'ETSIIP' | 'GPRS' | 'GPRSROSE' | 'T1.678' | 'TIA1072' | 'TIA1066' | 'T1IAS'
  services?: string[]
  afs?: string[]
  dxOwner?: string
  dxAccess?: string
  bufferCC?: boolean
  cellTowerLocation: boolean
  contact?: Contact
  deliveryType: 'DSR' | 'MAPPED' | 'FORWARD'
  insideNat: boolean
  timeZone: string
  cfGroup?: {
    id: string
    name: string
  }
  graceTimer?: string
  hi2Interfaces?: HI2Interface[]
  hi3Interfaces?: HI3Interface[]
  vpnConfig?: VpnConfig[]
  operation?: Operation
  mdf2NodeName?: string,
  mdf3NodeName?: string,
}

export interface CollectionFunctionFormData extends Omit<CollectionFunction, 'cfGroup' | 'contact' | 'hi2Interface' | 'hi3Interface'> {
  deliveryThru: 'TCP_OR_FTP' | 'AF_DELIVERY'
  cfGroup?: string
  contact?: string
  hi2Interfaces?: HI2InterfaceFormData[]
  hi3Interfaces?: HI3InterfaceFormData[]
}

export interface CollectionFunctionListData extends CollectionFunction {
  tagName?: string
}

export type CollectionFunctionMap = {
  [collectionFunctionId: string]: CollectionFunction
}

export type CollectionFunctionGroup = {
  id: string
  name: string
  dxOwner: string
  dxAccess: string
  contact: Contact
  description: string
  cfs: Identity[]
}

export type Option = {
  key:string,
  value:string
};

export type CollectionFunctionInterface = {
  id: string,
  name: string,
  transportType?: 'TCP' | 'UDP' | 'FTP' | 'SFTP' | 'ROSE',
  options?: Option[],
  operation?: Operation,
  // If isRealHI3 is undefined, it is an HI3 Template. This flag is only assigned
  // a value when Warrant Form/Wizard is open.
  isRealHI3?: boolean,
  // This CF associates with this interface.  This flag is only assigned
  // a value when Warrant Form/Wizard is open.
  cfId?: string,
  mdf2NodeName?: string,
  mdf3NodeName?: string,
}

export type CollectionFunctionIntegrityCheck = 'OFF' | 'HASH1WAY' | 'HASH2WAY' | 'SIGNEDHASH'
export type CollectionFunctionCountryCode = 'NONE' | 'GB' | 'JP' | 'NL' | 'VN' | 'OM' | 'TR' | 'US' | 'IE'
export type CollectionFunctionDestinationType = 'ipAddress' | 'domainGroup'

export type HI2Interface = CollectionFunctionInterface
export type HI3Interface = CollectionFunctionInterface

export interface CollectionFunctionInterfaceFormData extends Omit<CollectionFunctionInterface, 'options'> {
  comments?: string
  connectionType?: 'PRIMARY' | 'FAILOVER'
  destIPAddr?: string
  destPort?: string
  displayLabel?: string
  dscp?: string
  keepAliveFreq?: string
  keepAliveRetries?: string
  inactivityTimer?: string
  ownIPAddr?: string
  ownIPPort?: string
  reqState?: 'ACTIVE' | 'INACTIVE'
  state?: string
  tpkt?: boolean
  version?: string
  useIpdu?: boolean
  traceLevel?: '0' | '1' | '2' | '3' | '4'
  ifid?: string
  cfid?: string
  ftpInfoId?: string
  userName?: string
  userPassword?: string
  ftpFileSec?: string
  ftpFileSize?: string
  operatorId?: string
  fileMode?: 'ALLINONE' | 'CASE'
  destPath?: string
  transport?: 'TCP' | 'UDP'
  filter?: string
  countryCode?: CollectionFunctionCountryCode
  hashCert?: string
  hashNum?: string
  hashSec?: string
  icheck?: CollectionFunctionIntegrityCheck
  ownnetid?: string
  pldEncKey?: string
  pldEncType?: boolean
  useHi3?: boolean
  destType?: CollectionFunctionDestinationType
  fsid?: string
  secinfoid?: string
  ftpType?: 'FTP' | 'SFTP'
}

export type HI2InterfaceFormData = CollectionFunctionInterfaceFormData
export type HI3InterfaceFormData = CollectionFunctionInterfaceFormData

export interface CollectionFunctionState {
  cfs: CollectionFunction[]
  cfGroups: CollectionFunctionGroup[]
  single: CollectionFunction | null
  selected: CollectionFunction[]
}

export interface GoToCollectionFunctionCreateAction extends AnyAction {
  type: typeof GO_TO_COLLECTION_FUNCTION_CREATE
}

export interface GoToCollectionFunctionEditAction extends AnyAction {
  type: typeof GO_TO_COLLECTION_FUNCTION_EDIT
}

export interface GoToCollectionFunctionViewAction extends AnyAction {
  type: typeof GO_TO_COLLECTION_FUNCTION_VIEW
}

export interface GoToCollectionFunctionCopyAction extends AnyAction {
  type: typeof GO_TO_COLLECTION_FUNCTION_COPY
}

export interface GetCollectionFunctionListAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionListFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionListByFilterAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_BY_FILTER
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionListByFilterFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionListByCollectionFunctionIdsAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_BY_IDS
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionListByCollectionFunctionIdsFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionGroupListAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_GROUP_LIST
  payload: AxiosPromise<ServerResponse<CollectionFunctionGroup>>
}

export interface GetCollectionFunctionGroupListFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunctionGroup>>
}

export interface GetCollectionFunctionAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionPendingAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_PENDING
  payload: null
}

export interface GetCollectionFunctionFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionWithViewOptionsAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionWithViewOptionsFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface CreateCollectionFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}
export interface CreateCollectionFunctionAction extends AnyAction {
  type: typeof CREATE_COLLECTION_FUNCTION
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface CreateCollectionFunctionFulfilledAction extends AnyAction {
  type: typeof CREATE_COLLECTION_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface CreateCollectionFunctionRejectedAction extends AnyAction {
  type: typeof CREATE_COLLECTION_FUNCTION_REJECTED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface UpdateCollectionFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}
export interface UpdateCollectionFunctionAction extends AnyAction {
  type: typeof UPDATE_COLLECTION_FUNCTION
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface UpdateCollectionFunctionFulfilledAction extends AnyAction {
  type: typeof UPDATE_COLLECTION_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface UpdateCollectionFunctionRejectedAction extends AnyAction {
  type: typeof UPDATE_COLLECTION_FUNCTION_REJECTED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface DeleteCollectionFunctionThunkAction extends ThunkAction<void, RootState, void, AnyAction> {}
export interface DeleteCollectionFunctionAction extends AnyAction {
  type: typeof DELETE_COLLECTION_FUNCTION
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface DeleteCollectionFunctionFulfilledAction extends AnyAction {
  type: typeof DELETE_COLLECTION_FUNCTION_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface DeleteCollectionFunctionRejectedAction extends AnyAction {
  type: typeof DELETE_COLLECTION_FUNCTION_REJECTED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionHI2InterfaceListAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST
  payload: AxiosPromise<ServerResponse<HI2Interface>>
}

export interface GetCollectionFunctionHI2InterfaceListFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<HI2Interface>>
}

export interface GetCollectionFunctionHI3InterfaceListAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST
  payload: AxiosPromise<ServerResponse<HI3Interface>>
}

export interface GetCollectionFunctionHI3InterfaceListFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST_FULFILLED
  payload: AxiosResponse<ServerResponse<HI3Interface>>
}

export interface ClearCollectionFunctionState extends AnyAction {
  type: typeof CLEAR_COLLECTION_FUNCTION_STATE
  payload: null
}

export interface SelectCollectionFunctionAction extends AnyAction{
  type: typeof SELECT_COLLECTION_FUNCTION
  payload: CollectionFunction[]
}

export interface GetCollectionFunctionsWithViewOptionAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS
  payload: AxiosPromise<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionsWithViewOptionFulfilledAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export interface GetCollectionFunctionsWithViewOptionPendingAction extends AnyAction {
  type: typeof GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_PENDING
  payload: AxiosResponse<ServerResponse<CollectionFunction>>
}

export type CollectionFunctionActionTypes =
  GoToCollectionFunctionCreateAction |
  GoToCollectionFunctionEditAction |
  GoToCollectionFunctionViewAction |
  GoToCollectionFunctionCopyAction |
  GetCollectionFunctionListAction |
  GetCollectionFunctionListFulfilledAction |
  GetCollectionFunctionListByFilterAction |
  GetCollectionFunctionListByFilterFulfilledAction |
  GetCollectionFunctionGroupListAction |
  GetCollectionFunctionGroupListFulfilledAction |
  GetCollectionFunctionListByCollectionFunctionIdsAction |
  GetCollectionFunctionListByCollectionFunctionIdsFulfilledAction |
  GetCollectionFunctionAction |
  GetCollectionFunctionPendingAction |
  GetCollectionFunctionFulfilledAction |
  GetCollectionFunctionWithViewOptionsAction |
  GetCollectionFunctionWithViewOptionsFulfilledAction |
  CreateCollectionFunctionAction |
  CreateCollectionFunctionFulfilledAction |
  CreateCollectionFunctionRejectedAction |
  UpdateCollectionFunctionAction |
  UpdateCollectionFunctionFulfilledAction |
  UpdateCollectionFunctionRejectedAction |
  DeleteCollectionFunctionAction |
  DeleteCollectionFunctionFulfilledAction |
  DeleteCollectionFunctionRejectedAction |
  GetCollectionFunctionHI2InterfaceListAction |
  GetCollectionFunctionHI2InterfaceListFulfilledAction |
  GetCollectionFunctionHI3InterfaceListAction |
  GetCollectionFunctionHI3InterfaceListFulfilledAction |
  ClearCollectionFunctionState |
  SelectCollectionFunctionAction |
  GetCollectionFunctionsWithViewOptionAction |
  GetCollectionFunctionsWithViewOptionFulfilledAction |
  GetCollectionFunctionsWithViewOptionPendingAction;
