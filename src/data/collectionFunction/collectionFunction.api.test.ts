import MockAdapter from 'axios-mock-adapter';
import { WithAuditDetails } from 'data/types';
import Http from 'shared/http';
import { AuditActionType, AuditService } from 'shared/userAudit/audit-constants';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './collectionFunction.api';
import {
  API_PATH_COLLECTION_FUNCTION,
  API_PATH_COLLECTION_FUNCTION_GROUP,
  CollectionFunction,
  HI2Interface,
  HI3Interface,
} from './collectionFunction.types';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('collectionFunction.api', () => {
  let collectionFunctionWithAudit: WithAuditDetails<CollectionFunction>;
  let auditOnly: WithAuditDetails<void>;
  let collectionFunctionHi2: HI2Interface;
  let collectionFunctionHi3: HI3Interface;

  beforeEach(() => {
    collectionFunctionWithAudit = {
      data: mockCollectionFunctions[0],
    };

    auditOnly = {
      data: undefined,
      auditDetails: {
        auditEnabled: true,
        fieldDetails: '',
        recordName: 'recordName1',
        service: AuditService.CF,
        userId: 'userId1',
        userName: 'userName1',
        actionType: AuditActionType.COLLECTION_FUNCTION_DELETE,
      },
    };

    collectionFunctionHi2 = {
      id: 'hi2',
      name: 'Hi2',
      options: [{
        key: 'key1',
        value: 'value1',
      }],
    };

    collectionFunctionHi3 = {
      id: 'hi3',
      name: 'Hi3',
      options: [{
        key: 'key2',
        value: 'value2',
      }],
      transportType: 'FTP',
    };
  });

  it('should request the list for collection functions', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctions);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0`).reply(200, response);
    await expect(api.getCollectionFunctionList('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0` },
      status: 200,
    });
  });

  it('should request the list of collection functions filtered', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctions);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0?$filter=filter1`).reply(200, response);
    await expect(api.getCollectionFunctionListByFilter('0', 'filter1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0?$filter=filter1` },
      status: 200,
    });
  });

  it('should request the list of collection functions by ids', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctions);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0?ids=id1,id2`).reply(200, response);
    await expect(api.getCollectionFunctionListByIds('0', 'id1,id2')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0?ids=id1,id2` },
      status: 200,
    });
  });

  it('should get the list of collection function groups', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctionGroups);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION_GROUP}/0?$orderBy=NAME asc`).reply(200, response);
    await expect(api.getCollectionFunctionGroups('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION_GROUP}/0?$orderBy=NAME asc` },
      status: 200,
    });
  });

  it('should request a collection function', async () => {
    const response = buildAxiosServerResponse([mockCollectionFunctions[0]]);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0/id1`).reply(200, response);
    await expect(api.getCollectionFunction('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1` },
      status: 200,
    });
  });

  it('should request to create a collection function', async () => {
    const response = buildAxiosServerResponse([mockCollectionFunctions[0]]);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0`).reply(200, response);
    await expect(api.getCollectionFunction('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1` },
      status: 200,
    });
  });

  it('should request to update a collection function', async () => {
    const response = buildAxiosServerResponse([mockCollectionFunctions[0]]);
    axiosMock.onPut(`${API_PATH_COLLECTION_FUNCTION}/0/id1`).reply(200, response);
    await expect(api.updateCollectionFunction('0', 'id1', collectionFunctionWithAudit)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1` },
      status: 200,
    });
  });

  it('should request to delete a collection function', async () => {
    const response = buildAxiosServerResponse([mockCollectionFunctions[0]]);
    axiosMock.onDelete(`${API_PATH_COLLECTION_FUNCTION}/0/id1`).reply(200, response);
    await expect(api.deleteCollectionFunction('0', 'id1', auditOnly)).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1` },
      status: 200,
    });
  });

  it('should get the list of HI2 interfaces for a collection function', async () => {
    const response = buildAxiosServerResponse([collectionFunctionHi2]);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0/id1/hi2-interfaces?$view=options`).reply(200, response);
    await expect(api.getCollectionFunctionHi2InterfaceList('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1/hi2-interfaces?$view=options` },
      status: 200,
    });
  });

  it('should get the list of HI3 interfaces for a collection function', async () => {
    const response = buildAxiosServerResponse([collectionFunctionHi3]);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0/id1/hi3-interfaces?$view=options`).reply(200, response);
    await expect(api.getCollectionFunctionHi3InterfaceList('0', 'id1')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/id1/hi3-interfaces?$view=options` },
      status: 200,
    });
  });

  it('should request the list for collection functions passing view options params', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctions);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0?$view=options`).reply(200, response);
    await expect(api.getCollectionFunctionListWithViewOptions('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0?$view=options` },
      status: 200,
    });
  });

  it('should request the collection functions passing view options params', async () => {
    const response = buildAxiosServerResponse(mockCollectionFunctions);
    axiosMock.onGet(`${API_PATH_COLLECTION_FUNCTION}/0/cf_id?$view=options`).reply(200, response);
    await expect(api.getCollectionFunctionWithViewOptions('0', 'cf_id')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_COLLECTION_FUNCTION}/0/cf_id?$view=options` },
      status: 200,
    });
  });

});
