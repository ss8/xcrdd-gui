import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  API_PATH_COLLECTION_FUNCTION,
  API_PATH_COLLECTION_FUNCTION_GROUP,
  CollectionFunction,
  CollectionFunctionGroup,
  HI2Interface,
  HI3Interface,
} from './collectionFunction.types';
import { ServerResponse, WithAuditDetails } from '../types';

const http = Http.getHttp();

export function getCollectionFunctionList(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}`);
}

export function getCollectionFunctionListByFilter(
  deliveryFunctionId: string,
  filter: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}?$filter=${filter}`);
}

export function getCollectionFunctionListByIds(
  deliveryFunctionId: string,
  ids: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}?ids=${ids}`);
}

export function getCollectionFunctionGroups(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<CollectionFunctionGroup>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION_GROUP}/${deliveryFunctionId}?$orderBy=NAME asc`);
}

export function getCollectionFunction(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}`);
}

export function getCollectionFunctionWithViewOptions(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}?$view=options`);
}

export function createCollectionFunction(
  deliveryFunctionId: string,
  body: WithAuditDetails<CollectionFunction>,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.post(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}`, body);
}

export function updateCollectionFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<CollectionFunction>,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.put(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}`, body);
}

export function deleteCollectionFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.delete(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}`, { data: body.auditDetails });
}

export function getCollectionFunctionHi2InterfaceList(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<HI2Interface>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}/hi2-interfaces?$view=options`);
}

export function getCollectionFunctionHi3InterfaceList(
  deliveryFunctionId: string,
  id: string,
): AxiosPromise<ServerResponse<HI3Interface>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}/${id}/hi3-interfaces?$view=options`);
}

export function getCollectionFunctionListWithViewOptions(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<CollectionFunction>> {
  return http.get(`${API_PATH_COLLECTION_FUNCTION}/${deliveryFunctionId}?$view=options`);
}
