import { RootState } from 'data/root.reducers';
import { CollectionFunction, CollectionFunctionGroup } from './collectionFunction.types';

export const getCollectionFunctionList = (state: RootState): CollectionFunction[] => state.cfs.cfs;
export const getCollectionFunctionGroupList = (state: RootState): CollectionFunctionGroup[] => state.cfs.cfGroups;
export const getCollectionFunction = (state: RootState): CollectionFunction | null => state.cfs.single;
export const getSelectedCollectionFunction = (state: RootState): (CollectionFunction[]) => state.cfs.selected;
