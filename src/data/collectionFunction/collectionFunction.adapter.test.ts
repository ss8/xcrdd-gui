import cloneDeep from 'lodash.clonedeep';
import * as types from './collectionFunction.types';
import {
  adaptToForm,
  adaptToSubmit,
  adaptToCopy,
  setCollectionFunctionOperationValues,
} from './collectionFunction.adapter';

describe('collectionFunction.adapter', () => {
  let collectionFunction: types.CollectionFunction;
  let collectionFunctionWithInterfaces: types.CollectionFunction;
  let collectionFunctionForm: types.CollectionFunctionFormData;
  let collectionFunctionFormWithInterfaces: types.CollectionFunctionFormData;

  beforeEach(() => {
    collectionFunction = {
      id: 'id1',
      tag: 'CF-3GPP-33108',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'Y29udGFjdDE=', name: 'contact1' },
      insideNat: false,
      timeZone: 'timeZone1',
      type: 'T1.678',
      cellTowerLocation: false,
      deliveryType: 'DSR',
      hi2Interfaces: [],
      hi3Interfaces: [],
    };

    collectionFunctionWithInterfaces = {
      ...collectionFunction,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '15.6' },
          { key: 'traceLevel', value: '0' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'comments', value: 'some comments' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'dscp', value: '0' },
          { key: 'destPort', value: '1' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'tpkt', value: 'Y' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'ownIPAddr', value: '12.12.12.12' },
          { key: 'cfid', value: 'CA-DOJBNE-33108' },
          { key: 'ownIPPort', value: '12' },
          { key: 'useHi3', value: 'Y' },
          { key: 'pldEncType', value: 'AES-192-CBC' },
        ],
      }],
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'label 2' },
          { key: 'dscp', value: '10' },
          { key: 'keepAliveFreq', value: '120' },
          { key: 'keepAliveRetries', value: '20' },
          { key: 'inactivityTimer', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
          { key: 'useIpdu', value: 'N' },
          { key: 'useHi3', value: 'N' },
          { key: 'pldEncType', value: 'NONE' },
        ],
      }],
    };

    collectionFunctionForm = {
      id: 'id1',
      tag: 'CF-3GPP-33108',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: 'Y29udGFjdDE=',
      insideNat: false,
      timeZone: 'timeZone1',
      type: 'T1.678',
      cellTowerLocation: false,
      deliveryType: 'DSR',
      deliveryThru: 'TCP_OR_FTP',
      hi2Interfaces: [],
      hi3Interfaces: [],
    };

    collectionFunctionFormWithInterfaces = {
      ...collectionFunctionForm,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        version: '15.6',
        traceLevel: '0',
        state: 'INACTIVE',
        reqState: 'ACTIVE',
        comments: 'some comments',
        connectionType: 'PRIMARY',
        ifid: '1',
        dscp: '0',
        destPort: '1',
        keepAliveFreq: '60',
        keepAliveRetries: '6',
        tpkt: true,
        destIPAddr: '1.1.1.1',
        ownIPAddr: '12.12.12.12',
        cfid: 'CA-DOJBNE-33108',
        ownIPPort: '12',
        useHi3: true,
        pldEncType: true,
      }],
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        comments: 'some comment 2',
        connectionType: 'FAILOVER',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        displayLabel: 'label 2',
        dscp: '10',
        keepAliveFreq: '120',
        keepAliveRetries: '20',
        inactivityTimer: '20',
        ownIPAddr: '22.22.22.22',
        ownIPPort: '22',
        tpkt: false,
        version: '15.5',
        useIpdu: false,
        useHi3: false,
        pldEncType: false,
      }],
    };
  });

  it('should adapt the collection function data from server to be loaded in the form', () => {
    expect(adaptToForm(collectionFunction)).toEqual(collectionFunctionForm);
  });

  it('should adapt the collection function data from server to be loaded in the form with interfaces', () => {
    expect(adaptToForm(collectionFunctionWithInterfaces)).toEqual(collectionFunctionFormWithInterfaces);
  });

  it('should remove the ANY_ADDR value from ownIPAddr', () => {
    const collectionFunctionWithAnyAddress: types.CollectionFunction = {
      ...collectionFunction,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'ownIPAddr', value: 'ANY_ADDR' },
        ],
      }],
    };

    expect(adaptToForm(collectionFunctionWithAnyAddress)).toEqual({
      ...collectionFunctionForm,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        ownIPAddr: '',
      }],
    });
  });

  it('should remove the ADDR_ANY value from ownIPAddr', () => {
    const collectionFunctionWithAnyAddress: types.CollectionFunction = {
      ...collectionFunction,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'ownIPAddr', value: 'ADDR_ANY' },
        ],
      }],
    };

    expect(adaptToForm(collectionFunctionWithAnyAddress)).toEqual({
      ...collectionFunctionForm,
      hi2Interfaces: [{
        id: 'hi2InterfacesId1',
        name: 'hi2InterfacesName1',
        transportType: 'TCP',
        ownIPAddr: '',
      }],
    });
  });

  it('should adapt the CF-ETSI-102232 hi3 interface to set the destType', () => {
    const collectionFunctionETSI: types.CollectionFunction = {
      ...collectionFunction,
      tag: 'CF-ETSI-102232',
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
        ],
      }],
    };

    expect(adaptToForm(collectionFunctionETSI)).toEqual({
      ...collectionFunctionForm,
      tag: 'CF-ETSI-102232',
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        destType: 'ipAddress',
      }],
    });
  });

  it('should adapt the CF-ETSI-102232 hi3 interface to set the destType', () => {
    const collectionFunctionETSI: types.CollectionFunction = {
      ...collectionFunction,
      tag: 'CF-ETSI-102232',
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        options: [
          { key: 'destIPAddr', value: '' },
          { key: 'destPort', value: '12' },
          { key: 'fsid', value: 'domain2' },
        ],
      }],
    };

    expect(adaptToForm(collectionFunctionETSI)).toEqual({
      ...collectionFunctionForm,
      tag: 'CF-ETSI-102232',
      hi3Interfaces: [{
        id: 'hi3InterfacesId1',
        name: 'hi3InterfacesName1',
        transportType: 'TCP',
        destIPAddr: '',
        destPort: '12',
        destType: 'domainGroup',
        fsid: 'domain2',
      }],
    });
  });

  it('should adapt the collection function form data to be copied', () => {
    const {
      id,
      name,
      ...hi2InterfacesToCopy
    } = (collectionFunctionFormWithInterfaces.hi2Interfaces as types.CollectionFunctionInterfaceFormData[])[0];

    const {
      id: _id,
      name: _name,
      ...hi3InterfacesToCopy
    } = (collectionFunctionFormWithInterfaces.hi3Interfaces as types.CollectionFunctionInterfaceFormData[])[0];

    expect(adaptToCopy(collectionFunctionFormWithInterfaces)).toEqual({
      ...collectionFunctionFormWithInterfaces,
      id: '',
      name: 'Copy of name1',
      hi3Interfaces: [{
        ...hi3InterfacesToCopy, id: '3.collectionFunctionInterface', destPort: '',
      }],
      hi2Interfaces: [{
        ...hi2InterfacesToCopy, id: '2.collectionFunctionInterface', destPort: '',
      }],
    });
  });

  describe('when creating a new collection function', () => {
    let expectedCollectionFunction: types.CollectionFunction;
    let expectedCollectionFunctionWithInterfaces: types.CollectionFunction;

    it('should adapt the collection function data from the form to be sent to the server', () => {
      expect(adaptToSubmit(collectionFunctionForm)).toEqual(collectionFunction);
    });

    it('should adapt the collection function data from the form to be sent to the server when there is a cfGroup', () => {
      collectionFunctionForm.cfGroup = 'id1';

      expect(adaptToSubmit(collectionFunctionForm)).toEqual({
        ...collectionFunction,
        cfGroup: {
          id: 'id1',
          name: '',
        },
      });
    });

    it('should not add the operation fields values if not required', () => {
      expectedCollectionFunction = cloneDeep(collectionFunction);
      expect(setCollectionFunctionOperationValues(collectionFunction)).toEqual(expectedCollectionFunction);
    });

    it('should adapt the collection function data from the form to be sent to the server with interfaces', () => {
      expectedCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);
      expect(adaptToSubmit(collectionFunctionFormWithInterfaces)).toEqual(expectedCollectionFunctionWithInterfaces);
    });

    it('should remove the temporary id', () => {
      expectedCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);

      (collectionFunctionFormWithInterfaces.hi2Interfaces?.[0] as types.CollectionFunctionInterface).id = `1.${types.COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`;
      (collectionFunctionFormWithInterfaces.hi3Interfaces?.[0] as types.CollectionFunctionInterface).id = `2.${types.COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`;

      (expectedCollectionFunctionWithInterfaces.hi2Interfaces?.[0] as types.CollectionFunctionInterface).id = '';
      (expectedCollectionFunctionWithInterfaces.hi3Interfaces?.[0] as types.CollectionFunctionInterface).id = '';
      expect(adaptToSubmit(collectionFunctionFormWithInterfaces)).toEqual(
        expectedCollectionFunctionWithInterfaces,
      );
    });

    it('should add the operation fields values', () => {
      expectedCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);

      (expectedCollectionFunctionWithInterfaces.hi2Interfaces?.[0] as types.CollectionFunctionInterface).operation = 'CREATE';
      (expectedCollectionFunctionWithInterfaces.hi3Interfaces?.[0] as types.CollectionFunctionInterface).operation = 'CREATE';
      expect(setCollectionFunctionOperationValues(collectionFunctionWithInterfaces)).toEqual(
        expectedCollectionFunctionWithInterfaces,
      );
    });

    it('should adapt the CF-ETSI-102232 hi3 interface to remove the destType and related fields when ipAddress', () => {
      const collectionFunctionFormETSI: types.CollectionFunctionFormData = {
        ...collectionFunctionForm,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          destIPAddr: '3.3.3.3',
          destPort: '3',
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          destIPAddr: '2.2.2.2',
          destPort: '2',
          destType: 'ipAddress',
          fsid: 'domain2',
        }],
      };

      expect(adaptToSubmit(collectionFunctionFormETSI)).toEqual({
        ...collectionFunction,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          options: [
            { key: 'destIPAddr', value: '3.3.3.3' },
            { key: 'destPort', value: '3' },
          ],
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          options: [
            { key: 'destPort', value: '2' },
            { key: 'destIPAddr', value: '2.2.2.2' },
          ],
        }],
      });
    });

    it('should adapt the CF-ETSI-102232 hi3 interface to remove the destType and related fields when domainGroup', () => {
      const collectionFunctionFormETSI: types.CollectionFunctionFormData = {
        ...collectionFunctionForm,
        tag: 'CF-ETSI-102232',
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          destIPAddr: '2.2.2.2',
          destPort: '2',
          destType: 'domainGroup',
          fsid: 'domain2',
          pldEncKey: '1231',
        }],
      };

      expect(adaptToSubmit(collectionFunctionFormETSI)).toEqual({
        ...collectionFunction,
        tag: 'CF-ETSI-102232',
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          options: [
            { key: 'destPort', value: '2' },
            { key: 'destIPAddr', value: '' },
            { key: 'fsid', value: 'domain2' },
            { key: 'pldEncKey', value: '1231' },
          ],
        }],
      });
    });

    it('should adapt the CF-ETSI-102232 hi3 and hi2 interface to remove the pldEncKey if it is a placeholder', () => {
      const collectionFunctionFormETSI: types.CollectionFunctionFormData = {
        ...collectionFunctionForm,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          pldEncKey: '*****',
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          pldEncKey: '*****',
        }],
      };

      expect(adaptToSubmit(collectionFunctionFormETSI)).toEqual({
        ...collectionFunction,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          options: [],
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          options: [],
        }],
      });
    });

    it('should adapt the CF-ETSI-102232 hi3 and hi2 interface and not remove the pldEncKey if it is a valid value', () => {
      const collectionFunctionFormETSI: types.CollectionFunctionFormData = {
        ...collectionFunctionForm,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          pldEncKey: '',
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          pldEncKey: '1234',
        }],
      };

      expect(adaptToSubmit(collectionFunctionFormETSI)).toEqual({
        ...collectionFunction,
        tag: 'CF-ETSI-102232',
        hi2Interfaces: [{
          id: 'hi2InterfacesId1',
          name: 'hi2InterfacesName1',
          transportType: 'TCP',
          options: [],
        }],
        hi3Interfaces: [{
          id: 'hi3InterfacesId1',
          name: 'hi3InterfacesName1',
          transportType: 'TCP',
          options: [{ key: 'pldEncKey', value: '1234' }],
        }],
      });
    });
  });

  describe('when editing an collection function', () => {
    let originalCollectionFunction: types.CollectionFunction;
    let originalCollectionFunctionWithInterfaces: types.CollectionFunction;
    let expectedCollectionFunction: types.CollectionFunction;
    let expectedCollectionFunctionWithInterfaces: types.CollectionFunction;

    beforeEach(() => {
      originalCollectionFunction = {
        ...collectionFunction,
      };

      originalCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);
      (originalCollectionFunctionWithInterfaces.hi2Interfaces?.[0] as types.CollectionFunctionInterface).name = 'name changed';
    });

    it('should adapt the collection function data from the form to be sent to the server', () => {
      expect(adaptToSubmit(collectionFunctionForm)).toEqual(collectionFunction);
    });

    it('should adapt the collection function data from the form to be sent to the server when there is a cfGroup', () => {
      collectionFunctionForm.cfGroup = 'id1';

      expect(adaptToSubmit(collectionFunctionForm)).toEqual({
        ...collectionFunction,
        cfGroup: {
          id: 'id1',
          name: '',
        },
      });
    });

    it('should add the operation fields as expected', () => {
      expectedCollectionFunction = cloneDeep(collectionFunction);
      expectedCollectionFunction.operation = 'UPDATE';
      expect(setCollectionFunctionOperationValues(collectionFunction, originalCollectionFunction, 'UPDATE')).toEqual(expectedCollectionFunction);
    });

    it('should adapt the collection function data from the form to be sent to the server with interfaces', () => {
      expectedCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);
      expect(adaptToSubmit(collectionFunctionFormWithInterfaces)).toEqual(expectedCollectionFunctionWithInterfaces);
    });

    it('should add the operation fields as expected when there are interfaces', () => {
      expectedCollectionFunctionWithInterfaces = cloneDeep(collectionFunctionWithInterfaces);

      expectedCollectionFunctionWithInterfaces.operation = 'UPDATE';
      (expectedCollectionFunctionWithInterfaces.hi2Interfaces?.[0] as types.CollectionFunctionInterface).operation = 'UPDATE';
      expectedCollectionFunctionWithInterfaces.hi3Interfaces = [{
        ...(expectedCollectionFunctionWithInterfaces.hi3Interfaces?.[0] as types.CollectionFunctionInterface),
        operation: 'DELETE',
      }];

      collectionFunctionWithInterfaces.hi3Interfaces = [];

      expect(
        setCollectionFunctionOperationValues(collectionFunctionWithInterfaces, originalCollectionFunctionWithInterfaces, 'UPDATE'),
      ).toEqual(expectedCollectionFunctionWithInterfaces);
    });
  });
});
