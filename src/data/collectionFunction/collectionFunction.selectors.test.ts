import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import rootReducer, { RootState } from 'data/root.reducers';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import * as types from './collectionFunction.types';
import {
  getCollectionFunctionGroupList,
  getCollectionFunctionList,
  getCollectionFunction,
  getSelectedCollectionFunction,
} from './collectionFunction.selectors';

describe('collectionFunction.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    const collectionFunctionAction: AnyAction = {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    };

    const collectionFunctionGroupAction: AnyAction = {
      type: types.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    };

    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, collectionFunctionAction));
    store = mockStore(rootReducer(store.getState(), collectionFunctionGroupAction));
  });

  it('should return the list of collection function', () => {
    const action: types.CollectionFunctionActionTypes = {
      type: types.GET_COLLECTION_FUNCTION_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getCollectionFunctionList(store.getState())).toEqual(mockCollectionFunctions);
  });

  it('should return the list of collection function group', () => {
    const action: types.CollectionFunctionActionTypes = {
      type: types.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getCollectionFunctionGroupList(store.getState())).toEqual(mockCollectionFunctionGroups);
  });

  it('should return the single Collection Function', () => {
    const action: types.CollectionFunctionActionTypes = {
      type: types.GET_COLLECTION_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([mockCollectionFunctions[0]]),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getCollectionFunction(store.getState())).toEqual(mockCollectionFunctions[0]);
  });

  it('should return the selected Collection Function', () => {
    const action: types.CollectionFunctionActionTypes = {
      type: types.SELECT_COLLECTION_FUNCTION,
      payload: mockCollectionFunctions,
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSelectedCollectionFunction(store.getState())).toEqual(mockCollectionFunctions);
  });
});
