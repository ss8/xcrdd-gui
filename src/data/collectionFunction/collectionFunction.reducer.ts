import {
  GET_COLLECTION_FUNCTION_LIST_FULFILLED,
  GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED,
  GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
  GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED,
  CLEAR_COLLECTION_FUNCTION_STATE,
  CollectionFunctionActionTypes,
  CollectionFunctionState,
  GET_COLLECTION_FUNCTION_LIST,
  GET_COLLECTION_FUNCTION_LIST_BY_FILTER,
  GET_COLLECTION_FUNCTION_LIST_BY_IDS,
  GET_COLLECTION_FUNCTION_PENDING,
  GET_COLLECTION_FUNCTION_FULFILLED,
  SELECT_COLLECTION_FUNCTION,
  GO_TO_COLLECTION_FUNCTION_CREATE,
  GO_TO_COLLECTION_FUNCTION_EDIT,
  GO_TO_COLLECTION_FUNCTION_COPY,
  GO_TO_COLLECTION_FUNCTION_VIEW,
  GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
  GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED,
  GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_PENDING,
  DELETE_COLLECTION_FUNCTION_FULFILLED,
  GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS,
  GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED,
} from './collectionFunction.types';

const initialState: CollectionFunctionState = {
  cfs: [],
  cfGroups: [],
  single: null,
  selected: [],
};

export default function collectionFunctionReducer(
  state = initialState,
  action: CollectionFunctionActionTypes,
): CollectionFunctionState {
  switch (action.type) {
    case GET_COLLECTION_FUNCTION_LIST:
    case GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS:
    case GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_PENDING:
    case GET_COLLECTION_FUNCTION_LIST_BY_FILTER:
    case GET_COLLECTION_FUNCTION_LIST_BY_IDS:
      return {
        ...state,
        selected: [],
      };

    case GET_COLLECTION_FUNCTION_LIST_FULFILLED:
    case GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED:
      return {
        ...state,
        cfs: action.payload.data.data ?? [],
        selected: [],
      };

    case GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED:
      return {
        ...state,
        cfs: action.payload.data.data ?? [],
      };

    case GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED:
      return {
        ...state,
        cfs: action.payload.data.data ?? [],
      };

    case GET_COLLECTION_FUNCTION_PENDING:
    case GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS:
      return {
        ...state,
        single: null,
      };

    case GET_COLLECTION_FUNCTION_FULFILLED:
    case GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED:
      return {
        ...state,
        single: (action.payload.data.data ?? [])[0],
      };

    case SELECT_COLLECTION_FUNCTION:
      return {
        ...state,
        selected: action.payload || [],
      };

    case GO_TO_COLLECTION_FUNCTION_CREATE:
    case GO_TO_COLLECTION_FUNCTION_EDIT:
    case GO_TO_COLLECTION_FUNCTION_COPY:
    case GO_TO_COLLECTION_FUNCTION_VIEW:
      return {
        ...state,
        single: null,
      };

    case CLEAR_COLLECTION_FUNCTION_STATE:
      return {
        ...state,
        cfs: [],
      };

    case GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED:
      return {
        ...state,
        cfGroups: action.payload.data.data ?? [],
      };

    case DELETE_COLLECTION_FUNCTION_FULFILLED:
      return {
        ...state,
        selected: [],
      };
    default:
      return state;
  }
}
