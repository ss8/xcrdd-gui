import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import reducer from './collectionFunction.reducer';
import * as types from './collectionFunction.types';
import * as api from './collectionFunction.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('collectionFunction.reducer', () => {
  beforeAll(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: mockCollectionFunctions,
    }, {
      type: types.GET_COLLECTION_FUNCTION_LIST,
      payload: api.getCollectionFunctionList('0'),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_BY_FILTER', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: mockCollectionFunctions,
    }, {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_FILTER,
      payload: api.getCollectionFunctionList('0'),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_BY_IDS', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: mockCollectionFunctions,
    }, {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_IDS,
      payload: api.getCollectionFunctionList('0'),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    })).toEqual({
      cfs: mockCollectionFunctions,
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    }, {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_FILTER_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    })).toEqual({
      cfs: mockCollectionFunctions,
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_LIST_BY_IDS_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    })).toEqual({
      cfs: mockCollectionFunctions,
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_PENDING', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GET_COLLECTION_FUNCTION_PENDING,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([mockCollectionFunctions[0]]),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS,
      payload: api.getCollectionFunctionList('0'),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse([mockCollectionFunctions[0]]),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    });
  });

  it('should handle SELECT_COLLECTION_FUNCTION', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    }, {
      type: types.SELECT_COLLECTION_FUNCTION,
      payload: [mockCollectionFunctions[0]],
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [mockCollectionFunctions[0]],
    });
  });

  it('should handle GO_TO_COLLECTION_FUNCTION_CREATE', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_CREATE,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GO_TO_COLLECTION_FUNCTION_EDIT', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_EDIT,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GO_TO_COLLECTION_FUNCTION_COPY', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_COPY,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GO_TO_COLLECTION_FUNCTION_VIEW', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: mockCollectionFunctions[0],
      selected: [],
    }, {
      type: types.GO_TO_COLLECTION_FUNCTION_VIEW,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle CLEAR_COLLECTION_FUNCTION_STATE', () => {
    expect(reducer({
      cfs: mockCollectionFunctions,
      cfGroups: mockCollectionFunctionGroups,
      single: null,
      selected: [],
    }, {
      type: types.CLEAR_COLLECTION_FUNCTION_STATE,
      payload: null,
    })).toEqual({
      cfs: [],
      cfGroups: mockCollectionFunctionGroups,
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    })).toEqual({
      cfs: [],
      cfGroups: mockCollectionFunctionGroups,
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS', () => {
    expect(reducer({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: mockCollectionFunctions,
    }, {
      type: types.GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
      payload: api.getCollectionFunctionListWithViewOptions('0'),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctions),
    })).toEqual({
      cfs: mockCollectionFunctions,
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED if data payload is undefined', () => {
    expect(reducer(undefined, {
      type: types.GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(null),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

  it('should handle DELETE_COLLECTION_FUNCTION_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.DELETE_COLLECTION_FUNCTION_FULFILLED,
      payload: buildAxiosServerResponse([]),
    })).toEqual({
      cfs: [],
      cfGroups: [],
      single: null,
      selected: [],
    });
  });

});
