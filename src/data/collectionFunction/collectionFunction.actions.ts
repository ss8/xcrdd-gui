import { WithAuditDetails } from 'data/types';
import { History } from 'history';
import { Dispatch, Action } from 'redux';
import { RootState } from 'data/root.reducers';
import { showSuccessMessage, showError } from 'shared/toaster';
import { ThunkDispatch } from 'redux-thunk';
import * as api from './collectionFunction.api';
import { extractErrorMessage } from '../../utils';
import {
  GET_COLLECTION_FUNCTION_LIST,
  GET_COLLECTION_FUNCTION,
  CREATE_COLLECTION_FUNCTION,
  UPDATE_COLLECTION_FUNCTION,
  DELETE_COLLECTION_FUNCTION,
  GET_COLLECTION_FUNCTION_LIST_BY_FILTER,
  GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST,
  GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST,
  GET_COLLECTION_FUNCTION_GROUP_LIST,
  GET_COLLECTION_FUNCTION_LIST_BY_IDS,
  CollectionFunctionActionTypes,
  CollectionFunction,
  GO_TO_COLLECTION_FUNCTION_LIST,
  GO_TO_COLLECTION_FUNCTION_CREATE,
  GO_TO_COLLECTION_FUNCTION_EDIT,
  GO_TO_COLLECTION_FUNCTION_COPY,
  GO_TO_COLLECTION_FUNCTION_VIEW,
  SELECT_COLLECTION_FUNCTION,
  GetCollectionFunctionHI2InterfaceListAction,
  GetCollectionFunctionHI3InterfaceListAction,
  SelectCollectionFunctionAction,
  ClearCollectionFunctionState,
  CLEAR_COLLECTION_FUNCTION_STATE,
  DeleteCollectionFunctionThunkAction,
  UpdateCollectionFunctionThunkAction,
  CreateCollectionFunctionThunkAction,
  GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
  GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS,
} from './collectionFunction.types';
import { CF_ROUTE } from '../../constants';

export function goToCollectionFunctionList(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_COLLECTION_FUNCTION_LIST,
    });

    history.push(`${CF_ROUTE}`);
  };
}

export function goToCollectionFunctionCreate(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_COLLECTION_FUNCTION_CREATE,
    });

    history.push(`${CF_ROUTE}/create`);
  };
}

export function goToCollectionFunctionEdit(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_COLLECTION_FUNCTION_EDIT,
    });

    history.push(`${CF_ROUTE}/edit`);
  };
}

export function goToCollectionFunctionCopy(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_COLLECTION_FUNCTION_COPY,
    });

    history.push(`${CF_ROUTE}/copy`);
  };
}

export function goToCollectionFunctionView(history: History): (dispatch: Dispatch) => void {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GO_TO_COLLECTION_FUNCTION_VIEW,
    });

    history.push(`${CF_ROUTE}/view`);
  };
}

export function fetchCollectionFunctionList(deliveryFunctionId: string): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_LIST,
    payload: api.getCollectionFunctionList(deliveryFunctionId),
  };
}

export function fetchCollectionFunctionListViewOptions(deliveryFunctionId: string): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_LIST_WITH_VIEW_OPTIONS,
    payload: api.getCollectionFunctionListWithViewOptions(deliveryFunctionId),
  };
}

export function fetchCollectionFunctionListByFilter(
  deliveryFunctionId: string,
  filter: string,
): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_LIST_BY_FILTER,
    payload: api.getCollectionFunctionListByFilter(deliveryFunctionId, filter),
  };
}

export function fetchCollectionFunctionListByIds(
  deliveryFunctionId: string,
  ids: string,
): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_LIST_BY_IDS,
    payload: api.getCollectionFunctionListByIds(deliveryFunctionId, ids),
  };
}

export function fetchCollectionFunctionGroupList(deliveryFunctionId: string): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_GROUP_LIST,
    payload: api.getCollectionFunctionGroups(deliveryFunctionId),
  };
}

export function fetchCollectionFunction(deliveryFunctionId: string, id: string): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION,
    payload: api.getCollectionFunction(deliveryFunctionId, id),
  };
}

export function fetchCollectionFunctionWithViewOptions(
  deliveryFunctionId: string, id: string,
): CollectionFunctionActionTypes {
  return {
    type: GET_COLLECTION_FUNCTION_WITH_VIEW_OPTIONS,
    payload: api.getCollectionFunctionWithViewOptions(deliveryFunctionId, id),
  };
}

export function createCollectionFunction(
  deliveryFunctionId: string,
  body: WithAuditDetails<CollectionFunction>,
  history: History,
): CreateCollectionFunctionThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: CREATE_COLLECTION_FUNCTION,
        payload: api.createCollectionFunction(deliveryFunctionId, body),
      });

      dispatch(goToCollectionFunctionList(history));
      showSuccessMessage(`${body.data.name} successfully created.`, 5000);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create ${body.data.name}: ${errorMessage}.`);
    }
  };
}

export function updateCollectionFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<CollectionFunction>,
  history: History,
): UpdateCollectionFunctionThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>) => {
    try {
      await dispatch({
        type: UPDATE_COLLECTION_FUNCTION,
        payload: api.updateCollectionFunction(deliveryFunctionId, id, body),
      });

      dispatch(goToCollectionFunctionList(history));
      showSuccessMessage(`${body.data.name} successfully updated.`, 5000);

    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update ${body.data.name}: ${errorMessage}.`);
    }
  };
}

export function deleteCollectionFunction(
  deliveryFunctionId: string,
  id: string,
  body: WithAuditDetails<void>,
  history?: History,
): DeleteCollectionFunctionThunkAction {
  return async (dispatch: ThunkDispatch<RootState, unknown, Action<string>>, getState: () => RootState) => {
    const selectedCollectionFunction = getState()?.cfs?.selected[0];

    try {
      await dispatch({
        type: DELETE_COLLECTION_FUNCTION,
        payload: api.deleteCollectionFunction(deliveryFunctionId, id, body),
      });

      showSuccessMessage(`${selectedCollectionFunction?.name} successfully deleted.`, 5000);
      if (history != null) {
        dispatch(goToCollectionFunctionList(history));
      }
    } catch (e) {
      showError(`Unable to delete ${selectedCollectionFunction?.name}.`);

    } finally {
      dispatch(fetchCollectionFunctionListViewOptions(deliveryFunctionId));
    }
  };
}

export function fetchCollectionFunctionHI2InterfaceList(
  deliveryFunctionId: string,
  id: string,
): GetCollectionFunctionHI2InterfaceListAction {
  return {
    type: GET_COLLECTION_FUNCTION_HI2_INTERFACE_LIST,
    payload: api.getCollectionFunctionHi2InterfaceList(deliveryFunctionId, id),
  };
}

export function fetchCollectionFunctionHI3InterfaceList(
  deliveryFunctionId: string,
  id: string,
): GetCollectionFunctionHI3InterfaceListAction {
  return {
    type: GET_COLLECTION_FUNCTION_HI3_INTERFACE_LIST,
    payload: api.getCollectionFunctionHi3InterfaceList(deliveryFunctionId, id),
  };
}

export function clearCollectionFunctionState(): ClearCollectionFunctionState {
  return {
    type: CLEAR_COLLECTION_FUNCTION_STATE,
    payload: null,
  };
}

export function selectCollectionFunction(
  collectionFunction: CollectionFunction[],
): SelectCollectionFunctionAction {
  return {
    type: SELECT_COLLECTION_FUNCTION,
    payload: collectionFunction,
  };
}
