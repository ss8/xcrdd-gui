import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  FtpInfo,
  API_PATH_FTP_INFOS,
} from './ftpInfo.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getFtpInfos(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<FtpInfo>> {
  return http.get(`${API_PATH_FTP_INFOS}/${deliveryFunctionId}`);
}
