import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './ftpInfo.api';
import { API_PATH_FTP_INFOS } from './ftpInfo.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('ftpInfo.api', () => {
  it('should request the list for ftpInfos', async () => {
    const response = buildAxiosServerResponse(mockFtpInfos);
    axiosMock.onGet(`${API_PATH_FTP_INFOS}/0`).reply(200, response);
    await expect(api.getFtpInfos('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: `${API_PATH_FTP_INFOS}/0` },
      status: 200,
    });
  });
});
