import { mockFtpInfos } from '__test__/mockFtpInfos';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './ftpInfo.reducer';
import * as types from './ftpInfo.types';

describe('ftpInfo.reducer', () => {
  it('should handle GET_FTP_INFOS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_FTP_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockFtpInfos),
    })).toEqual({
      entities: mockFtpInfos,
    });
  });
});
