import * as api from './ftpInfo.api';
import { GET_FTP_INFOS, GetFtpInfoAction } from './ftpInfo.types';

// eslint-disable-next-line import/prefer-default-export
export function getFtpInfos(deliveryFunctionId: string): GetFtpInfoAction {
  return {
    type: GET_FTP_INFOS,
    payload: api.getFtpInfos(deliveryFunctionId),
  };
}
