import {
  GET_FTP_INFOS_FULFILLED,
  FtpInfoActionTypes,
  FtpInfoState,
} from './ftpInfo.types';

const initialState: FtpInfoState = {
  entities: [],
};

export default function ftpInfoReducer(
  state = initialState,
  action: FtpInfoActionTypes,
): FtpInfoState {
  switch (action.type) {
    case GET_FTP_INFOS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
