import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_FTP_INFOS = '/cf-ftp-infos';

export const GET_FTP_INFOS = 'GET_FTP_INFOS';
export const GET_FTP_INFOS_PENDING = 'GET_FTP_INFOS_PENDING';
export const GET_FTP_INFOS_FULFILLED = 'GET_FTP_INFOS_FULFILLED';
export const GET_FTP_INFOS_REJECTED = 'GET_FTP_INFOS_REJECTED';

export interface FtpInfo {
  id: string
  ftpId: string
  ipAddress: string
  port: string
  destPath: string
  fileMode: 'ALLINONE' | 'CASE'
  ftpFileTransferTimeout: string
  ftpFileSize: string
  ftpType: 'FTP' | 'SFTP'
  operatorId: string
  username: string
  userPassword: string
}

export interface FtpInfoState {
  entities: FtpInfo[]
}

export interface GetFtpInfoAction extends AnyAction {
  type: typeof GET_FTP_INFOS
  payload: AxiosPromise<ServerResponse<FtpInfo>>
}

export interface GetFtpInfoPendingAction extends AnyAction {
  type: typeof GET_FTP_INFOS_PENDING
  payload: AxiosResponse<ServerResponse<FtpInfo>>
}
export interface GetFtpInfoFulfilledAction extends AnyAction {
  type: typeof GET_FTP_INFOS_FULFILLED
  payload: AxiosResponse<ServerResponse<FtpInfo>>
}

export interface GetFtpInfoRejectedAction extends AnyAction {
  type: typeof GET_FTP_INFOS_REJECTED
  payload: AxiosResponse<ServerResponse<FtpInfo>>
}

export type FtpInfoActionTypes =
  GetFtpInfoAction |
  GetFtpInfoPendingAction |
  GetFtpInfoFulfilledAction |
  GetFtpInfoRejectedAction;
