import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import {
  getFtpInfos,
} from './ftpInfo.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './ftpInfo.types';

describe('ftpInfo.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of ftpInfos', () => {
    const action: types.FtpInfoActionTypes = {
      type: types.GET_FTP_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockFtpInfos),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getFtpInfos(store.getState())).toEqual(mockFtpInfos);
  });
});
