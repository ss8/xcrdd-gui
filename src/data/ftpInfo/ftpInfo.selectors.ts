import { RootState } from '../root.reducers';
import { FtpInfo } from './ftpInfo.types';

// eslint-disable-next-line import/prefer-default-export
export const getFtpInfos = (state: RootState): FtpInfo[] => state.ftpInfo.entities;
