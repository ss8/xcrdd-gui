import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './ftpInfo.actions';
import * as types from './ftpInfo.types';
import * as api from './ftpInfo.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('ftpInfo.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the ftpInfos', () => {
    const expectedAction = {
      type: types.GET_FTP_INFOS,
      payload: api.getFtpInfos('0'),
    };

    expect(actions.getFtpInfos('0')).toEqual(expectedAction);
  });
});
