import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  DistributorData,
  API_PATH_DISTRIBUTORS,
} from './distributor.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getDistributors(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<DistributorData>> {
  return http.get(`${API_PATH_DISTRIBUTORS}/${deliveryFunctionId}`);
}
