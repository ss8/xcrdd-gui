import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './distributor.actions';
import * as types from './distributor.types';
import * as api from './distributor.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('distributor.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the distributors', () => {
    const expectedAction = {
      type: types.GET_DISTRIBUTORS,
      payload: api.getDistributors('0'),
    };

    expect(actions.getDistributors('0')).toEqual(expectedAction);
  });
});
