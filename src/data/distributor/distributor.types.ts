import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_DISTRIBUTORS = '/distributors';

export const GET_DISTRIBUTORS = 'GET_DISTRIBUTORS';
export const GET_DISTRIBUTORS_FULFILLED = 'GET_DISTRIBUTORS_FULFILLED';
export const GET_DISTRIBUTORS_REJECTED = 'GET_DISTRIBUTORS_REJECTED';

export interface Distributor {
  moduleName: string
  instanceId: number
  afTags: string[]
  listenerIpHint?: string
  destinationIpHint?: string
}

export interface Value {
  field: string
  defaultValue: string | number | boolean
}

// TODO Remove the following interface once backend has fixed the missing camelCase in 'defaultvalue'
export interface ValueLowerCase {
  field: string
  defaultvalue: string | number | boolean
}

export interface DistributorGlobalDefaults {
  listeners: ValueLowerCase[]
  destinations: ValueLowerCase[]
}

export interface DistributorData {
  resource: 'DISTRIBUTORS'
  comments?: string
  globalDefaultValues: DistributorGlobalDefaults
  ss8Distributors: Distributor[]
  afTagsToProtocolMap: {
    [key: string]: {
      afProtocols: string[]
      destinationDefaultValues: Value[]
      listenerDefaultValues: Value[]
    }
  }
}

export interface DistributorState {
  entities: DistributorData[]
}

export interface GetDistributorAction extends AnyAction {
  type: typeof GET_DISTRIBUTORS
  payload: AxiosPromise<ServerResponse<DistributorData>>
}

export interface GetDistributorFulfilledAction extends AnyAction {
  type: typeof GET_DISTRIBUTORS_FULFILLED
  payload: AxiosResponse<ServerResponse<DistributorData>>
}

export type DistributorActionTypes =
  GetDistributorAction |
  GetDistributorFulfilledAction;
