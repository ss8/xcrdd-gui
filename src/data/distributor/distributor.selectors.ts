import { RootState } from '../root.reducers';
import { DistributorData } from './distributor.types';

// eslint-disable-next-line import/prefer-default-export
export const getDistributors = (state: RootState): DistributorData => state.distributor.entities[0];
