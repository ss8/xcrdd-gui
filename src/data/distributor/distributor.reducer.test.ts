import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './distributor.reducer';
import * as types from './distributor.types';

describe('distributor.reducer', () => {
  let distributorData: types.DistributorData[];

  beforeEach(() => {
    distributorData = [{
      resource: 'DISTRIBUTORS',
      globalDefaultValues: {
        listeners: [],
        destinations: [],
      },
      ss8Distributors: [{
        moduleName: 'Ericsson UPF X3',
        instanceId: 1,
        afTags: ['afProtocol1', 'afProtocol2', 'afProtocol3'],
      }],
      afTagsToProtocolMap: {},
    }];
  });

  it('should handle GET_DISTRIBUTORS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_DISTRIBUTORS_FULFILLED,
      payload: buildAxiosServerResponse(distributorData),
    })).toEqual({
      entities: distributorData,
    });
  });
});
