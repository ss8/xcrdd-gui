import * as api from './distributor.api';
import { GET_DISTRIBUTORS, GetDistributorAction } from './distributor.types';

// eslint-disable-next-line import/prefer-default-export
export function getDistributors(deliveryFunctionId: string): GetDistributorAction {
  return {
    type: GET_DISTRIBUTORS,
    payload: api.getDistributors(deliveryFunctionId),
  };
}
