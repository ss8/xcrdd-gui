import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './distributor.api';
import {
  Distributor,
} from './distributor.types';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('distributor.api', () => {
  let distributor: Distributor;

  beforeEach(() => {
    distributor = {
      moduleName: 'Ericsson UPF X3',
      instanceId: 1,
      afTags: ['afProtocol1', 'afProtocol2', 'afProtocol3'],
    };
  });

  it('should request the list for distributors', async () => {
    const response = buildAxiosServerResponse([distributor]);
    axiosMock.onGet('/distributors/0').reply(200, response);
    await expect(api.getDistributors('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/distributors/0' },
      status: 200,
    });
  });
});
