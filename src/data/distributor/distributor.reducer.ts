import {
  GET_DISTRIBUTORS_FULFILLED,
  DistributorActionTypes,
  DistributorState,
} from './distributor.types';

const initialState: DistributorState = {
  entities: [],
};

export default function distributorReducer(
  state = initialState,
  action: DistributorActionTypes,
): DistributorState {
  switch (action.type) {
    case GET_DISTRIBUTORS_FULFILLED:
      return { ...state, entities: action.payload.data.data || [] };

    default:
      return state;
  }
}
