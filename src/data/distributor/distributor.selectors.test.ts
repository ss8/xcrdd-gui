import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import {
  getDistributors,
} from './distributor.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './distributor.types';

describe('distributor.selectors', () => {
  let distributorData: types.DistributorData;
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    distributorData = {
      resource: 'DISTRIBUTORS',
      globalDefaultValues: {
        listeners: [],
        destinations: [],
      },
      ss8Distributors: [{
        moduleName: 'Ericsson UPF X3',
        instanceId: 1,
        afTags: ['afProtocol1', 'afProtocol2', 'afProtocol3'],
      }],
      afTagsToProtocolMap: {},
    };

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of distributors', () => {
    const action: types.DistributorActionTypes = {
      type: types.GET_DISTRIBUTORS_FULFILLED,
      payload: buildAxiosServerResponse([distributorData]),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getDistributors(store.getState())).toEqual(distributorData);
  });
});
