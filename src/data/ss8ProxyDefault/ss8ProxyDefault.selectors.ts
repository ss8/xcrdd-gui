import { createSelector } from 'reselect';
import { RootState } from '../root.reducers';
import { Ss8ProxyDefault, Value, Ss8ProxyDefaultsTemplate } from './ss8ProxyDefault.types';

export const getSs8ProxyDefaults = (state: RootState): Ss8ProxyDefault => state.ss8ProxyDefault.entities[0];

export const getSs8ProxyDefaultInstanceIds = createSelector(
  [getSs8ProxyDefaults],
  (ss8ProxyDefault) => ss8ProxyDefault?.ss8Proxies.map(({ instanceId }) => instanceId) ?? [],
);

export const getSs8ProxyDefaultsGlobalDefaultValues = (state: RootState): Ss8ProxyDefaultsTemplate => {
  const ss8ProxyDefault = state.ss8ProxyDefault.entities[0];

  if (!ss8ProxyDefault) {
    return {};
  }

  return adaptGlobalDefaultValues(state.ss8ProxyDefault.entities[0]);
};
const adaptGlobalDefaultValues = (ss8ProxyDefault: Ss8ProxyDefault) => {
  const { globalDefaultValues } = ss8ProxyDefault;
  const globalDefaultValuesKeys = Object.keys(globalDefaultValues);

  return globalDefaultValuesKeys
    .map(defaultValuesMap(globalDefaultValues))
    .reduce(mergeGlobalDefaultValues, {});
};

const defaultValuesMap = (globalDefaultValues: { [key: string]: Value[] }) => (key: string) => ({
  [key]: globalDefaultValues[key].reduce(defaultValuesEntriesMap, {}),
});

const defaultValuesEntriesMap = (
  previousValue:{[key:string]: string | number | boolean}, currentValue:Value,
): {[key:string]: string | number | boolean} => {

  return ({
    ...previousValue,
    [currentValue.field]: currentValue.defaultvalue,
  });
};

const mergeGlobalDefaultValues = (previousValue: Ss8ProxyDefaultsTemplate, currentValue: Ss8ProxyDefaultsTemplate) => {

  return ({ ...previousValue, ...currentValue });
};
