import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import {
  getSs8ProxyDefaultInstanceIds,
  getSs8ProxyDefaults,
  getSs8ProxyDefaultsGlobalDefaultValues,
} from './ss8ProxyDefault.selectors';
import rootReducer, { RootState } from '../root.reducers';
import * as types from './ss8ProxyDefault.types';

describe('ss8ProxyDefault.selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of ss8ProxyDefaults', () => {
    const action: types.Ss8ProxyDefaultActionTypes = {
      type: types.GET_SS8_PROXY_DEFAULTS_FULFILLED,
      payload: buildAxiosServerResponse(mockSs8ProxyDefaults),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSs8ProxyDefaults(store.getState())).toEqual(mockSs8ProxyDefaults[0]);
  });

  it('should return the list of instance ids', () => {
    const action: types.Ss8ProxyDefaultActionTypes = {
      type: types.GET_SS8_PROXY_DEFAULTS_FULFILLED,
      payload: buildAxiosServerResponse(mockSs8ProxyDefaults),
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getSs8ProxyDefaultInstanceIds(store.getState())).toEqual(['instanceId1', 'instanceId2']);
  });

  describe('getSs8ProxyDefaultsGlobalDefaultValues()', () => {
    it('should return teh Ss8ProxyDefaults Values', () => {
      const action: types.Ss8ProxyDefaultActionTypes = {
        type: types.GET_SS8_PROXY_DEFAULTS_FULFILLED,
        payload: buildAxiosServerResponse(mockSs8ProxyDefaults),
      };
      store = mockStore(rootReducer(undefined, action));
      const expected = {
        destinations: {
          keepaliveInterval: 60, keepaliveRetries: 6, numOfConnections: 1, transport: 'TCP',
        },
        listeners: {
          keepaliveTimeout: 300, maxConcurrentConnection: 128, requiredState: 'ACTIVE', routeRule: 'SRCADDR', transport: 'TCP',
        },
      };
      expect(getSs8ProxyDefaultsGlobalDefaultValues(store.getState())).toEqual(expected);
    });
  });
});
