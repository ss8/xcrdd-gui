import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { buildAxiosServerResponse } from '__test__/utils';
import * as api from './ss8ProxyDefault.api';

const http = Http.getHttp();

const axiosMock = new MockAdapter(http);

describe('ss8ProxyDefault.api', () => {
  it('should request the list for ss8ProxyDefaults', async () => {
    const response = buildAxiosServerResponse(mockSs8ProxyDefaults);
    axiosMock.onGet('/ss8ProxyDefaults/0').reply(200, response);
    await expect(api.getSs8ProxyDefaults('0')).resolves.toEqual({
      config: expect.anything(),
      data: response,
      headers: undefined,
      request: { responseUrl: '/ss8ProxyDefaults/0' },
      status: 200,
    });
  });
});
