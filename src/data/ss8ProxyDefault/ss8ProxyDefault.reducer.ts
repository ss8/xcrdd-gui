import {
  GET_SS8_PROXY_DEFAULTS_FULFILLED,
  Ss8ProxyDefaultActionTypes,
  Ss8ProxyDefaultState,
} from './ss8ProxyDefault.types';

const initialState: Ss8ProxyDefaultState = {
  entities: [],
};

export default function ss8ProxyDefaultReducer(
  state = initialState,
  action: Ss8ProxyDefaultActionTypes,
): Ss8ProxyDefaultState {
  switch (action.type) {
    case GET_SS8_PROXY_DEFAULTS_FULFILLED:
      return {
        ...state,
        entities: action.payload.data.data || [],
      };

    default:
      return state;
  }
}
