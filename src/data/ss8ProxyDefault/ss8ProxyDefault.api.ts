import { AxiosPromise } from 'axios';
import Http from '../../shared/http';
import {
  Ss8ProxyDefault,
  API_PATH_SS8_PROXY_DEFAULTS,
} from './ss8ProxyDefault.types';
import { ServerResponse } from '../types';

const http = Http.getHttp();

// eslint-disable-next-line import/prefer-default-export
export function getSs8ProxyDefaults(
  deliveryFunctionId: string,
): AxiosPromise<ServerResponse<Ss8ProxyDefault>> {
  return http.get(`${API_PATH_SS8_PROXY_DEFAULTS}/${deliveryFunctionId}`);
}
