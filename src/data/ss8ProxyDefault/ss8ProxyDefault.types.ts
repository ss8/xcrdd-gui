import { AnyAction } from 'redux';
import { AxiosPromise, AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';

export const API_PATH_SS8_PROXY_DEFAULTS = '/ss8ProxyDefaults';

export const GET_SS8_PROXY_DEFAULTS = 'GET_SS8_PROXY_DEFAULTS';
export const GET_SS8_PROXY_DEFAULTS_FULFILLED = 'GET_SS8_PROXY_DEFAULTS_FULFILLED';
export const GET_SS8_PROXY_DEFAULTS_REJECTED = 'GET_SS8_PROXY_DEFAULTS_REJECTED';

export interface Ss8Proxy {
  instanceId: string
}

export interface Value {
  field: string
  defaultvalue: string | number | boolean
}

export interface GlobalDefaultValues { [key: string]: Value }

export interface Ss8ProxyDefaultsTemplate {
  [key:string]: {[key:string]: string | number | boolean}
}

export interface Ss8ProxyDefault {
  resource: 'SS8_PROXY_DEFAULTS'
  globalDefaultValues: {[key:string]: Value[]},
  ss8Proxies: Ss8Proxy[]
}

export interface Ss8ProxyDefaultState {
  entities: Ss8ProxyDefault[]
}

export interface GetSs8ProxyDefaultAction extends AnyAction {
  type: typeof GET_SS8_PROXY_DEFAULTS
  payload: AxiosPromise<ServerResponse<Ss8ProxyDefault>>
}

export interface GetSs8ProxyDefaultFulfilledAction extends AnyAction {
  type: typeof GET_SS8_PROXY_DEFAULTS_FULFILLED
  payload: AxiosResponse<ServerResponse<Ss8ProxyDefault>>
}

export type Ss8ProxyDefaultActionTypes =
  GetSs8ProxyDefaultAction |
  GetSs8ProxyDefaultFulfilledAction;
