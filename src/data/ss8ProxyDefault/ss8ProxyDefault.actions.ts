import * as api from './ss8ProxyDefault.api';
import { GET_SS8_PROXY_DEFAULTS, GetSs8ProxyDefaultAction } from './ss8ProxyDefault.types';

// eslint-disable-next-line import/prefer-default-export
export function getSs8ProxyDefaults(deliveryFunctionId: string): GetSs8ProxyDefaultAction {
  return {
    type: GET_SS8_PROXY_DEFAULTS,
    payload: api.getSs8ProxyDefaults(deliveryFunctionId),
  };
}
