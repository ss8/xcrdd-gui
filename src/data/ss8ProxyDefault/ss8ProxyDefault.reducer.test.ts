import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { buildAxiosServerResponse } from '__test__/utils';
import reducer from './ss8ProxyDefault.reducer';
import * as types from './ss8ProxyDefault.types';

describe('ss8ProxyDefault.reducer', () => {
  it('should handle GET_SS8_PROXY_DEFAULTS_FULFILLED', () => {
    expect(reducer(undefined, {
      type: types.GET_SS8_PROXY_DEFAULTS_FULFILLED,
      payload: buildAxiosServerResponse(mockSs8ProxyDefaults),
    })).toEqual({
      entities: mockSs8ProxyDefaults,
    });
  });
});
