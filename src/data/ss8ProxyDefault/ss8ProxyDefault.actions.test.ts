import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import * as actions from './ss8ProxyDefault.actions';
import * as types from './ss8ProxyDefault.types';
import * as api from './ss8ProxyDefault.api';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('ss8ProxyDefault.actions', () => {
  beforeEach(() => {
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should create an action to get the ss8ProxyDefaults', () => {
    const expectedAction = {
      type: types.GET_SS8_PROXY_DEFAULTS,
      payload: api.getSs8ProxyDefaults('0'),
    };

    expect(actions.getSs8ProxyDefaults('0')).toEqual(expectedAction);
  });
});
