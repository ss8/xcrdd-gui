import React, {
  Fragment,
  FunctionComponent, ReactElement,
} from 'react';
import { Route } from 'react-router-dom';
import './lea.scss';
import LeaList from './containers/leaList';
import LeaNew from './containers/leaNew';
import LeaView from './containers/leaView';
import LeaEdit from './containers/leaEdit';

const data = [

  {

    leaId: 1,

    leaName: 'CA-DOJBNE',

    leaDescription: 'California DOJ Bureau of Narcotic Enforcement',

    ipAddress: '106.124.124.198',

    port: 8084,

    timeZone: 'US-Pacific(GMT-07)',

    mainContactName: 'John Doe',

    mainEmailAddress: 'jdoe@dojbne.ca.gov',

    mainPhoneNumber: '(408)321-3345',

    alternateContactName: null,

    alternateEmailAddress: null,

    alternatePhoneNumber: null,

  },

];

const LEA:FunctionComponent = (props:any):ReactElement => {
  return (
    <div className="xcrdd-lea">
      <Route exact path="/xcrdd/lea">
        <LeaList />
      </Route>
      <Route exact path="/xcrdd/lea/new">
        <LeaNew />
      </Route>
      <Route exact path="/xcrdd/lea/view">
        <LeaView />
      </Route>
      <Route exact path="/xcrdd/lea/edit">
        <LeaEdit />
      </Route>
    </div>
  );
};

export default LEA;
