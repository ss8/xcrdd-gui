import { FormLayout, FormLayoutContent, FormLayoutFooter } from 'components/FormLayout';
import React, {
  forwardRef, useRef, useImperativeHandle,
} from 'react';
import FormPanel from 'components/FormPanel';
import FormButtons from 'components/FormButtons';
import Form from 'shared/form';
import FormSection from '../../components/FormSection';

type Props={
    template:any
    data?:any
    formTitle?:string
    isEditing?:boolean
    onClickSave?:any
    onClickEdit?:any
    onClickCancelClose?:any
    noteAfterTitle?:string
    handleUpdate?:any
    breadCrumb?:string
}

export type Ref = {
    isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
    getValues: () => any
  }

const LEAForm = forwardRef<Ref, Props>((
  {
    template,
    data,
    formTitle,
    isEditing = true,
    onClickSave,
    onClickEdit,
    onClickCancelClose,
    noteAfterTitle,
    handleUpdate,
    breadCrumb,
  }:Props,
  ref,
) => {

  const generalRef = useRef<Form>(null);
  const contactsRef = useRef<Form>(null);

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isGeneralValid = generalRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isContactsValid = contactsRef.current?.isValid(forceValidation, forceTouched) ?? true;

    return isGeneralValid && isContactsValid;
  };

  const getValues = () => {
    const formValues = {
      ...generalRef.current?.getValues(),
      ...contactsRef.current?.getValues(),
    };
    return formValues;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const renderBreadCrumbs = (active:string|null = null) => {
    return (
      <ul className="bp3-breadcrumbs" style={{ textTransform: 'capitalize', marginBottom: '21px' }}>
        <li><a className="bp3-breadcrumb bp3-disabled">LEA</a></li>
        <li><a className="bp3-breadcrumb bp3-breadcrumb-current ">{active}</a></li>
      </ul>
    );
  };

  const renderFormPanel = (title: string | null = null) => (
    <FormPanel title={title}>
      <FormButtons
        isEditing={isEditing}
        onClickCancel={onClickCancelClose}
        onClickSave={onClickSave}
        onClickEdit={onClickEdit}
        onClickClose={onClickCancelClose}
      />
    </FormPanel>

  );

  return (
    <FormLayout>
      {renderBreadCrumbs(breadCrumb)}
      <FormLayoutContent>
        {renderFormPanel(formTitle)}
        <FormSection
          ref={generalRef}
          templateSection={template.general}
          name={template.key + Date.now().toString()}
          formSectionName="general"
          data={data}
          onFieldUpdate={handleUpdate}
        />
        <FormSection
          ref={contactsRef}
          title="Contacts"
          noteAfterTitle={noteAfterTitle}
          templateSection={template.contacts}
          name={Date.now().toString()}
          formSectionName="contacts"
          data={data}
          onFieldUpdate={handleUpdate}
        />
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel()}
      </FormLayoutFooter>
    </FormLayout>
  );
});

export default LEAForm;
