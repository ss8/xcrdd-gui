import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { adaptToViewOnly } from 'data/xcrddLea/xcrddLea.adapter';
import { RootState } from 'data/root.reducers';
import { bindActionCreators } from 'redux';
import { goToLeaEdit, goToLeaList } from 'data/xcrddLea/lea.action';
import { withRouter } from 'react-router-dom';
import { History } from 'history';
import formTemplate from '../../lea-template.json';
import LEAForm from '../../components/leaForm';

type Props={
    data:any
    goToEdit:any
    goToList:any
    history:History
}
const LEAView:FunctionComponent<Props> = ({
  data, goToEdit, history, goToList,
}:Props) => {
  const [template, setTemplate] = useState(formTemplate);

  useEffect(() => {
    if (!data?.leaId) {
      goToList(history);
    }
    const adaptedTemplate = adaptToViewOnly(template);
    setTemplate(adaptedTemplate);
  }, []);

  const handleClose = () => {
    goToList(history);
  };

  const handleEdit = () => {
    goToEdit(history);
  };

  if (!template) {
    return null;
  }

  return (
    <LEAForm
      formTitle="View LEA"
      isEditing={false}
      template={template}
      data={data}
      onClickCancelClose={handleClose}
      onClickEdit={handleEdit}
      breadCrumb="View"
    />
  );
};

const mapStateToProps = (state: RootState) => ({
  data: state.xcrddLea.selected[0],
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  goToEdit: goToLeaEdit,
  goToList: goToLeaList,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LEAView));
