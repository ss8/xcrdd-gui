import React, {
  Fragment,
  FunctionComponent, useEffect, useRef, useState,
} from 'react';
import { connect } from 'react-redux';
import { adaptToEdit } from 'data/xcrddLea/xcrddLea.adapter';
import { RootState } from 'data/root.reducers';
import { bindActionCreators } from 'redux';
import { goToLeaList, updateLea } from 'data/xcrddLea/lea.action';
import { withRouter } from 'react-router-dom';
import { History } from 'history';
import { showSuccessMessage, showError } from 'shared/toaster';
import ProgressDialog from 'components/progressDialog';
import formTemplate from '../../lea-template.json';
import LEAForm, { Ref } from '../../components/leaForm';

type Props={
    leaData:any
    goToList:any
    history:History
    updateLEA:any
}
const LEAEdit:FunctionComponent<Props> = ({
  leaData, history, goToList, updateLEA,
}:Props) => {
  const [template, setTemplate] = useState(formTemplate);
  const [data, setData] = useState(leaData);
  const [disable, setDisable] = useState<boolean>(true);
  const [openDialog, setOpenDialog] = useState(false);
  const formRef = useRef<Ref>(null);

  useEffect(() => {
    if (!data?.leaId) {
      goToList(history);
    }
    const adaptedTemplate = adaptToEdit(template);
    setTemplate(adaptedTemplate);
  }, []);

  const handleClose = () => {
    goToList(history);
  };

  const isValid = () => {
    const valid = formRef.current?.isValid(true, true) ?? true;
    const message = 'When entering contacts, please, provide contact name and at least one means of contact';
    const maincontactValidation = () => {
      if (data?.mainContactName) {
        if (data?.mainEmailAddress || data?.mainPhoneNumber) {
          return true;
        }
        showError(message, 5000);
        return false;
      }
      return true;
    };
    const alternateContactValidation = () => {
      if (data?.alternateContactName) {
        if (data?.alternateEmailAddress || data?.alternatePhoneNumber) {
          return true;
        }
        showError(message, 5000);
        return false;
      }
      return true;
    };
    return valid && maincontactValidation() && alternateContactValidation();
  };

  const handleSave = () => {
    const valid = isValid();
    if (valid) {
      const body = { ...formRef.current?.getValues(), leaId: data.leaId };
      setOpenDialog(true);
      updateLEA(body)
        .then(() => {
          showSuccessMessage(`${body.leaName} Successfully updated`, 5000);
          setOpenDialog(false);
          goToList(history);
        })
        .catch(() => {
          showError(`Unable to update ${body.leaName}. Try again later.`, 5000);
          setOpenDialog(false);
          goToList(history);
        });
    }
  };

  const handleUpdate = (event:any) => {
    const { name, value } = event;
    setData((prev:any) => ({ ...prev, [name]: value }));
    setDisable(false);
  };

  if (!template) {
    return null;
  }

  return (
    <Fragment>
      <LEAForm
        ref={formRef}
        formTitle="Edit LEA"
        isEditing
        noteAfterTitle="When entering contacts, please, provide contact name and at least one means of contact"
        template={template}
        data={data}
        onClickCancelClose={handleClose}
        onClickSave={disable ? null : handleSave}
        handleUpdate={handleUpdate}
        breadCrumb="Edit"
      />
      <ProgressDialog
        isOpen={openDialog}
        text={`Updating LEA "${data?.leaName}"...Please wait`}
      />
    </Fragment>
  );
};

const mapStateToProps = (state: RootState) => ({
  leaData: state.xcrddLea.selected[0],
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  goToList: goToLeaList,
  updateLEA: updateLea,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LEAEdit));
