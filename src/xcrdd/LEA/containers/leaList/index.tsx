import { RootState } from 'data/root.reducers';
import React, {
  FunctionComponent, ReactElement, useState,
  useEffect,
  Fragment,
} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CommonGrid from 'shared/commonGrid';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import {
  deleteLea,
  getAllLEAs,
  getSelectedLEA, goToLeaEdit, goToLeaNew, goToLeaView,
} from 'data/xcrddLea/lea.action';
import { withRouter } from 'react-router-dom';
import { History } from 'history';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import { SelectionChangedEvent } from '@ag-grid-community/core';
import ModalDialog from 'shared/modal/dialog';
import { showError, showSuccessMessage } from 'shared/toaster';
import ProgressDialog from 'components/progressDialog';
import leaColumnDefs from '../../lea.types';

type Props={
    createNew:any
    goToEdit:any
    goToView:any
    selectLea:any
    selected:any
    history:History
    getAllLEA:any
    data:any
    deleteLEA:any
}

const dp = async () => {
  return {
    value: {
      data: {
        data: [{
          dateUpdated: '2021-10-19T06:45:29.000+0000',
          id: '1',
          name: 'leaGridSettings',
          setting: '{"columnSetting":[{"colId":"leaName","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"leaDescription","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"ipAddress","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"port","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"mainContactName","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"mainEmailAddress","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"mainPhoneNumber","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null}],"sortSetting":[]}',
          type: 'json',
          user: '1',
        }],
      },
    },
  };
};

const LeaList:FunctionComponent<Props> = ({
  createNew,
  goToEdit,
  goToView,
  selectLea,
  history,
  selected,
  getAllLEA,
  data,
  deleteLEA,
}:Props):ReactElement => {
  const [updatedAt, setUpdatedAt] = useState<Date>();
  const [showDialog, setShowDialog] = useState({
    isOpen: false,
  });
  const [openDialog, setOpenDialog] = useState(false);

  const frameworkComponents = {
    actionMenuCellRenderer: ActionMenuCellRenderer,
  };

  useEffect(() => {
    setUpdatedAt(new Date());
  }, [data]);

  const handleCreateNew = () => {
    createNew(history);
  };

  const handleSelection = (event: SelectionChangedEvent) => {
    return selectLea(event.api.getSelectedRows());
  };

  const handleSelectItemMenuAction = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const actionType = event?.type;
    if (actionType === 'view') {
      return goToView(history);
    } if (actionType === 'edit') {
      return goToEdit(history);
    } if (actionType === 'delete') {
      setShowDialog({
        isOpen: true,
      });
    }
  };

  const handleClose = () => {
    setShowDialog((prev) => ({ ...prev, isOpen: false }));
  };

  const handleGetResultsFromServer = (body:any) => {
    return getAllLEA();
  };

  const handleDelete = () => {
    setOpenDialog(true);
    deleteLEA(selected.leaId)
      .then(() => {
        showSuccessMessage(`${selected.leaName} successfully deleted`, 5000);
        setOpenDialog(false);
      })
      .catch(() => {
        showError(`Unable to delte ${selected.leaName}. Try again later.`, 5000);
        setOpenDialog(false);
      });
  };

  return (
    <Fragment>
      <div className="grid-container" style={{ height: '577px' }}>
        <CommonGrid
          createNewText="New LEA"
          enableBrowserTooltips
          rowData={data}
          rowSelection={ROWSELECTION.SINGLE}
          pagination
          columnDefs={leaColumnDefs}
          noResults={false}
          getGridResultsFromServer={handleGetResultsFromServer}
          getGridSettingsFromServer={dp}
          renderDataFromServer
          hasPrivilegetoView
          hasPrivilegeToCreateNew
          hasPrivilegeToDelete
          handleCreateNew={handleCreateNew}
          onSelectionChanged={handleSelection}
          frameworkComponents={frameworkComponents}
          onActionMenuItemClick={handleSelectItemMenuAction}
          lastUpdatedAt={updatedAt}
        />
      </div>
      <ModalDialog
        width="500px"
        title="Delete LEA"
        isOpen={showDialog.isOpen}
        displayMessage={`Do you want to delete "${selected?.leaName}"?`}
        actionText="Yes, delete it"
        onClose={handleClose}
        onSubmit={handleDelete}
      />
      <ProgressDialog
        isOpen={openDialog}
        text={`Deleting LEA "${selected?.leaName}"...Please wait`}
      />
    </Fragment>
  );
};

const mapStateToProps = (state: RootState) => ({
  selected: state.xcrddLea.selected[0],
  data: state.xcrddLea.leaList,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  createNew: goToLeaNew,
  goToEdit: goToLeaEdit,
  goToView: goToLeaView,
  selectLea: getSelectedLEA,
  getAllLEA: getAllLEAs,
  deleteLEA: deleteLea,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LeaList));
