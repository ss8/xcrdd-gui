import React, {
  FunctionComponent, useState, useRef, Fragment,
} from 'react';
import { createLea, goToLeaList } from 'data/xcrddLea/lea.action';
import { connect } from 'react-redux';
import { History } from 'history';
import { bindActionCreators } from 'redux';
import LEAForm from 'xcrdd/LEA/components/leaForm';
import { withRouter } from 'react-router-dom';
import { showError, showSuccessMessage } from 'shared/toaster';
import ProgressDialog from 'components/progressDialog';
import { Ref } from '../../components/leaForm';
import formTemplate from '../../lea-template.json';

type Props={
  goToList:any
  history:History
  createLEA:any
}

const LeaCreateNew:FunctionComponent <Props> = ({
  goToList,
  history,
  createLEA,
}:Props) => {
  const [template, setTemplate] = useState(formTemplate);
  const [data, setData] = useState<any>();
  const [openDialog, setOpenDialog] = useState(false);
  const [disable, setDisable] = useState<boolean>(true);
  const formRef = useRef<Ref>(null);

  const handleClose = () => {
    goToList(history);
  };

  const isValid = () => {
    const valid = formRef.current?.isValid(true, true) ?? true;
    const message = 'When entering contacts, please, provide contact name and at least one means of contact';
    const maincontactValidation = () => {
      if (data?.mainContactName) {
        if (data?.mainEmailAddress || data?.mainPhoneNumber) {
          return true;
        }
        showError(message, 5000);
        return false;
      }
      return true;
    };
    const alternateContactValidation = () => {
      if (data?.alternateContactName) {
        if (data?.alternateEmailAddress || data?.alternatePhoneNumber) {
          return true;
        }
        showError(message, 5000);
        return false;
      }
      return true;
    };
    return valid && maincontactValidation() && alternateContactValidation();
  };
  const handleSave = () => {
    const valid = isValid();
    if (valid) {
      const body = formRef.current?.getValues();
      setOpenDialog(true);
      createLEA(body)
        .then(() => {
          showSuccessMessage(`${body.leaName} Successfully created`, 5000);
          setOpenDialog(false);
          goToList(history);
        })
        .catch(() => {
          showError(`Unable to create ${body.leaName}. Try again later.`, 5000);
          setOpenDialog(false);
          goToList(history);
        });
    }
  };

  const handleUpdate = (event:any) => {
    const { name, value } = event;
    setData((prev:any) => ({ ...prev, [name]: value }));
    setDisable(false);
  };

  if (!template) {
    return null;
  }
  return (
    <Fragment>
      <LEAForm
        ref={formRef}
        formTitle="New LEA"
        data={data}
        isEditing
        noteAfterTitle="When entering contacts, please, provide contact name and at least one means of contact"
        template={template}
        onClickCancelClose={handleClose}
        onClickSave={disable ? null : handleSave}
        breadCrumb="New"
        handleUpdate={handleUpdate}
      />
      <ProgressDialog
        isOpen={openDialog}
        text={`Creating LEA "${data?.leaName}"...Please wait`}
      />
    </Fragment>

  );
};

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  goToList: goToLeaList,
  createLEA: createLea,
}, dispatch);

export default withRouter(connect(null, mapDispatchToProps)(LeaCreateNew));
