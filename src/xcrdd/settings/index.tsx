import React, {
  useState, FunctionComponent, ReactElement, useEffect, Fragment,
} from 'react';
import './settings.scss';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import {
  Divider,
} from '@blueprintjs/core';
import { getAllTemplates } from 'xcipio/warrants/warrant-actions';
import { bindActionCreators } from 'redux';
import { Route } from 'react-router-dom';
import { getInstanceConfigurations } from 'data/xcrddSettings/settings.action';
import InstanceConfiguration from './containers/instanceConfiguration';
import RDHIServer from './containers/RDHIServer';
import IngestListenerList from './containers/ingestListeners/ingestListenerList';
import IngestListenerView from './containers/ingestListeners/ingestListenerView';

const breadCrumbMap:any = {
  instance: 'Instance',
  ingest: 'Ingest Listeners',
  view: 'View',
  rdhi: 'RDHI Server',
};

const settingMenu = {
  instance: { text: 'Instance', href: '/xcrdd/settings' },
  ingest: { text: 'Ingest Listeners', href: '/xcrdd/settings/ingest' },
};

type Props={
  rdhiEnabled:boolean
  getTemplates:any
  history?:any
  getInstace:any
}

const Settings:FunctionComponent<Props> = ({
  rdhiEnabled,
  getTemplates,
  history,
  getInstace,
}:Props):ReactElement => {
  const [breadCrumbs, setBreadCrumbs] = useState(['instance']);
  const [menuOptions, setMenuOptions] = useState(settingMenu);

  useEffect(() => {
    getTemplates();
    getInstace();
  }, []);

  useEffect(() => {
    if (rdhiEnabled) {
      setMenuOptions((prev) => ({ ...prev, rdhi: { text: 'RDHI Server', href: '/xcrdd/settings/rdhi' } }));
    }
  }, [rdhiEnabled]);

  useEffect(() => {
    let path = history.location.pathname.trim().split('/').filter((val:string[]) => val.length > 0).slice(2);
    if (path.length < 1) {
      path = ['instance'];
    }
    setBreadCrumbs(path);

  }, [history.location]);

  const SideMenu = () => {
    return (
      <Fragment>
        <div className="custom-side-menu-items">System</div>
        {Object.entries(menuOptions).map(
          (val) => {
            const [key, value] = val;
            return (
              <div key={key} className="custom-side-submenu-items">
                <div className={key === breadCrumbs[0] ? 'custom-side-submenu-items-active' : ''} onClick={() => history.push(value.href)}>{value.text}</div>
              </div>
            );
          },
        )}
      </Fragment>
    );
  };

  return (
    <div className="xcrdd-setting">
      <aside>
        <span id="xcrdd-setting-title">Settings</span>
        <SideMenu />
      </aside>
      <Divider />
      <section>
        <ul className="bp3-breadcrumbs" style={{ textTransform: 'capitalize' }}>
          <li><a className="bp3-breadcrumb bp3-disabled">Settings</a></li>
          <li><a className="bp3-breadcrumb">System</a></li>
          {breadCrumbs.map((val, index) => {
            if (index < breadCrumbs.length - 1) {
              return <li key={val}> <a className="bp3-breadcrumb ">{breadCrumbMap[val]}</a></li>;
            }
            return <li key={val}> <a className="bp3-breadcrumb bp3-breadcrumb-current ">{breadCrumbMap[val]}</a></li>;
          })}
        </ul>
        <Route exact path="/xcrdd/settings">
          <InstanceConfiguration />
        </Route>
        <Route exact path="/xcrdd/settings/ingest">
          <IngestListenerList />
        </Route>
        <Route exact path="/xcrdd/settings/ingest/view">
          <IngestListenerView />
        </Route>
        {rdhiEnabled && (
        <Route exact path="/xcrdd/settings/rdhi">
          <RDHIServer />
        </Route>
        )}
      </section>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  rdhiEnabled: state.xcrddSettings.instanceConfig.rdhiEnabled,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  getTemplates: getAllTemplates,
  getInstace: getInstanceConfigurations,

}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
