import React, {
  FunctionComponent, useState,
  useRef,
  useEffect,
} from 'react';
import Form from 'shared/form';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import {
  Button,
} from '@blueprintjs/core';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import * as templateSelectors from 'data/template/template.selectors';
import { FormTemplate } from 'data/template/template.types';
import { showSuccessMessage } from 'shared/toaster';
import { getRDHIConfigurations, setRDHIConfiguration } from 'data/xcrddSettings/settings.action';
import { rdhiState } from 'data/xcrddSettings/settings.types';
import FormSection from '../../../components/FormSection';

type Props={
  rdhiData:rdhiState
  getRDHI:any
  setRDHI:any
  rdhiTemplate:FormTemplate
}

const RDHIServer:FunctionComponent<Props> = (
  {
    rdhiData,
    getRDHI,
    setRDHI,
    rdhiTemplate,
  }:Props,
) => {
  const generalRef = useRef<Form>(null);
  const privateKeyRef = useRef<Form>(null);
  const certificateRef = useRef<Form>(null);
  const [template, setTemplate] = useState<FormTemplate>();
  const [buttonState, setButtonState] = useState({
    disabled: true,
    loading: false,
  });
  const [data, setData] = useState(rdhiData);
  const [areTemplatesMissing, setTemplatesMissing] = useState(false);

  const loadingSkeleton = data.rdhiId ? '' : 'bp3-skeleton';

  useEffect(() => {
    if (rdhiTemplate && Object.keys(rdhiTemplate).length === 0) {
      setTemplatesMissing(true);
    }
    getRDHI();
    setTemplate(rdhiTemplate);
  }, [rdhiTemplate]);

  useEffect(() => {
    setData(rdhiData);
  }, [rdhiData]);

  const getValues = () => {
    const formValues = {
      ...generalRef.current?.getValues(),
      ...privateKeyRef.current?.getValues(),
      ...certificateRef.current?.getValues(),
    };
    return formValues;
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isGeneralValid = generalRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isPrivateKeyValid = privateKeyRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isCertificateValid = certificateRef.current?.isValid(forceValidation, forceTouched) ?? true;
    return isGeneralValid && isPrivateKeyValid && isCertificateValid;
  };

  const handleUpdate = (event:any) => {
    const { name, value } = event;
    setButtonState((prev) => ({ ...prev, disabled: false }));
    setData((prev) => ({ ...prev, [name]: value }));
  };

  const passwordToggle = (e:any) => {
    const { name, type } = e;
    setTemplate((prev:any) => {
      if (type === 'password') {
        const showPassword = { ...prev };
        showPassword.privateKey.fields[name].type = 'text';
        showPassword.privateKey.fields[name].rightElement = 'eye-off';
        return showPassword;
      }
      if (type === 'text') {
        const showPassword = { ...prev };
        showPassword.privateKey.fields[name].type = 'password';
        showPassword.privateKey.fields[name].rightElement = 'eye-open';
        return showPassword;
      }
    });
  };

  const uploaRDHIConfig = () => {
    const valid = isValid(true, true) ?? true;
    if (valid) {
      const values = { ...getValues(), rdhiId: data.rdhiId };
      setButtonState((prev) => ({ ...prev, loading: true }));
      setRDHI(values)
        .then(() => {
          showSuccessMessage('RDHI Sever settings successfully saved', 5000);
          setButtonState({ disabled: true, loading: false });
        });
    }
  };
  if (areTemplatesMissing) {
    return (
      <div>
        <p className="notsupport-text">Template Unavailable</p>
      </div>
    );
  }
  if (!template) {
    return (
      <div>
        <p className="notsupport-text">Template Unavailable</p>
      </div>
    );
  }
  return (
    <div className={loadingSkeleton}>
      <div className="xcrdd-field-title ">RDHI Server</div>
      <FormLayout>
        <FormLayoutContent>
          <FormSection
            ref={generalRef}
            name={template.key}
            formSectionName="general"
            data={data}
            templateSection={template.general}
            shouldValidateOnRender={false}
            onFieldUpdate={handleUpdate}
          />
          <FormSection
            ref={privateKeyRef}
            name={template.key}
            title="Private Key"
            data={data}
            formSectionName="privateKey"
            templateSection={template.privateKey}
            shouldValidateOnRender={false}
            rightElementHandler={passwordToggle}
            onFieldUpdate={handleUpdate}
          />
          <FormSection
            ref={certificateRef}
            name={template.key}
            title="Certificate"
            data={data}
            formSectionName="certificate"
            templateSection={template.certificate}
            shouldValidateOnRender={false}
            onFieldUpdate={handleUpdate}
          />
        </FormLayoutContent>
        <FormLayoutFooter>
          <Button disabled={buttonState.disabled} loading={buttonState.loading} style={{ float: 'right' }} onClick={uploaRDHIConfig} intent="primary">Save</Button>
        </FormLayoutFooter>
      </FormLayout>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  rdhiData: state.xcrddSettings.rdhiConfig,
  rdhiTemplate: templateSelectors.getXCRDDFunctionFormTemplatesByKey(state).RDHIServer,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  getRDHI: getRDHIConfigurations,
  setRDHI: setRDHIConfiguration,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RDHIServer);
