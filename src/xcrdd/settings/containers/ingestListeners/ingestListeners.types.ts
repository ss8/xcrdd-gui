import {
  ColDef, ValueFormatterParams,
} from '@ag-grid-enterprise/all-modules';
import { stateValueFormatter } from 'utils/valueFormatters';
import { moduleNameAndInstanceIdValueGetter } from './utils/ingestValueGetters';

export const ingestListnersColumnDefs: ColDef[] = [
  {
    headerName: '',
    field: 'action',
    cellRenderer: 'actionViewRenderer',
  },
  {
    field: 'name',
    rowGroup: true,
    hide: true,
    lockVisible: true,
    minWidth: 147,
  },
  {
    field: 'moduleName',
    headerName: 'Module/Instance ID',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 160,
    aggFunc: 'first',
    valueGetter: moduleNameAndInstanceIdValueGetter,
    tooltipValueGetter: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }

      return '';
    },
    cellRenderer: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }
      return '';
    },
  },
  {
    headerName: 'Current Status',
    field: 'listener.status',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 147,
    valueFormatter: stateValueFormatter,
    tooltipValueGetter: stateValueFormatter,
    cellRenderer: 'statusCellRenderer',
  },
  {
    headerName: 'Listener Protocol',
    field: 'listener.protocol',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    tooltipField: 'listener protocol',
    flex: 1,
    minWidth: 140,
  },
  {
    headerName: 'Listner IP Address',
    field: 'listener.ipAddress',
    tooltipField: 'ipAddress',
    filter: 'agTextColumnFilter',
    sortable: true,
    resizable: true,
    flex: 1,
    minWidth: 140,
    cellRenderer: 'notApplicableCellRenderer',
  },
  {
    headerName: 'Listener Port',
    field: 'listener.port',
    tooltipField: 'port',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 109,
    cellRenderer: 'notApplicableCellRenderer',
  },
  {
    headerName: 'Configured State',
    field: 'listener.state',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 120,
    valueFormatter: stateValueFormatter,
    tooltipValueGetter: stateValueFormatter,
    cellRenderer: 'fieldStatusRenderer',
  },
];

export const ingestListenersAutoGroupColumnDefs: ColDef =
  {
    headerName: 'Ingest Listener Name',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 180,
    // headerComponentFramework: AccessFunctionGroupHeader,
    filterValueGetter: (params: Partial<ValueFormatterParams>): string => {
      return params.data?.name;
    },
  };
