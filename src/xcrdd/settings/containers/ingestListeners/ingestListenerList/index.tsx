import React, {
  Fragment, FunctionComponent, useEffect, useRef,
  useState,
} from 'react';
import { SelectionChangedEvent } from '@ag-grid-community/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FieldStatusRenderer from 'components/fieldStatusRenderer';
import CommonGrid from 'shared/commonGrid';
import notApplicableCellRenderer from 'components/NotApplicableCellRenderer';
import StatusCellRenderer from 'components/statusCellRenderer';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import { getIngestListenersList, selectIngestListener } from 'data/xcrddSettings/settings.action';
import { RootState } from 'data/root.reducers';
import { Button } from '@blueprintjs/core';
import { ingestListeners } from 'data/xcrddSettings/settings.types';
import { ingestListenersAutoGroupColumnDefs, ingestListnersColumnDefs } from '../ingestListeners.types';
import IngestActionViewRenderer from '../utils/ingestActionViewRenderer';

type Props = {
  selectIngestListeners: any,
  getIngestListeners:any
  data:Partial<ingestListeners>[]
};

const dp = async () => {
  return {
    value: {
      data: {
        data: [{
          dateUpdated: '2021-10-19T06:45:29.000+0000',
          id: '1',
          name: 'ingestGridSettings',
          setting: '{"columnSetting":[{"colId":"action","hide":false,"aggFunc":null,"width":30,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"ag-Grid-AutoColumn","hide":false,"aggFunc":null,"width":190,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"name","hide":true,"aggFunc":null,"width":190,"pivotIndex":null,"pinned":null,"rowGroupIndex":0},{"colId":"moduleName","hide":false,"aggFunc":"first","width":150,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"listener.status","hide":false,"aggFunc":"first","width":120,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"listener.protocol","hide":false,"aggFunc":null,"width":132,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"listener.ipAddress","hide":false,"aggFunc":null,"width":120,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"listener.port","hide":false,"aggFunc":null,"width":110,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"listener.state","hide":false,"aggFunc":null,"width":150,"pivotIndex":null,"pinned":null,"rowGroupIndex":null}],"sortSetting":[],"version":"1"}',
          type: 'json',
          user: '1',
        }],
      },
    },
  };
};

const IngestListeners:FunctionComponent<Props> = ({
  selectIngestListeners,
  getIngestListeners,
  data,
}:Props) => {

  const [expandAll, setExpandAll] = useState(true);
  const [updatedAt, setUpdatedAt] = useState<Date>();
  const gridRef = useRef<any>(null);

  useEffect(() => {
    getIngestListeners();
    setUpdatedAt(new Date());
  }, []);

  const frameworkComponents = {
    statusCellRenderer: StatusCellRenderer,
    actionViewRenderer: IngestActionViewRenderer,
    fieldStatusRenderer: FieldStatusRenderer,
    notApplicableCellRenderer,
  };

  const handleSelection = (event: SelectionChangedEvent) => {
    return selectIngestListeners(event.api.getSelectedRows());
  };

  const handleGrid = () => {
    if (expandAll) {
      gridRef?.current?.gridApi?.expandAll();
      setExpandAll(false);
    } else {
      gridRef?.current?.gridApi?.collapseAll();
      setExpandAll(true);
    }
  };

  const handleRefresh = () => {
    getIngestListeners();
    setUpdatedAt(new Date());
  };

  return (
    <Fragment>
      <div className="xcrdd-field-title ">Ingest Listeners</div>

      <div className="grid-container" style={{ height: '549px' }}>
        <CommonGrid
          ref={gridRef}
          pagination
          enableBrowserTooltips
          columnDefs={ingestListnersColumnDefs}
          frameworkComponents={frameworkComponents}
          noResults={false}
          onRefresh={handleRefresh}
          rowData={data}
          onSelectionChanged={handleSelection}
          autoGroupColumnDef={ingestListenersAutoGroupColumnDefs}
          getGridSettingsFromServer={dp}
          suppressAggFuncInHeader
          groupSelectsChildren
          suppressRowClickSelection
          enableGroupSelection
          rowSelection={ROWSELECTION.MULTIPLE}
          lastUpdatedAt={updatedAt}
        >
          <Button onClick={handleGrid} disabled={data.length <= 0}>{expandAll ? 'Expand All' : 'Collapse All'}</Button>
        </CommonGrid>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state: RootState) => ({
  data: state.xcrddSettings.ingestListeners,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  selectIngestListeners: selectIngestListener,
  getIngestListeners: getIngestListenersList,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(IngestListeners);
