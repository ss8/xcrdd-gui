import React, {
  Fragment, FunctionComponent, ReactElement, useState,
} from 'react';
import Form from 'shared/form';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import { FormLayout, FormLayoutContent } from 'components/FormLayout';
import { Button, Callout } from '@blueprintjs/core';
import { bindActionCreators } from 'redux';
import { goToIngestListenersList } from 'data/xcrddSettings/settings.action';
import { withRouter } from 'react-router-dom';
import { History } from 'history';
import template from './ingestListenerView.json';
import MultiFacetedFormSection from './components/multiFacetedForm';
import './ingestListenerView.scss';

type Props={
  data:any
  goToListeners:any
  history:History
}

const IngestListenerView:FunctionComponent<Props> = ({ data, goToListeners, history }:Props):ReactElement => {
  const handleGotOListeners = () => goToListeners(history);

  return (
    <div>
      <FormLayout>
        <FormLayoutContent>
          <div className="xcrdd-field-title ">
            <span>View Ingest Listener</span>
            <Button intent="primary" onClick={handleGotOListeners} style={{ float: 'right' }}>Back</Button>
          </div>
          {data?.length <= 0 ? (
            <Callout
              intent="danger"
              title="Ingest Listeners data is unavailable"
            >
              unable to load ingest listener data
            </Callout>
          )
            : (
              <Fragment>
                <Form
                  layout={template.general.metadata.layout?.rows}
                  name={template.key + Date.now().toString()}
                  fields={template.general.fields}
                  defaults={data[0]}
                />
                <MultiFacetedFormSection
                  title="Listeners"
                  template={template.listener}
                  formSection="listener"
                  data={data}
                />
                <MultiFacetedFormSection
                  title="Destinations"
                  template={template.destination}
                  formSection="destination"
                  data={data}
                />
              </Fragment>
)}
        </FormLayoutContent>
      </FormLayout>
      <br />
      <br />
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  data: state.xcrddSettings.selected,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  goToListeners: goToIngestListenersList,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IngestListenerView));
