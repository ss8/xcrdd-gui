import React, { FunctionComponent, ReactElement } from 'react';

import { Dialog, Classes, Button } from '@blueprintjs/core';
import { AgGridReact } from '@ag-grid-community/react';

type Props={
    isOpen:boolean
    handelDialog:any
    data:any
}

const ipTableColumnDefs = [
  {
    headerName: 'Client IP Address',
    field: 'ip',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'Client Name',
    field: 'client',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    tooltipField: 'listener protocol',
    flex: 1,
  },
];

const AllowedIPDialog :FunctionComponent<Props> = ({
  isOpen, handelDialog, data,
}:Props):ReactElement => {
  const { name, ipAddress, port } = data;
  return (
    <Dialog
      autoFocus
      isOpen={isOpen}
      title="Listener - Allowed IP Addresses List"
      className="edit-allowed-ips-dialog"
      onClose={handelDialog}
    >
      <div className={Classes.DIALOG_BODY}>
        <div className="dialog-fields">
          <div>
            <b>Ingest Listener Name</b>
            <span>{name}</span>
          </div>
          <div>
            <b>Listening IP Addresses</b>
            <span>{ipAddress}</span>
          </div>
          <div>
            <b>Listening Port</b>
            <span>{port}</span>
          </div>
        </div>
        <div style={{ marginBottom: '24px' }}>
          <p><strong>Allowed Clients IPs List</strong></p>
          <p className="edit-allowed-ips-dialog-listener-explanation">
            This listener will only listen to the IP addresses in the list below.
            When the list is empty it will accept connections from any client.
          </p>
        </div>
        <div className="ag-theme-balham allowed-ips-update-table allowed-ips-update0-table">
          <AgGridReact
            floatingFilter
            stopEditingWhenGridLosesFocus
            columnDefs={ipTableColumnDefs}
            rowData={data?.allowedIPAddresses}
          />
        </div>

      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button intent="primary" onClick={handelDialog}>Close</Button>
        </div>
      </div>
    </Dialog>
  );
};

export default AllowedIPDialog;
