import React, {
  Fragment,
  FunctionComponent, ReactElement, useState,
} from 'react';
import MultiTab from 'shared/multi-tab';
import Form from 'shared/form';
import { TabProp } from 'shared/multi-tab/MultiTab';
import FieldCurrentState from 'components/FieldCurrentState';
import { Button } from '@blueprintjs/core';
import { ingestListener } from 'data/xcrddSettings/settings.types';
import AllowedIPDialog from './allowedIPDialog';

type Props = {
  id?:string
  title: string,
  template:any
  formSection:string
  data:any
};

const MultiFacetedForm:FunctionComponent <Props> = ({
  id, title, template, formSection, data,
}:Props):ReactElement => {

  const { fields } = template;
  const { layout } = template.metadata;
  const [showDialog, setShowDialog] = useState(false);
  const [IPData, setIPData] = useState({});

  const handleShowDialog = (tabData:Partial<ingestListener>) => {
    setShowDialog(true);
    setIPData(tabData);
  };
  const handelDialog = () => {
    setShowDialog(false);
  };

  const createInterfaceTab = (
    tabData:Partial<ingestListener> = {},
    index: number,
  ): TabProp | undefined => {

    const AllowedIP = () => {
      return (
        <div>
          <Button onClick={() => handleShowDialog(tabData)}>Allowed IP Addresses</Button>
          {!(tabData.allowedIPAddresses?.length) && <span style={{ marginLeft: '12px' }}>All IP addresses are allowed</span>}
        </div>
      );
    };
    const customFieldComponentMap = {
      ingestStatus: FieldCurrentState,
      allowedIP: AllowedIP,
    };

    if (!template) {
      return;
    }
    if (tabData.id == null) {
      return;
    }

    const panel = (
      <Fragment>
        <Form
          name={tabData.id + Date.now().toString()}
          defaults={tabData}
          fields={fields}
          layout={layout?.rows}
          collapsibleLayout={template.metadata.layout?.collapsible}
          customFieldComponentMap={customFieldComponentMap}
        />
      </Fragment>
    );

    return {
      id: tabData.id ?? `id${Date.now().toString()}`,
      title: `${formSection} ${index + 1}`,
      panel,
      hasErrors: false,
    };
  };

  const tabs = data?.map((val:any, index:number) => createInterfaceTab(val[formSection], index))
  .filter((entry:any): entry is TabProp => entry != null) ?? [];

  return (
    <div className="multi-tab-form-section" data-testid="MultiTabFormSectionIL">
      <h3>{title}</h3>
      <MultiTab
        tabContainerId={template.id ?? '1'}
        tabs={tabs}
        isEditing={false}
      />
      {(formSection === 'listener' && showDialog) && (
      <AllowedIPDialog
        isOpen={showDialog}
        handelDialog={handelDialog}
        data={{ ...IPData, name: data[0].name }}
      />
      )}
    </div>
  );
};

export default MultiFacetedForm;
