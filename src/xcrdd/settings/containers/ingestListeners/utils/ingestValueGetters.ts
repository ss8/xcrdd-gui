import { ValueGetterParams, ITooltipParams } from '@ag-grid-enterprise/all-modules';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';

// eslint-disable-next-line import/prefer-default-export
export function moduleNameAndInstanceIdValueGetter(
  { data }: Partial<ValueGetterParams> | Partial<ITooltipParams>,
): string {
  const distributorConfig = data as DistributorConfiguration;
  const {
    moduleName,
    instanceId,
  } = distributorConfig;

  return `${moduleName}/${instanceId}`;
}
