import React from 'react';
import {
  Icon,
} from '@blueprintjs/core';
import { useHistory } from 'react-router-dom';

const IngestActionViewRenderer = (params:any) => {
  console.log(params);
  const history = useHistory();
  if (params.node.level !== 0) {
    return <span />;
  }
  return <Icon icon="eye-open" style={{ color: '#5C7080' }} onClick={() => { history.push('/xcrdd/settings/ingest/view'); }} />;
};

export default IngestActionViewRenderer;
