import React, {
  useState, FunctionComponent, ReactElement, useEffect, Fragment,
} from 'react';
import './instanceConfiguration.scss';
import {
  Button,
  ControlGroup,
  Label,
  MultiSlider,
  NumericInput,
  HandleInteractionKind,
} from '@blueprintjs/core';
import CHTMLSelect from 'components/html-select';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showSuccessMessage } from 'shared/toaster';
import { setRetentionConfiguration } from 'data/xcrddSettings/settings.action';

const durationUnitOptions = [
  { label: 'day(s)', value: 'DAYS' },
  { label: 'hour(s)', value: 'HOURS' },
  { label: 'month(s)', value: 'MONTHS' },
  { label: 'year(s)', value: 'YEARS' },
];

type retentionState={
  duration:number,
  durationUnit:string,
  fetching?:boolean
  minValue:number,
  maxValue:number,
  successMessage?:boolean,
  invalid:boolean
  disabled:boolean
  disableDiskDefault:boolean
}

type Props={
  instanceSettings: any,
  setInstance:any
}

const InstanceConfiguration:FunctionComponent<Props> = ({
  instanceSettings,
  setInstance,
}:Props):ReactElement => {
  const {
    instanceId,
    name,
    provisioningServer,
    retentionTime,
    retentionTimeUnit,
    rdhiEnabled,
    diskCapDefaultLevel1,
    diskCapUpdatedLevel1,
    diskCapDefaultLevel2,
    diskCapUpdatedLevel2,
  } = instanceSettings;

  const [retention, setRetention] = useState<retentionState>({
    duration: 0,
    durationUnit: '',
    minValue: 0,
    maxValue: 0,
    fetching: false,
    invalid: false,
    disabled: true,
    disableDiskDefault: true,
  });

  const loadingSkeleton = instanceId ? '' : 'bp3-skeleton';
  const invalidInput = retention.invalid ? { borderColor: '#de4343' } : {};
  const instanceFeilds = [
    { name: 'CNF Name', value: name },
    { name: 'URL', value: provisioningServer },
    { name: 'RDHI', value: rdhiEnabled ? 'Enabled' : 'Disabled' },
  ];

  useEffect(() => {
    const minValue = diskCapUpdatedLevel1 || diskCapDefaultLevel1;
    const maxValue = diskCapUpdatedLevel2 || diskCapDefaultLevel2;
    let disableDiskDefault = false;
    if (minValue === diskCapDefaultLevel1 && maxValue === diskCapDefaultLevel2) {
      disableDiskDefault = true;
    }
    setRetention((prev) => ({
      ...prev, minValue, maxValue, duration: retentionTime, durationUnit: retentionTimeUnit, disableDiskDefault,
    }));
  }, [retentionTime,
    retentionTimeUnit,
    diskCapDefaultLevel1,
    diskCapUpdatedLevel1,
    diskCapDefaultLevel2,
    diskCapUpdatedLevel2]);

  const handleRetention = (e:any) => {
    const { name, value } = e.target;
    let invalid = false;
    const reg = /^\d+$/;
    if (name === 'duration') {
      invalid = !(value > 0 && reg.test(value));
    }
    setRetention((prev) => {
      return {
        ...prev, [name]: value, disabled: invalid, successMessage: false, invalid,
      };
    });
  };

  const updateRetentionSettings = () => {
    if (!retention.invalid) {
      setRetention((prev) => ({ ...prev, fetching: true }));
      const body = {
        retentionTime: retention.duration,
        retentionTimeUnit: retention.durationUnit,
        diskCapUpdatedLevel1: retention.minValue === diskCapDefaultLevel1 ? null : retention.minValue,
        diskCapUpdatedLevel2: retention.maxValue === diskCapDefaultLevel2 ? null : retention.maxValue,
      };

      setInstance(instanceId, body)
        .then(() => {
          setRetention((prev:any) => ({
            ...prev, fetching: false, disabled: true,
          }));
          showSuccessMessage('Instance settings successfully saved', 5000);
        });
    }
  };

  const handleSlider = (values:number[]) => {
    const [minValue, maxValue] = values;
    setRetention((prev) => ({
      ...prev, disabled: false, disableDiskDefault: false, minValue, maxValue,
    }));
  };

  const handleSliderDefault = () => {
    setRetention((prev) => ({
      ...prev,
      disabled: false,
      disableDiskDefault: true,
      minValue: diskCapDefaultLevel1,
      maxValue: diskCapDefaultLevel2,
    }));
  };

  return (
    <Fragment>
      <div className="xcrdd-field-title ">Instance</div>
      <div className="xcrdd-instance-configuration">
        <div>
          {instanceFeilds.map((item) => {
            return (
              <div key={item.name} className={`xcrdd-instance-feilds ${loadingSkeleton}`}>
                <span>{item.name}</span>
                <span>{item.value}</span>
              </div>
            );
          })}
          <div className={`xcrdd-instance-retention ${loadingSkeleton}`}>
            <b>Retention Duration</b>
            <Label>
              Retention Time
            </Label>
            <ControlGroup>
              <NumericInput
                name="duration"
                buttonPosition="none"
                style={invalidInput}
                min={1}
                onInput={handleRetention}
                value={retention.duration}
              />
              <CHTMLSelect
                id="durationUnit"
                options={durationUnitOptions}
                value={retention.durationUnit}
                iconProps={{ icon: 'caret-down' }}
                onChange={handleRetention}
              />
            </ControlGroup>
            <span className="xcrdd-settings-footnote">Only non-decimal values.</span>
            {retention.invalid && <span className="retention-invalid">Number must be non decimal and greater than 0.</span>}
          </div>

          <div className={`xcrdd-settings-disk-threshold ${loadingSkeleton}`}>
            <b>Disk Capacity Threshold Levels</b>
            <p>Disk Capacity between <strong>{retention.minValue}%</strong> and
              <strong> {retention.maxValue}%</strong> will throw attention alarm.
              Above <strong>{retention.maxValue}%</strong> will be require major attention.
              And when Dist Capacity reaches <strong>100%</strong>, it will be critical and there will be data drop.
            </p>
            <MultiSlider
              max={100}
              labelStepSize={25}
              labelRenderer={(value) => `${value}%`}
              onChange={handleSlider}
            >
              <MultiSlider.Handle
                type="start"
                value={retention.minValue}
                interactionKind={HandleInteractionKind.PUSH}
              />
              <MultiSlider.Handle
                type="end"
                value={retention.maxValue}
                interactionKind={HandleInteractionKind.PUSH}
              />
            </MultiSlider>
            <p style={{ marginTop: '12px', marginBottom: '12px' }}>Use the sliders to change default thresholds.</p>
            <Button
              onClick={handleSliderDefault}
              disabled={retention.disableDiskDefault}
            >
              Reset to default
            </Button>
          </div>
          <div className="xcrdd-settings-retention-save">
            <Button
              disabled={retention.disabled}
              loading={retention.fetching}
              intent="primary"
              onClick={updateRetentionSettings}
            >Save
            </Button>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state: any) => ({
  instanceSettings: state.xcrddSettings.instanceConfig,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  // getInstance: getInstanceConfigurations,
  setInstance: setRetentionConfiguration,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InstanceConfiguration);
