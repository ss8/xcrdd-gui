import React, { FunctionComponent, ReactElement } from 'react';

const ConnectionStatus:FunctionComponent = (props:any):ReactElement => {
  return (
    <div>
      <p className="notsupport-text">Feature will soon be available</p>
    </div>
  );
};

export default ConnectionStatus;
