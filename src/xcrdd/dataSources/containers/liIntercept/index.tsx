import React, {
  FunctionComponent, ReactElement, useEffect, useState,
} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLIIntercept, getSelectedLIIntercept } from 'data/xcrddDataSource/dataSource.action';
import CommonGrid from 'shared/commonGrid';
import DateTimeCellRenderer from 'shared/commonGrid/date-cell-renderer';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import { RootState } from 'data/root.reducers';
import { SelectionChangedEvent } from '@ag-grid-community/core';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import { Button } from '@blueprintjs/core';
import liInterceptColDef from '../../dataSources.types';
import HeaderCheckbox from '../../components/headerCheckbox';

type Props={
    getLIIntercepts:any,
    data:any,
    setLIIntercepts:any
}

const dp = async () => {
  return {
    value: {
      data: {
        data: [{
          dateUpdated: '2021-10-19T06:45:29.000+0000',
          id: '1',
          name: 'leaGridSettings',
          setting: '{"columnSetting":[{"colId":"action","hide":false,"aggFunc":null,"width":30,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"liid","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"lea","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"priority","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"case_name","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"warrant_name","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"first_received_iri","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"last_received_iri","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null}, {"colId":"first_received_cc","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"last_received_cc","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"pdu_count","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null},{"colId":"cc_bytes_collected","hide":false,"aggFunc":null,"width":200,"pivotIndex":null,"pinned":null,"rowGroupIndex":null}],"sortSetting":[]}',
          type: 'json',
          user: '1',
        }],
      },
    },
  };
};

const LIIntercept:FunctionComponent<Props> = ({
  getLIIntercepts,
  data,
  setLIIntercepts,
}:Props):ReactElement => {
  const [updatedAt, setUpdatedAt] = useState<Date>();

  useEffect(() => {
    setUpdatedAt(new Date());
  }, [data]);

  const frameworkComponents = {
    datetimeCellRenderer: DateTimeCellRenderer,
    actionMenuCellRenderer: ActionMenuCellRenderer,
    headerCheckbox: HeaderCheckbox,
  };

  const handleGetResultsFromServer = (body:any) => {
    return getLIIntercepts();
  };

  const handleSelection = (event: SelectionChangedEvent) => {
    return setLIIntercepts(event.api.getSelectedRows());
  };

  return (
    <div className="grid-container" style={{ height: '540px' }}>
      <CommonGrid
        enableBrowserTooltips
        columnDefs={liInterceptColDef}
        rowData={data}
        pagination
        frameworkComponents={frameworkComponents}
        onSelectionChanged={handleSelection}
        noResults={false}
        getGridResultsFromServer={handleGetResultsFromServer}
        getGridSettingsFromServer={dp}
        renderDataFromServer
        rowSelection={ROWSELECTION.MULTIPLE}
        enableGroupSelection
        lastUpdatedAt={updatedAt}
      >
        <Button style={{ marginRight: '16px' }}>Purge Data</Button>
        <Button>Change Priority</Button>
      </CommonGrid>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  data: state.xcrddDataSources.liIntercepts,
});

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  getLIIntercepts: getLIIntercept,
  setLIIntercepts: getSelectedLIIntercept,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LIIntercept);
