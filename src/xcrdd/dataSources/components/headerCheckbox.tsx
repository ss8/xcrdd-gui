import { Checkbox } from '@blueprintjs/core';
import React, { FunctionComponent, ReactElement } from 'react';

const HeaderCheckbox:FunctionComponent = (params:any):ReactElement => {

  const handleSelectAll = (event:any) => {
    const { checked } = event.target;
    params.api.forEachNode((node:any) => {
      node.setSelected(checked);
    });
  };

  return (
    <div>
      <Checkbox
        onChange={handleSelectAll}
      />
    </div>
  );
};

export default HeaderCheckbox;
