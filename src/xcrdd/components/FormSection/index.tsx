import React, {
  forwardRef,
} from 'react';
import lodashGet from 'lodash.get';
import Form from '../../../shared/form';
import './form-section.scss';

type Props = {
  name: string,
  title?: string,
  noteAfterTitle?:string
  templateSection: any | undefined,
  data: any,
  formSectionName: any,
  dataSection?: string | undefined,
  showRemoveButton?: boolean,
  shouldValidateOnRender?: boolean
  rightElementHandler?:any,
  onFieldUpdate?: any,
  onRemoveButtonClick?: () => void,
}

const FormSection = forwardRef<Form, Props>(({
  name,
  title,
  noteAfterTitle,
  templateSection,
  data,
  formSectionName,
  rightElementHandler,
  dataSection = undefined,
  shouldValidateOnRender = false,
  onFieldUpdate = () => null,
}:Props, ref) => {

  if (!templateSection) {
    return null;
  }

  const renderTitle = () => {
    if (!title) {
      return null;
    }

    return (
      <div className="form-section-header">
        <h3 className="form-section-title">{title}</h3>
      </div>
    );
  };

  const renderNoteText = () => {
    if (!title) {
      return null;
    }

    return (
      <span className="form-heading-note ">{noteAfterTitle}</span>
    );
  };

  const formName = name + Date.now();

  let formData = data;

  if (dataSection != null) {
    formData = lodashGet(data, dataSection);
  }

  return (
    <div data-testid={`FormSection-${formSectionName}`}>
      {renderTitle()}
      {renderNoteText()}
      <Form
        ref={ref}
        name={formName}
        defaults={formData}
        fields={templateSection.fields}
        layout={templateSection.metadata.layout?.rows}
        collapsibleLayout={templateSection.metadata.layout?.collapsible}
        fieldUpdateHandler={onFieldUpdate}
        shouldValidateOnRender={shouldValidateOnRender}
        rightElementHandler={rightElementHandler}
      />
    </div>
  );
});

export default FormSection;
