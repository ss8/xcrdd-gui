import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WarrantNavigation from './xcipio/warrants/warrant-navigation';
import UsersNavigation from './users/users-navigation';
import DashboardNavigation from './dashboard/dashboard-navigation';
import ContactsNavigation from './xcipio/contacts/contacts-navigation';
import { setTemplate } from './xcipio/warrants/warrant-actions';

const isConfigPath = (pathname: string) => {
  const uri = pathname.split('/');
  const isConfig = uri.indexOf('config') > -1;
  return isConfig;
};

export const navigateToConfig = (pathname: string, ctx: any, subRoute: string) => {
  const isConfig: boolean = isConfigPath(pathname);
  if (!isConfig && !subRoute) {
    ctx.props.history.push(`${pathname}/config`);
  } else if (!isConfig) {
    ctx.props.history.push(`${pathname}/config/${subRoute}`);
  }
};

const subnavMap: any = {
  warrants: WarrantNavigation,
  users: UsersNavigation,
  dashboard: DashboardNavigation,
  contacts: ContactsNavigation,
};

const getComponent = (location: string) => subnavMap[location];

const Subnav = (props: any) => {
  const path = props.location.pathname.split('/')[1];
  const Component = getComponent(path);
  if (!Component) return null;
  return (<Component {...props} />);
};

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  setTemplate,
}, dispatch);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Subnav));
