import { mockTimeZones } from '__test__/mockTimeZones';
import { getSortedTimeZoneValueLabels, getTimeZoneLabel, getSortedTimeZoneLabels } from './timeZone.utils';

describe('getSortedTimeZoneLabels', () => {

  it('return sorted timezone labels when no date is provided', () => {
    const sortedTimeZones = getSortedTimeZoneLabels(mockTimeZones);
    expect(sortedTimeZones).toEqual([
      'US/Alaska',
      'US/Arizona',
      'US/Central',
      'US/Eastern',
      'US/Hawaii',
      'US/Mountain',
      'US/Pacific',
    ]);
  });
});

describe('getSortedTimeZoneValueLabels', () => {
  it('return sorted timezone values and labels when no date is provided', () => {
    const sortedTimeZones = getSortedTimeZoneValueLabels(null, mockTimeZones);
    expect(sortedTimeZones).toEqual([
      expect.objectContaining({ label: expect.stringMatching(/(US\/Alaska \(GMT-09\)|US\/Alaska \(GMT-08\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Arizona \(GMT-07\)|US\/Arizona \(GMT-07\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Central \(GMT-06\)|US\/Central \(GMT-05\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Eastern \(GMT-05\)|US\/Eastern \(GMT-04\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Hawaii \(GMT-10\)|US\/Hawaii \(GMT-10\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Mountain \(GMT-07\)|US\/Mountain \(GMT-06\))/) }),
      expect.objectContaining({ label: expect.stringMatching(/(US\/Pacific \(GMT-08\)|US\/Pacific \(GMT-07\))/) }),
    ]);
  });

  it('return sorted timezone values and labels as of Nov 7, 2021 at 2 AM', () => {
    const date = new Date('2021-11-07T02:00:00');
    const sortedTimeZones = getSortedTimeZoneValueLabels(date, mockTimeZones);
    expect(sortedTimeZones).toEqual([
      expect.objectContaining({ label: 'US/Alaska (GMT-09)' }),
      expect.objectContaining({ label: 'US/Arizona (GMT-07)' }),
      expect.objectContaining({ label: 'US/Central (GMT-06)' }),
      expect.objectContaining({ label: 'US/Eastern (GMT-05)' }),
      expect.objectContaining({ label: 'US/Hawaii (GMT-10)' }),
      expect.objectContaining({ label: 'US/Mountain (GMT-07)' }),
      expect.objectContaining({ label: 'US/Pacific (GMT-08)' }),

    ]);
  });

  it('return sorted timezone values and labels as of March 14, 2021 at 2 AM', () => {
    const date = new Date('2021-03-14T02:00:00');
    const sortedTimeZones = getSortedTimeZoneValueLabels(date, mockTimeZones);
    expect(sortedTimeZones).toEqual([
      expect.objectContaining({ label: 'US/Alaska (GMT-08)' }),
      expect.objectContaining({ label: 'US/Arizona (GMT-07)' }),
      expect.objectContaining({ label: 'US/Central (GMT-05)' }),
      expect.objectContaining({ label: 'US/Eastern (GMT-04)' }),
      expect.objectContaining({ label: 'US/Hawaii (GMT-10)' }),
      expect.objectContaining({ label: 'US/Mountain (GMT-06)' }),
      expect.objectContaining({ label: 'US/Pacific (GMT-07)' }),
    ]);
  });

  it('return sorted timezone values and labels as of Nov 7, 2021 at 2 AM for UK timezones', () => {
    const date = new Date('2021-11-07T02:00:00');
    const timeZones = [
      {
        value: 'Etc/GMT',
        label: 'UTC',
      },
      {
        value: 'Europe/London',
        label: 'UK',
      },
    ];

    const sortedTimeZones = getSortedTimeZoneValueLabels(date, timeZones);
    expect(sortedTimeZones).toEqual([
      expect.objectContaining({ label: 'UK (GMT+00)' }),
      expect.objectContaining({ label: 'UTC' }),
    ]);
  });

  it('return sorted timezone values and labels as of March 28, 2021 at 3 AM UK timezones', () => {
    const date = new Date('2021-03-28T03:00:00');
    const timeZones = [
      {
        value: 'Etc/GMT',
        label: 'UTC',
      },
      {
        value: 'Europe/London',
        label: 'UK',
      },
    ];

    const sortedTimeZones = getSortedTimeZoneValueLabels(date, timeZones);
    expect(sortedTimeZones).toEqual([
      expect.objectContaining({ label: 'UK (GMT+01)' }),
      expect.objectContaining({ label: 'UTC' }),
    ]);
  });

});

describe('getTimeZoneLabel', () => {
  it('return proper label when timezone is not recognized', () => {
    const timeZoneListHasUnknown = [
      {
        value: 'Unknown Timezone',
        label: 'US/Pacific',
      }];
    const label = getTimeZoneLabel(null, 'Unknown Timezone', timeZoneListHasUnknown);
    const expected = 'US/Pacific';
    expect(label).toEqual(expect.stringMatching(expected));
  });

  it('return proper label for US timezones when no date is provided', () => {
    const label = getTimeZoneLabel(null, 'America/Los_Angeles', mockTimeZones);
    const expected = /(US\/Pacific \(GMT-07\)|US\/Pacific \(GMT-08\))/;
    expect(label).toEqual(expect.stringMatching(expected));
  });

  it('should not add GMT+0 if timezone is Etc/GMT', () => {
    const timezones = [{
      value: 'Etc/GMT',
      label: 'UTC',
    }];
    const label = getTimeZoneLabel(null, 'Etc/GMT', timezones);
    expect(label).toEqual('UTC');
  });

  describe('return proper label for US timezones as of Nov 7, 2021 at 2 AM', () => {
    const date = new Date('2021-11-07T02:00:00');
    it('return proper label for America/Los_Angeles timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Los_Angeles', mockTimeZones);
      expect(label).toEqual('US/Pacific (GMT-08)');
    });
    it('return proper label for America/Phoenix timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Phoenix', mockTimeZones);
      expect(label).toEqual('US/Arizona (GMT-07)');
    });
    it('return proper label for America/Denver timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Denver', mockTimeZones);
      expect(label).toEqual('US/Mountain (GMT-07)');
    });
    it('return proper label for America/Chicago timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Chicago', mockTimeZones);
      expect(label).toEqual('US/Central (GMT-06)');
    });
    it('return proper label for America/New_York timezone', () => {
      const label = getTimeZoneLabel(date, 'America/New_York', mockTimeZones);
      expect(label).toEqual('US/Eastern (GMT-05)');
    });
    it('return proper label for America/Anchorage timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Anchorage', mockTimeZones);
      expect(label).toEqual('US/Alaska (GMT-09)');
    });
    it('return proper label for Pacific/Honolulu timezone', () => {
      const label = getTimeZoneLabel(date, 'Pacific/Honolulu', mockTimeZones);
      expect(label).toEqual('US/Hawaii (GMT-10)');
    });
  });

  describe('return proper label for US timezones as of March 14, 2021 at 2 AM', () => {
    const date = new Date('2021-03-14T02:00:00');
    it('return proper label for America/Los_Angeles timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Los_Angeles', mockTimeZones);
      expect(label).toEqual('US/Pacific (GMT-07)');
    });
    it('return proper label for America/Phoenix timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Phoenix', mockTimeZones);
      expect(label).toEqual('US/Arizona (GMT-07)');
    });
    it('return proper label for America/Denver timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Denver', mockTimeZones);
      expect(label).toEqual('US/Mountain (GMT-06)');
    });
    it('return proper label for America/Chicago timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Chicago', mockTimeZones);
      expect(label).toEqual('US/Central (GMT-05)');
    });
    it('return proper label for America/New_York timezone', () => {
      const label = getTimeZoneLabel(date, 'America/New_York', mockTimeZones);
      expect(label).toEqual('US/Eastern (GMT-04)');
    });
    it('return proper label for America/Anchorage timezone', () => {
      const label = getTimeZoneLabel(date, 'America/Anchorage', mockTimeZones);
      expect(label).toEqual('US/Alaska (GMT-08)');
    });
    it('return proper label for Pacific/Honolulu timezone', () => {
      const label = getTimeZoneLabel(date, 'Pacific/Honolulu', mockTimeZones);
      expect(label).toEqual('US/Hawaii (GMT-10)');
    });
  });
});
