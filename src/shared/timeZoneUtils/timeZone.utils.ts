import moment from 'moment-timezone';
import { ValueLabelPair } from 'data/types';

function addOffSet(
  date: Date | null,
  timeZoneValue: string,
  timeZoneLabel: string,
) : string {
  if (!date) {
    date = new Date();
  }

  if (timeZoneValue === 'Etc/GMT') {
    // Prevent the (GMT-0x) label to be added to UTC timezone
    return timeZoneLabel;
  }

  const timezoneOffSet = moment.tz([date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()], timeZoneValue).format('Z');
  return `${timeZoneLabel} (GMT${timezoneOffSet.substring(0, timezoneOffSet.indexOf(':'))})`;
}

export function getTimeZoneLabel(
  date: Date | null,
  timeZone: string,
  timeZones:ValueLabelPair[],
):string {
  let timeZoneLabel = timeZone;
  if (!timeZones || timeZones.length === 0) {
    return timeZoneLabel;
  }

  const matchedTimeZone = timeZones.find((item) => (item.value === timeZone));
  if (matchedTimeZone === undefined) {
    timeZoneLabel = timeZone;
  } else {
    timeZoneLabel = matchedTimeZone.label;
  }

  return addOffSet(date, timeZone, timeZoneLabel);
}

export function getSortedTimeZoneValueLabels(
  date: Date | null,
  timeZones:ValueLabelPair[],
):ValueLabelPair[] {

  if (!timeZones || timeZones.length === 0) {
    return timeZones;
  }

  const timeZonesWithUpdatedLabels = timeZones.map(({ value, label }) => (
    {
      value,
      label: addOffSet(date, value, label),
    }
  ));

  // returned sorted TimeZone Value/Label arrays. Sort by time zone label.
  return timeZonesWithUpdatedLabels.sort(({ label: item1Label }, { label: item2Label }) =>
    (item1Label.localeCompare(item2Label)));
}

export const getSortedTimeZoneLabels = (
  timeZones:ValueLabelPair[],
): string[] => (
  timeZones ? timeZones.map((timeZone) => (timeZone.label)).sort((item1Label, item2Label) =>
    (item1Label.localeCompare(item2Label))) : []
);
