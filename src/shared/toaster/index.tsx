import {
  Position, Toaster,
} from '@blueprintjs/core';
import './index.scss';

/** Singleton toaster instance. Create separate instances for different options. */

export const TOAST_TIMEOUT = 5000;

export const showErrors = (errors: string[], timeout = 0): void => {
  AppToaster.clear();
  errors.forEach((error:string) => {
    AppToaster.show({
      icon: 'error',
      intent: 'danger',
      timeout,
      message: `${error}`,
    });
  });
};

export const showSuccessMessage = (message:string, timeout = 0): void => {
  AppToaster.clear();
  AppToaster.show({
    icon: 'tick',
    intent: 'success',
    timeout,
    message: `${message}`,
  });
};

export const showError = (error: string | JSX.Element, timeout = 0): void => {
  AppToaster.clear();
  AppToaster.show({
    icon: 'error',
    intent: 'danger',
    timeout,
    message: error,
  });
};

const AppToaster = Toaster.create({
  className: 'app-toaster toaster-no-overlay',
  position: Position.TOP,
  canEscapeKeyClear: true,
});

export { AppToaster };
