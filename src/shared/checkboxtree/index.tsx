import React from 'react';
import CheckboxTree from 'react-checkbox-tree';
import {
  Icon,
} from '@blueprintjs/core';

export default class CommonCheckboxTree extends CheckboxTree {
  render() {
    return (
      <CheckboxTree
        {...this.props}
        icons={{
          check: <Icon intent="primary" icon="tick-circle" />,
          uncheck: <Icon intent="primary" icon="circle" />,
          halfCheck: <Icon intent="primary" icon="confirm" />,
          expandClose: <Icon intent="primary" icon="chevron-right" />,
          expandOpen: <Icon intent="primary" icon="chevron-down" />,
          expandAll: <Icon intent="primary" icon="expand-all" />,
          collapseAll: <Icon intent="primary" icon="collapse-all" />,
          parentClose: <Icon intent="primary" icon="folder-close" />,
          parentOpen: <Icon intent="primary" icon="folder-open" />,
          leaf: <Icon intent="primary" icon="document" />,
        }}
      />
    );
  }
}
