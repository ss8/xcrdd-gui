import React, { Fragment } from 'react';
import {
  Button, Dialog, Classes,
} from '@blueprintjs/core';

export const DONT_CLOSE_ON_SUBMIT = 'DONT_CLOSE_ON_SUBMIT';
export default class ModalDialog extends React.Component<any, any> {
  handleClose = () => {
    if (this.props.onClose) {
      this.props.onClose(); return;
    }
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  handleSubmit = () => {
    const { onSubmit } = this.props;
    if (onSubmit() !== DONT_CLOSE_ON_SUBMIT) {
      this.handleClose();
    }
  }

  render() {
    const {
      width = '500px', displayMessage, customComponent, actionText = 'Save', doNotDisplayCancelButton = false,
    } = this.props;
    return (
      <Dialog
        title={this.props.title}
        onClose={this.handleClose}
        isOpen={this.props.isOpen !== undefined ? this.props.isOpen : true}
        style={{ width }}
        className="modalDialog"
      >
        <div data-test="modalDialog-body" className={Classes.DIALOG_BODY}>
          {displayMessage}
          {customComponent && customComponent() && (
          <div style={{ paddingTop: '10px' }}>
            {customComponent()}
          </div>
          )}
        </div>
        <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px' }}>
          {!doNotDisplayCancelButton && <Button data-test="modalDialog-button-cancel" onClick={this.handleClose} intent="none" text="Cancel" />}
          <Button data-test="modalDialog-button-save" onClick={this.handleSubmit} intent="primary" active text={actionText} />
        </div>
      </Dialog>
    );
  }
}
