/**
 * trim starting and trailing white spaces in a any JSON structure
 */
// eslint-disable-next-line import/prefer-default-export
export const trimJsonObj = (json:any) => {
  if (json) {
    let jsonStr = JSON.stringify(json);
    let length = 0;
    do {
      // eslint-disable-next-line prefer-destructuring
      length = jsonStr.length;
      jsonStr = jsonStr.replace(' "', '"').replace('" ', '"');
    } while (length > jsonStr.length);
    return JSON.parse(jsonStr);
  }
  return json;
};
