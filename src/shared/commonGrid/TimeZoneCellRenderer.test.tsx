import React from 'react';
import {
  render,
  screen,
} from '@testing-library/react';
import { mockTimeZones } from '__test__/mockTimeZones';
import { UnconnectedTimeZoneCellRenderer } from './TimeZoneCellRenderer';

describe('<TimeZoneCellRenderer />', () => {
  it('when display timezone label when field is timeZone', () => {
    const colDef = { field: 'timeZone' };
    const data = { timeZone: 'America/New_York' };

    render(<UnconnectedTimeZoneCellRenderer
      colDef={colDef}
      data={data}
      timeZones={mockTimeZones}
    />);

    expect(screen.getByText('US/Eastern', { exact: false })).toBeVisible();
  });

  it('when display timezone label when data.timezone is an empty string', () => {
    const colDef = { field: 'timeZone' };
    const data = { timeZone: '' };

    render(<UnconnectedTimeZoneCellRenderer
      colDef={colDef}
      data={data}
      timeZones={mockTimeZones}
    />);

    expect(screen.getByTestId('timezone-cell')).toBeEmptyDOMElement();
  });

  it('when display timezone label when data.timezone is null', () => {
    const colDef = { field: 'timeZone' };
    const data = { timeZone: null };

    render(<UnconnectedTimeZoneCellRenderer
      colDef={colDef}
      data={data}
      timeZones={mockTimeZones}
    />);

    expect(screen.getByTestId('timezone-cell')).toBeEmptyDOMElement();
  });

  it('when display timezone label when data.timezone is undefined', () => {
    const colDef = { field: 'timeZone' };
    const data = { };

    render(<UnconnectedTimeZoneCellRenderer
      colDef={colDef}
      data={data}
      timeZones={mockTimeZones}
    />);

    expect(screen.getByTestId('timezone-cell')).toBeEmptyDOMElement();
  });

});
