import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import cloneDeep from 'lodash.clonedeep';
import { Warrant } from 'data/warrant/warrant.types';
import { findByTestAttr } from '../__test__/testUtils';
import ActionMenu from './grid-action-menu';
import showActionMenuItem from '../../xcipio/warrants/grid/utils';
import { WARRANT_ACTIVE, WARRANT_EXPIRED, WARRANT_PAUSED }
  from '../../xcipio/warrants/grid/test.data';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
});

const mockStore = configureStore([]);

const defaultProps = {
  context: {
    props: {
      showActionMenuItem: false,
      hasPrivilegetoView: true,
      hasPrivilegeToCreateNew: true,
      hasPrivilegeToDownload: true,
      hasPrivilegeToDelete: true,
      hasPrivilegeToUpdateWarrantState: true,
    },
    showActionMenuItem: jest.fn(),
  },
  data: null,
  disabled: false,
};

const defaultInitialState = {
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bArchiveWarrant: true,
      isAdditionalWarrantStateEnabled: true,
    },
  },
  users: {
    timezones: {},
  },
  warrants: { selectionLastCleared: null },
};

const setup = (props = {}) => {
  return shallow(<ActionMenu {...props} />);
};

describe('Test if Addtional Warrant Action like Stop is visible if isAdditionalWarrantStateEnabled flag is true', () => {
  let wrapper;
  let store: any;
  let props: any;
  let state;
  beforeEach(() => {
    state = cloneDeep(defaultInitialState);
    state.discoveryXcipio.discoveryXcipioSettings.isAdditionalWarrantStateEnabled = true;
    store = mockStore(state);
    props = cloneDeep(defaultProps);
    props.context.props.showActionMenuItem = (menuId: string, rowData: Warrant) => {
      return showActionMenuItem(menuId, rowData, true, true);
    };
  });

  test('Stop action is visible', () => {
    props.data = WARRANT_ACTIVE;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(1);
  });
});

describe('Test if Addtional Warrant Action like Stop is hidden if isAdditionalWarrantStateEnabled flag is false', () => {
  let wrapper;
  let store: any;
  let props: any;
  let state;
  beforeEach(() => {
    state = cloneDeep(defaultInitialState);
    state.discoveryXcipio.discoveryXcipioSettings.isAdditionalWarrantStateEnabled = false;
    store = mockStore(state);
    props = cloneDeep(defaultProps);
    props.context.props.showActionMenuItem = (menuId: string, rowData: Warrant) => {
      return showActionMenuItem(menuId, rowData, true, true);
    };
  });

  test('Stop action is visible', () => {
    props.data = WARRANT_ACTIVE;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(0);
  });
});

describe('Test if Warrant Actions-Start/Stop/Pause/Resume/Reschedule are visible/hidden based upon selected warrants state', () => {
  let wrapper;
  let store: any;
  let props: any;
  beforeEach(() => {
    store = mockStore(defaultInitialState);
    props = cloneDeep(defaultProps);
    props.context.props.showActionMenuItem = (menuId: string, rowData: Warrant) => {
      return showActionMenuItem(menuId, rowData, true, true);
    };
  });

  test('For Active Warrant; Pause and Stop action are displayed; Reschedule, Resume and Start are hidden;', () => {
    props.data = WARRANT_ACTIVE;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(1);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(0);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(0);
  });

  test('For Expired Warrant; Reschedule and Start action is displayed; Resume, Pause and Stop are hidden', () => {
    props.data = WARRANT_EXPIRED;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(0);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(0);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(1);
  });

  test('For Paused Warrant; Resume action is displayed; Reschedule,Start, Pause and Stop are hidden', () => {
    props.data = WARRANT_PAUSED;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(0);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(1);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(0);
  });
});

describe('Test if Warrant Actions-Start/Stop/Pause/Resume/Reschedule are visible/hidden based upon selected warrants state and if Pause and Resume actions are enabled', () => {
  let wrapper;
  let store: any;
  let props: any;
  beforeEach(() => {
    store = mockStore(defaultInitialState);
    props = cloneDeep(defaultProps);
    props.context.props.showActionMenuItem = (menuId: string, rowData: Warrant) => {
      return showActionMenuItem(menuId, rowData, false, false);
    };
  });

  test('For Active Warrant; Pause and Stop action are displayed; Reschedule, Resume and Start are hidden;', () => {
    props.data = WARRANT_ACTIVE;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(0);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(0);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(0);
  });

  test('For Expired Warrant; Reschedule and Start action is displayed; Resume, Pause and Stop are hidden', () => {
    props.data = WARRANT_EXPIRED;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(0);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(0);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(1);
  });

  test('For Paused Warrant; Resume action is displayed; Reschedule,Start, Pause and Stop are hidden', () => {
    props.data = WARRANT_PAUSED;
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });
    const pauseActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'pause-action-menu-item');
    expect(pauseActionMenuItem.length).toBe(0);
    const stopActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'stop-action-menu-item');
    expect(stopActionMenuItem.length).toBe(1);
    const rescheduleActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'reschedule-action-menu-item');
    expect(rescheduleActionMenuItem.length).toBe(0);
    const resumeActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'resume-action-menu-item');
    expect(resumeActionMenuItem.length).toBe(0);
    const startActionMenuItem = findByTestAttr(wrapper.dive().dive(), 'start-action-menu-item');
    expect(startActionMenuItem.length).toBe(0);
  });
});
