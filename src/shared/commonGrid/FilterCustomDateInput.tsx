import React, { Component } from 'react';
import { DateInput, TimePrecision, Classes as ClassesDateInput } from '@blueprintjs/datetime';
import { Position } from '@blueprintjs/core';
import './form.scss';
import moment from 'moment';
import { getDateTimeFormatTemplate } from '../../utils';

export default class FilterCustomDateInput extends Component<any, any> {
picker: any;

public customDate: any;

constructor(props: any) {
  super(props);
  this.state = {
    date: null,
    outOfRangeMessage: '',
  };
}

render() {
  return (
    <div className="ag-custom-component-popup">
      <DateInput
        value={this.state.date}
        inputProps={{ autoCorrect: 'off', autoComplete: 'off', style: { height: 'auto', lineHeight: '0px', boxShadow: 'none' } }}
        className={`${ClassesDateInput.DATEINPUT} noBorderRadius`}
        popoverProps={{
          position: Position.BOTTOM,
          popoverClassName: 'custom-date-filter ag-custom-component-popup',
        }}
        formatDate={(date) => moment(date).format(getDateTimeFormatTemplate())}
        parseDate={(str) => moment(str, getDateTimeFormatTemplate()).toDate()}
        onChange={this.onDateChanged.bind(this)}
        timePrecision={TimePrecision.MINUTE}
        closeOnSelection={false}
        maxDate={moment(new Date()).add(100, 'year').toDate()}
        minDate={moment(new Date()).subtract(100, 'year').toDate()}
      />
    </div>
  );
}

getDate() {
  return this.state.date;
}

setDate(date: any) {
  this.setState({ date });
}

onResetDate() {
  this.setDate(null);
  this.props.onDateChanged();
}

updateAndNotifyAgGrid(date: any) {
  this.setState({
    date,
  },
  this.props.onDateChanged);
}

    onDateChanged = (selectedDates: any) => {
      const date = moment(selectedDates);
      const outOfRangeMessage = date.isValid() ? null : 'Out of range';
      this.setState({
        date,
        outOfRangeMessage,
      });
      this.updateAndNotifyAgGrid(selectedDates);
    }
}
