import React from 'react';
import { shallow } from 'enzyme';
import FilterCustomDateInput from './FilterCustomDateInput';

describe('<FilterCustomDateInput />', () => {
  describe('render()', () => {
    let dateSpy: jest.SpyInstance;

    beforeEach(() => {
      const mockedDate = new Date(1592412954271);
      dateSpy = jest.spyOn<any, string>(global, 'Date').mockImplementation(() => mockedDate);
    });

    afterEach(() => {
      dateSpy.mockRestore();
    });

    it('should render the input when date not set', () => {
      const wrapper = shallow(<FilterCustomDateInput />);
      expect(wrapper).toMatchSnapshot();
    });

    it('should render the input when date set', () => {
      const wrapper = shallow(<FilterCustomDateInput />);
      wrapper.setState({ date: new Date(1592412954271) });
      expect(wrapper).toMatchSnapshot();
    });
  });
});
