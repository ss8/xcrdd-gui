import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import {
  Button, Popover,
} from '@blueprintjs/core';

const ActionMenuCellRenderer = ({
  context,
  data,
  selectedWarrants,
  node,
}: any) => {
  const isMultiSelection = selectedWarrants.length > 1;
  const content = React.createElement(context.actionMenuClass, {
    disabled: isMultiSelection,
    context,
    data,
    node,
  });

  if (node.level !== 0) {
    return <span />;
  }

  return (
    <Popover
      disabled={isMultiSelection}
      minimal
      content={content}
    >
      <Button minimal icon="more" disabled={isMultiSelection} title="View available actions" />
    </Popover>
  );
};

const mapStateToProps = ({ warrants: { selectedWarrants } }: any) => ({
  selectedWarrants,
});

export default connect(mapStateToProps, null)(ActionMenuCellRenderer);
