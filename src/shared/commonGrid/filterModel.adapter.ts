import cloneDeep from 'lodash.clonedeep';
import { ValueLabelPair } from 'data/types';
import { FilterModel } from './common-grid.types';

// eslint-disable-next-line import/prefer-default-export
export function adaptToServerValueFilterModel(
  filterModel:FilterModel,
  field:string,
  valueLabelPairList: ValueLabelPair[],
): FilterModel {
  const newFilterModel: FilterModel = cloneDeep(filterModel);
  Object.entries(filterModel).forEach(([key, item]) => {
    if (key === field) {
      // look up all timezone containing the filter, add to filter.
      newFilterModel[field].values = [];
      filterModel[field].values.forEach((value:string) => {
        const valueLabel = valueLabelPairList.find(
          (aValueLabel: ValueLabelPair) => (aValueLabel.label === value),
        );
        if (valueLabel !== undefined) {
          newFilterModel[field].values.push(valueLabel.value);
        }
      });
    } else {
      newFilterModel[key] = item;
    }
  });

  return newFilterModel;
}
