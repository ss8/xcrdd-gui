// eslint-disable-next-line import/prefer-default-export
export enum COMMON_GRID_COLUMN_FIELD_NAMES {
    Action = 'action',
}

export enum COMMON_GRID_ACTION_DISPLAY {
    view = 'View',
    edit = 'Edit',
    delete = 'Delete',
    download = 'Download',
    restore = 'Restore',
    reschedule = 'Reschedule',
    pause = 'Pause',
    resume = 'Resume',
    start = 'Start',
    stop = 'Stop',
    trash = 'Move to Trash',
    reprovision = 'Provisioning Retry'
}

export enum COMMON_GRID_ACTION {
    View = 'view',
    Edit = 'edit',
    Delete = 'delete',
    Download = 'download',
    Restore = 'restore',
    Reschedule = 'reschedule',
    Pause = 'pause',
    Resume = 'resume',
    Start = 'start',
    Stop = 'stop',
    Trash = 'trash',
    Untrash = 'untrash',
    Reprovision = 'reprovision'
}

export enum ROWSELECTION {
    SINGLE = 'single',
    MULTIPLE = 'multiple',
}
