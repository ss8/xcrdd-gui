// eslint-disable-next-line import/prefer-default-export
export const stringArrayComparator = (filter:string, value:any, filterText:string) => {
  if (!value) return false;

  const arrayInString = value.join(', ');
  const filterTextLoweCase = filterText.toLowerCase();
  const valueLowerCase = arrayInString.toLowerCase();

  switch (filter) {
    case 'contains':
      return valueLowerCase.indexOf(filterTextLoweCase) >= 0;
    case 'notContains':
      return valueLowerCase.indexOf(filterTextLoweCase) === -1;
    case 'equals':
      return valueLowerCase === filterTextLoweCase;
    case 'notEqual':
      return valueLowerCase !== filterTextLoweCase;
    case 'startsWith':
      return valueLowerCase.indexOf(filterTextLoweCase) === 0;
    case 'endsWith':
      const index = valueLowerCase.lastIndexOf(filterTextLoweCase);
      return index >= 0 && index === (valueLowerCase.length - filterTextLoweCase.length);
    default:
      // should never happen
      // eslint-disable-next-line no-console
      console.warn(`invalid filter type ${filter}`);
      return false;
  }
};
