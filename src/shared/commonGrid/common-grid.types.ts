export type FilterModel = {
  [key: string]: any;
}
