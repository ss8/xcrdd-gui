import React from 'react';
import { connect } from 'react-redux';
import { Menu, MenuItem } from '@blueprintjs/core';
import { COMMON_GRID_ACTION } from './grid-enums';

const ActionMenu = ({
  context, data, disabled, isAdditionalWarrantStateEnabled,
}: any) => {
  const {
    showActionMenuItem,
    hasPrivilegetoView,
    hasPrivilegeToCreateNew,
    hasPrivilegeToDownload,
    hasPrivilegeToDelete,
    hasPrivilegeToUpdateWarrantState,
  } = context.props;

  const showViewActionMenu = (
    (((showActionMenuItem
        && context.showActionMenuItem(COMMON_GRID_ACTION.View, data))
    ) || !showActionMenuItem) && hasPrivilegetoView
  );

  const showEditActionMenu = (
    (((showActionMenuItem
        && context.showActionMenuItem(COMMON_GRID_ACTION.Edit, data))
    ) || !showActionMenuItem) && hasPrivilegeToCreateNew
  );
  const showDownloadActionMenu = (
    (((showActionMenuItem
        && context.showActionMenuItem(COMMON_GRID_ACTION.Download, data))
    ) || !showActionMenuItem) && hasPrivilegeToDownload
  );
  const showDeleteActionMenu = (
    (((showActionMenuItem
        && context.showActionMenuItem(COMMON_GRID_ACTION.Delete, data))
    ) || !showActionMenuItem) && hasPrivilegeToDelete
  );

  const showRescheduleActionMenu = ((hasPrivilegeToUpdateWarrantState && showActionMenuItem &&
    showActionMenuItem(COMMON_GRID_ACTION.Reschedule, data))
      && isAdditionalWarrantStateEnabled);

  const showPauseActionMenu = ((hasPrivilegeToUpdateWarrantState && showActionMenuItem &&
    showActionMenuItem(COMMON_GRID_ACTION.Pause, data))
      && isAdditionalWarrantStateEnabled);
  const showResumeActionMenu = ((hasPrivilegeToUpdateWarrantState && showActionMenuItem &&
    showActionMenuItem(COMMON_GRID_ACTION.Resume, data))
      && isAdditionalWarrantStateEnabled);
  const showStartActionMenu = ((hasPrivilegeToUpdateWarrantState && showActionMenuItem &&
    showActionMenuItem(COMMON_GRID_ACTION.Start, data))
      && isAdditionalWarrantStateEnabled);
  const showStopActionMenu = ((hasPrivilegeToUpdateWarrantState && showActionMenuItem &&
    showActionMenuItem(COMMON_GRID_ACTION.Stop, data))
      && isAdditionalWarrantStateEnabled);

  return (
    <Menu>
      {
        // eslint-disable-next-line max-len
        showViewActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.View}
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="View"
            />
          )
          : <div />
        }
      {
        showEditActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Edit}
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Edit"
            />
          )
          : <div />
      }
      {
        showDownloadActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Download}
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Download"
            />
          )
          : <div />
      }
      {
        showDeleteActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Delete}
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Delete"
            />
          )
          : <div />
      }
      {
        showRescheduleActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Reschedule}
              data-test="reschedule-action-menu-item"
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Reschedule"
            />
          )
          : <div />
      }
      {
        showPauseActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Pause}
              data-test="pause-action-menu-item"
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Pause"
            />
          )
          : <div />
        }
      {
        showResumeActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Resume}
              data-test="resume-action-menu-item"
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Resume"
            />
          )
          : <div />
        }
      {
        showStartActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Start}
              data-test="start-action-menu-item"
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Start"
            />
          )
          : <div />
      }
      {
        showStopActionMenu
          ? (
            <MenuItem
              id={COMMON_GRID_ACTION.Stop}
              data-test="stop-action-menu-item"
              onClick={context.onActionMenuItemClick}
              disabled={disabled}
              text="Stop"
            />
          )
          : <div />
      }
    </Menu>
  );
};

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isAdditionalWarrantStateEnabled,
    },
  },
}: any) => ({
  isAdditionalWarrantStateEnabled,
});

export default connect(mapStateToProps, null)(ActionMenu);
