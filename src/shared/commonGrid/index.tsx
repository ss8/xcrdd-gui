import React, { Fragment, ReactNode } from 'react';
import { AgGridReact, AgGridReactProps } from '@ag-grid-community/react';
import {
  Button, InputGroup, Position, Tooltip,
} from '@blueprintjs/core';
import {
  ColDef, ColGroupDef, RowClickedEvent, GridReadyEvent,
} from '@ag-grid-enterprise/all-modules';
import { ColumnState } from '@ag-grid-community/core/dist/es6/columnController/columnController';
import { dateTimeFormatter } from 'shared/constants';
import { FilterModel } from './common-grid.types';
import { EmptyGrid } from '../grid';
import ServerSideDatasource from './serverSideDatasource';
import ActionMenuCellRenderer from './action-menu-cell-renderer';
import { COMMON_GRID_ACTION, COMMON_GRID_COLUMN_FIELD_NAMES, ROWSELECTION } from './grid-enums';
import FilterCustomDateInput from './FilterCustomDateInput';
import ActionMenu from './grid-action-menu';
import './common-grid.scss';

// Let's separate the properties that come with ag-grid from properties we are adding to it.
export interface ICommonGridDefaultProperties {
  /*
  *****  Row Selecton props: {
  */
  onSelectionChanged: any,
  rowSelection: ROWSELECTION,
  /*
  *  If true, rows won't be selected when clicked.
  *  This is to be used in combination with checkbox option
  */
  suppressRowClickSelection?: boolean
  /*
  *  Set to true to allow multiple rows to be selected with clicks.
  *  This is to be used if we don't want to go with the checkbox option
  */
  rowMultiSelectWithClick?: boolean
  /*
  *****  Row Selecton props: }
  */
  defaultColDef?: any
 }

export interface ICommonGridProperties extends ICommonGridDefaultProperties {
  onGridSizeChanged?: any,
  onCellFocused?: any,
  onActionMenuItemClick?: any,
  showActionMenuItem?: any,
  actionMenuClass?:any, // class that specifies the list of menu items
  onRefresh?: any,
  columnDefs: (ColDef | ColGroupDef)[],
  columnDefsVersion?: string,
  autoGroupColumnDef?: ColDef,
  rowData: any,
  handleCreateNew?: any,
  createNewText?: string,
  handleCreateNewWizard?: () => void,
  createNewWizardText?: string,
  noResults: boolean,
  getDataPath?: any,
  onRowSelected?: any,
  enableSorting?: boolean,
  enableFilter?: boolean,
  enableBrowserTooltips?: boolean,
  context?: any,
  pagination?: any,
  paginationAutoPageSize?: any,
  frameworkComponents?: any,
  renderDataFromServer?: boolean,
  adaptToFilterModel?: (filterModel:FilterModel) => FilterModel;
  getGridResultsFromServer?: any,
  hasPrivilegeToCreateNew?: boolean,
  hasPrivilegeToDownload?: boolean,
  hasPrivilegeToDelete?: boolean, // if not specified, no Delete menu item
  hasPrivilegetoView?: boolean, // if not specified, no View menu item
  hasPrivilegeToReschedule?: boolean,
  hasPrivilegeToUpdateWarrantState?:boolean,
  renderAttachmentField?: boolean,
  getGridSettingsFromServer?: any,
  saveGridSettings?: any,
  paginationPageSize?: number,
  isBeforeLogout?: boolean,
  additionalFilterSettings?: any,
  lastUpdatedAt?: Date | undefined,
  renderExtraButtons?: () => ReactNode,
  onRowDoubleClicked?: any
  suppressColumnVirtualisation?: boolean, // used for testing purposes to avoid ag-grid render only 2 columns in tests
  suppressAggFuncInHeader?: boolean,
  groupSelectsChildren?: boolean,
  suppressRowClickSelection?: boolean,
  enableGroupSelection?: boolean,
 }

export default class CommonGrid extends React.Component<ICommonGridProperties, any> {
  gridApi: any;

  gridColumnApi: any;

  gridOptions: any;

  actionMenuClass:any;

  mounted = false;

  state = {
    selection: [],
    quickFilterText: '',
  }

  actionColumnDefs = {
    sortable: false,
    filter: false,
    lockVisible: true,
    lockPosition: true, // locks the column in the first position
    resizable: false,
    editable: false,
    suppressSizeToFit: true,
    suppressNavigable: true,
    suppressMenu: true,
    width: 50,
    minWidth: 50,
    maxWidth: 50,
  };

  constructor(props: ICommonGridProperties) {
    super(props);

    // if you need to bind callback to this
    this.handleBeforeUnload = this.handleBeforeUnload.bind(this);

    if (this.props.actionMenuClass) {
      this.actionMenuClass = this.props.actionMenuClass;
    } else {
      this.actionMenuClass = ActionMenu;
    }
  }

  updateColDefs(colDefs: (ColDef | ColGroupDef)[]): (ColDef | ColGroupDef)[] {
    return colDefs.map((element: ColDef | ColGroupDef) => {
      if (('children' in element)) {
        return this.updateColGroupDefElement(element);
      }

      return this.updateColDefElement(element);
    });
  }

  updateColGroupDefElement(element: ColGroupDef): ColGroupDef {
    return {
      ...element,
      children: element.children.map(this.updateColDefElement),
    };
  }

  updateColDefElement = (element: ColDef): ColDef => {
    const filterParams = {
      applyButton: true,
      clearButton: true,
      suppressAndOrCondition: true,
      newRowsAction: 'keep',
    };

    const finalFilterParams = ('filterParams' in element)
      ? ({ ...element.filterParams, ...filterParams })
      : filterParams;

    const newColumnDefs = (element.field === COMMON_GRID_COLUMN_FIELD_NAMES.Action)
      ? ({ ...element, ...this.actionColumnDefs })
      : element;

    newColumnDefs.colId = newColumnDefs.field;
    return ({ ...newColumnDefs, filterParams: finalFilterParams });
  }

  componentDidMount() {
    this.mounted = true;

    // Browser's refresh button triggers beforeunload event.
    window.addEventListener('beforeunload', this.handleBeforeUnload);
  }

  /**
   * componentWillUnmount is not invoked on browser refresh. So need to handle browser
   * unload event to save settings
   */
  componentWillUnmount() {
    this.mounted = false;

    // Browser's refresh button triggers beforeunload event.
    window.removeEventListener('beforeunload', this.handleBeforeUnload);

    if (this.props.isBeforeLogout === false) {
      this.saveGridSettings();
    }

    if (this.gridApi) {
      (this.gridApi as any).destroy();
      this.gridApi = null;
    }
  }

  /**
   * This is called as soon as this component gets a redux property/state update
   */
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps:any) {
    if (nextProps.actionMenuClass) {
      this.actionMenuClass = nextProps.actionMenuClass;
    } else {
      this.actionMenuClass = ActionMenu;
    }
    if (nextProps.isBeforeLogout !== undefined) {
      if (this.props.isBeforeLogout === false &&
        nextProps.isBeforeLogout === true) {
        this.saveGridSettings();
      }
    }
  }

  onGridReady = async (params: GridReadyEvent) => {

    const {
      getGridSettingsFromServer,
      renderAttachmentField,
      columnDefsVersion,
      paginationPageSize,
      renderDataFromServer,
      getGridResultsFromServer,
      additionalFilterSettings,
      adaptToFilterModel,
    } = this.props;

    if (!this.mounted || getGridSettingsFromServer == null) {
      // Component was unmounted, nothing to be done here
      return;
    }

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    await getGridSettingsFromServer().then((resp: any) => {
      if (!this.mounted) {
        // Component was unmounted, nothing to be done here
        return;
      }

      if (resp.value.data.data.length > 0) {
        const gridSettingJSON = resp.value.data.data[0].setting;
        const gridSettingObject = JSON.parse(gridSettingJSON);
        gridSettingObject.paginationPageSize = paginationPageSize;
        if (renderAttachmentField === false) {
          this.getLeastFieldsFromColumnDefAndUserGridSetting(gridSettingObject);
        }

        if (columnDefsVersion == null || columnDefsVersion === gridSettingObject.version) {
          this.applyGridSettings(gridSettingObject);
        }
      }
    });

    if (renderDataFromServer === true) {
      const propsToSend = {
        getDataFromServer: getGridResultsFromServer,
        additionalFilterSettings,
        adaptToFilterModel,
      };
      params.api.setServerSideDatasource(new ServerSideDatasource(propsToSend));
    }
  }

  getLeastFieldsFromColumnDefAndUserGridSetting = (gridSettings: {[key: string]: any}) => {
    const miniGridSetting: Array<Record<string, unknown>> = [];
    gridSettings.columnSetting.forEach((userGridSetting: any) => {
      this.props.columnDefs.forEach((column:any) => {
        const userGridSettingName = `${userGridSetting.colId}`.replace(/[\d_]+/g, '');
        if (userGridSettingName === column.field) {
          const newUserGridSetting = { ...userGridSetting };
          if (!/\d/.test(userGridSetting.colId)) {
            newUserGridSetting.colId = `${userGridSetting.colId}`;
          }
          miniGridSetting.push(newUserGridSetting);
        }
      });
    });
    gridSettings.columnSetting = miniGridSetting;
  }

  static arrayToMap<T extends { colId: string }>(arr:T[], key: 'colId'):({ [keyR:string]: T }) {
    return arr.reduce((previous, current) => ({ ...previous, [current[key]]: current }), {});
  }

  static mergeColumnSettings(currentColumnSettings:ColumnState[], serverColumnSettings:ColumnState[]): ColumnState[] {

    const stateColsDefsMap = CommonGrid.arrayToMap<ColumnState>(currentColumnSettings, 'colId');
    const serverColsDefsMap = CommonGrid.arrayToMap<ColumnState>(serverColumnSettings, 'colId');

    const mergedSettingsKeys = Object.keys({ ...serverColsDefsMap, ...stateColsDefsMap });

    const newGridSettings = mergedSettingsKeys.map((key: string) => ({
      ...(stateColsDefsMap[key] || {}),
      ...(serverColsDefsMap[key] || {}),
    }));

    return newGridSettings;
  }

  applyGridSettings = (gridSettings: {[key: string]: any}) => {
    // Apply previously stored column hide/show & size setting to the grid.
    // Need to check whether gridApi is null because  this.gridColumnApi.setColumnState
    // crashes if Grid is not ready.
    if (this.gridColumnApi !== null && this.gridApi !== null) {
      if (gridSettings.columnSetting != null) {

        gridSettings.columnSetting.map((element: any) => {
          const columDef = (element.field === COMMON_GRID_COLUMN_FIELD_NAMES.Action)
            ? ({ ...element, ...this.actionColumnDefs })
            : element;
          return columDef;
        });

        const columnSettings = CommonGrid.mergeColumnSettings(
          this.gridColumnApi.getColumnState(),
          gridSettings.columnSetting,
        );

        this.gridColumnApi.setColumnState(columnSettings);
      }
    }

    if (this.gridApi !== null) {
      if (gridSettings.sortSetting != null) {
        this.gridApi.setSortModel(gridSettings.sortSetting);
      }

      if (gridSettings.paginationPageSize > 0) {
        this.gridApi.paginationSetPageSize(Number(gridSettings.paginationPageSize));
      }
    }
  }

  getGridSettings = () => {
    const { columnDefsVersion } = this.props;

    const gridSettings: {[key: string]: any} = {};

    if (this.gridColumnApi && (this.gridColumnApi as any).getColumnState() != null) {
      gridSettings.columnSetting = (this.gridColumnApi as any).getColumnState();
    }

    if (this.gridApi && (this.gridApi as any).getSortModel() != null) {
      gridSettings.sortSetting = (this.gridApi as any).getSortModel();
    }

    if (columnDefsVersion != null) {
      gridSettings.version = columnDefsVersion;
    }

    // if (this.props.paginationPageSize !== undefined) {
    //   gridSettings.paginationPageSize = this.props.paginationPageSize;
    // }

    return gridSettings;
  }

  saveGridSettings = () => {
    if (this.props.saveGridSettings === undefined) {
      return;
    }
    const gridSettings = this.getGridSettings();
    if (gridSettings.columnSetting !== undefined && gridSettings.sortSetting !== undefined) {
      this.props.saveGridSettings(gridSettings);
    }
  }

  getContextMenuItems = () => {
    const result = [
      {
        name: 'Reset Columns',
        action: () => {
          this.gridColumnApi.resetColumnState();
        },
      }];
    return result;
  }

  /*
  * Note: We are saving same data in two different places,
  * at the commongrid level and the parent level,
  * which could be problematic/confusing - !SSOT
  */
  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
    this.props.onSelectionChanged(e);
  }

  handleRowGroupClicked = (event: RowClickedEvent) => {
    const { node } = event;
    const { enableGroupSelection } = this.props;

    if (!enableGroupSelection) {
      return;
    }
    event.api.deselectAll();

    if (node.group) {

      if (node.isSelected()) {
        node.setSelected(false);
        this.setState({ selection: [] });
      } else {
        node.setSelected(true);
        this.setState({ selection: event.api.getSelectedRows() });
      }
    } else {
      node.parent?.setSelected(true);
      this.setState({ selection: event.api.getSelectedRows() });
    }

    this.props.onSelectionChanged(event);
  }

  // by default ag-grid focuses individual cell, this maps up/dn arrows to row selection
  handleCellFocused = (e: any) => {
    if (e.column) {
      e.api.forEachNode((node: any) => {
        if (node.id === e.rowIndex) node.setSelected(true, true);
      });
    }
  }

  handleQuickFilterChange = (e: any) => {
    this.setState({ quickFilterText: e.currentTarget.value });
  }

  /*
   * handleBeforeUnload is called when browser refresh button is pressed
   */
  handleBeforeUnload = () => {
    this.saveGridSettings();
  }

  handleGridReloadFromServer = (getGridResultsFromServer: any, additionalFilters: any) => {
    const propsToSend = {
      getDataFromServer: getGridResultsFromServer,
      additionalFilterSettings: additionalFilters,
    };
    if (this.gridApi) {
      // Deselect rows
      this.gridApi.forEachNode((node: any) => {
        node.setSelected(false);
      });
      this.gridApi.setServerSideDatasource(new ServerSideDatasource(propsToSend));
    }
  }

  handleServerModelRefresh = () => {
    if (this.props.renderDataFromServer === true) {
      const propsToSend = {
        getDataFromServer: this.props.getGridResultsFromServer,
        additionalFilterSettings: this.props.additionalFilterSettings,
      };
      this.gridApi.setServerSideDatasource(new ServerSideDatasource(propsToSend));
    }
  }

  handleClientModelRefresh = (event: any) => {
    if (this.props.onRefresh !== undefined) {
      this.props.onRefresh(event);
    }
  }

  onActionMenuItemClick= (e: any) => {
    if (this.props.onActionMenuItemClick !== undefined) {
      const { id } = e.currentTarget;
      const event = new Event(id);
      this.props.onActionMenuItemClick(event);
    }
  }

  showActionMenuItem = (menuId : string, rowData : any) => {
    if (this.props.showActionMenuItem !== undefined) {
      return this.props.showActionMenuItem(menuId, rowData, this.props.context);
    }
    return true;
  }

  handleView = (e: any) => {
    if (this.props.onActionMenuItemClick !== undefined) {
      const event = new Event(COMMON_GRID_ACTION.View);
      this.props.onActionMenuItemClick(event);
    }
  }

  setFrameworkComponents = () => {
    if (this.props.frameworkComponents) {
      const extraCellRenderer = { ...this.props.frameworkComponents };
      extraCellRenderer.agDateInput = FilterCustomDateInput;
      return extraCellRenderer;
    }
    return {
      actionMenuCellRenderer: ActionMenuCellRenderer,
      agDateInput: FilterCustomDateInput,
    };
  }

  renderLastUpdatedAt(): ReactNode {
    const {
      lastUpdatedAt,
    } = this.props;

    if (!lastUpdatedAt) {
      return null;
    }

    return (
      <span className="last-updated-at">Last updated at {dateTimeFormatter(lastUpdatedAt)}</span>
    );
  }

  renderExtraButtons(): ReactNode {
    const {
      renderExtraButtons,
    } = this.props;

    if (!renderExtraButtons) {
      return null;
    }

    return renderExtraButtons();
  }

  /**
   * If you want to set the server request block size, use cacheBlockSize={10}
   */
  render() {
    const coldefsUpdated = this.updateColDefs(this.props.columnDefs);
    const gridFrameWorks = this.setFrameworkComponents();

    const {
      enableBrowserTooltips,
      enableFilter,
      enableSorting,
      getDataPath,
      onCellFocused,
      onGridSizeChanged,
      onRowSelected,
      pagination,
      paginationAutoPageSize,
      rowData,
      rowSelection,
      suppressRowClickSelection,
      defaultColDef,
      rowMultiSelectWithClick,
      suppressColumnVirtualisation,
      suppressAggFuncInHeader,
      groupSelectsChildren,
      autoGroupColumnDef,
    } = this.props;

    const agGridReactProps: AgGridReactProps = {
      getDataPath,
      onGridReady: this.onGridReady,
      onSelectionChanged: this.handleSelection,
      onGridSizeChanged,
      onCellFocused,
      onRowDoubleClicked: this.handleView,
      rowSelection,
      columnDefs: coldefsUpdated,
      autoGroupColumnDef,
      quickFilterText: this.state.quickFilterText,
      context: this,
      pagination,
      paginationAutoPageSize,
      onRowSelected,
      enableSorting,
      enableFilter,
      enableBrowserTooltips,
      getContextMenuItems: this.getContextMenuItems,
      floatingFilter: true,
      frameworkComponents: gridFrameWorks,
      suppressRowClickSelection,
      defaultColDef,
      rowMultiSelectWithClick,
      suppressColumnVirtualisation,
      suppressAggFuncInHeader,
      groupSelectsChildren,
      onRowClicked: this.handleRowGroupClicked,
    };

    if (this.props.renderDataFromServer) {
      agGridReactProps.rowModelType = 'serverSide';
      agGridReactProps.cacheBlockSize = 200;
    } else agGridReactProps.rowData = rowData;

    return (
      <Fragment>
        {this.props.children}
        <div style={{
          display: 'flex', marginBottom: '15px', justifyContent: 'left', float: 'right',
        }}
        >
          {
            this.props.hasPrivilegeToCreateNew && this.props.handleCreateNewWizard
              ? (
                <Button
                  onClick={this.props.handleCreateNewWizard}
                  intent="none"
                  style={{ marginLeft: '5px', marginRight: '10px' }}
                  text={this.props.createNewWizardText}
                />
              )
              : <div />
          }
          {
            this.props.hasPrivilegeToCreateNew
              ? (
                <Button
                  onClick={this.props.handleCreateNew}
                  intent="none"
                  style={{ marginLeft: '5px', marginRight: '10px' }}
                  text={this.props.createNewText}
                />
              )
              : <div />
          }
          { !this.props.renderDataFromServer && (
          <InputGroup
            style={{ marginRight: '10px', width: '500px' }}
            onChange={this.handleQuickFilterChange}
            type="text"
            leftIcon="search"
            placeholder="Search"
            value={this.state.quickFilterText}
          />
          )}
          <Tooltip content="Refresh data" position={Position.BOTTOM} openOnTargetFocus={false}>
            <Button
              style={{ paddingTop: '5px' }}
              icon="refresh"
              minimal
              onClick={(this.props.renderDataFromServer === true) ?
                this.handleServerModelRefresh : this.handleClientModelRefresh}
            />
          </Tooltip>

          {this.renderExtraButtons()}

        </div>
        {
          this.props.noResults
            ? <EmptyGrid />
            : (
              <div className="ag-theme-balham common-grid-container">
                <AgGridReact
                  {...agGridReactProps}
                />
                {this.renderLastUpdatedAt()}
              </div>
            )
        }
      </Fragment>
    );
  }
}
