import React, { Component, Fragment } from 'react';
import moment from 'moment-timezone';
import lodashGet from 'lodash.get';
import { getDateTimeFormatTemplate } from '../../utils';

const DEFAULT_TIMEZONE_FIELD_NAME = 'timeZone';

export interface DateTimeWithTimezoneCellRendererProps {
  colDef: {
    field: string
  },
  data: {
    [key: string]: any
  }
  timezoneField?: string
}

/**
 * Class responsible for rendering the Date/Time value considering
 * the Timezone.
 *
 * This class will get the timezone value from the data using the
 * 'timezoneField' prop content or if not defined, it will
 * get the timezone value from 'timeZone' field.
 */
class DateTimeWithTimezoneCellRenderer extends Component<DateTimeWithTimezoneCellRendererProps> {
  getFormattedDateTimeInTimezone(dateTime: string) {
    const {
      data,
      timezoneField = DEFAULT_TIMEZONE_FIELD_NAME,
    } = this.props;

    const timezone = lodashGet(data, timezoneField, '');

    if (!timezone) {
      // eslint-disable-next-line no-console
      console.error('DateTimeWithTimezoneCellRenderer', 'Missing timezone value, will show date in computer timezone');
      return moment(dateTime).format(getDateTimeFormatTemplate());
    }

    return moment(dateTime).tz(timezone).format(getDateTimeFormatTemplate());
  }

  render() {
    const {
      colDef: { field },
      data,
    } = this.props;

    const dateTime = lodashGet(data, field, '');

    return (
      <Fragment>
        {(dateTime === '' || dateTime === null || dateTime === 'undefined') ? '' :
          this.getFormattedDateTimeInTimezone(dateTime)}
      </Fragment>
    );
  }
}

export default DateTimeWithTimezoneCellRenderer;
