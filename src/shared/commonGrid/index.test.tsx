import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import CommonGrid, { ICommonGridProperties } from './index';
import { ROWSELECTION, COMMON_GRID_COLUMN_FIELD_NAMES } from './grid-enums';

describe('<CommonGrid />', () => {
  let wrapper: ReactWrapper<ICommonGridProperties, any, CommonGrid>;
  let columnDefs: ColDef[];
  let columnGroupDef: ColGroupDef[];

  const filterParams = {
    applyButton: true,
    clearButton: true,
    newRowsAction: 'keep',
    suppressAndOrCondition: true,
  };

  const actionColumnParams = {
    colId: 'action',
    editable: false,
    filter: false,
    lockPosition: true,
    lockVisible: true,
    maxWidth: 50,
    minWidth: 50,
    resizable: false,
    sortable: false,
    suppressMenu: true,
    suppressNavigable: true,
    suppressSizeToFit: true,
    width: 50,
  };

  beforeEach(() => {
    columnDefs = [{
      headerName: '',
      field: COMMON_GRID_COLUMN_FIELD_NAMES.Action,
      cellRenderer: 'actionMenuCellRenderer',
    }, {
      headerName: 'User ID',
      field: 'userID',
      sortable: true,
      filter: 'agTextColumnFilter',
      resizable: true,
      tooltipField: 'userID',
    }, {
      headerName: 'Email',
      field: 'email',
      sortable: true,
      filter: 'agTextColumnFilter',
      resizable: true,
      tooltipField: 'email',
    }];

    columnGroupDef = [{
      headerName: '',
      children: [{
        headerName: '',
        field: COMMON_GRID_COLUMN_FIELD_NAMES.Action,
        cellRenderer: 'actionMenuCellRenderer',
      }, {
        headerName: 'User ID',
        field: 'userID',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'userID',
      }],
    }, {
      headerName: '',
      children: [{
        headerName: 'Email',
        field: 'email',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'email',
      }],
    }];

    wrapper = mount(
      <CommonGrid
        columnDefs={columnDefs}
        rowData={[]}
        noResults
        onSelectionChanged={() => false}
        rowSelection={ROWSELECTION.SINGLE}
      />,
    );
  });

  describe('updateColDefs()', () => {
    it('should update the column definition when there are only ColDefs types entries', () => {
      expect(wrapper.instance().updateColDefs(columnDefs)).toEqual([{
        ...columnDefs[0],
        filterParams,
        ...actionColumnParams,
      }, {
        ...columnDefs[1],
        filterParams,
      },
      {
        ...columnDefs[2],
        filterParams,
      }]);
    });

    it('should update the column definition when there are ColGroupDef with ColDefs types entries', () => {
      expect(wrapper.instance().updateColDefs(columnGroupDef)).toEqual([{
        ...columnGroupDef[0],
        children: [{
          ...columnGroupDef[0].children[0],
          filterParams,
          ...actionColumnParams,
        }, {
          ...columnGroupDef[0].children[1],
          filterParams,
        }],
      }, {
        ...columnGroupDef[1],
        children: [{
          ...columnGroupDef[1].children[0],
          filterParams,
        }],
      }]);
    });
  });

  describe('updateColGroupDefElement()', () => {
    it('should update the column group definition', () => {
      expect(wrapper.instance().updateColGroupDefElement(columnGroupDef[0])).toEqual({
        ...columnGroupDef[0],
        children: [{
          ...columnGroupDef[0].children[0],
          filterParams,
          ...actionColumnParams,
        }, {
          ...columnGroupDef[0].children[1],
          filterParams,
        }],
      });
    });
  });

  describe('updateColDefElement()', () => {
    it('should update the column definition element for an action column type', () => {
      expect(wrapper.instance().updateColDefElement(columnDefs[0])).toEqual({
        ...columnDefs[0],
        filterParams,
        ...actionColumnParams,
      });
    });

    it('should update the column definition element for an generic column type', () => {
      const columnDef = columnDefs[1];
      expect(wrapper.instance().updateColDefElement(columnDef)).toEqual({
        ...columnDef,
        filterParams,
      });
    });

    it('should update the colum definition element replacing the filterParams', () => {
      const columnDef = {
        ...columnDefs[1],
        filterParams: {
          suppressAndOrCondition: false,
        },
      };
      expect(wrapper.instance().updateColDefElement(columnDef)).toEqual({
        ...columnDef,
        filterParams: {
          ...filterParams,
          suppressAndOrCondition: true,
        },
      });
    });
  });

  describe('renderLastUpdatedAt()', () => {
    it('should not return anything if lastUpdatedAt is not defined', () => {
      expect(wrapper.instance().renderLastUpdatedAt()).toMatchSnapshot();
    });

    it('should return as expected if lastUpdatedAt is defined', () => {
      wrapper.setProps({ lastUpdatedAt: new Date('Thu Aug 27 2020 12:13:34 GMT-0300') });
      wrapper.update();
      expect(wrapper.instance().renderLastUpdatedAt()).toMatchSnapshot();
    });
  });

  describe('render()', () => {
    it('should not render lastUpdatedAt is not defined', () => {
      expect(wrapper.render().find('.last-updated-at')).toHaveLength(0);
    });

    it('should return as expected if lastUpdatedAt is defined', () => {
      wrapper.setProps({
        lastUpdatedAt: new Date('Thu Aug 27 2020 12:13:34 GMT-0300'),
        noResults: false,
        rowData: [{}],
      });
      wrapper.update();

      expect(wrapper.render().find('.last-updated-at').text()).toEqual('Last updated at 08/27/20 08:13:34 AM');
    });
  });
});
