import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import DateTimeWithTimezoneCellRenderer, { DateTimeWithTimezoneCellRendererProps } from './DateTimeWithTimezoneCellRenderer';

describe('<DateTimeWithTimezoneCellRenderer />', () => {
  let props: DateTimeWithTimezoneCellRendererProps;
  let wrapper: ShallowWrapper<DateTimeWithTimezoneCellRendererProps, void, DateTimeWithTimezoneCellRenderer>;

  beforeEach(() => {
    props = {
      colDef: {
        field: 'dateTime',
      },
      data: {
        dateTime: '2020-06-16T18:20:00Z',
        timeZone: 'America/New_York',
      },
    };

    wrapper = shallow<DateTimeWithTimezoneCellRenderer, DateTimeWithTimezoneCellRendererProps, void>(
      <DateTimeWithTimezoneCellRenderer {...props} />,
    );
  });

  describe('getFormattedDateTimeInTimezone()', () => {
    it('should return the date formatted in the timezone if available', () => {
      expect(wrapper.instance().getFormattedDateTimeInTimezone(props.data.dateTime))
        .toEqual('06/16/20 02:20:00 PM');
    });

    it('should return the date formatted in the timezone if timezoneField in the props', () => {
      wrapper.setProps({
        data: {
          settings: {
            timezone: 'Europe/London',
          },
        },
        timezoneField: 'settings.timezone',
      });

      expect(wrapper.instance().getFormattedDateTimeInTimezone(props.data.dateTime))
        .toEqual('06/16/20 07:20:00 PM');
    });

    it('should return the date formatted in current timezone if not available', () => {
      wrapper.setProps({
        data: {
          dateTime: '2020-06-16T18:20:00Z',
        },
      });
      expect(wrapper.instance().getFormattedDateTimeInTimezone(props.data.dateTime))
        .toEqual('06/16/20 11:20:00 AM');
    });

    it('should return the date formatted in current timezone if timezone is invalid', () => {
      wrapper.setProps({
        data: {
          dateTime: '2020-06-16T18:20:00Z',
          timeZone: 'America/Invalid',
        },
      });
      expect(wrapper.instance().getFormattedDateTimeInTimezone(props.data.dateTime))
        .toEqual('06/16/20 11:20:00 AM');
    });
  });

  describe('render()', () => {
    it('should render the date as expected', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('should render the date as expected if field has dot notation', () => {
      wrapper.setProps({
        colDef: {
          field: 'start.dateTime',
        },
        data: {
          start: { dateTime: '2020-06-16T18:20:00Z' },
          timeZone: 'America/Los_Angeles',
        },
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if date null', () => {
      wrapper.setProps({
        data: { dateTime: null },
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if date empty string', () => {
      wrapper.setProps({
        data: { dateTime: '' },
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if date undefined string', () => {
      wrapper.setProps({
        data: { dateTime: 'undefined' },
      });
      expect(wrapper).toMatchSnapshot();
    });
  });
});
