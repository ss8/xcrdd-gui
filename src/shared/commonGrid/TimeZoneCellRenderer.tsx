import React, { Component, Fragment } from 'react';
import lodashGet from 'lodash.get';
import { RootState } from 'data/root.reducers';
import { connect } from 'react-redux';
import * as userSelectors from 'users/users-selectors';
import { getTimeZoneLabel } from 'shared/timeZoneUtils/timeZone.utils';
import { ValueLabelPair } from 'data/types';

export type TimeZoneCellRendererProps = {
  colDef: {
    field: string
  },
  data: {
    [key: string]: any
  },
  timeZones: ValueLabelPair[]
};
export class UnconnectedTimeZoneCellRenderer extends Component<TimeZoneCellRendererProps> {

  renderTimeZone(timeZone:string) {
    const {
      timeZones,
    } = this.props;

    if (timeZone === '' || timeZone === null) {
      return '';
    }

    return getTimeZoneLabel(null, timeZone, timeZones);
  }

  render() {
    const {
      colDef: { field },
      data,
    } = this.props;

    const timeZone = lodashGet(data, field, '');

    return (
      <span data-testid="timezone-cell">
        {this.renderTimeZone(timeZone)}
      </span>
    );
  }
}

// time zone list must be retrieved by the grid that uses this renderer
const mapState = (state: RootState) => ({
  timeZones: userSelectors.getTimezones(state),
});
export const TimeZoneCellRenderer = connect(mapState, null)(UnconnectedTimeZoneCellRenderer);
