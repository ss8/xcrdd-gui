import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import store from 'data/store';
import { ColDef } from '@ag-grid-enterprise/all-modules';
import { ROWSELECTION } from './grid-enums';
import CommonGrid from './index';

describe('<CommonGrid />', () => {
  let userSettings: unknown;
  const handleRefresh = jest.fn();
  const handleCreateNew = jest.fn();
  const handleSelection = jest.fn();
  const handleSaveGridSettings = jest.fn();
  const handleSelectItemMenuAction = jest.fn();
  const handleShowActionMenu = jest.fn();
  const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

  const ROW_DATA_MOCK = [{
    id: 'id1',
    name: 'name1',
  }, {
    id: 'id2',
    name: 'name2',
  }, {
    id: 'id3',
    name: 'name3',
  }];

  describe('when server settings and coldDefs fields properties are the same', () => {

    const COL_DEFS: ColDef[] = [
      {
        headerName: 'Id',
        field: 'id',
        filter: 'agTextColumnFilter',
        hide: false,
      },
      {
        headerName: 'Name',
        field: 'name',
        filter: 'agTextColumnFilter',
        hide: false,
      },
    ];

    const SERVER_SETTINGS = {
      columnSetting: [
        {
          colId: 'id', hide: false, aggFunc: null, width: 200, pivotIndex: null, pinned: null, rowGroupIndex: null,
        },
        {
          colId: 'name', hide: false, aggFunc: null, width: 200, pivotIndex: null, pinned: null, rowGroupIndex: null,
        },
      ],
    };

    beforeEach(async () => {
      userSettings = {
        value: {
          data: {
            data: [
              {
                setting: JSON.stringify(SERVER_SETTINGS),
              },
            ],
          },
        },
      };

      render(
        <Provider store={store}>
          <CommonGrid
            createNewText="Common Grid"
            pagination
            enableBrowserTooltips
            rowSelection={ROWSELECTION.SINGLE}
            hasPrivilegeToCreateNew
            hasPrivilegeToDelete
            hasPrivilegetoView
            rowData={ROW_DATA_MOCK}
            noResults={false}
            isBeforeLogout
            columnDefs={COL_DEFS}
            frameworkComponents={{}}
            actionMenuClass={{}}
            handleCreateNew={handleCreateNew}
            onRefresh={handleRefresh}
            onSelectionChanged={handleSelection}
            showActionMenuItem={handleShowActionMenu}
            onActionMenuItemClick={handleSelectItemMenuAction}
            saveGridSettings={handleSaveGridSettings}
            getGridSettingsFromServer={handleGetGridSettingsFromServer}
            lastUpdatedAt={new Date('Thu Aug 27 2020 12:13:34 GMT-0300')}
          />
        </Provider>,
      );

      await screen.findByRole('grid');
    });

    it('should render field ID in the grid', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].id)).toBeVisible();
    });

    it('should render field NAME in the grid', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].name)).toBeVisible();
    });
  });

  describe('when a field exists in coldDefs but not in server settings', () => {

    const COL_DEFS: ColDef[] = [
      {
        headerName: 'Id',
        field: 'id',
        filter: 'agTextColumnFilter',
        hide: false,
      },
      {
        headerName: 'Name',
        field: 'name',
        filter: 'agTextColumnFilter',
        hide: false,
      },
    ];

    const SERVER_SETTINGS = {
      columnSetting: [
        {
          colId: 'id', hide: false, aggFunc: null, width: 200, pivotIndex: null, pinned: null, rowGroupIndex: null,
        },
      ],
    };

    beforeEach(async () => {
      userSettings = {
        value: {
          data: {
            data: [
              {
                setting: JSON.stringify(SERVER_SETTINGS),
              },
            ],
          },
        },
      };

      render(
        <Provider store={store}>
          <CommonGrid
            createNewText="Common Grid"
            pagination
            enableBrowserTooltips
            rowSelection={ROWSELECTION.SINGLE}
            hasPrivilegeToCreateNew
            hasPrivilegeToDelete
            hasPrivilegetoView
            rowData={ROW_DATA_MOCK}
            noResults={false}
            isBeforeLogout
            columnDefs={COL_DEFS}
            frameworkComponents={{}}
            actionMenuClass={{}}
            handleCreateNew={handleCreateNew}
            onRefresh={handleRefresh}
            onSelectionChanged={handleSelection}
            showActionMenuItem={handleShowActionMenu}
            onActionMenuItemClick={handleSelectItemMenuAction}
            saveGridSettings={handleSaveGridSettings}
            getGridSettingsFromServer={handleGetGridSettingsFromServer}
            lastUpdatedAt={new Date('Thu Aug 27 2020 12:13:34 GMT-0300')}
          />
        </Provider>,
      );

      await screen.findByRole('grid');
    });

    it('should render field ID present in coldDefs and server settings', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].id)).toBeVisible();
    });

    it('should render field NAME in the grid even it is not present in server settings', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].name)).toBeVisible();
    });
  });

  describe('when a field a property in coldDefs has a different value in server settings', () => {

    const COL_DEFS: ColDef[] = [
      {
        headerName: 'Id',
        field: 'id',
        filter: 'agTextColumnFilter',
        hide: false,
      },
      {
        headerName: 'Name',
        field: 'name',
        filter: 'agTextColumnFilter',
        hide: true,
      },
    ];

    const SERVER_SETTINGS = {
      columnSetting: [
        {
          colId: 'id', hide: false, aggFunc: null, width: 200, pivotIndex: null, pinned: null, rowGroupIndex: null,
        },
        {
          colId: 'name', hide: false, aggFunc: null, width: 200, pivotIndex: null, pinned: null, rowGroupIndex: null,
        },
      ],
    };

    beforeEach(async () => {
      userSettings = {
        value: {
          data: {
            data: [
              {
                setting: JSON.stringify(SERVER_SETTINGS),
              },
            ],
          },
        },
      };

      render(
        <Provider store={store}>
          <CommonGrid
            createNewText="Common Grid"
            pagination
            enableBrowserTooltips
            rowSelection={ROWSELECTION.SINGLE}
            hasPrivilegeToCreateNew
            hasPrivilegeToDelete
            hasPrivilegetoView
            rowData={ROW_DATA_MOCK}
            noResults={false}
            isBeforeLogout
            columnDefs={COL_DEFS}
            frameworkComponents={{}}
            actionMenuClass={{}}
            handleCreateNew={handleCreateNew}
            onRefresh={handleRefresh}
            onSelectionChanged={handleSelection}
            showActionMenuItem={handleShowActionMenu}
            onActionMenuItemClick={handleSelectItemMenuAction}
            saveGridSettings={handleSaveGridSettings}
            getGridSettingsFromServer={handleGetGridSettingsFromServer}
            lastUpdatedAt={new Date('Thu Aug 27 2020 12:13:34 GMT-0300')}
          />
        </Provider>,
      );

      await screen.findByRole('grid');
    });

    it('should render field ID present in coldDefs and server settings, both having hide:false ', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].id)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].id)).toBeVisible();
    });

    it('should prioritize server settings and render field NAME even having hide:true in cold def, because in server settings it has hide:false', async () => {
      expect(await screen.findByText(ROW_DATA_MOCK[0].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[1].name)).toBeVisible();
      expect(await screen.findByText(ROW_DATA_MOCK[2].name)).toBeVisible();
    });
  });

});
