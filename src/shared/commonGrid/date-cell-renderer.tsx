import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { getDateTimeFormatTemplate } from '../../utils';

export default class DateTimeCellRenderer extends Component<any, any> {
  render() {
    const field = this.props.colDef.field.split('.');
    let data: any;
    if (field.length > 1) {
      data = this.props.data && this.props.data[field[0]][field[1]] ?
        this.props.data[field[0]][field[1]] : '';
    } else {
      data = this.props.data && this.props.data[this.props.colDef.field] ?
        this.props.data[this.props.colDef.field] : '';
    }

    return (
      <Fragment>
        {(data === '' || data === 'undefined' || data === null) ? '' :
          moment(data).format(getDateTimeFormatTemplate())}
      </Fragment>
    );
  }
}
