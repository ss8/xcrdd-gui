import moment from 'moment';
import { FilterModel } from './common-grid.types';

type Props = {
  additionalFilterSettings?: string,
  adaptToFilterModel?: (filterModel:FilterModel) => FilterModel;
}

export default class ServerSideDatasource {
  /* eslint-disable-next-line */
  private props: any;

  private FILTER_TYPES: { [key: string]: string } = {
    equals: "eq '@'",
    contains: "like '%25@%25'",
    startsWith: "like '@%25'",
    endsWith: "like '%25@'",
    notContains: "not like '%25@%25'",
    notEqual: "ne '@'",
    lessThan: "lt '@'",
    lessThanOrEqual: "le '@'",
    greaterThan: "gt '@'",
    greaterThanOrEqual: "ge '@'",
    set: 'IN (@)',
  };

  constructor(props: Props) {
    this.props = props;
  }

  getRows(params: any) {
    const { request } = params;
    const optionsText = this.buildParamsFromJsonForXREST(request);
    this.props.getDataFromServer(optionsText)
      .then((resp: any) => {
        params.successCallback(resp.value.data.data, resp.value.data.metadata.recordsInView);
      }).catch((e: any) => {
        params.failCallback();
      });
  }

  buildParamsFromJsonForXREST(request: any) {
    const top = this.getTop(request); // Pagination number of rows to get
    // Pagination the starting row number of the next requested data block
    const skip = this.getSkip(request);
    const order = this.getSortOrder(request); // Sorting
    const filter = this.getFilter(request, this.props.additionalFilterSettings); // Filtering
    let result = `${top}&${skip}&${order}&${filter}`;

    while (result.indexOf('&&') >= 0) {
      result = result.replace('&&', '&');
    }
    if (result.endsWith('&')) result = result.substring(0, result.length - 1);
    return (result);
  }

  getFilter(request: any, additionalFilterSettings: string) {
    let filter = '';
    if ('filterModel' in request) {
      // adapt filter
      const newFilterModel = this.props.adaptToFilterModel ?
        this.props.adaptToFilterModel(request.filterModel) : request.filterModel;

      const keys = Object.keys(newFilterModel);
      let iKey;
      // Step through each filter item and collect its condition
      for (iKey = 0; iKey < keys.length; iKey += 1) {
        const filterItem = newFilterModel[keys[iKey]];
        if ('type' in filterItem) {
          if (filterItem.type in this.FILTER_TYPES) {
            let value = null;
            // We found a matching filter-type, so we know how to construct this
            if ('dateFrom' in filterItem) {
              value = filterItem.dateFrom;
              if ((value == null) && 'dateTo' in filterItem) value = filterItem.dateTo;
            } else if ('filter' in filterItem) value = filterItem.filter;
            if (value != null) {
              // If the column is date convert the value to UTC string
              if (filterItem.filterType === 'date') {
                value = `${moment(value).format('YYYY-MM-DDTHH:mm:ss')}Z`;
              }
              // We have the field, operation, and value, so compase the condition
              const operation = this.FILTER_TYPES[filterItem.type].replace('@', value);
              if (filter.length > 0) filter += ' and ';
              filter = (`${filter} (${keys[iKey]}  ${operation})`);
            }
          }
        } else if ('filterType' in filterItem && filterItem.filterType === 'set') {
          if (keys[iKey] === 'reporting.provisionStatus') {
            filter = filterItem.values.reduce((acc: any, value: any) => {
              return `${acc}(reporting.provisionStatus eq '${value}') or `;
            }, '').slice(0, -3);
          } else {
            const values = `'${filterItem.values.join("','")}'`;
            const operation = this.FILTER_TYPES[filterItem.filterType].replace('@', values);
            if (filter.length > 0) filter += ' and ';
            filter = (`${filter} (${keys[iKey]}  ${operation})`);
          }
        }
      }
    }

    if (additionalFilterSettings) {
      if (filter.length > 0) {
        filter = `$filter=${filter}and${additionalFilterSettings}`;
      } else {
        filter = (`$filter=${additionalFilterSettings}`);
      }
    } else if (filter.length > 0) filter = (`$filter=${filter}`);
    return (filter);
  }

  getTop = (request: any) => {
    const start = 'startRow' in request ? request.startRow : 0;
    const end = 'endRow' in request ? request.endRow : -1;
    // TODO : Is this correct /valid ? - Need to check
    return ((start < end) ? `$top= ${end - start}` : '');
  }

  getSkip = (request: any) => {
    const start = 'startRow' in request ? request.startRow : 0;
    return ((start > 0) ? `$skip= ${start}` : '');
  }

  getSortOrder = (request: any) => {
    let order = '';
    if ('sortModel' in request) {
      const sorts = request.sortModel;
      // Step through each sort-by item and collect its column-name and ordering
      for (let iSort = 0; iSort < sorts.length; iSort += 1) {
        const sortItem = sorts[iSort];
        if ('colId' in sortItem) {
          if (order.length > 0) order = `${order},`;
          order += sortItem.colId;
          if ('sort' in sortItem /* && (sortItem.sort != "asc") */) {
            order = `${order}  ${sortItem.sort}`;
          }
        }
      }
    }

    if (order.length > 0) order = `$orderBy= ${order}`;
    return (order);
  }
}
