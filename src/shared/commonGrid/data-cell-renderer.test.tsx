import React from 'react';
import { shallow } from 'enzyme';
import DateTimeCellRenderer from './date-cell-renderer';

describe('<DateTimeCellRenderer />', () => {
  let testMocks: any;
  let wrapper: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.props = {
      colDef: {
        field: 'dateTime',
      },
      data: {
        dateTime: '2020-06-16T18:20:00Z',
      },
    };

    wrapper = shallow(<DateTimeCellRenderer {...testMocks.props} />);
  });

  describe('render()', () => {
    it('should render the date as expected', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if data null', () => {
      wrapper.setProps({
        data: { dateTime: null },
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if data empty string', () => {
      wrapper.setProps({
        data: { dateTime: '' },
      });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render empty string if data undefined string', () => {
      wrapper.setProps({
        data: { dateTime: 'undefined' },
      });
      expect(wrapper).toMatchSnapshot();
    });
  });
});
