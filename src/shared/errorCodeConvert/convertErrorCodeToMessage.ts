import errorCodeConvert from './errorCodeConvert';
import roleLabels from '../../roles/roles-labels.json';

function convertErrorcodeToMessage(error: any) {
  if (errorCodeConvert[error.errorCode]) {
    if (error.errorCode === 'ROLE_LACKS_REQUIRED_PERMISSIONS') {
      let permissionDepency = error.errorMessage.split(':').pop().replace(/;([^;]*)$/, '.');
      const roleLabelsConvertKey = Object.keys(roleLabels.roles.permissions);
      roleLabelsConvertKey.forEach((key: string) => {
        permissionDepency = permissionDepency.split(key).join(
          (roleLabels.roles.permissions as any)[key],
        );
      });
      return errorCodeConvert[error.errorCode]
        .replace(/%t/g, permissionDepency).replace(/%s/g, error.errorFieldName);
    }
    return errorCodeConvert[error.errorCode].replace(/%s/g, error.errorFieldName);
  }
  return error.errorMessage;
}

export default convertErrorcodeToMessage;
