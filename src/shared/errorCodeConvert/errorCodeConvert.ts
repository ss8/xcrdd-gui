const errorCodeConvert : any = {
  DUPLICATE_ROLE: 'Unable to create/update role %s: Proposed role name has already been used.',
  DUPLICATE_TEAM: 'Unable to create/update team %s: Proposed team name has already been used.',
  DUPLICATE_USER: 'Cannot create/update a user with the username %s. The propossed user name for the account has already been used.',
  ROLE_LACKS_REQUIRED_PERMISSIONS: 'Unable to create new role %s: Missing permission dependencies: %t',
  // Todo: verify the error sending out from XRest
  500: 'Another warrant already exists with the name %s. Please assign a different name to this warrant.',
  // x7xx: 'The target ID %s has already been used the maximum allowed times and
  //       cannot be applied to another case.',
  // x8xx: 'The CF (%s) selected for warrant %s case %s does not exist and cannot be used.',
  // x9xx: 'The warrant cannot be created because the
  //       existing warrants have already exhausted the number of available (COGRP) licenses.',
};
export default errorCodeConvert;
