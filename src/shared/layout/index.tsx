// eslint-disable-next-line max-classes-per-file
import React from 'react';
import { Dialog } from '@blueprintjs/core';
import { ResizableBox } from 'react-resizable';

export const withDialog = (WrappedComponent: any) => class WithDialog extends React.Component {
  render() {
    return <Dialog usePortal isOpen canEscapeKeyClose><WrappedComponent /></Dialog>;
  }
};

export const withResizableOverlay = (WrappedComponent: any, options: any) =>
  class WithResizableOverlay extends React.Component {
    render() {
      return <ResizableBox {...options}><WrappedComponent {...this.props} /></ResizableBox>;
    }
  };
