import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';

const primaryIntentColor = '#2965CC';

export const CustomTabs = withStyles({
  indicator: {
    backgroundColor: primaryIntentColor,
  },
})(Tabs);

  interface CustomTabProps {
    label: string;
  }

export const CustomTab = withStyles((theme: Theme) =>
  createStyles({
    root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      fontFamily: [
        'Arial',
        'sans-serif',
      ].join(','),
      '&:hover': {
        color: primaryIntentColor,
        opacity: 1,
      },
      '&$selected': {
        color: primaryIntentColor,
        fontWeight: theme.typography.fontWeightMedium,
      },
      '&:focus': {
        color: primaryIntentColor,
      },
    },
    selected: {},
  }))((props: CustomTabProps) => <Tab disableRipple {...props} />);
