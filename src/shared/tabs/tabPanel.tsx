import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

// eslint-disable-next-line import/prefer-default-export
export function TabPanel(props: any) {
  const {
    children, value, index, ...other
  } = props;

  return (
    <div
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

TabPanel.defaultProps = {
  children: null,
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};
