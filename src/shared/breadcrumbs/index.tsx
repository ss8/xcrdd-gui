import React from 'react';
import { Breadcrumbs, Breadcrumb } from '@blueprintjs/core';

const BREADCRUMBS: any = [
  { href: '/events/grid', icon: 'folder-close', text: 'Intercepts' },
  { href: '/people/grid', icon: 'folder-close', text: 'Provision' },
  { icon: 'document', text: '(Selection)' },
];

export class ContextBreadcrumbs extends React.Component {
  public render() {
    return (
      <Breadcrumbs
        currentBreadcrumbRenderer={this.renderCurrentBreadcrumb}
        items={BREADCRUMBS}
      />
    );
  }

  private renderCurrentBreadcrumb =
    ({ text, ...restProps }: any) => (<Breadcrumb {...restProps}>{text} </Breadcrumb>);
}

export default ContextBreadcrumbs;
