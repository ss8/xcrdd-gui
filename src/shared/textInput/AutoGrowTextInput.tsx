import React, { Fragment } from 'react';
import {
  IInputGroupProps, InputGroup,
} from '@blueprintjs/core';

declare type HTMLInputProps = React.InputHTMLAttributes<HTMLInputElement>;

interface IAutoGrowTextInput {
  inputGroupProps: IInputGroupProps & HTMLInputProps;
  maxWidth?: number;
}
/**
 * This is a text field that extends its width as user types.  It will grow until the text field
 * reaches its max characters
 * allowed, or until the text field reaches its maxLength.
 *
 * This AutoGrowTextInput uses a hidden div element to grow the input field in the same container.
 * It assumes that the input field's style takes up 100% width of its containing parent
 */
export default class AutoGrowTextInput extends
  React.Component<IAutoGrowTextInput, any> {
  render() {
    const sizerValue = `${this.props.inputGroupProps.value}`;
    const sizerStyle: {[k: string]: any } = { visibility: 'hidden', height: 0, paddingLeft: 20 };

    if (this.props.maxWidth) {
      sizerStyle.maxWidth = `${this.props.maxWidth - 50}px`;
    }
    return (
      <Fragment>
        <InputGroup {...this.props.inputGroupProps} />
        <div style={sizerStyle}> {sizerValue}</div>
      </Fragment>
    );
  }
}
