import React from 'react';
import { Button, Collapse, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { MessageLabel } from './MessageLabel';
import { MessageType } from './MessageLabel.type';
import './collapsible-form.scss';

type Props = {
  title: string,
  isOpen?: boolean,
  message?: string | null,
  messageType?: MessageType,
}

export default class CollapsibleForm extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isOpen: props.isOpen || false,
    };
  }

  private handleClick = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  public render() : JSX.Element | null {
    const { isOpen } = this.state;
    const {
      title, messageType, message, children,
    } = this.props;

    if (!children) {
      return null;
    }

    return (
      <div>
        <div className="collapsible-form">
          <Button
            data-testid="CollapsibleFormButton"
            minimal
            intent={Intent.PRIMARY}
            text={title}
            onClick={this.handleClick}
            rightIcon={isOpen ? IconNames.CARET_UP : IconNames.CARET_DOWN}
          />
          <MessageLabel
            bShow={!isOpen}
            type={messageType}
            message={message}
          />
        </div>
        <Collapse isOpen={isOpen}>
          {children}
        </Collapse>
      </div>
    );
  }
}
