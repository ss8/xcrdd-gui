import React from 'react';
import {
  render,
  screen,
} from '@testing-library/react';
import { IconNames } from '@blueprintjs/icons';
import CollapsibleForm from './CollapsibleForm';
import { MessageType } from './MessageLabel.type';

describe('<CollapsibleForm />', () => {
  it('should render with collapsed form', () => {
    const messageStr = 'This is an error';
    const childText = 'Hello world';

    render(
      <CollapsibleForm
        title="I am a collapsible form"
        isOpen={false}
        messageType={MessageType.Error}
        message={messageStr}
      >
        <div>{childText}</div>
      </CollapsibleForm>,
    );

    // When form is collapsed, we should see message text and down caret arrow.
    expect(screen.queryByText(IconNames.CARET_DOWN)).not.toBeNull();
    expect(screen.queryByText(childText)).toBeNull();
    expect(screen.getByTestId('MessageIcon')).toBeVisible();
    expect(screen.getByText(messageStr)).toBeVisible();
  });

  it('should render with uncollapsed form', () => {
    const messageStr = 'This is an error';
    const childText = 'Hello world';

    render(
      <CollapsibleForm
        title="I am a collapsible form"
        isOpen
        messageType={MessageType.Error}
        message={messageStr}
      >
        <div>{childText}</div>
      </CollapsibleForm>,
    );

    // When form is expanded, we should see child component and up caret arrow.
    expect(screen.queryByText(IconNames.CARET_UP)).not.toBeNull();
    expect(screen.queryByText(childText)).not.toBeNull();
    expect(screen.queryByTestId('MessageIcon')).toBeNull();
    expect(screen.queryByText(messageStr)).toBeNull();
  });

  it('should not render with no children', () => {
    const messageStr = 'This is an error';

    const { container } = render(
      <CollapsibleForm
        title="I am a collapsible form"
        isOpen
        messageType={MessageType.Error}
        message={messageStr}
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

});
