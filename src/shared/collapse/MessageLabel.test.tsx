import React from 'react';
import {
  render,
  screen,
} from '@testing-library/react';
import { IconNames } from '@blueprintjs/icons';
import MessageLabel from './MessageLabel';
import { MessageType } from './MessageLabel.type';

describe('<MessageLabel />', () => {

  it('should render with message with error icon', () => {
    const messageStr = 'This is an error';

    render(
      <MessageLabel
        bShow
        type={MessageType.Error}
        message={messageStr}
      />,
    );

    expect(screen.getByTestId('MessageIcon').getAttribute('icon')).toEqual(IconNames.ERROR);
    expect(screen.getByTestId('MessageIcon')).toBeVisible();
    expect(screen.getByText(messageStr)).toBeVisible();

  });

  it('should render with message with info icon', () => {
    const messageStr = 'This is an info';
    render(
      <MessageLabel
        bShow
        type={MessageType.Info}
        message={messageStr}
      />,
    );

    expect(screen.getByTestId('MessageIcon').getAttribute('icon')).toEqual(IconNames.INFO_SIGN);
    expect(screen.getByTestId('MessageIcon')).toBeVisible();
    expect(screen.getByText(messageStr)).toBeVisible();
  });

  it('should not render if message is null', () => {
    const { container } = render(
      <MessageLabel
        bShow
        type={MessageType.Info}
        message={null}
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

  it('should not render if bShow is false', () => {
    const { container } = render(
      <MessageLabel
        bShow={false}
        type={MessageType.Info}
        message="This is a message"
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

});
