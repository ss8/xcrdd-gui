import { Label, Icon, Intent } from '@blueprintjs/core';
import { IconNames, IconName } from '@blueprintjs/icons';
import './message-label.scss';
import React, {
  FunctionComponent,
} from 'react';
import { MessageType } from './MessageLabel.type';

type Props = {
    bShow: boolean
    type?: MessageType,
    message?: string | null
}

export const MessageLabel: FunctionComponent<Props> = ({
  bShow,
  type,
  message,
}: Props) => {
  if (bShow === false || !message) {
    return null;
  }

  const iconName:IconName = type === 'error' ? IconNames.ERROR : IconNames.INFO_SIGN;
  const color:Intent = type === 'error' ? Intent.DANGER : Intent.NONE;

  return (
    <div className="message-label">
      <Icon data-testid="MessageIcon" intent={color} icon={iconName} iconSize={15} />
      <Label className="field-error-message">{message}</Label>
    </div>
  );
};

export default MessageLabel;
