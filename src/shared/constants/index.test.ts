import { dateTimeFormatter } from './index';

describe('dateTimeFormatter()', () => {
  it('should format the date as expected', () => {
    const date = '2020-06-17T13:43:02.275Z';
    expect(dateTimeFormatter(date)).toEqual('06/17/20 06:43:02 AM');
  });

  it('should format the current date as expected', () => {
    const spy = jest.spyOn(Date, 'now').mockImplementation(() => 1592401503751);
    expect(dateTimeFormatter(null)).toEqual('06/17/20 06:45:03 AM');
    spy.mockRestore();
  });
});
