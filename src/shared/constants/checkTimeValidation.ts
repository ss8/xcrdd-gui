const timeZoneConvert : any = {
  'US/Alaska': 'America/Anchorage',
  'US/Aleutian': 'America/Adak',
  'US/Arizona': 'America/Phoenix',
  'US/Central': 'America/Chicago',
  'US/East-Indiana': 'America/Indiana/Indianapolis',
  'US/Eastern': 'America/New_York',
  'US/Hawaii': 'Pacific/Honolulu',
  'US/Indiana-Starke': 'America/Indiana/Knox',
  'US/Michigan': 'America/Detroit',
  'US/Mountain': 'America/Shiprock',
  'US/Pacific': 'America/Vancouver',
  'US/Pacific-New': 'Canada/Pacific',
  'US/Samoa': 'Pacific/Pago_Pago',
  'PST/PDT': 'America/Vancouver',
  'MST/MDT': 'America/Shiprock',
  'CST/CDT': 'America/Chicago',
  'EST/EDT': 'America/New_York',
  'GMT+0': 'GMT',
  'GMT-0': 'GMT',
  GMT0: 'GMT',
  'Antarctica/McMurdo': 'Pacific/Saipan',
};

function convertToStandardTimeZone(timezone:any) {
  if (timeZoneConvert[timezone]) return timeZoneConvert[timezone];
  return timezone;
}

// input: start time, stop time and time zone of warrant or warrant case
// output: if start time< current time < stop time, return true; else return false
export default function checkTimeValidation(startTime: any, stopTime: any, aTimeZone: any): boolean {
  const timeZone = convertToStandardTimeZone(aTimeZone);
  if (timeZone === null || timeZone === undefined || timeZone === '') {
    return false;
  }
  const curTime = (new Date((new Date()).toLocaleString('en-US', { timeZone }))).getTime();
  const startTimeValue = (startTime === null || startTime === '' || startTime === undefined) ? curTime : (new Date(startTime)).getTime();
  const stopTimeValue = (new Date(stopTime)).getTime();
  return ((startTimeValue < curTime) && (curTime < stopTimeValue));
}

export function checkTimeValidationInComputerTimezone(startTime: string | undefined, stopTime: string): boolean {
  const curTime = Date.now();
  const startTimeValue = !startTime ? curTime : (new Date(startTime)).getTime();
  const stopTimeValue = (new Date(stopTime)).getTime();
  return ((startTimeValue < curTime) && (curTime < stopTimeValue));
}

/**
 * @returns true if given time is before the current time.
 * @param aTime given time
 * @param aTimeZone time zone
 */
export function checkGivenTimeWithCurrentTime(aTime: any, aTimeZone: any) {
  const timeZone = convertToStandardTimeZone(aTimeZone);
  if (timeZone === null || timeZone === undefined || timeZone === '') {
    return false;
  }
  const curTime = (new Date((new Date()).toLocaleString('en-US', { timeZone }))).getTime();
  const aTimeValue = (aTime === null || aTime === '' || aTime === undefined) ? curTime : (new Date(aTime)).getTime();
  return aTimeValue < curTime;
}

export function areTimesEqual(aStartTime:any, aStopTime:any, aTimeZone:any,
  bStartTime:any, bStopTime:any, bTimeZone: any) {
  return (aStartTime === bStartTime) && (aStopTime === bStopTime) && (aTimeZone === bTimeZone);
}

export function addToDate(datetime: Date, value: number, valueUnit: 'day' | 'month') : Date {
  const date = new Date(datetime);
  if (valueUnit === 'day') {
    date.setDate(date.getDate() + value);
  } else {
    date.setMonth(date.getMonth() + value);
  }

  return date;
}
