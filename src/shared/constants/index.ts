import moment from 'moment';
import { getDateTimeFormatTemplate } from '../../utils';

export const dateTimeFormatter = (data: any) => {
  if (!data) {
    return moment(Date.now()).format(getDateTimeFormatTemplate());
  }

  return moment(new Date(data)).format(getDateTimeFormatTemplate());
};

const dateFilterComparatorClientSide =
  (filterLocalDateAtMidnight: Date, cellValue: string) => {
    const dateAsString = moment(cellValue).format('MM/DD/YYYY/HH/mm/ss');
    const dateParts = dateAsString.split('/');
    const cellDate = new Date(Number(dateParts[2]), Number(dateParts[0]) - 1, Number(dateParts[1]));
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }

    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }

    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
    return 0;
  };

const dateFilterOptions = ['equals', 'greaterThan', 'lessThan'];

export const dateFilterParams = {
  comparator: dateFilterComparatorClientSide,
  browserDatePicker: true,
  filterOptions: dateFilterOptions,
};

export const dashboardPrivileges = [
  'dashboard_af',
  'dashboard_cf',
  'dashboard_warrant',
  'dashboard_KPI',
  'dashboard_alarm',
  'dashboard_license',
  'dashboard_sh',
  'dashboard_admin',
];

export const groupsMap: { [key: string]: string } = {
  dashboard_af: 'AF',
  dashboard_cf: 'CF',
  dashboard_warrant: 'Warrants',
  dashboard_KPI: 'KPIs',
  dashboard_alarm: 'Alarms',
  dashboard_license: 'Licenses',
  dashboard_sh: 'Dashboard System Health',
  dashboard_admin: 'Dashboard Administrators',
  dashboard_user: 'Regular Users',
};

export const rolesMapSet = {
  dashboard_sh: new Set(['ROLE_SHARE_DASHBOARDS', 'ROLE_EXPORT_DASHBOARDS']),
  dashboard_admin: new Set([
    'ROLE_SAVE_DASHBOARDS',
    'ROLE_INVOKE_ACTIONS',
    'ROLE_CREATE_SOURCES',
    'ROLE_READ_FORMULAS',
    'ROLE_SAVE_FILTERS',
    'ROLE_SHARE_DASHBOARDS',
    'ROLE_EXPORT_DASHBOARDS',
    'ROLE_MANAGE_ACTION_TEMPLATES',
    'ROLE_EDIT_FORMULAS',
    'ROLE_MANAGE_CONNECTIONS',
    'ROLE_ADMINISTER_DASHBOARDS',
    'ROLE_RAW_DATA_ACCESS',
    'ROLE_ADMINISTER_USERS',
  ]),
};

export const basicRolesSet = new Set([
  'ROLE_SAVE_DASHBOARDS',
  'ROLE_INVOKE_ACTIONS',
  'ROLE_READ_FORMULAS',
  'ROLE_SAVE_FILTERS',
  'ROLE_MANAGE_ACTION_TEMPLATES',
  'ROLE_EDIT_FORMULAS',
  'ROLE_RAW_DATA_ACCESS',
]);
