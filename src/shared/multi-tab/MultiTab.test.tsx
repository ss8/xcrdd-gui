import React from 'react';
import { render, screen } from '@testing-library/react';
import { mount, ReactWrapper } from 'enzyme';
import cloneDeep from 'lodash.clonedeep';
import {
  Button, Icon, Tabs,
} from '@blueprintjs/core';
import MultiTab, { MultiTabProps, TabProp } from './MultiTab';

describe('<MultiTab />', () => {
  const getNewTab = (id:number) => {
    return {
      id,
      title: `Tab-${id}`,
      panel: <div />,
    };
  };

  const getTabs = (num: number) => {
    const tabs = [];
    for (let i = 0; i < num; i += 1) {
      tabs.push(getNewTab(i + 1));
    }
    return tabs;
  };

  const PROPS: MultiTabProps = {
    tabContainerId: 'multiTabId',
    tabs: [],
    maxNumberOfTabs: 3,
    isEditing: false,
    addTab: () => {},
    removeTab: (id: number | string) => {},
  };

  const wrapper: ReactWrapper = mount(
    <MultiTab {...cloneDeep(PROPS)} />,
  );

  describe('Test - Show/Hide of add and remove buttons', () => {
    let props: MultiTabProps;
    beforeEach(() => {
      props = cloneDeep(PROPS);
    });
    it('isEditing is set false; Should not render add or remove button', () => {
      wrapper.setProps(props);
      expect(wrapper.find(Button).filterWhere((n) => n.prop('icon') === 'plus')).toHaveLength(0);
      expect(wrapper.find(Icon).filterWhere((n) => n.prop('icon') === 'remove')).toHaveLength(0);
    });

    it('isEditing is set true; Passed one tab; Should render only add button', () => {
      props.tabs.push(...getTabs(1));
      props.isEditing = true;
      props.maxNumberOfTabs = 3;
      wrapper.setProps(props);
      expect(wrapper.find(Button).filterWhere((n) => n.prop('icon') === 'plus')).toHaveLength(1);
      expect(wrapper.find(Icon).filterWhere((n) => n.prop('icon') === 'remove')).toHaveLength(0);
    });

    it('isEditing is set true; Passed 3 tabs; Max number of tabs 3; Should render only remove button', () => {
      props.tabs.push(...getTabs(3));
      props.isEditing = true;
      props.maxNumberOfTabs = 3;
      wrapper.setProps(props);
      expect(wrapper.find(Button).filterWhere((n) => n.prop('icon') === 'plus')).toHaveLength(0);
      expect(wrapper.find(Icon).filterWhere((n) => n.prop('icon') === 'remove')).toHaveLength(3);
    });
  });

  describe('Test - Add Tab, Remove tab and current tab selected', () => {
    let mockAddCallback: jest.Mock;
    let mockRemoveCallback: jest.Mock;
    let props: MultiTabProps;
    beforeEach(() => {
      mockAddCallback = jest.fn();
      mockRemoveCallback = jest.fn();
      props = cloneDeep(PROPS);
    });
    it('Passed 2 tabs; Should select 1st tab', () => {
      props.tabs.push(...getTabs(2));
      props.isEditing = true;
      props.maxNumberOfTabs = 3;
      wrapper.setProps(props);
      wrapper.update();
      const firstTabId = props.tabs[0].id;
      expect(wrapper.find(Tabs).prop('selectedTabId')).toBe(firstTabId);
    });

    it('Passed 2 tabs; Clicked add button; Should call function addTab; Should select third tab', () => {
      props.tabs.push(...getTabs(2));
      props.isEditing = true;
      props.maxNumberOfTabs = 3;
      props.addTab = mockAddCallback;
      wrapper.setProps(props);
      const addButton = wrapper.find(Button).filterWhere((n) => n.prop('icon') === 'plus').at(0);
      addButton.simulate('click');
      expect(mockAddCallback).toBeCalledTimes(1);
      props.tabs = [];
      props.tabs.push(...getTabs(3));
      wrapper.setProps(props);
      wrapper.update();
      const thirdTabId = props.tabs[2].id;
      expect(wrapper.find(Tabs).prop('selectedTabId')).toBe(thirdTabId);
    });

    it('Passed 3 tabs; Clicked remove button on 2nd Tab; Should call function removeTab; Should remove second tab; Should keep in current tab',
      () => {
        props.tabs.push(...getTabs(3));
        props.isEditing = true;
        props.maxNumberOfTabs = 3;
        props.removeTab = mockRemoveCallback;
        wrapper.setProps(props);
        const removeButton = wrapper.find(Icon).filterWhere((n) => n.prop('icon') === 'remove').at(1);
        removeButton.simulate('click');
        expect(mockRemoveCallback).toHaveBeenCalledWith(2);
        expect(mockRemoveCallback).toBeCalledTimes(1);
        props.tabs = [];
        props.tabs.push(...getTabs(2));
        wrapper.setProps(props);
        wrapper.update();
        const firstTabId = props.tabs[0].id;
        expect(wrapper.find(Tabs).prop('selectedTabId')).toBe(firstTabId);
      });
  });

  describe('when a tab has an error', () => {
    let props: MultiTabProps;

    beforeEach(() => {
      props = cloneDeep(PROPS);
    });

    it('should add a class name indicating the error', () => {
      props.tabs = [{
        id: 1,
        title: 'Title1',
        panel: <span />,
        hasErrors: true,
      }];
      wrapper.setProps(props);
      wrapper.update();
      expect(wrapper.find('.multi-tab-has-error')).toHaveLength(1);
    });
  });

  describe('when a tab has no error', () => {
    let props: MultiTabProps;

    beforeEach(() => {
      props = cloneDeep(PROPS);
    });

    it('should not add a class name indicating the error', () => {
      props.tabs = [{
        id: 1,
        title: 'Title1',
        panel: <span />,
        hasErrors: false,
      }];
      wrapper.setProps(props);
      wrapper.update();
      expect(wrapper.find('.multi-tab-has-error')).toHaveLength(0);
    });
  });

  describe('min and max number of tabs', () => {
    let tabs: TabProp[] = [];

    beforeEach(() => {
      tabs = [{
        id: 1,
        title: 'tab1',
        panel: <span />,
        hasErrors: false,
      }, {
        id: 2,
        title: 'tab2',
        panel: <span />,
        hasErrors: false,
      }];
    });

    it('should not show remove button or add button min and max equals to the number of tabs', () => {
      render(
        <MultiTab
          tabContainerId="multiTabId"
          tabs={tabs}
          minNumberOfTabs={2}
          maxNumberOfTabs={2}
          isEditing
          addTab={() => {}}
          removeTab={() => {}}
        />,
      );

      expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
      expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    });

    it('should not show remove button if min is equal to the number of tabs', () => {
      render(
        <MultiTab
          tabContainerId="multiTabId"
          tabs={tabs}
          minNumberOfTabs={2}
          maxNumberOfTabs={3}
          isEditing
          addTab={() => {}}
          removeTab={() => {}}
        />,
      );

      expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
      expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(1);
    });

    it('should not show add button if max is equal to the number of tabs', () => {
      render(
        <MultiTab
          tabContainerId="multiTabId"
          tabs={tabs}
          minNumberOfTabs={1}
          maxNumberOfTabs={2}
          isEditing
          addTab={() => {}}
          removeTab={() => {}}
        />,
      );

      expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(2);
      expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    });
  });
});
