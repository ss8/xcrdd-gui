import React, { useState, useEffect } from 'react';
import {
  Tab, Tabs, Button, Tooltip, Icon, Position, Intent,
} from '@blueprintjs/core';
import './multi-tab.scss';

type RemovePanelFunction = (index: number | string) => void;
type AddPanelFunction = () => void;
export interface TabProp {
  id: number | string,
  title: string,
  panel: JSX.Element,
  hasErrors?: boolean,
}

export interface MultiTabProps {
  tabContainerId: string
  tabs: TabProp[]
  addTab?: AddPanelFunction,
  removeTab?: RemovePanelFunction,
  minNumberOfTabs?: number,
  maxNumberOfTabs?: number,
  isEditing: boolean
}

type MultiTabFunctionComponent = (props: MultiTabProps) => JSX.Element;
const MultiTab: MultiTabFunctionComponent = ({
  tabContainerId,
  tabs,
  addTab,
  removeTab,
  minNumberOfTabs = 1,
  maxNumberOfTabs = 10,
  isEditing,
}: MultiTabProps) => {
  const [currentTab, setCurrentTab] = useState<string | number | undefined>(undefined);
  const [hasAddedTab, setHasAddedTab] = useState(false);

  useEffect(() => {
    computeNextActiveTab();
  }, [tabs]);

  const computeNextActiveTab = () => {
    if (hasAddedTab) {
      setCurrentTab(tabs[tabs.length - 1].id);
      setHasAddedTab(false);

    } else {
      const foundTab = tabs.find(({ id }) => id === currentTab);

      if (foundTab == null && tabs.length > 0) {
        setCurrentTab(tabs[0].id);

      } else if (tabs.length === 0) {
        setCurrentTab(undefined);
      }
    }
  };

  const handleTabChange = (newTab: number | string) => {
    setCurrentTab(newTab);
  };

  const handleAddTab = () => {
    setHasAddedTab(true);
    addTab();
  };

  const renderTitle = ({ hasErrors, title, id }: TabProp) => {
    let className = '';
    if (hasErrors) {
      className = 'multi-tab-has-error';
    }

    return (
      <div data-testid={`MultiTab${title}`}>
        <Tooltip
          disabled={!hasErrors}
          content="Review values"
          intent={Intent.DANGER}
          position={Position.TOP}
          boundary="viewport"
        >
          <span className={className}>{title}</span>
        </Tooltip>
        {renderRemoveButton(id)}
      </div>
    );
  };

  const renderRemoveButton = (id: number | string) => {
    if (tabs.length === minNumberOfTabs || !isEditing) {
      return null;
    }

    const handleClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      event.stopPropagation();
      removeTab(id);
    };

    return (
      <Icon data-testid="MultiTabRemoveIcon" onClick={handleClick} icon="remove" className="multi-tab-remove-icon" />
    );
  };

  const renderAddButton = () => {
    if (tabs.length === maxNumberOfTabs || !isEditing) {
      return null;
    }

    return (
      <Button data-testid="MultiTabAddButton" icon="plus" small onClick={handleAddTab} className="multi-tab-add-button" />
    );
  };

  const renderTabs = () => {
    return (
      <div className="multi-tab-container">
        <Tabs id={tabContainerId} onChange={handleTabChange} selectedTabId={currentTab} large>
          {tabs.map((tab: TabProp) => {
            return (
              <Tab key={tab.id} id={tab.id} title={renderTitle(tab)} panel={tab.panel} />
            );
          })}
          {renderAddButton()}
        </Tabs>
      </div>
    );
  };

  return renderTabs();
};
export default MultiTab;
