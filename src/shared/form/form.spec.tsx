import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, {
  shallow,
  mount,
  ReactWrapper,
  ShallowWrapper,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Form from './index';
import formConfig from './form.spec.json';

Enzyme.configure({ adapter: new Adapter() });

describe('Form rendering engine', () => {
  it('throws an exception if no props are supplied', () => {
    const div = document.createElement('div');
    expect(() => {
      const props: any = {};
      ReactDOM.render(<Form {...props} />, div);
    }).toThrow();
  });
});

describe('Form rendering engine using the sample form', () => {
  let component: ReactWrapper;
  let ref:any;

  beforeEach(() => {
    component = mount(
      <Form
        ref={(instance) => { ref = instance; }}
        name={formConfig.key}
        fields={formConfig.forms.spec.fields}
        layout={formConfig.forms.spec.metadata.layout.double}
      />,
    );
  });

  afterEach(() => {
    component.unmount();
  });

  it('should return an instance of the Form builder', () => {
    const instance = component.instance();
    expect(instance).toBeInstanceOf(Form);
  });

  it('should call the initialize fields method one time', () => {
    const spy = jest.spyOn(Form, 'getDerivedStateFromProps');
    const localComponent = mount(
      <Form
        name={formConfig.key}
        fields={formConfig.forms.spec.fields}
        layout={formConfig.forms.spec.metadata.layout.double}
      />,
    );
    expect(spy).toHaveBeenCalledTimes(1);
    localComponent.unmount();
  });

  it('should render the Form builder with form tags', () => {
    const elem = component.find('form');
    expect(elem.length).toBe(2);
  });

  it('should render a text input for the "judge" field', () => {
    const elem = component.find('input[name="judge"]');
    expect(elem.length).toBe(1);
  });

  it('should render a text input for the "region" field', () => {
    const elem = component.find('input[name="region"]');
    expect(elem.length).toBe(1);
  });

  it('should render a text input for the "city" field', () => {
    const elem = component.find('input[name="city"]');
    expect(elem.length).toBe(1);
  });

  it('should render a text input for the "state" field', () => {
    const elem = component.find('input[name="state"]');
    expect(elem.length).toBe(1);
  });

  // it('should render a date field for the "startDate" field', () => {
  //   const elem = component.find('.bp3-datepicker .startDate');
  //   expect(elem.length).toBe(1);
  // });

  // it('should render a date field for the "endDate" field', () => {
  //   const elem = component.find('.bp3-datepicker .endDate');
  //   expect(elem.length).toBe(1);
  // });

  it('should render a textarea for the "notes" input', () => {
    const elem = component.find('textarea[name="notes"]');
    expect(elem.length).toBe(1);
  });

  it('should render a select for the "contacts" input', () => {
    const elem = component.find('select[name="contacts"]');
    expect(elem.length).toBe(1);
  });

  describe('when template field "port" has textSuggest type', () => {
    it('should render "port" input', () => {
      const input = component.find('input[name="port"]');
      expect(input.length).toBe(1);
    });

    it('should render "port" input', () => {
      const input = component.find('input[name="port"]');
      expect(input.props().placeholder).toBe('placeholder for port');
    });

    it('should render port options according the template when focus in textSuggest input', () => {
      component.find('input[name="port"]').simulate('focus');

      const portOptions = component.find('.bp3-menu-item');
      expect(portOptions).toHaveLength(3);
      expect(portOptions.at(0).text()).toBe('1111');
      expect(portOptions.at(1).text()).toBe('2222');
      expect(portOptions.at(2).text()).toBe('3333');
    });

    it('should select one option and set its value to textSuggest input', () => {
      component.find('input[name="port"]').simulate('focus');
      component.find('.bp3-menu-item').at(1).simulate('click');
      expect(component.find('input[name="port"]').props().value).toBe('2222');
    });

    it('should set textSuggest input value by typing', () => {
      component.find('input[name="port"]').simulate('change', { target: { value: '1234' } });
      expect(component.find('input[name="port"]').props().value).toBe('1234');
    });

    it('should render error message when textSuggest input value is invalid', () => {
      component.find('input[name="port"]').simulate('change', { target: { value: '123456789' } });
      expect(component.find('.bp3-label.field-error-message').text().trim()).toBe('Port must have 4 digits.');
    });
  });

  describe('radio button', () => {
    it('should set radio buttons options according to the template ', () => {
      expect(component.find('input[name="caseTarget"]').at(0).props().value).toEqual('YES');
      expect(component.find('input[name="caseTarget"]').at(1).props().value).toEqual('NO');
    });

    it('should should change form value when radio button change value', () => {
      component.find('input[name="caseTarget"]').at(0).simulate('change', { target: { value: 'YES' } });
      expect(ref?.getValues().caseTarget).toEqual('YES');

      component.find('input[name="caseTarget"]').at(1).simulate('change', { target: { value: 'NO' } });
      expect(ref?.getValues().caseTarget).toEqual('NO');

    });
  });
});
