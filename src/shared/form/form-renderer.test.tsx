import React, { Fragment } from 'react';
import { ReactWrapper, mount } from 'enzyme';
import { render, screen } from '@testing-library/react';
import { renderFields } from './form-renderer';

const CustomComponent = ({ value }: {value:string}) => <span data-testid="MyFieldCustom">{value}&nbsp;and info from component!!!</span>;

describe('form-renderer', () => {
  let context: any;
  let wrapper: ReactWrapper;

  beforeEach(() => {
    context = {
      props: {
        fields: {
          field1: {
            label: 'Field1',
            type: 'text',
            tabIndex: 1,
          },
          field2: {
            label: 'Field2',
            type: 'text',
            hide: 'true',
          },
          field3: {
            label: 'Field3',
            type: 'text',
            tabIndex: 2,
          },
          field4: {
            label: 'Field4',
            type: 'text',
            hide: 'true',
            renderAsEmptyIfHidden: true,
          },
          field5: {
            label: 'Field5',
            type: 'text',
            className: 'field5-class-name',
          },
          fieldText: {
            label: 'FieldText',
            type: 'text',
          },
          fieldAutoGrowText: {
            label: 'FieldAutoGrowText',
            type: 'autoGrowText',
          },
          fieldPassword: {
            label: 'FieldPassword',
            type: 'password',
          },
          fieldTextarea: {
            label: 'FieldTextarea',
            type: 'textarea',
          },
          fieldSelect: {
            label: 'FieldSelect',
            type: 'select',
            options: [{ value: 'option1', label: 'Option 1' }, { value: 'option2', label: 'Option 2' }],
          },
          fieldRadio: {
            label: 'FieldRadio',
            type: 'radio',
            options: [{ value: 'option1', label: 'Option 1' }, { value: 'option2', label: 'Option 2' }],
          },
          fieldNumber: {
            label: 'FieldNumber',
            type: 'number',
          },
          fieldDatetime: {
            label: 'FieldDatetime',
            type: 'datetime',
          },
          fieldCheckbox: {
            label: 'FieldCheckbox',
            type: 'checkbox',
          },
          fieldCustom: {
            label: 'FieldCustom',
            type: 'custom',
          },
        },
      },
      state: {
        fields: {
          field1: {
            label: 'Field1',
            value: '',
            hide: 'false',
          },
          field2: {
            label: 'Field2',
            value: '',
            hide: 'true',
          },
          field3: {
            label: 'Field3',
            value: '',
            hide: 'false',
          },
          field4: {
            label: 'Field4',
            value: '',
            hide: 'true',
          },
          field5: {
            label: 'Field5',
            value: '',
            hide: 'false',
          },
          fieldText: {
            label: 'FieldText',
            value: 'value for text',
            hide: 'false',
            htmlId: 'input-fieldAutoGrowText-0',
          },
          fieldAutoGrowText: {
            label: 'FieldAutoGrowText',
            value: 'value for autoGrowText',
            hide: 'false',
            htmlId: 'input-fieldAutoGrowText-1',
          },
          fieldPassword: {
            label: 'FieldPassword',
            value: 'value for password',
            hide: 'false',
            htmlId: 'input-fieldPassword-2',
          },
          fieldTextarea: {
            label: 'FieldTextarea',
            value: 'value for textarea',
            hide: 'false',
            htmlId: 'input-fieldTextarea-3',
          },
          fieldSelect: {
            label: 'FieldSelect',
            value: 'option2',
            hide: 'false',
            htmlId: 'input-fieldSelect-4',
            options: [{ value: 'option1', label: 'Option 1' }, { value: 'option2', label: 'Option 2' }],
          },
          fieldRadio: {
            label: 'FieldRadio',
            value: 'option2',
            hide: 'false',
            htmlId: 'input-fieldRadio-5',
            options: [{ value: 'option1', label: 'Option 1' }, { value: 'option2', label: 'Option 2' }],
          },
          fieldNumber: {
            label: 'FieldNumber',
            value: '1234',
            hide: 'false',
            htmlId: 'input-fieldNumber-6',
          },
          fieldDatetime: {
            label: 'FieldDatetime',
            value: '',
            hide: 'false',
            htmlId: 'input-fieldDatetime-7',
          },
          fieldCheckbox: {
            label: 'FieldCheckbox',
            value: true,
            hide: 'false',
            htmlId: 'input-fieldCheckbox-8',
          },
          fieldCustom: {
            label: 'FieldCustom',
            value: 'fieldCustomValue',
            hide: 'false',
            htmlId: 'input-fieldCustom-9',
          },
        },
      },
      handleChange: jest.fn(),
      handleTouched: jest.fn(),
      handleKeyPress: jest.fn(),
      getFieldClass: jest.fn(),
      handleDateTimeChange: jest.fn(),
    };
  });

  describe('renderFields()', () => {
    it('should render the single field as an input', () => {
      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1'])}
        </Fragment>,
      );

      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
    });

    it('should render a group with a single input', () => {
      const rowLayout = [{
        group: 'group1',
        fields: ['field1'],
      }];

      wrapper = mount(
        <Fragment>
          {renderFields(context, rowLayout)}
        </Fragment>,
      );

      expect(wrapper.find('[label="group1"]')).toHaveLength(1);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
    });

    it('should render an input field and an empty field', () => {
      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field2'])}
        </Fragment>,
      );

      expect(wrapper.find('div.field')).toHaveLength(2);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
      expect(wrapper.find('div.field').at(1).text()).toEqual('');
    });

    it('should not render an input that is hidden', () => {
      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field2'])}
        </Fragment>,
      );

      expect(wrapper.find('div.field')).toHaveLength(2);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
      expect(wrapper.find('input[data-test="input-field2"]')).toHaveLength(0);
      expect(wrapper.find('div.field').at(1).text()).toEqual('');
    });

    it('should render a hidden input as empty is renderAsEmptyIfHidden is defined', () => {
      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field4'])}
        </Fragment>,
      );

      expect(wrapper.find('div.field')).toHaveLength(3);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
      expect(wrapper.find('div.field').at(1).text()).toEqual('');
    });

    it('should render the field with the defined className', () => {
      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field5'])}
        </Fragment>,
      );

      wrapper.debug();

      expect(wrapper.find('div.field')).toHaveLength(3);
      expect(wrapper.find('div.field5-class-name')).toHaveLength(1);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(1);
      expect(wrapper.find('div.field').at(1).text()).toEqual('');
    });

    it('should not render a group that is hidden', () => {
      const rowLayout = [{
        group: 'group2',
        hide: 'true',
        fields: ['field2'],
      }];

      wrapper = mount(
        <Fragment>
          {renderFields(context, rowLayout)}
        </Fragment>,
      );

      expect(wrapper.find('[label="group1"]')).toHaveLength(0);
      expect(wrapper.find('input[data-test="input-field1"]')).toHaveLength(0);
    });

    it('should add the cssClassName defined in the props', () => {
      context.props.cssClassName = 'some-class-name';

      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1'])}
        </Fragment>,
      );

      expect(wrapper.find('.some-class-name')).toHaveLength(1);
    });

    it('should render fields as a row', () => {
      context.props.flexFlow = 'row';

      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field2', 'field3'])}
        </Fragment>,
      );

      expect(wrapper.find('.form-flex-row-css')).toHaveLength(1);
      expect(wrapper.find('.field')).toHaveLength(3);
    });

    it('should render fields as a column', () => {
      context.props.flexFlow = 'column';

      wrapper = mount(
        <Fragment>
          {renderFields(context, ['field1', '$empty', 'field2', 'field3'])}
        </Fragment>,
      );

      expect(wrapper.find('.form-flex-col-css')).toHaveLength(1);
      expect(wrapper.find('.field')).toHaveLength(3);
    });

    it('should render the fields for a group as a column', () => {
      const rowLayout = [{
        group: 'group1',
        fields: ['field1', '$empty', 'field2', 'field3'],
      }];

      wrapper = mount(
        <Fragment>
          {renderFields(context, rowLayout)}
        </Fragment>,
      );

      expect(wrapper.find('[label="group1"]').find('.form-flex-col-css')).toHaveLength(1);
      expect(wrapper.find('[label="group1"]').find('.field')).toHaveLength(3);
    });

    it('should render the fields for a group as a row', () => {
      const rowLayout = [{
        group: 'group1',
        flexDirection: 'row',
        fields: ['field1', '$empty', 'field2', 'field3'],
      }];

      wrapper = mount(
        <Fragment>
          {renderFields(context, rowLayout)}
        </Fragment>,
      );

      expect(wrapper.find('[label="group1"]').find('.form-flex-row-css')).toHaveLength(1);
      expect(wrapper.find('[label="group1"]').find('.field')).toHaveLength(3);
    });

    it('should be possible to select the field by label for text input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldText'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldText:')).toBeVisible();
      expect(screen.getByLabelText('FieldText:')).toBeEnabled();
      expect(screen.getByLabelText('FieldText:')).toHaveValue('value for text');
    });

    it('should be possible to select the field by label for autoGrowText input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldAutoGrowText'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldAutoGrowText:')).toBeVisible();
      expect(screen.getByLabelText('FieldAutoGrowText:')).toBeEnabled();
      expect(screen.getByLabelText('FieldAutoGrowText:')).toHaveValue('value for autoGrowText');
    });

    it('should be possible to select the field by label for password input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldPassword'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldPassword:')).toBeVisible();
      expect(screen.getByLabelText('FieldPassword:')).toBeEnabled();
      expect(screen.getByLabelText('FieldPassword:')).toHaveValue('value for password');
    });

    it('should be possible to select the field by label for textarea input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldTextarea'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldTextarea:')).toBeVisible();
      expect(screen.getByLabelText('FieldTextarea:')).toBeEnabled();
      expect(screen.getByLabelText('FieldTextarea:')).toHaveValue('value for textarea');
    });

    it('should be possible to select the field by label for select input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldSelect'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldSelect:')).toBeVisible();
      expect(screen.getByLabelText('FieldSelect:')).toBeEnabled();
      expect(screen.getByLabelText('FieldSelect:')).toHaveValue('option2');
    });

    it('should be possible to select the field by label for radio input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldRadio'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('Option 1')).toBeVisible();
      expect(screen.getByLabelText('Option 1')).toBeEnabled();
      expect(screen.getByLabelText('Option 1')).not.toBeChecked();

      expect(screen.getByLabelText('Option 2')).toBeVisible();
      expect(screen.getByLabelText('Option 2')).toBeEnabled();
      expect(screen.getByLabelText('Option 2')).toBeChecked();
    });

    it('should be possible to select the field by label for number input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldNumber'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldNumber:')).toBeVisible();
      expect(screen.getByLabelText('FieldNumber:')).toBeEnabled();
      expect(screen.getByLabelText('FieldNumber:')).toHaveValue(1234);
    });

    it('should be possible to select the field by label for datetime input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldDatetime'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldDatetime:')).toBeVisible();
      expect(screen.getByLabelText('FieldDatetime:')).toBeEnabled();
      expect(screen.getByLabelText('FieldDatetime:')).toHaveValue('');
    });

    it('should be possible to select the field by label for checkbox input', () => {
      render(
        <Fragment>
          {renderFields(context, ['fieldCheckbox'])}
        </Fragment>,
      );

      expect(screen.getByLabelText('FieldCheckbox')).toBeVisible();
      expect(screen.getByLabelText('FieldCheckbox')).toBeEnabled();
      expect(screen.getByLabelText('FieldCheckbox')).toBeChecked();
    });

    it('should render the radio button without a label', () => {
      context.props.fields.fieldRadio.label = undefined;
      context.state.fields.fieldRadio.label = undefined;
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldRadio'])}
        </Fragment>,
      );

      expect(container.querySelectorAll('label')).toHaveLength(2);
    });

    it('should render the radio button with a label', () => {
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldRadio'])}
        </Fragment>,
      );

      expect(container.querySelectorAll('label')).toHaveLength(3);
      expect(screen.getByText('FieldRadio:')).toBeVisible();
    });

    it('should not render custom type if "customComponent" is not defined', () => {
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldCustom'])}
        </Fragment>,
      );

      expect(container.querySelector('[data-testid="MyFieldCustom"')).toBeNull();
    });

    it('should not render custom type if "customComponentName" is not found', () => {
      context.props.fields.fieldCustom.customComponent = 'myCustomComponent';
      context.props.customFieldComponentMap = { otherCustomComponent: CustomComponent };
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldCustom'])}
        </Fragment>,
      );

      expect(container.querySelector('[data-testid="MyFieldCustom"')).toBeNull();
    });

    it('should render a custom type based on "customComponent" when field label is defined in template', () => {
      context.props.fields.fieldCustom.customComponent = 'myCustomComponent';
      context.props.customFieldComponentMap = { myCustomComponent: CustomComponent };
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldCustom'])}
        </Fragment>,
      );

      expect(screen.getByText('FieldCustom:')).toBeVisible();
      expect(screen.getByTestId('MyFieldCustom')).toBeVisible();
      expect(screen.getByText('fieldCustomValue and info from component!!!')).toBeVisible();
      expect(container.querySelectorAll('label')).toHaveLength(1);
    });

    it('should render a custom type based on "customComponent" when field label is not defined in template', () => {
      context.props.fields.fieldCustom.label = undefined;
      context.state.fields.fieldCustom.label = undefined;
      context.props.fields.fieldCustom.customComponent = 'myCustomComponent';
      context.props.customFieldComponentMap = { myCustomComponent: CustomComponent };
      const { container } = render(
        <Fragment>
          {renderFields(context, ['fieldCustom'])}
        </Fragment>,
      );

      expect(screen.getByTestId('MyFieldCustom')).toBeVisible();
      expect(screen.getByText('fieldCustomValue and info from component!!!')).toBeVisible();
      expect(container.querySelectorAll('label')).toHaveLength(0);
    });
  });
});
