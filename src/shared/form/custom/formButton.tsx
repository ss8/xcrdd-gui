import React, { FunctionComponent, ReactElement } from 'react';
import { Button, IconName } from '@blueprintjs/core';
import '../form.scss';

type Props={
    label:string,
    className?:string
    id:string | undefined
    onClick:any
    icon:IconName
    disabled:boolean
    name:string
}

const FormButton:FunctionComponent<Props> = ({
  label, className, onClick, icon,
}:Props):ReactElement => {
  return (
    <Button text={label} onClick={onClick} icon={icon} />
  );
};

export default FormButton;
