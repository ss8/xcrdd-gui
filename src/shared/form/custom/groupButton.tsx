import React, { Fragment } from 'react';
import {
  Button as button, InputGroup, Icon,
} from '@blueprintjs/core';
import '../form.scss';

const MyButton = (props: any) => {
  return (<button type="button" key={props.key} id={props.id} onClick={props.onClick}> {props.userID}</button>);
};

/* This component contains two selection boxes, available items and selected items.
Select one item from one box, the item will move to the other box. Vice verse. This
component has search function for available items and selected items.
input: need to pass data to this component as props
*/
class GroupButton extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      searchAvailableItems: '',
      searchSelectedItems: '',
      availableItems: [],
      selectedItems: [],
      availableItemflag: false,
      selectedItemflag: false,
    };
  }

  componentDidMount() {
    this.setState({ availableItems: this.props.data });
  }

  // handle search
  handleSearch = (e: any) => this.setState({ [e.target.name]: e.target.value });

  // handle select
  handleClickFromAvailableItem = (e: any) => {
    e.preventDefault();
    const curSelect = this.state.availableItems.filter((ele:any) => ele.id === e.target.id)[0];
    const curAvailableItems = this.state.availableItems.filter((ele:any) => ele.id !== e.target.id);
    this.setState({
      availableItems: curAvailableItems,
      selectedItems: [...this.state.selectedItems, curSelect],
      availableItemflag: true,
      selectedItemflag: false,
    });
  }

  // handle select
  handleClickFromSelectedItem = (e: any) => {
    e.preventDefault();

    const curSelect = this.state.selectedItems.filter((ele:any) => ele.id === e.target.id)[0];
    const curSelectedItems = this.state.selectedItems.filter((ele:any) => ele.id !== e.target.id);
    this.setState({
      availableItems: [...this.state.availableItems, curSelect],
      selectedItems: curSelectedItems,
      availableItemflag: false,
      selectedItemflag: true,
    });
  }

  render() {
    const displayAvailableItems = this.state.availableItems.filter((item: any) => {
      return (item.userID).toLowerCase().includes((this.state.searchAvailableItems.toLowerCase()));
    });
    const displaySelectedItem = this.state.selectedItems.filter((item:any) => {
      return (item.userID).toLowerCase().includes((this.state.searchSelectedItems.toLowerCase()));
    });
    return (
      <Fragment>
        <div className="groupButton-container">
          <div className="flex-item">
            <p>Available Items</p>
            <InputGroup
              placeholder="search"
              name="searchAvailableItems"
              onChange={this.handleSearch}
              type="text"
              value={this.state.searchAvailableItems}
              style={{ width: '180px', height: '30px' }}
            />
            <p />
            <div className="groupButton">
              {displayAvailableItems.map((item:any) => {
                return (
                  <MyButton
                    key={item.id}
                    {...item}
                    onClick={this.handleClickFromAvailableItem}
                  />
                );
              })}

            </div>
          </div>
          <div className="arrow-container flex-item">
            {this.state.availableItemflag && <div><Icon icon="arrow-right" iconSize={28} /></div>}
            {this.state.selectAvailableUsersflag && (<div><Icon icon="arrow-left" iconSize={28} /></div>) }

          </div>
          <div className="flex-item">
            <p>Selected Items</p>
            <InputGroup
              autoComplete="off"
              placeholder="search"
              name="searchSelectedItems"
              onChange={this.handleSearch}
              type="text"
              value={this.state.searchSelectedItems}
              style={{ width: '180px', height: '30px' }}
            />
            <p />
            <div className="groupButton">

              {displaySelectedItem.map((item:any) => {
                return (
                  <MyButton
                    key={item.id}
                    {...item}
                    onClick={this.handleClickFromSelectedItem}
                  />
                );
              })}

            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default GroupButton;
