import React, { Fragment } from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import TextSuggest, { TextSuggestType } from './TextSuggest';

const OPTIONS_MOCK: TextSuggestType[] = [
  {
    label: 'label 1',
    value: 'value 1',
  },
  {
    label: 'label 2',
    value: 'value 2',
  },
  {
    label: 'label 3',
    value: 'value 3',
  },
];

describe('<TextSuggest />', () => {

  describe('when TextSuggest items is empty', () => {
    const onItemSelectFn = jest.fn();
    const onQueryChangeFn = jest.fn();
    const onBlurFn = jest.fn();
    const onFocusFn = jest.fn();

    it('should not show options items', async () => {
      render(
        <TextSuggest
          name="name"
          dataTestName="dataTestName"
          disabled={false}
          fieldValue=""
          items={[]}
          onItemSelect={onItemSelectFn}
          onQueryChange={onQueryChangeFn}
          onBlur={onBlurFn}
          onFocus={onFocusFn}
        />,
      );
      const inputEl = await screen.findByDisplayValue('');
      fireEvent.focus(inputEl);
      expect(document.querySelectorAll('.bp3-menu-item.bp3-popover-dismiss').length).toBe(0);
    });
  });

  describe('when TextSuggest items has data', () => {
    const onItemSelectFn = jest.fn();
    const onQueryChangeFn = jest.fn();
    const onBlurFn = jest.fn();
    const onFocusFn = jest.fn();

    beforeEach(() => {
      render(
        <Fragment>
          <TextSuggest
            name="name"
            dataTestName="dataTestName"
            disabled={false}
            fieldValue=""
            items={OPTIONS_MOCK}
            onItemSelect={onItemSelectFn}
            onQueryChange={onQueryChangeFn}
            onBlur={onBlurFn}
            onFocus={onFocusFn}
          />
          <button type="button">Outside Click</button>
        </Fragment>,
      );
    });

    it('should show options when input has focus', async () => {
      const inputEl = await screen.findByDisplayValue('');
      fireEvent.focus(inputEl);
      await waitFor(() => expect(screen.queryByText(OPTIONS_MOCK[0].label)).toBeVisible());
      expect(screen.queryByText(OPTIONS_MOCK[1].label)).toBeVisible();
      expect(screen.queryByText(OPTIONS_MOCK[2].label)).toBeVisible();
    });

    it('should show options based on input value filtering', async () => {
      const inputEl = await screen.findByDisplayValue('');
      fireEvent.focus(inputEl);
      expect(document.querySelectorAll('.bp3-menu-item.bp3-popover-dismiss').length).toBe(3);
      fireEvent.change(inputEl, { target: { value: 'value 3' } });

      expect(document.querySelectorAll('.bp3-menu-item.bp3-popover-dismiss').length).toBe(1);
      expect(await screen.findByText(OPTIONS_MOCK[2].label)).toBeVisible();
    });

    it('should show empty message on popover when any item match the filter value', async () => {
      const inputEl = await screen.findByDisplayValue('');
      fireEvent.focus(inputEl);
      expect(document.querySelectorAll('.bp3-menu-item.bp3-popover-dismiss').length).toBe(3);
      fireEvent.change(inputEl, { target: { value: 'nothing' } });

      expect(await screen.findByText('No results.')).toBeVisible();
    });

    it('should change the input value by selecting a option', async () => {
      const inputEl = await screen.findByDisplayValue('');
      fireEvent.focus(inputEl);
      const selectedOption = await screen.findByText(OPTIONS_MOCK[1].label);
      fireEvent.click(selectedOption);
      expect(await screen.findByDisplayValue(OPTIONS_MOCK[1].value)).toBeVisible();
    });

    it('should change the input value by typing', async () => {
      const value = 'new value informed';
      const inputEl = await screen.findByDisplayValue('');

      fireEvent.change(inputEl, { target: { value } });

      expect(await screen.findByDisplayValue(value)).toBeVisible();
    });

  });
});
