import React, { FC, useEffect, useState } from 'react';
import { MenuItem } from '@blueprintjs/core';
import { IItemRendererProps, Suggest } from '@blueprintjs/select';

export type TextSuggestType = {
  label: string,
  value: string
}

type Props = {
  name: string,
  dataTestName: string,
  disabled: boolean,
  className?: string,
  items: TextSuggestType[],
  fieldValue?: string,
  placeholder?: string,
  onItemSelect: (item: TextSuggestType, event?: React.SyntheticEvent<HTMLElement>) => void;
  onQueryChange?: (query: string, event?: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur: () => void,
  onFocus: () => void,
}

const TextSuggestWrap = Suggest.ofType<TextSuggestType>();

const TextSuggest: FC<Props> = ({
  dataTestName,
  disabled,
  className,
  items,
  fieldValue,
  name,
  placeholder,
  onItemSelect,
  onQueryChange,
  onBlur,
  onFocus,
}: Props) => {

  const [currentValue, setCurrentValue] = useState(fieldValue);

  useEffect(() => {
    setCurrentValue(fieldValue);
  }, [fieldValue]);

  const itemPredicateFilter = (inputValue: string, item: TextSuggestType) => item.value.includes(inputValue);

  const renderInputValueRenderer = (item: TextSuggestType) => item.value;

  const handleItemSelect = (args: TextSuggestType, event?: React.SyntheticEvent<HTMLElement>) => {
    onItemSelect(args, event);
    setCurrentValue(args.value);
  };

  const handleQueryChange = (query: string, event?: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentValue(query);
    if (onQueryChange) {
      onQueryChange(query, event);
    }
  };

  const renderItemRenderer = (item: TextSuggestType, { handleClick, modifiers }: IItemRendererProps) => {
    if (!modifiers.matchesPredicate) return null;
    return (
      <MenuItem
        active={modifiers.active}
        key={item.value}
        text={item.label}
        onClick={handleClick}
      />
    );
  };

  return (
    <TextSuggestWrap
      resetOnClose={false}
      resetOnQuery={false}
      resetOnSelect={false}
      data-test={dataTestName}
      disabled={disabled}
      className={className}
      items={items || []}
      noResults={<MenuItem disabled text="No results." />}
      popoverProps={{ minimal: true }}
      query={currentValue}
      selectedItem={{
        value: currentValue || '',
        label: currentValue || '',
      }}
      inputValueRenderer={renderInputValueRenderer}
      itemRenderer={renderItemRenderer}
      itemPredicate={itemPredicateFilter}
      inputProps={{
        placeholder: placeholder || '',
        name,
        onBlur,
        onFocus,
      }}
      onQueryChange={handleQueryChange}
      onItemSelect={handleItemSelect}
    />
  );
};

export default TextSuggest;
