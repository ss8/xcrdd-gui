import React from 'react';
import {
  ItemPredicate, // eslint-disable-line
} from '@blueprintjs/select';
import { MenuItem } from '@blueprintjs/core';

export const defaultTypeaheadPredicate: ItemPredicate<Typeahead> = (query, item) => `${item.label.toLowerCase()} ${item.value}`.indexOf(query.toLowerCase()) >= 0;

export const defaultTypeaheadRenderer = (item: any, { handleClick, modifiers }: any) => {
  if (!modifiers.matchesPredicate) return null;
  return (
    <MenuItem
      active={modifiers.active}
      key={item.value}
      text={item.label}
      onClick={handleClick}
    />
  );
};

export interface Typeahead {
  label: string;
  value: string;
}
