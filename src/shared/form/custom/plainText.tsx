import React, { FunctionComponent, ReactElement } from 'react';
import '../form.scss';

type Props={
    value:any,
    className?:string
    id:string | undefined
    tabIndex?:number|undefined
}

const PlainText:FunctionComponent<Props> = ({ value, className }:Props):ReactElement => {
  return (
    <div className="plainText">
      <span>{value}</span>
    </div>
  );
};

export default PlainText;
