import { FormTemplateValidator } from 'data/template/template.types';
import ipRegex from 'ip-regex';

const INVALID_VALUES = [
  '0.0.0.0',
  '255.255.255.255',
];

/**
 * Sample with invalidIPs.
 * invalidIPs is optional, it uses INVALID_VALUES if it isn't defined.
 * validators: [
 *  {
 *    type: "IPAddress",
 *    invalidIPs: ["255.255.255.255"],
 *    errorMessage: "Must be a valid IP Address. Please re-enter."
 *  }
 * ]
 */
export default class IPAddressValidator {
  private static TYPE = 'IPAddress';

  public static isType(type: string): boolean {
    return IPAddressValidator.TYPE === type;
  }

  public static isValid(
    _propFields: unknown,
    _stateFields: unknown,
    value: string,
    _lastSavedValue: unknown,
    validator: FormTemplateValidator | null,
    _form: unknown,
  ): boolean {

    let invalidIPs = INVALID_VALUES;
    if (validator?.invalidIPs != null) {
      invalidIPs = validator.invalidIPs;
    }

    if (invalidIPs.includes(value)) {
      return false;
    }

    return ipRegex({ exact: true }).test(value);
  }
}
