export enum OPERATOR {
  type = 'Operator',
  GREATER_THAN = '>',
  GREATER_THAN_EQUAL_TO = '>=',
  LESS_THAN = '<',
  LESS_THAN_EQUAL_TO = '<=',
  EQUAL = '='
}

export default class OperatorValidator {
  private static TYPE:string = 'Operator' as const;

  public static isType = (aType:string): boolean => {
    return OperatorValidator.TYPE === aType;
  }

  public static isValid = (propFields:any, stateFields:any, value:any,
    _lastSavedValue:any, validator:any, _form:any) => {
    const { operator, fieldName } = validator;
    let operand1 = -1;
    let operand2 = -1;
    if (fieldName === 'now') {
      operand1 = new Date(value).getTime();
      operand2 = new Date().getTime();
    } else if (fieldName === '$value') {
      operand1 = +value;
      operand2 = +validator.value;
    } else {
      const { type } = propFields[fieldName];
      if (type === 'datetime') {
        operand1 = new Date(value).getTime();
        operand2 = new Date(stateFields[fieldName].value).getTime();
      } else {
        operand1 = value;
        operand2 = stateFields[fieldName].value;
      }
    }

    switch (operator) {
      case OPERATOR.EQUAL: return (operand1 === operand2);
      case OPERATOR.GREATER_THAN: return (operand1 > operand2);
      case OPERATOR.GREATER_THAN_EQUAL_TO: return (operand1 >= operand2);
      case OPERATOR.LESS_THAN: return (operand1 < operand2);
      case OPERATOR.LESS_THAN_EQUAL_TO: return (operand1 <= operand2);
      default: return true;
    }
  };
}
