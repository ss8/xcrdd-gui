import EmailValidator from './EmailValidator';

describe('EmailValidator', () => {
  describe('isType()', () => {
    it('should return true if type is EmailAddress', () => {
      expect(EmailValidator.isType('EMAIL')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(EmailValidator.isType('something else')).toEqual(false);
    });
  });

  describe('isValid()', () => {
    it('should return true when value is a valid Email address', () => {
      const validEmailAddresses = [
        'email@ss8.com',
        'firstname.lastname@ss8.com',
        'email@subdomain.ss8.com',
        'email@123.123.123.123',
        '1234567890@ss8.com',
        'email@ss8-one.com',
        '_______@ss8.com',
        'email@ss8.name',
        'email@ss8.co.jp',
        'firstname-lastname@ss8.com',
      ];
      validEmailAddresses.forEach((emailAddress) => {
        const retValue = {
          emailAddress,
          isValid: EmailValidator.isValid(null, null, emailAddress, null, null, null),
        };
        expect(retValue).toEqual({
          emailAddress,
          isValid: true,
        });
      });
    });

    it('should return false when value is a invalid Email address', () => {
      const invalidEmailAddresses = [
        'plainaddress',
        '#@%^%#$@#$@#.com',
        '@ss8.com',
        'First Last <email@ss8.com>',
        'email.ss8.com',
        'email@ss8@ss8.com',
        '.email@ss8.com',
        'email@ss8.com (Joe Smith)',
        'email@ss8',
        'email@ss8..com',
      ];
      invalidEmailAddresses.forEach((invalidEmailAddress) => {
        const retValue = {
          invalidEmailAddress,
          isValid: EmailValidator.isValid(null, null, invalidEmailAddress, null, null, null),
        };
        expect(retValue).toEqual({
          invalidEmailAddress,
          isValid: false,
        });
      });
    });
  });
});
