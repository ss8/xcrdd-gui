import { FormTemplateValidator } from 'data/template/template.types';

export default class IntervalValidator {
  private static TYPE: string = 'Interval' as const;

  public static isType = (aType: string): boolean => {
    return IntervalValidator.TYPE === aType;
  }

  public static isValid = (
    _propFields: unknown,
    _stateFields: unknown,
    value: unknown,
    _lastSavedValue: unknown,
    validator: FormTemplateValidator,
    _form: unknown,
  ): boolean => {
    const { intervals = [] } = validator;

    let fieldValue: number;
    if (typeof value === 'string') {
      fieldValue = +value;
    }

    return intervals.some(([lower, upper]) => fieldValue >= lower && fieldValue <= upper);
  }
}
