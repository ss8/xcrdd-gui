/**
 * This code is copied from XMS, com\ss8\xms\gwt\shared\validator\validators\URLFieldValidator.java
 * XMS has been using the code to verify the URI and URL entries. I did not see any bug/ticket on
 * the URI/URL field in XMS.  Therefore I decided to use the XMS code.
 */
export default class URLValidator {
  private static TYPE = 'URL';

  private static scheme = '((http|https|ftp|gopher|news|telnet|[a-zA-Z]+):(//){0,1})?';
  // authority IPv4
  private static shareIp = '((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))';
  // authority domain
  private static shareDomain = '(([A-Za-z0-9_][_A-Za-z0-9-@]*(\\.[A-Za-z_][_A-Za-z0-9-]+){0,10})+)';
  // authority port
  private static sharePort = '((:\\d{1,5})?)';
  // path
  private static sharePath = '(((/([_A-Za-z0-9%~-][_A-Za-z0-9%~&-:@\\.]*)?)+(\\.)?)(\\.[A-Za-z0-9]*)?)';// like   /opt/jboss5/xyz/server.log
  // query
  private static paramName = '[_A-Za-z0-9%&-]{0,50}';
  private static paramValue = '.{0,100}';
  private static shareQuery = `((\\?${URLValidator.paramName}(=${URLValidator.paramValue})?){1}(&${URLValidator.paramName}(=${URLValidator.paramValue})?){0,50})`;
  // fragment
  private static shareFragment = '(#[_A-Za-z0-9-!%@#;\\$]+)';

  private static pathQueryFragment = `((${URLValidator.sharePath})?(${URLValidator.shareQuery})?(${URLValidator.shareFragment})?)`;

  private static urlRegExp = `^${URLValidator.scheme}(${URLValidator.shareIp}|${URLValidator.shareDomain})${URLValidator.sharePort}${URLValidator.pathQueryFragment}$`;
  private static uriRegExp = '^(([_A-Za-z0-9%][_A-Za-z0-9%-:]*))*(/[_A-Za-z0-9%-]+)*(@([_A-Za-z0-9%][_A-Za-z0-9%-]*(\\.([_A-Za-z%~][_A-Za-z0-9%~-]+))*))?(\\?[_A-Za-z0-9-]+(#[_A-Za-z0-9-]+)?)?$';

  public static getUrlRegExp(): string {
    return URLValidator.urlRegExp;
  }

  public static isType(type: string): boolean {
    return URLValidator.TYPE === type;
  }

  public static isValid(
    _propFields: unknown,
    _stateFields: unknown,
    value: string,
    _lastSavedValue: unknown,
    _validator: unknown,
    _form: unknown,
  ): boolean {

    return (value.match(URLValidator.urlRegExp) !== null);
  }
}
