import { FormTemplateValidator } from 'data/template/template.types';
import TriggerValidationOnAnotherFieldValidator, { FormCallable } from './triggerValidationOnAnotherFieldValidator';

describe('TriggerValidationOnAnotherFieldValidator', () => {
  describe('isType()', () => {
    it('should return true if type is triggerValidationOnAnotherField', () => {
      expect(TriggerValidationOnAnotherFieldValidator.isType('triggerValidationOnAnotherField')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(TriggerValidationOnAnotherFieldValidator.isType('something else')).toEqual(false);
    });
  });

  describe('isValid()', () => {
    let propFields: any;
    let stateFields: any;
    let validator: FormTemplateValidator;
    let form: FormCallable;

    beforeEach(() => {
      propFields = {
        field1: {
          type: 'text',
        },
      };

      stateFields = {
        field1: {
          value: 'value 1',
          label: 'label 1',
        },
      };

      validator = {
        theOtherField: 'field1',
        type: 'triggerValidationOnAnotherField',
      };

      form = {
        handleChange: jest.fn(),
        handleDateTimeChange: jest.fn(),
        handleTextSuggestChange: jest.fn(),
      };
    });

    it('should return true if different validator type', () => {
      validator.type = 'otherType';
      expect(TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form)).toEqual(true);
    });

    it('should always return true', () => {
      expect(TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form)).toEqual(true);
    });

    it('should call the handleChange function with the expected parameters for text type', () => {
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'text',
          value: 'value 1',
        },
      });
    });

    it('should call the handleChange function with the expected parameters for password type', () => {
      propFields.field1.type = 'password';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'password',
          value: 'value 1',
        },
      });
    });

    it('should call the handleChange function with the expected parameters for textarea type', () => {
      propFields.field1.type = 'textarea';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'textarea',
          value: 'value 1',
        },
      });
    });

    it('should call the handleChange function with the expected parameters for select type', () => {
      propFields.field1.type = 'select';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'select',
          value: 'value 1',
        },
      });
    });

    it('should call the handleChange function with the expected parameters for number type', () => {
      propFields.field1.type = 'number';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'number',
          value: 'value 1',
        },
      });
    });

    it('should call the handleChange function with the expected parameters for checkbox type', () => {
      propFields.field1.type = 'checkbox';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).toBeCalledWith({
        currentTarget: {
          name: 'field1',
          type: 'checkbox',
          value: 'value 1',
        },
      });
    });

    it('should call the handleTextSuggestChange function with the expected parameters for textSuggest type', () => {
      propFields.field1.type = 'textSuggest';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleTextSuggestChange).toBeCalledWith({ name: 'field1' }, { label: 'label 1', value: 'value 1' });
    });

    it('should call the handleDateTimeChange function with the expected parameters for datetime type', () => {
      propFields.field1.type = 'datetime';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleDateTimeChange).toBeCalledWith({ name: 'field1' }, 'value 1');
    });

    it('should not call any function otherwise', () => {
      propFields.field1.type = 'otherType';
      TriggerValidationOnAnotherFieldValidator.isValid(propFields, stateFields, '', '', validator, form);
      expect(form.handleChange).not.toBeCalled();
      expect(form.handleDateTimeChange).not.toBeCalled();
    });
  });
});
