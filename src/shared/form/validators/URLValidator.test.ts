import URLValidator from './URLValidator';

describe('URLValidator', () => {
  describe('isType()', () => {
    it('should return true if type is URL', () => {
      expect(URLValidator.isType('URL')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(URLValidator.isType('something else')).toEqual(false);
    });
  });

  describe('getUrlRegExp()', () => {
    it('should return correct reg exp for URL validation', () => {
      const expected = '^((http|https|ftp|gopher|news|telnet|[a-zA-Z]+):(//){0,1})?(((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(([A-Za-z0-9_][_A-Za-z0-9-@]*(\\.[A-Za-z_][_A-Za-z0-9-]+){0,10})+))((:\\d{1,5})?)(((((/([_A-Za-z0-9%~-][_A-Za-z0-9%~&-:@\\.]*)?)+(\\.)?)(\\.[A-Za-z0-9]*)?))?(((\\?[_A-Za-z0-9%&-]{0,50}(=.{0,100})?){1}(&[_A-Za-z0-9%&-]{0,50}(=.{0,100})?){0,50}))?((#[_A-Za-z0-9-!%@#;\\$]+))?)$';
      expect(URLValidator.getUrlRegExp()).toEqual(expected);
    });
  });

  describe('isValid()', () => {
    it('should return true for a valid HTTPS URLhttps://www.mavenir.net:443/li/', () => {
      expect(URLValidator.isValid(null, null, 'https://www.mavenir.net:443/li/', null, null, null)).toEqual(true);
    });
    it('should return true for a valid HTTP URL http://www.mavenir.net:443/li/', () => {
      expect(URLValidator.isValid(null, null, 'http://www.mavenir.net:443/li/', null, null, null)).toEqual(true);
    });
    it('should return true for a valid URL http://49.45.36.32:443/li/', () => {
      expect(URLValidator.isValid(null, null, 'http://49.45.36.32:443/li/', null, null, null)).toEqual(true);
    });
    it('should return true for a valid URL www.mavenir.net:443/li/', () => {
      expect(URLValidator.isValid(null, null, 'www.mavenir.net:443/li/', null, null, null)).toEqual(true);
    });
    it('should return false for a invalid URL www.mavenir.net.23.34.34.34:443/li/', () => {
      expect(URLValidator.isValid(null, null, 'www.mavenir.net.23.34.34.34:443/li/', null, null, null)).toEqual(false);
    });
    it('should return false for a invalid URL www.mavenir.net:123456/li/', () => {
      expect(URLValidator.isValid(null, null, 'www.mavenir.net:123456/li/', null, null, null)).toEqual(false);
    });
    it('should return true for a valid URL https://1.1.12.3:443/cgi-bin/CSCF/CSCF.py', () => {
      expect(URLValidator.isValid(null, null, 'https://1.1.12.3:443/cgi-bin/CSCF/CSCF.py', null, null, null)).toEqual(true);
    });
    it('should return true for a valid URL sip:conference@vzwfmc.com', () => {
      expect(URLValidator.isValid(null, null, 'sip:conference@vzwfmc.com', null, null, null)).toEqual(true);
    });
    it('should return true for a valid URL telnet://192.0.2.16:80/', () => {
      expect(URLValidator.isValid(null, null, 'telnet://192.0.2.16:80/', null, null, null)).toEqual(true);
    });
  });
});
