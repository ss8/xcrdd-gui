import { FormTemplateValidator } from 'data/template/template.types';
import IPAddressValidator from './IPAddressValidator';

describe('IPAddressValidator', () => {
  describe('isType()', () => {
    it('should return true if type is IPAddress', () => {
      expect(IPAddressValidator.isType('IPAddress')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(IPAddressValidator.isType('something else')).toEqual(false);
    });
  });

  describe('isValid()', () => {
    it('should return true for a valid IPv4', () => {
      expect(IPAddressValidator.isValid(null, null, '1.1.1.1', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '12.12.12.12', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '254.254.254.254', null, null, null)).toEqual(true);

      const validator: FormTemplateValidator = {
        type: 'IPAddress',
        invalidIPs: ['255.255.255.255'],
      };
      expect(IPAddressValidator.isValid(null, null, '0.0.0.0', null, validator, null)).toEqual(true);
    });

    it('should return true for a valid IPv6', () => {
      expect(IPAddressValidator.isValid(null, null, '::', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '::1', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '2001:db8::', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '::1234:5678', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '1:1:1:1:1:1:1:1', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '2001:0db8:0000:0000:0000:ff00:0042:8329', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '2001:0DB8:0000:0000:0000:FF00:0042:8329', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '2001:db8:0:0:0:ff00:42:8329', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, '2001:db8::ff00:42:8329', null, null, null)).toEqual(true);
      expect(IPAddressValidator.isValid(null, null, 'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff', null, null, null)).toEqual(true);
    });

    it('should return false to an invalid IPv4', () => {
      expect(IPAddressValidator.isValid(null, null, '0.0.0.0', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '255.255.255.255', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '1.1.1', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, 'text 12.12.12.12', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, 'a.a.a.a', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '192.168.0.2000000000', null, null, null)).toEqual(false);

      const validator = {
        invalidIPs: ['255.255.255.255'],
      };
      expect(IPAddressValidator.isValid(null, null, '255.255.255.255', validator, null, null)).toEqual(false);
    });

    it('should return false for an invalid IPv6', () => {
      expect(IPAddressValidator.isValid(null, null, ':', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '\'::1', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '2001:db8:', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, ':1234:5678', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, '1:1:1:1:1:1:1', null, null, null)).toEqual(false);
      expect(IPAddressValidator.isValid(null, null, 'ffff:ffff:ffff:ffff:ffff:ffff:ffff:fffg', null, null, null)).toEqual(false);
    });
  });
});
