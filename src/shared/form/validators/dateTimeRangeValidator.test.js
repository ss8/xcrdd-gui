import DateTimeRangeValidator from './dateTimeRangeValidator';
import * as dateTimeRangeValidationTestData from './__test__/dateTimeRangeValidator-test-data';

const getDateTimeFromToday = (value) => {
  const retDate = new Date();
  retDate.setDate(retDate.getDate() + value);
  return retDate;
};

describe('A stopDateTime field does not have datetime range validator', () => {
  test('A1 DateTimeRangeValidator.isValid return true when stopDateTime field does not have any validator', () => {
    const endDate = getDateTimeFromToday(15);

    let returnValue = DateTimeRangeValidator.isValid(null, {}, endDate, null,
      null, null);
    expect(returnValue).toBeTruthy();

    returnValue = DateTimeRangeValidator.isValid(null, {}, endDate, null, {});
    expect(returnValue).toBeTruthy();
  });

  test('A2 DateTimeRangeValidator.isValid return true when stopDateTime field does not have range validator', () => {
    const endDate = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, {}, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithNonRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });
});

describe('B stopDateTime field has datetime range validator', () => {
  const defaultStateFields = {
    timeZone: { value: 'America/Los_Angeles' },
  };

  test('B1. DateTimeRangeValidator.isValid returns true when stopDateTime is WITHIN the specified day range from receivedDateTime', () => {
    const receivedDateTimeValue = getDateTimeFromToday(5);
    const endDate = getDateTimeFromToday(5 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnOneFieldRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('B2. DateTimeRangeValidator.isValid returns true when stopDateTime is WITHIN the specified day range from receivedDateTime and receivedDateTime is empty', () => {
    const endDate = getDateTimeFromToday(15);

    const stateFields = { ...defaultStateFields, receivedDateTime: { value: null } };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnOneFieldRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('B3. DateTimeRangeValidator.isValid returns false when stopDateTime is BEFORE the specified day range from receivedDateTime', () => {
    const receivedDateTimeValue = getDateTimeFromToday(8);
    const endDate = getDateTimeFromToday(8 + 3);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnOneFieldRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('B4. DateTimeRangeValidator.isValid returns false when stopDateTime is AFTER the specified day range from receivedDateTime', () => {
    const receivedDateTimeValue = getDateTimeFromToday(8);
    const endDate = getDateTimeFromToday(8 + 63);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnOneFieldRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('B5. DateTimeRangeValidator.isValid returns true when validation only applies to create scenario and it is an edit situation', () => {
    const receivedDateTimeValue = getDateTimeFromToday(8);
    const endDate = getDateTimeFromToday(8 + 62);
    const lastSavedValue = getDateTimeFromToday(8 + 100);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
    };

    // there is a lastSavedValue. So this is an edit
    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnOneFieldRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('B6. DateTimeRangeValidator.isValid returns false when stopDateTime is before the specified day range from today', () => {
    const endDate = getDateTimeFromToday(3);

    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithRangeBasedOnNowValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('B7. DateTimeRangeValidator.isValid returns false when stopDateTime is after the specified day range from today', () => {
    const endDate = getDateTimeFromToday(63);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithRangeBasedOnNowValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('B8. DateTimeRangeValidator.isValid returns true when stopDateTime WITHIN the specified day range from today', () => {
    const endDate = getDateTimeFromToday(20);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithRangeBasedOnNowValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('B9. DateTimeRangeValidator.isValid proceed with validation and ignore lastSavedValue when validateOn is not specified in the validator', () => {
    let endDate = getDateTimeFromToday(63);
    let lastSavedValue = getDateTimeFromToday(60);
    let returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithRangeBasedOnNowValidator, null);
    expect(returnValue).toBeFalsy();

    endDate = getDateTimeFromToday(20);
    lastSavedValue = getDateTimeFromToday(19);
    returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithRangeBasedOnNowValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('B10. DateTimeRangeValidator.isValid returns true when stopDateTime WITHIN the specified day range from last saved value', () => {
    const endDate = getDateTimeFromToday(15 + 14 + 3);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeTruthy();
  });

  test('B11. DateTimeRangeValidator.isValid returns true when stopDateTime BEFORE the specified day range from last saved value', () => {
    const endDate = getDateTimeFromToday(10);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeFalsy();
  });

  test('B12. DateTimeRangeValidator.isValid returns true when stopDateTime AFTER the specified day range from last saved value', () => {
    const endDate = getDateTimeFromToday(15 + 70);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeFalsy();
  });

  test('B13. DateTimeRangeValidator.isValid returns true when there is no lastSavedValue for dateRangeBasedOnLastSavedValue validation', () => {
    const endDate = getDateTimeFromToday(15 + 70);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeTruthy();
  });

  test('B14. DateTimeRangeValidator.isValid returns true when stopDateTime is a string and WITHIN the specified day range from last saved value', () => {
    let endDate = JSON.stringify(getDateTimeFromToday(15 + 14 + 3));
    // get rid of the double quotes
    endDate = endDate.substring(1, endDate.length - 1);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeTruthy();
  });

  test('B15. DateTimeRangeValidator.isValid returns true when stopDateTime is a string and BEFORE the specified day range from last saved value', () => {
    let endDate = JSON.stringify(getDateTimeFromToday(10));
    // get rid of the double quotes
    endDate = endDate.substring(1, endDate.length - 1);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeFalsy();
  });

  test('B16. DateTimeRangeValidator.isValid returns true when stopDateTime is a string and AFTER the specified day range from last saved value', () => {
    let endDate = JSON.stringify(getDateTimeFromToday(15 + 70));
    // get rid of the double quotes
    endDate = endDate.substring(1, endDate.length - 1);
    const lastSavedValue = getDateTimeFromToday(15);
    const returnValue = DateTimeRangeValidator.isValid(null, defaultStateFields, endDate,
      lastSavedValue,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator,
      null);
    expect(returnValue).toBeFalsy();
  });
});

describe('C test dateRangeBasedOnOtherFields when there are multiple other fields', () => {
  const defaultStateFields = {
    timeZone: { value: 'America/Los_Angeles' },
  };

  test('C1. return true when stopDateTime is WITHIN the specified day range from first field', () => {
    const receivedDateTimeValue = getDateTimeFromToday(5);
    const startDateTimeValue = getDateTimeFromToday(1000);
    const endDate = getDateTimeFromToday(5 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('C2. return true when stopDateTime is WITHIN the specified day range from second field because first field is empty', () => {
    const receivedDateTimeValue = null;
    const startDateTimeValue = getDateTimeFromToday(5);
    const endDate = getDateTimeFromToday(5 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('C3. return true when stopDateTime is WITHIN the specified day range from now because first and second fields are empty', () => {
    const receivedDateTimeValue = null;
    const startDateTimeValue = null;
    const endDate = getDateTimeFromToday(0 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeTruthy();
  });

  test('C4. return false when stopDateTime is BEFORE the specified day range from first field', () => {
    const startDateTimeValue = getDateTimeFromToday(20);
    const receivedDateTimeValue = getDateTimeFromToday(20 + 90);
    const endDate = getDateTimeFromToday(20 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('C5. return false when stopDateTime is BEFORE the specified day range from second field because first field is empty', () => {
    const startDateTimeValue = getDateTimeFromToday(20);
    const receivedDateTimeValue = null;
    const endDate = getDateTimeFromToday(15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('C6. return false when stopDateTime is BEFORE the specified day range from today because first field and second field are empty', () => {
    const startDateTimeValue = null;
    const receivedDateTimeValue = null;
    const endDate = getDateTimeFromToday(5);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('C7. return false when stopDateTime is AFTER the specified day range from first field', () => {
    const startDateTimeValue = getDateTimeFromToday(20 + 90);
    const receivedDateTimeValue = getDateTimeFromToday(20);
    const endDate = getDateTimeFromToday(20 + 90 + 15);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('C8. return false when stopDateTime is AFTER the specified day range from second field because first field is empty', () => {
    const startDateTimeValue = getDateTimeFromToday(20);
    const receivedDateTimeValue = null;
    const endDate = getDateTimeFromToday(20 + 90);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });

  test('C9. return false when stopDateTime is AFTER the specified day range from today because first field and second field are empty', () => {
    const startDateTimeValue = null;
    const receivedDateTimeValue = null;
    const endDate = getDateTimeFromToday(90);

    const stateFields = {
      ...defaultStateFields,
      receivedDateTime: { value: receivedDateTimeValue },
      startDateTime: { value: startDateTimeValue },
    };

    const returnValue = DateTimeRangeValidator.isValid(null, stateFields, endDate, null,
      dateTimeRangeValidationTestData.stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator, null);
    expect(returnValue).toBeFalsy();
  });
});
