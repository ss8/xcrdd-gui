import { FormTemplateValidator } from 'data/template/template.types';
import IntervalValidator from './intervalValidator';

describe('IntervalValidator', () => {
  describe('isType()', () => {
    it('should return true if type is Interval', () => {
      expect(IntervalValidator.isType('Interval')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(IntervalValidator.isType('something else')).toEqual(false);
    });
  });

  describe('isValid()', () => {
    let validator: FormTemplateValidator;

    beforeEach(() => {
      validator = {
        type: 'Interval',
        intervals: [[0, 10]],
      };
    });

    it('should return true if value is empty', () => {
      expect(IntervalValidator.isValid(null, null, '', null, validator, null)).toEqual(true);
    });

    it('should return true if value is in the valid interval', () => {
      expect(IntervalValidator.isValid(null, null, '1', null, validator, null)).toEqual(true);
    });

    it('should return true if value is in the lower limit', () => {
      expect(IntervalValidator.isValid(null, null, '0', null, validator, null)).toEqual(true);
    });

    it('should return true if value is in the upper limit', () => {
      expect(IntervalValidator.isValid(null, null, '10', null, validator, null)).toEqual(true);
    });

    it('should return false if value is up the upper limit', () => {
      expect(IntervalValidator.isValid(null, null, '11', null, validator, null)).toEqual(false);
    });

    it('should return false if value is low the lower limit', () => {
      expect(IntervalValidator.isValid(null, null, '-1', null, validator, null)).toEqual(false);
    });

    it('should return true if no interval is defined', () => {
      validator.intervals = [];
      expect(IntervalValidator.isValid(null, null, '-1', null, validator, null)).toEqual(false);
    });

    it('should return true if value is in one element interval', () => {
      validator.intervals = [[0, 0], [10, 20]];
      expect(IntervalValidator.isValid(null, null, '0', null, validator, null)).toEqual(true);
    });

    it('should return true if value is in second interval', () => {
      validator.intervals = [[0, 0], [10, 20]];
      expect(IntervalValidator.isValid(null, null, '15', null, validator, null)).toEqual(true);
    });

    it('should return false if value is in middle of intervals', () => {
      validator.intervals = [[0, 0], [10, 20]];
      expect(IntervalValidator.isValid(null, null, '5', null, validator, null)).toEqual(false);
    });
  });
});
