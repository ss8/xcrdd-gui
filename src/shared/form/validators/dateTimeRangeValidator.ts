import { addToDate } from '../../constants/checkTimeValidation';

/*
 * Example 1: date range validation based on the date in another field
 *   validators: [ {
 *      type: 'dateRangeBasedOnAnotherField',
 *      basedOnFieldName: 'receivedDateTime', // Required. calculate the valid start and end dates
 *                                           // using the value in receivedDateTime field
 *      defaultBasedOnDate: 'now',  //Optional. Default is valid if basedOnFieldName is blank.
 *                                  // in this example, if receivedDateTime is blank,
 *                                 // the valid start and end dates are calculated using now.
 *      validateOn: 'Create',  //Optional. Default is all scenarios.
 *                           // in this example, this validation is only applicable in
 *                           // creation scenaio
 *      startRange : 14,  // Required. in this example, the start date range is 14 days from
 *                        // the based-on-date
 *      startRangeUnit: "day", // Required. in this example, the start date range, 14, is in days.
 *      endRange : 6, // Required. in this example, the end date range is 6 months from
 *                    // the based-on-date.
 *      endRangeUntil: "month",  // Required. in this example, the end date range, 6, is in months.
 *      errorMessage: "Stop date must be within 2 weeks to 6 months from the start date."
 *                    // Required. message to show if field value fails this test.
 *   }]
 *
 * Example 2: date range validation based on last saved value
 *   validators: [ {
 *      type: 'dateRangeBasedOnLastSavedValue',
 *      validateOn: 'Edit',  //Optional. Default is all scenarios.
 *                           // in this example, this validation is only applicable in
 *                           // edit scenaio
 *      startRange : 14,  // Required. in this example, the start date range is 14 days from
 *                        // the based-on-date
 *      startRangeUnit: "day", // Required. in this example, the start date range, 14, is in days.
 *      endRange : 6, // Required. in this example, the end date range is 6 months from
 *                    // the based-on-date.
 *      endRangeUntil: "month",  // Required. in this example, the end date range, 6, is in months.
 *      errorMessage: "Stop date must be within 2 weeks to 6 months from the start date."
 *                    // Required. message to show if field value fails this test.
 *   }]
 *
 * Example 3: date range validation based on now
 *   validators: [ {
 *      type: 'dateRangeBasedOnNow',
 *      startRange : 14,  // Required. in this example, the start date range is 14 days from
 *                        // the based-on-date
 *      startRangeUnit: "day", // Required. in this example, the start date range, 14, is in days.
 *      endRange : 6, // Required. in this example, the end date range is 6 months from
 *                    // the based-on-date.
 *      endRangeUntil: "month",  // Required. in this example, the end date range, 6, is in months.
 *      errorMessage: "Stop date must be within 2 weeks to 6 months from the start date."
 *                    // Required. message to show if field value fails this test.
 *   }]
 */
export default class DateTimeRangeValidator {
    public static DATE_RANGE_BASED_ON_OTHER_FIELDS_TYPE:string = 'dateRangeBasedOnOtherFields' as const;

    public static DATE_RANGE_BASED_ON_LAST_SAVED_VALUE_TYPE:string = 'dateRangeBasedOnLastSavedValue' as const;

    public static DATE_RANGE_BASED_ON_NOW_TYPE:string = 'dateRangeBasedOnNow' as const;

    public static isType = (aType:string): boolean => {
      return DateTimeRangeValidator.DATE_RANGE_BASED_ON_OTHER_FIELDS_TYPE === aType ||
          DateTimeRangeValidator.DATE_RANGE_BASED_ON_NOW_TYPE === aType ||
          DateTimeRangeValidator.DATE_RANGE_BASED_ON_LAST_SAVED_VALUE_TYPE === aType;
    }

    public static isValid = (_propFields:any, stateFields:any, aValue:Date | string,
      originalValue: Date | string, validator:any, _form:any) : boolean => {
      // this validation is meant to validate non-null value
      if (!aValue) return true;

      // Ensure the validator matches this validation class
      if (!validator || !validator.type) return true;
      if (DateTimeRangeValidator.isType(validator.type) === false) return true;

      // If validation should only be done in creation scenario, no need to validate if
      // there is originalValue.
      if (validator.validateOn && validator.validateOn === 'Create' &&
        originalValue) {
        return true;
      }
      // if validation should only be done in edit scenario, no need to validate if there isn't
      // originalValue
      if (validator.validateOn && validator.validateOn === 'Edit' &&
        !originalValue) {
        return true;
      }

      // this validation is meant to validate non-null value
      let value : Date | string | null = aValue;
      if (typeof aValue === 'string') {
        value = new Date(aValue);
      }
      if (value === null) return true;

      // convert last saved value to Date
      if (typeof originalValue === 'string') {
        originalValue = new Date(originalValue);
      }

      // if value did not change from the last saved value, no need to validate
      if (originalValue && value instanceof Date &&
        originalValue.getTime() === value.getTime()) {
        return true;
      }

      const basedOnThisDateTime = DateTimeRangeValidator.getBasedOnDateTime(
        validator, stateFields, originalValue,
      );

      if (basedOnThisDateTime === null) {
        return true;
      }

      const {
        startRange, startRangeUnit, endRange, endRangeUnit,
      } = validator;

      const startDateRange: Date = addToDate(basedOnThisDateTime, startRange,
        startRangeUnit);
      const endDateRange: Date = addToDate(basedOnThisDateTime, endRange, endRangeUnit);

      // console.log(`***isEndDateRangeValid ${validator.type}
      // basedOnThisDateTime=${basedOnThisDateTime}
      //    [${startDateRange} < ${value} < ${endDateRange}]
      //    [originalValue=${originalValue}]
      //    [startRange=${startRange}, startRangeUnit=${startRangeUnit},
      // endRange=${endRange}, endRangeUnit=${endRangeUnit}]`);
      if (value instanceof Date && startDateRange.getTime() <= value.getTime() &&
        value.getTime() <= endDateRange.getTime()) {
        return true;
      }

      return false;
    }

    private static getBasedOnDateTime = (validator:any, stateFields:any,
      originalValue: Date) : Date | null => {
      let basedOnThisDate: Date | null = null;

      // Get the date to based the range on
      switch (validator.type) {
        case DateTimeRangeValidator.DATE_RANGE_BASED_ON_OTHER_FIELDS_TYPE:
          if (validator.basedOnFieldNames && validator.basedOnFieldNames.length > 0) {
            validator.basedOnFieldNames.some((fieldName:string) => {
              if (stateFields[fieldName] && stateFields[fieldName].value) {
                basedOnThisDate = stateFields[fieldName].value;
                return true;
              }
              return false;
            });
          }
          break;

        case DateTimeRangeValidator.DATE_RANGE_BASED_ON_LAST_SAVED_VALUE_TYPE:
          if (originalValue) {
            basedOnThisDate = originalValue;
          }
          break;

        case DateTimeRangeValidator.DATE_RANGE_BASED_ON_NOW_TYPE:
        default:
          basedOnThisDate = new Date();
          break;
      }

      if (basedOnThisDate === null && validator.defaultBasedOnDate === 'now') {
        basedOnThisDate = new Date();
      }

      return basedOnThisDate;
    }
}
