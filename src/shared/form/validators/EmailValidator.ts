/**
 * This code is copied from XMS, com\ss8\xms\gwt\shared\validator\validators\EmailFieldValidator.java
 * XMS has been using the code to verify the EMAIL and EMAIL entries.
 */
export default class EmailValidator {
  private static TYPE = 'EMAIL';

  private static pattern = '^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*(\\.\\"[\\w/#%&<>\\\\\\*\\(\\)\\.\\?]+\\")*(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)+$';

  public static getEmailRegExp() {
    return EmailValidator.pattern;
  }

  public static isType(type: string): boolean {
    return EmailValidator.TYPE === type;
  }

  public static isValid(
    _propFields: unknown,
    _stateFields: unknown,
    value: string,
    _lastSavedValue: unknown,
    _validator: unknown,
    _form: unknown,
  ): boolean {

    return (value.match(EmailValidator.pattern) !== null);
  }
}
