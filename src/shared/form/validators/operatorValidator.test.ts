import OperatorValidator, { OPERATOR } from './operatorValidator';

describe('OperatorValidator', () => {
  describe('isType()', () => {
    it('should return true if type is Operator', () => {
      expect(OperatorValidator.isType('Operator')).toEqual(true);
    });

    it('should return false otherwise', () => {
      expect(OperatorValidator.isType('something else')).toEqual(false);
    });
  });

  describe('isValid()', () => {
    let propFields: {[key: string]: {[key: string]: string}};
    let stateFields: {[key: string]: {[key: string]: string | number}};

    let validator: {
      operator: string
      fieldName: string
      value?: string | number
    };

    beforeEach(() => {
      propFields = {
        field1: {
          type: 'datetime',
        },
        field2: {
          value: 'text',
        },
      };

      stateFields = {
        field1: {
          value: '2020-08-20T14:37:02.120Z',
        },
        field2: {
          value: 10,
        },
      };

      validator = {
        fieldName: '$value',
        operator: OPERATOR.GREATER_THAN,
        value: 10,
      };
    });

    describe('when fieldName now and operator is LESS_THAN', () => {
      beforeEach(() => {
        validator.fieldName = 'now';
        validator.operator = OPERATOR.LESS_THAN;
      });

      it('should return true when value is LESS_THAN a date', () => {
        const value = '2020-08-20T14:37:02.121Z';
        expect(OperatorValidator.isValid(propFields, stateFields, value, null, validator, null)).toEqual(true);
      });
    });

    describe('when fieldName is $value and operator is GREATER_THAN', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.GREATER_THAN;
      });

      it('should return true when value is GREATER_THAN', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 11, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not GREATER_THAN', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 9, null, validator, null)).toEqual(false);
      });
    });

    describe('when fieldName is $value and operator is GREATER_THAN', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.GREATER_THAN_EQUAL_TO;
      });

      it('should return true when value is GREATER_THAN_EQUAL_TO', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 10, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not GREATER_THAN_EQUAL_TO', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 9, null, validator, null)).toEqual(false);
      });
    });

    describe('when fieldName is $value and operator is LESS_THAN', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.LESS_THAN;
      });

      it('should return true when value is LESS_THAN', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 9, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not LESS_THAN', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 10, null, validator, null)).toEqual(false);
      });
    });

    describe('when fieldName is $value and operator is LESS_THAN_EQUAL_TO', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.LESS_THAN_EQUAL_TO;
      });

      it('should return true when value is LESS_THAN_EQUAL_TO', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 10, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not LESS_THAN_EQUAL_TO', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 11, null, validator, null)).toEqual(false);
      });
    });

    describe('when fieldName is $value and operator is EQUAL', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.EQUAL;
      });

      it('should return true when value is EQUAL', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 10, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not EQUAL', () => {
        expect(OperatorValidator.isValid(propFields, stateFields, 11, null, validator, null)).toEqual(false);
      });
    });

    describe('when fieldName is another field and operator is GREATER_THAN', () => {
      beforeEach(() => {
        validator.operator = OPERATOR.GREATER_THAN;
      });

      it('should return true when value is GREATER_THAN a date', () => {
        validator.fieldName = 'field1';
        const value = '2020-08-20T14:37:02.121Z';
        expect(OperatorValidator.isValid(propFields, stateFields, value, null, validator, null)).toEqual(true);
      });

      it('should return true when value is GREATER_THAN a number', () => {
        validator.fieldName = 'field2';
        const value = 11;
        expect(OperatorValidator.isValid(propFields, stateFields, value, null, validator, null)).toEqual(true);
      });

      it('should return false when value is not GREATER_THAN a date', () => {
        validator.fieldName = 'field1';
        const value = '2020-08-20T14:37:02.120Z';
        expect(OperatorValidator.isValid(propFields, stateFields, value, null, validator, null)).toEqual(false);
      });

      it('should return false when value is not GREATER_THAN a number', () => {
        validator.fieldName = 'field2';
        const value = 10;
        expect(OperatorValidator.isValid(propFields, stateFields, value, null, validator, null)).toEqual(false);
      });
    });
  });
});
