export const stopDateTimeFieldWithNonRangeValidator = {
  type: 'regexp',
  regexp: '^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?/]{1,64}$',
  errorMessage: 'Must be between 1-64 characters.  Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter.',
};

export const stopDateTimeFieldWithBasedOnTwoFieldsRangeValidator = {
  type: 'dateRangeBasedOnOtherFields',
  basedOnFieldNames: ['receivedDateTime', 'startDateTime'],
  defaultBasedOnDate: 'now',
  validateOn: 'Create',
  startRange: 14,
  startRangeUnit: 'day',
  endRange: 2,
  endRangeUnit: 'month',
  errorMessage: 'Stop date must be between 2 weeks and 2 months from received date time.',
};

export const stopDateTimeFieldWithBasedOnOneFieldRangeValidator = {
  type: 'dateRangeBasedOnOtherFields',
  basedOnFieldNames: ['receivedDateTime'],
  defaultBasedOnDate: 'now',
  validateOn: 'Create',
  startRange: 14,
  startRangeUnit: 'day',
  endRange: 2,
  endRangeUnit: 'month',
  errorMessage: 'Stop date must be between 2 weeks and 2 months from received date time.',
};

export const stopDateTimeFieldWithBasedOnLastSavedValueRangeValidator = {
  type: 'dateRangeBasedOnLastSavedValue',
  validateOn: 'Edit',
  startRange: 14,
  startRangeUnit: 'day',
  endRange: 2,
  endRangeUnit: 'month',
  errorMessage: 'Stop date must be between 2 weeks and 2 months from the start date.',
};

export const stopDateTimeFieldWithRangeBasedOnNowValidator = {
  type: 'dateRangeBasedOnNow',
  startRange: 14,
  startRangeUnit: 'day',
  endRange: 2,
  endRangeUnit: 'month',
  errorMessage: 'Stop date must be between 2 weeks and 2 months from the today.',
};
