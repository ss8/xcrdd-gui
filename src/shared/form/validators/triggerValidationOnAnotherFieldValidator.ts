import { FormTemplateValidator } from 'data/template/template.types';
import { TextSuggestType } from '../custom/TextSuggest/TextSuggest';

export type FormCallable = {
  handleChange: (e: { currentTarget: { type: string, value: unknown, name: string } }) => void,
  handleDateTimeChange: (ctx: { name: string }, value: unknown) => void,
  handleTextSuggestChange: (ctx: { name: string }, item: TextSuggestType) => void,
}

/**
 * This validator appears in a field of a form template
 * It does not validate the value of its field, but it triggers/invokes validation on another field.
 * For example, warrant stop time validation depends on the value in warrant received time.
 * Therefore when the received time value change, the stop time needs to revalidated again.
 *
 * Validator format in field:
    "receivedDateTime": {
        ....
        "validation": {
            "validators": [
                {
                    "type": "triggerValidationOnAnotherField",
                    "theOtherField": "stopDateTime"
                }
            ]
        }
    }
 *
 */
export default class TriggerValidationOnAnotherFieldValidator {
  private static TYPE: string = 'triggerValidationOnAnotherField' as const;

  public static isType = (aType: string): boolean => {
    return TriggerValidationOnAnotherFieldValidator.TYPE === aType;
  }

  public static isValid = (
    propFields: any,
    stateFields: any,
    _aValue: Date | string,
    _lastSavedValue: Date | string,
    validator: FormTemplateValidator,
    form: FormCallable,
  ): boolean => {
    // Ensure the validator matches this validation class
    if (!validator || !validator.type) return true;
    if (TriggerValidationOnAnotherFieldValidator.isType(validator.type) === false) return true;

    const {
      theOtherField = '',
    } = validator;

    // This class only support fields type listed below
    switch (propFields[theOtherField].type) {
      case 'text':
      case 'autoGrowText':
      case 'password':
      case 'textarea':
      case 'select':
      case 'number':
      case 'checkbox':
        const simulatedEvent = {
          currentTarget: {
            type: propFields[theOtherField].type,
            name: theOtherField,
            value: stateFields[theOtherField].value,
          },
        };
        form.handleChange(simulatedEvent);
        break;

      case 'textSuggest':
        form.handleTextSuggestChange(
          { name: theOtherField },
          { label: stateFields[theOtherField].label, value: stateFields[theOtherField].value },
        );
        break;

      case 'datetime':
        form.handleDateTimeChange({ name: theOtherField },
          stateFields[theOtherField].value);
        break;

      default:
        // eslint-disable-next-line no-console
        console.error(`coding error: type '${propFields[theOtherField].type}' not support in TriggerValidationOnAnotherFieldValidator.isValid`);
        break;
    }

    return true;
  }
}
