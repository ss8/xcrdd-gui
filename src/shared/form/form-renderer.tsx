import React, { Fragment, ReactNode } from 'react';
import {
  MenuItem, Button, Label, HTMLSelect, Checkbox,
  Position, FormGroup, InputGroup, TextArea, RadioGroup, Icon,
} from '@blueprintjs/core';
import {
  Select, Classes, MultiSelect, Suggest,
} from '@blueprintjs/select';
import { TimezonePicker, TimezoneDisplayFormat } from '@blueprintjs/timezone';
import { DateInput, TimePrecision, Classes as ClassesDateInput } from '@blueprintjs/datetime';
import moment from 'moment';
import { defaultTypeaheadPredicate, defaultTypeaheadRenderer, Typeahead } from './custom/typeahead'; // eslint-disable-line
import { defaultultiselectTypeaheadPredicate, defaultMultiselectTypeaheadRenderer, MultiselectTypeahead } from './custom/multiselectTypeahead';
import AutoGrowTextInput from '../textInput/AutoGrowTextInput';
import { getDateTimeFormatTemplate } from '../../utils';
import TextSuggest from './custom/TextSuggest';
import PlainText from './custom/plainText';
import FormButton from './custom/formButton';
import { EMPTY_FIELD } from '.';

export const createSelect = (arr: Array<any>, key: string, label: string) => {
  if (!arr.length) return [];
  return arr.map((entry: any) => ({ value: entry[key], label: entry[label] }));
};

export const defaultSelectOption = 'Select';

const iconElement = (customIcon:string|any, ctx:any) => {
  if (!customIcon) {
    return undefined;
  }
  return (
    <Icon
      onClick={() => ctx()}
      style={{ margin: '7px', color: '#5C7080' }}
      icon={customIcon}
    />
  );
};

function wrapField(
  label: string,
  jsx: any,
  required: boolean,
  index: number,
  errorMessage: any,
  htmlFor?: string,
  type?:string,
  footNote?:any,
) {
  let colon = ':';
  if (type === 'textPlain') {
    colon = '';
  }
  return (
    <div key={index}>
      <Label htmlFor={htmlFor}>
        {
          label === undefined ?
            required ? <span className="required-field">*</span> : <span>&nbsp;</span>
            :
            required ? <Fragment> {label + colon}<span className="required-field">*</span> </Fragment> : <span>{label + colon}</span>
        }
      </Label>
      {jsx}
      {footNote}
      {errorMessage}
    </div>
  );
}

function renderErrorMessage(ctx: any, name: string) {
  return (
    <Fragment>
      {(ctx.state.fields[name].validations && ctx.state.fields[name].validations.errorMessage && ctx.state.fields[name].validations.errorMessage !== '') ?
        <Label className="field-error-message"> {ctx.state.fields[name].validations.errorMessage} </Label> : <Fragment />}
    </Fragment>
  );
}

function renderFootNote(name: string) {
  return (
    <Fragment>
      <Label className="field-footnote"> {name} </Label><Fragment />
    </Fragment>
  );
}

function renderField(ctx: any, field: any, name: string, value: any, index: number, isFormGroupField = false) {

  const {
    icon, rightElement, tabIndex, extended, filterable, disabled, readOnly,
    position, readOnlyIfOneOption, placeholder, inline, minimal, intent, customComponent,
    footNote,
  } = field;

  const { type } = field;

  const rightIcon = iconElement(rightElement, () => ctx.handleRightElement({ name, type }));

  let fieldValue = value;
  const stateField = ctx.state.fields[name];
  let maxLength = 255;
  if ((type === 'text' || type === 'autoGrowText' || type === 'textarea') && stateField.validation && stateField.validation.validators) {
    const lengthValidator = (stateField.validation.validators as Array<any>).find((e: any) => e.type === 'length');
    if (lengthValidator) {
      maxLength = lengthValidator.length;
    }
  }
  const required = stateField.validation && stateField.validation.required;
  const className = 'input-maxwidth';
  const checkboxAdditionalStyle = isFormGroupField ? { margin: '0px' } : {};
  const errorMessageJSX = renderErrorMessage(ctx, name);
  const footMessageJSX = renderFootNote(footNote);
  const dataTestName = `input-${name}`;
  const { htmlId, options, label } = ctx.state.fields[name];

  if (!type) return null;
  switch (type) {
    case 'text':
      return wrapField(label, <InputGroup
        data-test={dataTestName}
        autoComplete="off"
        autoCorrect="off"
        name={name}
        id={htmlId}
        tabIndex={tabIndex}
        className={ctx.getFieldClass(name, className)}
        onChange={ctx.handleChange}
        onBlur={ctx.handleTouched}
        type={type}
        maxLength={maxLength}
        leftIcon={icon}
        rightElement={rightIcon}
        disabled={disabled || readOnly}
        value={fieldValue || ''}
        placeholder={placeholder}
      />, required, index, errorMessageJSX, htmlId, type, footMessageJSX);

    case 'textPlain':
      return wrapField(label, (
        <PlainText
          data-test={dataTestName}
          value={fieldValue || ''}
          id={htmlId}
          tabIndex={tabIndex}
          className={ctx.getFieldClass(name, className)}
        />
      ), required, index, errorMessageJSX, '', type);
    case 'button':
      return (
        <FormButton
          name={name}
          id={htmlId}
          label={label}
          className={ctx.getFieldClass(name, className)}
          onClick={ctx.handleClick}
          icon={icon}
          disabled={disabled || readOnly}
        />
      );
    case 'textSuggest':
      return wrapField(label, (
        <TextSuggest
          dataTestName={dataTestName}
          disabled={disabled || readOnly}
          className={ctx.getFieldClass(name, className)}
          items={options}
          fieldValue={fieldValue}
          name={name}
          placeholder={placeholder}
          onItemSelect={ctx.handleTextSuggestChange.bind(ctx, { name, value: fieldValue })}
          onQueryChange={ctx.handleTextSuggestQueryChange.bind(ctx, { name, value: fieldValue })}
          onBlur={ctx.handleTouched.bind(ctx, { currentTarget: { name, type, value: fieldValue } })}
          onFocus={ctx.handleTextSuggestFocus.bind(ctx, { currentTarget: { name, type, value: fieldValue } })}
        />
      ), required, index, errorMessageJSX);
    case 'autoGrowText':
      const inputGroupProps = {
        autoComplete: 'off',
        autoCorrect: 'off',
        name,
        id: htmlId,
        tabIndex,
        className: ctx.getFieldClass(name, className),
        onChange: ctx.handleChange,
        onBlur: ctx.handleTouched,
        type: 'text',
        maxLength,
        leftIcon: icon,
        disabled: (disabled || readOnly),
        value: (fieldValue || ''),
      };
      return wrapField(label, <AutoGrowTextInput
        data-test={dataTestName}
        inputGroupProps={inputGroupProps}
        maxWidth={ctx.props.parentSize && ctx.props.parentSize.width ?
          ctx.props.parentSize.width : null}
      />, required, index, errorMessageJSX, htmlId);
    case 'password':
      return wrapField(label, <InputGroup
        data-test={dataTestName}
        autoComplete="off"
        autoCorrect="off"
        name={name}
        id={htmlId}
        tabIndex={tabIndex}
        className={ctx.getFieldClass(name, className)}
        onChange={ctx.handleChange}
        onBlur={ctx.handleTouched}
        onFocus={ctx.handleFocus}
        type={type}
        leftIcon={icon}
        rightElement={rightIcon}
        disabled={disabled || readOnly}
        value={fieldValue || ''}
      />, required, index, errorMessageJSX, htmlId);
    case 'textarea':
      return wrapField(label, <TextArea
        data-test={dataTestName}
        autoComplete="off"
        autoCorrect="off"
        name={name}
        id={htmlId}
        fill
        tabIndex={tabIndex}
        className={ctx.getFieldClass(name, className)}
        onChange={ctx.handleChange}
        maxLength={maxLength}
        onBlur={ctx.handleTouched}
        disabled={disabled || readOnly}
        value={fieldValue || ''}
      />, required, index, errorMessageJSX, htmlId);
    case 'select':
      if (readOnlyIfOneOption && options.length === 1 && options[0].value !== 'undefined' && options[0].value !== '') {
        return wrapField(label, <InputGroup
          data-test={dataTestName}
          autoComplete="off"
          autoCorrect="off"
          tabIndex={tabIndex}
          name={name}
          id={htmlId}
          className={ctx.getFieldClass(name, className)}
          value={options[0].label}
          readOnly
        />, required, index, errorMessageJSX, htmlId);
      }
      return wrapField(label, <HTMLSelect
        data-test={dataTestName}
        autoComplete="off"
        autoCorrect="off"
        name={name}
        id={htmlId}
        tabIndex={tabIndex}
        className={ctx.getFieldClass(name, className)}
        options={options ||
          createSelect(ctx.props[extended.optionSource], extended.optionKey, extended.optionValue)}
        onChange={ctx.handleChange}
        onBlur={ctx.handleTouched}
        disabled={disabled || readOnly}
        value={fieldValue || ''}
      />, required, index, errorMessageJSX, htmlId);
    case 'radio':
      const radioComponent = (
        <div>
          <RadioGroup
            inline={inline}
            data-test={dataTestName}
            name={name}
            className={ctx.getFieldClass(name, className)}
            options={options}
            onChange={ctx.handleChange}
            disabled={disabled || readOnly}
            selectedValue={fieldValue || ''}
          />
          {errorMessageJSX}
        </div>
      );

      if (!label) {
        return radioComponent;
      }

      return wrapField(label, radioComponent, required, index, errorMessageJSX);
    case 'typeahead':
      const SelectTypeahead = Select.ofType<Typeahead>();
      const selectedText =
        options.filter((item: any) => item.value === ctx.state.fields[name].value)[0].label;
      return wrapField(label, (
        <SelectTypeahead
          data-test={dataTestName}
          disabled={disabled || readOnly}
          itemRenderer={defaultTypeaheadRenderer}
          itemPredicate={defaultTypeaheadPredicate}
          className={`${Classes.SELECT} ${className}`}
          filterable={filterable}
          items={options}
          query={ctx.state.fields[name].query}
          noResults={<MenuItem disabled text="No results." />}
          onItemSelect={ctx.handleTypeaheadChange.bind(ctx, { name, value: fieldValue })}
          activeItem={fieldValue}
        >
          <Button
            fill
            text={selectedText}
          />
        </SelectTypeahead>
      ), required, index, errorMessageJSX);
    case 'number':
      return wrapField(label, <InputGroup
        data-test={dataTestName}
        autoComplete="off"
        autoCorrect="off"
        tabIndex={tabIndex}
        name={name}
        id={htmlId}
        className={ctx.getFieldClass(name, className)}
        onChange={ctx.handleChange}
        onBlur={ctx.handleTouched}
        type={type}
        leftIcon={icon}
        disabled={disabled}
        value={fieldValue || 0}
        min={0}
        readOnly={readOnly}
      />, required, index, errorMessageJSX, htmlId);
    case 'datetime':
      const timePrecision = field.timePrecision ? field.timePrecision : TimePrecision.MINUTE;

      const component = (
        <DateInput
          data-test={dataTestName}
          dayPickerProps={{
            className: name,
          }}
          value={fieldValue ? new Date(fieldValue) : null}
          inputProps={{
            tabIndex, name, autoCorrect: 'off', autoComplete: 'off', id: htmlId, leftIcon: icon,
          }}
          className={`${ClassesDateInput.DATEINPUT} ${className}`}
          popoverProps={{ position: Position.BOTTOM }}
          formatDate={(date) => moment(date).format(getDateTimeFormatTemplate())}
          parseDate={(str) => moment(str, getDateTimeFormatTemplate()).toDate()}
          onChange={ctx.handleDateTimeChange.bind(ctx, { name, value: fieldValue })}
          disabled={disabled || readOnly}
          timePrecision={timePrecision}
          maxDate={moment(new Date()).add(20, 'year').toDate()}
        />
      );

      return wrapField(label, component, required, index, errorMessageJSX, htmlId);
    case 'checkbox':
      return (
        <Fragment><Checkbox
          data-test={dataTestName}
          tabIndex={tabIndex}
          key={index}
          name={name}
          className={ctx.getFieldClass(name, className)}
          label={label}
          onBlur={ctx.handleTouched}
          onChange={ctx.handleChange}
          checked={!!fieldValue}
          disabled={disabled || readOnly}
          style={required
            ? { display: 'inline-block', width: 'auto', ...checkboxAdditionalStyle }
            : checkboxAdditionalStyle}
        />
          {required && <span className="required-field">*</span>}
        </Fragment>
      );
    case 'timezone':
      return wrapField(label, <TimezonePicker
        data-test={dataTestName}
        className={ctx.getFieldClass(name, className)}
        onChange={ctx.handleTimeZoneChange.bind(ctx, { name, value: fieldValue })}
        popoverProps={{
          position: Position.BOTTOM,
          popoverClassName: 'popover-scroll',
        }}
        valueDisplayFormat={TimezoneDisplayFormat.NAME}
        buttonProps={{ fill: true }}
        disabled={disabled || readOnly}
        value={fieldValue || ''}
      />, required, index, errorMessageJSX);
    case 'multiselectTypeahead':
      let optionsAvailable = Object.assign([], options.sort((first: any, next: any) => {
        if (first.value < next.value) { return -1; }
        if (first.value > next.value) { return 1; }
        return 0;
      }));
      if (fieldValue === undefined) {
        fieldValue = [];
      } else {
        optionsAvailable = options.filter((e: { label: any; value: any }) =>
          !fieldValue.includes(e.value));
        fieldValue = options.filter((e: { label: any; value: any }) =>
          fieldValue.includes(e.value));
      }
      const MultiselectTypeaheadComponent = MultiSelect.ofType<MultiselectTypeahead>();
      return wrapField(label,
        <div tabIndex={tabIndex}>
          <MultiselectTypeaheadComponent
            data-test={dataTestName}
            itemPredicate={defaultultiselectTypeaheadPredicate}
            itemRenderer={defaultMultiselectTypeaheadRenderer}
            className={ctx.getFieldClass(name, className)}
            tagRenderer={(item: any) => item.label}
            items={optionsAvailable}
            query={ctx.state.fields[name].query}
            noResults={<MenuItem disabled text="No results." />}
            tagInputProps={{
              disabled: disabled || readOnly,
              onRemove: ctx.handleMultiselectTypeaheadItemRemove
                .bind(ctx, { name, value: fieldValue }),
            }}
            popoverProps={{
              minimal: true,
              popoverClassName: 'popover-scroll',
              position: position || Position.BOTTOM,
            }}
            resetOnSelect
            onItemSelect={ctx.handleMultiselectTypeaheadChange.bind(ctx, { name, value: fieldValue })}
            selectedItems={fieldValue}
          />
        </div>, required, index, errorMessageJSX);
    case 'timeInterval':
      return wrapField(label, (
        <div>
          <HTMLSelect
            data-test={dataTestName}
            autoComplete="off"
            autoCorrect="off"
            defaultValue="0"
            name={`${name}-Hours`}
            tabIndex={tabIndex}
            disabled={disabled || readOnly}
            className={ctx.getFieldClass(name)}
            options={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
              16, 17, 18, 19, 20, 21, 22, 23, 24]}
            onChange={ctx.handletimeIntervalChange}
            value={ctx.getTimeIntervalValue(fieldValue, 'Hours')}
          />
          <span style={{ paddingRight: '10px' }}> Hours</span>
          <HTMLSelect
            data-test={dataTestName}
            autoComplete="off"
            autoCorrect="off"
            defaultValue="0"
            name={`${name}-Mins`}
            tabIndex={tabIndex}
            disabled={disabled || readOnly}
            className={ctx.getFieldClass(name)}
            options={[0, 15, 30, 45]}
            onChange={ctx.handletimeIntervalChange}
            value={ctx.getTimeIntervalValue(fieldValue, 'Mins')}
          />
          <span> Mins</span>
        </div>
      ), required, index, errorMessageJSX);
    case 'label':
      return wrapField(label,
        <Label
          data-test={dataTestName}
          className={`label-field label-field-${name}`}
        >
          {fieldValue}
        </Label>, required, index, errorMessageJSX);
    case 'detailsButton':
      return wrapField(label,
        <Button
          intent={intent}
          minimal={minimal}
          icon={icon}
          data-test={dataTestName}
          text={field.initial}
          onClick={field.handleClick}
        />, required, index, errorMessageJSX);
    case 'buttonWithMessage':
      return (
        <div className="form-flex-row-css">
          <Button
            data-test={dataTestName}
            text={field.initial}
            onClick={field.handleClick}
          />
          <Label>
            {!label ? '' : label}
          </Label>
        </div>
      );

    case 'custom':

      const CustomComponent = ctx.props.customFieldComponentMap?.[customComponent];

      if (CustomComponent == null) {
        return null;
      }

      const customComponentElement = (
        <CustomComponent
          value={fieldValue || ''}
          label={label}
        />
      );

      if (!label) {
        return (
          customComponentElement
        );
      }

      return wrapField(label, customComponentElement, required, index, errorMessageJSX, htmlId);

    default:
      break;
  }
  return null;
}

export function renderFields(ctx: any, rowLayout: any, isFormGroupFields = false, formGroupFlexFlow = 'row'): ReactNode {
  const {
    fields,
    flexFlow = 'row',
    cssClassName = '',
  } = ctx.props;

  if (isFormGroupFields) {
    const formGroupClassName = formGroupFlexFlow === 'row' ? 'form-flex-row-css' : 'form-flex-col-css';
    return (
      <div className={formGroupClassName}>
        {renderDivElements(rowLayout, ctx, fields, isFormGroupFields)}
      </div>
    );
  }

  const className = `${flexFlow === 'row' ? 'form-flex-row-css' : 'form-flex-col-css'} ${cssClassName}`;
  return (
    <form autoComplete="off" autoCorrect="off">
      <div className={className}>
        {renderDivElements(rowLayout, ctx, fields, isFormGroupFields)}
      </div>
    </form>
  );
}

function renderDivElements(rowLayout: any, ctx: any, fields: any, isFormGroupFields: boolean) {
  return rowLayout.map((rowLayoutEntry: any, j: number) => {
    if (typeof (rowLayoutEntry) === 'object' && ('group' in rowLayoutEntry)) {
      if (rowLayoutEntry.hide === 'true') {
        return (
          <Fragment key={j} />
        );
      }

      return (
        <div className="field-group" key={j}>
          {renderGroup(ctx, rowLayoutEntry, j)}
        </div>
      );
    }

    if (rowLayoutEntry === EMPTY_FIELD) {
      // Indicates that the field should be rendered as a empty field
      return (
        <div key={j} className="field field-empty" />
      );
    }

    const {
      value,
      hide,
      className = '',
    } = ctx.state.fields[rowLayoutEntry];

    const {
      renderAsEmptyIfHidden,
      className: fieldClassName = '',
    } = fields[rowLayoutEntry];

    try {
      if (hide === 'true') {
        if (renderAsEmptyIfHidden) {
          return (
            <div key={j} className="field" />
          );
        }

        return (
          <Fragment key={j} />
        );
      }

      return (
        <div key={j} className={`field ${className} ${fieldClassName}`} onKeyPress={ctx.handleKeyPress.bind(ctx)}>
          {renderField(ctx, fields[rowLayoutEntry], rowLayoutEntry, value, j, isFormGroupFields)}
        </div>
      );
    } catch (err) {
      throw Error(`Problem rendering form fields. Check JSON configuration: ${rowLayoutEntry}`);
    }
  });
}

function renderGroup(ctx: any, groupObj: any, index: number) {
  return (
    <FormGroup
      key={index}
      label={groupObj.group}
      style={(groupObj.group === '') ? { border: 'none' } : {}}
    >
      {renderFields(ctx, groupObj.fields, true, groupObj.flexDirection !== undefined ? groupObj.flexDirection : 'column')}
    </FormGroup>
  );
}
