import { FormTemplate } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export const fieldWithAffectedFieldOnChangeExtendedForm: FormTemplate = {
  version: '1.0.1',
  name: 'fieldWithAffectedFieldOnChangeExtendedForm',
  key: 'fieldWithAffectedFieldOnChangeExtendedForm',
  selectField: {
    metadata: {
      layout: {
        rows: [
          [
            'transport',
            'uri',
            'enableKpis',
          ],
        ],
      },
    },
    fields: {
      enableKpis: {
        type: 'checkbox',
        label: 'Enable KPIs',
        initial: false,
        hide: 'true',
        validation: {
          required: true,
        },
      },
      transport: {
        label: 'Delivery Type',
        type: 'select',
        initial: 'HTTP',
        options: [
          {
            label: 'IP/Port',
            value: 'TCP',
          },
          {
            label: 'URI',
            value: 'HTTP',
          },
          {
            label: 'E164 Number',
            value: 'E164',
          },
        ],
        affectedFieldsOnChangeExtended: {
          TCP: {
            layoutOperations: [
              {
                operation: 'setLayoutOrder',
                parameters: ['enableKpis', 'uri'],
              },
            ],
            fields: {
              enableKpis: {
                hide: 'false',
              },
              uri: {
                hide: 'true',
                validation: {
                  required: false,
                },
              },
            },
          },
          E164: {
            layoutOperations: [
              {
                operation: 'setLayoutOrder',
                parameters: ['uri', 'enableKpis'],
              },
            ],
            fields: {
              enableKpis: {
                hide: 'true',
                validation: {
                  required: false,
                },
              },
              uri: {
                label: 'Delivery E164 Phone Number',
                hide: 'false',
                validation: {
                  required: true,
                  validators: [
                    {
                      type: 'regexp',
                      regexp: '^[0-9]*$',
                      errorMessage: 'Please enter a valid E.164 phone number.',
                    },
                    {
                      type: 'length',
                      length: 15,
                      errorMessage: 'Exceeds maximum length, 15.  Please re-enter.',
                    },
                  ],
                },
              },
            },
          },
          HTTP: {
            layoutOperations: [
              {
                operation: 'setLayoutOrder',
                parameters: ['uri', 'enableKpis'],
              },
            ],
            fields: {
              enableKpis: {
                hide: 'true',
                validation: {
                  required: false,
                },
              },
              uri: {
                label: 'Delivery URI',
                hide: 'false',
                validation: {
                  required: false,
                  validators: [
                    {
                      type: 'length',
                      length: 256,
                    },
                    {
                      type: 'URL',
                      errorMessage: 'Must be a valid URL. Please re-enter.',
                    },
                  ],
                },
              },
            },
          },
        },
      },
      uri: {
        label: 'Delivery URI',
        type: 'text',
        hide: 'false',
        validation: {
          required: false,
          validators: [
            {
              type: 'length',
              length: 256,
            },
            {
              type: 'URL',
              errorMessage: 'Must be a valid URL. Please re-enter.',
            },
          ],
        },
      },
    },
  },
};
