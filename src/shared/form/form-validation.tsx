import { FormTemplateValidator, FormTemplateValidation } from 'data/template/template.types';
import OperatorValidator from './validators/operatorValidator';
import DateTimeRangeValidator from './validators/dateTimeRangeValidator';
import TriggerValidationOnAnotherFieldValidator from './validators/triggerValidationOnAnotherFieldValidator';
import IPAddressValidator from './validators/IPAddressValidator';
import URLValidator from './validators/URLValidator';
import EmailValidator from './validators/EmailValidator';
import Form from './index';
import IntervalValidator from './validators/intervalValidator';

/**
 * This function is used to apply the validators that are defined in the FormTemplate.
 *
 * Validator type 'customFunction'
 *
 * This type should be used when it is required to access dynamic or additional data
 * that is not available in the form itself.  This may be useful when you have multiples
 * forms in the page and a field from a form depends on a field that is available in another
 * form.
 *
 * @return returns an object { isValid: true, errorMessage: '' }
 * isValid is true if field has valid value, otherwise false
 * errorMessage has the corresponding error message when field is invalid.
 */
export function applyFieldValidators(
  validation: FormTemplateValidation | undefined,
  value: any,
  originalValue: any,
  propFields?: any,
  stateFields?: any,
  form?: Form,
): { isValid: boolean, errorMessage: string } {
  const validationResult = { isValid: true, errorMessage: '' };

  if (typeof (value) === 'string') value = value.trim();
  if (validation?.required && (!value || value.length === 0)) {
    validationResult.isValid = false;
    validationResult.errorMessage = 'Please enter a value.';
    return validationResult;
  }

  if (validation?.validators && value) {
    validation.validators.every((validator) => {
      if (!validator.type) return true;

      if (validator.type === 'regexp') {
        if (value.match(validator.regexp) == null) {
          validationResult.isValid = false;
        }
      } else if (validator.type === 'length') {
        if (typeof value === 'string' && validator.length && value.length > validator.length) {
          validationResult.isValid = false;
        }

      } else if (validator.type === 'customFunction') {
        // Check function documentation for detailed explanation
        const { customValidationFunctionMap } = form?.props || {};
        const { customValidationFunction } = validator;

        if ((customValidationFunctionMap == null) ||
            (customValidationFunction == null) ||
            (customValidationFunctionMap[customValidationFunction] == null)) {
          return true;
        }

        const validationFunction = customValidationFunctionMap[customValidationFunction];
        const { isValid, errorMessage } = validationFunction(
          propFields,
          stateFields,
          value,
          originalValue,
          validator,
          form,
        );
        validationResult.isValid = isValid;
        validationResult.errorMessage = errorMessage;

      } else {
        const validatorClasses = [
          OperatorValidator,
          IPAddressValidator,
          URLValidator,
          DateTimeRangeValidator,
          IntervalValidator,
          TriggerValidationOnAnotherFieldValidator,
          EmailValidator,
        ];
        const validatorClass = validatorClasses.filter((aValidatorClass) => {
          return (aValidatorClass.isType(validator.type));
        });
        if (validatorClass && validatorClass.length > 0) {
          validationResult.isValid = validatorClass[0].isValid(
            propFields,
            stateFields,
            value,
            originalValue,
            validator,
            form,
          );
        }
      }

      if (validationResult.isValid === false) {
        validationResult.errorMessage = validationResult.errorMessage || validator.errorMessage || '';
        return false;
      }

      return true;
    });
  }

  return validationResult;
}

export function checkValid(
  form: Form,
  name: string,
  value: any,
  _forceTouched = false,
): { isValid: boolean, errorMessage: string } {
  const validationResult = { isValid: true, errorMessage: '' };
  const propFields = form.props.fields; // template fields
  const stateFields = form.state.fields;

  // skip the fields that do not have validation requirements
  if (!stateFields[name] || !stateFields[name].validation || stateFields[name].hide === 'true') return validationResult;
  return applyFieldValidators(stateFields[name].validation, value, stateFields[name].originalValue,
    propFields, stateFields, form);
}
