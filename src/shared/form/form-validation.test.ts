import { FormTemplateField } from 'data/template/template.types';
import { applyFieldValidators } from './form-validation';
import { OPERATOR } from './validators/operatorValidator';

describe('applyFieldValidators', () => {
  let field: FormTemplateField;
  let propFields: {[key: string]: {[key: string]: string}};
  let stateFields: {[key: string]: {[key: string]: string | number}};
  let form: any;

  beforeEach(() => {
    field = {
      label: 'Field 1',
      type: 'text',
      validation: {
        required: true,
      },
    };

    propFields = {
      field1: {
        type: 'dateTime',
      },
      field2: {
        type: 'text',
      },
    };

    stateFields = {
      field1: {
        value: '2020-08-20T14:37:02.120Z',
      },
      field2: {
        value: 10,
      },
    };
  });

  describe('when field is required', () => {
    it('should return isValid as true and no errorMessage if value is not empty', () => {
      expect(applyFieldValidators(field.validation, 'new value', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and errorMessage if value is empty', () => {
      expect(applyFieldValidators(field.validation, '   ', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Please enter a value.',
      });
    });
  });

  describe('when field is not required', () => {
    it('should return isValid as true and no errorMessage if value is empty', () => {
      field.validation = { required: false };
      expect(applyFieldValidators(field.validation, '   ', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if value is empty and there are other validators', () => {
      field.validation = {
        required: false,
        validators: [{
          type: 'regexp',
          regexp: '^(6[0-3]|[1-5][0-9]|[0-9])$',
          errorMessage: 'Must be between 0 - 63. Please re-enter.',
        }],
      };
      expect(applyFieldValidators(field.validation, '', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });
  });

  describe('when field has Operator validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: OPERATOR.type,
          fieldName: '$value',
          operator: OPERATOR.GREATER_THAN,
          value: 10,
          errorMessage: 'Some error message',
        }],
      };
    });

    it('should return isValid as true and no errorMessage if value is empty', () => {
      expect(applyFieldValidators(field.validation, '', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if value GREATER_THAN', () => {
      expect(applyFieldValidators(field.validation, '11', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if value is not GREATER_THAN', () => {
      expect(applyFieldValidators(field.validation, '10', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });
  });

  describe('when field has IPAddress validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: 'IPAddress',
          errorMessage: 'Some error message',
        }],
      };
    });

    it('should return isValid as true and no errorMessage if value is a valid ip address', () => {
      expect(applyFieldValidators(field.validation, '12.12.12.12', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if value is a invalid ip address', () => {
      expect(applyFieldValidators(field.validation, 'a.a.a.a', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });
  });

  describe('when field has Email address validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: 'EMAIL',
          errorMessage: 'Some error message',
        }],
      };
    });

    it('should return isValid as true and no errorMessage if value is a valid Email address', () => {
      expect(applyFieldValidators(field.validation, 'bob@ss8.com', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if value is a invalid Email address', () => {
      expect(applyFieldValidators(field.validation, 'bobss8.com', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });
  });

  describe('when field has URLAddress validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: 'URL',
          errorMessage: 'Some error message',
        }],
      };
    });

    it('should return isValid as true and no errorMessage if value is a valid URL address', () => {
      expect(applyFieldValidators(field.validation, 'https://www.mavenir.net:443/li/', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if value is a invalid URL address', () => {
      expect(applyFieldValidators(field.validation, 'www.mavenir.net:123456/li/', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });
  });

  describe('when field has customFunction validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: 'customFunction',
          customValidationFunction: 'customValidator',
          errorMessage: 'Some error message',
        }],
      };

      form = {
        props: {
          customValidationFunctionMap: {
            customValidator: jest.fn().mockImplementation(() => ({
              isValid: true,
              errorMessage: '',
            })),
          },
        },
      };
    });

    it('should return isValid as true and no errorMessage if customValidationFunctionMap is not defined', () => {
      const formInstance: any = { props: {} };
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, formInstance)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if customValidationFunction is not defined', () => {
      if (field.validation?.validators?.[0].customValidationFunction) {
        field.validation.validators[0].customValidationFunction = undefined;
      }
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, form)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if customValidationFunction is not in the map', () => {
      if (field.validation?.validators?.[0].customValidationFunction) {
        field.validation.validators[0].customValidationFunction = 'someThing';
      }
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, form)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if customValidator returns it is valid', () => {
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, form)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if customValidator returns it is invalid', () => {
      form.props.customValidationFunctionMap.customValidator.mockImplementation(() => ({
        isValid: false,
        errorMessage: 'Some error message',
      }));
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, form)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });

    it('should return the custom error message if it was defined', () => {
      form.props.customValidationFunctionMap.customValidator.mockImplementation(() => ({
        isValid: false,
        errorMessage: 'Some custom error message',
      }));
      expect(applyFieldValidators(field.validation, 'test', '', propFields, stateFields, form)).toEqual({
        isValid: false,
        errorMessage: 'Some custom error message',
      });
    });
  });

  describe('when field has Interval validator', () => {
    beforeEach(() => {
      field.validation = {
        validators: [{
          type: 'Interval',
          intervals: [[0, 0], [10, 20]],
          errorMessage: 'Some error message',
        }],
      };
    });

    it('should return isValid as true and no errorMessage if value is empty', () => {
      expect(applyFieldValidators(field.validation, '', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as true and no errorMessage if value in interval', () => {
      expect(applyFieldValidators(field.validation, '11', '', propFields, stateFields)).toEqual({
        isValid: true,
        errorMessage: '',
      });
    });

    it('should return isValid as false and a errorMessage if value is not in interval', () => {
      expect(applyFieldValidators(field.validation, '5', '', propFields, stateFields)).toEqual({
        isValid: false,
        errorMessage: 'Some error message',
      });
    });
  });
});
