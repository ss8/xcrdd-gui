import cloneDeep from 'lodash.clonedeep';
import {
  FormTemplate,
  FormTemplateSection,
} from 'data/template/template.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { LayoutOperation } from '../../data/template/template.types';
import { adaptFormTemplate, adaptFormTemplateState } from './form.adapter';
import { DynamicFormFieldMap } from './form.types';

describe('form.adapter', () => {
  let template: FormTemplate;
  let templateWithAffectedFieldsOnChangeExtended : FormTemplateSection;
  let templateWithoutAffectedFieldsOnChangeExtended : FormTemplateSection;
  let expected: FormTemplateSection;

  beforeEach(() => {
    template = getMockTemplateByKey('ETSI103221');
    templateWithAffectedFieldsOnChangeExtended = cloneDeep(template.dataInterfaces_directFromAF as FormTemplateSection);

    templateWithoutAffectedFieldsOnChangeExtended = cloneDeep(templateWithAffectedFieldsOnChangeExtended);
    templateWithoutAffectedFieldsOnChangeExtended.fields.transport.affectedFieldsOnChangeExtended = undefined;

    expected = cloneDeep(templateWithAffectedFieldsOnChangeExtended);
    expected.metadata.layout = {
      ...expected.metadata.layout,
      rows: [
        expected.metadata.layout?.rows[0] ?? [],
        [
          'transport',
          'uri',
          'ownnetid',
          '$empty',
          '$empty',
        ],
        expected.metadata.layout?.rows[2] ?? [],
      ],
    };
    expected.fields = {
      ...expected.fields,
      ownnetid: {
        ...expected.fields.ownnetid,
        hide: 'true',
        validation: {
          required: false,
        },
      },
      uri: {
        ...expected.fields.uri,
        label: 'Delivery Email Address',
        hide: 'false',
        validation: {
          required: true,
          validators: [
            {
              type: 'EMAIL',
              errorMessage: 'Please enter a valid email address.',
            },
            {
              type: 'length',
              length: 255,
              errorMessage: 'Exceeds maximum length, 255.  Please re-enter.',
            },
          ],
        },
      },
    };
  });

  describe('adaptFormTemplate', () => {
    it('should return correct updated template section when updated value requires fields to swap layout position and update their definitions', () => {
      const formSectionData = {
        transport: 'EMAIL',
      };

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        formSectionData,
      );
      expect(adaptedFields).toEqual(expected.fields);
      expect(adaptedLayoutRows).toEqual(expected.metadata?.layout?.rows);
    });

    it('should return the same fields and layoutRows when data is undefined', () => {
      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        undefined,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same fields and layoutRows when affectedFieldsOnChangeExtended is not in fields', () => {
      const formSectionData = {
        transport: 'EMAIL',
      };

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithoutAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same fields and layoutRows when data value is not in affectedFieldsOnChangeExtended', () => {
      const formSectionData = {
        transport: 'BlaBla',
      };

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same fields and layoutRows when data field is not in fields', () => {
      const formSectionData = {
        BlaBla: 'BlaBla',
      };

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same layoutRows when layoutOperations is not defined in affectedFieldsActionDefs', () => {
      const formSectionData = {
        transport: 'EMAIL',
      };

      const templateWithoutLayoutOperations = cloneDeep(templateWithAffectedFieldsOnChangeExtended);
      const { affectedFieldsOnChangeExtended } = templateWithoutLayoutOperations.fields.transport;
      if (affectedFieldsOnChangeExtended) {
        Object.keys(affectedFieldsOnChangeExtended).forEach((value:string) => {
          affectedFieldsOnChangeExtended[value].layoutOperations = undefined;
        });
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same layoutRows when field names are not found in layoutOperations.parameters[0]', () => {
      const formSectionData = {
        transport: 'EMAIL',
      };

      const layoutOperations = templateWithAffectedFieldsOnChangeExtended.fields.transport
        .affectedFieldsOnChangeExtended?.EMAIL.layoutOperations as LayoutOperation[];
      if (layoutOperations) {
        layoutOperations[0].parameters = ['blabla1', 'ownnetid'];
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
          templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
          formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same layoutRows when field names are not found in layoutOperations.parameters[1]', () => {
      const formSectionData = {
        transport: 'EMAIL',
      };

      const layoutOperations = templateWithAffectedFieldsOnChangeExtended.fields.transport
        .affectedFieldsOnChangeExtended?.EMAIL.layoutOperations as LayoutOperation[];
      if (layoutOperations) {
        layoutOperations[0].parameters = ['uri', 'blabla'];
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(
        templateWithAffectedFieldsOnChangeExtended.fields,
          templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
          formSectionData,
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

  });

  describe('adaptFormTemplateState', () => {
    let formStateFields:DynamicFormFieldMap;
    let expectedFields:DynamicFormFieldMap;

    let formStateFieldsWithoutAffectedFieldsOnChangeExtended:DynamicFormFieldMap;

    beforeEach(() => {
      const { fields } = templateWithAffectedFieldsOnChangeExtended;
      formStateFields = {};
      Object.keys(fields).forEach((key) => {
        formStateFields[key] = {
          ...fields[key],
          validations: {},
          value: '',
        };
      });

    });

    it('should return correct updated template section when updated value requires fields to swap layout position and update their definitions', () => {
      expectedFields = {};
      Object.keys(expected.fields).forEach((key) => {
        expectedFields[key] = {
          ...expected.fields[key],
          validations: {},
          value: '',
        };
      });

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'EMAIL',
      );
      expect(adaptedFields).toEqual(expectedFields);
      expect(adaptedLayoutRows).toEqual(expected.metadata?.layout?.rows);
    });

    it('should return the same fields and layoutRows when value is undefined', () => {
      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        undefined,
      );
      expect(adaptedFields).toEqual(formStateFields);
      expect(adaptedLayoutRows).toEqual(templateWithAffectedFieldsOnChangeExtended.metadata?.layout?.rows);
    });

    it('should return the same fields and layoutRows when fieldname is undefined', () => {
      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        undefined,
        'EMAIL',
      );
      expect(adaptedFields).toEqual(formStateFields);
      expect(adaptedLayoutRows).toEqual(templateWithAffectedFieldsOnChangeExtended.metadata?.layout?.rows);
    });

    it('should return the same fields and layoutRows when affectedFieldsOnChangeExtended is not in fields', () => {
      formStateFields.transport.affectedFieldsOnChangeExtended = undefined;

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithoutAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'EMAIL',
      );

      expect(adaptedFields).toEqual(formStateFields);
      expect(adaptedLayoutRows).toEqual(templateWithoutAffectedFieldsOnChangeExtended.metadata?.layout?.rows);

    });

    it('should return the same fields and layoutRows when data value is not in affectedFieldsOnChangeExtended', () => {
      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'BlaBla',
      );
      expect(adaptedFields).toEqual(formStateFields);
      expect(adaptedLayoutRows).toEqual(templateWithAffectedFieldsOnChangeExtended.metadata?.layout?.rows);
    });

    it('should return the same fields and layoutRows when data field is not in fields', () => {
      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'BlaBla',
        'BlaBla',
      );
      expect(adaptedFields).toEqual(formStateFields);
      expect(adaptedLayoutRows).toEqual(templateWithAffectedFieldsOnChangeExtended.metadata?.layout?.rows);
    });

    it('should return the same layoutRows when layoutOperations is not defined in affectedFieldsActionDefs', () => {
      const formStateFieldsWithoutLayoutOperations = cloneDeep(formStateFields);
      const { affectedFieldsOnChangeExtended } = formStateFieldsWithoutLayoutOperations.transport;
      if (affectedFieldsOnChangeExtended) {
        Object.keys(affectedFieldsOnChangeExtended).forEach((value:string) => {
          affectedFieldsOnChangeExtended[value].layoutOperations = undefined;
        });
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFieldsWithoutLayoutOperations,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'EMAIL',
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same layoutRows when field names are not found in layoutOperations.parameters[0]', () => {
      const layoutOperations = formStateFields.transport
        .affectedFieldsOnChangeExtended?.EMAIL.layoutOperations as LayoutOperation[];
      if (layoutOperations) {
        layoutOperations[0].parameters = ['blabla1', 'ownnetid'];
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'EMAIL',
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });

    it('should return the same layoutRows when field names are not found in layoutOperations.parameters[1]', () => {
      const layoutOperations = formStateFields.transport
        .affectedFieldsOnChangeExtended?.EMAIL.layoutOperations as LayoutOperation[];
      if (layoutOperations) {
        layoutOperations[0].parameters = ['uri', 'blabla'];
      }

      const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(
        formStateFields,
        templateWithAffectedFieldsOnChangeExtended.metadata.layout?.rows ?? [],
        'transport',
        'EMAIL',
      );
      expect(adaptedFields).toEqual(adaptedFields);
      expect(adaptedLayoutRows).toEqual(adaptedLayoutRows);
    });
  });
});
