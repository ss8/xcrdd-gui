import cloneDeep from 'lodash.clonedeep';
import {
  FormTemplateField, FormTemplateFieldMap, LayoutOperation, ValueToAffectedField,
} from 'data/template/template.types';
import { DynamicFormFieldMap, NameValueMap } from './form.types';

export function adaptFormTemplate(
  fields: FormTemplateFieldMap,
  layoutRows: string[][],
  data: NameValueMap | undefined,
): { adaptedFields:FormTemplateFieldMap, adaptedLayoutRows:string[][] } {
  if (!data) {
    return { adaptedFields: fields, adaptedLayoutRows: layoutRows };
  }

  const fieldsWithAffectedFieldsOnChangeExtended = findFieldsWithAffectedFieldsOnChangeExtended(fields);

  // return the original templateSection when there is no field defined with affectedFieldsOnChangeExtended
  if (fieldsWithAffectedFieldsOnChangeExtended.length === 0) {
    return { adaptedFields: fields, adaptedLayoutRows: layoutRows };
  }

  const clonedFields = cloneDeep(fields);
  const clonedLayoutRows = cloneDeep(layoutRows);
  fieldsWithAffectedFieldsOnChangeExtended.forEach(([fieldName, fieldDefinition]) => {
    // only update the template when field value matches one of the affected-field-change conditions
    if (data[fieldName]) {
      const { affectedFieldsOnChangeExtended } = fieldDefinition;
      if (affectedFieldsOnChangeExtended) {
        Object.entries(affectedFieldsOnChangeExtended).forEach(([value, affectedFieldsActionDefs]) => {
          if (value === data[fieldName]) {
            adaptTemplateToAffectedFields(clonedFields, affectedFieldsActionDefs.fields);
            adaptTemplateToAffectedFieldsLayout(clonedLayoutRows, affectedFieldsActionDefs.layoutOperations);
          }
        });
      }
    }
  });

  return { adaptedFields: clonedFields, adaptedLayoutRows: clonedLayoutRows };
}

export function adaptFormTemplateState(
  fields: DynamicFormFieldMap,
  layoutRows: string[][],
  fieldName:string | undefined,
  value:string | undefined,
): { adaptedFields:DynamicFormFieldMap, adaptedLayoutRows: string[][] } {
  if (!value || !fieldName || !fields[fieldName]) {
    return { adaptedFields: fields, adaptedLayoutRows: layoutRows };
  }
  // return the original templateSection when field does not define affectedFieldsOnChangeExtended
  const { affectedFieldsOnChangeExtended } = fields[fieldName];
  if (!affectedFieldsOnChangeExtended) {
    return { adaptedFields: fields, adaptedLayoutRows: layoutRows };
  }

  const clonedFields = cloneDeep(fields);
  const clonedLayoutRows = cloneDeep(layoutRows);
  Object.entries(affectedFieldsOnChangeExtended).forEach(([possibleValue, affectedFieldsActionDefs]) => {
    if (possibleValue === value) {
      adaptTemplateToAffectedFields(clonedFields, affectedFieldsActionDefs.fields);
      adaptTemplateToAffectedFieldsLayout(clonedLayoutRows, affectedFieldsActionDefs.layoutOperations);
    }
  });

  return { adaptedFields: clonedFields, adaptedLayoutRows: clonedLayoutRows };
}

function adaptTemplateToAffectedFields(
  clonedFields: FormTemplateFieldMap,
  affectedFields:ValueToAffectedField,
): FormTemplateFieldMap {

  Object.keys(affectedFields).forEach((fieldName) => {
    clonedFields[fieldName] = {
      ...clonedFields[fieldName],
      ...affectedFields[fieldName],
    };
  });
  return clonedFields;
}

function adaptTemplateToAffectedFieldsLayout(
  clonedLayoutRows: string[][],
  layoutOperations: LayoutOperation[] | undefined,
): string[][] {
  if (!layoutOperations) {
    return clonedLayoutRows;
  }
  layoutOperations.forEach((layoutOperation) => {
    const { operation, parameters } = layoutOperation;
    if (operation === 'setLayoutOrder') {
      if (!parameters || parameters.length !== 2) {
        console.error('DynamicForm.adaptTemplateToAffectedFieldsLayout - AF template has a field with affectedFieldsOnChangeExtended that has incorrect number of arguments for "swap" layoutOperation');
        return;
      }
      setLayoutOrder(clonedLayoutRows, parameters[0], parameters[1]);
    }
  });
  return clonedLayoutRows;
}

function setLayoutOrder(
  clonedLayoutRows: string[][],
  field1Name: string,
  field2Name: string,
):string[][] {
  if (!clonedLayoutRows) {
    return clonedLayoutRows;
  }

  let field1Index:number[] = [];
  let field2Index:number[] = [];
  clonedLayoutRows.find((row, rowIndex) => {
    row.forEach((fieldName, colIndex) => {
      if (fieldName === field1Name) {
        field1Index = [rowIndex, colIndex];
      } else if (fieldName === field2Name) {
        field2Index = [rowIndex, colIndex];
      }
    });
    if (field1Index.length !== 0 && field2Index.length !== 0) {
      return true;
    }
    return false;
  });

  // Only reorder when both field names are found.
  if (field1Index.length > 0 && field2Index.length > 0) {
    let beforeField = field1Index;
    let afterField = field2Index;
    if (field1Index[0] > field2Index[0] || (field1Index[0] === field2Index[0] && field1Index[1] > field2Index[1])) {
      beforeField = field2Index;
      afterField = field1Index;
    }
    clonedLayoutRows[beforeField[0]][beforeField[1]] = field1Name;
    clonedLayoutRows[afterField[0]][afterField[1]] = field2Name;
  } else if (field1Index.length === 0) {
    console.error(`DynamicForm.setLayoutOrder ${field1Name} not found `);
  } else if (field2Index.length === 0) {
    console.error(`DynamicForm.setLayoutOrder ${field2Name} not found`);
  }

  return clonedLayoutRows;
}

function findFieldsWithAffectedFieldsOnChangeExtended(
  fields: FormTemplateFieldMap,
) : [string, FormTemplateField][] {
  const formTemplateFieldMap = Object.entries(fields).filter(([_, fieldDefinition]) => Object.keys(fieldDefinition).includes('affectedFieldsOnChangeExtended'));

  return formTemplateFieldMap;
}
