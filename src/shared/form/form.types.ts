import { FormTemplateField } from 'data/template/template.types';
import { Position } from '@blueprintjs/core';

export type DynamicFormFieldMap = {
  [fieldName: string]: DynamicFormField
}

export type NameValueMap = {
  [fieldName: string]: any
}

export interface DynamicFormField extends FormTemplateField {
  value: string | boolean | string[],
  originalValue?: string,
  validations: {
    touched?: boolean,
    valid?: boolean,
    errorMessage?: string,
  },
  dirty?: boolean,
  position?: Position,
  htmlId?: string,
}
