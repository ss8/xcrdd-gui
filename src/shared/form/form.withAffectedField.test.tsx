import React from 'react';
import {
  render,
  fireEvent,
  screen,
} from '@testing-library/react';
import { getByDataTest } from '__test__/customQueries';
import { FormTemplateSection } from 'data/template/template.types';
import Form from '.';
import { fieldWithAffectedFieldOnChangeExtendedForm } from './form.withAffectedField.testData';

describe('<Form/> template has affectedFieldsOnChangeExtended', () => {
  let formData: { [key:string] : string | boolean | undefined};

  beforeEach(() => {
    formData = {
      transport: 'HTTP',
      enableKpis: true,
      uri: 'https://abc.com',
    };
  });

  it('should render form and validate form properly when select field has affectedFieldsOnChangeExtended and its default value is undefined', () => {
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    formData.transport = undefined;

    render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

  });

  it('should render form and validate form properly when select field has affectedFieldsOnChangeExtended and default data is same as initial value', () => {
    formData.transport = 'HTTP';
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    const { container } = render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

    fireEvent.change(getByDataTest(container, 'input-uri'), { target: { value: '&&&' } });
    expect(screen.getByText('Must be a valid URL. Please re-enter.')).toBeVisible();

  });

  it('should render form properly when select field has affectedFieldsOnChangeExtended and its default value not the same as initial', () => {
    formData.transport = 'TCP';
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Enable KPIs')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Delivery URI')).toBeNull();
  });

  it('should render form and set validation properly when select field has affectedFieldsOnChangeExtended and its value changed', () => {
    formData.transport = 'HTTP';
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    const { container } = render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

    fireEvent.change(getByDataTest(container, 'input-transport'), { target: { value: 'E164' } });
    expect(screen.getByLabelText('Delivery E164 Phone Number:*')).toBeVisible();

    fireEvent.change(getByDataTest(container, 'input-uri'), { target: { value: '&&&' } });
    expect(screen.getByText('Please enter a valid E.164 phone number.')).toBeVisible();

    fireEvent.change(getByDataTest(container, 'input-transport'), { target: { value: 'HTTP' } });
    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();

  });

  it('should render form properly when template has no affectedFieldsOnChangeExtended', () => {
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    formTemplateSection.fields.transport.affectedFieldsOnChangeExtended = undefined;
    const { container } = render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

    fireEvent.change(getByDataTest(container, 'input-uri'), { target: { value: '&&&' } });
    expect(screen.getByText('Must be a valid URL. Please re-enter.')).toBeVisible();
  });

  it('should render form and set validation properly when template has no affectedFieldsOnChangeExtended and its value changed', () => {
    const formTemplateSection = fieldWithAffectedFieldOnChangeExtendedForm.selectField as FormTemplateSection;
    formTemplateSection.fields.transport.affectedFieldsOnChangeExtended = undefined;
    const { container } = render(
      <Form
        name={fieldWithAffectedFieldOnChangeExtendedForm.name}
        defaults={formData}
        fields={formTemplateSection.fields}
        layout={formTemplateSection.metadata.layout?.rows}
      />,
    );

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

    // changing value has no impact to the other fields
    fireEvent.change(getByDataTest(container, 'input-transport'), { target: { value: 'E164' } });

    expect(screen.getByLabelText('Delivery URI:')).toBeVisible();
    expect(screen.getByLabelText('Delivery Type:')).toBeVisible();
    expect(screen.queryByLabelText('Delivery E164 Phone Number')).toBeNull();
    expect(screen.queryByLabelText('Enable KPIs')).toBeNull();

  });
});
