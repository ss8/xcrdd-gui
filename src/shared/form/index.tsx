import React, { Fragment } from 'react';
import './form.scss';
import { CollapsibleSection, FormTemplateValidator } from 'data/template/template.types';
import { Typeahead } from './custom/typeahead'; // eslint-disable-line
import { renderFields } from './form-renderer';
import { MultiselectTypeahead } from './custom/multiselectTypeahead';
import { checkValid } from './form-validation';
import CollapsibleForm from '../collapse';
import { MessageType } from '../collapse/MessageLabel.type';
import { TextSuggestType } from './custom/TextSuggest/TextSuggest';
import { adaptFormTemplate, adaptFormTemplateState } from './form.adapter';
import { NameValueMap } from './form.types';

export interface ChangedField {
  name:string, // field key
  label?:string, // field label
  value:any, // field value
  // the label to the field value.  For example, you may need to send CC to server,
  // but on the GUI we need to translate CC to a meaningful label.
  valueLabel?:any,
}

export const EMPTY_FIELD = '$empty';

let nextHtmlId = 0;
function getNextHtmlId(name: string): string {
  nextHtmlId += 1;

  return `input-${name}-${nextHtmlId}`;
}

export const initializeFields = (fields: any = {}, defaults: any, originalValues : any) => {
  const fieldNames = Object.keys(fields);
  if (!fieldNames.length) {
    return {};
  }

  const newFields: any = {};

  fieldNames.forEach((fieldName: any) => {
    const {
      name,
      initial,
      hide = 'false',
      timePrecision = null,
      position = null,
      affectedFieldsOnChangeExtended,
      options,
      label,
      validation,
    } = fields[fieldName];

    newFields[fieldName] = {};

    if (defaults && defaults[fieldName] != null) {
      newFields[fieldName].value = defaults[fieldName];
    } else {
      newFields[fieldName].value = initial;
    }

    if (originalValues && originalValues[fieldName]) {
      newFields[fieldName].originalValue = originalValues[fieldName];
    } else {
      newFields[fieldName].originalValue = null;
    }

    if ((fields[fieldName].type === 'select' || fields[fieldName].type === 'multiselectTypeahead')
    && fields[fieldName].options && fields[fieldName].options.length === 1
    && fields[fieldName].options[0].value !== ''
    && fields[fieldName].options[0].value !== 'undefined'
    ) {
      newFields[fieldName].value = fields[fieldName].options[0].value;
    }

    newFields[fieldName].validations = {};
    newFields[fieldName].validations.touched = false;
    newFields[fieldName].hide = hide;
    newFields[fieldName].dirty = false;
    newFields[fieldName].timePrecision = timePrecision;
    newFields[fieldName].position = position;
    newFields[fieldName].htmlId = getNextHtmlId(fieldName);
    newFields[fieldName].affectedFieldsOnChangeExtended = affectedFieldsOnChangeExtended;
    newFields[fieldName].options = options;
    newFields[fieldName].label = label;
    newFields[fieldName].validation = validation;
  });

  return newFields;
};

export type FieldUpdateHandlerParam = {
  name: any,
  value: any,
  label: any,
  valueLabel: any,
}

export type CustomValidationFunction = (
  propFields?: Props,
  stateFields?: State,
  value?: string,
  lastSavedValue?: string,
  validator?: FormTemplateValidator,
  form?: Form,
) => { isValid: boolean, errorMessage: string }

export type CustomFormFieldComponent = (props: {value: any, label?:string})=> JSX.Element;

type Props = {
  name: string,
  fields: any,
  layout: any,
  collapsibleLayout?: CollapsibleSection
  defaults?: NameValueMap,
  originalValues?: any,
  shouldValidateOnRender?: boolean
  fieldUpdateHandler?: (param: FieldUpdateHandlerParam) => void,
  customValidationFunctionMap?: { [key: string]: CustomValidationFunction }
  flexFlow?: string, // Used in form-renderer - code refactor required
  cssClassName?: string, // Used in form-renderer - code refactor required
  default?: any, // Not sure where used - code refactor required
  colWidth?: any, // Not sure where used - code refactor required
  onSubmit?: any, // Not sure where used - code refactor required
  currentStateMode?: any, // Not sure where used - code refactor required
  parentSize?: any // Not sure where used - code refactor required
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent }
  rightElementHandler?:any // Used in rdhi-server to toggle between show and hide password - code reactor required
}

type State = {
  name: string,
  fields: any,
  layoutRows: string[][],
  collapsibleFormMessage: string | null,
}

class Form extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(props.fields, props.layout, props.defaults);
    this.state = {
      name: props.name,
      layoutRows: adaptedLayoutRows,
      fields: initializeFields(adaptedFields, props.defaults, props.originalValues),
      collapsibleFormMessage: null,
    };
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.name === state.name) return null;
    // if the name of the template is the same, leave state alone
    const { adaptedFields, adaptedLayoutRows } = adaptFormTemplate(props.fields, props.layout, props.defaults);
    return {
      name: props.name,
      layoutRows: adaptedLayoutRows,
      fields: initializeFields(adaptedFields, props.defaults, props.originalValues),
    };
  }

  componentDidUpdate(prevProps: Props): void {
    let updateStateFlag = false;
    const fieldNames = Object.keys(this.props.fields);
    if (!fieldNames.length) return;
    const newFields = { ...this.state.fields };
    fieldNames.forEach((fieldName: any) => {
      const { fields } = this.props;
      if ((fields[fieldName].type === 'select' || fields[fieldName].type === 'multiselectTypeahead')
          && fields[fieldName].options && fields[fieldName].options.length === 1
          && fields[fieldName].options[0].value !== ''
          && fields[fieldName].options[0].value !== 'undefined'
          && this.state.fields[fieldName].value !== fields[fieldName].options[0].value
      ) {
        newFields[fieldName].value = fields[fieldName].options[0].value;
        updateStateFlag = true;
      }
    });

    if (updateStateFlag) {
      this.setState({ fields: newFields }, () => {
        this.checkIfShouldValidateOnRender();
      });

    } else if (this.props.name !== prevProps.name) {
      this.checkIfShouldValidateOnRender();
    }
  }

  componentDidMount(): void {
    this.checkIfShouldValidateOnRender();
  }

  private checkIfShouldValidateOnRender(): void {
    const {
      shouldValidateOnRender,
    } = this.props;

    if (shouldValidateOnRender) {
      this.forceCheckValidAndTouchAllFields();
    }
  }

  /**
   * validation is forced in creation scenario when required fields are empty
   * and user has not touched the fields.
   */
  private forceCheckValidAndTouchAllFields = (forceTouched = false) => {
    let retIsValid = true;
    const newFields = { ...this.state.fields };
    Object.keys(this.state.fields).forEach((key: string) => {
      // if forceTouched is false, skip the fields that is already validated/touched
      if (!(this.state.fields[key].validations.touched === true && forceTouched === false)) {
        const validationResult = checkValid(this, key, newFields[key].value, forceTouched);
        const { isValid } = validationResult;
        newFields[key].validations.touched = true;
        newFields[key].validations.valid = isValid;
        newFields[key].validations.errorMessage = validationResult.errorMessage;
      }
      retIsValid = retIsValid && newFields[key].validations.valid;
    });

    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
    return retIsValid;
  }

  getCollapsibleFormMessage = (newFields:any) : string | null => {
    const { collapsibleLayout } = this.props;

    if (!collapsibleLayout) {
      return null;
    }

    const hasNotValid = (collapsibleLayout.rows.some((row) => {
      return row.filter((fieldName) => fieldName !== EMPTY_FIELD)
        .some((fieldName) => (newFields[fieldName].validations.valid === false));
    }));

    if (hasNotValid) {
      return 'Review values';
    }
    return null;
  }

  getValueLabel = (name:string, value:string|string[]|boolean) => {
    if (this.props.fields[name].options) {
      if (Array.isArray(value) === false) {
        value = [value as string];
      }
      const labels:string[] = [];
      (value as Array<string>).forEach((v) => {
        const option = this.props.fields[name].options.find(
          (element:any) => element.value === v,
        );
        if (option && option.label) {
          labels.push(option.label);
        }
      });
      return labels.join(', ');
    }
    return value;
  }

  /**
   * handleChange does the following:
   * - check to make sure the value is valid
   * - notify listener about field changed.
   */
  handleChange = (e: any) => {
    const { type, value, name } = e.currentTarget;
    const { adaptedFields, adaptedLayoutRows } = adaptFormTemplateState(this.state.fields,
      this.state.layoutRows, name, value);

    const newFields = { ...adaptedFields };
    if (!type) return;
    if (type === 'checkbox') {
      newFields[name].value = !this.state.fields[name].value;
    } else {
      newFields[name].value = value;
    }
    const validationResult = checkValid(this, name, value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }

    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({
      layoutRows: adaptedLayoutRows,
      fields: newFields,
      collapsibleFormMessage: newCollapsibleFormMessage,
    });
  }

  handleClick=(e:any) => {

  }

  handleTimeZoneChange = (ctx: any, timeZone: string) => {
    const { value, name } = ctx;
    const newFields = { ...this.state.fields };
    newFields[name].value = timeZone;
    const validationResult = checkValid(this, name, value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleTypeaheadChange = (ctx: any, item: Typeahead) => {
    const { name } = ctx;
    const newFields = { ...this.state.fields };
    newFields[name].value = item.value;
    const validationResult = checkValid(this, name, item.value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleDateTimeChange = (ctx: any, value: any) => {
    const { name } = ctx;
    const newFields = { ...this.state.fields };
    newFields[name].value = value;
    const validationResult = checkValid(this, name, value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleFocus = (e: any) => {
    const { type, name } = e.currentTarget;
    const { fields } = this.state;
    if (!type) return;
    // clear the password field when it is in focus and it is not dirty
    if (type === 'password' && fields[name].dirty === false) {
      const newFields = { ...this.state.fields };
      newFields[name].value = '';
      this.setState({ fields: newFields });
    }
  }

  handleRightElement=(e:any) => {
    // e={name,value}
    this.props.rightElementHandler(e);
  }

  handleTouched = (e: any) => {
    const {
      name, type,
    } = e.currentTarget;
    let { value } = e.currentTarget;
    const newFields = { ...this.state.fields };
    // password field is cleared when it is in focus, then it is reset to original value
    // if user did not enter anything into the field.
    if (type === 'password' && newFields[name].dirty === false) {
      value = this.props.fields[name].initial;
      newFields[name].value = value;
    }
    newFields[name].validations.touched = true;
    const validationResult = checkValid(this, name, value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleMultiselectTypeaheadChange = (ctx: any, item: MultiselectTypeahead) => {
    const { name } = ctx;
    const newFields = { ...this.state.fields };
    let newValues;
    if (newFields[name].value === undefined) {
      newValues = item.value;
    } else {
      newValues = newFields[name].value.some((e: string) => e === item.label)
        ? newFields[name].value
        : [...newFields[name].value, item.value];
    }
    newFields[name].value = newValues;
    const validationResult = checkValid(this, name, item.value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleMultiselectTypeaheadItemRemove = (ctx: any, item: string) => {
    const { name } = ctx;
    const { options } = this.props.fields[name];
    const removedItem = options.find((option: any) => {
      return option.label === item;
    });
    const newFields = { ...this.state.fields };
    let newFieldsValue = newFields[name].value;
    if (newFieldsValue && !(newFieldsValue instanceof Array)) {
      newFieldsValue = [newFields[name].value];
    }
    let newValues;
    if (newFieldsValue !== undefined) {
      newValues = newFieldsValue.filter(
        (e: string) => (e !== removedItem.value),
      );
    }
    newFields[name].value = newValues;
    const validationResult = checkValid(this, name, item);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handletimeIntervalChange = (e: any) => {
    const { value, name } = e.currentTarget;
    const fieldNames = name.split('-');
    const fieldname = fieldNames[0];
    const newFields = { ...this.state.fields };
    if (fieldNames[1] === 'Hours') {
      newFields[fieldname].value = value * 60;
    } else {
      newFields[fieldname].value = parseInt(newFields[fieldname].value, 10) + parseInt(value, 10);
    }
    newFields[fieldname].dirty = true;
    this.setState({ fields: newFields });
  }

  handleKeyPress = (e: any) => {
    if (e.which === 13) { // On enter prevent default
      e.preventDefault();
    }
  }

  handleTextSuggestChange = (ctx: any, item: TextSuggestType): void => {
    const { name } = ctx;
    const newFields = { ...this.state.fields };
    newFields[name].value = item.value;
    const validationResult = checkValid(this, name, item.value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleTextSuggestQueryChange = (ctx: any, value: string): void => {
    const { name } = ctx;
    const newFields = { ...this.state.fields };
    newFields[name].value = value;
    const validationResult = checkValid(this, name, value);
    newFields[name].validations.valid = validationResult.isValid;
    newFields[name].validations.errorMessage = validationResult.errorMessage;
    newFields[name].dirty = true;
    if (this.props.fieldUpdateHandler) {
      this.props.fieldUpdateHandler({
        name,
        value: newFields[name].value,
        label: this.props.fields[name].label,
        valueLabel: this.getValueLabel(name, newFields[name].value),
      });
    }
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  handleTextSuggestFocus = () => {
    const newFields = { ...this.state.fields };
    const newCollapsibleFormMessage:string | null = this.getCollapsibleFormMessage(newFields);
    this.setState({ fields: newFields, collapsibleFormMessage: newCollapsibleFormMessage });
  }

  getTimeIntervalValue = (value: any, type: string) => {
    if (value === undefined) return 0;
    let hours = Math.floor(value / 60);
    let minutes = value % 60;
    if (value >= '1440') {
      hours = 24;
      minutes = 0;
    }
    return (type === 'Hours') ? hours : minutes;
  }

  getFieldClass = (name: string, existingClassName : string) => {
    if (!name) return existingClassName;
    const { valid, touched } = this.state.fields[name].validations;
    if (valid || !touched) return existingClassName;
    return (`invalid-field ${existingClassName}`);
  }

  setValues = (fieldValues : any) => {
    this.setState({
      fields: initializeFields(this.state.fields, fieldValues, this.props.originalValues),
    });
  }

  getValues = () => {
    const result: any = {};
    Object.keys(this.state.fields).forEach((key: string) => {
      if (typeof (this.state.fields[key].value) === 'string') {
        result[key] = this.state.fields[key].value.trim();
      } else {
        result[key] = this.state.fields[key].value;
      }
    });
    return result;
  }

  getValue = (key : any) => {
    if (this.state.fields[key]) {
      if (typeof (this.state.fields[key].value) === 'string') {
        return this.state.fields[key].value.trim();
      }
      return this.state.fields[key].value;
    }
    return null;
  }

  /**
   * validation is forced in creation scenario when required fields are empty
   * and user has not touched the fields.
   */
  isValid = (forceValidation = false, forceTouched = true) => {
    if (forceValidation) {
      return this.forceCheckValidAndTouchAllFields(forceTouched);
    }
    return Object.keys(this.state.fields).every((key: string) => {
      if (this.state.fields[key].validations.valid === false) {
        return false;
      }
      return true;
    });
  }

  renderCollapsibleLayout(): JSX.Element | null {
    const { collapsibleLayout } = this.props;
    const { collapsibleFormMessage } = this.state;

    if (!collapsibleLayout) {
      return null;
    }

    const { label, rows } = collapsibleLayout;

    return (
      <CollapsibleForm title={label} message={collapsibleFormMessage} messageType={MessageType.Error}>
        {rows.map((row: string[], i: number) => (
          <div key={i}>
            {renderFields(this, row)}
          </div>
        ))}
      </CollapsibleForm>
    );
  }

  render(): JSX.Element | null {
    const { layoutRows } = this.state;
    if (!this.state.fields || !layoutRows || !layoutRows.length) {
      throw Error('No fields or layout to render. Please check documentation.');
    }
    return (
      <Fragment>
        {layoutRows.map((row: string[], i: number) => {
          return (
            <div key={i}>
              {renderFields(this, row)}
            </div>
          );
        })}
        {this.renderCollapsibleLayout()}
      </Fragment>
    );
  }
}

export default Form;
