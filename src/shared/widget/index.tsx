import React from 'react';
import { Card, Elevation } from '@blueprintjs/core';
import './widget.scss';
import Chart from '../chart';

const Widget = () => (
  <div className="widget-container">
    <Card style={{ height: 200 }} interactive elevation={Elevation.TWO} className="widget">
      <h5><a href="#">Card heading</a></h5>
      <Chart />
    </Card>
  </div>
);

export default Widget;
