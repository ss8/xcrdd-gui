import React from 'react';
import {
  Button, Dialog, Classes, IconName, MaybeElement,
} from '@blueprintjs/core';
import './config-form-popup.scss';

export enum CONFIG_FORM_MODE {
  Edit = 'edit',
  View = 'view',
  Create = 'create',
}

export interface IConfigFormPopupProperties {
  currentStateMode: CONFIG_FORM_MODE,
  hasPrivilegesToEdit: boolean,
  isFirstChildLeftOfEditButton: boolean,
  onStateModeChanged?: any, // would be nice to specify the type
  onClose: any,
  onSubmit: any,
  getTitle?: any,
  fieldDirty: any,
  isOpen?: any,
  width?: any,
  height?: any,
  minWidth?: any,
  maxWidth?: any,
  overflow?: any,
  minHeight?: any,
  doNotDisplayEdit?: boolean,
  className?: string
}

export interface IConfigFormPopupChildProperties {
  setReadOnly: any,
}

export default class ConfigFormPopup extends React.Component<IConfigFormPopupProperties, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
      currentStateMode: this.props.currentStateMode,
      title: (this.props.getTitle) ? this.props.getTitle(props.currentStateMode) : 'Dialog',
    };
  }

  public static defaultProps = {
    width: '100%',
    height: 'auto',
    overflow: 'auto',
    minWidth: '300px',
    maxWidth: '100%',
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps:any) {
    if (nextProps.isOpen !== undefined && this.props.isOpen !== nextProps.isOpen) {
      this.setState({
        isOpen: nextProps.isOpen,
        currentStateMode: nextProps.currentStateMode,
        title: (this.props.getTitle) ? this.props.getTitle(nextProps.currentStateMode) : 'Dialog',
      });
    }
  }

  static submitDialogLabel = (isInProcessingMode: boolean, mode:string):string => {
    if (mode === CONFIG_FORM_MODE.View) {
      return 'Close';
    } if (isInProcessingMode) {
      return 'Cancel';
    }

    return 'Ok';
  }

  static submitDialogIcon = (isInProcessingMode: boolean, mode:string):IconName | MaybeElement => {
    if (mode === CONFIG_FORM_MODE.View) {
      return false;
    } if (isInProcessingMode) {
      return 'cross';
    }
    return 'tick';
  }

  onClose = (event: any) => {
    if (this.props.onClose) {
      this.props.onClose(event);
    }
  }

  onEditMode = () => {
    this.setState({
      currentStateMode: CONFIG_FORM_MODE.Edit,
      title: (this.props.getTitle) ? this.props.getTitle(CONFIG_FORM_MODE.Edit) : 'Dialog',
    });

    if (this.props.onStateModeChanged) {
      this.props.onStateModeChanged(CONFIG_FORM_MODE.Edit);
    }
  }

  returnRef = (node:any) => {
    return node;
  }

  render() {
    const {
      children,
      className,
      doNotDisplayEdit,
      fieldDirty,
      hasPrivilegesToEdit,
      height,
      isFirstChildLeftOfEditButton,
      maxWidth,
      minWidth,
      onClose,
      onSubmit,
      overflow,
      width,
    } = this.props;
    const { currentStateMode } = this.state;

    const isInProcessingMode = ((
      currentStateMode === CONFIG_FORM_MODE.Edit ||
      currentStateMode === CONFIG_FORM_MODE.Create
    ) && fieldDirty);
    const childrenArray = React.Children.toArray(children);
    let mainContent = children;
    let topItem = null;
    if (isFirstChildLeftOfEditButton && childrenArray.length > 0) {
      // eslint-disable-next-line prefer-destructuring
      topItem = childrenArray[0];
      mainContent = childrenArray.slice(1, childrenArray.length);
    }
    return (
      <Dialog
        onClose={onClose}
        title={this.state.title}
        isOpen={this.state.isOpen}
        canEscapeKeyClose={false}
        canOutsideClickClose={false}
        className={className}
        style={{
          width,
          height,
          minWidth,
          maxWidth,
          overflow,
        }}
      >

        <div className="config-form-header" style={{ paddingRight: '20px' }} data-testid="ConfigFormHeader">
          <div className="config-form-header-leftside">
            { (topItem !== null) ? topItem : null }
          </div>
          {(currentStateMode === CONFIG_FORM_MODE.View &&
            hasPrivilegesToEdit && !doNotDisplayEdit) ?
              <Button data-test="configFormPopup-Edit" className="actionButton" rightIcon="edit" onClick={this.onEditMode} intent="none" text="Edit" /> : ''}
        </div>
        <div className="config-form-body" data-testid="ConfigFormBody">
          {mainContent}
        </div>
        <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px' }} data-testid="ConfigFormFooter">
          <Button
            data-test="configFormPopup-CancelOrOk"
            className="actionButton"
            rightIcon={ConfigFormPopup.submitDialogIcon(isInProcessingMode, currentStateMode)}
            onClick={this.onClose}
            intent={isInProcessingMode ? 'none' : 'primary'}
            text={ConfigFormPopup.submitDialogLabel(isInProcessingMode, currentStateMode)}
          />
          { isInProcessingMode && (
            <Button
              data-test="configFormPopup-Save"
              className="actionButton"
              rightIcon="arrow-right"
              onClick={onSubmit}
              intent="primary"
              active
              text="Save"
            />
          )}
        </div>
      </Dialog>
    );
  }
}
