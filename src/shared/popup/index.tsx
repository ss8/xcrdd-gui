import React from 'react';
import { Button, Dialog, Classes } from '@blueprintjs/core';

function withDialog(WrappedComponent: React.ComponentClass, config : any = {}) {
  return class WithDialog extends React.Component<any, any> {
    handleClose = () => {
      if (!this.props.history) return;
      this.props.history.goBack();
    }

    render() {
      const { title = 'Dialog' } = config;
      return (
        <Dialog
          icon="info-sign"
          onClose={this.handleClose}
          title={title}
          isOpen
          style={{ width: config.width }}
        >
          <div className={Classes.DIALOG_BODY}>
            <WrappedComponent {...this.props} />
          </div>
          <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px' }}>
            <Button onClick={this.handleClose} intent="none" text="Cancel" />
          </div>
        </Dialog>
      );
    }
  };
}

// eslint-disable-next-line
export { withDialog };
