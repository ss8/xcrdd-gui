import React from 'react';
import { Button, Dialog, Classes } from '@blueprintjs/core';
import { isOpen } from '@blueprintjs/core/lib/esm/components/context-menu/contextMenu';

export default class PopUp extends React.Component<any, any> {
  render() {
    const {
      title = 'Dialog', width = '300px', height = '100%', minWidth = '300px', maxWidth = '910px', overflow = 'none', onSubmit,
    } = this.props;
    return (
      <Dialog
        onClose={this.props.handleClose}
        title={title}
        isOpen={this.props.isOpen}
        canEscapeKeyClose={false}
        canOutsideClickClose={false}
        style={{
          width, height, minWidth, maxWidth, overflow,
        }}
      >
        <div className={Classes.DIALOG_BODY}>
          {this.props.children}
        </div>
        <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px' }}>
          <Button
            data-test="popup-button-cancel"
            className="actionButton"
            rightIcon="cross"
            onClick={this.props.handleClose}
            intent="none"
            text="Cancel"
          />
          <Button
            data-test="popup-button-withlabel"
            className="actionButton"
            rightIcon="arrow-right"
            onClick={onSubmit}
            intent="primary"
            active
            text="Save"
          />
        </div>
      </Dialog>
    );
  }
}
