import checkPropTypes from 'check-prop-types';
import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import reduxThunkMiddleware from 'redux-thunk';
import rootReducer from 'data/root.reducers';

/**
 * Create a testing store with imported reducers, middleware, and initial state.
 *  globals: rootReducer, middlewares.
 * @param {object} initialState - Initial state for store.
 * @function storeFactory
 * @returns {Store} - Redux store.
 */
export const storeFactory = (initialState) => {
  const middlewares = [reduxThunkMiddleware, promiseMiddleware];
  const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
  return createStoreWithMiddleware(rootReducer, initialState);
};

/**
 * Return node(s) with the given data-test attribute.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper.
 * @param {string} val - Value of data-test attribute for search.
 * @returns {ShallowWrapper}
 */
export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

export const checkProps = (component, conformingProps) => {
  const propError = checkPropTypes(
    component.propTypes,
    conformingProps,
    'prop',
    component.name,
  );
  expect(propError).toBeUndefined();
};
/**
 * Find the specific request in moxios.requests
 * @param {moxios.Tracker} requests
 * @param {string} url
 */
export const findInMockHttpRequests = (requests, url) => {
  const result = [];
  for (let i = 0; i < requests.count(); i += 1) {
    const request = requests.at(i);
    if (request.url === url) {
      result.push(request);
    }
  }
  return result;
};
