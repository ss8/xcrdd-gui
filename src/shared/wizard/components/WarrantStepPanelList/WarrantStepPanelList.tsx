import React, { FC } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Chip from '@material-ui/core/Chip';
import SubjectIcon from '@material-ui/icons/Subject';
import { makeStyles } from '@material-ui/core';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { CaseModel } from 'data/case/case.types';
import { TEMPORARY_CASE_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';

type Props = {
    dataArray?: any[];
    onSubStep?: (subStep:number) => void;
    subStep?: number,
    mode?: string
  }

const useStyles = makeStyles({
  ListItem: {
    borderRadius: '3px',
    color: '#182026',
    height: '40px',
    marginBottom: '10px',
    paddingLeft: '10px',
    paddingRight: '10px',
    '&.Mui-selected': {
      color: '#106BA3',
      backgroundColor: 'rgba(183, 228, 255, 0.6)',
    },
  },
  ListItemIcon: {
    color: 'inherit',
    minWidth: '30px',
  },
  ListItemText: {
    'white-space': 'nowrap',
    overflow: 'hidden',
    'text-overflow': 'ellipsis',

    '&.MuiTypography-body1': {
      fontFamily: 'Titillium Web, sans-serif',
    },
  },
  NewChip: {
    borderRadius: '3px',
    backgroundColor: '#0F9960',
    color: '#ffffff',
    fontSize: '10px',
    textTransform: 'uppercase',
    height: '18px',
    '& .MuiChip-labelSmall': {
      paddingLeft: '4px',
      paddingRight: '4px',
    },
  },
});

const WarrantStepPanelList: FC<Props> = ({
  dataArray,
  subStep,
  onSubStep,
  mode,
}: Props) => {
  const classes = useStyles();

  const isEditMode = (): boolean => {
    return mode === CONFIG_FORM_MODE.Edit;
  };

  const isNewDevice = (warrantCase: CaseModel): boolean => {
    return warrantCase.id.startsWith(TEMPORARY_CASE_ID_PREFIX) ?? true;
  };

  if (!dataArray) {
    return null;
  }

  const renderListItem = (data: any, i: number) => {
    const itemContent = `Case ${i + 1}`;

    if (data.hideFromView) {
      return null;
    }

    return (
      <ListItem
        className={classes.ListItem}
        key={i}
        button
        selected={subStep === (i + 1)}
        onClick={() => (onSubStep ? onSubStep(i + 1) : null)}
      >
        <ListItemIcon className={classes.ListItemIcon}>
          <SubjectIcon />
        </ListItemIcon>
        <ListItemText
          className={classes.ListItemText}
          primary={itemContent}
          title={itemContent}
        />
        { isEditMode() && isNewDevice(data) && (
          <Chip className={classes.NewChip} size="small" label="new" />
        )}
      </ListItem>
    );
  };

  return (
    <div className="listMenuStyle" data-testid="WarrantStepPanelList">
      <List>
        {dataArray.map(renderListItem)}
      </List>
    </div>
  );
};

export default WarrantStepPanelList;
