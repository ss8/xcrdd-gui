import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { TEMPORARY_CASE_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import WarrantStepPanelList from './WarrantStepPanelList';

describe('<WarrantStepPanelList />', () => {
  it('should render an empty list if data is empty', () => {
    const { container } = render(<WarrantStepPanelList />);

    expect(container).toMatchInlineSnapshot('<div />');
  });

  it('should render two cases', () => {
    render(
      <WarrantStepPanelList
        dataArray={[{ id: '1' }, { id: '2' }]}
      />,
    );

    expect(screen.getByText('Case 1')).toBeVisible();
    expect(screen.getByText('Case 2')).toBeVisible();
  });

  it('should not render the case that should be hidden', () => {
    render(
      <WarrantStepPanelList
        dataArray={[{ id: '1' }, { id: '2', hideFromView: true }]}
      />,
    );

    expect(screen.getByText('Case 1')).toBeVisible();
    expect(screen.queryByText('Case 2')).toBeNull();
  });

  it('should show the new indication if edit mode and case is new', () => {
    render(
      <WarrantStepPanelList
        dataArray={[{ id: '1' }, { id: `${TEMPORARY_CASE_ID_PREFIX}2` }]}
        mode={CONFIG_FORM_MODE.Edit}
      />,
    );

    expect(screen.getByText('Case 1')).toBeVisible();
    expect(screen.getByText('Case 2')).toBeVisible();
    expect(screen.getByText('new')).toBeVisible();
  });

  it('should call onSubStep when clicking in an option', () => {
    const onSubStep = jest.fn();

    render(
      <WarrantStepPanelList
        dataArray={[{ id: '1' }, { id: '2' }]}
        mode={CONFIG_FORM_MODE.Edit}
        onSubStep={onSubStep}
      />,
    );

    fireEvent.click(screen.getByText('Case 2'));

    expect(onSubStep).toHaveBeenCalledWith(2);
  });
});
