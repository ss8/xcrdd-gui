import React from 'react';
import {
  Button,
} from '@blueprintjs/core';
import './prog-tracker.scss';
import './wizardStyle.scss';
import { FormLayout, FormLayoutContent, FormLayoutFooter } from 'components/FormLayout';
import FormPanel from 'components/FormPanel';
import { FormLayoutFixedHeader } from 'components/FormLayout/FormLayout';

export enum PROGRESS_STATUS {
  AbortedCanModify, AbortedMustExit, InProgress, Successful, FailedCanModify, FailedMustExit
}

export interface DiscoveryWizardProps {
  onNext: () => void,
  onBack: () => void,
  onCancel: () => void,
  onSubmit: () => void,
  submitButtonLabel?: string,
  getTitle: (step:number) => string,
  currentStep: number,
  hasNextStep: (step:number) => boolean,
  isSummaryStep: (step:number) => boolean,
  isProgressStep: (step:number) => boolean,
  isOpen?: boolean,
  progressStatus: PROGRESS_STATUS,
  jumpToStep?: (evt:any) => void,
}

interface DiscoveryWizardState {
}

const getNavStyles = (indx: number, length: number) => {
  const styles = [];
  for (let i = 1; i <= length; i += 1) {
    if (i < indx) {
      styles.push('done');
    } else if (i === indx) {
      styles.push('doing');
    } else {
      styles.push('todo');
    }
  }
  return styles;
};

class DiscoveryWizard extends React.Component<DiscoveryWizardProps, DiscoveryWizardState> {
  renderSteps = (currentStep:number) => {
    // DiscoveryWizard steps will display textLabel only when
    // children component has stepTextLabel props
    const childrenDisplayingStep = (this.props.children as Array<any>).filter(
      (child:any) => child && child.props.stepTextLabel && child.props.stepTextLabel.length > 0,
    );
    const totalStep:number = childrenDisplayingStep.length;
    const styles:string[] = getNavStyles(currentStep, totalStep);
    return (
      <ol className="warrant-progress">
        {childrenDisplayingStep.map((step, i) => (
          <li
            className={`warrant-progress-step warrant-progress-${styles[i]} `}
            onClick={this.props.jumpToStep}
            key={i}
            value={i}
            style={this.props.jumpToStep ? { cursor: 'pointer' } : { cursor: 'default' }}
          >
            <span className="warrant-progress-step-label">{step.props.stepTextLabel ? step.props.stepTextLabel : `Step ${i + 1}`}</span>
            <span className="warrant-progress-step-counter">{i + 1}</span>
          </li>
        ))}
      </ol>
    );
  }

  showCancel = (currentStep: number, progressStatus: PROGRESS_STATUS, onCancel: () => void) => {
    if (!onCancel) return false;

    const isProgressStep:boolean = this.props.isProgressStep(currentStep);

    if (isProgressStep && progressStatus === PROGRESS_STATUS.InProgress) return false;

    if (isProgressStep && progressStatus === PROGRESS_STATUS.Successful) return false;

    if (isProgressStep && progressStatus === PROGRESS_STATUS.AbortedMustExit) return false;

    if (isProgressStep && progressStatus === PROGRESS_STATUS.FailedMustExit) return false;

    return true;
  }

  showBack = (currentStep: number, progressStatus: PROGRESS_STATUS, onBack: () => void) => {
    if (!onBack) return false;
    const isProgressStep:boolean = this.props.isProgressStep(currentStep);

    if (isProgressStep && progressStatus === PROGRESS_STATUS.AbortedCanModify) return true;

    if (isProgressStep && progressStatus === PROGRESS_STATUS.FailedCanModify) return true;

    if (currentStep > 1 && isProgressStep === false) return true;

    return false;
  }

  renderFormPanel(formTitle: string | null = null): JSX.Element {
    const {
      currentStep,
      progressStatus,
      onCancel,
      onBack,
      onNext,
      children,
      hasNextStep,
      onSubmit,
      isSummaryStep,
      submitButtonLabel,
    } = this.props;

    return (
      <FormPanel title={formTitle}>
        {this.showCancel(currentStep, progressStatus, onCancel) && (
          <Button
            onClick={onCancel}
            className="actionButton"
            text="Cancel"
          />
        )}
        {this.showBack(currentStep, progressStatus, onBack) && (
          <Button
            onClick={onBack}
            className="actionButton"
            text="Back"
          />
        )}
        {(onNext !== undefined && children && hasNextStep(currentStep)) && (
          <Button
            onClick={onNext}
            className="actionButton"
            active
            intent="primary"
            text="Next"
          />
        )}
        {(onSubmit !== undefined && children && isSummaryStep(currentStep)) && (
          <Button
            onClick={onSubmit}
            className="actionButton"
            active
            intent="primary"
            text={submitButtonLabel || 'Submit'}
          />
        )}
      </FormPanel>
    );
  }

  render(): JSX.Element {
    const {
      currentStep,
      children,
    } = this.props;

    return (
      <FormLayout>
        <FormLayoutContent paddingTop="124px">
          <FormLayoutFixedHeader height="142px">
            {this.renderFormPanel('Create Warrant Wizard')}
            {this.renderSteps(currentStep)}
          </FormLayoutFixedHeader>
          {children}
        </FormLayoutContent>
        <FormLayoutFooter>
          {this.renderFormPanel(null)}
        </FormLayoutFooter>
      </FormLayout>
    );
  }
}

export default DiscoveryWizard;
