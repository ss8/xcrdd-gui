import React from 'react';
import WarrantStepPanelList from './components/WarrantStepPanelList';

export interface FormPanel {
  // validates entries in this panel.  Return true if entries are valid, otherwise false
  // errors is a list of error messages to be displayed by the containing wizard
  isValid?: (errors: string[]) => boolean;
  // saves entries to redux store on sub step. Return true if entries are saved successfully,
  // otherwise false.
  submitSubStep?: () => Promise<boolean>;
  // saves entries to redux store. Return true if entries are saved successfully, otherwise false.
  submit?: (shouldShowErrors: boolean) => Promise<boolean>;
}

interface WithDiscoveryWizardStepProps {
  show: boolean;
  stepTextLabel?: string;
  dataArray?: any[];
  onSubStep?: (subStep:number) => void;
  subStep?: number,
}

// eslint-disable-next-line max-len
export const withDiscoveryWizardStepPanel = <P extends Record<string, unknown>>(WrappedComponent: React.ComponentType<P>) =>
// eslint-disable-next-line max-len
  class WithDiscoveryWizardStepPanel extends React.Component<P & WithDiscoveryWizardStepProps> {
    wrappedComponent:FormPanel|null = null;

    isValid = (errors: string[]) =>
      (this.wrappedComponent && this.wrappedComponent.isValid ?
        this.wrappedComponent.isValid(errors) : true);

    submit = async (shouldShowErrors: true) => new Promise<boolean>((res, rej) =>
      (this.wrappedComponent && this.wrappedComponent.submit ?
        res(this.wrappedComponent.submit(shouldShowErrors)) : res(true)));

    submitSubStep = async () => new Promise<boolean>((res, rej) =>
      (this.wrappedComponent && this.wrappedComponent.submitSubStep ?
        res(this.wrappedComponent.submitSubStep()) : res(true)));

    hasMultipleEntries() {
      const {
        dataArray = [],
      } = this.props;

      const visibleDataArray = dataArray.filter(({ hideFromView }) => !hideFromView);
      return visibleDataArray.length > 1;
    }

    renderListMenu() {
      if (!this.hasMultipleEntries()) {
        return null;
      }

      return (
        <WarrantStepPanelList
          dataArray={this.props.dataArray}
          subStep={this.props.subStep}
          onSubStep={this.props.onSubStep}
        />
      );
    }

    render() {
      const { show } = this.props;

      if (!show) {
        return null;
      }

      let className = 'wizardChildPanelStyle';
      if (this.hasMultipleEntries()) {
        className += ' multiple-entries';
      }

      return (
        <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
          {this.renderListMenu()}
          <div className={className}>
            <WrappedComponent
              ref={(ref:FormPanel) => { this.wrappedComponent = ref; }}
              {...this.props}
            />
          </div>
        </div>
      );
    }
  };
