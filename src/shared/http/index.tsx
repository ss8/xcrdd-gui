import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { RENEW_AUTHENTICATION_TIMEOUT } from 'data/authentication/authentication.types';
import { AUTH, getAuth } from '../../utils';

const getEndpoint = (basePath: string) => {
  if (basePath) {
    return `${window.location.origin}/${basePath}`;
  }
  return `${window.location.origin}`;
};

class Http {
  private http: AxiosInstance;

  constructor(basePath: string) {
    this.http = axios.create({
      baseURL: getEndpoint(basePath),
    });
  }

  init(store: any) {
    this.http.interceptors.request.use((config: AxiosRequestConfig) => {
      const token = getAuth(AUTH.TOKEN);
      if (token) {
        config.headers['X-Auth-Token'] = token;
      }

      store.dispatch({ type: RENEW_AUTHENTICATION_TIMEOUT });

      return config;
    });

    this.http.interceptors.response.use(
      (response: AxiosResponse<unknown>) => response,
      (error: any) => {
        if (error?.response?.status === 401) {
          store.dispatch({ type: 'LOGOUT_FULFILLED' });
        }

        throw error;
      },
    );

    return this.http;
  }

  getHttp() { return this.http; }
}

const instance = new Http('api/v1');
Object.freeze(instance);

export default instance;
