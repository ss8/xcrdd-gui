import isObjectEqual from 'lodash.isequal';

export enum AuditService {
  WARRANTS = 'Warrants',
  CF = 'CFs',
  AF = 'AFs',
  TEAM = 'Teams',
  ROLE = 'Roles',
  USER = 'Users',
  DISTRIBUTOR_CONFIGS = 'DistributorConfigs',
  SECURITY_GATEWAY = 'SecurityGateway',
}

export enum AuditActionType {
  WARRANT_ADD = 'Warrant Add',
  WARRANT_EDIT = 'Warrant Edit',
  WARRANT_DELETE = 'Warrant Delete',
  WARRANT_RESCHEDULE = 'Warrant Reschedule',
  WARRANT_RESTORE = 'Warrant Restore',
  WARRANT_TRASH = 'Warrant Trash',
  CASE_ADD = 'Case Add',
  CASE_EDIT = 'Case Edit',
  CASE_DELETE = 'Case Delete',
  TARGET_ADD = 'Target Add',
  TARGET_EDIT = 'Target Edit',
  TARGET_DELETE = 'Target Delete',
  ACCESS_FUNCTION_ADD = 'Access Function Add',
  ACCESS_FUNCTION_EDIT = 'Access Function Edit',
  ACCESS_FUNCTION_DELETE = 'Access Function Delete',
  ACCESS_FUNCTION_AUDIT = 'Access Function Audit',
  ACCESS_FUNCTION_REFRESH = 'Access Function Refresh',
  ACCESS_FUNCTION_RESET = 'Access Function Reset',
  COLLECTION_FUNCTION_ADD = 'Collection Function Add',
  COLLECTION_FUNCTION_EDIT = 'Collection Function Edit',
  COLLECTION_FUNCTION_DELETE = 'Collection Function Delete',
  DISTRIBUTOR_CONFIGURATION_ADD = 'Distributor Configuration Add',
  DISTRIBUTOR_CONFIGURATION_EDIT = 'Distributor Configuration Edit',
  DISTRIBUTOR_CONFIGURATION_DELETE = 'Distributor Configuration Delete',
  VPNCONFIG_ADD = 'VPN Configuration Add',
  VPNCONFIG_EDIT = 'VPN Configuration Edit',
  VPNCONFIG_DELETE = 'VPN Configuration Delete',
  SECURITY_GATEWAY_ADD = 'Security Gateway Add',
  SECURITY_GATEWAY_EDIT = 'Security Gateway Edit',
  SECURITY_GATEWAY_DELETE = 'Security Gateway Delete',
  USER_ADD = 'User Add',
  USER_EDIT = 'User Edit',
  USER_DELETE = 'User Delete',
  ROLE_ADD = 'Role Add',
  ROLE_EDIT = 'Role Edit',
  ROLE_DELETE = 'Role Delete',
  TEAM_ADD = 'Team Add',
  TEAM_EDIT = 'Team Edit',
  TEAM_DELETE = 'Team Delete',
}

export const getAuditFieldInfo = (formData: any, auditData: any = null, isEdit = false,
  beforeFieldValues: any = null) => {
  const fieldInfo: any = [];
  if (formData) {
    Object.keys(formData).forEach((key: any) => {
      if (isEdit) {
        const beforeValue = beforeFieldValues?.[key];
        if (!isObjectEqual(beforeValue, formData[key])) {
          fieldInfo.push({
            field: key,
            before: beforeValue ?? '',
            after: formData[key],
          });
        }
      } else {
        fieldInfo.push({ field: key, value: formData[key] });
      }
    });
  }

  if (auditData) {
    Object.keys(auditData).forEach((key: any) => {
      fieldInfo.push({
        field: key,
        value: auditData[key],
      });
    });
  }
  return fieldInfo;
};

export const getAuditDetails = (auditEnabled: boolean, userId: string, userName: string,
  userAudit: any, recordName: string, service: string, actionType: string) => {
  return {
    auditEnabled,
    userId,
    userName,
    fieldDetails: JSON.stringify(userAudit),
    recordName,
    service,
    actionType,
  };
};
