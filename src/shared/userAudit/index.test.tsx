import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { findByTestAttr } from '../__test__/testUtils';
import UserAudit from './index';
import Form from '../form';
import { templates } from './audit-test-data';
import userAuditTemplate from './audit-template-test.json';

const mockStore = configureStore([]);
Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

const defaultInitialState = {
  warrants: { templates },
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (props = {}) => {
  return shallow(<UserAudit {...props} />);
};

describe('UserAudit is displayed when create a warrant', () => {
  const createAudit = {
    warrantNumber: '6679',
    signer1: 'John Smith',
    date1: '2/12/2020',
    signer2: 'Peter Lubenburg',
    date2: '6/7/2020',
  };
  const createProps = {
    ref: React.createRef(),
    defaults: createAudit,
    mode: 'create',
  };
  let wrapper: any;

  beforeEach(() => {
    const store = mockStore(defaultInitialState);
    const setupProps = { ...createProps, store };
    wrapper = setup({ ...setupProps });
  });

  test('UserAudit component shows form fields when creating a warrant', () => {
    const userAudit = wrapper.dive().dive().dive();
    const createAuditTemplate = userAuditTemplate.createWarrant.general.fields;
    const createAuditForm = userAudit.find(Form);
    expect(createAuditForm.prop('fields')).toStrictEqual(createAuditTemplate);
    expect(createAuditForm.prop('default')).toStrictEqual(createAudit);
  });

  test('UserAudit field contents change when user enter input', () => {
    const userAudit = wrapper.dive().dive().dive();
    const createAuditForm = userAudit.find(Form).dive();
    const inputGroupWarrantNumber = findByTestAttr(createAuditForm, 'input-warrantNumber');
    const inputGroupSigner1 = findByTestAttr(createAuditForm, 'input-signer1');
    const inputGroupDate1 = findByTestAttr(createAuditForm, 'input-date1');
    inputGroupWarrantNumber.simulate('change', { currentTarget: { type: 'input', value: '8890', name: 'warrantNumber' } });
    inputGroupSigner1.simulate('change', { currentTarget: { type: 'input', value: 'Mary Lucifer', name: 'signer1' } });
    inputGroupDate1.simulate('change', '14/6/2020');
    const newUserAuditState = {
      warrantNumber: '8890',
      signer1: 'Mary Lucifer',
      date1: '14/6/2020',
      signer2: 'Peter Lubenburg',
      date2: '6/7/2020',
    };
    expect(userAudit.state('userAudit')).toEqual(newUserAuditState);
  });
});
