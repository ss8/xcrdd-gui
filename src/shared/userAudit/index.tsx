import React from 'react';
import { connect } from 'react-redux';
import { FormTemplateFieldMap } from 'data/template/template.types';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import DynamicForm, { ChangedField } from '../form';
import { trimJsonObj } from '../dataUtils/json-utils';
import { CONFIG_FORM_MODE } from '../popup/config-form-popup';

class UserAudit extends React.Component<any, any> {
  private ref: any;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      userAudit: this.props.defaults ? this.props.defaults : '',
    };
  }

  getValue = () => {
    return this.state.userAudit;
  }

  auditFieldUpdateHandler = (changedField: ChangedField) => {
    const {
      name, value,
    } = changedField;

    const newUserAudit = { ...this.state.userAudit };
    if (name === 'date1' || name === 'date2') {
      newUserAudit[name] = trimJsonObj(value);
    } else {
      newUserAudit[name] = value;
    }
    this.setState({ userAudit: newUserAudit });
  }

  isValid = () => {
    if (this.ref?.current) {
      return this.ref.current.isValid(true, true);
    }
    return true;
  }

  static getFormTemplateFields = (
    formTemplateFields: FormTemplateFieldMap,
    mode:string,
  ): FormTemplateFieldMap => {

    if (mode === CONFIG_FORM_MODE.View) {
      setAllTemplateFieldsAsReadOnly(formTemplateFields);
    }

    return formTemplateFields;
  }

  render() {
    const { mode } = this.props;

    if (!this.props.userAuditForm) return null;
    const renderTemplate: any = (mode === CONFIG_FORM_MODE.Create)
      ? this.props.userAuditForm.createWarrant : (mode === CONFIG_FORM_MODE.Edit)
        ? this.props.userAuditForm.editWarrant : this.props.userAuditForm.deleteWarrant;

    return (
      <DynamicForm
        name={renderTemplate.key}
        ref={this.ref}
        fields={UserAudit.getFormTemplateFields(renderTemplate.general.fields, mode)}
        layout={renderTemplate.general.metadata.layout.rows}
        default={this.state.userAudit}
        fieldUpdateHandler={this.auditFieldUpdateHandler}
        colWidth={6}
        {...this.props}
      />
    );
  }
}

const getAuditTemplate = (templates:any[]) => {
  const auditTemplates = templates.filter((template:any) =>
    template.category === 'audit');
  let template: any = null;
  if (auditTemplates.length > 0) {
    template = JSON.parse(auditTemplates[0].template);
  }
  return template;
};

const mapStateToProps = ({
  warrants: { templates },
}: any) => ({
  userAuditForm: getAuditTemplate(templates),
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(UserAudit);
