import { getAuditFieldInfo } from './audit-constants';

describe('audit-constants', () => {
  describe('getAuditFieldInfo()', () => {
    let formData: any;
    let formData2: any;
    let auditData: any;
    let beforeFieldValues: any;

    beforeEach(() => {
      formData = {
        stringTypeEmpty: '',
        stringType: 'string1',
        numberTypeZero: 0,
        numberType: 10,
        booleanFalseType: false,
        booleanTrueType: true,
        objectType: { stringType: 'dummy' },
        arrayLiteralType: ['value1', 'value2'],
        arrayObjectType: [{ a: 1 }, { b: 2 }],
        nullType: null,
        undefinedType: undefined,
      };

      formData2 = {
        stringType: 'string1',
        numberType: 10,
        booleanType: true,
        objectType: { stringType: 'dummy2' },
        arrayLiteralType: ['value1', 'value3'],
        arrayObjectType: [{ a: 1 }, { b: 3 }],
        nullType: 'not null anymore',
        undefinedType: 'not undefined anymore',
      };

      auditData = {
        user: 'user1',
        sign: 'sign1',
      };

      beforeFieldValues = {
        stringType: '',
        numberType: 0,
        booleanType: false,
        objectType: { stringType: 'dummy' },
        arrayLiteralType: ['value1', 'value2'],
        arrayObjectType: [{ a: 1 }, { b: 2 }],
        nullType: null,
        undefinedType: undefined,
      };
    });

    it('should return an empty array if nothing passed', () => {
      expect(getAuditFieldInfo(null)).toEqual([]);
    });

    it('should return the audit fields when not editing', () => {
      expect(getAuditFieldInfo(formData)).toEqual([
        { field: 'stringTypeEmpty', value: '' },
        { field: 'stringType', value: 'string1' },
        { field: 'numberTypeZero', value: 0 },
        { field: 'numberType', value: 10 },
        { field: 'booleanFalseType', value: false },
        { field: 'booleanTrueType', value: true },
        { field: 'objectType', value: { stringType: 'dummy' } },
        { field: 'arrayLiteralType', value: ['value1', 'value2'] },
        { field: 'arrayObjectType', value: [{ a: 1 }, { b: 2 }] },
        { field: 'nullType', value: null },
        { field: 'undefinedType', value: undefined },
      ]);
    });

    it('should return the audit fields empty if no change', () => {
      expect(getAuditFieldInfo(formData, null, true, formData)).toEqual([]);
    });

    it('should return the audit fields when there are changes', () => {
      expect(getAuditFieldInfo(formData2, null, true, beforeFieldValues)).toEqual([
        { field: 'stringType', before: '', after: 'string1' },
        { field: 'numberType', before: 0, after: 10 },
        { field: 'booleanType', before: false, after: true },
        { field: 'objectType', before: { stringType: 'dummy' }, after: { stringType: 'dummy2' } },
        { field: 'arrayLiteralType', before: ['value1', 'value2'], after: ['value1', 'value3'] },
        { field: 'arrayObjectType', before: [{ a: 1 }, { b: 2 }], after: [{ a: 1 }, { b: 3 }] },
        { field: 'nullType', before: '', after: 'not null anymore' },
        { field: 'undefinedType', before: '', after: 'not undefined anymore' },
      ]);
    });

    it('should return the audit fields with the audit data', () => {
      expect(getAuditFieldInfo(formData2, auditData, true, beforeFieldValues)).toEqual([
        { field: 'stringType', before: '', after: 'string1' },
        { field: 'numberType', before: 0, after: 10 },
        { field: 'booleanType', before: false, after: true },
        { field: 'objectType', before: { stringType: 'dummy' }, after: { stringType: 'dummy2' } },
        { field: 'arrayLiteralType', before: ['value1', 'value2'], after: ['value1', 'value3'] },
        { field: 'arrayObjectType', before: [{ a: 1 }, { b: 2 }], after: [{ a: 1 }, { b: 3 }] },
        { field: 'nullType', before: '', after: 'not null anymore' },
        { field: 'undefinedType', before: '', after: 'not undefined anymore' },
        { field: 'user', value: 'user1' },
        { field: 'sign', value: 'sign1' },
      ]);
    });
  });
});
