import {
  Boundary, Button, Overlay, Popover,
} from '@blueprintjs/core';
import React, { FunctionComponent, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { IconXCRDD, IconDiscovery, IconAppLauncher } from '../../utils/icons';

type appLauncherConfig={
  id:string,
  text:string,
  icon:JSX.Element,
  route:string,
}

const AppLauncher: FunctionComponent = (props: any): ReactElement => {
  const {
    setActiveHeading, activeApplication, closeAppLauncher, openAppLauncher, showOverlay,
  } = props;
  const appLauncherItems:appLauncherConfig[] = [
    {
      id: 'discovery',
      text: 'Discovery',
      icon: IconDiscovery(),
      route: '/warrants',
    },
    {
      id: 'xcrdd',
      icon: IconXCRDD(),
      text: 'Xcipio - Retained Data Delivery',
      route: '/xcrdd',
    },
  ];

  return (
    <Popover
      isOpen={showOverlay}
      hasBackdrop={false}
      position="bottom"
      onClose={closeAppLauncher}
      content={(
        <div className="bp3-menu-container">
          <div style={{ padding: '8px' }}>
            <strong>Go to</strong>
            <div>
              {appLauncherItems
                .filter((item) => item.id !== activeApplication)
                .map((launcherItem) => {
                  const {
                    route, id, icon, text,
                  } = launcherItem;
                  return (
                    <Link to={route} key={id} onClick={() => setActiveHeading(id)}>
                      {icon}
                      {text}
                    </Link>
                  );
                })}
            </div>
          </div>
        </div>
)}
    >
      <div onClick={openAppLauncher} className="bp3-menu-burger">
        <IconAppLauncher />
      </div>
    </Popover>
  );
};

export default AppLauncher;
