import React from 'react';
import { shallow } from 'enzyme';
import { Nav } from './index';
import { AF_ROUTE } from '../../constants';

describe('<Nav />', () => {
  let testMocks: any;
  let wrapper: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.props = {
      authentication: {
        isAuthenticated: true,
        privileges: [],
      },
      location: {
        pathname: '/warrants',
      },
      history: {
        push: jest.fn(),
      },
      global: {
        dfs: [],
      },
      getDXSettings: jest.fn(),
      getAllDFs: jest.fn(),
    };

    testMocks.privileges = [
      'user_delete',
      'team_view',
      'df_delete',
      'sh_access',
      'team_write',
      'alarm_access',
      'cf_delete',
      'af_view',
      'userrole_view',
      'warrant_delete',
      'policy_view',
      'mml_access',
      'contact_view',
      'cf_view',
      'stat_report',
      'audit_log_delete',
      'warrant_view',
      'dashboard_access',
      'user_admin',
      'dfsubsys_access',
      'system_admin',
      'warrant_write',
      'userrole_delete',
      'userrole_write',
      'config_admin',
      'contact_delete',
      'user_write',
      'br_access',
      'user_view',
      'group_admin',
      'policy_write',
      'warrant_report',
      'af_write',
      'wt_access',
      'af_report',
      'df_view',
      'df_write',
      'cf_write',
      'userrole_admin',
      'team_admin',
      'contact_write',
      'team_delete',
      'audit_log_view',
      'af_delete',
      'cf_report',
      'dc_access',
      'tp_access',
    ];

    wrapper = shallow(<Nav {...testMocks.props} />);
  });

  describe('getSelectedNavItemFromRouter()', () => {
    it('should return the nav item based in the location.pathname for root path', () => {
      wrapper.setProps({
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toEqual(
        wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/warrants'),
      );
    });

    it('should return the nav item based in the location.pathname for child path', () => {
      wrapper.setProps({
        location: {
          pathname: '/cases',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toEqual(
        wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/warrants'),
      );
    });

    it('should return the nav item based in the location.pathname root for child path if child path not defined', () => {
      wrapper.setProps({
        location: {
          pathname: '/users/create',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toEqual(
        wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/users'),
      );
    });

    it('should return the nav item based in the location.pathname root for child path if child path defined', () => {
      wrapper.setProps({
        location: {
          pathname: '/afs/xms',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toEqual(
        wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === AF_ROUTE),
      );
    });

    it('should return the nav item based in the location.pathname for xms', () => {
      wrapper.setProps({
        location: {
          pathname: '/xms/reports',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toEqual(
        wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/xms/reports'),
      );
    });

    it('should return null if can not select the item based in the location.pathname', () => {
      wrapper.setProps({
        location: {
          pathname: '/invalid',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedNavItemFromRouter()).toBeNull();
    });
  });

  describe('componentDidUpdate()', () => {
    beforeEach(() => {
      jest.spyOn(wrapper.instance(), 'setState');
    });

    it('should set state with selected nav item', () => {
      wrapper.setProps({
        location: {
          pathname: '/roles',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().setState).toBeCalledWith({
        active: wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/users'),
      });
    });

    it('should set state with default selection if can not find path in config', () => {
      wrapper.setProps({
        location: {
          pathname: '/invalid',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().setState).toBeCalledWith({
        active: wrapper.instance().availableNavBarConfig.find(({ route }: { route: string}) => route === '/warrants'),
      }, expect.any(Function));
    });
  });
});
