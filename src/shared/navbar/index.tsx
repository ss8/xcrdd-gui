import React, { Fragment, ReactElement } from 'react';
import './navbar.scss';
import {
  HTMLSelect,
  Navbar,
  NavbarHeading,
  Classes,
  NavbarDivider,
  Alignment,
  NavbarGroup,
  Button,
} from '@blueprintjs/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserMenu from 'components/UserMenu';
import { RootState } from 'data/root.reducers';
import { IconAppLauncher } from 'utils/icons';
import * as authenticationActions from 'data/authentication/authentication.actions';
import xcrddlogo from '../../assets/xcrdd-logo.png';
import discoverylogo from '../../assets/discovery-logo.png';

import { getPasswordAndGoToPage } from '../../policies/policy-actions';
import { getAllDFs, setDF } from '../../global/global-actions';
import { getDXSettings } from '../../xcipio/discovery-xcipio-actions';
import { createSelect } from '../form/form-renderer';
import navbarConfig from './navbar-config';
import AuthUtils from '../../authentication/auth-utils';
import SubNavComponent from './sub-nav-component';
import { AUTH, getAuth } from '../../utils';
import { getRouteRootPathname } from './utils';
import { NavbarItem } from './types';
import AppLauncher from './app-launcher';

export class Nav extends React.Component<any, any> {

  private ref: any;

  private availableNavBarConfig: NavbarItem[] = [];

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      active: null,
      curPathBeforePasswordUpdate: '',
      showMenu: false,
      activeApplication: 'discovery',
    };
  }

  handleNavItemClick = (e: any) => {
    // Set the state to active on the selected main nav item.
    // Get the respective navbarConfig for the clicked target.
    for (let index = 0; index < this.availableNavBarConfig.length; index += 1) {
      if (this.availableNavBarConfig[index].id === e.currentTarget.id) {
        this.ref.current.state.active = this.state.active.children
          ? { ...this.state.active.children[0] }
          : null;
        this.setState({ active: this.availableNavBarConfig[index] }, () => {
          this.props.history.push(this.availableNavBarConfig[index].route);
        });
        break;
      }
    }
  };

  getSelectedNavItemFromRouter() {
    const {
      location: { pathname },
    } = this.props;

    const navBarMap: { [key: string]: any } = {};
    this.availableNavBarConfig.forEach((config) => {
      if (config.children && config.children.length > 1) {
        config.children.forEach((childConfig: any) => {
          navBarMap[childConfig.route] = config;
        });
      } else {
        navBarMap[config.route] = config;
      }
    });

    return (
      navBarMap[pathname] || navBarMap[getRouteRootPathname(pathname)] || null
    );
  }

  getDefaultSelectedNavItem = () => {
    for (let index = 0; index < this.availableNavBarConfig.length; index += 1) {
      if (this.availableNavBarConfig[index].defaultSelected) {
        return this.availableNavBarConfig[index];
      }
    }
    return this.availableNavBarConfig.length > 0
      ? this.availableNavBarConfig[0]
      : null;
  };

  isActive = (selection: any) => {
    if (this.state.active != null && this.state.active.id === selection) {
      return true;
    }
    return false;
  };

  handlePasswordUpdate = () => {
    this.props.setPasswordUpdateFlag();
    this.setState({
      curPathBeforePasswordUpdate: this.props.history.location.pathname,
    });
    const body = {
      pathname: '/authentication/passwordupdate',
      username: this.props.authentication.userId,
      curpath: this.props.history.location.pathname,
    };
    this.props.getPasswordAndGoToPage(this.props.history, body);
  };

  openAppLauncher= () => {
    this.setState((prevState: any) => {
      return { showMenu: true };
    });
  }

  closeAppLauncher= () => {
    this.setState((prevState: any) => {
      return { showMenu: false };
    });
  }

  setActiveHeading= (heading: string) => {
    this.setState({ activeApplication: heading, showMenu: false });
  }

  componentDidMount() {
    const location = this.props.history.location.pathname.split('/')[1];
    if (location === 'xcrdd') {
      this.setState({ activeApplication: 'xcrdd' });
    }

    if (this.props.authentication) {
      const { isAuthenticated, userId: user } = this.props.authentication;

      if (isAuthenticated && getAuth(AUTH.TOKEN)) {
        this.props.doCheckToken(getAuth(AUTH.TOKEN));

        const {
          externalDashboardIP,
          logiOAuthClientId,
          logiOAuthClientName,
          logiSupervisorUserName,
          logiSupervisorPswd,
        } = this.props;

        this.props.checkLogiToken({
          user,
          externalDashboardIP,
          logiOAuthClientId,
          logiOAuthClientName,
          logiSupervisorUserName,
          logiSupervisorPswd,
        });
      }
    }
  }

  componentDidUpdate(prevProps: any, prevState:any) {
    const { isAuthenticated, privileges } = this.props.authentication;
    if (
      (isAuthenticated && !prevProps.authentication.isAuthenticated) ||
      privileges !== prevProps.authentication.privileges ||
      this.props.isExternalDashboardEnabled !== prevProps.isExternalDashboardEnabled ||
       this.state.activeApplication !== prevState.activeApplication
    ) {
      // Reloading in case of browser refresh
      this.props.getDXSettings();
      // Make a call to dfs only if you have access.
      if (AuthUtils.hasPermissions(['df_view'], privileges)) {
        this.props.getAllDFs();
      }
      this.availableNavBarConfig = navbarConfig(
        this.props.isExternalDashboardEnabled,
        this.props.isSecurityGatewayEnabled,
      ).filter((navItem: any) => {
        if (navItem.application && navItem.application.includes(this.state.activeApplication)) {
          return AuthUtils.hasPermissions(navItem.permissions, privileges);
        }

      });

      const selectedNavItem = this.getSelectedNavItemFromRouter();
      if (selectedNavItem != null) {
        return this.setState({ active: selectedNavItem });
      }

      const defaultNavItem = this.getDefaultSelectedNavItem();
      if (defaultNavItem != null) {
        this.setState({ active: defaultNavItem }, () => {
          this.props.history.push(defaultNavItem.route);
        });
      }
    }
  }

  render() {
    const {
      location,
      authentication: { userId, firstName, lastName },
      handleLogout,
    } = this.props;

    const dfValues = createSelect(this.props.global.dfs, 'id', 'name');

    const ActiveHeading = ():any => {
      const headings = [
        {
          id: 'discovery',
          logo: discoverylogo,
          alt: 'Discovery Logo',
        },
        {
          id: 'xcrdd',
          logo: xcrddlogo,
          alt: 'XCRDD Logo',
        },
      ];

      return headings.filter((item) => item.id === this.state.activeApplication)
        .map((heading):ReactElement => (
          <img
            key={heading.alt}
            height={31}
            style={{ marginTop: '7px', marginLeft: '6px' }}
            src={heading.logo}
            alt={heading.alt}
          />
        ));
    };
    return (
      <Fragment>
        <Navbar fixedToTop className="bp3-dark">
          <NavbarGroup align={Alignment.LEFT}>
            <AppLauncher
              setActiveHeading={this.setActiveHeading}
              activeApplication={this.state.activeApplication}
              openAppLauncher={this.openAppLauncher}
              closeAppLauncher={this.closeAppLauncher}
              showOverlay={this.state.showMenu}
            />
            <NavbarHeading>
              <ActiveHeading />
            </NavbarHeading>
            <NavbarDivider />
            {this.availableNavBarConfig.map((navItem: any, i: number) => {
              const {
                text, icon, id, permissions,
              } = navItem;
              if (
                !AuthUtils.hasPermissions(
                  permissions,
                  this.props.authentication.privileges,
                )
              ) {
                return null;
              }
              return (
                <Button
                  key={i}
                  className={Classes.MINIMAL}
                  icon={icon}
                  active={this.isActive(id)}
                  id={id}
                  text={text}
                  onClick={this.handleNavItemClick}
                />
              );
            })}
          </NavbarGroup>
          <NavbarGroup align={Alignment.RIGHT}>
            {this.props.global.dfs.length > 1 && (
              <HTMLSelect
                name="setDF"
                options={dfValues}
                onChange={(e: any) =>
                  this.props.setDF(parseInt(e.target.value, 10))}
                value={this.props.global.selectedDF}
              />
            )}
            <UserMenu
              username={userId}
              firstName={firstName}
              lastName={lastName}
              onLogout={handleLogout}
              passwordUpdate={this.handlePasswordUpdate}
            />
          </NavbarGroup>
        </Navbar>
        <SubNavComponent
          location={location}
          subNavItems={
            this.state.active !== null
              ? this.state.active.children
                ? this.state.active.children
                : []
              : null
          }
          ref={this.ref}
          {...this.props}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (
  {
    authentication,
    global,
    policies,
    discoveryXcipio: {
      discoveryXcipioSettings: {
        isExternalDashboardEnabled,
        isSecurityGatewayEnabled,
        externalDashboardIP,
        logiOAuthClientId,
        logiOAuthClientName,
        logiSupervisorUserName,
        logiSupervisorPswd,
      },
    },
  }: RootState,
  { location }: any,
) => ({
  authentication,
  global,
  policies,
  isExternalDashboardEnabled,
  isSecurityGatewayEnabled,
  externalDashboardIP,
  logiOAuthClientId,
  logiOAuthClientName,
  logiSupervisorUserName,
  logiSupervisorPswd,
  location,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      checkLogiToken: authenticationActions.checkLogiToken,
      handleLogout: authenticationActions.handleLogout,
      getAllDFs,
      setDF,
      doCheckToken: authenticationActions.doCheckToken,
      setPasswordUpdateFlag: authenticationActions.setPasswordUpdateFlag,
      getPasswordAndGoToPage,
      getDXSettings,
    },
    dispatch,
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(Nav),
);
