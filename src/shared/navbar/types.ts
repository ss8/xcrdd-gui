export interface NavbarItem {
  id: string
  icon?: string | JSX.Element
  text: string
  route: string
  application: string[]
  url?: string
  className: string
  inactiveClassName?: string
  defaultSelected?: boolean
  permissions: string[]
  children?: NavbarSubItem[]
}

export interface NavbarSubItem {
  id: string
  icon?: string | JSX.Element
  text: string,
  route: string,
  application: string[],
  url?: string,
  className: string,
  inactiveClassName?: string,
  permissions: string[],
}
