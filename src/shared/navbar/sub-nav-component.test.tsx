import React from 'react';
import { shallow } from 'enzyme';
import SubNavComponent from './sub-nav-component';
import navbarConfig from './navbar-config';
import { AF_ROUTE } from '../../constants';

describe('<SubNavComponent />', () => {
  let testMocks: any;
  let wrapper: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.props = {
      authentication: {
        isAuthenticated: true,
        privileges: [],
      },
      location: {
        pathname: '/cases',
      },
      history: {
        push: jest.fn(),
      },
      subNavItems: [],
    };

    wrapper = shallow(<SubNavComponent {...testMocks.props} />);
  });

  describe('getSelectedItemFromRouter()', () => {
    it('should return the nav item based in the location.pathname', () => {
      const navBarConfig = navbarConfig(false, false);
      const { children = [] } = navBarConfig.find(({ route }) => route === '/warrants') ?? {};
      wrapper.setProps({
        subNavItems: children,
      });
      expect(wrapper.instance().getSelectedItemFromRouter()).toEqual(children[1]);
    });

    it('should return the nav item based in the location.pathname root for child path', () => {
      const navBarConfig = navbarConfig(false, false);
      const { children = [] } = navBarConfig.find(({ route }) => route === '/users') ?? {};
      wrapper.setProps({
        location: {
          pathname: '/teams/create',
        },
        subNavItems: children,
      });
      expect(wrapper.instance().getSelectedItemFromRouter()).toEqual(children[1]);
    });

    it('should return the nav item based in the location.pathname for child path', () => {
      const navBarConfig = navbarConfig(false, false);
      const { children = [] } = navBarConfig.find(({ route }) => route === AF_ROUTE) ?? {};
      wrapper.setProps({
        location: {
          pathname: `${AF_ROUTE}/xms`,
        },
        subNavItems: children,
      });
      expect(wrapper.instance().getSelectedItemFromRouter()).toEqual(children[1]);
    });

    it('should return null if can not select the item based in the location.pathname', () => {
      wrapper.setProps({
        location: {
          pathname: '/invalid',
        },
        authentication: {
          privileges: testMocks.privileges,
        },
      });
      expect(wrapper.instance().getSelectedItemFromRouter()).toBeNull();
    });
  });

  describe('componentDidUpdate()', () => {
    beforeEach(() => {
      jest.spyOn(wrapper.instance(), 'setState');
    });

    it('should set state with selected nav item', () => {
      const navBarConfig = navbarConfig(false, false);
      const { children = [] } = navBarConfig.find(({ route }) => route === '/warrants') ?? {};
      wrapper.setProps({
        subNavItems: children,
      });
      expect(wrapper.instance().setState).toBeCalledWith({
        subNavbar: children,
        active: children[1],
      }, expect.any(Function));
    });

    it('should set state with default selection if can not find path in config', () => {
      const navBarConfig = navbarConfig(false, false);
      const { children = [] } = navBarConfig.find(({ route }) => route === '/warrants') ?? {};
      wrapper.setProps({
        location: {
          pathname: '/invalid',
        },
        subNavItems: children,
      });
      expect(wrapper.instance().setState).toBeCalledWith({
        subNavbar: children,
        active: children[0],
      }, expect.any(Function));
    });
  });
});
