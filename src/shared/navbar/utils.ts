// eslint-disable-next-line import/prefer-default-export
export function getRouteRootPathname(pathname: string) {
  return pathname.split('/').slice(0, 2).join('/');
}
