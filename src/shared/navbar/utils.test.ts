import { getRouteRootPathname } from './utils';

describe('utils', () => {
  describe('getRouteRootPathname()', () => {
    it('should return the route root pathname', () => {
      expect(getRouteRootPathname('/warrants')).toEqual('/warrants');
    });

    it('should return the route root pathname when additional path entry', () => {
      expect(getRouteRootPathname('/warrants/create')).toEqual('/warrants');
    });
  });
});
