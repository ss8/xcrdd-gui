import React, { Fragment } from 'react';
import './navbar.scss';
import { Navbar, Button } from '@blueprintjs/core';
import AuthUtils from '../../authentication/auth-utils';
import { getRouteRootPathname } from './utils';

class SubNavComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      active: null,
      subNavbar: [],
    };
  }

  handleNavItemClick = (e: any) => {
    // Set the state to active on the selected main nav item.
    // Get the respective navbarConfig for the clicked target.
    for (let index = 0; index < this.state.subNavbar.length; index += 1) {
      if (this.state.subNavbar[index].id === e.currentTarget.id) {
        this.setState({ active: this.state.subNavbar[index] }, () => {
          this.props.history.push(this.state.subNavbar[index].route);
        });
        break;
      }
    }
  }

  isActive = (selection: any) => {
    if (this.state.active != null && this.state.active.id === selection) return true;
    return false;
  }

  getSelectedItemFromRouter() {
    const {
      subNavItems,
      location: { pathname },
    } = this.props;

    const subNavBarMap = subNavItems.reduce((map: any, item: any) => ({
      ...map,
      [item.route]: item,
    }), {});

    return subNavBarMap[pathname] || subNavBarMap[getRouteRootPathname(pathname)] || null;
  }

  componentDidUpdate(prevProps: any) {
    if (prevProps && this.props.subNavItems && prevProps.subNavItems !== this.props.subNavItems) {
      let selectedItem = this.getSelectedItemFromRouter();

      if (selectedItem == null) {
        selectedItem = this.props.subNavItems.length > 0 ?
          this.props.subNavItems[0] : null;
      }

      this.setState({ subNavbar: this.props.subNavItems, active: selectedItem }, () => {
        if (selectedItem != null) {
          this.props.history.push(selectedItem.route);
        }
      });
    }
  }

  render() {
    return (
      <Fragment>
        {this.state.subNavbar.length > 0 && (
        <Navbar
          fixedToTop
          className="bp3-nav-bar"
          style={{
            marginTop: '50px', height: '37px', background: 'rgb(229, 233, 240)', zIndex: 1,
          }}
        >
          {this.state.subNavbar.map((subNavItem: any, i: number) => {
            const {
              text,
              icon,
              id,
              className,
              inactiveClassName,
              permissions,
            } = subNavItem;
            if (!AuthUtils.hasPermissions(permissions, this.props.authentication.privileges)) {
              return null;
            }
            return (
              <Button
                style={{ marginTop: '3px' }}
                key={i}
                className={!this.isActive(id) && inactiveClassName ?
                  inactiveClassName : className}
                icon={icon}
                active={this.isActive(id)}
                id={id}
                text={text}
                onClick={this.handleNavItemClick}
              />
            );
          }) }
        </Navbar>
        )}
      </Fragment>
    );
  }
}

export default SubNavComponent;
