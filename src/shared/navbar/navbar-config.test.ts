import navbarConfig from './navbar-config';

describe('navbarConfig', () => {
  it('should return the enable nav configs without the external dashboard and without the security gateway', () => {
    expect(navbarConfig(false, false)).toMatchSnapshot();
  });

  it('should return the enable nav configs with the external dashboard and without the security gateway', () => {
    expect(navbarConfig(true, false)).toMatchSnapshot();
  });

  it('should return the enable nav configs without the external dashboard and with the security gateway', () => {
    expect(navbarConfig(false, true)).toMatchSnapshot();
  });
});
