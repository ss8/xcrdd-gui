import React from 'react';

const EmptyGrid = (props: any) => (
  <div className="event-grid-container">
    <div
      className="empty-grid"
      style={{
        height: props.height ? props.height : '675px',
        width: '100%',
      }}
    >
      <span>No Results</span>
    </div>
  </div>
);

export { EmptyGrid }; // eslint-disable-line
