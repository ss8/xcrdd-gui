import {
  GET_ALL_ROLES_FULFILLED,
  GET_ALL_PERMISSION_GROUPS_FULFILLED,
  GET_ALL_PERMISSIONS_FULFILLED,
  ROLES_CLEAR_ERRORS,
} from './roles-actions';
import roleLabels from './roles-labels.json';

const defaultState = {
  roles: [],
  permissionGroups: [],
  permissionsMap: {}, // this maps permission id to permission data
  error: {
    errorCode: null,
    errorMessage: null,
  },
};

const moduleReducer = (state = defaultState, action: any) => {
  if (/(ROLE|PERMISSION|PERMISSION_GROUPS).*_REJECTED/.test(action.type)) {
    let errorFieldName = '';
    let errorCode = action.payload.response.status;
    let errorMessage = action.payload.response.statusText;
    if (action.payload.response.data && action.payload.response.data !== '' &&
        action.payload.response.data.error) {
      errorFieldName = action.payload.response.config.data ?
        JSON.parse(action.payload.response.config.data).name : null;
      errorCode = action.payload.response.data.error.code;
      errorMessage = action.payload.response.data.error.message;
    }
    return { ...state, error: { errorMessage, errorCode, errorFieldName } };
  }

  switch (action.type) {
    case GET_ALL_ROLES_FULFILLED:
      return { ...state, roles: action.payload.data.data };

    case GET_ALL_PERMISSIONS_FULFILLED:
      // map id to permission data and adding label to the data
      const newPermissionsMap : any = {};
      action.payload.data.data.forEach((permission:any) => {
        if (newPermissionsMap[permission.id] === undefined) {
          const permissionLabel =
          ((roleLabels.roles.permissions as any)[permission.name] === undefined) ?
            permission.name : (roleLabels.roles.permissions as any)[permission.name];

          const newPermission = { ...permission, label: permissionLabel };
          newPermissionsMap[permission.id] = newPermission;
        }
      });

      return { ...state, permissionsMap: newPermissionsMap };

    case GET_ALL_PERMISSION_GROUPS_FULFILLED:
      return { ...state, permissionGroups: action.payload.data.data };
    case ROLES_CLEAR_ERRORS:
      return { ...state, error: { errorMessage: null, errorCode: null } };
    default:
      return state;
  }
};
export default moduleReducer;
