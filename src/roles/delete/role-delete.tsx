import React from 'react';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import { WithAuditDetails, AuditDetails } from 'data/types';
import {
  getAuditDetails, getAuditFieldInfo, AuditService, AuditActionType,
} from 'shared/userAudit/audit-constants';
import { Role } from 'data/role/role.types';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import ModalDialog from '../../shared/modal/dialog';

class RoleDeleteRole extends React.Component<any, any> {
  handleDelete = () => {
    const { selection } = this.props.location.state;

    selection.forEach((role: Role) => {
      const payload: WithAuditDetails<void> = { data: undefined };
      if (this.props.isAuditEnabled) {
        const auditDetails = getAuditDetails(
          this.props.isAuditEnabled,
          this.props.userDBId,
          this.props.userId,
          getAuditFieldInfo(null),
          role.name,
          AuditService.ROLE,
          AuditActionType.ROLE_DELETE,
        );
        payload.auditDetails = auditDetails as AuditDetails;
      }
      this.props.deleteRoleAndRefresh(String(role.id), payload);
    });
  }

  render() {
    const { selection } = this.props.location.state;
    const displayMessage = `Are you sure you want to delete role "${selection[0].name}" ?`;
    return (
      <ModalDialog
        onSubmit={this.handleDelete}
        width="500px"
        {...this.props}
        actionText="Delete"
        displayMessage={displayMessage}
      />
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  userId: state.authentication.userId,
  userDBId: state.authentication.userDBId,
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

export default connect(mapStateToProps, null)(RoleDeleteRole);
