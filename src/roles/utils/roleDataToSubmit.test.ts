import { Role } from 'data/role/role.types';
import { AuditDetails } from 'data/types';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import getRoleDataToSubmit from './roleDataToSubmit';

describe('getRoleDataToSubmit', () => {
  const userId = '29';
  const userName = 'userName';

  describe('when create role', () => {
    let permissionIdsInStr: string[];
    let roleFormData: Role;
    let expectedRole: Role;
    let expectedAuditDetails: AuditDetails;

    beforeEach(() => {
      permissionIdsInStr = ['13', '14', '15', '22', '23', '24'];
      roleFormData = {
        name: 'RoleName',
        description: '',
      };

      expectedRole = {
        name: 'RoleName',
        description: '',
        permissions: [
          {
            id: 13,
            name: null,
          },
          {
            id: 14,
            name: null,
          },
          {
            id: 15,
            name: null,
          },
          {
            id: 22,
            name: null,
          },
          {
            id: 23,
            name: null,
          },
          {
            id: 24,
            name: null,
          },
        ],
        isActive: true,
      };

      expectedAuditDetails = {
        auditEnabled: true,
        userId: '29',
        userName: 'userName',
        fieldDetails: '[{"field":"name","value":"RoleName"},{"field":"description","value":""},{"field":"permissions","value":[{"id":13,"name":null},{"id":14,"name":null},{"id":15,"name":null},{"id":22,"name":null},{"id":23,"name":null},{"id":24,"name":null}]},{"field":"isActive","value":true}]',
        recordName: 'RoleName',
        service: AuditService.ROLE,
        actionType: AuditActionType.ROLE_ADD,
      };
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getRoleDataToSubmit(
          roleFormData,
          permissionIdsInStr,
          null,
          false,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedRole,
      });
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getRoleDataToSubmit(
          roleFormData,
          permissionIdsInStr,
          null,
          true,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedRole,
        auditDetails: expectedAuditDetails,
      });
    });
  });

  describe('when edit role', () => {
    let permissionIdsInStr: string[];
    let roleFormData: Role;
    let originalRole: Role;
    let expectedRole: Role;
    let expectedAuditDetails: AuditDetails;

    beforeEach(() => {
      permissionIdsInStr = ['13', '14', '15', '22', '23', '24', '25', '26', '27', '28', '29', '30'];

      roleFormData = {
        id: 24,
        name: 'TEST',
        description: '',
        dateCreated: '2020-10-29T06:46:35.000+0000',
        dateUpdated: '2020-10-29T06:46:35.000+0000',
        isActive: false,
        permissions: [
          {
            id: 13,
            name: 'af_view',
          },
          {
            id: 14,
            name: 'af_write',
          },
          {
            id: 15,
            name: 'af_delete',
          },
          {
            id: 22,
            name: 'contact_view',
          },
          {
            id: 23,
            name: 'contact_write',
          },
          {
            id: 24,
            name: 'contact_delete',
          },
        ],
        reporting: null,
        permissionLabels: [
          'Af View',
          'Af Write',
          'Af Delete',
          'Contact View',
          'Contact Write',
          'Contact Delete',
        ],
      };

      originalRole = {
        id: 24,
        name: 'TEST',
        description: '',
        dateCreated: '2020-10-29T06:46:35.000+0000',
        dateUpdated: '2020-10-29T06:46:35.000+0000',
        isActive: false,
        permissions: [
          {
            id: 13,
            name: 'af_view',
          },
          {
            id: 14,
            name: 'af_write',
          },
          {
            id: 15,
            name: 'af_delete',
          },
          {
            id: 22,
            name: 'contact_view',
          },
          {
            id: 23,
            name: 'contact_write',
          },
          {
            id: 24,
            name: 'contact_delete',
          },
        ],
        reporting: null,
        permissionLabels: [
          'Af View',
          'Af Write',
          'Af Delete',
          'Contact View',
          'Contact Write',
          'Contact Delete',
        ],
      };

      expectedRole = {
        id: 24,
        name: 'TEST',
        description: '',
        dateCreated: '2020-10-29T06:46:35.000+0000',
        dateUpdated: '2020-10-29T06:46:35.000+0000',
        isActive: true,
        permissions: [
          {
            id: 13,
            name: null,
          },
          {
            id: 14,
            name: null,
          },
          {
            id: 15,
            name: null,
          },
          {
            id: 22,
            name: null,
          },
          {
            id: 23,
            name: null,
          },
          {
            id: 24,
            name: null,
          },
          {
            id: 25,
            name: null,
          },
          {
            id: 26,
            name: null,
          },
          {
            id: 27,
            name: null,
          },
          {
            id: 28,
            name: null,
          },
          {
            id: 29,
            name: null,
          },
          {
            id: 30,
            name: null,
          },
        ],
        reporting: null,
        permissionLabels: [
          'Af View',
          'Af Write',
          'Af Delete',
          'Contact View',
          'Contact Write',
          'Contact Delete',
        ],
      };

      expectedAuditDetails = {
        auditEnabled: true,
        userId: '29',
        userName: 'userName',
        fieldDetails: '[{"field":"isActive","before":false,"after":true},{"field":"permissions","before":[{"id":13,"name":"af_view"},{"id":14,"name":"af_write"},{"id":15,"name":"af_delete"},{"id":22,"name":"contact_view"},{"id":23,"name":"contact_write"},{"id":24,"name":"contact_delete"}],"after":[{"id":13,"name":null},{"id":14,"name":null},{"id":15,"name":null},{"id":22,"name":null},{"id":23,"name":null},{"id":24,"name":null},{"id":25,"name":null},{"id":26,"name":null},{"id":27,"name":null},{"id":28,"name":null},{"id":29,"name":null},{"id":30,"name":null}]}]',
        recordName: 'TEST',
        service: AuditService.ROLE,
        actionType: AuditActionType.ROLE_EDIT,
      };
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getRoleDataToSubmit(
          roleFormData,
          permissionIdsInStr,
          originalRole,
          false,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedRole,
      });
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getRoleDataToSubmit(
          roleFormData,
          permissionIdsInStr,
          originalRole,
          true,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedRole,
        auditDetails: expectedAuditDetails,
      });
    });
  });

});
