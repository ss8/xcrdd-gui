import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';
import { WithAuditDetails, AuditDetails } from 'data/types';
import { Role, Permission } from 'data/role/role.types';

export default function getRoleDataToSubmit(
  roleFormData: Role,
  permissionIdsInStr: string[],
  originalRoleData: Role | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
): WithAuditDetails<Role> | undefined {

  const roleData:Role = {
    ...roleFormData,
    permissions: permissionIdsInStr.map((permissionId:string) => ({ id: +permissionId, name: null } as Permission)),
    isActive: true,
  };

  const roleBody : WithAuditDetails<Role> = {
    data: roleData as Role,
  };

  if (isAuditEnabled) {
    const auditDetails = getAuditDetails(
      isAuditEnabled,
      userId,
      userName,
      getAuditFieldInfo(roleData, null, originalRoleData != null, originalRoleData),
      (roleData as Role).name,
      AuditService.ROLE,
      originalRoleData ? AuditActionType.ROLE_EDIT : AuditActionType.ROLE_ADD,
    );
    roleBody.auditDetails = auditDetails as AuditDetails;
  }
  return roleBody;
}
