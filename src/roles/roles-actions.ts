import { History } from 'history';
import { WithAuditDetails } from '../data/types';
import Http from '../shared/http';
import { showSuccessMessage, showError, TOAST_TIMEOUT } from '../shared/toaster';
import { extractErrorMessage } from '../utils';

const http = Http.getHttp();

// action export constants
export const GET_ALL_ROLES = 'GET_ALL_ROLES';
export const GET_ROLE_BY_ID = 'GET_ROLE_BY_ID';
export const UPDATE_ROLE = 'UPDATE_ROLE';
export const PATCH_ROLE = 'PATCH_ROLE';
export const DELETE_ROLE = 'DELETE_ROLE';
export const CREATE_ROLE = 'CREATE_ROLE';
export const GET_ALL_PERMISSIONS = 'GET_ALL_PERMISSIONS';
export const GET_ALL_PERMISSION_GROUPS = 'GET_ALL_PERMISSION_GROUPS';

export const GET_ALL_ROLES_FULFILLED = 'GET_ALL_ROLES_FULFILLED';
export const GET_ROLE_BY_ID_FULFILLED = 'GET_ROLE_BY_ID_FULFILLED';
export const UPDATE_ROLE_FULFILLED = 'UPDATE_ROLE_FULFILLED';
const PATCH_ROLE_FULFILLED = 'PATCH_ROLE_FULFILLED';
export const DELETE_ROLE_FULFILLED = 'DELETE_ROLE_FULFILLED';
export const ROLES_CLEAR_ERRORS = 'ROLES_CLEAR_ERRORS';
export const GET_ALL_PERMISSIONS_FULFILLED = 'GET_ALL_PERMISSIONS_FULFILLED';
export const GET_ALL_PERMISSION_GROUPS_FULFILLED = 'GET_ALL_PERMISSION_GROUPS_FULFILLED';

// action creators
export const clearErrors = () => ({ type: ROLES_CLEAR_ERRORS, payload: null });
export const getAllRoles = () => ({ type: GET_ALL_ROLES, payload: http.get('/roles') });
export const getRoleById = (id: number) => ({ type: GET_ROLE_BY_ID, payload: http.get(`/roles/${id}`) });
export const getAllPermissionGroups = () => ({ type: GET_ALL_PERMISSION_GROUPS, payload: http.get('/permission-groups') });
export const getAllPermissions = () => ({ type: GET_ALL_PERMISSIONS, payload: http.get('/permissions') });

const createRole = (body: any) => ({ type: CREATE_ROLE, payload: http.post('/roles', body) });
const updateRole = (id: number, body: any) => ({ type: UPDATE_ROLE, payload: http.put(`/roles/${id}`, body) });
const patchRole = (id: number, body: any) => ({ type: PATCH_ROLE, payload: http.patch(`/roles/${id}`, body) });
const deleteRole = (id: number, body: WithAuditDetails<void>) => ({ type: DELETE_ROLE, payload: http.delete(`/roles/${id}`, { data: body.auditDetails }) });

function goToPreviousPage(history: History): () => void {
  return () => {
    history.goBack();
  };
}

export const createRoleAndRefresh = (payload: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(createRole(payload));
      dispatch(goToPreviousPage(history));
      showSuccessMessage('Role successfully created.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create role: ${errorMessage}.`);

    } finally {
      dispatch(getAllRoles());
    }
  };
};

export const updateRoleAndRefresh = (roleId: any, payload: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(updateRole(roleId, payload));
      showSuccessMessage('Role successfully updated.', TOAST_TIMEOUT);
      dispatch(goToPreviousPage(history));
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update role: ${errorMessage}.`);

    } finally {
      dispatch(getAllRoles());
    }
  };
};

export const deleteRoleAndRefresh = (id: number, body: WithAuditDetails<void>) => {
  return async (dispatch: any) => {
    try {
      await dispatch(deleteRole(id, body));
      showSuccessMessage('Role successfully deleted.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to delete role: ${errorMessage}.`);

    } finally {
      dispatch(getAllRoles());
    }
  };
};
