import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllRoles,
  getRoleById,
  createRoleAndRefresh,
  updateRoleAndRefresh,
  deleteRoleAndRefresh,
  getAllPermissionGroups,
  getAllPermissions,
  clearErrors,
} from './roles-actions';
import ModuleRoutes from './roles-routes';
import { getUserSettings, putUserSettings } from '../users/users-actions';

const Roles = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  roles: state.roles,
  authentication: state.authentication,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllRoles,
  getRoleById,
  createRoleAndRefresh,
  updateRoleAndRefresh,
  deleteRoleAndRefresh,
  getAllPermissionGroups,
  getAllPermissions,
  getUserSettings,
  putUserSettings,
  clearErrors,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Roles));
