import { COMMON_GRID_COLUMN_FIELD_NAMES } from '../../shared/commonGrid/grid-enums';
import { stringArrayComparator } from '../../shared/commonGrid/grid-util';

export default [
  {
    headerName: '', field: COMMON_GRID_COLUMN_FIELD_NAMES.Action, cellRenderer: 'actionMenuCellRenderer',
  },
  {
    headerName: 'Name', field: 'name', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'name',
  },
  {
    headerName: 'Description', field: 'description', sortable: true, filter: 'agTextColumnFilter', resizable: true, tooltipField: 'description',
  },
  {
    headerName: 'Permissions',
    field: 'permissionLabels',
    sortable: true,
    filter: 'agTextColumnFilter',
    tooltipField: 'permissionLabels',
    resizable: true,
    filterParams: { textCustomComparator: stringArrayComparator },
  },
];
