import React from 'react';
import CommonGrid, { ICommonGridProperties } from '../../shared/commonGrid';
import RoleGridColumnDefs from './role-grid-column-defs';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../shared/commonGrid/grid-enums';
import { ROLE_PRIVILEGE } from '../roles-enum';
import { CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';

class RolesGrid extends React.Component<any, any> {
  ROLE_GRID_SETTINGS = 'RoleGridSettings';

  columnDefs = RoleGridColumnDefs;

  constructor(props: any) {
    super(props);
    this.state = {
      selection: [],
    };
  }

  saveGridSettings = (roleGridSettings: {[key: string]: any}) => {
    if (roleGridSettings !== null) {
      this.props.putUserSettings(this.ROLE_GRID_SETTINGS, this.props.authentication.userId,
        roleGridSettings);
    }
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
  }

  componentDidMount() {
    this.props.getAllRoles();
    this.props.getAllPermissions();
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getUserSettings(this.ROLE_GRID_SETTINGS, this.props.authentication.userId);
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    const role = { ...this.state.selection[0] };
    if (type === COMMON_GRID_ACTION.Delete) {
      this.props.history.push({ pathname: '/roles/delete', state: this.state });
    } else if (type === COMMON_GRID_ACTION.Edit) {
      this.props.history.push({ pathname: '/roles/edit', state: { action: CONFIG_FORM_MODE.Edit, data: role } });
    } else {
      this.props.history.push({ pathname: '/roles/view', state: { action: CONFIG_FORM_MODE.View, data: role } });
    }
  }

  getRoles = (roles : any) => {
    return roles.map((role:any) => {
      const { permissions } = role;
      const permissionLabels = permissions.map((permission:any) => {
        const { id } = permission;
        return ((this.props.roles.permissionsMap[id]) ?
          String(this.props.roles.permissionsMap[id].label) : id);
      });
      return { ...role, permissionLabels };
    });
  }

  handleCreateNew = () => { this.props.history.push({ pathname: '/roles/create', action: 'create' }); }

  handleRefresh = () => {
    this.props.getAllRoles();
  }

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToCreate = privileges !== undefined &&
      (privileges.includes(ROLE_PRIVILEGE.Edit) || privileges.includes(ROLE_PRIVILEGE.Admin));
    const hasPrivilegeToDelete = privileges !== undefined &&
      (privileges.includes(ROLE_PRIVILEGE.Delete) || privileges.includes(ROLE_PRIVILEGE.Admin));
    const hasPrivilegetoView = privileges !== undefined &&
      (privileges.includes(ROLE_PRIVILEGE.View) || privileges.includes(ROLE_PRIVILEGE.Admin));

    const commonGridProps: ICommonGridProperties = {
      onRefresh: this.handleRefresh,
      onSelectionChanged: this.handleSelection,
      onActionMenuItemClick: this.onActionMenuItemClick,
      rowSelection: ROWSELECTION.SINGLE,
      columnDefs: this.columnDefs,
      rowData: this.getRoles(this.props.roles.roles),
      context: this,
      pagination: true,
      enableSorting: true,
      enableFilter: true,
      enableBrowserTooltips: true,
      handleCreateNew: this.handleCreateNew,
      createNewText: 'New Role',
      hasPrivilegeToCreateNew: hasPrivilegesToCreate,
      hasPrivilegeToDelete,
      hasPrivilegetoView,
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
      noResults: (!this.props.roles ||
        !this.props.roles.roles || !this.props.roles.roles.length),
    };
    return (
      <div className="grid-container" style={{ height: 'inherit' }}>
        <CommonGrid {...commonGridProps} />
      </div>
    );
  }
}

export default RolesGrid;
