// eslint-disable-next-line import/prefer-default-export
export enum ROLE_PRIVILEGE {
    View = 'userrole_view',
    Edit = 'userrole_write',
    Delete = 'userrole_delete',
    Admin = 'userrole_admin', // Predefined Role edit/create/delete permission)
}
