import React, { Fragment } from 'react';
import RolesGrid from './grid';
import { AppToaster } from '../shared/toaster';
import convertErrorCodeToMessage from '../shared/errorCodeConvert/convertErrorCodeToMessage';

class RoleFrame extends React.Component<any, any> {
  handleChange = (e: any) => {
    this.setState({ selectedTab: e });
  }

  handleViewChange = (e: any) => {
    this.setState({ selectedView: e.currentTarget.id });
  }

  componentDidUpdate() {
    const { error } = this.props.roles;
    // handle server side error
    const message = convertErrorCodeToMessage(error);
    if (error.errorCode != null || error.errorMessage != null) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${message}`,
      });
      this.props.clearErrors();
    }
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/authentication/login');
    }
  }

  render() {
    return (
      <Fragment>
        <div className="col-md-12" style={{ height: 'inherit' }}>
          <RolesGrid {...this.props} />
        </div>
      </Fragment>
    );
  }
}

export default RoleFrame;
