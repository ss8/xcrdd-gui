import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import RolesFrame from './roles-frame';
import RolesCreate from './create';
import RolesDelete from './delete/role-delete';
import RoleConfig from './edit';

const moduleRoot = '/roles';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <RolesFrame {...props} />}
    />
    <Route
      path={`${moduleRoot}/delete`}
      render={() => <RolesDelete {...props} />}
    />
    <Route
      path={`${moduleRoot}/create`}
      render={() => <RolesCreate {...props} />}
    />
    <Route
      path={`${moduleRoot}/edit`}
      render={() => <RoleConfig {...props} />}
    />
    <Route
      path={`${moduleRoot}/view`}
      render={() => <RoleConfig {...props} />}
    />

  </Fragment>
);

export default ModuleRoutes;
