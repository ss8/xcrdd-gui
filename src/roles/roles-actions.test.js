import moxios from 'moxios';
import Http from '../shared/http';

import { storeFactory } from '../shared/__test__/testUtils';
import getAllPermissionData from './__test__/getAllPermissionData.json';
import permissionMap from './__test__/permissionMap.json';
import { getAllPermissions } from './roles-actions';

describe('getAllPermissions action creator ', () => {
  let axiosInstance = null;
  let store = null;
  beforeEach(() => {
    const reduxState = {
      authentication: {
        isAuthenticated: true, userId: '', privileges: [], errorStatus: '', beforeLogout: false, passwordUpdate: false, loginMessage: '',
      },
    };
    store = storeFactory(reduxState);
    Http.init(store);
    axiosInstance = Http.getHttp();
    moxios.install(axiosInstance);
  });
  afterEach(() => {
    moxios.uninstall(axiosInstance);
  });
  test('permission map is created properly', () => {
    // Stub a response to be used to respond to a request matching a method and a URL or RegExp
    const response = {
      status: 200,
      response: getAllPermissionData,
    };
    moxios.stubOnce('get', new RegExp('/permissions$'), response);

    // test the getAllPerimssion action and reducer
    store.dispatch(getAllPermissions())
      .then(() => {
        const newState = store.getState();
        expect(newState.roles.permissionsMap).toEqual(permissionMap);
      });
  });
});
