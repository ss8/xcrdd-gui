import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Label,
} from '@blueprintjs/core';
import { RootState } from 'data/root.reducers';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import { Role } from 'data/role/role.types';
import { AppToaster } from '../../shared/toaster';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';
import DynamicForm from '../../shared/form';
import roleConfig from './roles-edit-form.json';
import { ROLE_PRIVILEGE } from '../roles-enum';
import { updateRoleAndRefresh } from '../roles-actions';
import roleLabels from '../roles-labels.json';
import '../../shared/form/form.scss';
import CommonCheckboxTree from '../../shared/checkboxtree';
import getRoleDataToSubmit from '../utils/roleDataToSubmit';

class RoleConfig extends React.Component<any, any> {
  private ref: React.RefObject<DynamicForm>;

  private fields:any;

  private bInitialized = false;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    // makes a copy so the json instance is not modified because it is shared with
    // edit.
    this.fields = JSON.parse(JSON.stringify(roleConfig.forms.caseNotes.fields));

    this.state = {
      data: props.location.state.data, // role data
      currentStateMode: props.location.state.action,
      fieldDirty: false,
      checked: [],
      expanded: [],
      treeShowError: false,
    };
  }

  componentDidMount() {
    this.props.getAllPermissionGroups();
    this.bInitialized = false;
    this.setState({
      data: this.props.location.state.data,
      currentStateMode: this.props.location.state.action,
      fieldDirty: false,
      checked:
        this.props.location.state.data.permissions.map((permission:any) => (String(permission.id))),
      expanded: this.getExpanded([], this.props.roles.permissionGroups),
      treeShowError: false,
    });
    this.handleStateModeChanged(this.props.location.state.action);
  }

  getExpanded = (expanded: [], permissionNodes:[]) => {
    if (this.bInitialized === false && permissionNodes.length > 0) {
      if (permissionNodes) {
        this.bInitialized = true;
        const newexpanded = permissionNodes.map((child:any) => (`top=true ${child.id}`));
        return newexpanded;
      }
    }
    return expanded;
  }

  getPermissionTreeNodes = (permissionNodes:[], level = 1,
    enableOnlyGrpSelection = false) => {
    let treeNodes: any = [];
    if (permissionNodes) {
      treeNodes = permissionNodes.map((child:any) => {
        const aTreeNodeLabel: string =
          ((roleLabels.roles.permissions as any)[child.name] === undefined) ?
            child.name : (roleLabels.roles.permissions as any)[child.name];
        const aTreeNode: any = {
          label:
  <Fragment>{aTreeNodeLabel}</Fragment>,
          value: (level === 1) ? `top=true ${child.id}` : `${child.id}`,
          showCheckbox: !enableOnlyGrpSelection, // Do not show selection on some child nodes
        };
        if (level === 1 && child.privileges && child.privileges.length > 0) {
          aTreeNode.children = this.getPermissionTreeNodes(child.privileges, (level + 1),
            child.enableOnlyGrpSelection);
        }
        return aTreeNode;
      });
    }
    return treeNodes;
  }

  getPermissionNames = () => {
    const permissionIds = this.state.checked.filter((value:string) => (!value.match('^top=true/g')));
    return permissionIds.map((id:number) => this.props.roles.permissionsMap[id].name);
  }

  treeIsValid = () => {
    const currentChecked = this.state.checked.filter((value:string) => (!value.match('^top=true/g')));
    if (currentChecked.length === 0) {
      this.setState({ treeShowError: true });
      return false;
    }
    return true;
  }

  isValid = () => {
    if (!this.ref.current ||
        this.ref.current?.isValid(true) === false ||
        this.treeIsValid() === false) {
      return false;
    }
    return true;
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (this.isValid() === false || !this.ref.current) {
      return;
    }
    const roleFormData = this.ref.current.getValues() as Role;
    const permissionIdsInStr = this.state.checked.filter((value:string) => (!value.match('^top=true/g')));

    const newRole = { ...this.props.location.state.data, ...roleFormData };
    const { id } = this.props.location.state.data;

    const payload = getRoleDataToSubmit(
      newRole,
      permissionIdsInStr,
      this.props.location.state.data,
      this.props.isAuditEnabled,
      this.props.authentication.userDBId,
      this.props.authentication.userId,
    );

    this.props.updateRoleAndRefresh(id, payload, this.props.history);
  }

  handleClose = () => {
    AppToaster.clear();
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  setReadOnly = (readOnly:boolean) => {
    Object.keys(this.fields).forEach((key: any) => {
      (this.fields as any)[key].readOnly = readOnly;
    });
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    this.setState({ currentStateMode: stateMode });
    if (stateMode === CONFIG_FORM_MODE.Edit) {
      this.setReadOnly(false);
    } else {
      this.setReadOnly(true);
    }
  }

  handleFieldUpdate = () => {
    this.setState({ fieldDirty: true });
  }

  getTitle = (currentStateMode : CONFIG_FORM_MODE) => {
    return (currentStateMode === CONFIG_FORM_MODE.Edit) ? 'Edit Role' : 'View Role';
  }

  onCheck = (checked: any) => {
    this.setState({ checked, fieldDirty: true, treeShowError: false });
  }

  onExpand = (expanded: any) => {
    this.setState({ expanded });
  }

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToEdit = privileges !== undefined
      && (privileges.includes(ROLE_PRIVILEGE.Edit) || privileges.includes(ROLE_PRIVILEGE.Admin));

    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: this.state.currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: true,
      width: '500px',
      height: 'auto',
      overflow: 'auto',
    };

    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          <DynamicForm
            name={roleConfig.key}
            ref={this.ref}
            fields={this.fields}
            layout={roleConfig.forms.caseNotes.metadata.layout.rows}
            colWidth={6}
            onSubmit={this.handleSubmit}
            defaults={this.state.data}
            fieldUpdateHandler={this.handleFieldUpdate}
          />
          <div className="form-flex-col-css field-group">
            <Label>Permissions:<span className="required-field"> *</span></Label>
            <div className={(this.state.treeShowError) ? 'invalid-field permissionTree input-maxwidth' : 'permissionTree input-maxwidth'}>
              <CommonCheckboxTree
                disabled={(this.state.currentStateMode === CONFIG_FORM_MODE.View)}
                checked={this.state.checked}
                expanded={this.getExpanded(this.state.expanded, this.props.roles.permissionGroups)}
                nodes={this.getPermissionTreeNodes(this.props.roles.permissionGroups)}
                onCheck={this.onCheck}
                onExpand={this.onExpand}
                showExpandAll
              />
            </div>
            <Fragment>
              {(this.state.treeShowError) ?
                <Label className="field-error-message">Please make selections</Label> : <Fragment />}
            </Fragment>
          </div>
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  authentication: state.authentication,
  roles: state.roles,
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  updateRoleAndRefresh,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RoleConfig));
