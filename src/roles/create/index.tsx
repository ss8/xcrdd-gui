import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import {
  Label,
} from '@blueprintjs/core';
import { RootState } from 'data/root.reducers';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import { Role } from 'data/role/role.types';
import { AppToaster } from '../../shared/toaster';
import DynamicForm from '../../shared/form';
import roleConfig from '../edit/roles-edit-form.json';
import PopUp from '../../shared/popup/dialog';
import roleLabels from '../roles-labels.json';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import '../roles.scss';
import CommonCheckboxTree from '../../shared/checkboxtree';
import getRoleDataToSubmit from '../utils/roleDataToSubmit';

class RolesCreate extends React.Component<any, any> {
  private ref: React.RefObject<DynamicForm>;

  private fields:any;

  private bInitialized = false;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    // makes a copy so the json instance is not modified because it is shared with
    // edit.
    this.fields = JSON.parse(JSON.stringify(roleConfig.forms.caseNotes.fields));
    this.state = {
      isOpen: true,
      checked: [],
      expanded: [],
      treeShowError: false,
    };
  }

  componentDidMount() {
    this.props.getAllPermissionGroups();
    this.bInitialized = false;
    this.setState({
      isOpen: true,
      checked: [],
      expanded: this.getExpanded([], this.props.roles.permissionGroups),
      treeShowError: false,
    });
  }

  getExpanded = (expanded: [], permissionNodes:[]) => {
    if (this.bInitialized === false && permissionNodes.length > 0) {
      if (permissionNodes) {
        this.bInitialized = true;
        const newexpanded = permissionNodes.map((child:any) => (`top=true ${child.id}`));
        return newexpanded;
      }
    }
    return expanded;
  }

  // TODO: Refactor to use same getPermissioTreeNodes for both Create and Edit roles
  getPermissionTreeNodes = (permissionNodes:[], level = 1, enableOnlyGrpSelection = false) => {
    let treeNodes: any = [];
    if (permissionNodes) {
      treeNodes = permissionNodes.map((child:any) => {
        const aTreeNodeLabel: string =
          ((roleLabels.roles.permissions as any)[child.name] === undefined) ?
            child.name : (roleLabels.roles.permissions as any)[child.name];
        const aTreeNode: any = {
          label:
  <Fragment>{aTreeNodeLabel}</Fragment>,
          value: (level === 1) ? `top=true ${child.id}` : `${child.id}`,
          showCheckbox: !enableOnlyGrpSelection, // Do not show selection on some child nodes
        };
        if (level === 1 && child.privileges && child.privileges.length > 0) {
          aTreeNode.children = this.getPermissionTreeNodes(child.privileges, (level + 1), child.enableOnlyGrpSelection);
        }
        return aTreeNode;
      });
    }
    return treeNodes;
  }

  treeIsValid = () => {
    const currentChecked = this.state.checked.filter((value:string) => (!value.match('^top=true/g')));
    if (currentChecked.length === 0) {
      this.setState({ treeShowError: true });
      return false;
    }
    return true;
  }

  isValid = () => {
    if (!this.ref.current ||
        this.ref.current.isValid(true) === false ||
        this.treeIsValid() === false) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: 'Please correct entry field errors.',
      });
      return false;
    }
    return true;
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (this.isValid() === false || !this.ref.current) {
      return;
    }
    const newRole = this.ref.current.getValues() as Role;
    const permissionIdsInStr = this.state.checked.filter((value:string) => (!value.match('^top=true/g')));

    const payload = getRoleDataToSubmit(
      newRole,
      permissionIdsInStr,
      null,
      this.props.isAuditEnabled,
      this.props.authentication.userDBId,
      this.props.authentication.userId,
    );

    this.props.createRoleAndRefresh(payload, this.props.history);
  }

  handleClose = () => {
    AppToaster.clear();
    this.setState({ isOpen: false });
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  onCheck = (checked: any) => {
    this.setState({ checked, treeShowError: false });
  }

  onExpand = (expanded: any) => {
    this.setState({ expanded });
  }

  render() {
    return (
      <PopUp title="Create New Role" isOpen={this.state.isOpen} onSubmit={this.handleSubmit} handleClose={this.handleClose} width="500px" {...this.props}>
        <DynamicForm
          name={roleConfig.key}
          ref={this.ref}
          fields={this.fields}
          layout={roleConfig.forms.caseNotes.metadata.layout.rows}
          colWidth={6}
          onSubmit={this.handleSubmit}
          {...this.props}
        />
        <div className="form-flex-col-css field-group">
          <Label>Permissions:<span className="required-field"> *</span></Label>
          <div className={(this.state.treeShowError) ? 'invalid-field permissionTree input-maxwidth' : 'permissionTree input-maxwidth'}>
            <CommonCheckboxTree
              checked={this.state.checked}
              expanded={this.getExpanded(this.state.expanded, this.props.roles.permissionGroups)}
              nodes={this.getPermissionTreeNodes(this.props.roles.permissionGroups)}
              onCheck={this.onCheck}
              onExpand={this.onExpand}
              showNodeIcon={false}
              showExpandAll
            />
          </div>
          <Fragment>
            {(this.state.treeShowError) ?
              <Label className="field-error-message">Please make selections</Label> : <Fragment />}
          </Fragment>
        </div>
      </PopUp>
    );
    /* */
  }
}

const mapStateToProps = (state: RootState) => ({
  authentication: state.authentication,
  roles: state.roles,
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

export default connect(mapStateToProps, null)(RolesCreate);
