import React, { Fragment } from 'react';
import UserAuditGrid from './grid';

class UserAuditFrame extends React.Component<any, any> {
    private ref: any;

    constructor(props:any) {
      super(props);
      this.ref = React.createRef();
    }

    render() {
      return (
        <Fragment>
          <div className="col-md-12" style={{ height: 'inherit' }}>
            <UserAuditGrid ref={this.ref} {...this.props} />
          </div>
        </Fragment>
      );
    }
}

export default UserAuditFrame;
