import {
  GET_ALL_USER_AUDIT_FULFILLED,
} from './user-audit-actions';

const defaultState = { userAudit: [] };

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_USER_AUDIT_FULFILLED:
      return { ...state, userAudit: action.payload.data.data };
    default:
      return state;
  }
};
export default moduleReducer;
