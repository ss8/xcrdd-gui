import { dateFilterParams } from '../../../shared/constants/index';

export default [
  {
    headerName: 'Username', field: 'userName', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Service', field: 'serviceId', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Action Type', field: 'actionType', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Record Name', field: 'recordName', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'URL', field: 'endPointUrl', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Fields', field: 'fieldDetails', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Time', field: 'dateTimeEntry', sortable: true, filter: 'agDateColumnFilter', resizable: true, filterParams: dateFilterParams, cellRenderer: 'datetimeCellRenderer',
  },
  {
    headerName: 'Status', field: 'status', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'IP Address', field: 'clientIPAddress', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
];
