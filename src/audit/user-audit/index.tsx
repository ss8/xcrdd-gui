import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllUserAudit,
} from './user-audit-actions';
import { getUserSettings, putUserSettings } from '../../users/users-actions';
import ModuleRoutes from './user-audit-routes';

const UserAudit = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  userAudit: state.userAudit,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllUserAudit,
  getUserSettings,
  putUserSettings,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserAudit));
