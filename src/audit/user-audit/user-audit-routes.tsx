import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import UserAuditFrame from './user-audit-frame';

const moduleRoot = '/userAudit';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <UserAuditFrame {...props} />}
    />
  </Fragment>
);

export default ModuleRoutes;
