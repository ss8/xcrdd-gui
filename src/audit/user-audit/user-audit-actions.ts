import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_USER_AUDIT = 'GET_ALL_USER_AUDIT';
export const GET_ALL_USER_AUDIT_FULFILLED = 'GET_ALL_USER_AUDIT_FULFILLED';

// action creators
export const getAllUserAudit = (body = '') => ({ type: GET_ALL_USER_AUDIT, payload: http.get(`/auditAction?${body}`) });
