import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import AuditFrame from './audit-frame';

const moduleRoot = '/audit';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <AuditFrame {...props} />}
    />
  </Fragment>
);

export default ModuleRoutes;
