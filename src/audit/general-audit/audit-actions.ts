import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_AUDIT = 'GET_ALL_AUDIT';
export const GET_AUDIT_BY_ID = 'GET_AUDIT_BY_ID';
export const GET_ALL_AUDIT_FULFILLED = 'GET_ALL_AUDIT_FULFILLED';
export const GET_AUDIT_BY_ID_FULFILLED = 'GET_AUDIT_BY_ID_FULFILLED';

// action creators
export const getAllAudit = (body = '') => ({ type: GET_ALL_AUDIT, payload: http.get(`/audit?${body}`) });
export const getAuditById = (id: number) => ({ type: GET_AUDIT_BY_ID, payload: http.get(`/audit/${id}`) });
