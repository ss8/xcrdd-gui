import { dateFilterParams } from '../../../shared/constants/index';

export default [
  {
    headerName: 'Response Status', field: 'respStatus', sortable: true, filter: 'agNumberColumnFilter', resizable: true,
  },
  {
    headerName: 'User Name', field: 'userName', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'http Method', field: 'httpMethod', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'URL',
    field: 'url',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
  },
  {
    headerName: 'ID', field: 'id', sortable: true, filter: 'agNumberColumnFilter', resizable: true,
  },
  {
    headerName: 'IP Address', field: 'ipAddress', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Request Body', field: 'reqBody', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Request Parameters', field: 'reqParams', sortable: true, filter: 'agTextColumnFilter', resizable: true,
  },
  {
    headerName: 'Request Time',
    field: 'reqTime',
    sortable: true,
    filter: 'agDateColumnFilter',
    resizable: true,
    filterParams: dateFilterParams,
    cellRenderer: 'datetimeCellRenderer',
  },
  {
    headerName: 'Session ID',
    field: 'sessionId',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
  },
];
