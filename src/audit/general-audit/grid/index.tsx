import React, { Fragment } from 'react';
import AuditGridColumnDefs from './audit-grid-column-defs';
import CommonGrid, { ICommonGridProperties } from '../../../shared/commonGrid';
import DateTimeCellRenderer from '../../../shared/commonGrid/date-cell-renderer';
import { ROWSELECTION } from '../../../shared/commonGrid/grid-enums';

class AuditGrid extends React.Component<any, any> {
    AUDIT_GRID_SETTINGS = 'auditGridSettings';

    columnDefs = AuditGridColumnDefs;

    handleSelection = (e: any) => {
      this.setState({ selection: e.api.getSelectedRows() });
    };

    handleGetResultsFromServer = (body:any) => {
      return this.props.getAllAudit(body);
    }

    handleGetGridSettingsFromServer = () => {
      return this.props.getUserSettings(this.AUDIT_GRID_SETTINGS, this.props.authentication.userId);
    }

    saveGridSettings = (auditGridSettings: { [key: string]: any }) => {
      if (auditGridSettings !== null) {
        this.props.putUserSettings(this.AUDIT_GRID_SETTINGS, this.props.authentication.userId,
          auditGridSettings);
      }
    }

    render() {
      const frameworkComponents = {
        datetimeCellRenderer: DateTimeCellRenderer,
      };
      const commonGridProps: ICommonGridProperties = {
        onSelectionChanged: this.handleSelection,
        rowSelection: ROWSELECTION.SINGLE,
        columnDefs: this.columnDefs,
        rowData: this.props.audit.audit,
        context: this,
        pagination: true,
        noResults: false,
        frameworkComponents,
        getGridResultsFromServer: this.handleGetResultsFromServer,
        renderDataFromServer: true,
        getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
        saveGridSettings: this.saveGridSettings,
        isBeforeLogout: this.props.authentication.beforeLogout,
      };
      return (
        <Fragment>
          <div style={{ height: 'inherit' }}>
            <CommonGrid {...commonGridProps} />
          </div>
        </Fragment>
      );
    }
}

export default AuditGrid;
