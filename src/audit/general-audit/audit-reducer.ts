import {
  GET_ALL_AUDIT_FULFILLED,
} from './audit-actions';

const defaultState = { audit: [] };

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_AUDIT_FULFILLED:
      return { ...state, audit: action.payload.data.data };
    default:
      return state;
  }
};
export default moduleReducer;
