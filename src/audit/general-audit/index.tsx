import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllAudit,
  getAuditById,
} from './audit-actions';
import { getUserSettings, putUserSettings } from '../../users/users-actions';
import ModuleRoutes from './audit-routes';

const Audit = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  audit: state.audit,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllAudit,
  getAuditById,
  getUserSettings,
  putUserSettings,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Audit));
