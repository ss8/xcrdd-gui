import React, { Fragment } from 'react';
import AuditGrid from './grid';

class AuditFrame extends React.Component<any, any> {
    private ref: any;

    constructor(props:any) {
      super(props);
      this.ref = React.createRef();
    }

    render() {
      return (
        <Fragment>
          <div className="col-md-12" style={{ height: 'inherit' }}>
            <AuditGrid ref={this.ref} {...this.props} />
          </div>
        </Fragment>
      );
    }
}

export default AuditFrame;
