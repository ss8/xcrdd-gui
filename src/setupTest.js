// Add any setup required for Jest in this file.
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import moment from 'moment-timezone';
import '@testing-library/jest-dom';
import { LicenseManager, ModuleRegistry, AllModules } from '@ag-grid-enterprise/all-modules';

configure({ adapter: new Adapter() });

moment.tz.setDefault('America/Los_Angeles');

LicenseManager.setLicenseKey('SS8_Networks_MultiApp_1Devs17_July_2020__MTU5NDk0MDQwMDAwMA==dbf856c42e67d7b8eaad22a5e449b61a');
ModuleRegistry.registerModules(AllModules);

document.createRange = () => ({
  setStart: jest.fn(),
  setEnd: jest.fn(),
  commonAncestorContainer: {
    nodeName: 'BODY',
    ownerDocument: document,
  },
});
