export const LANGUAGE_EN_US = 'en-US';
export const LANGUAGE_EN_GB = 'en-GB';

export const DATE_TIME_FORMAT_US = 'MM/DD/YY hh:mm:ss A';
export const DATE_TIME_FORMAT_GB = 'DD/MM/YY HH:mm:ss';

export const WARRANT_ROUTE = '/warrants';
export const CASE_ROUTE = '/cases';
export const AF_ROUTE = '/afs';
export const CF_ROUTE = '/cfs';
export const SYSTEM_SETTINGS_ROUTE = '/systemSettings';
export const DISTRIBUTORS_ROUTE = '/distributors';
export const SECURITY_GATEWAY_ROUTE = '/securityGateway';
