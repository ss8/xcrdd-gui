import { RootState } from 'data/root.reducers';

// eslint-disable-next-line import/prefer-default-export
export const getSelectedDeliveryFunctionId = (state: RootState): string => state.global.selectedDF;
