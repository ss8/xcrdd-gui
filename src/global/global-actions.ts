import Http from '../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_DFS = 'GET_ALL_DFS';
export const GET_ALL_DFS_FULFILLED = 'GET_ALL_DFS_FULFILLED';
export const SET_DF = 'SET_DF';

// action creators
export const getAllDFs = () => ({ type: GET_ALL_DFS, payload: http.get('/dfs') });
export const setDF = (dfId: number) => ({ type: SET_DF, payload: dfId });
