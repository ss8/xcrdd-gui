import {
  GET_ALL_DFS_FULFILLED,
  SET_DF,
} from './global-actions';

const defaultState = {
  dfs: [],
  selectedDF: 0,
  error: null,
};

const moduleReducer = (state = defaultState, action: any) => {
  switch (action.type) {
    case GET_ALL_DFS_FULFILLED:
      return { ...state, dfs: action.payload.data.data };
    case SET_DF:
      return { ...state, selectedDF: action.payload };
    default:
      return state;
  }
};
export default moduleReducer;
