import configureStore, { MockStoreCreator } from 'redux-mock-store';
import rootReducer, { RootState } from 'data/root.reducers';
import {
  getSelectedDeliveryFunctionId,
} from './global-selectors';
import { setDF } from './global-actions';

describe('global-selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of securityInfos', () => {
    store = mockStore(rootReducer(undefined, setDF(0)));
    expect(getSelectedDeliveryFunctionId(store.getState())).toEqual(0);
  });
});
