import { Team, TeamFormData } from 'data/team/team.types';
import { User } from 'data/user/user.types';
import { AuditDetails } from 'data/types';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import getTeamDataToSubmit from './teamDataToSubmit';

describe('getTeamDataToSubmit', () => {
  const userId = '29';
  const userName = 'userName';

  describe('when create team', () => {
    const teamID = '0';
    let teamFormData: TeamFormData;
    let users: User[];
    let expectedTeam: Team;
    let expectedAuditDetails: AuditDetails;

    beforeEach(() => {
      teamFormData = {
        name: 'TEAM',
        description: '',
        members: ['anna', 'automation_test'],
      };

      users = [
        {
          agreements: 0,
          datecreated: '2020-08-21T07:53:28.000+0000',
          dateupdated: '2020-08-21T07:53:28.000+0000',
          dept: 'Engineering',
          displayName: 'anna',
          email: 'test1@deleteme.ss8.com',
          firstName: 'anna',
          id: '2',
          lastLoginAddress: null,
          lastLoginMessageSeen: null,
          lastName: 'lastName User 1',
          lastPasswordChange: null,
          lastlogin: null,
          middleName: null,
          office: 'SS8-Milpitas',
          password: '8f3e0941a3234b82350b01fc899779db8c480809d5bd4677',
          phone: '408-000-0000',
          reporting: { assignedRoles: Array(4) },
          role: 'ga_role',
          state: 'new-password',
          superior: 0,
          userID: 'anna',
        },
        {
          agreements: 0,
          datecreated: '2020-08-21T07:53:28.000+0000',
          dateupdated: '2020-08-21T07:53:28.000+0000',
          dept: 'Engineering',
          displayName: 'automation_test',
          email: 'test1@deleteme.ss8.com',
          firstName: 'automation_test',
          id: '3',
          lastLoginAddress: null,
          lastLoginMessageSeen: null,
          lastName: 'lastName User 2',
          lastPasswordChange: null,
          lastlogin: null,
          middleName: null,
          office: 'SS8-Milpitas',
          password: '8f3e0941a3234b82350b01fc899779db8c480809d5bd4677',
          phone: '408-000-0000',
          reporting: { assignedRoles: Array(4) },
          role: 'ga_role',
          state: 'new-password',
          superior: 0,
          userID: 'automation_test',
        },
      ];

      expectedTeam = {
        name: 'TEAM',
        description: '',
        members: [
          {
            belongsTo: '0',
            user: '2',
            team: 0,
            defaultTeam: false,
          },
          {
            belongsTo: '0',
            user: '3',
            team: 0,
            defaultTeam: false,
          },
        ],
        id: '0',
      };

      expectedAuditDetails = {
        auditEnabled: true,
        userId: '29',
        userName: 'userName',
        fieldDetails: '[{"field":"name","value":"TEAM"},{"field":"description","value":""},{"field":"members","value":["anna","automation_test"]}]',
        recordName: 'TEAM',
        service: AuditService.TEAM,
        actionType: AuditActionType.TEAM_ADD,
      };
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTeamDataToSubmit(
          teamFormData,
          users,
          teamID,
          null,
          false,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedTeam,
      });
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTeamDataToSubmit(
          teamFormData,
          users,
          teamID,
          null,
          true,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedTeam,
        auditDetails: expectedAuditDetails,
      });
    });
  });

  describe('when edit team', () => {
    let teamFormData: TeamFormData;
    let users: User[];
    let teamID: string;
    let originalTeamData: TeamFormData;
    let expectedTeam: Team;
    let expectedAuditDetails: AuditDetails;

    beforeEach(() => {
      teamID = 'T8';

      teamFormData = {
        name: 'TEAM',
        description: '',
        members: ['anna', 'automation_test'],
      };

      originalTeamData = {
        id: 'T8',
        name: 'TEAM',
        description: '',
        members: ['anna', 'ken'],
      };

      users = [
        {
          agreements: 0,
          datecreated: '2020-08-21T07:53:28.000+0000',
          dateupdated: '2020-08-21T07:53:28.000+0000',
          dept: 'Engineering',
          displayName: 'anna',
          email: 'test1@deleteme.ss8.com',
          firstName: 'anna',
          id: '2',
          lastLoginAddress: null,
          lastLoginMessageSeen: null,
          lastName: 'lastName User 1',
          lastPasswordChange: null,
          lastlogin: null,
          middleName: null,
          office: 'SS8-Milpitas',
          password: '8f3e0941a3234b82350b01fc899779db8c480809d5bd4677',
          phone: '408-000-0000',
          reporting: { assignedRoles: Array(4) },
          role: 'ga_role',
          state: 'new-password',
          superior: 0,
          userID: 'anna',
        },
        {
          agreements: 0,
          datecreated: '2020-08-21T07:53:28.000+0000',
          dateupdated: '2020-08-21T07:53:28.000+0000',
          dept: 'Engineering',
          displayName: 'automation_test',
          email: 'test1@deleteme.ss8.com',
          firstName: 'automation_test',
          id: '3',
          lastLoginAddress: null,
          lastLoginMessageSeen: null,
          lastName: 'lastName User 2',
          lastPasswordChange: null,
          lastlogin: null,
          middleName: null,
          office: 'SS8-Milpitas',
          password: '8f3e0941a3234b82350b01fc899779db8c480809d5bd4677',
          phone: '408-000-0000',
          reporting: { assignedRoles: Array(4) },
          role: 'ga_role',
          state: 'new-password',
          superior: 0,
          userID: 'automation_test',
        },
        {
          agreements: 0,
          datecreated: '2020-08-21T07:53:28.000+0000',
          dateupdated: '2020-08-21T07:53:28.000+0000',
          dept: 'Engineering',
          displayName: 'ken',
          email: 'test1@deleteme.ss8.com',
          firstName: 'ken',
          id: '4',
          lastLoginAddress: null,
          lastLoginMessageSeen: null,
          lastName: 'lastName User 2',
          lastPasswordChange: null,
          lastlogin: null,
          middleName: null,
          office: 'SS8-Milpitas',
          password: '8f3e0941a3234b82350b01fc899779db8c480809d5bd4677',
          phone: '408-000-0000',
          reporting: { assignedRoles: Array(4) },
          role: 'ga_role',
          state: 'new-password',
          superior: 0,
          userID: 'ken',
        },
      ];

      expectedTeam = {
        name: 'TEAM',
        description: '',
        members: [
          {
            belongsTo: 'T8',
            user: '2',
            team: 0,
            defaultTeam: false,
          },
          {
            belongsTo: 'T8',
            user: '3',
            team: 0,
            defaultTeam: false,
          },
        ],
        id: 'T8',
      };

      expectedAuditDetails = {
        auditEnabled: true,
        userId: '29',
        userName: 'userName',
        fieldDetails: '[{"field":"members","before":["anna","ken"],"after":["anna","automation_test"]}]',
        recordName: 'TEAM',
        service: AuditService.TEAM,
        actionType: AuditActionType.TEAM_EDIT,
      };
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTeamDataToSubmit(
          teamFormData,
          users,
          teamID,
          originalTeamData,
          false,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedTeam,
      });
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTeamDataToSubmit(
          teamFormData,
          users,
          teamID,
          originalTeamData,
          true,
          userId,
          userName,
        ),
      ).toEqual({
        data: expectedTeam,
        auditDetails: expectedAuditDetails,
      });
    });
  });

});
