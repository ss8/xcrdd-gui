import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';
import { WithAuditDetails, AuditDetails } from 'data/types';
import { Team, Member, TeamFormData } from 'data/team/team.types';
import { User } from 'data/user/user.types';

export default function getTeamDataToSubmit(
  teamFormData: TeamFormData,
  users: User[],
  teamID: string,
  originalTeamData: TeamFormData | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
): WithAuditDetails<Team> | undefined {

  // const updatedTeam = this.ref.current.getValues();
  // const users = this.props.users.users.data;
  // const teamID = this.state.data.id;
  let memberArray:Member[] = [];
  users.forEach((user:User) => {
    (teamFormData as TeamFormData).members.forEach((memberName:string) => {
      if (user.userID === memberName) {
        const member:Member = {
          belongsTo: teamID,
          user: user.id,
          team: 0,
          defaultTeam: false,
        };
        memberArray = [...memberArray, member];
      }
    });
  });

  const teamData: Team = { ...teamFormData, members: memberArray, id: teamID };

  const teamBody : WithAuditDetails<Team> = {
    data: teamData as Team,
  };

  if (isAuditEnabled) {
    const auditDetails = getAuditDetails(
      isAuditEnabled,
      userId,
      userName,
      getAuditFieldInfo(teamFormData, null, originalTeamData != null, originalTeamData),
      (teamData as Team).name,
      AuditService.TEAM,
      originalTeamData ? AuditActionType.TEAM_EDIT : AuditActionType.TEAM_ADD,
    );
    teamBody.auditDetails = auditDetails as AuditDetails;
  }
  return teamBody;
}
