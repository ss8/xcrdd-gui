import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, {
  shallow,
  ShallowWrapper,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { TeamCreate } from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('TeamCreate component can render', () => {
  it('throws an exception if TeamCreate are not rendering', () => {
    const div = document.createElement('div');
    expect((props : any) => {
      ReactDOM.render(<TeamCreate {...props} />, div);
    }).toThrow();
  });
});

describe('TeamCreate component rendering', () => {
  let component: ShallowWrapper;
  const users = [
    { label: 'USER2', value: 'USER2' },
    { label: 'USer3', value: 'USer3' },
    { label: 'User4', value: 'User4' },
    { label: 'anna', value: 'anna' },
  ];
  const team = {
    id: 'T8',
    name: 'TEAM',
    description: '',
    members: ['anna', 'ken'],
  };
  const props = { users: { data: users } };
  const getAllUsers = jest.fn(() => props);

  beforeEach(() => {
    component = shallow(<TeamCreate users={props} getAllUsers={getAllUsers} />);
  });

  afterEach(() => {
    component.unmount();
  });

  it('TeamCreate component is existing', () => {
    expect(component.exists()).toBe(true);
  });

  it('TeamCreate popup is existing', () => {
    const ele = component.find('PopUp');
    expect(ele.length).toBe(1);
  });

  it('TeamCreate form is existing', () => {
    const elem = component.find('Form');
    expect(elem.length).toBe(1);
  });

  it('TeamCreate componentDidMount being called once', () => {
    const spy = jest.spyOn(TeamCreate.prototype, 'componentDidMount');
    const localComponent = shallow(
      <TeamCreate users={props} getAllUsers={getAllUsers} />,
    );
    expect(spy).toHaveBeenCalledTimes(1);
    localComponent.unmount();
  });

  it('TeamCreate getFields being called once', () => {
    const localComponent: ShallowWrapper<any, any, TeamCreate> = shallow(
      <TeamCreate users={props} getAllUsers={getAllUsers} />,
    );
    const instance = localComponent.instance();
    const spy = jest.spyOn(instance, 'getFields');
    instance.getFields();
    expect(spy).toHaveBeenCalledTimes(1);
    localComponent.unmount();
  });

  it('TeamCreate handleClose being called once', () => {
    const history = { goBack: jest.fn() };
    const localComponent: ShallowWrapper<any, any, TeamCreate> = shallow(
      <TeamCreate users={props} getAllUsers={getAllUsers} history={history} />,
    );
    const instance = localComponent.instance();
    const spy = jest.spyOn(instance, 'handleClose');
    instance.handleClose();
    expect(spy).toHaveBeenCalledTimes(1);
    localComponent.unmount();
  });
});
