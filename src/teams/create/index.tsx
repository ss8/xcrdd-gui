import React from 'react';
import { RootState } from 'data/root.reducers';
import { connect } from 'react-redux';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import DynamicForm from '../../shared/form';
import teamCreate from './team-create-form.json';
import PopUp from '../../shared/popup/dialog';
import { AppToaster } from '../../shared/toaster';
import getTeamDataToSubmit from '../utils/teamDataToSubmit';

export class TeamCreate extends React.Component<any, any> {
  private ref: React.RefObject<DynamicForm>;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      isOpen: true,

    };
  }

  getFields = () => {
    const { fields } = teamCreate.forms.caseNotes;
    const { data: users = [] } = this.props.users.users;
    fields.members.options = users.map((element: any) => {
      return { label: element.userID, value: element.userID };
    });

    return fields;
  }

  componentDidMount() {
    this.props.getAllUsers();
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (!this.ref.current ||
      this.ref.current.isValid(true) === false) {
      return;
    }

    const users = this.props.users.users.data;
    const newTeamContent = this.ref.current.getValues();
    const body = getTeamDataToSubmit(
      newTeamContent,
      users,
      '0',
      null,
      this.props.isAuditEnabled,
      this.props.authentication.userDBId,
      this.props.authentication.userId,
    );
    this.props.createTeamAndRefresh(body, this.props.history);
  }

  handleClose = () => {
    AppToaster.clear();
    this.setState({ isOpen: false });
    this.props.history.goBack();
  }

  render() {
    return (
      <PopUp title="Create New Team" isOpen={this.state.isOpen} onSubmit={this.handleSubmit} handleClose={this.handleClose} width="500px" {...this.props}>
        <DynamicForm
          name={teamCreate.key}
          ref={this.ref}
          fields={this.getFields()}
          layout={teamCreate.forms.caseNotes.metadata.layout.rows}
          colWidth={6}
          onSubmit={this.handleSubmit}
          {...this.props}
        />
      </PopUp>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  authentication: state.authentication,
  roles: state.roles,
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

export default connect(mapStateToProps, null)(TeamCreate);
