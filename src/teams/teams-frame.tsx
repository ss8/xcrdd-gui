import React, { Fragment } from 'react';
import TeamsGrid from './grid';
import { AppToaster } from '../shared/toaster';
import convertErrorCodeToMessage from '../shared/errorCodeConvert/convertErrorCodeToMessage';

class TeamsFrame extends React.Component<any, any> {
  handleChange = (e: any) => {
    this.setState({ selectedTab: e });
  }

  handleViewChange = (e: any) => {
    this.setState({ selectedView: e.currentTarget.id });
  }

  componentDidUpdate() {
    const { error } = this.props.teams;
    const message = convertErrorCodeToMessage(error);
    if (error.errorCode != null || error.errorMessage != null) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${message}`,
      });
      this.props.clearErrors();
    }
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/authentication/login');
    }
  }

  render() {
    return (
      <Fragment>
        <div className="col-md-12" style={{ height: 'inherit' }}>
          <TeamsGrid {...this.props} />
        </div>
      </Fragment>
    );
  }
}

export default TeamsFrame;
