import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';
import DynamicForm from '../../shared/form';
import teamEdigConf from './team-edit-form.json';
import TEAM_PRIVILEGE from '../team-enum';
import { AppToaster } from '../../shared/toaster';
import getTeamDataToSubmit from '../utils/teamDataToSubmit';

class TeamEdit extends React.Component<any, any> {
  private ref: React.RefObject<DynamicForm>;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      data: this.props.location.data,
      currentStateMode: this.props.location.action,
      fieldDirty: false,
    };
  }

  componentDidMount() {
    this.props.getAllUsers();
    this.setState({
      data: this.props.location.data,
      currentStateMode: this.props.location.action,
    });
    this.handleStateModeChanged(this.props.location.action);
  }

  getFields = () => {
    const { fields } = teamEdigConf.forms.caseNotes;
    const users = this.props.users.users.data;
    fields.members.options = users.map((element: any) => {
      return { label: element.userID, value: element.userID };
    });

    return fields;
  }

  handleClose = () => {
    AppToaster.clear();
    if (!this.props.history) return;
    this.props.history.goBack();
  }

  handleSubmit = () => {
    AppToaster.clear();
    if (!this.ref.current ||
      this.ref.current.isValid(true) === false) {
      return;
    }
    const updatedTeam = this.ref.current.getValues();
    const users = this.props.users.users.data;
    const teamID = this.state.data.id;
    const body = getTeamDataToSubmit(
      updatedTeam,
      users,
      teamID,
      this.props.location.data,
      this.props.isAuditEnabled,
      this.props.authentication.userDBId,
      this.props.authentication.userId,
    );
    this.props.updateTeamAndRefresh(teamID, body, this.props.history);
  }

  handleFieldUpdate = () => {
    this.setState({ fieldDirty: true });
  }

  setReadOnly = (readOnly:boolean) => {
    const { fields } = teamEdigConf.forms.caseNotes;
    Object.keys(fields).forEach((key: any) => {
      (fields as any)[key].readOnly = readOnly;
    });
    this.setState({ fields });
    this.setState({ fieldDirty: readOnly });
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    this.setState({ currentStateMode: stateMode });
    if (stateMode === CONFIG_FORM_MODE.Edit) {
      this.setReadOnly(false);
    } else {
      this.setReadOnly(true);
    }
  }

  getTitle = (currentStateMode : CONFIG_FORM_MODE) => {
    return (currentStateMode === CONFIG_FORM_MODE.Edit) ? 'Edit Team' : 'View Team';
  }

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToEdit = privileges !== undefined
       && (privileges.includes(TEAM_PRIVILEGE.Edit) || privileges.includes(TEAM_PRIVILEGE.Admin));

    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: this.state.currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: true,
      width: '800px',
      height: 'auto',
      overflow: 'auto',
    };

    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          <DynamicForm
            name={teamEdigConf.key}
            ref={this.ref}
            fields={this.getFields()}
            layout={teamEdigConf.forms.caseNotes.metadata.layout.rows}
            colWidth={6}
            onSubmit={this.handleSubmit}
            currentStateMode={this.state.currentStateMode}
            defaults={this.state.data}
            fieldUpdateHandler={this.handleFieldUpdate}
            {...this.props}
          />
        </ConfigFormPopup>
      </Fragment>
    );
  }
}
const mapStateToProps = (state: RootState) => ({
  authentication: state.authentication,
  roles: state.roles,
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

export default connect(mapStateToProps, null)(TeamEdit);
