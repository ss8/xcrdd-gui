import { History } from 'history';
import { WithAuditDetails } from '../data/types';
import Http from '../shared/http';
import { showSuccessMessage, showError, TOAST_TIMEOUT } from '../shared/toaster';
import { extractErrorMessage } from '../utils';

const http = Http.getHttp();

// action export constants
export const GET_ALL_TEAMS = 'GET_ALL_TEAMS';
export const GET_TEAM_BY_ID = 'GET_TEAM_BY_ID';
export const UPDATE_TEAM = 'UPDATE_TEAM';
export const DELETE_TEAM = 'DELETE_TEAM';
export const CREATE_TEAM = 'CREATE_TEAM';
export const GET_ALL_TEAMS_FULFILLED = 'GET_ALL_TEAMS_FULFILLED';
export const GET_TEAM_BY_ID_FULFILLED = 'GET_TEAM_BY_ID_FULFILLED';
export const UPDATE_TEAM_FULFILLED = 'UPDATE_TEAM_FULFILLED';
export const DELETE_TEAM_FULFILLED = 'DELETE_TEAM_FULFILLED';
export const CREATE_TEAM_FULFILLED = 'CREATE_TEAM_FULFILLED';
export const CLEAR_TEAM_FULFILLED = 'CLEAR_TEAM_FULFILLED';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

// action creators
export const getAllTeams = () => ({ type: GET_ALL_TEAMS, payload: http.get('/teams') });
export const getTeamById = (id: number) => ({ type: GET_TEAM_BY_ID, payload: http.get(`/teams/${id}`) });
export const createTeam = (body: any) => ({ type: CREATE_TEAM, payload: http.post('/teams', body) });
export const updateTeam = (id: number, body: any) => ({ type: UPDATE_TEAM, payload: http.put(`/teams/${id}`, body) });
export const deleteTeam = (id: number, body: WithAuditDetails<void>) => ({ type: DELETE_TEAM, payload: http.delete(`/teams/${id}`, { data: body.auditDetails }) });
export const clearTeam = () => ({ type: CLEAR_TEAM_FULFILLED });
export const clearErrors = () => ({ type: CLEAR_ERRORS });

function goToPreviousPage(history: History): () => void {
  return () => {
    history.goBack();
  };
}

export const createTeamAndRefresh = (body: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(createTeam(body));
      dispatch(goToPreviousPage(history));
      showSuccessMessage('Team successfully created.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to create team: ${errorMessage}.`);

    } finally {
      dispatch(getAllTeams());
    }
  };
};
export const deleteTeamAndRefresh = (id: any, body: WithAuditDetails<void>) => {
  return async (dispatch: any) => {
    try {
      await dispatch(deleteTeam(id, body));
      showSuccessMessage('Team successfully deleted.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to delete team: ${errorMessage}.`);

    } finally {
      dispatch(getAllTeams());
    }
  };
};
export const updateTeamAndRefresh = (id: number, body: any, history: History) => {
  return async (dispatch: any) => {
    try {
      await dispatch(updateTeam(id, body));
      dispatch(goToPreviousPage(history));
      showSuccessMessage('Team successfully updated.', TOAST_TIMEOUT);
    } catch (e) {
      const errorMessage = extractErrorMessage(e);
      showError(`Unable to update team: ${errorMessage}.`);

    } finally {
      dispatch(getAllTeams());
    }
  };
};
