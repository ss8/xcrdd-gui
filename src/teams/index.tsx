import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllTeams,
  getTeamById,
  createTeam,
  updateTeam,
  deleteTeam,
  clearTeam,
  createTeamAndRefresh,
  deleteTeamAndRefresh,
  updateTeamAndRefresh,
  clearErrors,
} from './teams-actions';
import { getAllUsers, getUserSettings, putUserSettings } from '../users/users-actions';

import ModuleRoutes from './teams-routes';

const Teams = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  teams: state.teams,
  users: state.users,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllTeams,
  getTeamById,
  createTeam,
  updateTeam,
  deleteTeam,
  clearTeam,
  createTeamAndRefresh,
  deleteTeamAndRefresh,
  updateTeamAndRefresh,
  getAllUsers,
  getUserSettings,
  putUserSettings,
  clearErrors,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Teams));
