import { COMMON_GRID_COLUMN_FIELD_NAMES } from '../../shared/commonGrid/grid-enums';
import { stringArrayComparator } from '../../shared/commonGrid/grid-util';

export default [
  {
    headerName: '', field: COMMON_GRID_COLUMN_FIELD_NAMES.Action, cellRenderer: 'actionMenuCellRenderer',
  },
  {
    headerName: 'Name', field: 'name', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'name',
  },
  {
    headerName: 'Description', field: 'description', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'description',
  },
  {
    headerName: 'Number of Members', field: 'num_of_members', filter: 'agNumberColumnFilter', resizable: true, sortable: true, tooltipField: 'num_of_members',
  },
  {
    headerName: 'Members',
    field: 'members',
    sortable: true,
    resizable: true,
    filter: 'agTextColumnFilter',
    filterParams: { textCustomComparator: stringArrayComparator },
    tooltipField: 'members',
  },

];
