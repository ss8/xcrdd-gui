import React from 'react';
import CommonGrid, { ICommonGridProperties } from '../../shared/commonGrid';
import TeamGridColumnDefs from './team-grid-column-defs';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../shared/commonGrid/grid-enums';
import TEAM_PRIVILEGE from '../team-enum';

class TeamsGrid extends React.Component<any, any> {
 TEAM_GRID_SETTINGS = 'teamGridSettings';

    columnDefs = TeamGridColumnDefs;

  state = {
    selection: [],
  }

  saveGridSettings = (teamGridSettings: {[key: string]: any}) => {
    if (teamGridSettings !== null) {
      this.props.putUserSettings(this.TEAM_GRID_SETTINGS, this.props.authentication.userId,
        teamGridSettings);
    }
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
  }

  componentDidMount() {
    this.props.getAllTeams();
    this.props.getAllUsers();
  }

  componentWillUnmount() {
    this.props.clearTeam();
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getUserSettings(this.TEAM_GRID_SETTINGS, this.props.authentication.userId);
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    // eslint-disable-next-line prefer-object-spread
    const team = Object.assign({}, this.state.selection[0]);
    if (type === COMMON_GRID_ACTION.Delete) {
      this.props.history.push({ pathname: '/teams/delete', state: this.state });
    } else if (type === COMMON_GRID_ACTION.Edit) {
      this.props.history.push({ pathname: '/teams/edit', action: 'edit', data: team });
    } else {
      this.props.history.push({ pathname: '/teams/view', action: 'view', data: team });
    }
  }

  handleCreateNew = () => { this.props.history.push({ pathname: '/teams/create', action: 'create' }); }

  handleRefresh = () => {
    this.props.getAllTeams();
  }

  render() {
    const { privileges } = this.props.authentication;
    const curTeams: any = [];
    const { teams } = this.props.teams;
    const { data: users = [] } = this.props.users.users;

    if (teams.length > 0) {
      teams.forEach((team: any) => {
        const curMemberNameArry:any = [];
        team.members.forEach((member:any) => {
          users.forEach((user:any) => {
            if (user.id === member) {
              curMemberNameArry.push(user.userID);
            }
          });
        });
        curTeams.push({ ...team, members: curMemberNameArry });
      });
    }

    const hasPrivilegesToCreate = privileges !== undefined &&
      (privileges.includes(TEAM_PRIVILEGE.Edit) || privileges.includes(TEAM_PRIVILEGE.Admin));
    const hasPrivilegeToDelete = privileges !== undefined &&
      (privileges.includes(TEAM_PRIVILEGE.Delete) || privileges.includes(TEAM_PRIVILEGE.Admin));
    const hasPrivilegetoView = privileges !== undefined &&
      (privileges.includes(TEAM_PRIVILEGE.View) || privileges.includes(TEAM_PRIVILEGE.Admin));

    const commonGridProps: ICommonGridProperties = {
      onRefresh: this.handleRefresh,
      onSelectionChanged: this.handleSelection,
      onActionMenuItemClick: this.onActionMenuItemClick,
      rowSelection: ROWSELECTION.SINGLE,
      columnDefs: this.columnDefs,
      rowData: curTeams,
      context: this,
      pagination: true,
      enableSorting: true,
      enableFilter: true,
      enableBrowserTooltips: true,
      hasPrivilegetoView,
      hasPrivilegeToCreateNew: hasPrivilegesToCreate,
      hasPrivilegeToDelete,
      handleCreateNew: this.handleCreateNew,
      createNewText: 'New Team',
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
      noResults: (!this.props.teams || !this.props.teams.teams ||
        !this.props.teams.teams.length),
    };
    return (
      <div style={{ height: 'inherit' }} className="ag-theme-balham">
        <CommonGrid {...commonGridProps} />
      </div>
    );
  }
}

export default TeamsGrid;
