import {
  GET_ALL_TEAMS_FULFILLED,
  // GET_TEAM_BY_ID_FULFILLED,
  CREATE_TEAM_FULFILLED,
  UPDATE_TEAM_FULFILLED,
  DELETE_TEAM_FULFILLED,
  CLEAR_TEAM_FULFILLED,
  CLEAR_ERRORS,
} from './teams-actions';

const defaultState = {
  teams: [],
  error: { errorMessage: null, errorCode: null },
};

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED
  if (/TEAM.*_REJECTED/.test(action.type)) {
    let errorFieldName = '';
    let errorCode = action.payload.response.status;
    let errorMessage = action.payload.response.statusText;
    if (action.payload.response.data && action.payload.response.data !== '') {
      errorFieldName = action.payload.response.config.data ?
        JSON.parse(action.payload.response.config.data).name : null;
      errorCode = action.payload.response.data.error.code;
      errorMessage = action.payload.response.data.error.message;
    }
    return { ...state, error: { errorMessage, errorCode, errorFieldName } };
  }

  switch (action.type) {
    case GET_ALL_TEAMS_FULFILLED:
      const teams = action.payload.data.data;
      let teamWithNeededProperty;
      let teamsWithNeededProperty:any = [];
      teams.forEach((team: any) => {
        teamWithNeededProperty = {
          id: team.id,
          name: team.name,
          description: team.description,
          num_of_members: team.members.length,
          members: team.members.map((member: any) => member.user),
        };
        teamsWithNeededProperty = [...teamsWithNeededProperty, teamWithNeededProperty];
      });
      return { ...state, teams: teamsWithNeededProperty };
    case CREATE_TEAM_FULFILLED:
      return state;
    case DELETE_TEAM_FULFILLED:
      return state;
    case UPDATE_TEAM_FULFILLED:
      return state;
    case CLEAR_TEAM_FULFILLED:
      return {
        ...state,
        teams: [],
        error: { errorMessage: null, errorCode: null },
      };
    case CLEAR_ERRORS:
      return { ...state, error: { errorMessage: null, errorCode: null } };
    default:
      return state;
  }
};
export default moduleReducer;
