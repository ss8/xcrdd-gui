// eslint-disable-next-line import/prefer-default-export
enum TEAM_PRIVILEGE {
  View = 'team_view',
  Edit = 'team_write',
  Delete = 'team_delete',
  Admin = 'team_admin', // Predefined team edit/create/delete permission)
}
export default TEAM_PRIVILEGE;
