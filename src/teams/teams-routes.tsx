import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import TeamsFrame from './teams-frame';
import TeamCreate from './create';
import TeamDelete from './delete/team-delete';
import TeamEdit from './edit';

const moduleRoot = '/teams';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <TeamsFrame {...props} />}
    />
    <Route
      path={`${moduleRoot}/create`}
      render={() => <TeamCreate {...props} />}
    />
    <Route
      path={`${moduleRoot}/delete`}
      render={() => <TeamDelete {...props} />}
    />
    <Route
      path={`${moduleRoot}/edit`}
      render={() => <TeamEdit {...props} />}
    />
    <Route
      path={`${moduleRoot}/view`}
      render={() => <TeamEdit {...props} />}
    />
  </Fragment>
);

export default ModuleRoutes;
