import React, { useEffect } from 'react';
import {
  Redirect, Route, Switch, withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import AuthenticationModule from './authentication';
import navbarConfig from './shared/navbar/navbar-config';
import AuthUtils from './authentication/auth-utils';
import { Projects } from './navbarItems';

const NoMatch = () => (<h2>No matching route.</h2>);

type AuthenticatedRouteProps<T> = T & { component: React.Component }
const AuthenticatedRoute = ({
  component: Component,
  routePermissions,
  ...rest
}: AuthenticatedRouteProps<any>) => (
  <Route
    {...rest}
    render={(props: any) => {
      if (rest.isAuthenticated && AuthUtils.hasPermissions(routePermissions, rest.privileges)) {
        return <Component {...props} />;
      }

      if (!rest.isAuthenticated) {
        window.location.replace(`${window.location.origin}/authentication/login`);
        return (
          <Redirect to={{
            pathname: '/authentication/login',
          }}
          />
        );
      }
      return null;
    }}
  />
);

const mapStateToProps = (state: any) => ({
  isAuthenticated: state.authentication.isAuthenticated,
  privileges: state.authentication.privileges,
});

const ConnectedAuthenticatedRoute = connect(mapStateToProps)(AuthenticatedRoute);

const RootRoutes = (props: any) => {

  const navBarItemToPrivileges = navbarConfig(props.isExternalDashboardEnabled, props.isSecurityGatewayEnabled)
    .reduce((accumulator: any, navItem: any) => {
      accumulator[navItem.id] = navItem.permissions;
      if (navItem.children) {
        for (let index = 0; index < navItem.children.length; index += 1) {
          accumulator[navItem.children[index].id] = navItem.children[index].permissions;
        }
      }
      return accumulator;
    }, {});

  const getClassName = (): string => {
    const {
      location: {
        pathname,
      },
    } = props;

    if (pathname.includes('xms') || pathname.includes('external/')) {
      return 'external-apps-container';
    }

    if (pathname.includes('/cfs') || pathname.includes('xcrdd/settings') || pathname.includes('xcrdd/lea')) {
      return 'app-container-no-sub-menu';
    }

    return '';
  };

  return (
    <div className={`app-container ${getClassName()}`}>
      <Switch>
        <Route exact path="/authentication*" component={AuthenticationModule} />

        {Projects.map((project: any, index: number) => {
          return (
            <ConnectedAuthenticatedRoute
              key={index}
              {...props}
              path={project.path}
              component={project.component}
              routePermissions={navBarItemToPrivileges[project.permissions]}
            />
          );
        })}

        <Redirect from="/" to="/authentication/login" />
        <Route component={NoMatch} />
      </Switch>
    </div>
  );
};

const mapStateToPropsRootRoutes = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isExternalDashboardEnabled,
      isSecurityGatewayEnabled,
    },
  },
}: any) => ({
  isExternalDashboardEnabled,
  isSecurityGatewayEnabled,
});

// do not attempt to connect the store here
export default withRouter(connect(mapStateToPropsRootRoutes)(RootRoutes));
