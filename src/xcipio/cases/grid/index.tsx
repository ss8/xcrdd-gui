import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { getSortedTimeZoneLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { FilterModel } from 'shared/commonGrid/common-grid.types';
import { adaptToServerValueFilterModel } from 'shared/commonGrid/filterModel.adapter';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { TimeZoneCellRenderer } from 'shared/commonGrid/TimeZoneCellRenderer';
import CaseGridColumnDefs from './case-grid-column-defs';
import CommonGrid, { ICommonGridProperties } from '../../../shared/commonGrid';
import DateTimeCellRenderer from '../../../shared/commonGrid/date-cell-renderer';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../../shared/commonGrid/grid-enums';
import DateTimeWithTimezoneCellRenderer from '../../../shared/commonGrid/DateTimeWithTimezoneCellRenderer';
import ActionMenuBar from '../actions-bar/action-menu';
import * as caseActions from '../case-actions';

class CaseGrid extends React.Component<any, any> {
  CASE_GRID_SETTINGS = 'caseGridSettings';
  commonGridRef: any;
  columnDefs = CaseGridColumnDefs;

  state: any = {
    selection: [],
    selectionLastCleared: null,
  }

  initializeColumnDef = () => {
    const {
      timezones,
    } = this.props;

    const timeZoneCol = this.columnDefs.find((columnDef) => (columnDef.field === 'timeZone'));
    if (timeZoneCol) {
      timeZoneCol.filter = 'agSetColumnFilter';
      timeZoneCol.filterParams = {
        values: getSortedTimeZoneLabels(timezones),
      };
    }
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
    this.props.broadcastSelectedCases(e.api.getSelectedRows());
  };

  handleGetResultsFromServer = (body: any) => {
    return this.props.getAllCases(0, body);
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getUserSettings(this.CASE_GRID_SETTINGS, this.props.authentication.userId);
  }

  saveGridSettings = (caseGridSettings: { [key: string]: any }) => {
    if (caseGridSettings !== null) {
      this.props.putUserSettings(this.CASE_GRID_SETTINGS, this.props.authentication.userId,
        caseGridSettings);
    }
  }

  reloadGridDataFromServer = () => {
    if (this.commonGridRef) {
      this.commonGridRef.handleGridReloadFromServer(
        this.handleGetResultsFromServer,
        null,
      );
    }
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    switch (type) {
      case COMMON_GRID_ACTION.Edit:
        this.handleEditWarrantWizard();
        break;
      case COMMON_GRID_ACTION.View:
        this.handleViewWarrantWizard();
        break;
      default:
    }
  }

  handleEditWarrantWizard = () => {
    const {
      history,
      goToWarrantEdit,
      broadcastSelectedWarrants,
      getWarrantById,
      clearCasesState,
      broadcastSelectedWarrantCase,
    } = this.props;
    const { selectedDF: dfId } = this.props.global;
    const { warrantId } = this.state.selection[0];
    getWarrantById(dfId, warrantId.id).then(() => {
      clearCasesState();
      broadcastSelectedWarrants([this.props.warrant]);
      broadcastSelectedWarrantCase(this.state.selection[0]);
      goToWarrantEdit(history);
    });
  }

  handleViewWarrantWizard = async () => {
    const {
      history,
      goToWarrantView,
      broadcastSelectedWarrants,
      getWarrantById,
      clearCasesState,
      broadcastSelectedWarrantCase,
    } = this.props;
    const { selectedDF: dfId } = this.props.global;
    const { warrantId } = this.state.selection[0];
    getWarrantById(dfId, warrantId.id).then(() => {
      clearCasesState();
      broadcastSelectedWarrants([this.props.warrant]);
      broadcastSelectedWarrantCase(this.state.selection[0]);
      goToWarrantView(history);
    });
  }

  componentDidMount() {
    const {
      clearSelectedCases,
    } = this.props;

    clearSelectedCases();
  }

  componentDidUpdate() {
    this.initializeColumnDef();
    if (this.props.selectionLastCleared !== this.state.selectionLastCleared) {
      this.setState({ selectionLastCleared: this.props.selectionLastCleared }, () => {
        this.reloadGridDataFromServer();
      });
    }
  }

  componentWillUnmount(): void {
    const {
      clearCasesState,
    } = this.props;

    clearCasesState();
  }

  adaptToGridFilterModel = (filterModel:FilterModel): FilterModel => {
    return adaptToServerValueFilterModel(filterModel, 'timeZone', this.props.timezones);
  }

  render() {
    const frameworkComponents = {
      datetimeCellRenderer: DateTimeCellRenderer,
      DateTimeWithTimezoneCellRenderer,
      TimeZoneCellRenderer,
    };
    const commonGridProps: ICommonGridProperties = {
      onActionMenuItemClick: this.onActionMenuItemClick,
      onSelectionChanged: this.handleSelection,
      rowSelection: ROWSELECTION.MULTIPLE,
      columnDefs: this.columnDefs,
      rowData: this.props.cases.cases,
      enableBrowserTooltips: true,
      context: this,
      pagination: true,
      noResults: false,
      frameworkComponents,
      getGridResultsFromServer: this.handleGetResultsFromServer,
      adaptToFilterModel: this.adaptToGridFilterModel,
      renderDataFromServer: true,
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
    };
    return (
      <Fragment>
        <div className="wiretap-grid-container" style={{ height: 'inherit' }}>
          <CommonGrid
            data-test="warrants-common-grid"
            ref={(ref: any) => { this.commonGridRef = ref; }}
            {...commonGridProps}
          >
            <ActionMenuBar
              handleEdit={(this.handleEditWarrantWizard)}
            />
          </CommonGrid>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isBulkActionsEnabled,
    },
  },
  warrants: { warrant },
  cases: { selectionLastCleared, selectedCases },
  users: {
    timezones,
  },
}: any) => ({
  isBulkActionsEnabled,
  selectionLastCleared,
  selectedCases,
  timezones,
  warrant,
});

const mapDispatchToProps = {
  broadcastSelectedCases: caseActions.broadcastSelectedCases,
  clearCasesState: caseActions.clearCasesState,
  clearSelectedCases: caseActions.clearSelectedCases,
  goToWarrantEdit: warrantActions.goToWarrantEdit,
  goToWarrantView: warrantActions.goToWarrantView,
  broadcastSelectedWarrants: warrantActions.broadcastSelectedWarrants,
  getWarrantById: warrantActions.getWarrantById,
  broadcastSelectedWarrantCase: warrantActions.broadcastSelectedWarrantCase,
};

export default connect(mapStateToProps,
  mapDispatchToProps, null, { forwardRef: true })(CaseGrid);
