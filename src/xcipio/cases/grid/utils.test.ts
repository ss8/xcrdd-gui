import { CaseModel } from 'data/case/case.types';
import cloneDeep from 'lodash.clonedeep';
import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { mockCases } from '__test__/mockCases';
import { CASE_RESSTATE } from '../case-enums';
import showActionMenuItem from './utils';

describe('utils', () => {
  describe('showActionMenuItem()', () => {
    let caseEntry: CaseModel;

    beforeEach(() => {
      caseEntry = cloneDeep(mockCases[0]);
    });

    it('should return true for Pause if warrant is active', () => {
      caseEntry.subStatus = CASE_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Pause, caseEntry, true, true)).toBe(true);
    });

    it('should return false for Pause if warrant is active but pause action is not enabled', () => {
      caseEntry.subStatus = CASE_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Pause, caseEntry, false, false)).toBe(false);
    });

    it('should return true for Resume if warrant is paused', () => {
      caseEntry.subStatus = CASE_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Resume, caseEntry, true, true)).toBe(true);
    });

    it('should return false for Resume if warrant is paused but resume action is not enabled', () => {
      caseEntry.subStatus = CASE_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Resume, caseEntry, false, false)).toBe(false);
    });

    it('should return true for Start if warrant is stopped or expired', () => {
      caseEntry.subStatus = CASE_RESSTATE.Stopped;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Start, caseEntry, true, true)).toBe(true);
      caseEntry.subStatus = CASE_RESSTATE.Expired;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Start, caseEntry, true, true)).toBe(true);
    });

    it('should return true for Stop if warrant is active or paused', () => {
      caseEntry.subStatus = CASE_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Stop, caseEntry, true, true)).toBe(true);
      caseEntry.subStatus = CASE_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Stop, caseEntry, true, true)).toBe(true);
    });

    it('should return true for Reschedule if warrant is pending, stopped or expired', () => {
      caseEntry.subStatus = CASE_RESSTATE.PENDING;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, caseEntry, true, true)).toBe(true);
      caseEntry.subStatus = CASE_RESSTATE.Stopped;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, caseEntry, true, true)).toBe(true);
      caseEntry.subStatus = CASE_RESSTATE.Expired;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, caseEntry, true, true)).toBe(true);
    });
  });
});
