import { CaseModel } from 'data/case/case.types';
import { CASE_RESSTATE } from '../case-enums';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

const showActionMenuItem = (
  menuId: string,
  rowData: CaseModel,
  isWarrantResumeActionEnabled: boolean,
  isWarrantPauseActionEnabled: boolean,
): boolean => {
  if (!rowData) return false;
  switch (menuId) {
    case COMMON_GRID_ACTION.Pause: {
      if (isWarrantPauseActionEnabled === true && rowData.subStatus === CASE_RESSTATE.Active) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Resume: {
      if (isWarrantResumeActionEnabled === true && rowData.subStatus === CASE_RESSTATE.Paused) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Start: {
      if (rowData.subStatus === CASE_RESSTATE.Stopped ||
        rowData.subStatus === CASE_RESSTATE.Expired) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Stop: {
      if (rowData.subStatus === CASE_RESSTATE.Active ||
        rowData.subStatus === CASE_RESSTATE.Paused) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Reschedule: {
      if (rowData.subStatus === CASE_RESSTATE.PENDING ||
        rowData.subStatus === CASE_RESSTATE.Expired ||
        rowData.subStatus === CASE_RESSTATE.Stopped) {
        return true;
      }
      break;
    }
    default: return true;
  }

  return false;
};

export default showActionMenuItem;
