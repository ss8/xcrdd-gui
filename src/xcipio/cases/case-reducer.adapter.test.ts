import { CaseModel } from 'data/case/case.types';
import cloneDeep from 'lodash.clonedeep';
import { mockCases } from '__test__/mockCases';
import { mockHi3Interfaces } from '__test__/mockCollectionFunctions';
import { adaptCaseToXRESTbug } from './case-reducer.adapter';

describe('adaptCasesToXRESTbug', () => {
  let caseWithDuplicateHI3s:CaseModel;
  beforeEach(() => {
    caseWithDuplicateHI3s = cloneDeep(mockCases[0]);
    caseWithDuplicateHI3s.hi3Interfaces = [
      ...mockHi3Interfaces,
      ...mockHi3Interfaces,
    ];
  });
  it('should remove duplicate Hi3 interfaces', () => {
    adaptCaseToXRESTbug(caseWithDuplicateHI3s);
    expect(caseWithDuplicateHI3s.hi3Interfaces).toEqual(expect.objectContaining([
      ...mockHi3Interfaces,
    ]));
  });
});
