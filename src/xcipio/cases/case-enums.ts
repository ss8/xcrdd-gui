export enum CASE_PRIVILEGE {
    View = 'warrant_view',
    Edit = 'warrant_write',
    Delete = 'warrant_delete',
}

export enum CASE_PROVSTATE {
    Start = 'START',
    Stop = 'STOP',
    Pause = 'PAUSE',
    Resume = 'RESUME',
    Reschedule = 'SCHEDULE',
}

/*
State attribute from Warrant's Response object
*/
export enum CASE_RESSTATE {
    Active = 'ACTIVE',
    PENDING = 'PENDING',
    Expired = 'EXPIRED',
    Stopped = 'STOPPED',
    Paused = 'PAUSED',
}
