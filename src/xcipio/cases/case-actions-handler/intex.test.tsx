import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import {
  CaseActionsHandler,
  ICaseActionsHandlerProps,
  ICaseActionsHandlerStatus,
} from './index';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

describe('<CaseActionsHandler />', () => {
  let testMocks: any;
  let wrapper: ShallowWrapper<ICaseActionsHandlerProps, ICaseActionsHandlerStatus, CaseActionsHandler>;
  let props: ICaseActionsHandlerProps;

  beforeEach(() => {
    testMocks = {};
    testMocks.warrantCase = {
      id: '2',
      dxOwner: '',
      dxAccess: 'tweaked_flag_in_DXACCESS_because_it_is_NOT_used',
      name: 'Test Case',
      warrantId: {
        id: '1',
        name: 'Test warrant',
      },
      liId: 'Test case ',
      coId: 'Test warrant',
      device: '',
      services: [
        'LTE-4G-DATA',
      ],
      cfId: {
        id: 'Q0EtRE9KQk5FLTMzMTA4',
        name: 'CA-DOJBNE-33108',
        cfId: 'CA-DOJBNE-33108',
        tag: 'CF-3GPP-33108',
      },
      entryDateTime: '2021-01-07T23:46:00Z',
      warrantStartDateTime: '2021-01-08T06:00:00Z',
      warrantStopDateTime: '2021-01-16T06:00:00Z',
      hi2Interfaces: [
        {
          id: 'Q0EtRE9KQk5FLTMzMTA4IzE',
          name: 'Q0EtRE9KQk5FLTMzMTA4IzE',
        },
      ],
      hi3Interfaces: [],
      genccc: {
        leaphone1: '',
        leaphone2: '',
      },
      type: 'CC',
      status: 'ACTIVE',
      subStatus: 'ACTIVE',
      startDateTime: '2020-02-20T01:41:00Z',
      stopDateTime: '2020-02-29T08:00:00Z',
      timeZone: 'America/New_York',
      templateUsed: '',
      options: {},
      targets: [
        {
          id: 'VGVzdCB3YXJyYW50IHNyZSMx',
          name: 'VGVzdCB3YXJyYW50IHNyZSMx',
        },
      ],
      reporting: {
        packetsMediated: 0,
        provisionStatus: 'FAILED',
        comments: '',
        contact: {
          id: '',
          name: '',
        },
        city: '',
        region: '',
        state: '',
        judge: '',
        receivedDateTime: '',
      },
    };

    props = {
      action: '',
      authentication: {},
      clearSelectedCases: jest.fn(),
      deleteCase: jest.fn(),
      isAuditEnabled: false,
      global: {
        selectedDF: 'some df id',
      },
      isOpen: true,
      onClose: jest.fn(),
      selectedCases: [testMocks.warrantCase],
      caseUpdateProvStatus: jest.fn(),
      broadcastSelectedCases: jest.fn(),
      isWarrantDefaultExpireEnabled: false,
      warrantDefaultExpireDays: '10',
    };

    wrapper = shallow(<CaseActionsHandler {...props} />);
  });

  describe('shouldAdaptDateTimeToTimezone()', () => {
    it('should return true if action is Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Start });
      expect(wrapper.instance().shouldAdaptDateTimeToTimezone()).toBe(true);
    });

    it('should return false if action is not Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Delete });
      expect(wrapper.instance().shouldAdaptDateTimeToTimezone()).toBe(false);
    });
  });

  describe('adaptDateTimeToTimezone()', () => {
    it('should return the data with the date/time field converted to the given timezone for Start action', () => {
      const data = {
        action: COMMON_GRID_ACTION.Start,
        stopDateTime: '2020-07-30T12:20:00.000Z',
      };
      expect(CaseActionsHandler.adaptDateTimeToTimezone(data, 'America/New_York'))
        .toEqual({
          action: COMMON_GRID_ACTION.Start,
          stopDateTime: '2020-07-30T09:20:00.000Z',
        });
    });
  });

  describe('handleAction()', () => {
    beforeEach(() => {
      jest.spyOn(wrapper.instance(), 'handleDeleteTrash');
    });

    it('should call caseUpdateProvStatus with adapted dates if action is Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Start });
      wrapper.instance().formData = { action: 'START', stopDateTime: '2020-07-30T12:20:00.000Z' };
      wrapper.instance().handleAction(testMocks.warrantCase);
      expect(props.caseUpdateProvStatus).toBeCalledWith(
        'some df id',
        '1',
        '2',
        {
          auditDetails: null,
          data: {
            action: 'START',
            stopDateTime: '2020-07-30T09:20:00.000Z',
          },
        },
      );
    });

    it('should call caseUpdateProvStatus with data as is if action is Stop', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Stop });
      wrapper.instance().formData = { action: 'STOP' };
      wrapper.instance().handleAction(testMocks.warrantCase);
      expect(props.caseUpdateProvStatus).toBeCalledWith(
        'some df id',
        '1',
        '2',
        {
          auditDetails: null,
          data: {
            action: 'STOP',
          },
        },
      );
    });

    it('should call handleDeleteTrash with warrantCase if action is Delete', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Delete });
      wrapper.instance().handleAction(testMocks.warrantCase);
      expect(wrapper.instance().handleDeleteTrash).toBeCalledWith(testMocks.warrantCase, null);
    });
  });
});
