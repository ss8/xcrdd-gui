import { OPERATOR } from '../../../shared/form/validators/operatorValidator';

export const START_FORM = {
  stopDateTime: {
    label: 'New Case Stop Time',
    icon: 'calendar',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
    },
  },
  note: {
    label: undefined,
    type: 'label',
    initial: 'Note: Each Case Start and Stop time will follow the related warrant timezone',
  },
};

export const RESCHEDULE_FORM = {
  caseNameLabel: {
    label: undefined,
    type: 'label',
    initial: '',
  },
  warrantNameLabel: {
    label: undefined,
    type: 'label',
    initial: '',
  },
  warrantValidityLabel: {
    label: undefined,
    type: 'label',
    initial: '',
  },
  startDateTime: {
    label: 'New Case Start Time',
    icon: 'calendar',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
    },
  },
  stopDateTime: {
    label: 'New Case Stop Time',
    icon: 'calendar',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
      validators: [
        {
          type: 'Operator',
          operator: OPERATOR.GREATER_THAN,
          fieldName: 'startDateTime',
          errorMessage: 'Stop time should be greater than the start time',
        },
      ],
    },
  },
  note: {
    label: undefined,
    type: 'label',
    initial: 'Note: Each Case Start and Stop time will follow the related warrant timezone',
  },
};

export const formatAction = (action: string) => {
  return action
    ? action[0].toUpperCase() + action.slice(1)
    : '';
};
