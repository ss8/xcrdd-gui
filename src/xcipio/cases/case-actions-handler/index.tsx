import React, { Fragment, ReactElement } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import moment from 'moment-timezone';
import CaseAdapter from 'data/case/case.adapter';
import ModalDialog, { DONT_CLOSE_ON_SUBMIT } from 'shared/modal/dialog';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { CaseModel } from 'data/case/case.types';
import { RootState } from 'data/root.reducers';
import { getDateTimeFormatTemplate } from 'utils';
import { getSuccessMessageForAction } from 'xcipio/warrants/warrant-actions-handler/utils';
import DynamicForm, { ChangedField } from '../../../shared/form';
import { CASE_PROVSTATE } from '../case-enums';
import {
  caseUpdateProvStatus,
  deleteCase,
  clearSelectedCases,
  broadcastSelectedCases,
} from '../case-actions';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';
import ActionHandler from '../actions-bar/action-handler';
import {
  AuditService, AuditActionType, getAuditDetails, getAuditFieldInfo,
} from '../../../shared/userAudit/audit-constants';
import UserAudit from '../../../shared/userAudit';
import { START_FORM, RESCHEDULE_FORM, formatAction } from './utils';
import { AppToaster, showSuccessMessage, TOAST_TIMEOUT } from '../../../shared/toaster';
import { extractErrorMessage } from '../../../utils';
import { validateAutoForm } from '../../../components/auto-form';
import { checkGivenTimeWithCurrentTime }
  from '../../../shared/constants/checkTimeValidation';
import ShowStatusMessage from '../forms/updateCase/show-status';

export interface ICaseActionsHandlerStatus {
  action: string,
  title: string,
  displayMessage: string,
  form: any,
  formFields: any,
  layout: any,
  selectedCases: Array<any>,
  width: string,
  isOpenProgressBarIndicartor: boolean,
  errors: string[],
}

export interface ICaseActionsHandlerProps {
  action: string,
  authentication: any,
  clearSelectedCases: any,
  deleteCase: any,
  isAuditEnabled: any,
  global: any,
  isOpen: any,
  onClose: any,
  selectedCases: Array<any>,
  caseUpdateProvStatus: any,
  broadcastSelectedCases: any,
  isWarrantDefaultExpireEnabled: boolean
  warrantDefaultExpireDays: string
  onReloadCase?: () => void
}

export class CaseActionsHandler extends React.Component<ICaseActionsHandlerProps, ICaseActionsHandlerStatus> {
  formRef: any;

  userAuditformRef: any;

  formData: any = {};

  auditData: any = {};

  constructor(props: any) {
    super(props);
    this.state = this.defaultState();
  }

  defaultState = () =>
    ({
      title: '',
      displayMessage: '',
      form: {},
      formFields: undefined,
      layout: undefined,
      width: '500px',
      selectedCases: [],
      action: '',
      isOpenProgressBarIndicartor: false,
      payload: {},
      auditFields: {},
      errors: [],
    })

  componentDidUpdate = () => {

    const { action, selectedCases } = this.props;
    if (action &&
      (this.state.selectedCases !== selectedCases ||
        this.state.action !== action
      )) {
      let displayMessage = '';
      if (selectedCases.length) {
        if (selectedCases.length > 1) {
          displayMessage = `Are you sure you want to ${action} ${selectedCases.length} selected cases?`;
        } else {
          const name = selectedCases[0].liId;
          const warrantName = selectedCases[0].warrantId.name;
          displayMessage = `Are you sure you want to ${action} case '${name}' of warrant '${warrantName}' ?`;
        }
      }
      const title = `${formatAction(action)} Case`;
      this.setState({
        ...this.defaultState(),
        title,
        displayMessage,
        selectedCases,
        action,
      }, this.modifyFormBasedOnAction);
    }
  }

  modifyFormBasedOnAction = () => {
    const formState: any = {};
    if (this.state.action === COMMON_GRID_ACTION.Start) {
      formState.formFields = cloneDeep(START_FORM);
      this.augmentStopTimeValidators(formState.formFields.stopDateTime);
      formState.layout = [['stopDateTime'], ['note']];
    } else if (this.state.action === COMMON_GRID_ACTION.Reschedule) {
      this.modifyRescheduleForm(formState);
    } else formState.formFields = null;
    this.setState({ ...formState });
  }

  modifyRescheduleForm = (formState: any) => {
    const { selectedCases } = this.props;
    const { warrantStartDateTime, warrantStopDateTime } = selectedCases[0];
    const name = selectedCases[0].liId;
    const warrantName = selectedCases[0].warrantId.name;
    formState.formFields = cloneDeep(RESCHEDULE_FORM);
    formState.formFields.caseNameLabel.initial = `Case Name:  '${name}'`;
    formState.formFields.warrantNameLabel.initial = `Warrant Name:  '${warrantName}'`;
    formState.formFields.warrantValidityLabel.initial = `Warrant Validity:  '${moment(warrantStartDateTime).format(getDateTimeFormatTemplate())}' - ` +
      `'${moment(warrantStopDateTime).format(getDateTimeFormatTemplate())}'`;
    if (selectedCases.length > 1) {
      formState.formFields.note.initial = 'Note: Each Case Start and Stop time will follow the related warrant timezone. ' +
        'Each Warrant start time and stop time will be adjusted as needed to accommodate new Case start and end times';
      formState.layout = [
        ['startDateTime', 'stopDateTime'],
        ['note'],
      ];
    } else {
      formState.layout = [
        ['caseNameLabel'],
        ['warrantNameLabel'],
        ['warrantValidityLabel'],
        ['$empty'],
        ['$empty'],
        ['startDateTime', 'stopDateTime'],
        ['note'],
      ];
    }
    formState.width = '800px';
  }

  augmentStopTimeValidators = (stopDateTimeField: any) => {
    const { selectedCases } = this.props;
    // For single case selection, we would process additional requested validation as part of UI validation.
    // For multi-select, we let Bulk component handle validation error.
    if (selectedCases.length === 1) {
      stopDateTimeField.initial = selectedCases[0].stopDateTime;
      if (!stopDateTimeField.validation.validators) {
        stopDateTimeField.validation.validators = [];
      }
    }
  }

  fieldUpdateHandler = (changedField: ChangedField) => {
    const { name, value } = changedField;
    const form = cloneDeep(this.state.form);
    form[name] = value;
    const errors = this.validateDateTimeFields(form);
    this.setState({
      form,
      errors,
    });
  }

  getCustomComponent = (): ReactElement => {
    const {
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      selectedCases,
      isAuditEnabled,
    } = this.props;

    const {
      action,
      formFields,
      layout,
      errors,
    } = this.state;

    let caseModel: Partial<CaseModel> = {};
    if (selectedCases.length === 1) {
      caseModel = { ...selectedCases[0] };
    }

    if (isWarrantDefaultExpireEnabled) {
      WarrantAdapter.setCaseDefaultStopTime(caseModel, warrantDefaultExpireDays);
    }

    return (
      <Fragment>
        {(formFields) && (
          <DynamicForm
            ref={(ref) => { this.formRef = ref; }}
            name="case status update form"
            fields={formFields}
            layout={layout}
            originalValues={caseModel}
            defaults={caseModel}
            fieldUpdateHandler={this.fieldUpdateHandler}
          />
        )}
        {
          ((action === COMMON_GRID_ACTION.Delete
            || action === COMMON_GRID_ACTION.Start
            || action === COMMON_GRID_ACTION.Stop)
            && isAuditEnabled) && (
            <UserAudit
              data-test="userAudit"
              ref={(ref: any) => { this.userAuditformRef = ref; }}
              mode={action === COMMON_GRID_ACTION.Delete ? 'delete' : 'edit'}
            />
          )
        }
        {
          errors.length > 0 &&
          <ShowStatusMessage hasErrors msgs={errors} />
        }
      </Fragment>
    );
  }

  getAuditDetailsInfo = (warrantCase: any, actionType: AuditActionType) => {
    if (this.props.isAuditEnabled) {
      const name = warrantCase?.name;
      const userId = this.props.authentication.userDBId;
      const userName = this.props.authentication.userId;
      const isEdit = actionType === AuditActionType.CASE_EDIT;
      const data = isEdit ? this.formData : null;
      const auditFieldDetails = getAuditFieldInfo(data, this.auditData, isEdit, warrantCase);

      const auditDetails = getAuditDetails(
        this.props.isAuditEnabled,
        userId,
        userName,
        auditFieldDetails,
        name,
        AuditService.WARRANTS,
        actionType,
      );
      return auditDetails;
    }
    return null;
  }

  validateDateTimeFields(formFields: any) {
    const { action } = this.state;
    // For bulk actions it validates based on first selected case timezone
    const { timeZone } = this.state.selectedCases[0];
    let errors: string[] = [];

    if (action === COMMON_GRID_ACTION.Start && formFields.stopDateTime) {
      if (checkGivenTimeWithCurrentTime(formFields.stopDateTime, timeZone)) {
        errors = ['Stop Time should be after the current time. Please select based on case time zone'];
      }
    }
    return errors;
  }

  handleSubmit = () => {
    let formFields = {};
    let auditFields = {};
    let isValid = true;
    if (this.formRef) {
      if (!this.formRef.isValid(true, true)) {
        isValid = false;
      }
      formFields = this.formRef.getValues();
      const errors = this.validateDateTimeFields(formFields);
      if (errors.length > 0) {
        isValid = false;
      }
      this.setState({ errors });
    }
    if (this.userAuditformRef) {
      if (this.userAuditformRef.isValid(true, true)) {
        auditFields = this.userAuditformRef?.getValue();
      } else {
        isValid = false;
      }
    }

    if (!isValid) {
      return DONT_CLOSE_ON_SUBMIT;
    }
    const data: any = {
      action: (CASE_PROVSTATE as any)[formatAction(this.state.action)],
      ...formFields,
    };

    this.formData = data;
    this.auditData = auditFields;

    if (this.state.selectedCases.length === 1) {
      const caseEntry = this.state.selectedCases[0];

      this.handleAction(caseEntry)
        .then(() => {
          if (CaseActionsHandler.isStopingOrEditing(data.action) && !!this.props.onReloadCase) {
            this.props.onReloadCase();
          } else {
            this.props.clearSelectedCases();
          }

          const { action } = this.state;
          const message = getSuccessMessageForAction(caseEntry.name, action);
          showSuccessMessage(message, TOAST_TIMEOUT);
        })
        // TODO: REZA: handle error in reducer so we don't have to handle per component
        .catch((error: any) => {
          AppToaster.show({
            icon: 'error',
            intent: 'danger',
            timeout: 0,
            message: `Error: ${extractErrorMessage(error)}`,
          });
        });
    } else this.handleBulkAction();
  }

  shouldAdaptDateTimeToTimezone() {
    const { action } = this.state;
    return action === COMMON_GRID_ACTION.Start || action === COMMON_GRID_ACTION.Reschedule;
  }

  static isStopingOrEditing(action: string): boolean {
    if (action == null) {
      return false;
    }

    return (
      action.toLowerCase() === COMMON_GRID_ACTION.Start
      || action.toLowerCase() === COMMON_GRID_ACTION.Stop
      || action.toLowerCase() === COMMON_GRID_ACTION.Pause
      || action.toLowerCase() === COMMON_GRID_ACTION.Resume
      || action.toLowerCase() === COMMON_GRID_ACTION.Reschedule
    );
  }

  /**
   * Convert the case date/time fields to the case timezone
   *
   * @param warrantCase case data
   * @param timeZone the timezone to use
   * @return the case adapted
   */
  static adaptDateTimeToTimezone(warrantCase: any, timeZone: string): any {
    const { timeZone: timeZoneToRemove, ...adaptedCase } =
      CaseAdapter.adaptCaseDateTimeFieldsToSubmit({ ...warrantCase, timeZone });
    return adaptedCase;
  }

  handleAction = async (warrantCase: any) => {
    const { selectedDF: dfId } = this.props.global;
    const { id, timeZone } = warrantCase;
    const wid = warrantCase.warrantId.id;
    let data = {
      ...this.formData,
    };

    if (this.shouldAdaptDateTimeToTimezone()) {
      data = CaseActionsHandler.adaptDateTimeToTimezone(data, timeZone);
    }

    let auditActionType: AuditActionType = AuditActionType.CASE_EDIT;
    switch (this.state.action) {
      case COMMON_GRID_ACTION.Delete: auditActionType = AuditActionType.CASE_DELETE; break;
      default: break;
    }
    const auditDetails = this.getAuditDetailsInfo(warrantCase, auditActionType);

    const payload = {
      data,
      auditDetails,
    };
    if (this.state.action === COMMON_GRID_ACTION.Delete) {
      await this.handleDeleteTrash(warrantCase, auditDetails);
    } else await this.props.caseUpdateProvStatus(dfId, wid, id, payload);
  }

  handleDeleteTrash = async (warrantCase: any, auditDetails: any) => {
    const { selectedDF: dfId } = this.props.global;
    const { id }: any = warrantCase;
    const wid = warrantCase.warrantId.id;
    await this.props.deleteCase(dfId, wid, id, auditDetails);
  }

  handleBulkAction = () => {
    // apply validation to each case, let bulk handle errors
    if (this.state.action === COMMON_GRID_ACTION.Start) {
      const { formFields } = this.state;
      if (formFields != null && formFields.stopDateTime) {
        const formInputs: any = { stopDateTime: { autoValidate: [] } };
        const validatedSelectedCases = this.props.selectedCases.map((warrantCase: any) => {
          const { isValid, errorMessage } = validateAutoForm(formInputs);
          warrantCase.autoFormValidation = { isValid, errorMessage };
          return warrantCase;
        });
        this.props.broadcastSelectedCases(validatedSelectedCases);
      }
    }
    this.setState({ isOpenProgressBarIndicartor: true });
  };

  handleClose = () => {
    this.setState({ isOpenProgressBarIndicartor: false });
    this.props.onClose();
    this.props.clearSelectedCases();
  };

  render() {

    return (
      <Fragment>
        <ModalDialog
          onSubmit={this.handleSubmit}
          customComponent={this.getCustomComponent}
          width={this.state.width}
          actionText={formatAction(this.state.action)}
          displayMessage={this.state.displayMessage}
          onClose={this.props.onClose}
          isOpen={this.props.isOpen}
          title={this.state.title}
        />
        <ActionHandler
          isOpen={this.state.isOpenProgressBarIndicartor}
          onClose={this.handleClose}
          handleAction={this.handleAction}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  authentication,
  global,
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Case_bArchiveCase: bArchiveCase,
      isAuditEnabled,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
    },
  },
  cases: { selectedCases },
}: RootState) => ({
  authentication,
  global,
  bArchiveCase,
  isAuditEnabled,
  selectedCases,
  isWarrantDefaultExpireEnabled,
  warrantDefaultExpireDays,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  deleteCase,
  caseUpdateProvStatus,
  clearSelectedCases,
  broadcastSelectedCases,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CaseActionsHandler);
