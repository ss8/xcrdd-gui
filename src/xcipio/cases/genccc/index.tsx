import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import { FormTemplateFieldMap } from 'data/template/template.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import DynamicForm, { ChangedField } from '../../../shared/form';
import GencccForm from './genccc-form.json';
import './genccc.scss';

interface Props {
  ref: (ref: any) => void;
  defaults: any;
  fieldUpdateHandler: (changedField: ChangedField) => Promise<void>;
  changeTracker?: any;
  caseId: string;
  mode: string
}

const GencccFields: any = cloneDeep(GencccForm.fields);
class GencccPanel extends React.Component<Props, any> {
    genCCCRef: any;

    gencccFieldUpdateHandler = (changedField: ChangedField, genccc:any) => {
      const { name, value, label } = changedField;
      const prevValue = genccc[name];
      genccc[name] = value;
      this.props.fieldUpdateHandler({ name: 'genccc', value: genccc });
      if (this.props.changeTracker) {
        this.props.changeTracker.addUpdated(`case.${this.props.caseId}.genccc.${name}`, value, label, prevValue);
      }
    }

    isValid = (forceTouched = true) => {
      if (this.genCCCRef) {
        return this.genCCCRef.isValid(true, forceTouched);
      }
      return true;
    }

    static getFormTemplateFields = (
      formTemplateFields: FormTemplateFieldMap,
      mode:string,
    ): FormTemplateFieldMap => {

      if (mode === CONFIG_FORM_MODE.View) {
        setAllTemplateFieldsAsReadOnly(formTemplateFields);
      }

      return formTemplateFields;
    }

    render() {
      const { mode } = this.props;

      return (
        <Fragment>
          <div className="collection-function-destination-phone">
            <b>Collection Function Destination Phone Numbers:</b>
            <div style={{ display: 'flex' }}>
              <span className="gencccRowContainer" style={{ flexGrow: 12 }}>
                <DynamicForm
                  ref={(ref: any) => { this.genCCCRef = ref; }}
                  name={`genccc_${new Date().getTime()}`}
                  fields={GencccPanel.getFormTemplateFields(GencccFields, mode)}
                  layout={[Object.keys(GencccFields)]}
                  defaults={this.props.defaults}
                  fieldUpdateHandler={(changedField: ChangedField) => {
                    this.gencccFieldUpdateHandler(changedField, this.props.defaults);
                  }}
                />
              </span>
            </div>
          </div>
        </Fragment>

      );
    }
}

const mapStateToProps = ({
  warrants: { changeTracker },
}: any) => ({
  changeTracker,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(GencccPanel);
