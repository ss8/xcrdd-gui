import { CaseState } from 'data/case/case.types';
import {
  GET_ALL_CASES_BY_CASEIDS_FULFILLED,
  GET_CASE_BY_ID_FULFILLED,
  GET_ALL_CASES_FULFILLED,
  UPDATE_CASE_STATE,
  UPDATE_CASES_STATE,
  UPDATE_SELECTED_INDEX,
  CLEAR_CASES_STATE,
  BROADCAST_SELECTED_CASES,
  CLEAR_SELECTED_CASES,
} from './case-actions';
import {
  adaptCasesToXRESTbug, adaptCaseToXRESTbug,
} from './case-reducer.adapter';

const defaultState: CaseState = {
  cases: [],
  selectedIndex: -1,
  error: '',
  selectedCases: [],
  selectionLastCleared: null,
};

const moduleReducer = (state = defaultState, action: any): CaseState => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_CASES_BY_CASEIDS_FULFILLED: {
      const cases = action.payload.data.data;
      cases.forEach((warrantCase: any) => {
        if (!(warrantCase.services && warrantCase.services.length > 0)) {
          warrantCase.services = ['unspecified'];
        }
      });
      adaptCasesToXRESTbug(cases);
      return { ...state, cases };
    }

    case GET_CASE_BY_ID_FULFILLED: {
      const { cases, selectedIndex } = state;
      const newCases: any[] = cases.slice();
      if (selectedIndex != null) {
        newCases[selectedIndex] = { ...action.payload.data.data };
      }
      adaptCaseToXRESTbug(newCases[selectedIndex]);
      return { ...state, cases: newCases };
    }

    case GET_ALL_CASES_FULFILLED:
      const cases = action.payload.data.data;
      adaptCasesToXRESTbug(cases);
      return { ...state, cases };

    case UPDATE_CASE_STATE: {
      return {
        ...state,
        cases: state.cases.map((item: any) => {
          if (item.id === action.payload.id) {
            return action.payload;
          }
          return item;
        }),
      };
    }
    case UPDATE_SELECTED_INDEX: {
      return { ...state, selectedIndex: action.payload };
    }
    case UPDATE_CASES_STATE: {
      return { ...state, cases: action.payload };
    }
    case CLEAR_CASES_STATE: {
      return { ...state, cases: [] };
    }
    case BROADCAST_SELECTED_CASES:
      return {
        ...state,
        selectedCases: action.selectedCases,
      };
    case CLEAR_SELECTED_CASES:
      return {
        ...state,
        selectedCases: [],
        selectionLastCleared: new Date(),
      };
    default:
      return state;
  }
};
export default moduleReducer;
