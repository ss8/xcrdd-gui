import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Label, HTMLSelect } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { FormTemplateFieldMap } from 'data/template/template.types';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import DynamicForm, { ChangedField } from '../../../shared/form';
import CaseCreateOptions from './case-options.json';
import { WARRANT_PRIVILEGE } from '../../warrants/warrant-enums';
import ChangeTracker from '../../warrants/forms/warrant-change-tracker';
import { setWarrantTracker } from '../../warrants/warrant-actions';

interface Props {
  children: any[],
  isOpen: boolean,
  currentStateMode: string,
  handleClose: () => void,
  inputCaseOptions: any,
  handleSubmit: (options: any, templateId: any) => void,
  caseId?: string,
  warrantId?: string | null,
  templateUsed: string,
  templates: any[],
  privileges?: string[],
  changeTracker?: any,
  setWarrantTracker: (changeTracker: ChangeTracker) => void,
}

class CaseOptions extends React.Component<Props, any> {
  private ref: any;

  private caseCreateOptions: any;

  private templateOptions: any;

  private localChangeTracker: ChangeTracker|undefined;

  public static defaultProps = {
    children: undefined,
  }

  constructor(props:any) {
    super(props);
    this.ref = React.createRef();
    this.templateOptions = this.getAllCaseOptionTemplates();
    const templateId = this.props.templateUsed !== undefined && this.props.templateUsed !== ''
      ? this.props.templateUsed : this.templateOptions[0].value;
    this.caseCreateOptions = this.getSelectedTemplate(templateId);
    this.state = {
      templateId,
      fields: [],
      fieldDirty: false,
      currentStateMode: this.props.currentStateMode,
      isOpen: this.props.isOpen,
    };
  }

  // TODO: Change this usage
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextprops: any) {
    if (nextprops.isOpen !== this.state.isOpen) {
      const templateId = nextprops.templateUsed !== undefined
        && nextprops.templateUsed !== ''
        ? nextprops.templateUsed : this.state.templateId;
      this.caseCreateOptions = this.getSelectedTemplate(templateId);
      this.setState({
        inputCaseOptions: nextprops.inputCaseOptions,
        templateId,
        fields: (nextprops.currentStateMode === CONFIG_FORM_MODE.Create &&
          nextprops.templateUsed === undefined)
          ? this.updateFieldsByTemplate()
          : this.updateAndGetIntialFieldsAndValues(
            nextprops.currentStateMode, nextprops.inputCaseOptions,
          ),
        fieldDirty: false,
        currentStateMode: nextprops.currentStateMode,
        readOnly: (nextprops.currentStateMode === CONFIG_FORM_MODE.View),
        isOpen: nextprops.isOpen,
      });
      // Need a localChangeTracker because users can 'cancel' out of
      // this popup.  Once user presses OK in this popup, we can
      // set the global ChangeTracker to this localChangeTracker
      if (this.props.changeTracker) {
        this.localChangeTracker = cloneDeep(this.props.changeTracker);
      }
    }
  }

  getAllCaseOptionTemplates() {
    let { templates = CaseCreateOptions } = this.props;
    if (templates.length === 0) {
      templates = CaseCreateOptions;
    }
    const defaultOption = {
      label: 'Select',
      value: '',
    };

    const result = templates.filter((template: any) => template.category === 'CaseOptions').map((template: any) =>
      ({ label: template.name, value: template.id }));
    result.unshift(defaultOption);
    return result;
  }

  updateAndGetIntialFieldsAndValues = (currentStateMode: CONFIG_FORM_MODE, options: any) => {
    const readOnly = currentStateMode === CONFIG_FORM_MODE.View;
    const { fields } = this.caseCreateOptions;
    if (!options) return fields;
    const phirf = options.packetHeaderInformationReportFilter;
    const disabled = false; // TODO: Need to get the value from server? TBD if this is even needed.

    Object.keys(fields).forEach((key) => {
      if (key === 'summaryAF') {
        if (phirf && (phirf === 'REPORT' || phirf === 'ALL')) {
          fields[key].initial = true;
        } else {
          fields[key].initial = false;
        }
      } else if (key === 'headersAF') {
        if (phirf && (phirf === 'ALL' || phirf === 'SUMMARY')) {
          fields[key].initial = true;
        } else {
          fields[key].initial = false;
        }
      } else if (options[key] !== undefined) {
        fields[key].initial = options[key];
      }

      fields[key].disabled = disabled;
      fields[key].readOnly = readOnly;
    });

    return fields;
  }

  updateFieldsByTemplate = () => {
    if (this.ref.current === undefined || this.ref.current === null) {
      return this.caseCreateOptions.fields;
    }
    const options = this.ref.current.getValues();
    const { fields } = this.caseCreateOptions;

    Object.keys(fields).forEach((key) => {
      if (fields[key].initial !== undefined) {
        if (fields[key].initial === 'false') fields[key].initial = false;
        if (fields[key].initial === 'true') fields[key].initial = true;
      }
      if (options[key] !== undefined
        && (fields[key].initial === undefined || fields[key].initial === '')) {
        fields[key].initial = options[key];
      }
    });
    return fields;
  }

  shouldComponentUpdate(nextProps: any) {
    return nextProps.isOpen || this.props.isOpen !== nextProps.isOpen;
  }

  handleSubmit = () => {
    const payload = this.ref.current.getValues();
    payload.configureOptions = true;

    if (payload.summaryAF && payload.headersAF) {
      payload.packetHeaderInformationReportFilter = 'ALL';
    } else if (payload.summaryAF && !payload.headersAF) {
      payload.packetHeaderInformationReportFilter = 'REPORT';
    } else if (!payload.summaryAF && payload.headersAF) {
      payload.packetHeaderInformationReportFilter = 'SUMMARY';
    } else {
      payload.packetHeaderInformationReportFilter = 'NONE';
    }
    if (payload.summaryAF !== undefined) {
      payload.summaryAF = false;
    }
    if (payload.headersAF !== undefined) {
      payload.headersAF = false;
    }
    if (payload.encryptionType === undefined || payload.encryptionType === '') {
      payload.encryptionType = 'NONE';
    }
    if (payload.encryptionKey === undefined) {
      payload.encryptionKey = '';
    }
    if (payload.cccBillingType === undefined) {
      payload.cccBillingType = '';
    }
    if (payload.cccBillingNumber === undefined) {
      payload.cccBillingNumber = '';
    }
    if (payload.callTraceLogging === undefined ||
      payload.callTraceLogging === '' ||
      payload.callTraceLogging < 0) {
      payload.callTraceLogging = 0;
    }
    if (payload.traceLevel === undefined ||
      payload.traceLevel === '' ||
      payload.traceLevel < 0) {
      payload.traceLevel = 0;
    }
    if (payload.packetEnvelopeMessage === undefined ||
      payload.packetEnvelopeMessage === '') {
      payload.packetEnvelopeMessage = false;
    }
    if (payload.provideTargetIdentityInCcChannel) {
      payload.provideTargetIdentityInCcChannel =
        payload.provideTargetIdentityInCcChannel.toUpperCase();
    }

    payload.kpi = payload.kpi === undefined || payload.kpi === '' ?
      'false' : payload.kpi;

    payload.packetEnvelopeContent =
    payload.packetEnvelopeContent === undefined || payload.packetEnvelopeContent === ''
      ? 'false' : payload.packetEnvelopeContent;

    payload.realTimeText =
    payload.realTimeText === undefined || payload.realTimeText === ''
      ? 'false' : payload.realTimeText;

    payload.callPartyNumberDisplay =
    payload.callPartyNumberDisplay === undefined || payload.callPartyNumberDisplay === ''
      ? 'false' : payload.callPartyNumberDisplay;

    payload.directDigitExtractionData =
    payload.directDigitExtractionData === undefined || payload.directDigitExtractionData === ''
      ? 'false' : payload.directDigitExtractionData;

    payload.monitoringReplacementPartiesAllowed =
    payload.monitoringReplacementPartiesAllowed === undefined || payload.monitoringReplacementPartiesAllowed === ''
      ? 'false' : payload.monitoringReplacementPartiesAllowed;

    payload.circuitSwitchedVoiceCombined =
    payload.circuitSwitchedVoiceCombined === undefined || payload.circuitSwitchedVoiceCombined === ''
      ? 'false' : payload.circuitSwitchedVoiceCombined;

    payload.includeInBandOutBoundSignalling =
    payload.includeInBandOutBoundSignalling === undefined || payload.includeInBandOutBoundSignalling === ''
      ? 'false' : payload.includeInBandOutBoundSignalling;

    payload.location =
    payload.location === undefined || payload.location === ''
      ? 'false' : payload.location;

    payload.sms =
    payload.sms === undefined || payload.sms === ''
      ? 'false' : payload.sms;

    payload.target =
    payload.target === undefined || payload.target === ''
      ? 'false' : payload.target;

    payload.callIndependentSupplementaryServicesContent =
    payload.callIndependentSupplementaryServicesContent === undefined
      || payload.callIndependentSupplementaryServicesContent === ''
      ? 'false' : payload.callIndependentSupplementaryServicesContent;

    payload.packetSummaryCount =
    payload.packetSummaryCount === undefined || payload.packetSummaryCount === ''
      ? '0' : payload.packetSummaryCount;

    payload.packetSummaryTimer =
    payload.packetSummaryTimer === undefined || payload.packetSummaryTimer === ''
      ? '0' : payload.packetSummaryTimer;

    payload.generatePacketDataHeaderReport =
    payload.generatePacketDataHeaderReport === undefined || payload.generatePacketDataHeaderReport === ''
      ? 'false' : payload.generatePacketDataHeaderReport;

    payload.featureStatusInterval =
    payload.featureStatusInterval === undefined || payload.featureStatusInterval === ''
      ? '0' : payload.featureStatusInterval;

    payload.surveillanceStatusInterval =
    payload.surveillanceStatusInterval === undefined || payload.surveillanceStatusInterval === ''
      ? '0' : payload.surveillanceStatusInterval;

    delete payload.summaryAF;
    delete payload.headersAF;
    // user is committing the change, so we can set the global ChangeTracker to
    // this localChangeTracker
    if (this.localChangeTracker) {
      this.props.setWarrantTracker(this.localChangeTracker);
    }
    this.props.handleSubmit(payload, this.state.templateId);
  }

  handleFieldUpdate = (changedField: ChangedField) => {
    const {
      caseId,
      warrantId,
    } = this.props;

    const {
      name, value, label, valueLabel,
    } = changedField;
    const prevValue = this.state.fields[name].initial;
    const prevDisplayValue = prevValue; // todo: map prevValue to prevDisplayValue
    if (name === 'options') {
      const { fields } = this.caseCreateOptions;
      Object.keys(fields).forEach((key: any) => {
        if (key !== 'options' && key !== 'summaryAF' && key !== 'headersAF') {
          fields[key].disabled = !value;
        }
      });
      fields.summaryAF.disabled = !value;
      fields.headersAF.disabled = !value;
      this.setState({ fields });
    }
    this.setState({ fieldDirty: true });
    // when case option is updated, fire changeTracker case option
    if (this.localChangeTracker) {
      // ChangeTracker takes the original value from the first time addUpdated is called on a field.
      // Subsequent invocation of addUpdated on the same field will not change the original value.
      // Therefore, we can keep passing in the new prevTemplateId even after we have provided
      // the original value in the first invocation.

      let fieldKey = '';
      if (caseId != null) {
        fieldKey = `case.${caseId}.caseOption.${name}`;

      } else if (warrantId != null) {
        fieldKey = `warrant.${warrantId}.caseOption.${name}`;
      }

      this.localChangeTracker.addUpdated(fieldKey, valueLabel, label, prevDisplayValue);
    }
  }

  getSelectedTemplate = (templateId: any) => {
    const { templates = CaseCreateOptions } = this.props;
    let result = templates.filter((template: any) =>
      template.id === templateId);
    if (result === undefined || result.length === 0) {
      result = CaseCreateOptions;
    }
    const templateJson = JSON.parse(result[0].template);
    return templateJson;
  }

  handleTemplateChange = (e: any) => {
    const {
      caseId,
      warrantId,
    } = this.props;

    const prevValue = this.state.templateId;
    const prevTemplate = this.getSelectedTemplate(prevValue);
    const { value } = e.currentTarget;
    const template = this.getSelectedTemplate(value);

    this.caseCreateOptions = template;
    const fields = this.updateFieldsByTemplate();
    this.setState({ templateId: value, fields, fieldDirty: true });

    if (this.localChangeTracker) {
      // ChangeTracker takes the original value from the first time addUpdated is called on a field.
      // Subsequent invocation of addUpdated on the same field will not change the original value.
      // Therefore, we can keep passing in the new prevTemplateId even after we have provided
      // the original value in the first invocation.

      let fieldKey = '';
      if (caseId != null) {
        fieldKey = `case.${caseId}.caseOption.template`;

      } else if (warrantId != null) {
        fieldKey = `warrant.${warrantId}.caseOption.template`;
      }

      this.localChangeTracker.addUpdated(fieldKey, template.name, 'Template', prevTemplate.name);
    }
  }

  setReadOnly = (readOnly:boolean) => {
    const { fields } = this.caseCreateOptions;
    Object.keys(fields).forEach((key: any) => {
      fields[key].readOnly = readOnly;
    });
    this.setState({ fields });
    this.setState({ readOnly });
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    this.setState({ currentStateMode: stateMode });
    if (stateMode !== CONFIG_FORM_MODE.View) {
      this.setReadOnly(false);
    } else {
      this.setReadOnly(true);
    }
  }

  getTitle = (currentStateMode : CONFIG_FORM_MODE) => {
    return (currentStateMode === CONFIG_FORM_MODE.Edit || currentStateMode === CONFIG_FORM_MODE.Create) ? 'Edit Case Options' : 'View Case Options';
  }

  static getFormTemplateFields = (
    formTemplateFields: FormTemplateFieldMap,
    mode:string,
  ): FormTemplateFieldMap => {

    if (mode === CONFIG_FORM_MODE.View) {
      setAllTemplateFieldsAsReadOnly(formTemplateFields);
    }

    return formTemplateFields;
  }

  render() {
    const { privileges } = this.props;
    const { currentStateMode } = this.state;
    const hasPrivilegesToEdit =
       privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.Edit);

    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: true,
      fieldDirty: this.state.fieldDirty,
      onClose: this.props.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: this.props.isOpen,
      width: '100%',
      height: 'auto',
      overflow: 'auto',
      maxWidth: '910px',
      doNotDisplayEdit: true,
    };
    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          <Label className="bp3-inline">
            Template :
            <HTMLSelect
              options={this.templateOptions}
              value={(this.state.templateId) ? this.state.templateId : ''}
              onChange={this.handleTemplateChange}
              disabled={(this.state.currentStateMode === CONFIG_FORM_MODE.View)}
            />
          </Label>
          <DynamicForm
            name={this.state.templateId}
            fields={CaseOptions.getFormTemplateFields(this.state.fields, currentStateMode)}
            layout={this.caseCreateOptions.metadata.layout.rows}
            flexFlow="column"
            ref={this.ref}
            onSubmit={this.handleSubmit}
            fieldUpdateHandler={this.handleFieldUpdate}
            cssClassName="case-options-column-wrap"
          />
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  authentication: { privileges },
  warrants: {
    changeTracker,
  },
}: any) => ({
  privileges,
  changeTracker,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  setWarrantTracker,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CaseOptions);
