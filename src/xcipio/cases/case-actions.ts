import { History } from 'history';
import Http from '../../shared/http';
import { CASE_ROUTE } from '../../constants';

const http = Http.getHttp();

// action export constants
export const GET_ALL_CASES = 'GET_ALL_CASES';
export const GET_CASE_BY_ID = 'GET_CASE_BY_ID';
export const UPDATE_CASE = 'UPDATE_CASE';
export const PATCH_CASE = 'PATCH_CASE';
export const DELETE_CASE = 'DELETE_CASE';
export const CREATE_CASE = 'CREATE_CASE';
export const GET_ALL_CASES_FULFILLED = 'GET_ALL_CASES_FULFILLED';
export const GET_CASE_BY_ID_FULFILLED = 'GET_CASE_BY_ID_FULFILLED';
export const UPDATE_CASE_FULFILLED = 'UPDATE_CASE_FULFILLED';
export const PATCH_CASE_FULFILLED = 'PATCH_CASE_FULFILLED';
export const DELETE_CASE_FULFILLED = 'DELETE_CASE_FULFILLED';
export const CREATE_CASE_FULFILLED = 'CREATE_CASE_FULFILLED';
export const GET_ALL_CASES_BY_CASEIDS = 'GET_ALL_CASES_BY_CASEIDS';
export const GET_ALL_CASES_BY_CASEIDS_FULFILLED = 'GET_ALL_CASES_BY_CASEIDS_FULFILLED';
export const UPDATE_CASE_STATE = 'UPDATE_CASE_STATE';
export const UPDATE_CASES_STATE = 'UPDATE_CASES_STATE';
export const UPDATE_SELECTED_INDEX = 'UPDATE_SELECTED_INDEX';
export const CLEAR_CASES_STATE = 'CLEAR_CASES_STATE';
export const UPDATE_CASE_PROVISIONING_STATUS = 'UPDATE_CASE_PROVISIONING_STATUS';
export const BROADCAST_SELECTED_CASES = 'BROADCAST_SELECTED_CASES';
export const CLEAR_SELECTED_CASES = 'CLEAR_SELECTED_CASES';

// action creators
export const getAllCases = (id: number, body = '') => ({ type: GET_ALL_CASES, payload: http.get(`/cases/${id}?${body}&$view=reporting`) });
export const createCase = (body: any) => ({ type: CREATE_CASE, payload: http.post('/cases', body) });
export const updateCase = (id: number, body: any) => ({ type: UPDATE_CASE, payload: http.put(`/cases/${id}`, body) });
export const patchCase = (id: number, body: any) => ({ type: PATCH_CASE, payload: http.patch(`/cases/${id}`, body) });
export const deleteCase = (dfId: string, wId: string, id: number, body: any = null) => ({ type: DELETE_CASE, payload: http.delete(`/warrants/${dfId}/${wId}/cases/${id}`, { data: body }) });
export const getAllCasesByCaseIds = (dfId: string, wId: string, ids: string) =>
  ({ type: GET_ALL_CASES_BY_CASEIDS, payload: http.get(`/warrants/${dfId}/${wId}/cases?ids=${ids}`) });
export const getCaseById = (dfId: string, wId: string, cId: string) =>
  ({ type: GET_CASE_BY_ID, payload: http.get(`/warrants/${dfId}/${wId}/cases/${cId}`) });
export const updateCaseState = (caseDetails: any) => (
  { type: UPDATE_CASE_STATE, payload: caseDetails });
export const updateCasesState = (cases: any) => (
  { type: UPDATE_CASES_STATE, payload: cases });
export const updateCaseSelectedIndex = (index: number) => (
  { type: UPDATE_SELECTED_INDEX, payload: index });
export const clearCasesState = () => (
  { type: CLEAR_CASES_STATE, payload: null });
export const caseUpdateProvStatus = (dfId: string, wId: string, cId: string, payload: any) => (
  {
    type: UPDATE_CASE_PROVISIONING_STATUS,
    payload: http.put(`/warrants/${dfId}/${wId}/cases/${cId}/state`, payload),
  }
);

export const broadcastSelectedCases = (selectedCases?: any) => (
  { type: BROADCAST_SELECTED_CASES, selectedCases });
export const clearSelectedCases = () => (
  { type: CLEAR_SELECTED_CASES });
export function goToCaseList(history: History): () => void {
  return () => {
    history.push(`${CASE_ROUTE}`);
  };
}
