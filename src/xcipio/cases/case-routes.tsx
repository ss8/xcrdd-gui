import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import CaseFrame from './case-frame';

const moduleRoot = '/cases';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <CaseFrame {...props} />}
    />
  </Fragment>
);

export default ModuleRoutes;
