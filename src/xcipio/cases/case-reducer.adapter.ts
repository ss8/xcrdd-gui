import { CaseModel } from 'data/case/case.types';

/**
 * XREST returns duplicate HI3Interfaces in the case.hi3Interfaces array. (XC-34427)
 * Remove the duplicate here so there is no problem in where this case is used.
 */
export function adaptCasesToXRESTbug(cases: CaseModel[]): void {
  cases.forEach((aCase) => {
    adaptCaseToXRESTbug(aCase);
  });
}

/**
 * XREST returns duplicate HI3Interfaces in the case.hi3Interfaces array. (XC-34427)
 * Remove the duplicate here so there is no problem in where this case is used.
 */
export function adaptCaseToXRESTbug(aCase: CaseModel): void {
  if (!aCase.hi3Interfaces || aCase.hi3Interfaces.length <= 1) {
    return;
  }
  const allHi3InterfaceIds = aCase.hi3Interfaces.map((hi3Interface) => hi3Interface.id).sort();

  const duplicateInterfaceIds:string[] = [];
  let previousInterfaceId = allHi3InterfaceIds[0];
  for (let i = 1; i < allHi3InterfaceIds.length; i += 1) {
    if (allHi3InterfaceIds[i] === previousInterfaceId) {
      duplicateInterfaceIds.push(previousInterfaceId);
    }
    previousInterfaceId = allHi3InterfaceIds[i];
  }

  duplicateInterfaceIds.forEach((duplicateId) => {
    const foundIndex = aCase.hi3Interfaces?.findIndex((hi3Interface) => hi3Interface.id === duplicateId);
    if (foundIndex !== -1 && foundIndex !== undefined) {
      aCase.hi3Interfaces?.splice(foundIndex, 1);
    }
  });
}
