import React, { Fragment } from 'react';
import CaseGrid from './grid';

class CaseFrame extends React.Component<any, any> {
    private gridRef: any;

    constructor(props:any) {
      super(props);
      this.gridRef = React.createRef();
    }

    render() {
      return (
        <Fragment>
          <div className="col-md-12" style={{ height: 'inherit' }}>
            <CaseGrid ref={this.gridRef} {...this.props} />
          </div>
        </Fragment>
      );
    }
}

export default CaseFrame;
