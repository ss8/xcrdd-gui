import {
  TargetInfoHashItem, TargetInfo, TargetInfoOption, TARGET_INFO_TYPE,
} from './target-info-defs';
import AxsgpTargetInfo from './axsgp-target-info';

/**
 * Temporary solution for now until we have better mechanism to get message text
 */
export const errorMessages = {
  REQUIRED_TARGET: 'Target {}: Fill the required Target Info on the selected AFs',
  GPTARGET_INFO_PVOIP_NEED_CASETYPE_ALL: 'Target {}: Case type must be \'Data and Content\' for \'PVOIP\' selection',
  AF_MODEL_NOT_SUPPORTED: 'AF Model {} not supported',
};

export const getMessage = (message:string, errorMsgPrefix:string|null, arg1:string): string => {
  let errorMessage:string = message.replace(/{}/g, arg1);

  if (errorMsgPrefix) {
    errorMessage = `${errorMsgPrefix} ${errorMessage}`;
  } else {
    errorMessage = `Error: ${errorMessage}`;
  }
  return errorMessage;
};

export const getTargetInfoHashItem = (targetAfsJsonData: any, targetInfoType: string)
  : TargetInfoHashItem | null => {
  if (!targetAfsJsonData || !targetAfsJsonData.targetInfoHash ||
      targetAfsJsonData.targetInfoHash.length <= 0) return null;

  const targetInfoHashItem = targetAfsJsonData.targetInfoHash.find(
    (aTargetInfoHashItem: TargetInfoHashItem) =>
      (aTargetInfoHashItem.targetInfoType === targetInfoType),
  );

  return targetInfoHashItem;
};

/**
 *
 * @param afDetails has af.id, af.model, af.type, af.name, af.x3TargetAssociations...
 */
export const createDefaultTargetInfoHashItem = (
  afDetails: any,
  targetInfoType: TARGET_INFO_TYPE,
  targetType:string,
  caseServices: string[],
) : TargetInfoHashItem | null => {
  if (targetInfoType === TARGET_INFO_TYPE.GPTARGETINFO) {
    const targetInfo : TargetInfo | null = AxsgpTargetInfo.createDefaultTargetInfo(afDetails, targetType, caseServices);

    if (targetInfo) {
      return {
        targetInfoType: TARGET_INFO_TYPE.GPTARGETINFO,
        targetInfos: [targetInfo],
      };
    }
  }
  return null;
};

/**
 * Add targetInfoHashItem to tracker.
 *
 * Value update scenario is not implemented yet because summary screen that
 * uses the previous value is not used for Edit yet.
 *
 * @param changeTracker
 * @param targetInfoHashItem
 * @param caseId
 * @param targetId
 * @param _previousTargetInfoHashItem
 */
export const trackTargetInfoUpdate = (changeTracker:any, targetInfoHashItem: TargetInfoHashItem,
  caseId:string, targetId: string, _previousTargetInfoHashItem:TargetInfoHashItem|null) => {
  if (!changeTracker || !targetInfoHashItem || !caseId || !targetId) return;

  targetInfoHashItem.targetInfos.forEach((targetInfo:TargetInfo) => {
    if (targetInfo.options) {
      targetInfo.options.forEach((option:TargetInfoOption) => {
        let changeTrackerField:{label: string, value: string} | null = null;
        if (targetInfoHashItem.targetInfoType === TARGET_INFO_TYPE.GPTARGETINFO) {
          changeTrackerField = AxsgpTargetInfo.toDisplayLabel(option);
        }
        if (changeTrackerField) {
          changeTracker.addUpdated(`case.${caseId}.target.${targetId}.af.${targetInfoHashItem.targetInfoType}.afccc.${option.key}`,
            changeTrackerField.value, changeTrackerField.label, null);
        }
      });
    }
  });
};
