import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm from '../../../shared/form';
import AxsgpTargetTemplate from './axsgp-target-template.json';
import {
  TargetInfoHashItem, TargetInfo, TargetInfoOption, TARGET_INFO_TYPE,
} from './target-info-defs';
import { getTargetInfoHashItem, errorMessages, getMessage } from './util';
import { showError } from '../../../shared/toaster';

enum SERVICE {
  SMS = 'SMS',
  PVOIP = 'PVOIP',
  SIGTRAN = 'SIGTRAN',
  XCAP = 'XCAP',
  GTP = 'GTP'
}

interface Props {
  ref: (ref: any) => void; // not sure if needed

  // the data structure created on the GUI side to hold AF information
  // {
  //   id: string,
  //   name: string,
  //   model: string,
  //   type: string,
  // }
  afDetails: any;

  // The target type (string) of the target that this AXSGPTARGETINFO belongs to
  targetType: any;

  handleUpdate: (targetInfoHashItem: TargetInfoHashItem) => void;

  // the targetInfoHash JSON data of a target JSON data inside a warrant JSON data
  targetInfoHashItem: TargetInfoHashItem | null;

  children:never[];
}

/**
 * The available service types depends on the target type and AF type when provisioning
 * AxsgpTargetInfo.  Please see https://ss8inc.atlassian.net/wiki/spaces/XDIS/pages/902234473/DX+1.2+-+Endeavor+Warrant+Provisioning+Notes
 */
export class AxsgpTargetInfo extends React.Component<Props, any> {
  private targetCCCRef: any;

  constructor(props: any) {
    super(props);

    this.state = AxsgpTargetInfo.initializeState(this.props);
  }

  public static defaultProps = {
    children: undefined,
  }

  static initializeState = (props:any) => {
    if (AxsgpTargetInfo.isSupported(props.afDetails) === false) {
      return {
        afId: props.afDetails ? props.afDetails.id : undefined,
        fields: null,
        targetInfo: null,
      };
    }

    const fields = AxsgpTargetInfo.initializeTargetInfoFields(
      props.targetInfoHashItem, props.afDetails,
    );
    const originalTargetInfo: TargetInfo | null =
      AxsgpTargetInfo.findGpTargetInfo(props.targetInfoHashItem);

    return {
      afId: props.afDetails ? props.afDetails.id : undefined,
      fields,
      targetInfo: originalTargetInfo ? cloneDeep(originalTargetInfo) :
        AxsgpTargetInfo.createDefaultTargetInfo(props.afDetails, props.targetType, null),
    };
  }

  static getDerivedStateFromProps(props: any, state: any) {
    if (!props.afDetails || props.afDetails.id === state.afId) return state;
    if (AxsgpTargetInfo.isSupported(props.afDetails) === false) return state;
    const newState = AxsgpTargetInfo.initializeState(props);
    return newState;
  }

  /**
   * This is called when Case does not have AXSGP Target Info
   */
  static createDefaultTargetInfo = (_afDetails: any, targetType: string, caseServices: string[] | null) => {
    const afModels = Object.keys(AxsgpTargetTemplate);
    let serviceValue:string | null = null;
    afModels.forEach((afModel:string) => {
      if (afModel.startsWith('AXS') === true) {
        const formFields = (AxsgpTargetTemplate as any)[afModel].general.fields;
        const layout = AxsgpTargetInfo.getLayout(afModel, targetType);
        layout.forEach((row:string[]) => {
          row.forEach((fieldKey:string) => {
            if (formFields[fieldKey]?.initial === true &&
              AxsgpTargetInfo.shouldDefaultBasedOnService(fieldKey, caseServices)) {
              serviceValue = AxsgpTargetInfo.addToServiceValue(serviceValue, fieldKey);
            }
          });
        });
      }
    });

    if (serviceValue != null) {
      const targetInfo: TargetInfo = {
        ifid: null,
        options: [{ key: 'SERVICE', value: serviceValue }],
      };
      return targetInfo;
    }
    return null;
  }

  static getTargetInfoHashItem = (targetJsonData: any): TargetInfoHashItem | null => {
    return getTargetInfoHashItem(targetJsonData, TARGET_INFO_TYPE.GPTARGETINFO);
  }

  static findGpTargetInfo = (targetInfoHashItem:TargetInfoHashItem | null): TargetInfo | null => {
    if (!targetInfoHashItem || !targetInfoHashItem.targetInfos
      || targetInfoHashItem.targetInfos.length < 1) return null;
    return targetInfoHashItem?.targetInfos[0];
  }

  /**
   * return true if aService is a value in the GPTargetInfo Service value
   * Example of the service value 'SMS_PVOIP_SIGTRAN'
   */
  static isEnabledService = (targetInfoOption: TargetInfoOption | undefined, aService: string)
   : boolean => {
    if (!targetInfoOption) return false;

    if (targetInfoOption.value === null) return false;

    const regExp = `(^${aService}$|^[a-zA-Z_]+_${aService}_[a-zA-Z_]+$|^[a-zA-Z_]+_${aService}$|${aService}_[a-zA-Z_]+$)`;
    return (targetInfoOption.value.match(regExp) != null);
  }

  /**
   * afDetails is GUI-side Af model
   * targetInfoHash is the Target JSON data inside the Warrant JSON data
   * for Warrant REST API
   *
   */
  static initializeTargetInfoFields = (targetInfoHashItem: TargetInfoHashItem, afDetails: any)
    : any => {
    const targetInfo: TargetInfo | null =
      AxsgpTargetInfo.findGpTargetInfo(targetInfoHashItem);
    const { model } = afDetails;
    // AF Model not supported
    if (!(AxsgpTargetTemplate as any)[model]) {
      showError(getMessage(errorMessages.AF_MODEL_NOT_SUPPORTED, null, model));
      return {};
    }

    const fields = cloneDeep((AxsgpTargetTemplate as any)[model].general.fields);

    if (targetInfo) {
      const targetInfoOption:TargetInfoOption | undefined = targetInfo.options.find(
        (option:TargetInfoOption) => (option.key === 'SERVICE'),
      );
      Object.keys(SERVICE).forEach((aService: string) => {
        if (!fields[aService]) return;
        if (AxsgpTargetInfo.isEnabledService(targetInfoOption, aService)) {
          fields[aService].initial = true;
        } else {
          fields[aService].initial = false;
        }
      });
    }

    return fields;
  }

  static getLayout = (model: string, targetType: string) => {
    let targetTypeSpecificLayout = 'layout';
    if (model === 'AXS-7100') {
      targetTypeSpecificLayout = `layout_${targetType}`;
    }
    // AF Model not supported
    if (!(AxsgpTargetTemplate as any)[model]) return [];

    const layout =
      (AxsgpTargetTemplate as any)[model].general.metadata[targetTypeSpecificLayout].rows;
    return layout;
  }

  static shouldDefaultBasedOnService = (targetService: string, caseServices: string[] | null): boolean => {
    const defaultTargetServiceMap: { [key:string] :string[] } = {
      SMS: ['MOBILE', 'SMS'],
      PVOIP: ['MOBILE', 'VOICE-AND-IP-SMS', 'OFFNET-VOICE'],
      SIGTRAN: ['MOBILE', 'ROAMING'],
      XCAP: ['MOBILE', 'SUPPLEMENTARY'],
      GTP: ['MOBILE', 'DATA'],
    };

    if (!caseServices) {
      return true;
    }
    return caseServices.some((service) => defaultTargetServiceMap[targetService]?.includes(service) ?? false);
  }

  static addToServiceValue = (currentValue: string | null, value: string) => {
    if (!currentValue) return value;

    // value is already in currentValue
    const regExp = `(^${value}$|^[a-zA-Z_]+_${value}_[a-zA-Z_]+$|^[a-zA-Z_]+_${value}$|${value}_[a-zA-Z_]+$)`;
    if ((currentValue as string).match(regExp) != null) {
      return currentValue;
    }

    return `${currentValue}_${value}`;
  }

  static removeFromServiceValue = (currentValue: string | null, value: string): string | null => {
    if (!currentValue) return null;

    let newValue = `${currentValue}`;
    const matchingStrings: string[] = [`_${value}_`, `_${value}`, `${value}_`, `${value}`];
    matchingStrings.some((matchingString:string) => {
      const index = currentValue.indexOf(matchingString);
      if (index === -1) return false;
      if (index === 0) { // at the beginning
        newValue = currentValue.substring(matchingString.length, currentValue.length);
      } else if (index + matchingString.length === currentValue.length) { // at the end
        newValue = `${currentValue.substr(0, index)}`;
      } else { // in the middle
        newValue = `${currentValue.substr(0, index)}_${currentValue.substr(index + matchingString.length, currentValue.length - (index + matchingString.length))}`;
      }
      return true;
    });

    if (newValue.trim().length === 0) {
      return null;
    }
    return newValue;
  }

  static isValidTargetInfo = (targetInfo: TargetInfo, afModel:string, caseType:string | undefined | null,
    targetPrimaryType: string, errors : any[], errorMsgPrefix:string|null = null)
   : boolean => {
    const targetInfoOption:TargetInfoOption | undefined = targetInfo.options.find(
      (option:TargetInfoOption) => (option.key === 'SERVICE'),
    );

    let isValid = true;
    if (afModel === 'AXS-7002') {
      if (AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.GTP) === false) {
        errors.push(getMessage(errorMessages.REQUIRED_TARGET, errorMsgPrefix, targetPrimaryType));
        isValid = false;
      }
    } if (afModel === 'AXS-7100') {
      if (AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.PVOIP) && caseType !== 'ALL') {
        errors.push(getMessage(errorMessages.GPTARGET_INFO_PVOIP_NEED_CASETYPE_ALL, errorMsgPrefix, targetPrimaryType));
        isValid = false;
      }
      if (AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.PVOIP) === false &&
        AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.SMS) === false &&
        AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.SIGTRAN) === false &&
        AxsgpTargetInfo.isEnabledService(targetInfoOption, SERVICE.XCAP) === false) {
        errors.push(getMessage(errorMessages.REQUIRED_TARGET, errorMsgPrefix, targetPrimaryType));
        isValid = false;
      }
    }

    return isValid;
  }

  static toDisplayLabel = (targetInfoOption: TargetInfoOption)
  : {label: any, value: string} => {
    const returnValue: {label: any, value: string} = { label: '', value: '' };

    if (targetInfoOption.key === 'SERVICE') {
      returnValue.label = { afLabel: 'all associated AFs', fieldLabel: 'Service' };

      const values: string[] = [];
      const afModels = Object.keys(AxsgpTargetTemplate).filter((key:string) => (key.startsWith('AXS-')));
      let fields: any = {};
      afModels.forEach((afModel:string) => {
        if ((AxsgpTargetTemplate as any)[afModel]) {
          const anAfModelFields:any[] = (AxsgpTargetTemplate as any)[afModel].general.fields;
          fields = Object.assign(fields, anAfModelFields);
        }
      });

      Object.keys(fields).forEach((fieldKey:string) => {
        if (AxsgpTargetInfo.isEnabledService(targetInfoOption, fieldKey)) {
          values.push(fields[fieldKey].label);
        }
      });
      returnValue.value = values.join(', ');
    }

    return returnValue;
  };

  /**
   * returns a targetInfoHash item = {targetInfoType: 'GPTARGETINFO', targetInfos:[]}
   */
  handleFieldUpdate = (changedField :any) => {
    const {
      name, value,
    } = changedField;

    let newValue = null;
    let currentValue = null;
    if (this.state.targetInfo && this.state.targetInfo.options.length > 0
      && this.state.targetInfo.options[0]) {
      currentValue = this.state.targetInfo.options[0].value;
    }

    if (value === true) {
      newValue = AxsgpTargetInfo.addToServiceValue(currentValue,
        name);
    } else {
      newValue = AxsgpTargetInfo.removeFromServiceValue(
        currentValue,
        name,
      );
    }

    // AxsgpTargetInfo has only one option
    const newTargetInfoOptions: {key:string, value:string}[] = [];
    if (newValue != null && newValue !== '') {
      newTargetInfoOptions[0] = { key: 'SERVICE', value: newValue };
    }

    const newState = cloneDeep(this.state);
    newState.targetInfo.options = newTargetInfoOptions;
    this.setState(newState);

    const newTargetInfoHashItem: TargetInfoHashItem = {
      targetInfoType: TARGET_INFO_TYPE.GPTARGETINFO,
      targetInfos: [{ ...newState.targetInfo }],
    };
    this.props.handleUpdate(newTargetInfoHashItem);
  }

  static isSupported = (afDetails: any) : boolean => {
    if (afDetails == null || afDetails.model == null) return false;

    if ((AxsgpTargetTemplate as any)[afDetails.model] == null) return false;

    if (afDetails.x3TargetAssociations === TARGET_INFO_TYPE.GPTARGETINFO) {
      return true;
    }
    return false;
  }

  showAxsgpTargetInfoPanel = (afDetails:any, targetType:string|null|undefined) => (
    AxsgpTargetInfo.isSupported(afDetails)
    && targetType != null
  );

  render() {
    const {
      afDetails,
      targetType,
    } = this.props;

    const {
      afId,
      fields,
    } = this.state;

    return (
      this.showAxsgpTargetInfoPanel(afDetails, targetType)
        ? (
          <DynamicForm
            ref={(ref) => { this.targetCCCRef = ref; }}
            data-test="axsgpTargetInfo-form"
            name={afId}
            fields={fields}
            layout={AxsgpTargetInfo.getLayout(afDetails.model, targetType)}
            flexFlow="row"
            fieldUpdateHandler={this.handleFieldUpdate}
          />
        )
        :
          <Fragment />
    );
  }
}

const mapStateToProps = ({
  warrants: { changeTracker },
}: any) => ({
  changeTracker,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(AxsgpTargetInfo);
