import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm from '../../../shared/form';
import NortelMtxTemplate from './target-ccc-nortel-mtx-template.json';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import {
  TARGET_INFO_TYPE,
} from './target-info-defs';

const defaultOption = {
  label: 'Select',
  value: '',
};

const getTargetCCCFields = (props: any) => {
  const { afDetails, afDIDetails } = props;
  const fields: {[key: string]: any} = cloneDeep(NortelMtxTemplate.generalCCC.general.fields);
  fields.CDCID.options = getCDCOptions(afDIDetails);

  const afCCCInfo = props.selectedAfs && props.selectedAfs.afs
  && props.selectedAfs.afs.afccc
  && props.selectedAfs.afs.afccc.length !== 0
    ? props.selectedAfs.afs.afccc.find((af: any) => af.afType === afDetails.type) : [];

  const afSettings = (afCCCInfo && afCCCInfo.length !== 0) ? afCCCInfo.afSettings : [];
  const afSettingsPerId = (afSettings && afSettings.length !== 0)
    ? afSettings.find((af: any) => af.afids[0] === afDetails.id) : [];

  if (afSettingsPerId && afSettingsPerId.length !== 0) {
    const afCCCOptions = afSettingsPerId.options;
    Object.keys(fields).forEach((key: any) => {
      fields[key].readOnly = props.currentStateMode === CONFIG_FORM_MODE.View;
      const cccKeyValue = afCCCOptions
        ? afCCCOptions.find((item: any) => item.key === key) : undefined;
      const keyValue = cccKeyValue ? cccKeyValue.value : undefined;
      fields[key].initial = keyValue;
    });
    return fields;
  }

  Object.keys(fields).forEach((key: any) => {
    fields[key].readOnly = props.currentStateMode === CONFIG_FORM_MODE.View;
    fields[key].initial = undefined;
  });
  return fields;
};

const getCDCOptions = (afDIData: any) => {
  let result: any = [];
  if (afDIData) {
    if (afDIData.length === 1) {
      const af = afDIData[0];
      const options = af.options?.find((item: any) => item.key === 'cdcid');
      const cdcValue = options ? options.value : null;
      if (cdcValue != null) {
        result = [cdcValue];
      }
      result.unshift(defaultOption);
      return result;
    }
    result = afDIData.map((af: any) => {
      const options = af.options ? af.options.find((item: any) => item.key === 'cdcid') : undefined;
      const cdcValue = options ? options.value : null;
      return cdcValue;
    });

    result.unshift(defaultOption);
    return result;
  }
  return result;
};

class TargetCCCInfo extends React.Component<any, any> {
  private targetCCCRef: any;

  constructor(props: any) {
    super(props);
    this.state = {
      afId: undefined,
    };
  }

  static isSupportedAFType = (afDetails: any) => {
    if (afDetails == null || afDetails.model == null) return false;

    if (afDetails.x3TargetAssociations === TARGET_INFO_TYPE.MTXCFCI) {
      return true;
    }
    return false;
  }

  static getDerivedStateFromProps(props: any, state: any) {
    if (props.afDetails.id === state.afId) return state;
    const fields = getTargetCCCFields(props);
    return {
      afId: props.afDetails ? props.afDetails.id : undefined,
      fields,
    };
  }

  handleFieldUpdate = (changedField :any) => {
    const {
      name, value, label,
    } = changedField;
    this.setState({ CDCIndexValue: value });

    const options = this.targetCCCRef.getValues();
    const isValid = this.targetCCCRef.isValid(true, true);
    this.props.handleUpdate(true, options, isValid);
  }

  render() {
    return (
      this.props.afDetails && this.state.fields
        ? (
          <Fragment>
            <DynamicForm
              name={this.state.afId}
              fields={this.state.fields}
              layout={NortelMtxTemplate.generalCCC.general.metadata.layout.rows}
              flexFlow="row"
              ref={(ref) => { this.targetCCCRef = ref; }}
              fieldUpdateHandler={this.handleFieldUpdate}
            />
          </Fragment>
        )
        :
          <Fragment />
    );
  }
}

const mapStateToProps = ({
  warrants: { changeTracker },
}: any) => ({
  changeTracker,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(TargetCCCInfo);
