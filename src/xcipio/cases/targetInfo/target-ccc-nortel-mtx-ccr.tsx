import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Label, Icon } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm from '../../../shared/form';
import NortelMtxTemplate from './target-ccc-nortel-mtx-template.json';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { TabPanel } from '../../../shared/tabs/tabPanel';
import { CustomTabs, CustomTab } from '../../../shared/tabs/tabs';
import { TargetInfoHashItem, TargetInfo, TARGET_INFO_TYPE } from './target-info-defs';
import './target-ccc-nortel-mtx-ccr.scss';

interface Props {
    name: string,
    ref: (ref: any) => void,
    selectedTargetAf: any,
    isSurvOptCombined? : boolean,
    handleFieldUpdate: (e: any) => void,
    currentStateMode: any,
    targetId: string,
    afDetails: any,
    caseId: string,
    changeTracker: any,
    afid:string,

    // the targetInfoHash JSON data of a target JSON data inside a warrant JSON data
    targetInfoHashItem: TargetInfoHashItem | null;
  }

const getTargetCCCOptions = (props: any) => {
  if (!props.targetInfoHashItem?.targetInfos) {
    return [];
  }
  return props.targetInfoHashItem.targetInfos;
};

const getFields = (_afDetails: any, props: any) => {
  const targetCCCOptions = getTargetCCCOptions(props);
  if (targetCCCOptions && targetCCCOptions.length !== 0) {
    const result = targetCCCOptions.map((ifid: any) => {
      const allFields = props.isSurvOptCombined
        ? NortelMtxTemplate.combined.general.fields
        : NortelMtxTemplate.separated.general.fields;
      const fields: any = cloneDeep(allFields);
      Object.keys(fields).forEach((key: any) => {
        fields[key].readOnly = props.currentStateMode === CONFIG_FORM_MODE.View;
        const cccKeyValue = ifid.options
          ? ifid.options.find((item: any) => item.key === key) : undefined;
        const keyValue = cccKeyValue ? cccKeyValue.value : undefined;
        fields[key].initial = keyValue;
      });
      if (fields.SWITCHED && fields.SWITCHED.initial && fields.SWITCHED.initial.toUpperCase() === 'YES') {
        enableDisableFields(fields, 'DEDICATED', true, false);
      } else if (fields.SWITCHED && fields.SWITCHED.initial && fields.SWITCHED.initial.toUpperCase() === 'NO') {
        enableDisableFields(fields, 'SWITCHED', true, false);
        fields.SWITCHED.initial = 'NO';
      } else {
        enableDisableFields(fields, 'SWITCHED', true, false);
        enableDisableFields(fields, 'DEDICATED', true, false);
      }
      return fields;
    });
    return result;
  }
  return getInitialFields(props.isSurvOptCombined, props.currentStateMode);
};

const getInitialFields = (isSurvOptCombined: any, currentStateMode: any) => {
  const allFields = isSurvOptCombined
    ? NortelMtxTemplate.combined.general.fields
    : NortelMtxTemplate.separated.general.fields;
  const fields: any = cloneDeep(allFields);
  Object.keys(fields).forEach((key: any) => {
    fields[key].readOnly = currentStateMode === CONFIG_FORM_MODE.View;
    fields[key].initial = undefined;
  });
  enableDisableFields(fields, 'SWITCHED', true, false);
  enableDisableFields(fields, 'DEDICATED', true, false);
  return [fields];
};
const enableDisableFields = (fields: any, groupTag: string,
  readOnly: boolean, required: boolean) => {
  Object.keys(fields).forEach((key: any) => {
    if (fields[key].groupTag === groupTag) {
      if (!required) {
        fields[key].initial = undefined;
      }
      fields[key].readOnly = readOnly;
      if (fields[key].validation) {
        fields[key].validation.required = required;
      }
    }
  });
};

const getFormName = (tabName: string, name: string) => {
  const currDateTime = new Date().getTime();
  return `${tabName}-${name}-${currDateTime}`;
};

class TargetCCCInfoNortelMtxCCR extends React.Component<Props, any> {
  private nortelMtxRef1: any;

  private nortelMtxRef2: any;

  constructor(props: any) {
    super(props);
    const fields: any = getFields(this.props.selectedTargetAf, props);
    this.state = {
      selectedTabIndex: 0,
      isSurvOptCombined: props.isSurvOptCombined,
      tabs: fields.length === 1
        ? [{
          formName: getFormName('1', this.props.name), name: 1, label: '1', options: fields[0],
        }]
        : [{
          formName: getFormName('1', this.props.name), name: 1, label: '1', options: fields[0],
        },
        {
          formName: getFormName('2', this.props.name), name: 2, label: '2', options: fields[1],
        }],
      ccrData: getTargetCCCOptions(this.props),
    };
  }

  static getDerivedStateFromProps(props: any, state: any) {
    if (props.isSurvOptCombined === state.isSurvOptCombined) return state;
    const fields = getFields(props.selectedTargetAf, props);
    return {
      selectedTabIndex: 0,
      isSurvOptCombined: props.isSurvOptCombined,
      tabs: fields.length === 1
        ? [{
          formName: getFormName('1', props.name), name: 1, label: '1', options: fields[0],
        }]
        : [{
          formName: getFormName('1', props.name), name: 1, label: '1', options: fields[0],
        },
        {
          formName: getFormName('2', props.name), name: 2, label: '2', options: fields[1],
        }],
      ccrData: getTargetCCCOptions(props),
    };
  }

  handleFieldUpdate = (changedField :any) => {
    const { name, label } = changedField;
    let { value } = changedField;
    const newTabs = cloneDeep(this.state.tabs);

    const currTab = newTabs[this.state.selectedTabIndex];
    const fields = currTab.options;
    fields[name].initial = value;
    if (name === 'SWITCHED') {
      if (value === 'YES') {
        enableDisableFields(fields, 'DEDICATED', true, false);
        enableDisableFields(fields, 'SWITCHED', false, true);
        fields.SWITCHED.initial = 'YES';
      } else if (value === 'NO') {
        enableDisableFields(fields, 'SWITCHED', true, false);
        enableDisableFields(fields, 'DEDICATED', false, true);
        fields.SWITCHED.initial = 'NO';
      } else {
        enableDisableFields(fields, 'DEDICATED', true, false);
        enableDisableFields(fields, 'SWITCHED', true, false);
      }
    }
    const isTabValid = this.state.selectedTabIndex === 0
      ? this.nortelMtxRef1.isValid(true, true)
      : this.nortelMtxRef2.isValid(true, true);
    currTab.isTabValid = isTabValid;

    this.setState({ tabs: newTabs }, () => this.updateTargetOnIndex());

    if (this.props.afid) {
      const preValue = this.state.tabs[this.state.selectedTabIndex].options[name].initial;
      if (name === 'SWITCHED' || name === 'DEDICATED') {
        value = (value === true) ? 'YES' : 'NO';
      }
      if (this.props.changeTracker) {
        this.props.changeTracker.addUpdated(`case.${this.props.caseId}.target.${this.props.targetId}.af.${this.props.afDetails.id}.afccc.${name}`, value, { afLabel: 'all associated AFs', fieldLabel: label }, preValue);
      }
    }
  }

  updateTargetOnIndex = () => {
    const afCCCOptions: TargetInfo[] = this.state.tabs.map((ifid: any, index: any) => {
      const options: any = [];
      const afCCCOptionFields = cloneDeep(ifid.options);
      Object.keys(afCCCOptionFields)
        .filter((item: any) => afCCCOptionFields[item].noServerSubmit === undefined)
        .forEach((key: any) => {
          const value = afCCCOptionFields[key].initial;
          if (value != null) { // != includes checking for value !== undefined
            const keyValue = { key, value };
            options.push(keyValue);
          }
        });
      const ifId = index + 1;
      options.push({ key: 'CCRTYPE', value: this.props.isSurvOptCombined ? 'COMBINED' : 'SEPARATED' });
      return {
        ifid: ifId,
        options,
        isValid: ifid.isTabValid,
      };
    });
    const targetInfoHashItem: TargetInfoHashItem = {
      targetInfoType: TARGET_INFO_TYPE.MTXCFCI,
      targetInfos: afCCCOptions,
    };

    this.props.handleFieldUpdate(targetInfoHashItem);
  }

  isCCRDataValid = () => {
    const isValid = this.state.tabs.map((tab: any) => (tab.isTabValid));
    if (isValid.includes(false)) return false;
    return true;
  }

  handleAddTab = () => {
    const tabs = cloneDeep(this.state.tabs);
    const newId = tabs[0].name + 1;
    const fields = getInitialFields(this.props.isSurvOptCombined, this.props.currentStateMode);
    const newTab = {
      name: newId, label: '2', options: fields[0], isTabValid: false,
    };
    tabs.push(newTab);
    this.setState({ tabs });
    this.updateTargetOnIndex();
  }

  handleRemoveTab = () => {
    const tabsToRemove = this.state.tabs;
    tabsToRemove.splice(this.state.selectedTabIndex, 1);
    const resultTabs = Object.assign([], tabsToRemove);
    resultTabs[0].label = '1';
    this.setState({ tabs: resultTabs, selectedTabIndex: 0 });
    this.updateTargetOnIndex();
  }

  handleTabChange = (event: any, newValue: any) => {
    this.setState({ selectedTabIndex: newValue });
  };

  renderContentDeliveryType(): JSX.Element {
    const { isSurvOptCombined } = this.props;

    let text = 'Combined';

    if (!isSurvOptCombined) {
      text = 'Separated';
    }

    return (
      <div className="target-ccc-nortel-mtx-ccr-content-deliver-type">
        <span className="target-ccc-nortel-mtx-ccr-content-deliver-type-label">Content Delivery (CCR) Type</span>
        <span className="target-ccc-nortel-mtx-ccr-content-deliver-type-variation">{text} </span>
      </div>
    );
  }

  static renderTargetInfoTab(element: any, index:number): JSX.Element {
    return (
      <CustomTab label={element.label} key={index} />
    );
  }

  renderTargetInfoAddTabButton(visible: boolean): JSX.Element {
    if (!visible) {
      return <Fragment />;
    }

    return <Icon className="target-ccc-nortel-mtx-ccr-tabs-add-icon" icon="add" iconSize={15} onClick={this.handleAddTab} />;
  }

  renderTargetInfoTabs(): JSX.Element {
    const { selectedTabIndex, tabs } = this.state;

    return (
      <div className="target-ccc-nortel-mtx-ccr-tabs">
        <CustomTabs value={selectedTabIndex} onChange={this.handleTabChange}>
          { tabs.map(TargetCCCInfoNortelMtxCCR.renderTargetInfoTab) }
        </CustomTabs>
        {this.renderTargetInfoAddTabButton(tabs.length < 2)}
      </div>
    );
  }

  renderRemoveInfoTabPanel(visible:boolean): JSX.Element {
    if (!visible) {
      return <Fragment />;
    }
    return <Icon icon="trash" className="target-ccc-nortel-mtx-ccr-tab-panels-remove" onClick={this.handleRemoveTab} />;
  }

  renderTargetInfoTabPanels(): JSX.Element {
    const { tabs, selectedTabIndex } = this.state;
    const { isSurvOptCombined } = this.props;

    return (
      <Fragment>
        {tabs.map((element: any, index: number) => (
          <TabPanel value={selectedTabIndex} key={`${element.name}-${index}`} index={index}>
            {this.renderRemoveInfoTabPanel(tabs.length > 1)}
            <DynamicForm
              name={element.formName}
              fields={element.options}
              layout={isSurvOptCombined
                ? NortelMtxTemplate.combined.general.metadata.layout.rows
                : NortelMtxTemplate.separated.general.metadata.layout.rows}
              flexFlow="column"
              ref={index === 0
                ? (ref) => { this.nortelMtxRef1 = ref; }
                : (ref) => { this.nortelMtxRef2 = ref; }}
              fieldUpdateHandler={this.handleFieldUpdate}
            />
          </TabPanel>
        ))}
      </Fragment>
    );
  }

  render() {
    return (
      <Fragment>
        {this.renderContentDeliveryType()}
        {this.renderTargetInfoTabs()}
        {this.renderTargetInfoTabPanels()}
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  warrants: { changeTracker },
}: any) => ({
  changeTracker,
});

export default connect(mapStateToProps, null, null,
  { forwardRef: true })(TargetCCCInfoNortelMtxCCR);
