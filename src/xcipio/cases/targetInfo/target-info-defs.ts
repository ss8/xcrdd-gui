export interface TargetInfoOption {
  key: string,
  value: string
}

export interface TargetInfo {
  ifid: string | null,
  options: TargetInfoOption[],
}

export interface TargetInfoHashItem {
    targetInfoType: string,
    targetInfos: TargetInfo[]
}

export enum TARGET_INFO_TYPE {
  MTXCFCI = 'MTXCFCI',
  GPTARGETINFO='GPTARGETINFO'
}
