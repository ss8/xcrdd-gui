import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import { findByTestAttr, checkProps } from '../../../shared/__test__/testUtils';
import { AxsgpTargetInfo } from './axsgp-target-info';
import { TARGET_INFO_TYPE } from './target-info-defs';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

const defaultProps: any = {
  afDetails: {
    bAFAssociation: true,
    x3TargetAssociations: TARGET_INFO_TYPE.GPTARGETINFO,
    contentInterfaces: undefined,
    id: 'YWFfYXhzZ3A3MDy',
    name: 'aa_axsgp',
    model: 'AXS-7100', // 'AXS-7002'
    type: 'AXSGP',
  },

  targetInfoHashItem: {
    targetInfoType: TARGET_INFO_TYPE.GPTARGETINFO,
    targetInfos: [{
      ifid: null,
      options: [{ key: 'SERVICE', value: 'GTP_SMS' }],
    }],
  },

  // For target and service types that are allowed for 'AXS-7100' and 'AXS-7002'
  // See https://ss8inc.atlassian.net/wiki/spaces/XDIS/pages/902234473/DX+1.2+-+Endeavor+Warrant+Provisioning+Notes
  targetType: 'MSISDN',

  handleUpdate: jest.fn(() => {}),
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (setupProps = defaultProps) => {
  return shallow<AxsgpTargetInfo>(<AxsgpTargetInfo {...setupProps} />);
};

test('0. does not throw warning with expected props', () => {
  checkProps(AxsgpTargetInfo, defaultProps);
});

describe('A. test that AxsgpTargetInfo displays proper fields based on props in creation scenario', () => {
  test('A1 AxsgpTargetInfo displays proper selections and initial values for AXS-7100 + MSISDN', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetInfoHashItem: {}, targetType: 'MSISDN' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeTruthy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(true);
    expect(fieldPVOIP.prop('checked')).toBeTruthy();

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeTruthy();

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(true);
    expect(fieldXCAP.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_PVOIP_SIGTRAN_XCAP_GTP');
  });

  test('A2 AxsgpTargetInfo displays proper selections and initial values for AXS-7100 + IMSI', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetInfoHashItem: {}, targetType: 'IMSI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeTruthy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(false);

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeTruthy();

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(false);

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_SIGTRAN_GTP');
  });

  test('A3 AxsgpTargetInfo displays proper selections and initial values for AXS-7100 + IMEI', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetInfoHashItem: {}, targetType: 'IMEI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeTruthy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(false);

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(false);

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(false);

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_GTP');
  });

  test('A4 AxsgpTargetInfo displays proper selections and initial values for AXS-7002 + MSISDN', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetInfoHashItem: {}, targetType: 'MSISDN' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_PVOIP_SIGTRAN_XCAP_GTP');
  });

  test('A5 AxsgpTargetInfo displays proper selections and initial values for AXS-7002 + IMSI', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetInfoHashItem: {}, targetType: 'IMSI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_SIGTRAN_GTP');
  });

  test('A6 AxsgpTargetInfo displays proper selections and initial values for AXS-7002 + IMEI', () => {
    const wrapper = setup({ afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetInfoHashItem: {}, targetType: 'IMEI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_GTP');
  });

  test('A7 AxsgpTargetInfo displays is not displayed if there is no AF information', () => {
    const wrapper = setup({ afDetails: null, targetInfoHashItem: {}, targetType: 'IMEI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');
    expect(dynamicForm.exists()).toBe(false);
  });

  test('A8 AxsgpTargetInfo displays is not displayed if AF is not AXS-7100 or AXS-7002', () => {
    const wrapper = setup({ afDetails: { model: 'ABC' }, targetInfoHashItem: {}, targetType: 'IMEI' });
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');
    expect(dynamicForm.exists()).toBe(false);
  });
});

describe('B. test that AxsgpTargetInfo fields are populated according to the existing data', () => {
  test('B1 AxsgpTargetInfo all fields checked when value is SMS_PVOIP_SIGTRAN_XCAP', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SMS_PVOIP_SIGTRAN_XCAP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeTruthy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(true);
    expect(fieldPVOIP.prop('checked')).toBeTruthy();

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeTruthy();

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(true);
    expect(fieldXCAP.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_PVOIP_SIGTRAN_XCAP');
  });

  test('B2 AxsgpTargetInfo all fields checked when value is SMS_PVOIP_SIGTRAN_XCAP_GTP', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SMS_PVOIP_SIGTRAN_XCAP_GTP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeTruthy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(true);
    expect(fieldPVOIP.prop('checked')).toBeTruthy();

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeTruthy();

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(true);
    expect(fieldXCAP.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_PVOIP_SIGTRAN_XCAP_GTP');
  });

  test('B3 AxsgpTargetInfo only SIGTRAN field is checked when value is SIGTRAN_GTP for AXS-7100', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'IMSI' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SIGTRAN_GTP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeFalsy();

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SIGTRAN_GTP');
  });

  test('B4 AxsgpTargetInfo only XCAP fields when value is XCAP', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'XCAP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    expect(fieldSMS.exists()).toBe(true);
    expect(fieldSMS.prop('checked')).toBeFalsy();

    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    expect(fieldPVOIP.exists()).toBe(true);
    expect(fieldPVOIP.prop('checked')).toBeFalsy();

    const fieldSIGTRAN = findByTestAttr(dynamicForm.dive(), 'input-SIGTRAN');
    expect(fieldSIGTRAN.exists()).toBe(true);
    expect(fieldSIGTRAN.prop('checked')).toBeFalsy();

    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    expect(fieldXCAP.exists()).toBe(true);
    expect(fieldXCAP.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('XCAP');
  });

  test('B5 AxsgpTargetInfo only GTP field is checked when value is SIGTRAN_GTP for AXS-7002', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SIGTRAN_GTP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SIGTRAN_GTP');
  });

  test('B6 AxsgpTargetInfo only GTP field is checked when value is GTP for AXS-7002', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'GTP';
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('GTP');
  });
});

describe('C. test that AxsgpTargetInfo displays proper fields when props are changed', () => {
  test('C1 AxsgpTargetInfo displays fields for AXS-7100, then props changed to AXS-7002', () => {
    const props = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SMS_PVOIP_SIGTRAN_XCAP';
    const wrapper = setup(props);

    const nextProps = { ...defaultProps, afDetails: { ...defaultProps.afDetails, id: 'AAAAAAAA', model: 'AXS-7002' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SMS_GTP_PVOIP_SIGTRAN_XCAP';
    wrapper.setProps(nextProps);

    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');
    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    expect(field.exists()).toBe(true);
    expect(field.prop('checked')).toBeTruthy();

    expect(wrapper.update().state('targetInfo').options[0].value).toBe('SMS_GTP_PVOIP_SIGTRAN_XCAP');
  });
});

describe('D. test AxsgpTargetInfo Service value is set properly when field changes', () => {
  test('D1  TargetInfo service value is set properly when they are changed in GPTargetInfo creation scenario for AXS-7100 ', () => {
    const props: any = {
      ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetInfoHashItem: {}, targetType: 'MSISDN',
    };
    let serviceValue: string | null = null;
    props.handleUpdate = jest.fn((targetInfoHashItem) => {
      expect(targetInfoHashItem.targetInfos[0].options[0].value).toBe(serviceValue);
    });
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    serviceValue = 'SMS_SIGTRAN_XCAP_GTP';
    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    fieldPVOIP.simulate('change', { currentTarget: { type: 'checkbox', name: 'PVOIP', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SMS_SIGTRAN_GTP';
    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    fieldXCAP.simulate('change', { currentTarget: { type: 'checkbox', name: 'XCAP', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SIGTRAN_GTP';
    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    fieldSMS.simulate('change', { currentTarget: { type: 'checkbox', name: 'SMS', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SIGTRAN_GTP_PVOIP';
    fieldPVOIP.simulate('change', { currentTarget: { type: 'checkbox', name: 'PVOIP', value: true } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);
  });

  test('D2  TargetInfo service value is set properly when they are changed in GPTargetInfo edit scenario for AXS-7100 ', () => {
    const props: any = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7100' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'SMS_PVOIP_SIGTRAN';
    let serviceValue: string | null = null;
    props.handleUpdate = jest.fn((targetInfoHashItem) => {
      expect(targetInfoHashItem.targetInfos[0].options[0].value).toBe(serviceValue);
    });
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');

    serviceValue = 'SMS_SIGTRAN';
    const fieldPVOIP = findByTestAttr(dynamicForm.dive(), 'input-PVOIP');
    fieldPVOIP.simulate('change', { currentTarget: { type: 'checkbox', name: 'PVOIP', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SMS_SIGTRAN_XCAP';
    const fieldXCAP = findByTestAttr(dynamicForm.dive(), 'input-XCAP');
    fieldXCAP.simulate('change', { currentTarget: { type: 'checkbox', name: 'XCAP', value: true } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SIGTRAN_XCAP';
    const fieldSMS = findByTestAttr(dynamicForm.dive(), 'input-SMS');
    fieldSMS.simulate('change', { currentTarget: { type: 'checkbox', name: 'SMS', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);
  });

  test('D3  TargetInfo service value is set properly when they are changed in GPTargetInfo creation scenario for AXS-7002', () => {
    const props: any = {
      ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetInfoHashItem: {}, targetType: 'MSISDN',
    };
    let serviceValue: string | null = null;
    props.handleUpdate = jest.fn((targetInfoHashItem) => {
      if (serviceValue === null || serviceValue.length === 0) {
        expect(targetInfoHashItem.targetInfos[0].options.length).toBe(0);
      } else {
        expect(targetInfoHashItem.targetInfos[0].options[0].value).toBe(serviceValue);
      }
    });
    const wrapper = setup(props);
    serviceValue = 'SMS_PVOIP_SIGTRAN_XCAP_GTP';
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');
    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SMS_PVOIP_SIGTRAN_XCAP';
    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    field.simulate('change', { currentTarget: { type: 'checkbox', name: 'GTP', value: false } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);

    serviceValue = 'SMS_PVOIP_SIGTRAN_XCAP_GTP';
    // field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    field.simulate('change', { currentTarget: { type: 'checkbox', name: 'GTP', value: true } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);
  });

  test('D4  TargetInfo service value is set properly when they are changed in GPTargetInfo edit scenario for AXS-7002 ', () => {
    const props: any = { ...defaultProps, afDetails: { ...defaultProps.afDetails, model: 'AXS-7002' }, targetType: 'MSISDN' };
    props.targetInfoHashItem.targetInfos[0].options[0].value = 'GTP';
    let serviceValue: string | null = null;
    props.handleUpdate = jest.fn((targetInfoHashItem) => {
      if (serviceValue === null || serviceValue.length === 0) {
        expect(targetInfoHashItem.targetInfos[0].options.length).toBe(0);
      } else {
        expect(targetInfoHashItem.targetInfos[0].options[0].value).toBe(serviceValue);
      }
    });
    const wrapper = setup(props);
    const dynamicForm = findByTestAttr(wrapper, 'axsgpTargetInfo-form');
    expect(wrapper.update().state('targetInfo').options[0].value).toBe('GTP');

    serviceValue = '';
    const field = findByTestAttr(dynamicForm.dive(), 'input-GTP');
    field.simulate('change', { currentTarget: { type: 'checkbox', name: 'GTP', value: false } });

    expect(wrapper.update().state('targetInfo').options.length).toBe(0);

    serviceValue = 'GTP';
    field.simulate('change', { currentTarget: { type: 'checkbox', name: 'GTP', value: true } });

    expect(wrapper.update().state('targetInfo').options[0].value).toBe(serviceValue);
  });
});

describe('test toDisplayLabel', () => {
  test('converts SMS_GTP to SMS, Data & SMS', () => {
    const targetInfoOption = {
      key: 'SERVICE',
      value: 'SMS_GTP',
    };

    const result = AxsgpTargetInfo.toDisplayLabel(targetInfoOption);
    const serviceLabels = ['SMS', 'Data & MMS'];
    serviceLabels.forEach((label) => {
      const regExp = `(^${label}$|^.+, ${label}, .+$|^.+, ${label}$|${label}, .+$)`;
      expect(result.value).toMatch(new RegExp(regExp));
    });
    expect(result.label).toEqual({ afLabel: 'all associated AFs', fieldLabel: 'Service' });
  });

  test('converts SMS_GTP_PVOIP to GTP, Data & SMS, Passive VOIP', () => {
    const targetInfoOption = {
      key: 'SERVICE',
      value: 'SMS_GTP_PVOIP',
    };

    const result = AxsgpTargetInfo.toDisplayLabel(targetInfoOption);
    const serviceLabels = ['SMS', 'Data & MMS', 'Passive VOIP'];
    serviceLabels.forEach((label) => {
      const regExp = `(^${label}$|^.+, ${label}, .+$|^.+, ${label}$|${label}, .+$)`;
      expect(result.value).toMatch(new RegExp(regExp));
    });
    expect(result.label).toEqual({ afLabel: 'all associated AFs', fieldLabel: 'Service' });
  });
});

describe('createDefaultTargetInfo()', () => {
  let af:AccessFunction;
  beforeEach(() => {
    af = {
      tag: 'AXSGP',
      model: 'AXS-7100',
      name: 'AXS-7100',
      type: 'AXSGP',
      timeZone: '',
    };
  });

  it('select all options because all applies to the one service', () => {
    expect(AxsgpTargetInfo.createDefaultTargetInfo(af, 'MSISDN', ['MOBILE'])).toEqual(
      expect.objectContaining(
        {
          options: [{ key: 'SERVICE', value: 'SMS_PVOIP_SIGTRAN_XCAP_GTP' }],
        },
      ),
    );
  });

  it('select one of the options because it is the only one to the service', () => {
    expect(AxsgpTargetInfo.createDefaultTargetInfo(af, 'MSISDN', ['ROAMING'])).toEqual(
      expect.objectContaining(
        {
          options: [{ key: 'SERVICE', value: 'SIGTRAN' }],
        },
      ),
    );
  });

  it('select twp options because they apply to the services', () => {
    expect(AxsgpTargetInfo.createDefaultTargetInfo(af, 'MSISDN', ['ROAMING', 'SMS'])).toEqual(
      expect.objectContaining(
        {
          options: [{ key: 'SERVICE', value: 'SMS_SIGTRAN' }],
        },
      ),
    );
  });

});
