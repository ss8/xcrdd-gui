import cloneDeep from 'lodash.clonedeep';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { updateCaseBasedOnDerivedTargets } from 'xcipio/cases/forms/updateCaseBasedOnDerivedTargets';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import {
  targetsCircuitSwitch,
  targetsGizmoETSI,
  targets4G,
  misconfiguredTargetValueCircuitSwitch,
  onlyOneCASEIDTargetNotMatchingCircuitSwitch,
} from './__data__/updateCaseBasedOnDerivedTargets.test-data';

describe('updateCaseBasedOnDeriveTargets', () => {
  let warrantConfigInstance: WarrantConfiguration;
  beforeEach(() => {
    warrantConfigInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: jest.fn(),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

  });

  it('return properly updated targets when targets have CASEIDs for Nortel MTX and Samsung MSC', () => {
    const warrantCase = {
      services: ['GSMCDMA-2G-VOICE'],
      targets: targetsCircuitSwitch,
    };

    updateCaseBasedOnDerivedTargets(warrantCase, warrantConfigInstance);
    expect(warrantCase.targets.length).toBe(5);
    expect(warrantCase.targets).toEqual(expect.arrayContaining([
      expect.objectContaining({
        primaryType: 'CASEID',
        primaryValue: '133333333',
      }),
      expect.objectContaining({
        primaryType: 'MSISDN',
        primaryValue: '222222222',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMDN',
        primaryValue: '222222222',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMIN',
        primaryValue: '1111111111',
      }),
      expect.objectContaining({
        primaryType: 'MIN',
        primaryValue: '1111111111',
      }),

    ]));
  });

  it('return properly updated targets when targets have MDN for Gizmo ETSI', () => {
    const warrantCase = {
      services: ['ETSI-GIZMO-SERVICE'],
      targets: targetsGizmoETSI,
    };
    warrantConfigInstance.selectedAfTypesList = ['ETSI_GIZMO'];
    updateCaseBasedOnDerivedTargets(warrantCase, warrantConfigInstance);
    expect(warrantCase.targets.length).toBe(1);
    expect(warrantCase.targets).toEqual(expect.arrayContaining([
      expect.objectContaining({
        primaryType: 'MDNwService',
        primaryValue: '3434343343',
      }),
    ]));
  });

  it('return properly updated targets when targets have MDN for 4G', () => {
    const warrantCase = {
      services: ['LTE_4G_DATA'],
      targets: targets4G,
    };
    const expectedTargets = cloneDeep(targets4G);
    updateCaseBasedOnDerivedTargets(warrantCase, warrantConfigInstance);
    expect(warrantCase.targets).toEqual(expectedTargets);
  });

  it('return properly updated targets when Circuit Switch target values are misprovisioned', () => {
    const warrantCase = {
      services: ['GSMCDMA-2G-VOICE'],
      targets: misconfiguredTargetValueCircuitSwitch,
    };

    updateCaseBasedOnDerivedTargets(warrantCase, warrantConfigInstance);
    expect(warrantCase.targets.length).toBe(5);
    expect(warrantCase.targets).toEqual(expect.arrayContaining([
      expect.objectContaining({
        primaryType: 'CASEID',
        primaryValue: '2222222222',
      }),
      expect.objectContaining({
        primaryType: 'MSISDN',
        primaryValue: '3333333333',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMDN',
        primaryValue: '4444444444',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMIN',
        primaryValue: '5555555555',
      }),
      expect.objectContaining({
        primaryType: 'MIN',
        primaryValue: '6666666666',
      }),

    ]));
  });

  it('return properly updated targets when Circuit Switch target values are misprovisioned', () => {
    const warrantCase = {
      services: ['GSMCDMA-2G-VOICE'],
      targets: onlyOneCASEIDTargetNotMatchingCircuitSwitch,
    };

    updateCaseBasedOnDerivedTargets(warrantCase, warrantConfigInstance);
    expect(warrantCase.targets.length).toBe(5);
    expect(warrantCase.targets).toEqual(expect.arrayContaining([
      expect.objectContaining({
        primaryType: 'CASEID',
        primaryValue: '2222222222',
      }),
      expect.objectContaining({
        primaryType: 'MSISDN',
        primaryValue: '3333333333',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMIN',
        primaryValue: '5555555555',
      }),
      expect.objectContaining({
        primaryType: 'MIN',
        primaryValue: '5555555555',
      }),
      expect.objectContaining({
        primaryType: 'CASEIDMDN',
        primaryValue: '4444444444',
      }),

    ]));
  });
});
