import { Text } from '@blueprintjs/core';
import React from 'react';
import { AppToaster } from 'shared/toaster';

export default class ShowStatusMessage extends React.Component<any, any> {

  renderErrorMessage = (msgs: string[]): JSX.Element => {
    const [first, ...rest] = msgs;

    return (
      <React.Fragment>
        <p><strong>{first}</strong></p>
        {
          rest.map((msg) => <p>{msg}</p>)
        }
      </React.Fragment>
    );
  }

  shouldComponentUpdate = (nextProps: any) => {
    return nextProps.msgs && nextProps.msgs.length > 0;
  }

  componentDidUpdate() {
    AppToaster.clear();
    const {
      hasErrors, msgs, mode, isDialog,
    } = this.props;
    if (!hasErrors || !msgs || mode === 'edit' || isDialog) {
      return;
    }

    AppToaster.show({
      icon: 'error',
      intent: 'danger',
      timeout: 0,
      className: 'show-status-message-bp3-toast',
      message: this.renderErrorMessage(this.props.msgs),
    });

  }

  renderEditMode(): JSX.Element {
    const colorClassName = this.props.hasErrors && this.props.msgs ? 'error' : 'success';
    return (
      <div className={`status-form-container ${this.props.className}`}>
        {this.props.msgs.map((item: any) => {
          return (
            <div key={item} className={`row status-container ${colorClassName} `}>
              <Text className="col-md-12">
                {item}
              </Text>
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const { mode, isDialog } = this.props;

    if (mode === 'edit' || isDialog) {
      return this.renderEditMode();
    }

    return null;
  }
}
