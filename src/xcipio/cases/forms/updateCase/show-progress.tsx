import React from 'react';
import {
  Spinner, Icon, ProgressBar, Classes, Dialog,
} from '@blueprintjs/core';
import './update-case.scss';
import { Progress } from './updateCase.types';

export default class ShowProgress extends React.Component<any, any> {
  static getLoadingProgress = (progress: Progress[]): boolean =>
    progress.find((progressItem) => progressItem.loading)?.loading || false;

  shouldComponentUpdate = (nextProps: any) => {
    return nextProps.progress && nextProps.progress.length > 0;
  }

  renderDialogMessagesMode(): JSX.Element {
    return (
      <div>
        {this.props.progress.map((item: any) => {
          const intent = item.failed ? 'danger' : 'success';
          const icon = item.failed ? 'cross' : 'tick';
          return (
            <div key={item.id} className="row padding-top-bottom" style={{ marginBottom: '20px' }}>
              <span className="col-md-10">{item.text}</span>
              <span className="col-md-2">{item.loading
                ? <Spinner className="display-inline" size={Spinner.SIZE_SMALL} intent="primary" />
                : <Icon className="display-inline" intent={intent} icon={icon} />}
              </span>
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const { mode, isDialog } = this.props;
    if (mode === 'edit' || isDialog) {
      return this.renderDialogMessagesMode();
    }

    return (
      <div>
        <Dialog isOpen={ShowProgress.getLoadingProgress(this.props?.progress)}>
          <div className={Classes.DIALOG_HEADER}>
            Creating Warrant
          </div>
          <div className={Classes.DIALOG_BODY}>
            <ProgressBar intent="primary" value={0.5} />
          </div>
        </Dialog>
      </div>
    );
  }
}
