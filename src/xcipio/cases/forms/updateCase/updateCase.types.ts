export type Progress = {
  failed: boolean
  id: string
  loading: boolean
  text: string
}
