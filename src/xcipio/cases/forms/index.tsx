import React, { Fragment } from 'react';
import { AgGridReact } from '@ag-grid-community/react';
import { Button, Label, Popover } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import CaseActionRenderer from './case-action-renderer';
import CaseDetails from './case-details';
import casesColumnDefs from './column-def';
import CaseModel from '../case-model.json';
import ModalDialog from '../../../shared/modal/dialog';
import { checkGivenTimeWithCurrentTime } from '../../../shared/constants/checkTimeValidation';
import DateTimeCellRenderer from '../../../shared/commonGrid/date-cell-renderer';
import { TimeZoneCellRenderer } from '../../../shared/commonGrid/TimeZoneCellRenderer';

const newCase = cloneDeep(CaseModel);
class CasesSummary extends React.Component<any, any> {
  caseGridApi: any;

  caseFormRef: any;

  constructor(props: any) {
    super(props);
    const popUp = {
      message: '',
      actionText: '',
      isOpen: false,
      handleClose: null,
      handleSubmit: null,
    };

    this.state = {
      cases: props.cases,
      currentCaseIndex: 0,
      shouldCaseDetailsUpdate: false,
      shouldGridUpdate: false,
      formTemplate: props.formTemplate,
      popupParent: document.querySelector('div.bp3-dialog-container'), // prevent Ag-Grid Context Menu from being partially rendered because of the grid height
      popUp,
    };
    if (props.warrant) {
      this.setNewCaseFromWarrant(props.warrant, props.formTemplate);
    }
  }

  componentDidMount = () => {
    // prevent Ag-Grid Context Menu from being partially rendered because of the grid height
    const popupParent: any = document.querySelector('div.bp3-dialog-container');
    this.setState({ popupParent });
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: any) {
    this.setPropertiesFromWarrant(nextProps);
    this.setFormTemplateFromWarrant(nextProps);
  }

  rowDataChanged = () => {
    this.selectCurrentRow();
  }

  isCurrentCaseValid = (forceTouched = true) => {
    if (this.caseFormRef) {
      return this.caseFormRef.isCaseValid(forceTouched);
    }
    return true;
  }

  rowSelected = (e: any) => {
    if (e.node.selected) {
      const cases = this.state.cases.slice();
      const currentIndex = this.state.currentCaseIndex;
      if (cases.length > 1 && e.node.rowIndex !== currentIndex) {
        const currentWarrantCase = cases[currentIndex];
        currentWarrantCase.isValid = this.isCurrentCaseValid(false);
      }
      const state = {
        cases,
        currentCaseIndex: e.node.rowIndex,
        shouldCaseDetailsUpdate: true,
        shouldGridUpdate: false,
      };
      this.updateState(state);
    }
  }

  gridReady = (event: any) => {
    this.caseGridApi = event.api;
    event.api.sizeColumnsToFit();
    this.selectCurrentRow();
  }

  updateGridOnCaseDetailFieldChange = (name: any, value: any) => {
    const cases = this.state.cases.slice();
    const warrantCase = cases[this.state.currentCaseIndex];
    warrantCase[name] = value;
    switch (name) {
      case 'cfId': {
        const cfs = this.caseFormRef.getCfs();
        let selectedCf = {
          id: value,
          name: value,
        };
        if (cfs && cfs.length) {
          const fullCfObject = cfs.find((cf: any) => { return cf.id === value; });
          if (fullCfObject) {
            selectedCf = {
              id: fullCfObject.id,
              name: fullCfObject.name,
            };
          }
        }
        warrantCase[name] = selectedCf;
        break;
      }
      default: break;
    }
    const state = {
      cases,
      shouldCaseDetailsUpdate: false,
      shouldGridUpdate: true,
    };
    this.updateState(state);
  }

  selectCurrentRow = () => {
    if (this.caseGridApi === undefined) return;
    const rowNode = this.caseGridApi.getDisplayedRowAtIndex(this.state.currentCaseIndex);
    if (rowNode !== undefined) {
      rowNode.setSelected(true);
    }
  }

  updateState = (state: any) => {
    if (state.shouldGridUpdate) {
      state.cases.forEach((warrantCase: any, index: number) => {
        this.updateGridRow(index, warrantCase);
      });
    }
    this.setState(state);
  }

  updateGridRow = (index: number, rowData: any) => {
    if (this.caseGridApi === undefined) return;
    const rowNode = this.caseGridApi.getDisplayedRowAtIndex(index);
    if (rowNode !== undefined) {
      rowNode.setData(rowData);
    }
  }

  addCase = () => {
    // Edit warrants: Add Case
    const cases = this.state.cases.slice();
    const newWarrantCase: any = cloneDeep(newCase);
    // when new case added, fire changeTracker.addAdded('case'),
    let idCase;
    if (this.props.changeTracker) {
      idCase = this.props.changeTracker.nextId();
      this.props.changeTracker.addAdded(`case.${idCase}`);
    }
    const { fields }: any = this.state.formTemplate.general;
    newWarrantCase.entryDateTime = new Date();
    newWarrantCase.id = idCase; // put caseID to newWarrantCase
    this.setCaseFromTemplate(newWarrantCase, fields);
    cases.push(newWarrantCase);
    if (cases.length > 1) {
      const currentWarrantCase = cases[this.state.currentCaseIndex];
      currentWarrantCase.isValid = this.isCurrentCaseValid(false);
    }
    const state = {
      cases,
      currentCaseIndex: cases.length - 1,
      shouldCaseDetailsUpdate: false,
      shouldGridUpdate: true,
    };
    this.updateState(state);
  }

  // set new case based on template and warrant
  setNewCaseFromWarrant = (warrant: any, formTemplate: any) => {
    const {
      startDateTime, stopDateTime, timeZone, dxOwner,
    } = warrant;
    const isStartTimeHidden = this.startDateTimeIsHidden(formTemplate);
    const isWarrantStarttimeAndStoptimeValidFuturetime =
    !checkGivenTimeWithCurrentTime(startDateTime, timeZone) &&
     !checkGivenTimeWithCurrentTime(stopDateTime, timeZone) &&
       startDateTime < stopDateTime;
    // case startTime field is not hidden or warrant startTime and stopTime are future time
    if (!isStartTimeHidden || (isStartTimeHidden && isWarrantStarttimeAndStoptimeValidFuturetime)) {
      newCase.startDateTime = startDateTime;
    } else {
      newCase.startDateTime = '';
    }
    newCase.stopDateTime = stopDateTime;
    newCase.timeZone = timeZone;
    newCase.dxOwner = dxOwner;
  }

  startDateTimeIsHidden = (formTemplate: any) => {
    return (formTemplate && formTemplate.general && formTemplate.general.fields
       && formTemplate.general.fields.startDateTime
       && formTemplate.general.fields.startDateTime.hide);
  }

  setPropertiesFromWarrant = (nextProps: any) => {
    const cases: any[] = this.state.cases.slice();
    let isPropertySet = false;

    if (nextProps.startDateTime !== this.props.startDateTime) {
      this.updatePropertyOfArrayOfObject(cases, 'startDateTime', nextProps.startDateTime);
      newCase.startDateTime = nextProps.startDateTime;
      isPropertySet = true;
    }

    if (nextProps.stopDateTime !== this.props.stopDateTime) {
      this.updatePropertyOfArrayOfObject(cases, 'stopDateTime', nextProps.stopDateTime);
      newCase.stopDateTime = nextProps.stopDateTime;
      isPropertySet = true;
    }

    if (nextProps.timeZone !== this.props.timeZone) {
      this.updatePropertyOfArrayOfObject(cases, 'timeZone', nextProps.timeZone);
      newCase.timeZone = nextProps.timeZone;
      isPropertySet = true;
    }

    if (nextProps.dxOwner !== this.props.dxOwner) {
      cases.forEach((warrantCase: any) => {
        warrantCase.dxOwner = nextProps.dxOwner;
        newCase.dxOwner = nextProps.dxOwner;
        this.updatePropertyOfArrayOfObject(warrantCase.targets, 'dxOwner', nextProps.dxOwner);
      });
      isPropertySet = true;
    }

    if (nextProps?.warrant?.state !== this.props?.warrant?.state) {
      const warrantState = nextProps?.warrant?.state;
      const caseStatus = warrantState === 'ACTIVE' ? warrantState : 'INACTIVE';

      this.updatePropertyOfArrayOfObject(cases, 'status', caseStatus);
      this.updatePropertyOfArrayOfObject(cases, 'subStatus', warrantState);
      isPropertySet = true;
    }

    if (!isPropertySet) return;
    const state = {
      cases,
      shouldCaseDetailsUpdate: true,
      shouldGridUpdate: true,
    };
    this.updateState(state);
    if (this.props.changeTracker) {
      this.props.cases.forEach((warrantCase: any) => {
        this.props.changeTracker.addUpdated(`case.${warrantCase.id}`);
      });
    }
  }

  updatePropertyOfArrayOfObject = (arr: any[], property: string, value: any) => {
    arr.forEach((item: any) => {
      item[property] = value;
    });
  }

  setFormTemplateFromWarrant = (nextProps: any) => {
    if (this.state.formTemplate.key !== nextProps.formTemplate.key) {
      const cases = this.state.cases.slice();
      this.setCasesFromTemplate(cases, nextProps.formTemplate);
      this.setState({
        cases,
        formTemplate: nextProps.formTemplate,
        shouldCaseDetailsUpdate: true,
      });
      this.setNewCaseFromWarrant(nextProps.warrant, nextProps.formTemplate);
    }
  }

  setCasesFromTemplate = (cases: any[], template: any) => {
    const { fields }: any = template.general;
    cases.forEach((warrantCase: any, index: number) => {
      this.setCaseFromTemplate(warrantCase, fields);
      this.updateGridRow(index, warrantCase);
    });
  }

  setCaseFromTemplate = (warrantCase: any, fields: any[]) => {
    Object.keys(fields).forEach((field: any) => {
      const { initial } = fields[field];
      if (initial) {
        warrantCase[field] = initial;
      }
    });
  }

  getCases = () => {
    const cases = this.state.cases.slice();
    cases.forEach((warrantCase: any) => {
      const targets: any[] = [];
      warrantCase.targets.forEach((target: any) => {
        if (target.primaryValue || target.secondaryValue) {
          const clonedTarget = cloneDeep(target);
          // converts CASEIDMIN to CASEID and CASEIDMDN to CASEID
          if (clonedTarget.primaryType === 'CASEIDMIN' || clonedTarget.primaryType === 'CASEIDMDN') {
            clonedTarget.primaryType = 'CASEID';
          }
          // converts MDNwService to MDN
          if (clonedTarget.primaryType === 'MDNwService') {
            clonedTarget.primaryType = 'MDN';
          }
          // Only include AFs that requires AFWT to be created
          if (clonedTarget.afs && clonedTarget.afs.afIds) {
            const afIds:any[] = [];
            clonedTarget.afs.afIds.forEach((afDetail:any) => {
              if (afDetail.bAFAssociation === undefined) {
                afIds.push({ id: afDetail.id, name: afDetail.name });
              } else if (afDetail.bAFAssociation === true) {
                afIds.push({ id: afDetail.id, name: afDetail.name });
              }
            });
            clonedTarget.afs.afIds = afIds;
          }
          targets.push(clonedTarget);
        }
      });
      warrantCase.targets = targets;
    });
    return cases;
  }

  getCurrentCaseIndex = () => {
    return this.state.currentCaseIndex;
  }

  handleCaseDelete = (index: number) => {
    // Edit warrants: Delete case
    const popUp = {
      message: 'Are you sure you want to delete this case?',
      actionText: 'Yes',
      isOpen: true,
      handleClose: this.handlePopUpClose,
      handleSubmit: () => {
        const cases = this.state.cases.slice();
        if (cases.length > 1) {
          const currentWarrantCase = cases[this.state.currentCaseIndex];
          currentWarrantCase.isValid = this.isCurrentCaseValid(false);
        }
        cases.splice(index, 1);
        const state = {
          cases,
          currentCaseIndex: cases.length - 1,
          shouldCaseDetailsUpdate: false,
          shouldGridUpdate: true,
        };
        this.updateState(state);
      },
    };
    this.setState({ popUp });
    // when case is deleted, fire changeTracker.addDeleted('case.caseID')
    if (this.props.changeTracker) {
      this.props.changeTracker.addDeleted(`case.${this.state.cases[index].id}`, this.state.cases[index].liId);
    }
  }

  handlePopUpClose = () => {
    const popUp = { ...this.state.popUp };
    popUp.isOpen = false;
    this.setState({ popUp });
  }

  render() {
    return (
      <div style={{ marginTop: '10px' }}>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Label className="formLabel"> Cases:</Label>
          {this.props.mode === 'view' ?
            <Fragment /> :
            <Button className="icon" intent="primary" icon="plus" onClick={this.addCase} />}
        </div>
        <div
          className="ag-theme-balham"
          style={{
            marginTop: '10px',
            width: '100%',
          }}
        >
          <AgGridReact
            onGridReady={this.gridReady}
            onRowDataChanged={this.rowDataChanged}
            suppressMovableColumns
            columnDefs={casesColumnDefs}
            popupParent={this.state.popupParent}
            domLayout="autoHeight"
            rowData={this.state.cases}
            rowSelection="single"
            suppressCellSelection
            context={this}
            onRowSelected={this.rowSelected}
            onCellFocused={(e: any) => {
              if (e.column && e.column.colDef.cellRenderer === 'caseActionRenderer') {
                e.api.gridOptionsWrapper.gridOptions.suppressRowClickSelection = true;
              } else {
                e.api.gridOptionsWrapper.gridOptions.suppressRowClickSelection = false;
              }
            }}
            frameworkComponents={{
              caseActionRenderer: CaseActionRenderer,
              datetimeCellRenderer: DateTimeCellRenderer,
              TimeZoneCellRenderer,
            }}
          />
        </div>
        {this.state.cases !== undefined && this.state.cases.length > 0 ? (
          <CaseDetails
            ref={(ref: any) => { this.caseFormRef = ref; }}
            formTemplate={this.state.formTemplate}
            warrantCase={this.state.cases[this.state.currentCaseIndex]}
            originalValues={this.props.originalValues[this.state.currentCaseIndex]}
            customizationconfig={this.props.customizationconfig}
            updateGridOnCaseDetailFieldChange={this.updateGridOnCaseDetailFieldChange}
            shouldCaseDetailsUpdate={this.state.shouldCaseDetailsUpdate}
            global={this.props.global}
            mode={this.props.mode}
            config={this.props.config}
            templates={this.props.templates}
            changeTracker={this.props.changeTracker}
          />
        )
          :
          <div />}
        <ModalDialog
          displayMessage={this.state.popUp.message}
          isOpen={this.state.popUp.isOpen}
          onSubmit={this.state.popUp.handleSubmit}
          onClose={this.state.popUp.handleClose}
          actionText={this.state.popUp.actionText}
        />
      </div>
    );
  }
}

export default CasesSummary;
