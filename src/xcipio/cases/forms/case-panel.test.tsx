import React from 'react';
import { shallow } from 'enzyme';
import cloneDeep from 'lodash.clonedeep';
import { HI3Interface } from 'data/collectionFunction/collectionFunction.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { FormTemplate } from 'data/template/template.types';
import { CaseModel } from 'data/case/case.types';
import { mockCases } from '__test__/mockCases';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { CasePanel } from './case-panel';

describe('CasePanel', () => {
  describe('render()', () => {
    it('Should not throw exception when props are updated and cases still empty', () => {
      const props: any = {};
      const wrapper = shallow(<CasePanel cases={[]} {...props} />);
      expect(() => wrapper.setProps({ test: true })).not.toThrow();
    });
  });

  describe('setHI3InterfacesOperation', () => {
    let interfaces: HI3Interface[];
    let originalInterfaces: HI3Interface[];

    describe('when mode is Create', () => {
      it('should set all interfaces as CREATE', () => {
        interfaces = [{
          id: 'id1',
          name: 'name1',
        }, {
          id: 'id2',
          name: 'name2',
        }];

        originalInterfaces = [];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Create);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'CREATE',
        }, {
          id: 'id2',
          name: 'name2',
          operation: 'CREATE',
        }]);
      });
    });

    describe('when mode is Edit', () => {
      beforeEach(() => {
        interfaces = [{
          id: 'id1',
          name: 'name1',
        }, {
          id: 'id2',
          name: 'name2',
        }];

        originalInterfaces = [];
      });

      it('should set all interfaces as CREATE if not previous interfaces set', () => {
        CasePanel.setHI3InterfacesOperation(interfaces, [], CONFIG_FORM_MODE.Edit);

        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'CREATE',
        }, {
          id: 'id2',
          name: 'name2',
          operation: 'CREATE',
        }]);
      });

      it('should set all interfaces as NO_OPERATION if no changes', () => {
        originalInterfaces = cloneDeep(interfaces);

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'NO_OPERATION',
        }, {
          id: 'id2',
          name: 'name2',
          operation: 'NO_OPERATION',
        }]);
      });

      it('should set all interfaces as DELETE if all removed', () => {
        originalInterfaces = cloneDeep(interfaces);
        interfaces = [];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'DELETE',
        }, {
          id: 'id2',
          name: 'name2',
          operation: 'DELETE',
        }]);
      });

      it('should set mixed operation values', () => {
        originalInterfaces = [{
          id: 'id1',
          name: 'name1',
        }, {
          id: 'id3',
          name: 'name3',
        }];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'NO_OPERATION',
        }, {
          id: 'id2',
          name: 'name2',
          operation: 'CREATE',
        }, {
          id: 'id3',
          name: 'name3',
          operation: 'DELETE',
        }]);
      });

      it('should set current interface as CREATE and previous as DELETE when names are same ', () => {
        const newInterfaces = [{
          id: 'id1',
          name: 'name1',
        }];

        originalInterfaces = [{
          id: 'id3',
          name: 'name1',
        }];

        CasePanel.setHI3InterfacesOperation(newInterfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(newInterfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'CREATE',
        }, {
          id: 'id3',
          name: 'name1',
          operation: 'DELETE',
        }]);
      });

      it('should set as CREATE if previous set as CREATE', () => {
        interfaces = [interfaces[0]];

        originalInterfaces = [{
          ...interfaces[0],
          operation: 'CREATE',
        }];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'CREATE',
        }]);
      });

      it('should set as NO_OPERATION if previous set as DELETE', () => {
        interfaces = [interfaces[0]];

        originalInterfaces = [{
          ...interfaces[0],
          operation: 'DELETE',
        }];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'NO_OPERATION',
        }]);
      });

      it('should set as NO_OPERATION if previous set as NO_OPERATION', () => {
        interfaces = [interfaces[0]];

        originalInterfaces = [{
          ...interfaces[0],
          operation: 'NO_OPERATION',
        }];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'NO_OPERATION',
        }]);
      });

      it('should be empty if removed and if previous set as CREATE', () => {
        originalInterfaces = [{
          ...interfaces[0],
          operation: 'CREATE',
        }];

        interfaces = [];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([]);
      });

      it('should set as DELETE if removed and if previous set as DELETE', () => {
        originalInterfaces = [{
          ...interfaces[0],
          operation: 'DELETE',
        }];

        interfaces = [];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'DELETE',
        }]);
      });

      it('should set as DELETE if removed and if previous set as NO_OPERATION', () => {
        originalInterfaces = [{
          ...interfaces[0],
          operation: 'NO_OPERATION',
        }];

        interfaces = [];

        CasePanel.setHI3InterfacesOperation(interfaces, originalInterfaces, CONFIG_FORM_MODE.Edit);
        expect(interfaces).toEqual([{
          id: 'id1',
          name: 'name1',
          operation: 'DELETE',
        }]);
      });
    });
  });

  describe('isCaseDetailsValid()', () => {
    let caseEntry: CaseModel;
    let formTemplate: FormTemplate;
    let errors: string[];

    beforeEach(() => {
      caseEntry = {
        ...mockCases[0],
        name: 'Case 1',
        templateUsed: '1',
        caseIndex: 1,
      };

      const warrantTemplate = getMockTemplateByKey('wizardWarrantTemplateAndromeda');
      formTemplate = warrantTemplate.case as unknown as FormTemplate;

      errors = [];
    });

    it('should return true if case is valid', () => {
      expect(CasePanel.isCaseDetailsValid(caseEntry, formTemplate, errors)).toBe(true);
      expect(errors).toEqual([]);
    });

    it('should return false if case is not valid', () => {
      caseEntry.liId = '';
      expect(CasePanel.isCaseDetailsValid(caseEntry, formTemplate, errors)).toBe(false);
      expect(errors).toEqual(['Case 1 - Case Name: Please enter a value.']);
    });
  });
});
