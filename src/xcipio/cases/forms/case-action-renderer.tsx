import React, { Component } from 'react';
import {
  Icon, Button, Menu, MenuItem, Popover,
} from '@blueprintjs/core';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

export default class CaseActionRenderer extends Component<any, any> {
  handleDelete = () => {
    const { context } = this.props;
    context.handleCaseDelete(this.props.node.rowIndex);
  }

  render() {
    return (
      <span hidden={this.props.context.props.mode === 'view'}>
        <Icon intent="danger" icon="cross" onClick={this.handleDelete} iconSize={Icon.SIZE_LARGE} />
      </span>
    );
  }
}
