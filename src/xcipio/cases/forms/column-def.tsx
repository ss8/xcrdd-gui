const typeMap: any = {
  CD: 'Data Only',
  CC: 'Content Only',
  ALL: 'Data and Content',
  PEN: 'PEN Register',
  TRACE: 'Trap and Trace',
};
const typeFormatter = (data: any) => {
  const type = typeMap[data.value];
  return type || '';
};
const targetFormatter = (data: any) => {
  const targets = data.value;
  let targetsString = '';
  if (!targets || !targets.length) return targetsString;
  targets.forEach((target: any) => {
    const value = target.primaryValue || target.secondaryValue;
    if (value) {
      targetsString = `${targetsString}, ${value}`;
    }
  });
  targetsString = targetsString.substring(1);
  return targetsString;
};
const casesColumnDefs = [
  {
    headerName: 'Name', field: 'liId', suppressMenu: true, filter: 'text', editable: false,
  },
  {
    headerName: 'Targets', field: 'targets', valueFormatter: targetFormatter, editable: false,
  },
  {
    headerName: 'CF', field: 'cfId.name', editable: false,
  },
  {
    headerName: 'Start Time',
    field: 'startDateTime',
    cellRenderer: 'datetimeCellRenderer',
    editable: false,
  },
  {
    headerName: 'Stop Time',
    field: 'stopDateTime',
    cellRenderer: 'datetimeCellRenderer',
    editable: false,
  },
  {
    headerName: 'Time Zone',
    field: 'timeZone',
    sortable: false,
    editable: false,
    cellRenderer: 'TimeZoneCellRenderer',
  },
  {
    headerName: 'Type', field: 'type', valueFormatter: typeFormatter, editable: false,
  },
  {
    headerName: 'Status', field: 'status', editable: false,
  },
  {
    headerName: 'Action', field: 'action', editable: false, cellRenderer: 'caseActionRenderer', width: 100,
  },
];

export default casesColumnDefs;
