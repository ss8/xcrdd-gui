import React from 'react';
import ModalDialog from '../../../shared/modal/dialog';

class WarrantCaseDelete extends React.Component<any, any> {
    handleDelete = () => {
      //  TODO: need to pass the props to this file
      const { selectedDF: dfId } = this.props.global;
      const wId = this.props.location.data.wid;
      const cId = this.props.location.data.cid;
      this.props.deleteCase(String(dfId), String(wId), String(cId));
    }

    render() {
      //  TODO: Populate this from state or props  - what ever holds the case detaisl data.
      //  const {caseDetails} = "";
      const caseName = '';
      const displayMessage = `Are you sure you want to delete Case "${caseName}" ?`;
      return (
        <ModalDialog
          onSubmit={this.handleDelete}
          width="500px"
          {...this.props}
          actionText="Delete"
          displayMessage={displayMessage}
        />
      );
    }
}

export default WarrantCaseDelete;
