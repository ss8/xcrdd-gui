import WarrantConfiguration from 'xcipio/config/warrant-config';
import { afOntologyHasTarget } from 'xcipio/devices/updateTarget';
import { TargetModel, TargetModelArrayMap } from 'data/target/target.types';

/*
  * If a case is associated to a Nortel MTX AF, this function change the CASEID target type
  * to CASEIDMIN in the case that is being passed in
  */
// eslint-disable-next-line import/prefer-default-export
export function updateCaseBasedOnDerivedTargets(warrantCase: any, warrantConfig: WarrantConfiguration): void {
  if (!warrantCase) return;

  updateCaseBasedOnDerivedTargetsForCircuitSwitch(warrantCase, warrantConfig);
  updateCaseBasedOnDerivedTargetsForETSIGizmo(warrantCase, warrantConfig);

}

function toTargetModelMap(targets:TargetModel[]): TargetModelArrayMap {
  const targetModelArrayMap: TargetModelArrayMap = {};
  targets.forEach((target) => {
    if (target.primaryType) {
      if (!targetModelArrayMap[target.primaryType]) {
        targetModelArrayMap[target.primaryType] = [];
      }
      targetModelArrayMap[target.primaryType].push(target);
    }
  });
  return targetModelArrayMap;

}

function updateCaseBasedOnDerivedTargetsForCircuitSwitch(warrantCase: any, warrantConfig: WarrantConfiguration):void {
  // Since at this time, NORTEL_MTX AFs and Samsung MSC AFs have derived targets that need
  // to be converted from CASEID back to CASEIDMIN.
  // Samsung MSC have derived targets that need to be converted from CASEID back to CASEIDMDN.
  // The solution below is going to try to convert it assuming that only if there is
  // a MIN target, the MIN target has NORTEL_MTX AFs or Samsung MSC AFs, and CASEID is same as MIN target
  // that has NORTEL_MTX AFs or Samsung MSC AFs.
  // Similarily, if there is a MDN target, the MDN target has Samsung MSC AFs and CASEID is same as MDN target
  // that has Samsung SMC AFs.
  // Note that this is not the best solution and a new solution will be implemented
  // in DX-2163.
  const caseTargetModelArrayMap = toTargetModelMap(warrantCase.targets);

  // Process only when device has CASEID targets and service is Circuit Switch
  const hasCASEID = caseTargetModelArrayMap.CASEID !== undefined && caseTargetModelArrayMap.CASEID.length > 0;
  if (warrantCase.services.indexOf('GSMCDMA-2G-VOICE') < 0 || hasCASEID === false) {
    return;
  }

  // We should use customization file to do the look up.  But for now just hardcode.
  const targets: {sameValueTarget:string, afType: string, derivedTarget:string}[] = [];

  if (afOntologyHasTarget(warrantConfig, 'NORTEL_MTX', 'MIN') && afOntologyHasTarget(warrantConfig, 'NORTEL_MTX', 'CASEIDMIN')) {
    targets.push({
      sameValueTarget: 'MIN',
      afType: 'NORTEL_MTX',
      derivedTarget: 'CASEIDMIN',
    });
  }
  if (afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'MSISDN') && afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'CASEIDMDN')) {
    targets.push({
      sameValueTarget: 'MSISDN',
      afType: 'SAMSUNG_MSC',
      derivedTarget: 'CASEIDMDN',
    });
  }

  // Using matching target values to determine which CASEID is CASEIDMDN or which CASEID is CASEIDMIN
  targets.forEach(({
    sameValueTarget, afType, derivedTarget,
  }) => {
    // if derived target already exists in device, no need to add it.
    if (caseTargetModelArrayMap[derivedTarget] !== undefined && caseTargetModelArrayMap[derivedTarget].length > 0) {
      return;
    }

    let derivedCaseIDTargetIndex = -1;

    // find the CASEID that matches MIN/MDN value and if MIN/MSISDN has the corresponding AF type,
    // make the CASEID a derivedTarget
    if (sameValueTarget && caseTargetModelArrayMap[sameValueTarget]) {
      // Can safely assume there is only one MIN or one MDN per device
      const minOrMdnTarget = caseTargetModelArrayMap[sameValueTarget][0];

      // get CASEID that has same value as MIN/MDN
      const matchingCaseIDTargetIndex = caseTargetModelArrayMap.CASEID.findIndex((caseTarget) => {
        return (minOrMdnTarget.primaryValue === caseTarget.primaryValue);
      });

      // if matching value CASEID is found and if there is an actualTarget, make the CASEID a derivedTarget
      if (matchingCaseIDTargetIndex >= 0) {
        if (caseTargetModelArrayMap[sameValueTarget]) {
          const actualTarget = caseTargetModelArrayMap[sameValueTarget][0];
          const hasNeededAFTypes = actualTarget.afs?.afIds.find(({ tag }) => (afType === tag));
          if (hasNeededAFTypes) {
            derivedCaseIDTargetIndex = matchingCaseIDTargetIndex;
          }
        }
      }
    }

    if (derivedCaseIDTargetIndex >= 0) {
      const chosenType = caseTargetModelArrayMap.CASEID[derivedCaseIDTargetIndex];
      chosenType.primaryType = derivedTarget;
      caseTargetModelArrayMap.CASEID.splice(derivedCaseIDTargetIndex, 1);
    }
  });

  // The following is to take care of the case when users misprovision and did not put the same MSISDN value
  // in CASEIDMDN for SAMSUNG_MSC or did not put the same MIN valeu in CASEMIN for NORTEL MTX
  if (caseTargetModelArrayMap.CASEID.length > 0) {
    targets.forEach(({
      sameValueTarget, afType, derivedTarget,
    }) => {
      const hasDerivedTarget = (caseTargetModelArrayMap[derivedTarget] !== undefined
        && caseTargetModelArrayMap[derivedTarget].length > 0);
      if (caseTargetModelArrayMap.CASEID.length === 0 || hasDerivedTarget === true) return;

      let sameValueTargetHasAF = false;
      if (caseTargetModelArrayMap[sameValueTarget]) {
        sameValueTargetHasAF = caseTargetModelArrayMap[sameValueTarget].some((target) => {
          return (target.afs?.afIds.find(({ tag }) => (tag === afType)) !== undefined);
        });
      }

      // If there is an MSISDN for SAMSUNG_MSC in the device, one of the CASEID targets could be CASEIDMDN
      // Simiarily if there is MIN for NORTEL_MTX in the device, one of the CASEID targets could be CASEIDMIN
      if (sameValueTargetHasAF) {
        const matchingCaseIDTargetIndex = caseTargetModelArrayMap.CASEID.findIndex((target) => {
          const caseIDHasAFType = (target.afs?.afIds.find(({ tag }) => (tag === afType)) !== undefined);
          if (caseIDHasAFType || target.afs?.afIds.length === 0) {
            return true;
          }
          return false;
        });

        if (matchingCaseIDTargetIndex >= 0) {
          const chosenType = caseTargetModelArrayMap.CASEID[matchingCaseIDTargetIndex];
          chosenType.primaryType = derivedTarget;
          caseTargetModelArrayMap.CASEID.splice(matchingCaseIDTargetIndex, 1);
        }
      }
    });
  }

  // Following really hardcoded for SAMSUNG_MSC AF
  // SAMSUNG_MSC AF has required targets, MSISDN and CASEIDMDN which are derived from MDN.
  // The loop above takes care of creating CASEIDMDN.
  // SAMSUNG_MSC AF could have an optional CASEIDMIN target that is derived from MIN
  // but MIN is not a SAMSUNG_MSC target.  The following code is to figure out if we need to create a CASEIDMIN
  // for SAMSUNG_MSC
  // If deviceModel has an MSISDN target that has SAMSUNG_MSC AF and there are CASEID left in deviceModel,
  // the CASEID could be CASEIDMIN.
  let msisdnTargetHasSamsungMSC = false;
  if (caseTargetModelArrayMap.MSISDN) {
    msisdnTargetHasSamsungMSC = caseTargetModelArrayMap.MSISDN.some((msisdnTarget) => {
      return (msisdnTarget.afs?.afIds.find(({ tag }) => (tag === 'SAMSUNG_MSC')) !== undefined);
    });
  }

  const hasCASEIDMIN = (warrantCase.targets.find((target: TargetModel) => (target.primaryType === 'CASEIDMIN')) !== undefined);
  if (caseTargetModelArrayMap.CASEID.length > 0 && msisdnTargetHasSamsungMSC &&
       afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'CASEIDMIN') && hasCASEIDMIN === false) {

    // Since there is an MSISDN for SAMSUNG_MSC in the device, one of the CASEID targets could be CASEIDMIN
    if (msisdnTargetHasSamsungMSC) {
      const matchingCaseIDTargetIndex = caseTargetModelArrayMap.CASEID.findIndex((caseTarget) => {
        const hasSamsungMSC = (caseTarget.afs?.afIds.find(({ tag }) => (tag === 'SAMSUNG_MSC')) !== undefined);
        if (hasSamsungMSC || caseTarget.afs?.afIds.length === 0) {
          return true;
        }
        return false;
      });
      if (matchingCaseIDTargetIndex >= 0) {
        const chosenType = caseTargetModelArrayMap.CASEID[matchingCaseIDTargetIndex];
        chosenType.primaryType = 'CASEIDMIN';
        caseTargetModelArrayMap.CASEID.splice(matchingCaseIDTargetIndex, 1);
      }
    }
  }
}

function updateCaseBasedOnDerivedTargetsForETSIGizmo(warrantCase:any, warrantConfig: WarrantConfiguration):void {
  // Note that this is not the best solution and a new solution will be implemented
  // in DX-3330.
  const hasETSIGizmoAFType = warrantConfig.getSelectedAFTypeList().some((afType: string) => (afType === 'ETSI_GIZMO'));
  if (hasETSIGizmoAFType) {
    const possibleTargets = warrantCase.targets.filter(({ primaryType, afs }: any) => {
      return primaryType === 'MDN' && afs?.afAutowireTypes.some(({ tag }: { tag: string }) => tag === 'ETSI_GIZMO');
    });

    if (possibleTargets.length === 0) {
      // eslint-disable-next-line no-console
      console.warn('[updateCaseBasedOnDerivedTargets]', 'There is no MDN that match the criteria to be converted back to MDNwService');
      return;
    }

    if (possibleTargets.length > 1) {
      // eslint-disable-next-line no-console
      console.warn('[updateCaseBasedOnDerivedTargets]', 'Unexpected number of MDN\'s, assuming single MDN to be converted to MDNwService');
      return;
    }

    const [chosenType] = possibleTargets;
    chosenType.primaryType = 'MDNwService';
  }
}
