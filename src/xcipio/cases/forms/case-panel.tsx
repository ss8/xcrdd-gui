import React, { Fragment } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Callout, Checkbox } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { CaseInterfacesRefs, CaseModel, INTERCEPT_TYPE_CD } from 'data/case/case.types';
import { RootState } from 'data/root.reducers';
import {
  FormTemplate, FormTemplateFieldMap, FormTemplateOption, FormTemplateSection,
} from 'data/template/template.types';
import { Warrant } from 'data/warrant/warrant.types';
import * as globalSelectors from 'global/global-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as caseSelectors from 'data/case/case.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as collectionFunctionSelectors from 'data/collectionFunction/collectionFunction.selectors';
import * as userSelectors from 'users/users-selectors';
import { TargetDefaultAFSelection } from 'data/customization/customization.types';
import WarrantConfiguration, { UNSPECIFIED_SERVICE } from 'xcipio/config/warrant-config';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import { HI2Interface, HI3Interface } from 'data/collectionFunction/collectionFunction.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import { getDefaultHI3Interfaces } from 'xcipio/devices/forms/devices-to-cases';
import processCasesChanges from 'xcipio/devices/utils/processCasesChanges';
import { RetrievingDataProgress } from 'components';
import DynamicForm, { ChangedField } from '../../../shared/form';
import TargetPanel, {
  handleTargetsValidation, handleAFandTargetCCCValidation,
} from '../advancedProvisioning/targetPanel';
import { applyFieldValidators } from '../../../shared/form/form-validation';
import CaseOptions from '../options/case-options';
import HI2AndHI3Options from '../../cfs/hi2-hi3-interface';
import { AppToaster, showErrors } from '../../../shared/toaster';
import ModalDialog, { DONT_CLOSE_ON_SUBMIT } from '../../../shared/modal/dialog';
import { fetchCollectionFunctionHI3InterfaceList, fetchCollectionFunctionHI2InterfaceList } from '../../../data/collectionFunction/collectionFunction.actions';
import GencccPanel from '../genccc';
import { updateCaseState, updateCaseSelectedIndex, clearCasesState } from '../case-actions';
import {
  updateShowRemoveCaseInWarrantConfirmation as updateShowRemoveCaseInWarrantConfirmationAction,
  getShowRemoveCaseInWarrantConfirmation as getShowRemoveCaseInWarrantConfirmationAction,
} from '../../../users/users-actions';
import { checkGivenTimeWithCurrentTime }
  from '../../../shared/constants/checkTimeValidation';
import './case-panel.scss';
import WarrantTemplates from '../../templates/warrant-template';
import { getFormTemplateForCase, initializeWarrantCase } from '../../templates/case-template';
import { getCaseOptionTemplateValue } from '../../warrants/forms/warrant-panel';
import { TARGET_INFO_TYPE } from '../targetInfo/target-info-defs';

const errorKey = 'TargetPanel';

const mapState = (state: RootState) => ({
  selectedDF: globalSelectors.getSelectedDeliveryFunctionId(state),
  privileges: authenticationSelectors.getAuthenticationPrivileges(state),
  templates: warrantSelectors.getRawTemplates(state),
  warrant: warrantSelectors.getWarrant(state),
  changeTracker: warrantSelectors.getChangeTrackerInstance(state),
  warrantConfigInstance: warrantSelectors.getWarrantConfigurationInstance(state),
  cases: caseSelectors.getCasesWithTargetsIfNotSet(state),
  cfs: collectionFunctionSelectors.getCollectionFunctionList(state),
  bSelectAllHI3Interfaces: discoverySelectors.shouldSelectAllHi3Interfaces(state),
  userId: authenticationSelectors.getAuthenticationUserName(state),
  hideIncludeCaseConfirmation: userSelectors.shouldHideIncludeCaseConfirmation(state),
  targetDefaultAFSelection: warrantSelectors.getTargetDefaultAFSelection(state),
});

const mapDispatch = {
  updateCaseState,
  updateCaseSelectedIndex,
  clearCasesState,
  updateShowRemoveCaseInWarrantConfirmation: updateShowRemoveCaseInWarrantConfirmationAction,
  getShowRemoveCaseInWarrantConfirmation: getShowRemoveCaseInWarrantConfirmationAction,
};

const connector = connect(mapState, mapDispatch, null, { forwardRef: true });

type PropsFromRedux = ConnectedProps<typeof connector>;

type CasePanelProps = PropsFromRedux & {
  mode: string
  index: number
  onDeleteCase?: (caseId: string)=> void
};

type CasePanelState = {
  formTemplate: any
  warrantCase: any
  isCaseOptionsVisible: boolean
  isHi23InterfaceVisible: boolean
  currentStateMode: string
  popUp: any
  genccc: any
  caseId: string
  includeCasePopUpIsOpen: boolean
  hideIncludeCaseConfirmationChecked: boolean,
  isRetrievingInterfaces: boolean,
}

export class CasePanel extends React.Component<CasePanelProps, CasePanelState> {
  warrantConfig: WarrantConfiguration | null = null;
  cfs: any = [];
  caseRef: any;
  caseOptionRef: any;
  targetPanelRef: any;
  genCCCSummaryRef: any;
  unTouchedWarrantCase: any = {};
  formTemplate: FormTemplate | null = null;
  warrantTemplates: any;
  cfId: any = {};
  hi2Data: HI2Interface[] = [];
  hi3Data: HI3Interface[] = [];

  constructor(props: CasePanelProps) {
    super(props);
    const popUp = {
      message: '',
      actionText: '',
      isOpen: false,
      handleClose: null,
      handleSubmit: null,
    };

    const warrantCase = cloneDeep(props.cases[props.index]);
    if (warrantCase) {
      this.unTouchedWarrantCase = cloneDeep(warrantCase);
      this.warrantConfig = props.warrantConfigInstance;
      this.formTemplate = getFormTemplateForCase(props.templates, props.warrant?.templateUsed ?? '',
        WarrantTemplates.CATEGORY_WARRANT_WIZARD);
      this.cfId = warrantCase.cfId;
      this.updateHi2andHi3Data(warrantCase);
      this.updateCfOptions(warrantCase);
      this.updateInterface(warrantCase);
      this.modifyCfId(warrantCase);
      this.warrantConfig?.computeGencccEnabledForTargetAfs(warrantCase.allTargetAfs ?? []);
      const genccc = this.isGencccEnabled(warrantCase);
      if (!warrantCase.genccc) {
        warrantCase.genccc = {};
      }

      const formTemplate = initializeWarrantCase(
        cloneDeep(this.formTemplate),
        warrantCase.id,
        props.changeTracker,
        this.showCaseOptions,
        this.showHI23Interface,
        props.templates,
        props.cases,
        this.getServicesOptions(warrantCase.services),
        props.mode,
        warrantCase,
        this.handleDeleteCase,
      );

      this.state = {
        formTemplate,
        warrantCase,
        isCaseOptionsVisible: false,
        isHi23InterfaceVisible: false,
        currentStateMode: props.mode,
        popUp,
        genccc,
        caseId: warrantCase.id,
        includeCasePopUpIsOpen: false,
        hideIncludeCaseConfirmationChecked: false,
        isRetrievingInterfaces: false,
      };
      this.props.updateCaseSelectedIndex(this.props.index);
    }
  }

  componentDidMount() {
    const { getShowRemoveCaseInWarrantConfirmation } = this.props;
    if (getShowRemoveCaseInWarrantConfirmation) {
      getShowRemoveCaseInWarrantConfirmation();
    }
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: CasePanelProps): void {
    const {
      mode,
    } = this.props;

    if (!nextProps.cases || nextProps.cases.length === 0) {
      return;
    }

    const warrantCase = cloneDeep(nextProps.cases[nextProps.index]);
    if (mode !== CONFIG_FORM_MODE.View && this.state.caseId === warrantCase.id) {
      return;
    }

    this.unTouchedWarrantCase = cloneDeep(warrantCase);
    this.cfId = warrantCase.cfId;
    this.updateHi2andHi3Data(warrantCase);
    this.updateCfOptions(warrantCase);
    this.updateInterface(warrantCase);
    this.modifyCfId(warrantCase);
    this.warrantConfig?.computeGencccEnabledForTargetAfs(warrantCase.allTargetAfs ?? []);
    const genccc = this.isGencccEnabled(warrantCase);
    if (!warrantCase.genccc) {
      warrantCase.genccc = {};
    }

    const formTemplate = initializeWarrantCase(
      cloneDeep(this.formTemplate),
      warrantCase.id,
      nextProps.changeTracker,
      this.showCaseOptions,
      this.showHI23Interface,
      nextProps.templates,
      nextProps.cases,
      this.getServicesOptions(warrantCase.services),
      nextProps.mode,
      warrantCase,
      this.handleDeleteCase,
    );

    this.setState({
      formTemplate,
      warrantCase,
      isCaseOptionsVisible: false,
      isHi23InterfaceVisible: false,
      currentStateMode: nextProps.mode,
      genccc,
      caseId: warrantCase.id,
    });

    this.props.updateCaseSelectedIndex(nextProps.index);
  }

  componentDidUpdate(_prevProps: CasePanelProps): void {
    const {
      cases,
      mode,
      index,
    } = this.props;

    if (!cases || cases.length === 0 || mode === CONFIG_FORM_MODE.View) {
      return;
    }

    const {
      warrantCase,
    } = this.state;

    if (warrantCase.isValid !== true && this.caseRef) {
      this.caseRef.isValid(true, true);
    }

    if (!this.targetsArrayEqual((cases[index].targets),
      warrantCase.targets)) {
      this.targetPanelRef.targetSummaryRef.areTargetsValid();
    }
  }

  getServicesOptions(services: string[]): FormTemplateOption[] {
    let serviceOptions: FormTemplateOption[] = [];

    if (WarrantConfiguration.hasUnsupportedServices(services)) {
      serviceOptions = [UNSPECIFIED_SERVICE];

    } else {
      serviceOptions = this.warrantConfig?.getServiceDropDownOptions() ?? [];
    }

    return serviceOptions;
  }

  targetsArrayEqual = (targetsA:any, targetsB:any) => {
    let isEqual = true;
    targetsA.forEach((atarget:any, index: number) => {
      if (atarget.primaryValue !== targetsB[index].primaryValue) {
        isEqual = false;
      }
    });
    return isEqual;
  }

  // verify current case
  isCurrentCaseValid = (forceTouched = true, warrantCase: any, errors: any[]) => {
    let isValid = true;
    if (this.caseRef) { // check case detail
      isValid = this.caseRef.isValid(forceTouched, true);
    }
    if (this.targetPanelRef && this.targetPanelRef.targetSummaryRef) { // target values
      isValid = isValid && this.targetPanelRef.targetSummaryRef.areTargetsValid();
    }

    if (this.genCCCSummaryRef) {
      const isGenCCCValid = this.genCCCSummaryRef.isValid(forceTouched);
      isValid = isValid && isGenCCCValid;
    }

    if (!isValid) {
      errors.push('Selected Case :: Required field values missing or invalid');
    }

    const { targets, allTargetAfs } = warrantCase;
    const isSurOptCombined = warrantCase.options
                        && warrantCase.options.circuitSwitchedVoiceCombined;
    isValid = isValid &&
    handleAFandTargetCCCValidation(targets, allTargetAfs, errorKey,
      warrantCase, errors, isSurOptCombined, this.props.targetDefaultAFSelection);
    return isValid;
  }

  // verify other cases include: caseDetails, targets, af, targetCCC
  static isCaseValid(
    warrantConfig: any,
    warrantCase: any,
    formTemplate: any,
    errors: any[],
    targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  ): boolean {
    let caseValidationResult = CasePanel.isCaseDetailsValid(warrantCase, formTemplate, errors);
    const { targets, allTargetAfs } = warrantCase;
    const targetFields = warrantConfig.getTargetFields(warrantCase.targets);
    const isSurOptCombined = warrantCase.options
      && warrantCase.options.circuitSwitchedVoiceCombined;
    caseValidationResult = caseValidationResult &&
     handleTargetsValidation(targets, allTargetAfs, errorKey, warrantCase,
       errors, targetFields, isSurOptCombined, targetDefaultAFSelection);
    return caseValidationResult;
  }

  static isCaseDetailsValid(warrantCase: CaseModel, formTemplate: FormTemplate, errors: string[]): boolean {
    let caseValidationResult = true;
    Object.entries((formTemplate.general as FormTemplateSection).fields).forEach(([fieldName, formTemplateField]) => {
      const { label, validation } = formTemplateField;
      const caseFieldValue = warrantCase[fieldName as keyof CaseModel];

      if (validation?.required) {
        const { isValid, errorMessage } = applyFieldValidators(formTemplateField.validation, caseFieldValue, null);
        caseValidationResult = caseValidationResult && isValid;

        if (!isValid && errorMessage) {
          errors.push(`Case ${warrantCase.caseIndex} - ${label}: ${errorMessage}`);

        } else if (!isValid && errorMessage === '') {
          const validators = validation.validators ?? [];

          validators.forEach((validator) => {
            errors.push(`Case ${warrantCase.caseIndex} - ${label}: ${validator.errorMessage}`);
          });
        }
      }
    });

    return caseValidationResult;
  }

  static updateKey = (formTemplate: any) => {
    formTemplate.key = `Key_${new Date().getTime()}`;
  }

  static getFormTemplateFields = (
    formTemplateFields: FormTemplateFieldMap,
    mode:string,
  ): FormTemplateFieldMap => {

    if (mode === CONFIG_FORM_MODE.View) {
      setAllTemplateFieldsAsReadOnly(formTemplateFields);
    }

    return formTemplateFields;
  }

  modifyCfId = (defaults: any) => {
    if (defaults && defaults.cfId) {
      defaults.cfId = defaults.cfId.id;
      defaults?.hi3Interfaces?.forEach((item: any) => {
        if (!item.operation) {
          item.operation = 'NO_OPERATION';
        }
      });
    }
  }

  showCaseOptions = () => {
    this.setState({ isCaseOptionsVisible: true });
  }

  handleCaseOptionsClose = () => {
    this.setState({ isCaseOptionsVisible: false });
  }

  handleCaseOptionsSubmit = (options: any, templateId: any) => {
    const warrantCase = { ...this.state.warrantCase };
    warrantCase.options = options;
    warrantCase.templateUsed = templateId;
    const formTemplate = cloneDeep(this.state.formTemplate);
    CasePanel.updateKey(formTemplate);
    if (options.circuitSwitchedVoiceCombined) {
      this.clearTargetsCCCvaluesCCRTypeSecondary(warrantCase);
    }
    this.setState({
      warrantCase,
      isCaseOptionsVisible: false,
      formTemplate,
    });

    const {
      bSelectAllHI3Interfaces,
      mode,
    } = this.props;
    const hi3Interfaces = CasePanel.createHI3Interfaces(warrantCase.type, warrantCase, bSelectAllHI3Interfaces, mode);
    this.handleHI23InterfaceSubmit(hi3Interfaces, warrantCase);

    // when case options updated, fire changeTracker.addUpdate('case.caseID)
    if (this.props.changeTracker) {
      this.props.changeTracker.addUpdated(`case.${this.state.warrantCase?.id}`, null);
    }
  }

  clearTargetsCCCvaluesCCRTypeSecondary = (warrantCase: any) => {
    const { targets } = warrantCase;
    targets.forEach((target: any) => {
      if (!target.afs || !target.targetInfoHash) return;
      const allSelectedTargetInfoHash = target.targetInfoHash;
      const nortelTargetInfoItem = allSelectedTargetInfoHash.find((targetInfoHashItem: any) =>
        targetInfoHashItem.afType === TARGET_INFO_TYPE.MTXCFCI);
      if (nortelTargetInfoItem && nortelTargetInfoItem.options) {
        const resultOptions = nortelTargetInfoItem.options.map((option: any) => {
          const returnREsult = option;
          const options = option.values;
          const cCC2DN = options.find((item: any) => item.key === 'CCC2DN');
          if (cCC2DN) { cCC2DN.value = undefined; }
          const tRKGRP2 = options.find((item: any) => item.key === 'TRKGRP2');
          const tRKMEM2 = options.find((item: any) => item.key === 'TRKMEM2');
          if (tRKGRP2) { tRKGRP2.value = undefined; }
          if (tRKMEM2) { tRKMEM2.value = undefined; }
          const newOptions = options.filter((optionEmpty: any) => optionEmpty.value !== undefined);
          returnREsult.values = newOptions;
          return returnREsult;
        });
        if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${warrantCase.id}.target.${target.id}`, null);
        }
        nortelTargetInfoItem.options = resultOptions;
      }
    });
  }

  handleServicesChange = async (formTemplate: any, warrantCase: any, value: any) => {
    // Edit warrants: For legacy warrant support, there is no need to delete non-blank target
    // A message will appear to confirm on deleting targets.  At that time, call
    // this.props.changeTracker.addDeleted('case.{IDC}.target.{IDT}'.replace('{IDC}',
    //     this.state.warrantCase.id).replace('{IDT}', target.id));
    // when user confirms to delete target
    this.warrantConfig?.computeAllowedCfsAndServicesOnServiceChange(
      value === undefined ? [] : value,
    );
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    if (!this.warrantConfig?.getAllowedCfTypes().includes(cfType)) {
      warrantCase.cfId = '';
    }
    await this.updateFormTemplate(formTemplate, warrantCase);
  }

  // handleCfChange = async (formTemplate: any, value: any) => {
  //   const selectedCf = this.cfs.find((cf: any) => { return cf.id === value; });
  //   const cfTag = selectedCf ? selectedCf.tag : undefined;
  //   this.warrantConfig.computeAllowedCfsOnCfChange(cfTag);
  //   await this.updateFormTemplate(formTemplate);
  // }

  changeTargets = async (formTemplate: any, warrantCase: any) => {
    const services = warrantCase.services.slice();
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    await this.warrantConfig?.computeSelectedAfTypes(cfType);
    WarrantConfiguration.setDxOwner(warrantCase.dxOwner);
    this.warrantConfig?.computePossibleTargets(warrantCase.services);
    // Comment to be removed after Edit warrants are done.
    // Edit warrants:Adds new targets - Anna response: no need to
    // changeTracker.addAdd(target) because when targets are added to
    // the form due to service + cf change, the targets have no values.
    // They are not provisioned to the server.  They are only provisioned
    // to the server when they are changed by the user.
    const originalExistingTargets: any[] = cloneDeep(warrantCase.targets);
    if (this.areThereServiceChangeErrors(formTemplate, services)) return true;
    if (this.areThereCfchangeErrors(services, warrantCase.cfId)) return true;
    if (services && services.length === 1 && services[0] === 'unspecified') {
      warrantCase.targets = cloneDeep(this.unTouchedWarrantCase.targets);
    } else {
      const result = this.mergeTargets(this.warrantConfig?.getPossibleTargets(),
        originalExistingTargets);
      warrantCase.targets = result.newTargets;
      if (!this.areExistingTargetsCompatible(result.notCompatibleExistingTargets,
        formTemplate, warrantCase)) {
        return true;
      }
    }
    warrantCase.targets.forEach((target: any) => {
      target.formId = `${target.primaryType}_${warrantCase.id}${warrantCase.cfId ? warrantCase.cfId : ''}${target.id}`;
    });
    return false;
  }

  areThereServiceChangeErrors = (formTemplate: any, value: any) => {
    if (value && value.length > 0 && !this.isValidCfs(formTemplate)) {
      AppToaster.clear();
      AppToaster.show({
        icon: 'error',
        intent: 'warning',
        timeout: 0,
        message: 'No CF found for the selected service',
      });
      this.caseDetailsFieldUpdateHandler({ name: 'services', value: this.state.warrantCase.services });
      return true;
    }
    return false;
  }

  areThereCfchangeErrors = (services: any, value: string) => {
    if (value && services && services.length > 0 && !(services.length === 1 && services[0] === 'unspecified')
      && this.warrantConfig?.getPossibleTargets().length === 0) {
      AppToaster.clear();
      AppToaster.show({
        icon: 'error',
        intent: 'warning',
        timeout: 0,
        message: 'No supported target types found for the selected services and cf combination',
      });
      if (value !== this.state.warrantCase.cfId) {
        this.caseDetailsFieldUpdateHandler({ name: 'cfId', value: this.state.warrantCase.cfId });
      }
      return true;
    }
    return false;
  }

  updateFormTemplate = async (formTemplate: any, warrantCase: any) => {
    CasePanel.updateKey(formTemplate);
    this.updateCfOptions(warrantCase);
  }

  isValidCfs = (formTemplate: any) => {
    return formTemplate.general.fields.cfId.options.length > 1;
  }

  updateCfOptions(warrantCase: CaseModel): void {
    if (this.formTemplate == null) {
      return;
    }

    const { mode } = this.props;

    let options = warrantCase.cfOptions ?? [];
    if (mode === CONFIG_FORM_MODE.View) {
      // When executing an action in View mode while in the Cases tab,
      // the Warrant, Cases and Targets are loaded again and this will cause the
      // cfOptions to be lost.
      // This cfOptions is created in the generateCases by the generateCaseCfsOptions.
      // And the generateCases is called when the uses moves from Warrant to Cases tabs.
      // Because in View mode there is no need to show the entire list, so just show
      // the CF that is selected.
      // Code refactoring will be done in DX-3349.
      options = [{
        label: warrantCase.cfId.name,
        value: warrantCase.cfId.id,
      }];
    }

    (this.formTemplate.general as FormTemplateSection).fields.cfId.options = options;
  }

  updateInterface = (warrantCase: any) => {
    let hi3InterfacesValue = '';
    if (!warrantCase.hi3Data) {
      return;
    }
    const selectedHi3DataLength = warrantCase.hi3Data.length;

    switch (selectedHi3DataLength) {
      case 0:
        if (this.formTemplate != null) {
          (this.formTemplate.general as FormTemplateSection).fields.interface.hide = 'true';
        }
        break;

      case 1:
        const hi3InterfacesIP = warrantCase.hi3Data[0].name.replace('IP:', '').replace('PORT', '');
        const hi3InterfacesPort = warrantCase.hi3Data[0].options.filter((item: any) => item.key === 'state')[0].value;
        hi3InterfacesValue = `${hi3InterfacesIP} ${hi3InterfacesPort}`;
        if (this.formTemplate != null) {
          (this.formTemplate.general as FormTemplateSection).fields.interface.readOnly = true;
          (this.formTemplate.general as FormTemplateSection).fields.interface.hide = 'false';
        }
        break;

      default:
        hi3InterfacesValue = 'Multiple';
        if (this.formTemplate != null) {
          (this.formTemplate.general as FormTemplateSection).fields.interface.readOnly = true;
          (this.formTemplate.general as FormTemplateSection).fields.interface.hide = 'false';
        }
    }
    warrantCase.interface = hi3InterfacesValue;
  }

  /**
   * This method is used to store the hi2Data and hi3Data
   * in case the warrant is updated and hi2Data and hi3Data
   * is lost.
   *
   * When executing an action in View mode while in the Cases tab,
   * the Warrant, Cases and Targets are loaded again and this will cause the
   * hi2Data and hi3Data to be lost.
   *
   * This hi2Data and hi3Data is created in the getAllCasesFromDevicesModel by the getAllCaseDetails.
   * And the getAllCaseDetails is called when the uses moves from Warrant to Cases tabs.
   * Code refactoring will be done in DX-3349.
   *
   * @param warrantCase the Case data
   */
  updateHi2andHi3Data(warrantCase: CaseModel): void {
    this.hi2Data = warrantCase.hi2Data ?? this.hi2Data;
    this.hi3Data = warrantCase.hi3Data ?? this.hi3Data;

    if (warrantCase.hi2Data == null) {
      warrantCase.hi2Data = this.hi2Data;
    }

    if (warrantCase.hi3Data == null) {
      warrantCase.hi3Data = this.hi3Data;
    }
  }

  showHI23Interface = () => {
    this.setState({ isHi23InterfaceVisible: true });
  }

  handleHI23InterfaceClose = () => {
    this.setState({ isHi23InterfaceVisible: false });
  }

  handleHI23InterfaceSubmit = (hi3Interfaces: any, warrantCase:any = null) => {
    if (!warrantCase) {
      warrantCase = cloneDeep(this.state.warrantCase);
    }

    const caseInterfacesRefs = CasePanel.getCaseInterfacesRefs(warrantCase);

    warrantCase.hi3Interfaces = hi3Interfaces;
    const errors:string[] = [];
    if (CasePanel.checkHI3InterfaceValidationRule(errors, warrantCase, hi3Interfaces, caseInterfacesRefs) === false) {
      showErrors(errors);
      return;
    }
    this.setState({
      warrantCase,
      isHi23InterfaceVisible: false,
    });
    if (this.props.changeTracker) {
      this.props.changeTracker.addUpdated(`case.${warrantCase.id}`, null);
      this.props.changeTracker.addHI3Interface(warrantCase.id);
    }
  }

  handleDeleteCase = (): void => {
    const {
      warrantCase,
    } = this.state;

    let device = 'Device';
    if (warrantCase.device) {
      device = `${device} ${warrantCase.device}`;
    }

    const popUp = {
      title: 'Delete Case',
      message: (
        <Fragment>
          <p>
            The services associated with this case will be removed from {device}.
            <br />
            Are you sure you want to delete the case {warrantCase.liId}?
          </p>
          <p>
            <strong>NOTE:</strong> Deletion will be effective upon Save.
          </p>
        </Fragment>
      ),
      actionText: 'Yes, delete it',
      isOpen: true,
      handleClose: this.handlePopUpClose,
      handleSubmit: this.confirmDeleteCase,
    };
    this.setState({ popUp });

  }

  confirmDeleteCase = ():void => {
    const { onDeleteCase } = this.props;

    const { warrantCase } = this.state;

    if (onDeleteCase) {
      onDeleteCase(warrantCase.id);
    }
  }

  isGencccEnabled = (warrantCase: any) => {
    return (this.warrantConfig?.genccc && (warrantCase.type === 'CC' || warrantCase.type === 'ALL'));
  }

  caseDetailsFieldUpdateHandler = async (changedField: ChangedField) => {
    const {
      changeTracker,
      bSelectAllHI3Interfaces,
      mode,
    } = this.props;

    const {
      name, value, label, valueLabel,
    } = changedField;
    const prevDisplayValue = this.state.warrantCase[name];

    // Track case changes.  For target change, tracking case update is in the target code.
    if (changeTracker && name !== 'primaryValue' && name !== 'includeInWarrant') {
      changeTracker.addUpdated(`case.${this.state.warrantCase.id}.${name}`, valueLabel, label, prevDisplayValue);
    }

    const formTemplate = cloneDeep(this.state.formTemplate);
    const warrantCase = cloneDeep(this.state.warrantCase);
    warrantCase[name] = value;

    let { genccc } = this.state;
    switch (name) {
      case 'services': {
        const unspecifiedServiceIndex = value.findIndex((service: any) => { return service === 'unspecified'; });
        if (value.length > 1 && unspecifiedServiceIndex !== -1) {
          value.splice(unspecifiedServiceIndex, 1);
          warrantCase[name] = value;
        }
        await this.handleServicesChange(formTemplate, warrantCase, value);
        const areThereErrors = await this.changeTargets(formTemplate, warrantCase);
        if (areThereErrors) return;
        break;
      }
      case 'cfId': {
        genccc = this.isGencccEnabled(warrantCase);
        this.cfId = value;
        if (changeTracker) {
          changeTracker.addHI3Interface(warrantCase.id);
        }

        this.setState({
          isRetrievingInterfaces: true,
        });

        await this.getHI2Interfaces(value, warrantCase);
        await this.getHI3Interfaces(value, warrantCase);

        this.setState({
          isRetrievingInterfaces: false,
        });

        break;
      }
      case 'type': {
        genccc = this.isGencccEnabled(warrantCase);
        if (value !== prevDisplayValue) {
          const hi3Interfaces = CasePanel.createHI3Interfaces(
            warrantCase.type,
            warrantCase,
            bSelectAllHI3Interfaces,
            mode,
          );
          this.handleHI23InterfaceSubmit(hi3Interfaces, warrantCase);

          if (value !== 'CC' && value !== 'ALL') {
            CasePanel.clearTargetsCCCvaluesCCRType(warrantCase, changeTracker);
          }
        }
        break;
      }
      case 'templateUsed':
        warrantCase.options = getCaseOptionTemplateValue(this.props.templates, value);
        break;
      case 'includeInWarrant':
        const { hideIncludeCaseConfirmation } = this.props;

        if (value === false && changeTracker != null) {
          // In edit mode, the case was already added to ChangeTracker when moving to
          // the Cases tab, so it is required to mark as deleted to remove it from
          // ChangeTracker
          processCasesChanges(changeTracker, [warrantCase], [], {});

        } if (value === true && changeTracker != null) {
          // Add the case back to the ChangeTracker
          processCasesChanges(changeTracker, [], [warrantCase], {});
        }

        if (!hideIncludeCaseConfirmation && !value) {
          this.setState({
            includeCasePopUpIsOpen: true,
          });
        }

        break;
      default:
        break;
    }
    this.setState({
      formTemplate,
      warrantCase,
      genccc,
    });
  }

  static createHI3Interfaces(
    type: string,
    warrantCase: CaseModel,
    bSelectAllHI3Interfaces: boolean,
    mode: string,
  ): HI3Interface[] {
    const caseInterfacesRefs = CasePanel.getCaseInterfacesRefs(warrantCase);

    const hi3Interfaces = getDefaultHI3Interfaces(
      type,
      warrantCase.hi2Data ?? [],
      warrantCase.hi3Data ?? [],
      caseInterfacesRefs.casePHIR,
      bSelectAllHI3Interfaces,
    );

    CasePanel.setHI3InterfacesOperation(hi3Interfaces, warrantCase.hi3Interfaces ?? [], mode);

    return hi3Interfaces;
  }

  static setHI3InterfacesOperation(
    interfaces: HI3Interface[],
    originalInterfaces: HI3Interface[],
    mode: string,
  ): void {
    if (mode === CONFIG_FORM_MODE.Create) {
      interfaces.forEach((interfaceEntry) => {
        interfaceEntry.operation = 'CREATE';
      });

      return;
    }

    // Check which interfaces will be marked as CREATE, UPDATE or NO_OPERATION
    interfaces.forEach((interfaceEntry) => {
      const originalInterfaceEntry = originalInterfaces.find(({ id }) => id === interfaceEntry.id);
      if (originalInterfaceEntry == null || originalInterfaceEntry.operation === 'CREATE') {
        interfaceEntry.operation = 'CREATE';
      } else {
        interfaceEntry.operation = 'NO_OPERATION';
      }
    });

    // Check which interfaces will be marked as DELETE
    const interfacesToDelete =
      originalInterfaces.filter((originalInterfaceEntry) => {
        return !interfaces.find(({ id }) => id === originalInterfaceEntry.id) && originalInterfaceEntry.operation !== 'CREATE';
      })
        .map((originalInterfaceEntry) => ({
          ...originalInterfaceEntry,
          operation: 'DELETE',
        }));

    if (interfacesToDelete) {
      interfaces?.push(...(interfacesToDelete as HI3Interface[]));
    }
  }

  static clearTargetsCCCvaluesCCRType(warrantCase: CaseModel, changeTracker?: ChangeTracker | null): void {
    const { targets } = warrantCase;

    targets.forEach((target) => {
      if (!target.afs || !target.targetInfoHash) return;
      const allSelectedTargetInfoHash = target.targetInfoHash;
      const nortelTargetInfoHashItem = allSelectedTargetInfoHash
        .find((targetInfoHashItem) => targetInfoHashItem.afType === TARGET_INFO_TYPE.MTXCFCI);

      if (nortelTargetInfoHashItem && nortelTargetInfoHashItem.options) {
        nortelTargetInfoHashItem.options = [];

        if (changeTracker) {
          changeTracker.addUpdated(`case.${warrantCase.id}.target.${target.id}`, null);
        }
      }
    });
  }

  getHI2Interfaces = async (cfId: any, newWarrantCase: CaseModel): Promise<void> => {
    if (cfId && cfId !== '') {
      const { selectedDF } = this.props;
      const { warrantCase } = this.state;

      const values = await fetchCollectionFunctionHI2InterfaceList(selectedDF, cfId).payload;

      const hi2Data = values?.data?.data ?? [];

      if (!newWarrantCase) {
        newWarrantCase = cloneDeep(warrantCase);
      }
      newWarrantCase.hi2Data = hi2Data;
      this.setState({ warrantCase: newWarrantCase });

    } else {
      newWarrantCase.hi2Data = [];
      this.setState({ warrantCase: newWarrantCase });
    }
  }

  getHI3Interfaces = async (cfId: any, newWarrantCase: CaseModel): Promise<void> => {
    if (cfId && cfId !== '') {

      const { selectedDF } = this.props;

      const values = await fetchCollectionFunctionHI3InterfaceList(selectedDF, cfId).payload;

      const hi3Data = values?.data?.data ?? [];

      newWarrantCase.hi3Data = hi3Data;
      const type = this.caseRef.getValue('type');

      const {
        bSelectAllHI3Interfaces,
        mode,
      } = this.props;
      const hi3Interfaces = CasePanel.createHI3Interfaces(type, newWarrantCase, bSelectAllHI3Interfaces, mode);
      this.handleHI23InterfaceSubmit(hi3Interfaces, newWarrantCase);

    } else {
      newWarrantCase.hi3Data = [];
      this.handleHI23InterfaceSubmit([], newWarrantCase);
    }
  }

  areExistingTargetsCompatible = (originalExistingTargets: any[], formTemplate: any,
    warrantCase: any) => {
    if (originalExistingTargets.length !== 0) {
      this.handleExistingTargetsDelete(cloneDeep(formTemplate), cloneDeep(warrantCase));
      return false;
    }
    return true;
  }

  mergeTargets = (possibleTargets: any, existingTargets: any) => {
    const newTargets: any[] = [];
    const possibleTargetsMap: any = {};
    const notCompatibleExistingTargets: any[] = [];
    possibleTargets.forEach((pt: any) => {
      possibleTargetsMap[pt.primaryType] = {
        isUsed: false,
        target: cloneDeep(pt),
      };
    });
    existingTargets.forEach((et: any) => {
      const pt = possibleTargetsMap[et.primaryType];
      if (pt && pt.target) {
        if (!this.isTargetEmpty(et)) {
          newTargets.push(this.mergeTarget(pt, et));
          pt.isUsed = true;
        }
      } else if (!this.isTargetEmpty(et)) {
        notCompatibleExistingTargets.push(et);
      }
    });
    Object.keys(possibleTargetsMap).forEach((key: any) => {
      if (possibleTargetsMap[key].isUsed === false) {
        newTargets.push(possibleTargetsMap[key].target);
      }
    });
    return {
      newTargets,
      notCompatibleExistingTargets,
    };
  }

  mergeTarget = (pt: any, existingTarget: any) => {
    const newTarget = cloneDeep(existingTarget);
    if (!pt.isUsed) {
      newTarget.isRequired = pt.target.isRequired;
    }
    return newTarget;
  }

  trackDeletedTargets = (caseId: string, oldTargetList: Array<any>,
    newTargetList: Array<any>) => {
    if (!this.props.changeTracker) return;
    if (!oldTargetList || !newTargetList) return;

    const newTargetIds: Array<string> = [];
    newTargetList.forEach((target: any) => {
      newTargetIds.push(target.id);
    });
    oldTargetList.forEach((target: any) => {
      if (!this.isTargetEmpty(target)) {
        this.props.changeTracker?.addDeleted(`case.${caseId}.target.${target.id}`, target.primaryType);
        this.props.changeTracker?.addUpdated(`case.${caseId}`, null);
      }
    });
  }

  handleExistingTargetsDelete = (formTemplate: any, warrantCase: any) => {
    let message = 'All target values will be lost. Click \'Ok\' to continue';
    let revertBackField = '';
    if ((warrantCase.services && warrantCase.services.length === 0)) {
      revertBackField = 'services';
    } else if (!warrantCase.cfId) {
      revertBackField = 'cfId';
    } else {
      message = 'Some or all target values may be lost due to current selection of'
        + ' services and CF combination. Click \'Ok\' to continue.';
      revertBackField = 'cfId';
    }
    const popUp = {
      message,
      actionText: 'Ok',
      isOpen: true,
      handleClose: () => {
        const newPopUp = { ...this.state.popUp };
        newPopUp.isOpen = false;
        this.setState({
          popUp: newPopUp,
        });
        this.caseDetailsFieldUpdateHandler({
          name: revertBackField,
          value: this.state.warrantCase[revertBackField],
        });
      },
      handleSubmit: () => {
        const newPopUp = { ...this.state.popUp };
        newPopUp.isOpen = false;
        this.trackDeletedTargets(warrantCase.id, this.state.warrantCase.targets,
          warrantCase.targets);
        this.setState({
          formTemplate,
          warrantCase,
          popUp: newPopUp,
        });
        return DONT_CLOSE_ON_SUBMIT;
      },
    };
    this.setState({ popUp });
  }

  handlePopUpClose = () => {
    const popUp = { ...this.state.popUp };
    popUp.isOpen = false;
    this.setState({ popUp });
  }

  isTargetEmpty = (target: any) => {
    return (!target.primaryValue && !target.secondaryValue
      && !(target.options && target.options.length > 0));
  }

  handleTargetPanelSubmit = (targets: any) => {
    const warrantCase = cloneDeep(this.state.warrantCase);
    const changedTargets = this.getUpdatedTargetsList(warrantCase.targets, targets);
    warrantCase.targets = targets;
    this.setState({
      warrantCase,
    });
  }

  sortArray = (first: any, next: any) => {
    if (first.id < next.id) { return -1; }
    if (first.id > next.id) { return 1; }
    return 0;
  }

  static getCaseInterfacesRefs(warrantCase: any): CaseInterfacesRefs {
    const selectedCf = warrantCase.cfs?.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    const casePHIR = warrantCase?.options?.generatePacketDataHeaderReport
      && cfType === 'CF-3GPP-33108';
    return {
      dataOnly: INTERCEPT_TYPE_CD === warrantCase?.type,
      casePHIR,
    };
  }

  getUpdatedTargetsList = (oldTargets: any, newTargets: any) => {
    // The targets in both old and new are same... the afs change
    const result = oldTargets.map((ot: any) => {
      const { id } = ot;
      const nt = newTargets.filter((item: any) => item.id === ot.id);
      if (nt.length === 0) return undefined;
      const oldAfs = cloneDeep(ot.afs);
      const newAfs = cloneDeep(nt[0].afs);

      // are afs equal
      if (oldAfs.afIds.length !== newAfs.afIds.length) return id;
      const oAfIds = Object.assign([], oldAfs.afIds
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfIds = Object.assign([], newAfs.afIds
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfIds.length; i += 1) {
        if (oAfIds[0].id !== nAfIds[0].id) {
          return id;
        }
      }

      // // are afg equal
      if (oldAfs.afGroups.length !== newAfs.afGroups.length) return id;
      const oAfgIds = Object.assign([], oldAfs.afGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfgIds = Object.assign([], newAfs.afGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfgIds.length; i += 1) {
        if (oAfgIds[0].id !== nAfgIds[0].id) {
          return id;
        }
      }

      // // are aft equal
      if (oldAfs.afAutowireTypes.length !== newAfs.afAutowireTypes.length) return id;
      const oAftIds = Object.assign([], oldAfs.afAutowireTypes
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAftIds = Object.assign([], newAfs.afAutowireTypes
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAftIds.length; i += 1) {
        if (oAftIds[0].id !== nAftIds[0].id) {
          return id;
        }
      }
      // // are awg equal
      if (oldAfs.afAutowireGroups.length !== newAfs.afAutowireGroups.length) return id;
      const oAfwgIds = Object.assign([], oldAfs.afAutowireGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfwgIds = Object.assign([], newAfs.afAutowireGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfwgIds.length; i += 1) {
        if (oAfwgIds[0].id !== nAfwgIds[0].id) {
          return id;
        }
      }
      return undefined;
    });
    return result.filter((r: any) => { return r !== undefined; });
  }

  submitSubStep = async () => {
    if (!this.props.cases || this.props.cases.length === 0) return true;

    const newWarrantCase = cloneDeep(this.state.warrantCase);

    // TODO: get only selected CFID if there are multiple CFId's
    const caseCfId = newWarrantCase.cfOptions
      .filter((cf: any) => cf.value === newWarrantCase.cfId)
      .map((cfDet: any) => ({ id: cfDet.value, name: cfDet.label }));

    const [caseCfIdDetails] = caseCfId;

    newWarrantCase.cfId = caseCfIdDetails;

    this.props.updateCaseState(newWarrantCase);
  }

  async submit(shouldShowErrors = true): Promise<boolean> {
    AppToaster.clear();

    const {
      cases,
      warrant,
      warrantConfigInstance,
    } = this.props;

    if (warrant == null || warrantConfigInstance == null) {
      return false;
    }

    if (cases == null || cases.length === 0) {
      return true;
    }

    const {
      warrantCase,
      formTemplate,
    } = this.state;

    this.submitSubStep();

    const errors: string[] = [];
    let isValid = true;

    this.isCurrentCaseValid(true, warrantCase, errors);

    const filteredCases = cases.filter(({ id }) => id !== warrantCase.id);
    [isValid] = CasePanel.areAllValid(warrant, filteredCases, formTemplate, warrantConfigInstance, errors,
      this.props.targetDefaultAFSelection);

    if (!isValid && shouldShowErrors) {
      showErrors(errors);
    }

    return isValid;
  }

  // verify current warrantcase
  isValid(errors: string[]): boolean {
    const {
      cases,
      warrant,
    } = this.props;

    if (warrant == null || !cases || cases.length === 0) {
      return true;
    }

    const {
      warrantCase,
      formTemplate,
    } = this.state;

    const caseInterfacesRefs = CasePanel.getCaseInterfacesRefs(warrantCase);

    const newWarrantCase = cloneDeep(warrantCase);
    const caseCfId = (newWarrantCase.cfOptions || [])
      .filter((cf: any) => cf.value === newWarrantCase.cfId)
      .map((cfDet: any) => ({ id: cfDet.value, name: cfDet.label }));
    const [caseCfIdDetails] = caseCfId;
    newWarrantCase.cfId = caseCfIdDetails;

    if (!errors) errors = [];
    CasePanel.validateInterfieldRules(errors, warrant, newWarrantCase, caseInterfacesRefs, formTemplate);
    const isValid = this.isCurrentCaseValid(true, newWarrantCase, errors);
    return !((errors.length !== 0 || !isValid));
  }

  static areAllValid(
    warrant: Warrant,
    cases: CaseModel[],
    formTemplate: FormTemplate,
    warrantConfigInstance: WarrantConfiguration,
    errors: string[],
    targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  ): [boolean, number | null] {
    let caseIndex: number | null = null;

    const filteredCases = cases.filter(({ hideFromView }) => !hideFromView);

    const isEveryCaseValid = filteredCases.every((caseEntry, index) => {
      const caseInterfacesRefs = CasePanel.getCaseInterfacesRefs(caseEntry);
      CasePanel.validateInterfieldRules(errors, warrant, caseEntry, caseInterfacesRefs, formTemplate);
      const isCaseValid = CasePanel.isCaseValid(warrantConfigInstance,
        caseEntry, formTemplate, errors, targetDefaultAFSelection);
      caseIndex = index;
      return isCaseValid && errors.length === 0;
    });

    return [isEveryCaseValid, caseIndex];
  }

  static validateInterfieldRules(
    errors: string[],
    warrantData: Warrant,
    warrantCase: CaseModel,
    caseInterfacesRefs: CaseInterfacesRefs,
    formTemplate: FormTemplate,
  ): void {
    if (CasePanel.areSomeDateFieldEditable(formTemplate)) {
      // Only validate date fields if there are at least one that is editable
      CasePanel.checkTimeValidationRule(warrantCase, warrantData, errors);

      if (checkGivenTimeWithCurrentTime(warrantCase.startDateTime, warrantCase.timeZone)) {
        errors.push(`Case ${warrantCase.caseIndex}: Start Time should be after the current time`);
      }
    }

    CasePanel.checkHI3InterfaceValidationRule(errors, warrantCase, warrantCase.hi3Interfaces ?? [], caseInterfacesRefs);
  }

  static areSomeDateFieldEditable(formTemplate: FormTemplate): boolean {
    const formFields = (formTemplate.general as FormTemplateSection).fields;

    const dateTimeFields = Object.entries(formFields).filter(([_key, field]) => {
      return field.type === 'datetime';
    });

    return dateTimeFields.some(([_key, field]) => {
      return field.hide !== 'true' && field.readOnly !== true && field.disabled !== true;
    });
  }

  static checkHI3InterfaceValidationRule(
    errors: string[],
    warrantCase: CaseModel,
    selectedHi3Interfaces: HI3Interface[],
    caseInterfacesRefs: CaseInterfacesRefs,
  ): boolean {
    const validInterfaces = selectedHi3Interfaces.filter((item) => item.operation !== 'DELETE');

    // if type is 'Data and Content' or 'Content' and CF has HI3, user must select an HI3 Interfaces
    if (warrantCase.type === 'ALL' || warrantCase.type === 'CC') {
      if (validInterfaces.length === 0 && warrantCase.hi3Data && warrantCase.hi3Data.length > 0) {
        errors.push(`Case -${warrantCase.liId} : Case Type has content. Please select at least one HI3 Interface.`);
        return false;
      }

    // if type is 'Call Data', user should not select any HI3 Interfaces
    } else if (caseInterfacesRefs.casePHIR === false && (validInterfaces.length > 0) === true) {
      errors.push(`Case -${warrantCase.liId} : Case Type has no content. Please remove all selected HI3 Interface.`);
      return false;
    }

    return true;
  }

  static checkTimeValidationRule(warrantCase: CaseModel, warrant: Warrant, errors: string[]): void {
    const caseStartDateTimeInDate = (!warrantCase.startDateTime || warrantCase.startDateTime === '') ? new Date() : new Date(warrantCase.startDateTime);
    const caseStopDateTimeInDate = new Date(warrantCase.stopDateTime);
    const warrantStartDateTimeInDate = (!warrant.startDateTime || warrant.startDateTime === '') ? new Date() : new Date(warrant.startDateTime);
    const warrantStopDateTimeInDate = new Date(warrant.stopDateTime);

    if (caseStartDateTimeInDate.getTime() > caseStopDateTimeInDate.getTime()) {
      errors.push(`Case -${warrantCase.liId} : Stop Time should be same or after the Start Time`);
      return;
    }

    if (caseStartDateTimeInDate.getTime() < warrantStartDateTimeInDate.getTime() ||
        caseStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
      errors.push(`Case -${warrantCase.liId} : Start Time should be within Warrant's Start Time and Stop Time`);
      return;
    }

    if (caseStopDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime() ||
      caseStopDateTimeInDate.getTime() < warrantStartDateTimeInDate.getTime()) {
      errors.push(`Case -${warrantCase.liId} : Stop Time should be within Warrant's Start Time and Stop Time`);
      return;
    }

    if (checkGivenTimeWithCurrentTime(warrantCase.stopDateTime, warrantCase.timeZone)) {
      errors.push(`Case -${warrantCase.liId} : Stop Time should be after the current time`);
    }
  }

  isAccessFunctionAndTargetAdditionalInfoValid(): boolean {
    const {
      warrantCase,
    } = this.state;

    const { targetDefaultAFSelection } = this.props;
    const { targets, allTargetAfs } = warrantCase;
    const isSurOptCombined = warrantCase.options?.circuitSwitchedVoiceCombined;
    const errors: string[] = [];

    handleAFandTargetCCCValidation(
      targets,
      allTargetAfs,
      errorKey,
      warrantCase,
      errors,
      isSurOptCombined,
      targetDefaultAFSelection,
    );

    return errors.length === 0;
  }

  handleChangeDontShowIncludeCaseInWarrantDialog = (event:React.FormEvent<HTMLInputElement>) => {
    this.setState({
      hideIncludeCaseConfirmationChecked: event.currentTarget.checked,
    });
  }

  handleCancelIncludeCaseInWarrantPopUp = (): void => {
    const { changeTracker } = this.props;
    const { warrantCase, formTemplate } = this.state;

    warrantCase.includeInWarrant = true;
    formTemplate.key = `${Date.now()}`;

    if (changeTracker != null) {
      // Add the case back to the ChangeTracker
      processCasesChanges(changeTracker, [], [warrantCase], {});
    }

    this.setState({
      warrantCase,
      includeCasePopUpIsOpen: false,
      formTemplate,
    });
  }

  handleSubmitIncludeCaseInWarrantPopUp = ():string => {
    const { hideIncludeCaseConfirmationChecked } = this.state;
    const { updateShowRemoveCaseInWarrantConfirmation } = this.props;

    updateShowRemoveCaseInWarrantConfirmation(hideIncludeCaseConfirmationChecked);

    this.setState({
      includeCasePopUpIsOpen: false,
    });

    return DONT_CLOSE_ON_SUBMIT;
  }

  formContainerClassesName = ():string => {
    const { mode, cases } = this.props;

    if (mode === CONFIG_FORM_MODE.View) {
      return '';
    }

    if (cases.length > 1) {
      return 'form form-translated formEnabled';
    }

    return 'form formEnabled';

  }

  renderIncludeCaseInWarrantDialog = (): JSX.Element | null => {
    const { hideIncludeCaseConfirmationChecked } = this.state;

    return (
      <div className="include-case-in-warrant-popup">
        <p className="include-case-in-warrant-popup-message">
          This case <strong>will not be included</strong>{' '}
          in the Warrant and the related services will be removed from the Device upon Save.  Continue?
        </p>
        <div>
          <Checkbox
            checked={hideIncludeCaseConfirmationChecked}
            onChange={this.handleChangeDontShowIncludeCaseInWarrantDialog}
            label="Do not show this again"
          />
        </div>
      </div>
    );
  }

  renderCallout = ():(JSX.Element | null) => {
    const { mode } = this.props;

    if (mode === CONFIG_FORM_MODE.View || this.isValid([])) {
      return null;
    }

    let text = null;
    if (!this.isAccessFunctionAndTargetAdditionalInfoValid()) {
      text = 'Please enter additional target information for selected AFs in Provisioning section below.';
    }

    return (
      <div className="callout">
        <Callout
          title="Complete all required case information before proceeding to save"
          icon="error"
          intent="danger"
        >
          {text}
        </Callout>
      </div>
    );
  }

  renderInstructionalText(): JSX.Element | null {
    const { mode } = this.props;

    if (mode === CONFIG_FORM_MODE.View) {
      return null;
    }

    if (mode === CONFIG_FORM_MODE.Edit) {
      return (
        <p className="instructional-text">
          Choose which case you want to edit by navigating on the cases on the left.{' '}
          Deleting a case will remove the service from the respective device.{' '}
          <br />
          <strong className="instructional-text-highlight">important note</strong>: All changes will be effective upon saving.
        </p>
      );
    }

    return (
      <p className="instructional-text">
        These cases were auto-generated based on the services you chose in
        previous step. Review each case Target and Access Functions provisioning
        by navigating on the cases on the left.
      </p>
    );
  }

  render(): JSX.Element {
    const cfDetails = this.cfs.find((cf: any) => (cf.id === this.state.warrantCase.cfId));

    const casesToShow = this.props.cases && this.props.cases.length !== 0;

    if (!casesToShow) {
      return (
        <div className="wizardCaseContainer">
          <p className="instructional-text">No cases because no devices were entered</p>
        </div>
      );
    }

    const {
      mode,
    } = this.props;

    const {
      warrantCase,
      isRetrievingInterfaces,
    } = this.state;

    const caseInterfacesRefs = CasePanel.getCaseInterfacesRefs(warrantCase);

    return (
      <div className="wizardCaseContainer">
        {this.renderCallout()}
        {this.renderInstructionalText()}
        <div className="container">
          <div className="section-header">Case Information</div>
          <div className={this.formContainerClassesName()}>
            <DynamicForm
              ref={(ref: any) => { this.caseRef = ref; }}
              name={this.state.formTemplate.key}
              fields={CasePanel.getFormTemplateFields(this.state.formTemplate.general.fields, mode)}
              layout={this.state.formTemplate.general.metadata.layout.rows}
              defaults={this.state.warrantCase}
              fieldUpdateHandler={this.caseDetailsFieldUpdateHandler}

            />
            {this.state.genccc && (
              <GencccPanel
                ref={(ref: any) => { this.genCCCSummaryRef = ref; }}
                defaults={this.state.warrantCase.genccc}
                fieldUpdateHandler={this.caseDetailsFieldUpdateHandler}
                caseId={this.state.warrantCase.id}
                mode={mode}
              />
            )}

            <TargetPanel
              currentStateMode={this.state.currentStateMode}
              ref={(ref: any) => { this.targetPanelRef = ref; }}
              handleSubmit={this.handleTargetPanelSubmit}
              targets={this.state.warrantCase.targets}
              targetFields={this.warrantConfig?.getTargetFields(this.state.warrantCase.targets)}
              caseDetailsFieldUpdateHandler={this.caseDetailsFieldUpdateHandler}
              changeTracker={this.props.changeTracker}
              warrantCase={this.state.warrantCase}
              drvirtual={this.warrantConfig?.getDrVirtual()}
              cfType={cfDetails ? cfDetails.type : ''}
              targetAfDetails={this.state.warrantCase.allTargetAfs}
              selectedDF={this.props.selectedDF}
              hideEmptyTargets
            />
          </div>
          <CaseOptions
            isOpen={this.state.isCaseOptionsVisible}
            currentStateMode={this.state.currentStateMode}
            handleClose={this.handleCaseOptionsClose}
            inputCaseOptions={this.state.warrantCase.options}
            handleSubmit={this.handleCaseOptionsSubmit}
            caseId={this.state.warrantCase.id}
            templateUsed={this.state.warrantCase.templateUsed}
            templates={this.props.templates}
          />
          <HI2AndHI3Options
            isOpen={this.state.isHi23InterfaceVisible}
            currentStateMode={this.state.currentStateMode as CONFIG_FORM_MODE}
            handleClose={this.handleHI23InterfaceClose}
            cfId={this.state.warrantCase.cfId}
            hi3Interfaces={this.state.warrantCase.hi3Interfaces}
            id={this.state.warrantCase.id}
            handeSubmit={this.handleHI23InterfaceSubmit}
            hi2Data={this.state.warrantCase.hi2Data}
            hi3Data={this.state.warrantCase.hi3Data}
            caseInterfacesRefs={caseInterfacesRefs}
          />
          <ModalDialog
            title={this.state.popUp.title}
            displayMessage={this.state.popUp.message}
            isOpen={this.state.popUp.isOpen}
            onSubmit={this.state.popUp.handleSubmit}
            onClose={this.state.popUp.handleClose}
            actionText={this.state.popUp.actionText}
          />
          <ModalDialog
            title="Warning"
            actionText="Yes, Continue"
            customComponent={this.renderIncludeCaseInWarrantDialog}
            isOpen={this.state.includeCasePopUpIsOpen}
            onSubmit={this.handleSubmitIncludeCaseInWarrantPopUp}
            onClose={this.handleCancelIncludeCaseInWarrantPopUp}
          />
          <RetrievingDataProgress isOpen={isRetrievingInterfaces} message="Retrieving Interfaces..." />
        </div>
      </div>
    );
  }
}

export default connector(CasePanel);
