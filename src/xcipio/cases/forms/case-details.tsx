import React, { Fragment } from 'react';
import { Button } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import isEqual from 'lodash.isequal';
import { CaseInterfacesRefs, CaseModel, INTERCEPT_TYPE_CD } from 'data/case/case.types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { getDefaultHI3Interfaces } from 'xcipio/devices/forms/devices-to-cases';
import DynamicForm, { ChangedField } from '../../../shared/form';
import TargetsSummary from '../../targets/forms';
import CaseOptions from '../options/case-options';
import HI2AndHI3Options from '../../cfs/hi2-hi3-interface';
import AdvancedProvisioning from '../advancedProvisioning';
import { AppToaster, showErrors } from '../../../shared/toaster';
import ModalDialog, { DONT_CLOSE_ON_SUBMIT } from '../../../shared/modal/dialog';
import { fetchCollectionFunctionHI2InterfaceList, fetchCollectionFunctionHI3InterfaceList } from '../../../data/collectionFunction/collectionFunction.actions';
import { checkTimeValidationInComputerTimezone } from '../../../shared/constants/checkTimeValidation';
import GencccPanel from '../genccc';
import { TARGET_INFO_TYPE } from '../targetInfo/target-info-defs';
import { updateCaseBasedOnDerivedTargets } from './updateCaseBasedOnDerivedTargets';

class CaseDetails extends React.Component<any, any> {
  warrantConfig: any = undefined;

  cfs: any = [];

  caseRef: any;

  TargetsSummaryRef: any;

  genCCCSummaryRef: any;

  unTouchedWarrantCase: any = {}

  constructor(props: any) {
    super(props);
    const popUp = {
      message: '',
      actionText: '',
      isOpen: false,
      handleClose: null,
      handleSubmit: null,
    };

    const warrantCase = cloneDeep(props.warrantCase);
    this.modifyCfId(warrantCase);
    const caseInterfacesRefs = this.getCaseInterfacesRefs(warrantCase);
    this.state = {
      formTemplate: this.initializeWarrantCase(cloneDeep(props.formTemplate), props.warrantCase),
      warrantCase,
      isCaseOptionsVisible: false,
      isHi23InterfaceVisible: false,
      currentStateMode: props.mode,
      shouldTargetsUpdate: false,
      isAdvancedProvisioningVisible: false,
      genccc: false,
      popUp,
      hi2Data: [],
      hi3Data: [],
      caseInterfacesRefs,
    };
    this.warrantConfig = this.props.config;
  }

  initializeWarrantCase(formTemplate: any, warrantCase: any) {
    if (this.props.changeTracker && !this.props.changeTracker.isNewCase(warrantCase.id)) {
      // Disable Services and CF selections for existing cases
      formTemplate.general.fields.services.disabled = true;
      // Dates were already converted to computer timezone when loaded
      if (checkTimeValidationInComputerTimezone(warrantCase.startDateTime, warrantCase.stopDateTime)) {
        formTemplate.general.fields.startDateTime.disabled = true;
      }
      // Disable Case Name of an active warrant; TODO: Refactor-DX-2242
      if (this.props.mode === 'edit') {
        formTemplate.general.fields.liId.disabled = true;
      }
    }
    return formTemplate;
  }

   // eslint-disable-next-line camelcase
   UNSAFE_componentWillReceiveProps = async (nextProps: any) => {
     if (!nextProps.shouldCaseDetailsUpdate) return null;
     const formTemplate = cloneDeep(nextProps.formTemplate);
     this.initializeWarrantCase(formTemplate, nextProps.warrantCase);
     const warrantCase = cloneDeep(nextProps.warrantCase);
     this.unTouchedWarrantCase = cloneDeep(nextProps.warrantCase);
     this.modifyCfId(warrantCase);
     await this.handleServicesChange(formTemplate, warrantCase, warrantCase.services);
     await this.handleCfChange(formTemplate, nextProps.warrantCase.cfId.id, warrantCase.services);
     if (this.state.hi2Data.length === 0 || this.state.warrantCase.cfId !== nextProps.warrantCase.cfId.id) {
       this.updateHI2Interfaces(nextProps.warrantCase.cfId.id, warrantCase, true);
     }
     if (this.state.hi3Data.length === 0 || this.state.warrantCase.cfId !== nextProps.warrantCase.cfId.id) {
       this.updateHI3Interfaces(nextProps.warrantCase.cfId.id, warrantCase, true);
     }
     warrantCase.cfId = nextProps.warrantCase.cfId.id;
     const areThereErrors = await this.changeTargets(formTemplate, warrantCase, true);
     if (!areThereErrors) {
       CaseDetails.updateKey(formTemplate);
       const genccc = this.isGencccEnabled(warrantCase);
       this.setState({
         formTemplate,
         warrantCase,
         genccc,
         shouldTargetsUpdate: true,
       });
     }
   }

  componentDidUpdate = () => {
    if (this.state.warrantCase.isValid !== true) {
      this.isCaseValid(true);
    }
  }

  isCaseValid = (forceTouched = true) => {
    let isValid = true;
    if (this.caseRef) {
      isValid = this.caseRef.isValid(true, true);
    }
    if (this.TargetsSummaryRef) {
      const areTargetsValid = this.TargetsSummaryRef.areTargetsValid(forceTouched);
      isValid = isValid && areTargetsValid;
    }
    if (this.genCCCSummaryRef) {
      const isGenCCCValid = this.genCCCSummaryRef.isValid(forceTouched);
      isValid = isValid && isGenCCCValid;
    }
    return isValid;
  }

  static updateKey = (formTemplate: any) => {
    formTemplate.key = `Key_${new Date().getTime()}`;
  }

  modifyCfId = (defaults: any) => {
    if (defaults && defaults.cfId) {
      defaults.cfId = defaults.cfId.id;
      defaults?.hi3Interfaces?.forEach((item: any) => {
        if (!item.operation) {
          item.operation = 'NO_OPERATION';
        }
      });
    }
  }

  showCaseOptions = () => {
    this.setState({ isCaseOptionsVisible: true });
  }

  handleCaseOptionsClose = () => {
    this.setState({ isCaseOptionsVisible: false });
  }

  setHI3Interfaces(type: string, warrantCase: CaseModel): void {
    const {
      bSelectAllHI3Interfaces,
    } = this.props;

    const {
      hi2Data,
      hi3Data,
    } = this.state;

    const caseInterfacesRefs = this.getCaseInterfacesRefs(warrantCase);
    this.setState({ caseInterfacesRefs });

    const hi3Interfaces = getDefaultHI3Interfaces(
      type,
      hi2Data,
      hi3Data,
      caseInterfacesRefs.casePHIR,
      bSelectAllHI3Interfaces,
    );
    this.handleHI23InterfaceSubmit(hi3Interfaces, warrantCase);
  }

  handleCaseOptionsSubmit = (options: any, templateId: any) => {
    const warrantCase = { ...this.state.warrantCase };
    const prevCaseOptionPHIR = warrantCase.options.generatePacketDataHeaderReport;
    warrantCase.options = options;
    warrantCase.templateUsed = templateId;
    this.setState({
      warrantCase,
      isCaseOptionsVisible: false,
    });
    const newCaseOptionPHIR = warrantCase.options.generatePacketDataHeaderReport;
    if (warrantCase.type === INTERCEPT_TYPE_CD && newCaseOptionPHIR !== prevCaseOptionPHIR) {
      this.setHI3Interfaces(warrantCase.type, warrantCase);
    }
    if (options.circuitSwitchedVoiceCombined) {
      this.clearTargetsCCCvaluesCCRTypeSecondary(warrantCase);
      this.props.updateGridOnCaseDetailFieldChange('targets', warrantCase.targets);
    }
    this.props.updateGridOnCaseDetailFieldChange('options', options);
    this.props.updateGridOnCaseDetailFieldChange('templateUsed', templateId);
    // when case options updated, fire changeTracker.addUpdate('case.caseID)
    if (this.props.changeTracker) {
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}`);
    }
  }

  clearTargetsCCCvaluesCCRTypeSecondary = (warrantCase: any) => {
    const { targets } = warrantCase;
    targets.forEach((target: any) => {
      if (!target.afs || !target.targetInfoHash) return;
      const allSelectedTargetInfoHash = target.targetInfoHash;
      const nortelTargetInfoItem = allSelectedTargetInfoHash
        .find((targetInfoHashItem: any) => targetInfoHashItem.targetInfoType === TARGET_INFO_TYPE.MTXCFCI);
      if (nortelTargetInfoItem && nortelTargetInfoItem.options) {
        const resultOptions = nortelTargetInfoItem.options.map((option: any) => {
          const returnREsult = option;
          const options = option.values;
          const cCC2DN = options.find((item: any) => item.key === 'CCC2DN');
          if (cCC2DN) { cCC2DN.value = undefined; }
          const tRKGRP2 = options.find((item: any) => item.key === 'TRKGRP2');
          const tRKMEM2 = options.find((item: any) => item.key === 'TRKMEM2');
          if (tRKGRP2) { tRKGRP2.value = undefined; }
          if (tRKMEM2) { tRKMEM2.value = undefined; }
          const newOptions = options.filter((optionEmpty: any) => optionEmpty.value !== undefined);
          returnREsult.values = newOptions;
          return returnREsult;
        });
        if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${target.id}`);
        }
        nortelTargetInfoItem.options = resultOptions;
      }
    });
  }

  handleServicesChange = async (formTemplate: any, warrantCase: any, value: any) => {
    // Edit warrants: For legacy warrant support, there is no need to delete non-blank target
    // A message will appear to confirm on deleting targets.  At that time, call
    // this.props.changeTracker.addDeleted('case.{IDC}.target.{IDT}'.replace('{IDC}',
    //     this.state.warrantCase.id).replace('{IDT}', target.id));
    // when user confirms to delete target
    this.warrantConfig.computeAllowedCfsAndServicesOnServiceChange(
      value === undefined ? [] : value,
    );
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    if (!this.warrantConfig.getAllowedCfTypes().includes(cfType)) {
      warrantCase.cfId = '';
    }
    await this.updateFormTemplate(formTemplate, warrantCase.services, warrantCase.cfId);
  }

  handleCfChange = async (formTemplate: any, value: any, services: any) => {
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === value; });
    const cfTag = selectedCf ? selectedCf.tag : undefined;
    this.warrantConfig.computeAllowedCfsOnCfChange(cfTag);
    await this.updateFormTemplate(formTemplate, services, value);
    const caseInterfacesRefs = this.getCaseInterfacesRefs(this.state.warrantCase);
    this.setState({ caseInterfacesRefs });
  }

  changeTargets = async (formTemplate: any, warrantCase: any, isNotServiceOrCfChange?: boolean) => {
    const services = warrantCase.services.slice();
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    await this.warrantConfig.computeSelectedAfTypes(cfType);
    WarrantConfiguration.setDxOwner(warrantCase.dxOwner);
    this.warrantConfig.computePossibleTargets(warrantCase.services);
    updateCaseBasedOnDerivedTargets(warrantCase, this.warrantConfig);
    // Comment to be removed after Edit warrants are done.
    // Edit warrants:Adds new targets - Anna response: no need to
    // changeTracker.addAdd(target) because when targets are added to
    // the form due to service + cf change, the targets have no values.
    // They are not provisioned to the server.  They are only provisioned
    // to the server when they are changed by the user.
    const originalExistingTargets: any[] = cloneDeep(warrantCase.targets);
    if (this.areThereServiceChangeErrors(formTemplate, services)) return true;
    if (this.areThereCfchangeErrors(services, warrantCase.cfId)) return true;
    if (services && services.length === 1 && services[0] === 'unspecified') {
      const untouchedTargets = cloneDeep(this.unTouchedWarrantCase.targets);
      warrantCase.targets = cloneDeep(this.unTouchedWarrantCase.targets);
      await this.warrantConfig?.computeAfsBasedOnCfAndTargets(this.unTouchedWarrantCase.cfId, untouchedTargets);
    } else {
      const result = this.mergeTargets(this.warrantConfig.getPossibleTargets(),
        originalExistingTargets, isNotServiceOrCfChange);
      warrantCase.targets = result.newTargets;
      if (!this.areExistingTargetsCompatible(result.notCompatibleExistingTargets,
        formTemplate, warrantCase)) {
        return true;
      }
    }
    return false;
  }

  areThereServiceChangeErrors = (formTemplate: any, value: any) => {
    // Do not validate CFs if the service include unspecified, since the CF cannot be determined
    if (value && value.length > 0 && !this.isValidCfs(formTemplate) && !value.includes('unspecified')) {
      AppToaster.clear();
      AppToaster.show({
        icon: 'error',
        intent: 'warning',
        timeout: 0,
        message: 'No CF found for the selected service',
      });
      if (!isEqual(this.state.warrantCase.services, value)) {
        this.caseDetailsFieldUpdateHandler({ name: 'services', value: this.state.warrantCase.services });
      }
      return true;
    }
    return false;
  }

  areThereCfchangeErrors = (services: any, value: string) => {
    if (value && services && services.length > 0 && !(services.length === 1 && services[0] === 'unspecified')
      && this.warrantConfig.getPossibleTargets().length === 0) {
      AppToaster.clear();
      AppToaster.show({
        icon: 'error',
        intent: 'warning',
        timeout: 0,
        message: 'No supported target types found for the selected services and cf combination',
      });
      if (value !== this.state.warrantCase.cfId) {
        this.caseDetailsFieldUpdateHandler({ name: 'cfId', value: this.state.warrantCase.cfId });
      }
      return true;
    }
    return false;
  }

  updateFormTemplate = async (formTemplate: any, services: any, cfId: string) => {
    CaseDetails.updateKey(formTemplate);
    this.updateServiceOptions(formTemplate);
    await this.updateCfOptions(formTemplate, services, cfId);
  }

  isValidCfs = (formTemplate: any) => {
    return (formTemplate.general.fields.cfId.options.length === 1
      && formTemplate.general.fields.cfId.options[0].value !== '') || formTemplate.general.fields.cfId.options.length > 1;
  }

  updateServiceOptions = (formTemplate: any) => {
    formTemplate.general.fields.services.options = this.warrantConfig.getServiceDropDownOptions();
  }

  checkHI3InterfaceValidationRule = (errors: any[], warrantCase: any,
    selectedHi3Interfaces:any) => {
    // if type is 'Data and Content' or 'Content' and CF has HI3, user must select an HI3 Interfaces
    const CaseOptionPHIR = warrantCase?.options?.generatePacketDataHeaderReport;
    if (warrantCase.type === 'ALL' || warrantCase.type === 'CC') {
      if (this.state.hi3Data && this.state.hi3Data.length > 0) {
        const hasValidHI3 = selectedHi3Interfaces.find((item: any) => (item.operation !== 'DELETE'));
        if (hasValidHI3 === false) {
          errors.push(`Case -${warrantCase.liId} : Case Type has content. Please select at least one HI3 Interface.`);
          return false;
        }
      }
    } else if (CaseOptionPHIR === false) {
      const hasValidHI3 = selectedHi3Interfaces.find((item: any) => (item.operation !== 'DELETE'));
      if (hasValidHI3 === false) {
        errors.push(`Case -${warrantCase.liId} : Case Type has no content. Please remove all selected HI3 Interface.`);
        return false;
      }
    }
    return true;
  }

  updateCfOptions = async (formTemplate: any, services: any, cfId: string) => {
    // If a case has a service that is unspecified, then its an existing case.
    // The application cannot determine all the allowed CFTypes with an unspecified service,
    // so we simply display that CF. This needs to be improvised when we allow for CF changes on an existing
    // case in DX 1.5
    const cfIdForDetails = (services && services.length === 1 && services.includes('unspecified'))
      ? cfId : undefined;
    formTemplate.general.fields.cfId.options =
      await this.warrantConfig.getCfDropDownOptions(this.setCfs, cfIdForDetails);
  }

  setCfs = (cfs: any) => {
    this.cfs = cfs;
  }

  // TODO: Remove the below unused method in DX 1.5
  getCfs = () => {
    return this.cfs;
  }

  showHI23Interface = () => {
    this.setState({ isHi23InterfaceVisible: true });
  }

  handleHI23InterfaceClose = () => {
    this.setState({ isHi23InterfaceVisible: false });
  }

  handleHI23InterfaceSubmit = (hi3Interfaces: any, warrantCase: any = null) => {
    if (!warrantCase) {
      warrantCase = cloneDeep(this.state.warrantCase);
    }
    /**
     * For Edit Warrant reusing Classic New Warrant component,
     * this function needs to handle warrantCase.hi3Interfaces upon new/original hi3 objects
     * and set the correct opetation.
     */
    const prevHi3 = cloneDeep(warrantCase.hi3Interfaces);
    // Handle the corner case when the hi3 was marked 'DELETE' then add it back,
    // so update hi3 id which should be real HI3 id rather than Hi3-template/Hi2 id.
    // and set the operation 'NO_OPERATION'
    hi3Interfaces.forEach((item: any) => {
      const findFunc = (hi3: any) => (hi3.id === item.id && hi3.operation === 'DELETE');
      if (prevHi3?.some(findFunc)) {
        if (item.operation === 'CREATE') {
          const realHi3 = prevHi3.find(findFunc);
          item.id = realHi3.id;
          item.options = realHi3.options;
          item.operation = 'NO_OPERATION';
        }
      }
    });
    /**
     * When the previous Hi3 objects are not contained in new Hi3 objects,
     * if the object is not 'CREATE', set operation 'DELETE', keep it in case.hi3interfaces.
     * if the object is 'CREATE', filter it out.
     */
    prevHi3?.forEach((item: any) => {
      if (!hi3Interfaces?.some((hi3: any) => (hi3.id === item.id))) {
        if (item.operation !== 'CREATE') {
          hi3Interfaces.push({ ...item, operation: 'DELETE' });
        }
      }
    });
    warrantCase.hi3Interfaces = hi3Interfaces;
    const errors:string[] = [];
    if (this.checkHI3InterfaceValidationRule(errors, warrantCase, hi3Interfaces) === false) {
      showErrors(errors);
      return;
    }
    this.setState({
      warrantCase,
      isHi23InterfaceVisible: false,
    });
    this.props.updateGridOnCaseDetailFieldChange('hi3Interfaces', hi3Interfaces);
    if (this.props.changeTracker) {
      this.props.changeTracker.addUpdated(`case.${warrantCase.id}`);
      this.props.changeTracker.addHI3Interface(warrantCase.id);
    }
  }

  isGencccEnabled = (warrantCase:any) => {
    return (this.warrantConfig.genccc && (warrantCase.type === 'CC' || warrantCase.type === 'ALL'));
  }

  caseDetailsFieldUpdateHandler = async (changedField: ChangedField) => {
    const {
      name, value,
    } = changedField;
    const prevDisplayValue = this.state.warrantCase[name];

    // Track case changes.  For target change, tracking case update is in the target code.
    if (this.props.changeTracker && name !== 'targets') {
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}`);
    }
    const formTemplate = cloneDeep(this.state.formTemplate);
    const warrantCase = cloneDeep(this.state.warrantCase);
    this.props.updateGridOnCaseDetailFieldChange(name, value);
    warrantCase[name] = value;
    let shouldTargetsUpdate = false;
    let { genccc } = this.state;
    switch (name) {
      case 'services': {
        const unspecifiedServiceIndex = value.findIndex((service: any) => { return service === 'unspecified'; });
        if (value.length > 1 && unspecifiedServiceIndex !== -1) {
          value.splice(unspecifiedServiceIndex, 1);
          warrantCase[name] = value;
        }
        await this.handleServicesChange(formTemplate, warrantCase, value);
        const areThereErrors = await this.changeTargets(formTemplate, warrantCase);
        if (areThereErrors) return;
        this.props.updateGridOnCaseDetailFieldChange('cfId', warrantCase.cfId);
        this.props.updateGridOnCaseDetailFieldChange('targets', warrantCase.targets);
        if (formTemplate.general.fields.cfId.options.length === 1 &&
           formTemplate.general.fields.cfId.options[0].value !== '') {
          warrantCase.cfId = formTemplate.general.fields.cfId.options[0].value;
          const result = await this.handleCFselect(formTemplate, warrantCase, value, genccc);
          if (result.areThereErrors) return;
          this.props.updateGridOnCaseDetailFieldChange('cfId', warrantCase.cfId);
          genccc = result.genccc;
        }
        shouldTargetsUpdate = true;
        break;
      }
      case 'cfId': {
        const result = await this.handleCFselect(formTemplate, warrantCase, value, genccc);
        if (result.areThereErrors) return;
        genccc = result.genccc;
        shouldTargetsUpdate = true;
        break;
      }
      case 'type': {
        genccc = this.isGencccEnabled(warrantCase);
        if (value !== prevDisplayValue) {
          this.setHI3Interfaces(value, warrantCase);
          if (warrantCase.type !== 'CC' && warrantCase.type !== 'ALL') {
            this.clearTargetsCCCvaluesCCRType(warrantCase);
            this.props.updateGridOnCaseDetailFieldChange('targets', warrantCase.targets);
          }
        }
        break;
      }
      default: break;
    }

    this.setState({
      formTemplate,
      warrantCase,
      shouldTargetsUpdate,
      genccc,
    });
  }

  clearTargetsCCCvaluesCCRType = (warrantCase: any) => {
    const { targets } = warrantCase;
    targets.forEach((target: any) => {
      if (!target.afs || !target.targetInfoHash) return;
      const allSelectedTargetInfoHash = target.targetInfoHash;
      const nortelTargetInfoHashItem = allSelectedTargetInfoHash
        .find((targetInfoHashItem: any) =>
          targetInfoHashItem.targetInfoType === TARGET_INFO_TYPE.MTXCFCI);
      if (nortelTargetInfoHashItem && nortelTargetInfoHashItem.options) {
        nortelTargetInfoHashItem.options = [];
        if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${target.id}`);
        }
      }
    });
  }

  handleCFselect = async (formTemplate: any, warrantCase: any, value: any, genccc: any) => {
    await this.handleCfChange(formTemplate, value, warrantCase.services);
    const areThereErrors = await this.changeTargets(formTemplate, warrantCase);
    if (areThereErrors) return { genccc: false, areThereErrors };
    genccc = this.isGencccEnabled(warrantCase);
    this.props.updateGridOnCaseDetailFieldChange('targets', warrantCase.targets);
    if (this.props.changeTracker) {
      this.props.changeTracker.addHI3Interface(warrantCase.id);
    }
    await this.updateHI2Interfaces(value, warrantCase);
    await this.updateHI3Interfaces(value, warrantCase);
    return { genccc, areThereErrors };
  }

  updateHI2Interfaces = (cfId: any, warrantCase: any, updateDataOnly = false) => {
    if (cfId && cfId !== '') {
      fetchCollectionFunctionHI2InterfaceList(this.props.selectedDF, cfId).payload
        .then((values: any) => {
          const hi2Data = (values !== undefined
            && values.data !== undefined && values.data.data !== undefined)
            ? values.data.data : [];
          this.setState({ hi2Data });
          if (!updateDataOnly) {
            const type = this.caseRef.getValue('type');
            this.setHI3Interfaces(type, warrantCase);
          }
        });
    } else {
      this.setState({ hi2Data: [] });
      if (!updateDataOnly) {
        this.handleHI23InterfaceSubmit([], warrantCase);
      }
    }
  }

  updateHI3Interfaces = (cfId: any, warrantCase: any, updateDataOnly = false) => {
    if (cfId && cfId !== '') {
      const { selectedDF } = this.props.global;
      fetchCollectionFunctionHI3InterfaceList(selectedDF, cfId).payload
        .then((values: any) => {
          const hi3Data = (values !== undefined
            && values.data !== undefined && values.data.data !== undefined)
            ? values.data.data : [];
          this.setState({ hi3Data });
          if (!updateDataOnly) {
            const type = this.caseRef.getValue('type');
            this.setHI3Interfaces(type, warrantCase);
          }
        });
    } else {
      this.setState({ hi3Data: [] });
      if (!updateDataOnly) {
        this.handleHI23InterfaceSubmit([], warrantCase);
      }
    }
  }

  areExistingTargetsCompatible = (originalExistingTargets: any[], formTemplate: any,
    warrantCase: any) => {
    if (originalExistingTargets.length !== 0) {
      this.handleExistingTargetsDelete(cloneDeep(formTemplate), cloneDeep(warrantCase), true);
      return false;
    }
    return true;
  }

  mergeTargets = (possibleTargets: any, existingTargets: any, isNotServiceOrCfChange?: boolean) => {
    const newTargets: any[] = [];
    const possibleTargetsMap: any = {};
    const notCompatibleExistingTargets: any[] = [];
    possibleTargets.forEach((pt: any) => {
      possibleTargetsMap[pt.primaryType] = {
        isUsed: false,
        target: cloneDeep(pt),
      };
    });
    existingTargets.forEach((et: any) => {
      const pt = possibleTargetsMap[et.primaryType];
      if (pt && pt.target) {
        if (!this.isTargetEmpty(et)) {
          newTargets.push(this.mergeTarget(pt, et));
          pt.isUsed = true;
        }
      } else if (!this.isTargetEmpty(et)) {
        if (isNotServiceOrCfChange) {
          newTargets.push(et);
        } else {
          notCompatibleExistingTargets.push(et);
        }
      }
    });
    Object.keys(possibleTargetsMap).forEach((key: any) => {
      if (possibleTargetsMap[key].isUsed === false) {
        newTargets.push(possibleTargetsMap[key].target);
      }
    });
    return {
      newTargets,
      notCompatibleExistingTargets,
    };
  }

  mergeTarget = (pt: any, existingTarget: any) => {
    const newTarget = cloneDeep(existingTarget);
    if (!pt.isUsed) {
      newTarget.isRequired = pt.target.isRequired;
    }
    return newTarget;
  }

  trackDeletedTargets = (caseId: string, oldTargetList: Array<any>, newTargetList: Array<any>) => {
    if (!this.props.changeTracker) return;
    if (!oldTargetList || !newTargetList) return;

    const newTargetIds: Array<string> = [];
    newTargetList.forEach((target: any) => {
      newTargetIds.push(target.id);
    });
    oldTargetList.forEach((target: any) => {
      if (!this.isTargetEmpty(target)) {
        this.props.changeTracker.addDeleted(`case.${caseId}.target.${target.id}`, target.primaryType);
        this.props.changeTracker.addUpdated(`case.${caseId}`);
      }
    });
  }

  handleExistingTargetsDelete =
    (formTemplate: any, warrantCase: any, shouldTargetsUpdate: boolean) => {
      let message = 'All target values will be lost. Click \'Ok\' to continue';
      let revertBackField = '';
      if ((warrantCase.services && warrantCase.services.length === 0)) {
        revertBackField = 'services';
      } else if (!warrantCase.cfId) {
        revertBackField = 'cfId';
      } else {
        message = 'Some or all target values may be lost due to current selection of'
          + ' services and CF combination. Click \'Ok\' to continue.';
        revertBackField = 'cfId';
      }
      const popUp = {
        message,
        actionText: 'Ok',
        isOpen: true,
        handleClose: () => {
          const newPopUp = { ...this.state.popUp };
          newPopUp.isOpen = false;
          this.setState({
            popUp: newPopUp,
          });
          this.caseDetailsFieldUpdateHandler({
            name: revertBackField,
            value: this.state.warrantCase[revertBackField],
          });
        },
        handleSubmit: () => {
          const newPopUp = { ...this.state.popUp };
          newPopUp.isOpen = false;
          this.trackDeletedTargets(warrantCase.id, this.state.warrantCase.targets,
            warrantCase.targets);
          this.setState({
            formTemplate,
            warrantCase,
            shouldTargetsUpdate,
            popUp: newPopUp,
          });
          return DONT_CLOSE_ON_SUBMIT;
        },
      };
      this.setState({ popUp });
    }

  handlePopUpClose = () => {
    const popUp = { ...this.state.popUp };
    popUp.isOpen = false;
    this.setState({ popUp });
  }

  isTargetEmpty = (target: any) => {
    return (!target.primaryValue && !target.secondaryValue
      && !(target.options && target.options.length > 0));
  }

  showAdvancedProvisioning = () => {
    this.setState({ isAdvancedProvisioningVisible: true });
  }

  handleAdvancedProvisioningClose = () => {
    this.setState({ isAdvancedProvisioningVisible: false });
  }

  handleAdvancedProvisioningSubmit = (targets: any) => {
    const warrantCase = cloneDeep(this.state.warrantCase);
    const changedTargets = this.getUpdatedTargetsList(warrantCase.targets, targets);
    warrantCase.targets = targets;
    this.setState({
      warrantCase,
      isAdvancedProvisioningVisible: false,
    });
    this.props.updateGridOnCaseDetailFieldChange('targets', targets);

    if (this.props.changeTracker && changedTargets) {
      changedTargets.forEach((item: any) =>
        this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${item}`));
    }
  }

  sortArray = (first: any, next: any) => {
    if (first.id < next.id) { return -1; }
    if (first.id > next.id) { return 1; }
    return 0;
  }

  getCaseInterfacesRefs = (warrantCase: any): CaseInterfacesRefs => {
    const selectedCf = this.cfs.find((cf: any) => { return cf.id === warrantCase.cfId; });
    const cfType = selectedCf ? selectedCf.tag : undefined;
    const casePHIR = warrantCase?.options?.generatePacketDataHeaderReport
      && cfType === 'CF-3GPP-33108';
    return {
      dataOnly: INTERCEPT_TYPE_CD === warrantCase?.type,
      casePHIR,
    };
  }

  getUpdatedTargetsList = (oldTargets: any, newTargets: any) => {
    // The targets in both old and new are same... the afs change
    const result = oldTargets.map((ot: any) => {
      const { id } = ot;
      const nt = newTargets.filter((item: any) => item.id === ot.id);
      if (nt.length === 0) return undefined;
      const oldAfs = cloneDeep(ot.afs);
      const newAfs = cloneDeep(nt[0].afs);

      // are afs equal
      if (oldAfs.afIds.length !== newAfs.afIds.length) return id;
      const oAfIds = Object.assign([], oldAfs.afIds
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfIds = Object.assign([], newAfs.afIds
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfIds.length; i += 1) {
        if (oAfIds[0].id !== nAfIds[0].id) {
          return id;
        }
      }

      // // are afg equal
      if (oldAfs.afGroups.length !== newAfs.afGroups.length) return id;
      const oAfgIds = Object.assign([], oldAfs.afGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfgIds = Object.assign([], newAfs.afGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfgIds.length; i += 1) {
        if (oAfgIds[0].id !== nAfgIds[0].id) {
          return id;
        }
      }

      // // are aft equal
      if (oldAfs.afAutowireTypes.length !== newAfs.afAutowireTypes.length) return id;
      const oAftIds = Object.assign([], oldAfs.afAutowireTypes
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAftIds = Object.assign([], newAfs.afAutowireTypes
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAftIds.length; i += 1) {
        if (oAftIds[0].id !== nAftIds[0].id) {
          return id;
        }
      }
      // // are awg equal
      if (oldAfs.afAutowireGroups.length !== newAfs.afAutowireGroups.length) return id;
      const oAfwgIds = Object.assign([], oldAfs.afAutowireGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      const nAfwgIds = Object.assign([], newAfs.afAutowireGroups
        .sort((first: any, next: any) => this.sortArray(first, next)));
      for (let i = 0; i < oAfwgIds.length; i += 1) {
        if (oAfwgIds[0].id !== nAfwgIds[0].id) {
          return id;
        }
      }
      return undefined;
    });
    return result.filter((r: any) => { return r !== undefined; });
  }

  render() {
    const { mode } = this.props;
    const cfDetails = this.cfs.find((cf: any) => (cf.id === this.state.warrantCase.cfId));
    const targetFields = this.warrantConfig.getTargetFields(this.state.warrantCase.targets);
    return (
      <div className="caseContainer">
        <Button
          className="bp3-button"
          style={{ marginRight: '10px' }}
          text="Case Options"
          onClick={this.showCaseOptions}
        />
        <Button
          className="bp3-button"
          style={{ marginRight: '10px' }}
          text="Interfaces"
          onClick={this.showHI23Interface}
        />
        <Button className="bp3-button" text="Advanced Provisioning" onClick={this.showAdvancedProvisioning} />
        <div className={this.props.mode === 'view' ? 'formDisabled' : 'formEnabled'}>
          <DynamicForm
            ref={(ref: any) => { this.caseRef = ref; }}
            name={this.state.formTemplate.key}
            fields={this.state.formTemplate.general.fields}
            layout={this.state.formTemplate.general.metadata.layout.rows}
            defaults={this.state.warrantCase}
            originalValues={this.props.originalValues}
            fieldUpdateHandler={this.caseDetailsFieldUpdateHandler}
          />
          {this.state.genccc ? (
            <GencccPanel
              ref={(ref: any) => { this.genCCCSummaryRef = ref; }}
              defaults={this.state.warrantCase.genccc}
              fieldUpdateHandler={this.caseDetailsFieldUpdateHandler}
              caseId={this.state.warrantCase.id}
              mode={mode}
            />
          )
            :
            <Fragment />}
          <TargetsSummary
            ref={(ref: any) => { this.TargetsSummaryRef = ref; }}
            targets={this.state.warrantCase.targets}
            targetFields={targetFields}
            caseDetailsFieldUpdateHandler={this.caseDetailsFieldUpdateHandler}
            shouldTargetsUpdate={this.state.shouldTargetsUpdate}
            warrantCase={this.props.warrantCase}
            changeTracker={this.props.changeTracker}
          />
        </div>
        <CaseOptions
          isOpen={this.state.isCaseOptionsVisible}
          currentStateMode={this.state.currentStateMode}
          handleClose={this.handleCaseOptionsClose}
          inputCaseOptions={this.state.warrantCase.options}
          handleSubmit={this.handleCaseOptionsSubmit}
          caseId={this.state.warrantCase.id}
          templateUsed={this.state.warrantCase.templateUsed}
          templates={this.props.templates}
        />
        <HI2AndHI3Options
          isOpen={this.state.isHi23InterfaceVisible}
          currentStateMode={this.state.currentStateMode}
          handleClose={this.handleHI23InterfaceClose}
          cfId={this.state.warrantCase.cfId}
          hi2Data={this.state.hi2Data}
          hi3Interfaces={this.state.warrantCase.hi3Interfaces}
          handeSubmit={this.handleHI23InterfaceSubmit}
          hi3Data={this.state.hi3Data}
          id={this.state.warrantCase.id}
          caseInterfacesRefs={this.state.caseInterfacesRefs}
        />
        <AdvancedProvisioning
          isOpen={this.state.isAdvancedProvisioningVisible}
          currentStateMode={this.state.currentStateMode}
          handleClose={this.handleAdvancedProvisioningClose}
          handleSubmit={this.handleAdvancedProvisioningSubmit}
          targetFields={targetFields}
          targets={this.state.warrantCase.targets}
          drvirtual={this.warrantConfig.getDrVirtual()} // TODO: get this value from server/config?
          cfType={cfDetails ? cfDetails.type : ''}
          targetAfDetails={this.warrantConfig.getTargetWithAllAfs()}
          caseType={this.state.warrantCase.type}
          isSurvOptCombined={this.state.warrantCase.options
          && this.state.warrantCase.options.circuitSwitchedVoiceCombined}
          warrantCase={this.state.warrantCase}
        />
        <ModalDialog
          displayMessage={this.state.popUp.message}
          isOpen={this.state.popUp.isOpen}
          onSubmit={this.state.popUp.handleSubmit}
          onClose={this.state.popUp.handleClose}
          actionText={this.state.popUp.actionText}
        />
      </div>
    );
  }
}

export default CaseDetails;
