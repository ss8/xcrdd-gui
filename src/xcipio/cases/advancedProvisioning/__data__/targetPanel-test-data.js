// eslint-disable-next-line import/prefer-default-export
export const endeavorTargetsData = [
  {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [
        {
          id: 'QVhTLTcwMDI',
          name: 'AXS-7002',
          type: 'AXSGP',
          model: 'AXS-7002',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
        {
          id: 'QVhTLTcxMDA',
          name: 'AXS-7100',
          type: 'AXSGP',
          model: 'AXS-7100',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
      ],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
      afccc: [],
    },
    primaryType: 'IMSI',
    primaryValue: '1231233112321',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'IMSI_1593018826222',
    isRequired: true,
    targetInfoHash: [
      {
        targetInfoType: 'GPTARGETINFO',
        targetInfos: [
          {
            ifid: null,
            options: [
              {
                key: 'SERVICE',
                value: 'SMS_SIGTRAN_GTP',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [
        {
          id: 'QVhTLTcwMDI',
          name: 'AXS-7002',
          type: 'AXSGP',
          model: 'AXS-7002',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
        {
          id: 'QVhTLTcxMDA',
          name: 'AXS-7100',
          type: 'AXSGP',
          model: 'AXS-7100',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
      ],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
      afccc: [],
    },
    primaryType: 'MSISDN',
    primaryValue: '12321321213231',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'MSISDN_1593018826222',
    isRequired: true,
    targetInfoHash: [
      {
        targetInfoType: 'GPTARGETINFO',
        targetInfos: [
          {
            ifid: null,
            options: [
              {
                key: 'SERVICE',
                value: 'SMS_SIGTRAN_PVOIP_XCAP_GTP',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [
        {
          id: 'QVhTLTcwMDI',
          name: 'AXS-7002',
          type: 'AXSGP',
          model: 'AXS-7002',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
        {
          id: 'QVhTLTcxMDA',
          name: 'AXS-7100',
          type: 'AXSGP',
          model: 'AXS-7100',
          x3TargetAssociations: 'GPTARGETINFO',
          bAFAssociation: true,
        },
      ],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
      afccc: [],
    },
    primaryType: 'IMEI',
    primaryValue: '1231213232212312',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'IMEI_1593018826222',
    isRequired: true,
    targetInfoHash: [
      {
        targetInfoType: 'GPTARGETINFO',
        targetInfos: [
          {
            ifid: null,
            options: [
              {
                key: 'SERVICE',
                value: 'SMS_GTP',
              },
            ],
          },
        ],
      },
    ],
  },
];
