import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import {
  Checkbox, HTMLSelect, Label, Button, Switch, Alignment, Icon,
} from '@blueprintjs/core';
import { RootState } from 'data/root.reducers';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { AxsgpTargetInfo } from '../targetInfo/axsgp-target-info';
import TargetCCCInfo from '../targetInfo/target-ccc-info';
import './access-functions.scss';
import AdditionalTargetInfoStatus from './components/AdditionalTargetInfoStatus';

// TODO: refactor this - Use react-checbox-tree
// For r-c-t - need to determine combining Af, AFG and AFT;
// Need to figure out implementing JSX and separate click on checkbox label and the ccc button
class AccessFunctions extends React.Component<any, any> {
  private afTypeFilterOptions: any;

  constructor(props: any) {
    super(props);
    const result = this.getAllValues(this.props, 'All');

    this.state = {
      disabled: result.disabled,
      allTTAfs: result.allTTAfs,
      allTTAFGroup: result.allTTAFGroups,
      allTTAfTypes: result.allTTAfTypes,
      filterValue: 'All',
      selectedTargetId: this.props.targetId,
      allAfsChecked: false,
      allAfGroupsChecked: false,
      allAfTypesChecked: false,
      allAutoAfGroupssChecked: false,
      targetAfDetails: this.props.targetAfDetails,
      selectedTargetType: this.props.targetType,
    };
  }

  // TODO: Use a different method
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if (nextProps !== undefined) {
      const filterValue = nextProps.targetId === this.state.selectedTargetId ? this.state.filterValue : 'All';
      const hasAdvancedAfs = nextProps.drvirtual ? this.targetHasAdvancedAf(nextProps) : false;
      const result = this.getAllValues(nextProps, filterValue);

      this.setState({
        disabled: result.disabled,
        allTTAfs: result.allTTAfs,
        allTTAFGroup: result.allTTAFGroups,
        allTTAfTypes: result.allTTAfTypes,
        filterValue,
        selectedTargetId: nextProps.targetId,
        selectedTargetType: nextProps.targetType,
        targetAfDetails: nextProps.targetAfDetails,
        allAfsChecked: this.enableOrDisableAll(result.allTTAfs,
          (nextProps.selectedAfs.afs && nextProps.selectedAfs.afs.afIds)
            ? nextProps.selectedAfs.afs.afIds : []),
        allAfGroupsChecked: this.enableOrDisableAll(result.allTTAFGroups,
          (nextProps.selectedAfs.afs && nextProps.selectedAfs.afs.afGroups)
            ? nextProps.selectedAfs.afs.afGroups : []),
        allAfTypesChecked: this.enableOrDisableAll(result.allTTAfTypes,
          (nextProps.selectedAfs.afs && nextProps.selectedAfs.afs.afAutowireTypes)
            ? nextProps.selectedAfs.afs.afAutowireTypes : []),
        allAutoAfGroupssChecked: this.enableOrDisableAll(result.allTTAFGroups,
          (nextProps.selectedAfs.afs && nextProps.selectedAfs.afs.afAutowireGroups)
            ? nextProps.selectedAfs.afs.afAutowireGroups : []),
        hasAdvancedAfs,
      });
    }
  }

  getCurrTargetDetails = (props: any) => {
    return props.targetAfDetails
      ? props.targetAfDetails.filter((af: any) => af.primaryType === props.targetType)[0] : [];
  }

  targetHasAdvancedAf = (props: any) => {
    const currTargetDetails = this.getCurrTargetDetails(props);
    const allTTAfTypes = currTargetDetails && currTargetDetails.length !== 0
      && currTargetDetails.afs && currTargetDetails.afs.afAutowireTypes
      ? currTargetDetails.afs.afAutowireTypes.map((aft: any) => aft.id) : [];

    const allAfs = currTargetDetails && currTargetDetails.length !== 0
      && currTargetDetails.afs && currTargetDetails.afs.afIds
      ? currTargetDetails.afs.afIds : [];

    if (allTTAfTypes.length === 0 || allAfs.length === 0) return false;

    const advancedAfs = (Object.assign([], allAfs.filter((af: any) => {
      return allTTAfTypes.some((aft: any) => af.tag === aft);
    })));

    return (advancedAfs.length > 0);
  }

  getAllValues = (props: any, filter: any) => {
    const disabled = props.currentStateMode === CONFIG_FORM_MODE.View;

    const currTargetDetails = this.getCurrTargetDetails(props);

    // get AF, AF type and AF Group selection options to display
    const allAfs = currTargetDetails && currTargetDetails.length !== 0
      && currTargetDetails.afs && currTargetDetails.afs.afIds
      ? currTargetDetails.afs.afIds.filter((af:any) => (af.bAFAssociation)) : [];

    let allTTAfTypes = this.getAllAfTypesByTargetType(props, filter);
    const allTTAfTypeIds = allTTAfTypes ? allTTAfTypes.map((aft: any) => (aft.id)) : [];
    let allTTAfs = props.drvirtual
      ? this.getBasicAfs(props, allAfs, filter)
      : this.getAFsByTargetType(props, allTTAfTypes, allAfs);
    let allTTAFGroups = this.getAFGroupsByTargetType(props, allTTAfTypeIds, allAfs);

    const { selectedAfs } = props;
    // Add the AfId,AfGroups and AFTypes that are checked on server but not in the determined list, if the list is empty
    if (allTTAfs.length === 0) {
      allTTAfs = Object.assign([], selectedAfs?.afs?.afIds);
    }
    // Add both groups and autowire groups
    if (allTTAFGroups.length === 0) {
      const allTTAFAuto = Object.assign([], selectedAfs?.afs?.afAutowireGroups);
      allTTAFGroups = Object.assign(allTTAFAuto, selectedAfs?.afs?.afGroups);
    }
    if (allTTAfTypes.length === 0) {
      allTTAfTypes = Object.assign([], selectedAfs?.afs?.afAutowireTypes);
    }

    return {
      disabled,
      allTTAfs,
      allTTAFGroups,
      allTTAfTypes,
    };
  }

  enableOrDisableAll = (allAfs: any, selectedAfs: any) => {
    const result = allAfs.filter((af: any) => selectedAfs.some((saf: any) => af.id === saf.id));
    return result.length === allAfs.length;
  }

  getAllAfTypesByTargetType = (props: any, filter: any) => {
    const allAfTypesByTarget = this.getCurrTargetDetails(props);
    const result = allAfTypesByTarget && allAfTypesByTarget.afs
      && allAfTypesByTarget.afs.afAutowireTypes
      ? allAfTypesByTarget.afs.afAutowireTypes.map((aft: any) => aft.id) : [];

    this.afTypeFilterOptions = Object.assign([], ['All'].concat(result));

    const filterResult = filter !== undefined && filter !== 'All' && result !== undefined
      ? result.filter((r: any) => (r === filter)) : result;
    return filterResult === undefined
      ? [] : filterResult.map((aft: any) => { return { id: aft, name: aft }; });
  }

  getAFsByTargetType = (props: any, allTTAfTypes: {id:string, name:string}[], allAfs: any) => {
    // the id in allTTAfTypes is an AF Tag.
    if (allTTAfTypes !== undefined && allTTAfTypes.length !== 0) {
      const result = (Object.assign([], allAfs.filter((af: any) => {
        return allTTAfTypes.some((aft: any) => af.tag === aft.id);
      })));
      return result;
    }
    return allAfs;
  }

  getAFGroupsByTargetType = (props: any, allTTAfTypes: string[], allAfs: any) => {
    const { allAfGroups } = props;
    if (!allAfGroups) return [];
    // allTTAfTypes is an array of AF Tags
    const allAfsByAfType = allAfs.filter((af: any) => allTTAfTypes.includes(af.tag));
    const allAFGrpsWithTypes = allAfGroups.map((afg: any) => {
      const afs = afg.afIds.map((id: any) => (id.id));
      const afsType = (Object.assign([],
        allAfsByAfType.filter((af: any) => afs.includes(af.id))));
      const asd = Array.from(new Set(afsType.map((a: any) => a.tag)));
      return { id: afg.id, name: afg.name, afTypes: asd };
    });

    const result = (Object.assign([], allAFGrpsWithTypes.filter((af: any) => {
      return allTTAfTypes.some((aft: any) => af.afTypes.includes(aft));
    })));
    return result;
  }

  getBasicAfs = (props: any, allAfs: any, filter: any) => {
    const allAfTypesByTarget = this.getCurrTargetDetails(props);
    const advAfTypes = allAfTypesByTarget && allAfTypesByTarget.afs
      && allAfTypesByTarget.afs.afAutowireTypes
      ? allAfTypesByTarget.afs.afAutowireTypes.map((af: any) => af.id) : [];

    if (advAfTypes && advAfTypes.length !== 0) {
      const result = (Object.assign([], allAfs.filter((af: any) => {
        return advAfTypes.indexOf(af.tag) === -1;
      })));
      this.afTypeFilterOptions = Object.assign([],
        this.afTypeFilterOptions.concat(result.map((faf: any) => (faf.tag))).sort());

      return filter !== undefined && filter !== 'All'
        ? result.filter((af: any) => af.tag === filter)
        : result;
    }
    this.afTypeFilterOptions = Object.assign([],
      this.afTypeFilterOptions.concat(allAfs.map((faf: any) => (faf.tag))).sort());
    return allAfs;
  }

  handleFilterChange = (e: any) => {
    const { value } = e.target;
    const result = this.getAllValues(this.props, value);
    const label = 'Access Functions: af type';
    if (this.props.changeTracker) {
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.AccessFunction.afType`, value, label, this.state.filterValue);
    }
    this.setState({
      disabled: result.disabled,
      allTTAfs: result.allTTAfs,
      allTTAFGroup: result.allTTAFGroups,
      allTTAfTypes: result.allTTAfTypes,
      filterValue: value,
      targetAfDetails: this.props.targetAfDetails,
      selectedTargetId: this.props.targetId,
      selectedTargetType: this.props.targetType,
      allAfsChecked: this.enableOrDisableAll(result.allTTAfs,
        (this.props.selectedAfs.afs && this.props.selectedAfs.afs.afIds)
          ? this.props.selectedAfs.afs.afIds : []),
      allAfGroupsChecked: this.enableOrDisableAll(result.allTTAFGroups,
        (this.props.selectedAfs.afs && this.props.selectedAfs.afs.afGroups)
          ? this.props.selectedAfs.afs.afGroups : []),
      allAfTypesChecked: this.enableOrDisableAll(result.allTTAfTypes,
        (this.props.selectedAfs.afs && this.props.selectedAfs.afs.afAutowireTypes)
          ? this.props.selectedAfs.afs.afAutowireTypes : []),
      allAutoAfGroupssChecked: this.enableOrDisableAll(result.allTTAFGroups,
        (this.props.selectedAfs.afs && this.props.selectedAfs.afs.afAutowireGroups)
          ? this.props.selectedAfs.afs.afAutowireGroups : []),
    });
  }

  handleAccessFunctionChange = (e: any) => {
    const {
      id, labels, checked, value,
    } = e.currentTarget;
    const selectedAfs = Object.assign([], this.props.selectedAfs);

    const newFields = JSON.parse(JSON.stringify(selectedAfs.afs));

    const label = labels[0];

    if (label !== undefined && label.className !== undefined) {
      if (label.className.includes('acc-func-all')) {
        newFields.afIds = this.addAfToSelectedList(
          this.state.allTTAfs, newFields.afIds, checked, id, true,
        );
        const allSelectAfIds = Object.assign([], this.props.selectedAfs.afs.afIds);
        allSelectAfIds.forEach((afid: any) => {
          if (checked && this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.af.${afid.id}`, 'selected', afid.name, 'unselected');
          } else if (this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.af.${afid.id}`, 'unselected', afid.name, 'selected');
          }
        });
      } else if (label.className.includes('af-grp-all')) {
        newFields.afGroups = this.addAfToSelectedList(
          this.state.allTTAFGroup, newFields.afGroups, checked, id, true,
        );
        const { afGroups } = newFields;
        afGroups.forEach((afgroup: any) => {
          if (checked && this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afGrp.${afgroup.id}`, 'selected', afgroup.name, 'unselected');
          } else if (this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afGrp.${afgroup.id}`, 'unselected', afgroup.name, 'selected');
          }
        });
      } else if ((label.className.includes('af-type-all'))) {
        newFields.afAutowireTypes = this.addAfToSelectedList(
          this.state.allTTAfTypes, newFields.afAutowireTypes, checked, id, true,
        );
        const { afAutowireTypes } = newFields;
        afAutowireTypes.forEach((afAutowireType: any) => {
          if (checked && this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoType.${afAutowireType.id}`, 'selected', afAutowireType.name, 'unselected');
          } else if (this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoType.${afAutowireType.id}`, 'unselected', afAutowireType.name, 'selected');
          }
        });
      } else if (label.className.includes('acc-fun')) {
        newFields.afIds = this.addAfToSelectedList(
          this.state.allTTAfs, newFields.afIds, checked, id,
        );

        const selectedAfsAFID = newFields.afIds.filter((afid: any) => afid.id === id);
        const selectedAfsAFIDName = selectedAfsAFID[0] ? selectedAfsAFID[0].name : '';
        if (checked && this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.af.${id}`, 'selected', selectedAfsAFIDName, 'unselected');
        } else if (this.props.changeTracker) {
          const selectedAfsAFIDs = Object.assign([], this.props.selectedAfs.afs.afIds)
            .filter((afid:any) => afid.id === id);
          const selectedAfsAFIDsName = selectedAfsAFIDs[0] ? selectedAfsAFIDs[0].name : '';
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.af.${id}`, 'unselected', selectedAfsAFIDsName, 'selected');
        }
      } else if (label.className.includes('af-grp')) {
        newFields.afGroups = this.addAfToSelectedList(
          this.state.allTTAFGroup, newFields.afGroups, checked, id,
        );
        const selectedAfGroup = newFields.afGroups.filter((afGroup: any) => afGroup.id === id);
        const selectedAfGroupName = selectedAfGroup[0] ? selectedAfGroup[0].name : '';
        if (checked && this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afGrp.${id}`, 'selected', selectedAfGroupName, 'unselected');
        } else if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afGrp.${id}`, 'unselected', selectedAfGroupName, 'selected');
        }
      } else if ((label.className.includes('af-type'))) {
        newFields.afAutowireTypes = this.addAfToSelectedList(
          this.state.allTTAfTypes, newFields.afAutowireTypes, checked, id,
        );
        const selectedAfAutowireGroup = newFields.afAutowireTypes.filter(
          (afAutowireType: any) => afAutowireType.id === id,
        );
        const selectedAfAutowireGroupName = selectedAfAutowireGroup[0] ? selectedAfAutowireGroup[0].name : '';
        if (checked && this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoType.${id}`, 'selected', selectedAfAutowireGroupName, 'unselected');
        } else if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoType.${id}`, 'unselected', selectedAfAutowireGroupName, 'selected');
        }
      } else if (label.className.includes('af-autogrp-all')) {
        newFields.afAutowireGroups = this.addAfToSelectedList(
          this.state.allTTAFGroup, newFields.afAutowireGroups, checked, id, true,
        );

        const { afAutowireGroups } = newFields;
        afAutowireGroups.forEach((afAutowireGroup: any) => {
          if (checked && this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoGrp.${afAutowireGroup.id}`, 'selected', afAutowireGroup.name, 'unselected');
          } else if (this.props.changeTracker) {
            this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoGrp.${afAutowireGroup.id}`, 'unselected', afAutowireGroup.name, 'selected');
          }
        });
      } else if ((label.className.includes('af-autogrp'))) {
        newFields.afAutowireGroups = this.addAfToSelectedList(
          this.state.allTTAFGroup, newFields.afAutowireGroups, checked, id,
        );
        const selectedAfAutowireGroup = newFields.afAutowireGroups.filter(
          (afAutowireGroup: any) => afAutowireGroup.id === id,
        );
        const selectedAfAutowireGroupName = selectedAfAutowireGroup[0] ? selectedAfAutowireGroup[0].name : '';
        if (checked && this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoGrp.${id}`, 'selected', selectedAfAutowireGroupName, 'unselected');
        } else if (this.props.changeTracker) {
          this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.props.targetId}.afAutoGrp.${id}`, 'unselected', selectedAfAutowireGroupName, 'selected');
        }
      }
    }

    selectedAfs.afs = newFields;
    this.props.accessFunctionUpdateSet(true, selectedAfs);
  }

  addAfToSelectedList = (allAfs: any, destinationAfs: any,
    checked: boolean, id: any, all = false) => {
    if (checked) {
      const selectedAf = all
        ? allAfs.map((af: any) => {
          return { ...af };
        })
        : allAfs.filter((af: any) => (af.id === id))
          .map((af: any) => {
            return { ...af };
          });

      selectedAf.forEach((af: any) => {
        if (!destinationAfs.some((item: any) => (item.id === af.id))) destinationAfs.push(af);
      });
    } else {
      destinationAfs = all
        ? [] : Object.assign([], destinationAfs.filter((af: any) => af.id !== id));
    }
    return destinationAfs;
  }

  getFilterValues = (filterValue: any) => {
    const uniqueValues = Object.assign([], Array.from(new Set(filterValue)));
    const result = uniqueValues.map((item: any, i: any) => { return ({ key: i, value: item }); });
    return result;
  }

  handlecccClick = (e: any) => {
    const { id } = e.currentTarget;
    const afDetails = this.state.allTTAfs.find((item: any) => item.id === id);
    return this.props.onTargetcccClick(afDetails);
  }

  isTargetInfoSupported = (afDetails:any): boolean => {
    if (afDetails == null) return false;
    return AxsgpTargetInfo.isSupported(afDetails) || TargetCCCInfo.isSupportedAFType(afDetails);
  }

  shouldShowAccessFunctions(): boolean {
    const {
      selectedAfs,
    } = this.props;

    const {
      allTTAfs,
    } = this.state;

    return allTTAfs && allTTAfs.length > 0 && selectedAfs.afs && selectedAfs.afs.afIds;
  }

  shouldShowAccessFunctionAutowireGroups(): boolean {
    const {
      drvirtual,
      selectedAfs,
    } = this.props;

    const {
      hasAdvancedAfs,
      allTTAFGroup,
    } = this.state;

    const showAutoWireGroup = drvirtual && hasAdvancedAfs;

    return showAutoWireGroup && allTTAFGroup
      && allTTAFGroup.length > 0 && selectedAfs.afs && selectedAfs.afs.afAutowireGroups;
  }

  shouldShowAccessFunctionGroups(): boolean {
    const {
      drvirtual,
      selectedAfs,
    } = this.props;

    const {
      hasAdvancedAfs,
      allTTAFGroup,
    } = this.state;

    const showAfGroups = !(drvirtual && hasAdvancedAfs);

    return showAfGroups && allTTAFGroup && allTTAFGroup.length > 0 &&
      selectedAfs.afs && selectedAfs.afs.afGroups;
  }

  shouldShowAccessFunctionTypes(): boolean {
    const {
      drvirtual,
      selectedAfs,
    } = this.props;

    const {
      allTTAfTypes,
    } = this.state;

    return drvirtual && allTTAfTypes.length > 0 && selectedAfs.afs && selectedAfs.afs.afAutowireTypes;
  }

  renderAccessFunctionHeader(title: string, className: string, checked: boolean) {
    const {
      disabled,
    } = this.state;

    return (
      <div className="access-function-header">
        <span className="title">
          {title}
        </span>
        <span className="align-right">
          <Switch
            className={className}
            label="Select all"
            disabled={disabled}
            onChange={this.handleAccessFunctionChange}
            checked={checked}
            alignIndicator={Alignment.RIGHT}
            inline
          />
        </span>
      </div>
    );
  }

  renderAccessFunctionListWithAdditionalInfoButton(list: any[], checkedList: any[], className: string): JSX.Element {
    const {
      afDetails,
      isAccessFunctionTargetDetailValid,
    } = this.props;

    const {
      disabled,
    } = this.state;

    return (
      <div className="access-function-list" data-testid="AccessFunctionListWithAdditionalInfoButton">
        {list.map((element: any) => {
          const selectedAF = afDetails && afDetails.id === element.id;
          const checked = checkedList.some((item: any) => (item.id === element.id));
          const isValid = isAccessFunctionTargetDetailValid(element);

          return (
            <div key={element.id} className="access-function-item">
              <Checkbox
                className={className}
                id={element.id}
                label={element.name || `${element.type} - ${element.id}`}
                disabled={disabled}
                checked={checked}
                onChange={this.handleAccessFunctionChange}
                inline
              />
              {this.isTargetInfoSupported(element) && (
                <Fragment>
                  <AdditionalTargetInfoStatus isValid={isValid} disabled={!checked} />
                  <Button
                    onClick={this.handlecccClick}
                    id={element.id}
                    disabled={!checked}
                    intent={selectedAF ? 'primary' : 'none'}
                    active={selectedAF}
                  >
                    Additional Info
                  </Button>
                </Fragment>
              )}
            </div>
          );
        })}
      </div>
    );
  }

  renderAccessFunctionList(list: any[], checkedList: any[], className: string) {
    const {
      disabled,
    } = this.state;

    return (
      <div className="access-function-list" data-testid="AccessFunctionList">
        {list.map(({ id, name, type }) => {
          const checked = checkedList.some((item) => (item.id === id));
          return (
            <div key={id} className="access-function-item">
              <Checkbox
                className={className}
                id={id}
                label={name || `${type} - ${id}`}
                disabled={disabled}
                checked={checked}
                onChange={this.handleAccessFunctionChange}
              />
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const {
      selectedTargetType,
      filterValue,
      allAfsChecked,
      allTTAfs,
      allAutoAfGroupssChecked,
      allAfGroupsChecked,
      allTTAFGroup,
      allAfTypesChecked,
      allTTAfTypes,
      disabled,
    } = this.state;

    const {
      selectedAfs,
    } = this.props;

    return (
      <div className="cases-advanced-provisioning" data-testid="AdvancedProvisioningAccessFunctions">
        <div className="section-header">
          {`Provisioning for ${selectedTargetType}`}
        </div>
        <p className="instruction-text">
          Select the AF and AF Types for Provisioning
        </p>
        <div className="access-function-filter">
          Show Access Function Type
          <HTMLSelect
            key="selectFilter"
            autoComplete="off"
            autoCorrect="off"
            onChange={this.handleFilterChange}
            options={this.getFilterValues(this.afTypeFilterOptions)}
            disabled={disabled}
            value={filterValue}
            fill
          />
        </div>
        {this.shouldShowAccessFunctions() && (
          <Fragment>
            {this.renderAccessFunctionHeader('Access Functions', 'acc-func-all', allAfsChecked)}
            {this.renderAccessFunctionListWithAdditionalInfoButton(allTTAfs, selectedAfs.afs.afIds, 'acc-func')}
          </Fragment>
        )}
        {this.shouldShowAccessFunctionAutowireGroups() && (
          <Fragment>
            {this.renderAccessFunctionHeader('Access Functions Groups', 'af-autogrp-all', allAutoAfGroupssChecked)}
            {this.renderAccessFunctionList(allTTAFGroup, selectedAfs.afs.afAutowireGroups, 'af-autogrp')}
          </Fragment>
        )}
        {this.shouldShowAccessFunctionGroups() && (
          <Fragment>
            {this.renderAccessFunctionHeader('Access Functions Groups', 'af-grp-all', allAfGroupsChecked)}
            {this.renderAccessFunctionList(allTTAFGroup, selectedAfs.afs.afGroups, 'af-grp')}
          </Fragment>
        )}
        {this.shouldShowAccessFunctionTypes() && (
          <Fragment>
            {this.renderAccessFunctionHeader('Access Functions Types', 'af-type-all', allAfTypesChecked)}
            {this.renderAccessFunctionList(allTTAfTypes, selectedAfs.afs.afAutowireTypes, 'af-type')}
          </Fragment>
        )}
      </div>
    );
  }
}
const mapState = ({
  warrants: {
    changeTracker, warrantConfigInstance,
  },
}: RootState) => ({
  changeTracker,
  warrantConfigInstance,
});

const mapDispatch = {};

export default connect(mapState, mapDispatch, null, { forwardRef: true })(AccessFunctions);
