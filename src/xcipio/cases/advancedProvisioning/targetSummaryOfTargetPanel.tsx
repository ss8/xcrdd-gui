import React, { Fragment } from 'react';
import cloneDeep from 'lodash.clonedeep';
import { TargetModel } from 'data/target/target.types';
import TargetDetails from '../../targets/forms/target-details';
import { ChangedField } from '../../../shared/form';

export default class TargetsSummaryOfTargetPanel extends React.Component<any, any> {
  targetRefs:any[] = [];

  targetFieldUpdateHandler = (changedField: ChangedField, key: string, targets: any[]) => {
    const {
      name, value, label, valueLabel,
    } = changedField;
    const newTargets = targets.slice();
    const newTarget = newTargets.find((target:any) => { return target.primaryType === key; });
    // Track target change
    if (this.props.changeTracker && newTarget) {
      const oldValue = this.getTargetValue(newTarget, name);
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${newTarget.id}`, valueLabel, label, oldValue);
    }
    this.customModifyTargetOutputByType(newTarget, name, value);
    this.props.caseDetailsFieldUpdateHandler(changedField);
  }

  getTargetValue = (target: any, name: string) => {
    switch (target.primaryType) {
      case 'MDNwService':
      case 'MDN': {
        if (name === 'options') {
          if (target[name].length > 0) {
            return target[name][0].value;
          }
          return '';
        }
        return target[name];
      }
      default:
        return target[name];
    }
  }

  customModifyTargetOutputByType = (target: any, name: string, value: any) => {
    switch (target.primaryType) {
      case 'MDN': {
        if (name === 'options') {
          target[name] = [{
            key: 'InterceptType',
            value,
          }];
        } else {
          target[name] = value;
        }
        break;
      }

      case 'MDNwService':
        if (name === 'options') {
          target[name] = [{
            key: 'SERVICETYPE',
            value,
          }];
        } else {
          target[name] = value;
        }
        break;

      default: target[name] = value;
    }
  }

  areTargetsValid = (forceTouched : boolean = true) => {
    let areTargetValid = true;
    this.targetRefs.forEach((targetRef: any) => {
      if (targetRef) {
        const isTargetValid = targetRef.isTargetValid(forceTouched);
        areTargetValid = areTargetValid && isTargetValid;
      }
    });
    return areTargetValid;
  }

  handleClick = (event:any, target:any) => {
    // This line is needed to fix DX-983
    event.preventDefault();
    this.props.handleClick(target);
  }

  static targetFieldWithDisabledAccessFunction = (field:any) => {
    const fieldClone = cloneDeep(field);
    fieldClone.subFields.primaryValue.disabled = true;
    return fieldClone;
  };

  shouldShowTarget(target: TargetModel | null): boolean {
    const {
      hideEmptyTargets,
    } = this.props;

    if (target == null) {
      return false;
    }

    if (hideEmptyTargets && (target.primaryValue == null || target.primaryValue === '')) {
      return false;
    }

    return true;
  }

  render(): JSX.Element {
    const {
      targetFields,
      selectedTarget,
      targets,
      warrantCase,
      changeTracker,
    } = this.props;

    return (
      <Fragment>
        <div className="section-header">
          Targets
        </div>
        <p className="instruction-text">
          Select a target to review Access Function provisioning.
        </p>
        {targetFields && (
          <div className="targetSummaryOfTargetPanelTargetsList" data-testid="TargetSummaryOfTargetPanelTargetsList">
            {
              Object.keys(targetFields).map((key: any, index: number) => {
                const target = targets.find((t:any) => {
                  return t.primaryType === key;
                });

                if (!this.shouldShowTarget(target)) {
                  return (<Fragment />);
                }

                return (
                  <div
                    key={index}
                    onClick={(event:any) => { this.handleClick(event, target); }}
                  >
                    <div className={selectedTarget?.id === target.id ? 'selectedclass' : 'mytestclass'}>
                      <div className="formDisabled">
                        <TargetDetails
                          key={index}
                          ref={(ref:any) => { this.targetRefs.push(ref); }}
                          formId={`${target.primaryType}_${warrantCase.id}${warrantCase.cfId ? warrantCase.cfId : ''}${target.id}`}
                          target={cloneDeep(target)}
                          targetField={
                            TargetsSummaryOfTargetPanel.targetFieldWithDisabledAccessFunction(targetFields[key])
                          }
                          changeTracker={changeTracker}
                          fieldUpdateHandler={(changedField: ChangedField) => {
                            this.targetFieldUpdateHandler(changedField,
                              key, targets.slice());
                          }}
                        />
                      </div>
                    </div>
                  </div>
                );
              })
            }
          </div>
        )}
      </Fragment>
    );
  }
}
