import React, { FunctionComponent } from 'react';
import { Icon } from '@blueprintjs/core';
import './additional-target-info-status.scss';

type AdditionalTargetInfoStatusProps = {
  isValid?: boolean
  disabled?: boolean
}

const AdditionalTargetInfoStatus: FunctionComponent<AdditionalTargetInfoStatusProps> = ({
  isValid,
  disabled,
}: AdditionalTargetInfoStatusProps) => {
  let text = 'Review additional target information';
  let className = 'required-info';

  if (isValid) {
    text = 'Additional target information ok';
    className = 'fulfilled-info';
  }

  if (disabled) {
    className += ' disabled-info';
  }

  return (
    <div title={text} className={`additional-target-info-status ${className}`}>
      {text}
      <Icon className="required-icon" icon="error" intent="danger" iconSize={15} />
      <Icon className="fulfilled-icon" icon="tick" intent="success" iconSize={15} />
    </div>
  );
};

export default AdditionalTargetInfoStatus;
