import React from 'react';
import { render, screen } from '@testing-library/react';
import AdditionalTargetInfoStatus from './AdditionalTargetInfoStatus';

describe('<AdditionalTargetInfoStatus />', () => {

  it('should render the expected text and icon when valid', () => {
    const { container } = render(
      <AdditionalTargetInfoStatus isValid />,
    );

    expect(screen.getByText('Additional target information ok')).toBeVisible();
    expect(screen.getByTitle('Additional target information ok')).toBeVisible();
    expect(container.querySelector('.fulfilled-icon')).toBeVisible();
  });

  it('should render the expected text and icon when not valid', () => {
    const { container } = render(
      <AdditionalTargetInfoStatus />,
    );

    expect(screen.getByText('Review additional target information')).toBeVisible();
    expect(screen.getByTitle('Review additional target information')).toBeVisible();
    expect(container.querySelector('.required-icon')).toBeVisible();
  });

  it('should render the text and icon as disabled when disabled', () => {
    const { container } = render(
      <AdditionalTargetInfoStatus disabled />,
    );

    expect(container.querySelector('.disabled-info')).toBeVisible();
  });
});
