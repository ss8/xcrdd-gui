import { TargetDefaultAFSelection } from 'data/customization/customization.types';
import { CaseModel } from 'data/case/case.types';
// eslint-disable-next-line import/prefer-default-export
export function afSelectionRequired(
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  warrantCase: Partial<CaseModel>,
  caseTargetTag: string, // target identifier as defined in the ontology file
):boolean {
  if (!targetDefaultAFSelection || !warrantCase.services) {
    return true;
  }

  let targetRequiresAFs:boolean = warrantCase.services.length === 0;
  for (let i = 0; i < warrantCase.services.length; i = +1) {
    const service = warrantCase.services[i];
    if (targetDefaultAFSelection[service]) {
      if (targetDefaultAFSelection[service][caseTargetTag] != null) {
        targetRequiresAFs = targetDefaultAFSelection[service][caseTargetTag].selectionRequired ?? true;
        if (targetRequiresAFs === true) break;
      } else {
        targetRequiresAFs = true;
        break;
      }
    } else {
      targetRequiresAFs = true;
      break;
    }
  }

  return targetRequiresAFs;
}
