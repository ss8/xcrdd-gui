import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import cloneDeep from 'lodash.clonedeep';
import { checkProps } from '../../../shared/__test__/testUtils';
import { AdvancedProvisioningWithoutConnect } from './index';
import {
  endeavorTargetsData,
} from './__data__/targetPanel-test-data';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

const defaultProps: any = {
  targets: endeavorTargetsData,
  targetAfDetails: endeavorTargetsData,
  currentStateMode: CONFIG_FORM_MODE.Create,
  handleSubmit: jest.fn(() => {}),
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (setupProps = defaultProps) => {
  return shallow<AdvancedProvisioningWithoutConnect>(<AdvancedProvisioningWithoutConnect {...setupProps} />);
};

test('0. does not throw warning with expected props', () => {
  checkProps(AdvancedProvisioningWithoutConnect, defaultProps);
});

describe('A. test TargetInfoHash update handler', () => {
  test('A1 test adding TargetInfo in target', () => {
    const props: any = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    delete props.targets[0].targetInfoHash;
    const wrapper = setup(props);
    wrapper.setState({ targets: props.targets, selectedTargetId: props.targets[0].id });

    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };
    wrapper.instance().handleTargetInfoUpdate(newTargetInfoHashItem);

    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });

  test('A2 test adding TargetInfo in target that has other targetInfos', () => {
    const props = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    props.targets[0].targetInfoHash = [
      {
        targetInfoType: 'MTXCFCI',
        targetInfos: [{
          ifid: null,
          options: [{ key: 'KEY', value: 'VALUE' }],
        },
        ],
      },
    ];
    const wrapper = setup(props);
    wrapper.setState({ targets: props.targets, selectedTargetId: props.targets[0].id });

    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };
    wrapper.instance().handleTargetInfoUpdate(newTargetInfoHashItem);

    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });

  test('A3 test changing TargetInfo in target', () => {
    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };

    const props = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    props.targets[0].targetInfoHash = [cloneDeep(newTargetInfoHashItem)];

    const wrapper = setup(props);
    wrapper.setState({ targets: props.targets, selectedTargetId: props.targets[0].id });

    newTargetInfoHashItem.targetInfos[0].options[0].value = 'SMS_SIGTRAN';
    wrapper.instance().handleTargetInfoUpdate(cloneDeep(newTargetInfoHashItem));

    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });
});

describe('B test GPTARGETINFO validation ', () => {
  let wrapper: any;
  let thisDescribeProps: any;

  beforeEach(() => {
    const caseType = 'ALL';
    thisDescribeProps = {
      ...defaultProps,
      isSurvOptCombined: false,
      warrantCase: { type: caseType },
      caseType,
    };
  });

  test('B1  no error message when GPTARGETINFO has required fields ', () => {
    wrapper = setup(thisDescribeProps);

    wrapper.setState({ targets: endeavorTargetsData });

    const errors: string[] = [];
    wrapper.instance().validTargetCCC(errors);
    expect(errors.length).toBe(0);
  });

  test('B2  has error messages when GPTARGETINFO GUI does not have required fields ', () => {
    wrapper = setup(thisDescribeProps);

    const missingRequiredDataTargets = [
      ...endeavorTargetsData,
    ];
    const imsiTargetIndex = missingRequiredDataTargets.findIndex((target) => (target.primaryType === 'IMSI'));
    const imsiTarget = cloneDeep(missingRequiredDataTargets[imsiTargetIndex]);
    imsiTarget.targetInfoHash[0].targetInfos[0].options[0].value = 'SMS_SIGTRAN';
    missingRequiredDataTargets[imsiTargetIndex] = imsiTarget;

    const msisdnTargetIndex = missingRequiredDataTargets.findIndex((target) => (target.primaryType === 'MSISDN'));
    const msisdnTarget = cloneDeep(missingRequiredDataTargets[msisdnTargetIndex]);
    msisdnTarget.targetInfoHash[0].targetInfos[0].options[0].value = 'GTP';
    missingRequiredDataTargets[msisdnTargetIndex] = msisdnTarget;

    wrapper.setState({ targets: missingRequiredDataTargets });

    const errors: string[] = [];
    wrapper.instance().validTargetCCC(errors);
    expect(errors.length).toBe(2);
    expect(errors[0]).toMatch(/(IMSI|MSISDN)/); expect(errors[0]).toContain('required');
    expect(errors[1]).toMatch(/(IMSI|MSISDN)/); expect(errors[1]).toContain('required');
  });

  test('B3  has error messages GPTARGETINFO PVOIP is only allowed when caseType is ALL ', () => {
    const caseType = 'CD';
    const props = {
      ...thisDescribeProps,
      warrantCase: { type: caseType },
      caseType,
    };
    wrapper = setup(props);

    const targets = [
      ...endeavorTargetsData,
    ];

    const msisdnTargetIndex = targets.findIndex((target) => (target.primaryType === 'MSISDN'));
    const msisdnTarget = cloneDeep(targets[msisdnTargetIndex]);
    msisdnTarget.targetInfoHash[0].targetInfos[0].options[0].value = 'PVOIP_GTP';
    targets[msisdnTargetIndex] = msisdnTarget;

    wrapper.setState({ targets });

    const errors: string[] = [];
    wrapper.instance().validTargetCCC(errors);
    expect(errors.length).toBe(1);
    expect(errors[0]).toContain('MSISDN'); expect(errors[0]).toContain('PVOIP'); expect(errors[0]).toContain('Data and Content');
  });

  test.only('B4  has error messages when there is no TargetInfoHash for AFs that has x3TargetAssociation', () => {
    wrapper = setup(thisDescribeProps);

    const targets: any = [
      ...endeavorTargetsData,
    ];
    const imsiTargetIndex = targets.findIndex((target: any) => (target.primaryType === 'IMSI'));
    const imsiTarget = cloneDeep(targets[imsiTargetIndex]);
    imsiTarget.targetInfoHash = undefined;
    targets[imsiTargetIndex] = imsiTarget;

    const msisdnTargetIndex = targets.findIndex((target: any) => (target.primaryType === 'MSISDN'));
    const msisdnTarget = cloneDeep(targets[msisdnTargetIndex]);
    msisdnTarget.targetInfoHash = undefined;
    targets[msisdnTargetIndex] = msisdnTarget;

    wrapper.setState({ targets });

    const errors: string[] = [];
    wrapper.instance().validTargetCCC(errors);

    expect(errors.length).toBe(2);
    expect(errors[0]).toMatch(/(IMSI|MSISDN)/); expect(errors[0]).toContain('required');
    expect(errors[1]).toMatch(/(IMSI|MSISDN)/); expect(errors[1]).toContain('required');
  });
});
