import React, { Fragment } from 'react';
import cloneDeep from 'lodash.clonedeep';
import { getAFDataInterfaces, getAllAFGroups } from 'data/accessFunction/accessFunction.actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TargetModel } from 'data/target/target.types';
import { TargetDefaultAFSelection } from 'data/customization/customization.types';
import { CaseModel } from 'data/case/case.types';
import AccessFunctions from './accessFunctions';
import { AppToaster, showErrors } from '../../../shared/toaster';
import { applyFieldValidators } from '../../../shared/form/form-validation';
import TargetSummaryOfTargetPanel from './targetSummaryOfTargetPanel';
import TargetCCCInfo from '../targetInfo/target-ccc-info';
import TargetCCCInfoNortelMtxCCR from '../targetInfo/target-ccc-nortel-mtx-ccr';
import AxsgpTargetInfo from '../targetInfo/axsgp-target-info';
import { afSelectionRequired } from './targetPanel.utils';

import { getTargetShouldHaveAFAssociationList, isAdditionalTargetInfoValid, validateTargetCCC } from '.';
import {
  TargetInfoHashItem, TARGET_INFO_TYPE,
} from '../targetInfo/target-info-defs';
import { getTargetInfoHashItem, createDefaultTargetInfoHashItem, trackTargetInfoUpdate } from '../targetInfo/util';

import './targetPanel.scss';

const dismissErrors = (errorkey: string) => {
  AppToaster.dismiss(errorkey);
};

const validTargetCCC = (targets: any, warrantCase:any, targetAfDetails:any, errors: any[],
  isSurvOptCombined: any) => {
  return validateTargetCCC(targets, targetAfDetails, isSurvOptCombined,
    warrantCase.type, errors, `Case ${warrantCase.caseIndex}`);
};

const validTargets = (caseId:string, targets: any, targetFields: any, errors: Array<string>) => {
  let targetValidationResult = true;
  Object.entries(targetFields).forEach(([key, value]: any, index: number) => {
    const { primaryType, primaryValue } = targets[index];
    const { validation } = value.subFields.primaryValue;
    const field = value.subFields.primaryValue;

    if (validation.required && key === primaryType) {
      // Since this panel is for create at this time, there is no lastSavedValue.
      const lastSavedValue = null;
      const fieldValidationResult = applyFieldValidators(field.validation, primaryValue, lastSavedValue);
      targetValidationResult = targetValidationResult && fieldValidationResult.isValid;
      if (!fieldValidationResult.isValid && fieldValidationResult.errorMessage) {
        errors.push(`${caseId}${key}: ${fieldValidationResult.errorMessage}`);
      } else if (!fieldValidationResult.isValid && fieldValidationResult.errorMessage === '') {
        const validators = field.validators ? field.validators : [];
        validators.forEach((validator:any) => {
          errors.push(`${caseId}${key}: ${validator.errorMessage}`);
        });
      }
    }
    if (errors.length > 0) { showErrors(errors); }
  });
  return targetValidationResult;
};

export const handleAFandTargetCCCValidation = (
  targets: any,
  targetAfDetails: any,
  errorKey:string,
  warrantCase: CaseModel,
  errors: any[],
  isSurvOptCombined: any,
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
) => {
  let targetShouldHaveAFAssociation: string[] = [];
  if (targetAfDetails != null) {
    targetShouldHaveAFAssociation = getTargetShouldHaveAFAssociationList(targetAfDetails);
  }
  // targetAfDetails that do not need an AFWT has the target.afs.afIds[i].bAFAssociation = false.
  // The GUI used bAFAssociation to determine whether to display the AF for a target.
  // Since all AFs are selected by default, targets that do not need an AFWT will have AFs.
  // Those AFs will be removed right before the warrant is to be created.
  const afCounts = targets.map((target: any) => {
    // For targets that do not need AF association, fakes AF count.
    const value = target.secondaryType ? `${target.primaryType}+${target.secondaryType}` : target.primaryType;
    if (targetShouldHaveAFAssociation.includes(value) === false) return 100;

    // If target does not require AF, fake AF count
    if (afSelectionRequired(targetDefaultAFSelection, warrantCase, target.primaryType) === false) return 100;

    // If the target value is empty dont validate AFs
    if (!target.primaryValue) return -1;
    const afCount = target.afs && target.afs.afIds ? target.afs.afIds.length : 0;
    const afGCount = target.afs && target.afs.afGroups ? target.afs.afGroups.length : 0;
    const afawGCount = target.afs && target.afs.afAutowireGroups
      ? target.afs.afAutowireGroups.length : 0;
    const afTCount = target.afs && target.afs.afAutowireTypes
      ? target.afs.afAutowireTypes.length : 0;
    return afCount + afGCount + afTCount + afawGCount;
  });

  const caseId = warrantCase.name;

  if (afCounts.includes(0)) {
    errors.push(`Error:${caseId} each target needs to have at least one AF selected`);
  }

  validTargetCCC(targets, warrantCase, targetAfDetails, errors, isSurvOptCombined);
  dismissErrors(errorKey);
  return true;
};

// handle cases targets validation
export const handleTargetsValidation = (
  targets: any,
  targetAfDetails: any,
  errorKey:string, warrantCase:any,
  errors: any[], targetFields:any,
  isSurvOptCombined: any,
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
) => {
  let targetValidationResult = true;
  const caseId = warrantCase.name;
  targetValidationResult = targetValidationResult &&
  validTargets(caseId, targets, targetFields, errors);
  targetValidationResult = targetValidationResult &&
  handleAFandTargetCCCValidation(targets, targetAfDetails, errorKey,
    warrantCase, errors, isSurvOptCombined, targetDefaultAFSelection);
  return targetValidationResult;
};

export class TargetPanel extends React.Component< any, any> {
  private errorKey = 'TargetPanel';

  private targetCCCSummaryRef: any;

  private targetSummaryRef: any;

  private targetCCCInfoRef: any;

  constructor(props: any) {
    super(props);

    const { targets, hideEmptyTargets, currentStateMode } = props;

    const selectedTarget = TargetPanel.selectTarget(targets, hideEmptyTargets);

    this.state = {
      fieldDirty: false,
      currentStateMode,
      targets,
      selectedAfs: selectedTarget,
      selectedTargetId: selectedTarget.id || '',
      selectedTargetType: selectedTarget.primaryType || '',
      selectedTargetAf: null,
    };
  }

  static selectTarget(targets: TargetModel[], hideEmptyTargets: boolean):Partial<TargetModel> {

    if (!targets?.length) {
      return {};
    }

    if (!hideEmptyTargets) {
      return targets[0];
    }

    return targets.find((target) => target.primaryValue != null && target.primaryValue !== '') || {};
  }

  componentDidMount = async () => {
    const { selectedDF } = this.props;
    const response = await this.props.getAllAFGroups(selectedDF === undefined ? '' : selectedDF);
    this.setState({ allAfGroups: response.action.payload.data.data });
  }

  isTargetsArrayIdEqual = (targetsA:any, targetsB:any) => {
    const targetsIDarrayA = targetsA.map((target: any) => target.id);
    const targetsIDarrayB = targetsB.map((target: any) => target.id);
    const intersection = targetsIDarrayA.filter((e: any) => targetsIDarrayB.indexOf(e) === -1);
    return intersection.length > 0;
  }

  componentDidUpdate = (prevProps: any) => {
    if (this.isTargetsArrayIdEqual(prevProps.targets, this.props.targets)) {
      dismissErrors(this.errorKey);

      const { targets, hideEmptyTargets, currentStateMode } = this.props;

      const selectedTarget = TargetPanel.selectTarget(targets, hideEmptyTargets);

      this.setState({
        fieldDirty: false,
        currentStateMode,
        targets,
        selectedAfs: selectedTarget,
        selectedTargetId: selectedTarget.id || '',
        selectedTargetType: selectedTarget.primaryType || '',
        selectedTargetAf: null,
      });
    }
  }

  newFieldUnselectTargetAf = (selectedTargetAf: any, newFields: any) => {
    const curselectedTargetAf =
      newFields.afs.afIds.filter((af:any) => af.id === selectedTargetAf.id);
    return curselectedTargetAf.length === 0;
  }

  handleAccessFunctionUpdate = (fieldDirty: boolean, newFields: any) => {
    dismissErrors(this.errorKey);
    const newTargets = JSON.parse(JSON.stringify(this.state.targets));
    const index = newTargets.findIndex((nt: any) => nt.id === newFields.id);
    newTargets[index] = { ...newFields };
    if (this.state.selectedTargetAf &&
    this.newFieldUnselectTargetAf(this.state.selectedTargetAf, newFields)) {
      this.setState({
        fieldDirty, selectedAfs: newFields, targets: newTargets, selectedTargetAf: null,
      }, () => {
        this.props.handleSubmit(this.state.targets);
      });
    } else {
      this.setState({ fieldDirty, selectedAfs: newFields, targets: newTargets }, () => {
        this.props.handleSubmit(this.state.targets);
      });
    }
  }

  handleTargetcccClick = async (afDetails: any) => {
    const { selectedDF } = this.props;
    const response = await getAFDataInterfaces(selectedDF === undefined ? '' : selectedDF, afDetails.id).payload;

    // *** create default TargetInfo if there isn't one exist.
    // look for the target
    let newTargetInfoHashItem:TargetInfoHashItem | null = null;
    const targetIndex: number = this.state.targets.findIndex((target: any) =>
      (target.id === this.state.selectedAfs.id));
    // look for the target info for the AF type
    const targetInfoHashIndex: number = this.state.targets[targetIndex].targetInfoHash ?
      this.state.targets[targetIndex].targetInfoHash.findIndex((targetInfoHashItem: any) =>
        (targetInfoHashItem.targetInfoType === afDetails.x3TargetAssociations)) : -1;
    // if there isn't a target info for the AF type, create a default one
    if (targetInfoHashIndex === -1 ||
    !this.state.targets[targetIndex].targetInfoHash[targetInfoHashIndex].targetInfos ||
    this.state.targets[targetIndex].targetInfoHash[targetInfoHashIndex].targetInfos.length <= 0) {
      const { selectedCaseIndex, cases } = this.props;
      const caseServices = selectedCaseIndex >= 0 && cases ? cases[selectedCaseIndex].services : null;
      newTargetInfoHashItem =
      createDefaultTargetInfoHashItem(afDetails, afDetails.x3TargetAssociations, this.state.selectedTargetType,
        caseServices);
    }

    let newTargets : any[] | null = null;
    // if there is a new default target info, add to the corresponding target
    if (newTargetInfoHashItem) {
      newTargets = [...this.state.targets];
      newTargets[targetIndex] = {
        ...this.state.targets[targetIndex],
        targetInfoHash: !this.state.targets[targetIndex].targetInfoHash ? [newTargetInfoHashItem] :
          [...this.state.targets[targetIndex].targetInfoHash, newTargetInfoHashItem],
      };
      // Add to change tracker
      trackTargetInfoUpdate(this.props.changeTracker, newTargetInfoHashItem,
        this.props.warrantCase?.id, this.state.selectedTargetId, null);
    }

    const newState = {
      fieldDirty: newTargetInfoHashItem === null ? this.state.fieldDirty : true,
      selectedAfs: newTargetInfoHashItem === null || newTargets == null ?
        this.state.selectedAfs : newTargets[targetIndex],
      targets: newTargetInfoHashItem === null || newTargets == null ?
        this.state.targets : newTargets,
      selectedTargetAf: afDetails,
      afDIDetails: response.data.data,
    };

    // this.setState({ selectedTargetAf: afDetails, afDIDetails: response.data.data });
    this.setState(newState, () => {
      if (newTargetInfoHashItem != null) {
        this.props.handleSubmit(this.state.targets);
      }
    });
  }

  handleTargetcccUpdate = (fieldDirty: boolean, fields: any, isValid: boolean) => {
    const { selectedTargetAf } = this.state;
    const options: any = [];
    Object.keys(fields).forEach((key: any) => {
      const keyValue = { key, value: fields[key] };
      options.push(keyValue);
    });

    const afCCCSettings = {
      afids: [selectedTargetAf.id],
      isValid,
      options,
      autowired: false,
    };
    const afCCC = {
      afType: selectedTargetAf.type,
      afSettings: [afCCCSettings],
    };
    const selectedAfsFromState = { ...this.state.selectedAfs };
    if (selectedAfsFromState && selectedAfsFromState.afs
    && selectedAfsFromState.afs.afccc && selectedAfsFromState.afs.afccc.length !== 0) {
      const afCCCInfo = selectedAfsFromState.afs.afccc
        .find((af: any) => af.afType === selectedTargetAf.type);
      if (afCCCInfo) {
        const targetCCCSettings = afCCCInfo && afCCCInfo.afSettings;
        if (targetCCCSettings) {
          const targetCCC = afCCCInfo.afSettings.length !== 0
            ? afCCCInfo.afSettings.find((af: any) => af.afids[0] === selectedTargetAf.id)
            : undefined;

          if (targetCCC) {
            targetCCC.options = Object.assign([], afCCCSettings.options);
            targetCCC.isValid = afCCCSettings.isValid;
          } else { afCCCInfo.afSettings.push(afCCCSettings); }
        } else {
          afCCCInfo.afSettings = [afCCCSettings];
        }
      } else { selectedAfsFromState.afs.afccc.push(afCCC); }
    } else {
      selectedAfsFromState.afs.afccc.push(afCCC);
    }

    const preValueTarget = this.state.targets
      .find((nt: any) => nt.id === this.state.selectedTargetId);
    const preValue = preValueTarget && preValueTarget.afs && preValueTarget.afs.afccc
      ? preValueTarget.afs.afccc : undefined;
    const newTargets = JSON.parse(JSON.stringify(this.state.targets));
    const index = newTargets.findIndex((nt: any) => nt.id === this.state.selectedTargetId);
    newTargets[index] = { ...selectedAfsFromState };

    this.setState({ fieldDirty, selectedAfs: selectedAfsFromState, targets: newTargets }, () => {
      this.props.handleSubmit(this.state.targets);
    });
    // Add to change tracker
    if (this.props.changeTracker && this.state.selectedTargetId && selectedTargetAf.type) {
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.state.selectedTargetId}.afs.afccc.${selectedTargetAf.type}`,
        afCCC, null, preValue);
    }
  }

  handleTargetInfoUpdate = (newTargetInfoHashItem: TargetInfoHashItem) => {
    const index = this.state.targets
      .findIndex((nt: any) => nt.id === this.state.selectedTargetId);

    const preValueTarget = this.state.targets[index];
    let preValue: TargetInfoHashItem | null = null;

    const newTargets = [...this.state.targets];
    newTargets[index] = cloneDeep(preValueTarget);
    let bfound = false;
    if (newTargets[index].targetInfoHash) {
      newTargets[index].targetInfoHash.some((targetInfoHashItem:TargetInfoHashItem, i: number) => {
        if (targetInfoHashItem.targetInfoType === newTargetInfoHashItem.targetInfoType) {
          preValue = newTargets[index].targetInfoHash[i];
          newTargets[index].targetInfoHash[i] = newTargetInfoHashItem;
          bfound = true;
          return true;
        }
        return false;
      });
    }
    if (bfound === false) {
      if (newTargets[index].targetInfoHash) {
        newTargets[index].targetInfoHash.push(newTargetInfoHashItem);
      } else {
        newTargets[index].targetInfoHash = [newTargetInfoHashItem];
      }
    }
    // Add to change tracker
    trackTargetInfoUpdate(
      this.props.changeTracker,
      newTargetInfoHashItem,
      this.props.warrantCase?.id,
      this.state.selectedTargetId,
      preValue,
    );

    this.setState({ fieldDirty: true, selectedAfs: newTargets[index], targets: newTargets },
      () => {
        this.props.handleSubmit(this.state.targets);
      });
  }

  isAccessFunctionTargetDetailValid = (afDetails: any): boolean => {
    const {
      warrantCase,
    } = this.props;

    const {
      targets,
      selectedAfs,
    } = this.state;

    const isSurvOptCombined = warrantCase?.options?.circuitSwitchedVoiceCombined;
    const target = targets.find((entry: any) => (entry.id === selectedAfs.id));
    const allSelectedAfCCC = target.afs.afccc;

    const result = isAdditionalTargetInfoValid(
      afDetails,
      [afDetails],
      allSelectedAfCCC,
      target,
      isSurvOptCombined ?? false,
      warrantCase.type,
      [],
      '',
    );

    return result;
  }

  handleTargetClick = (target:any) => { // handle target item click
    this.setState({
      selectedAfs: target,
      selectedTargetId: target.id,
      selectedTargetType: target.primaryType,
      selectedTargetAf: undefined,
    });
  }

  render() {
    const labelValue = this.state.selectedTargetAf && this.state.selectedTargetAf.name
      ? `Additional Target Info for ${this.state.selectedTargetAf.name}` : 'Additional Target Info';
    const isContent = this.props.warrantCase
    && (this.props.warrantCase.type === 'CC' || this.props.warrantCase.type === 'ALL');
    return (
      <Fragment>
        <div className="targetContainer">
          <div className="targetContainerDiv">
            <TargetSummaryOfTargetPanel
              ref={(ref: any) => { this.targetSummaryRef = ref; }}
              targets={this.props.targets}
              currentStateMode={this.state.currentStateMode}
              selectedTarget={this.state.selectedAfs}
              targetFields={this.props.targetFields}
              caseDetailsFieldUpdateHandler={this.props.caseDetailsFieldUpdateHandler}
              shouldTargetsUpdate={this.props.shouldTargetsUpdate}
              warrantCase={this.props.warrantCase}
              changeTracker={this.props.changeTracker}
              handleClick={this.handleTargetClick}
              hideEmptyTargets={this.props.hideEmptyTargets}
            />
          </div>
          <div className="targetContainerDiv">
            <AccessFunctions
              selectedAfs={this.state.selectedAfs}
              currentStateMode={this.state.currentStateMode}
              allAfGroups={this.state.allAfGroups}
              warrantCase={this.props.warrantCase}
              targetId={this.state.selectedTargetId}
              afDetails={this.state.selectedTargetAf}
              accessFunctionUpdateSet={this.handleAccessFunctionUpdate}
              targetAfDetails={this.props.targetAfDetails}
              targetType={this.state.selectedTargetType}
              drvirtual={this.props.drvirtual}
              onTargetcccClick={this.handleTargetcccClick}
              isAccessFunctionTargetDetailValid={this.isAccessFunctionTargetDetailValid}
            />
          </div>
          { this.state.selectedTargetAf && (
          <div className="targetContainerDiv additional-target-info">
            <div className="section-header">
              {labelValue}
            </div>
            <p className="instruction-text">
              Enter the required target information for Provisioning
            </p>
            <div className="additional-target-info-forms">
              {AxsgpTargetInfo.isSupported(this.state.selectedTargetAf) && (
              <AxsgpTargetInfo
                ref={(ref: any) => { this.targetCCCInfoRef = ref; }}
                afDetails={this.state.selectedTargetAf}
                targetType={this.state.selectedTargetType}
                handleUpdate={this.handleTargetInfoUpdate}
                targetInfoHashItem={AxsgpTargetInfo.getTargetInfoHashItem(
                  this.state.selectedAfs,
                )}
              />
              )}
              {TargetCCCInfo.isSupportedAFType(this.state.selectedTargetAf) && (
              <Fragment>
                <TargetCCCInfo
                  ref={(ref: any) => { this.targetCCCInfoRef = ref; }}
                  currentStateMode={this.state.currentStateMode}
                  targetId={this.state.selectedTargetId}
                  caseId={this.props.warrantCase.id}
                  afDetails={this.state.selectedTargetAf}
                  targetType={this.state.selectedTargetType}
                  handleUpdate={this.handleTargetcccUpdate}
                  selectedAfs={this.state.selectedAfs}
                  afDIDetails={this.state.afDIDetails}
                  afid={this.state.selectedTargetAf ?
                    this.state.selectedTargetAf.id : undefined}
                />

                { isContent && this.state.selectedAfs && this.state.selectedAfs.afs &&
                  this.state.selectedTargetAf.x3TargetAssociations &&
                  this.state.selectedTargetAf.x3TargetAssociations != null
                  ? (
                    <TargetCCCInfoNortelMtxCCR
                      name={this.state.selectedTargetAf.id}
                      ref={(ref: any) => { this.targetCCCSummaryRef = ref; }}
                      selectedTargetAf={this.state.selectedTargetAf}
                      isSurvOptCombined={this.props.warrantCase.options
                        && this.props.warrantCase.options.circuitSwitchedVoiceCombined}
                      handleFieldUpdate={this.handleTargetInfoUpdate}
                      targetInfoHashItem={getTargetInfoHashItem(this.state.selectedAfs,
                        TARGET_INFO_TYPE.MTXCFCI)}
                      currentStateMode={this.state.currentStateMode}
                      targetId={this.state.selectedTargetId}
                      afDetails={this.state.selectedTargetAf}
                      caseId={this.props.warrantCase.id}
                      afid={this.state.selectedTargetAf ?
                        this.state.selectedTargetAf.id : undefined}
                    />
                  )
                  : <Fragment /> }
              </Fragment>
              )}
            </div>
          </div>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllAFGroups,
}, dispatch);

const mapStateToProps = ({
  cases: { cases, selectedIndex: selectedCaseIndex },
  warrants: {
    customizationconfig: { targetDefaultAFSelection },
  },
}: any) => ({
  cases,
  selectedCaseIndex,
  targetDefaultAFSelection,
});

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(TargetPanel);
