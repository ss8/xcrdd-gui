import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { AgGridReact } from '@ag-grid-community/react';
import { FormGroup } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { getAllAFGroups, getAFDataInterfaces } from 'data/accessFunction/accessFunction.actions';
import { TargetDefaultAFSelection } from 'data/customization/customization.types';
import { WARRANT_PRIVILEGE } from '../../warrants/warrant-enums';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import AccessFunctions from './accessFunctions';
import { AppToaster, showErrors } from '../../../shared/toaster';
import TargetCCCInfo from '../targetInfo/target-ccc-info';
import TargetCCCInfoNortelMtxCCR from '../targetInfo/target-ccc-nortel-mtx-ccr';
import AxsgpTargetInfo from '../targetInfo/axsgp-target-info';
import { ROWSELECTION } from '../../../shared/commonGrid/grid-enums';
import { TargetInfo, TargetInfoHashItem, TARGET_INFO_TYPE } from '../targetInfo/target-info-defs';
import {
  getTargetInfoHashItem, createDefaultTargetInfoHashItem, trackTargetInfoUpdate, errorMessages, getMessage,
} from '../targetInfo/util';
import './targetPanel.scss';
import { afSelectionRequired } from './targetPanel.utils';

interface Props {
  children: never[],
  isOpen: boolean,
  currentStateMode: string,
  handleClose: () => void,
  handleSubmit: (targets: any) => void,
  targets: any,
  drvirtual: any,
  cfType: any,
  targetAfDetails: any,
  selectedDF?: string,
  privileges?: string[],
  caseType: string, // case type = ALL, CD, CC
  isSurvOptCombined? : boolean,
  warrantCase: any,
  changeTracker?: any,
  targetFields: any,
  targetDefaultAFSelection?: TargetDefaultAFSelection,
}

const CCCHasRequiredFields = (afSetting: any, targetPrimaryType: string,
  errors : any[], errorMsgPrefix:string|null = null) => {
  let isValid = false;
  if (afSetting && afSetting.options && afSetting.options.length !== 0) {
    const { options } = afSetting;
    // the !! is to ensure isValid is a boolean
    isValid = !!(options[0].value && options[0].value !== '');
  }
  if (isValid === false) {
    errors.push(getMessage(errorMessages.REQUIRED_TARGET, errorMsgPrefix, targetPrimaryType));
  }
  return isValid;
};

const isValidTargetInfo = (targetInfoHash: TargetInfoHashItem[], afDetails: any, isSurvOptCombined: any,
  caseType: string | undefined | null, targetPrimaryType: string,
  errors : any[], errorMsgPrefix:string|null = null) => {
  if (!targetInfoHash || targetInfoHash.length === 0) {
    // MTXCFCI is only applicable when caseType has content.  So even when targetInfoHash is empty, it is still valid
    // if X3TargetAssociation is MTXCFCI and caseType does not have content
    if (afDetails.x3TargetAssociations === TARGET_INFO_TYPE.MTXCFCI && caseType !== 'CC' && caseType !== 'ALL') {
      return true;
    }
    errors.push(getMessage(errorMessages.REQUIRED_TARGET,
      errorMsgPrefix, targetPrimaryType));
    return false;
  }

  const isValid = targetInfoHash.every((targetInfoHashItem: TargetInfoHashItem) => {
    // for each targetInfoHashItem, iterate through its targetInfos list and check if each targetInfo is valid
    if (targetInfoHashItem.targetInfos && targetInfoHashItem.targetInfos.length > 0) {
      return targetInfoHashItem.targetInfos.every((targetInfo: TargetInfo): boolean => {
        if (targetInfoHashItem.targetInfoType === TARGET_INFO_TYPE.MTXCFCI) {
          if ((caseType === 'CC' || caseType === 'ALL') &&
              isValidMtxCfCi(targetInfo.options, isSurvOptCombined) === false) {
            errors.push(getMessage(errorMessages.REQUIRED_TARGET,
              errorMsgPrefix, targetPrimaryType));
            return false;
          }
        } else if (targetInfoHashItem.targetInfoType === TARGET_INFO_TYPE.GPTARGETINFO) {
          if (AxsgpTargetInfo.isValidTargetInfo(targetInfo, afDetails.model, caseType, targetPrimaryType,
            errors, errorMsgPrefix) === false) {
            return false;
          }
        }
        return true;
      });
    }
    return false;
  });
  return isValid;
};

export const isValidMtxCfCi = (options: any, isSurvOptCombined: any) => {
  const switched = options.find((item: any) => item.key === 'SWITCHED');
  if (!switched) return false;

  if (switched.value.toUpperCase() === 'YES') {
    const cCC1DN = options.find((item: any) => item.key === 'CCC1DN');
    if (!cCC1DN || !cCC1DN.value) return false;
    if (!isSurvOptCombined) {
      const cCC2DN = options.find((item: any) => item.key === 'CCC2DN');
      if (!cCC2DN || !cCC2DN.value) return false;
    }
  } else if (switched.value.toUpperCase() === 'NO') {
    const tRKGRP1 = options.find((item: any) => item.key === 'TRKGRP1');
    const tRKMEM1 = options.find((item: any) => item.key === 'TRKMEM1');
    if (!tRKGRP1 || !tRKGRP1.value) return false;
    if (!tRKMEM1 || !tRKMEM1.value) return false;
    if (!isSurvOptCombined) {
      const tRKGRP2 = options.find((item: any) => item.key === 'TRKGRP2');
      const tRKMEM2 = options.find((item: any) => item.key === 'TRKMEM2');
      if (!tRKGRP2 || !tRKGRP2.value) return false;
      if (!tRKMEM2 || !tRKMEM2.value) return false;
    }
  }
  return true;
};

export function isAdditionalTargetInfoValid(
  af: any,
  allAfIds: any,
  allSelectedAfCCC: any,
  target: any,
  isSurvOptCombined: boolean,
  caseType: string | undefined | null,
  errors: any[],
  errorMsgPrefix: string | null = null,
): boolean {
  let result = true;

  const currAfDetails = allAfIds.find((afd: any) => afd.id === af.id);

  if (currAfDetails && currAfDetails.x3TargetAssociations != null) {
    if (currAfDetails.x3TargetAssociations === TARGET_INFO_TYPE.MTXCFCI) {
      const selectedAfCCC = allSelectedAfCCC?.find((item: any) => item.afType === currAfDetails.type);

      const afSettings = selectedAfCCC?.afSettings?.find((afd: any) => afd.afids[0] === af.id);

      result = CCCHasRequiredFields(afSettings, target.primaryType, errors, errorMsgPrefix);
    }

    if (result) {
      result = isValidTargetInfo(
        target.targetInfoHash,
        currAfDetails,
        isSurvOptCombined,
        caseType,
        target.primaryType,
        errors,
        errorMsgPrefix,
      );
    }
  }

  return result;
}

export const validateTargetCCC = (targets: any, targetAfDetails: any, isSurvOptCombined: any,
  caseType: string | undefined | null, errors : any[], errorMsgPrefix:string|null = null) => {
  const validCCC = targets.filter((tar:any) => tar.primaryValue !== undefined
  && tar.primaryValue !== null && tar.primaryValue !== '').map((target: any) => {
    const currTargetAfDetails = targetAfDetails
      .find((item: any) => item.primaryType === target.primaryType);
    const allAfIds = currTargetAfDetails && currTargetAfDetails.afs
      && currTargetAfDetails.afs.afIds
      ? currTargetAfDetails.afs.afIds : [];

    if (allAfIds.length === 0) return true;
    if (target.afs && target.afs.afIds) {
      const selectedAfs = target.afs.afIds;
      const allSelectedAfCCC = target.afs.afccc;
      let result = true;
      for (let i = 0; i < selectedAfs.length; i += 1) {
        const af = selectedAfs[i];
        result = isAdditionalTargetInfoValid(
          af,
          allAfIds,
          allSelectedAfCCC,
          target,
          isSurvOptCombined,
          caseType,
          errors,
          errorMsgPrefix,
        );

        if (result === false) break;
      }
      return result;
    }
    return true;
  });
  if (validCCC.includes(false)) return false;
  return true;
};

export const getTargetShouldHaveAFAssociationList = (targetAFDetails:any) => {
  // get a list of target types that should have AFWT
  // example: [ 'MIN', 'MDN+LOGIN', 'IMEI'] where 'primaryType+secondaryTYpe'
  const targetShouldHaveAFAssociation:string[] = [];
  targetAFDetails.forEach((targetAFDetail:any) => {
    if (targetAFDetail.afs && targetAFDetail.afs.afIds) {
      if (targetAFDetail.afs.afIds.some((afId:any) => (afId.bAFAssociation))) {
        const value = targetAFDetail.secondaryType ? `${targetAFDetail.primaryType}+${targetAFDetail.secondaryType}` : targetAFDetail.primaryType;
        targetShouldHaveAFAssociation.push(value);
      }
    }
  });
  return targetShouldHaveAFAssociation;
};

export class AdvancedProvisioningWithoutConnect extends React.Component<Props, any> {
  private errorKey = 'AdvProvisioning';

  private targetCCCSummaryRef: any;

  private targetCCCInfoRef: any;

  public static defaultProps = {
    children: undefined,
  }

  constructor(props: any) {
    super(props);
    this.state = {
      fieldDirty: false,
      currentStateMode: this.props.currentStateMode,
      isContent: true,
    };
  }

  async componentDidMount() {
    const { selectedDF } = this.props;
    const allAfGroups = await getAllAFGroups(selectedDF === undefined ? '' : selectedDF).payload;

    const allAfGroupsState = allAfGroups?.data?.data || [];

    this.setState({ allAfGroups: allAfGroupsState });
  }

  // TODO: Keep this implementation or mount/unmount the component on
  // isOpen/isClose  // TODO: Use a different method
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if (nextProps.isOpen) {
      this.dismissErrors();
      const targets = Object.assign([], nextProps.targets
        .filter((af: any) => af.primaryValue !== undefined && af.primaryValue.trim() !== '')
        .map((item: any) => {
          const displayName = nextProps.targetFields && nextProps.targetFields[item.primaryType]
          && nextProps.targetFields[item.primaryType].subFields
          && nextProps.targetFields[item.primaryType].subFields.primaryValue
          && nextProps.targetFields[item.primaryType].subFields.primaryValue.label
          && nextProps.targetFields[item.primaryType].subFields.primaryValue.label !== ''
            ? nextProps.targetFields[item.primaryType].subFields.primaryValue.label
            : item.primaryType;
          return {
            id: item.id, type: item.primaryType, value: item.primaryValue, displayName,
          };
        }));

      this.setState({
        fieldDirty: false,
        currentStateMode: nextProps.currentStateMode,
        targets: JSON.parse(JSON.stringify(nextProps.targets)),
        targetRowData: targets,
        selectedAfs: targets !== undefined && targets.length !== 0
          ? JSON.parse(JSON.stringify(targets[0])) : [],
        selectedTargetId: targets !== undefined && targets.length !== 0
          ? targets[0].id : '',
        selectedTargetType: targets !== undefined && targets.length !== 0
          ? targets[0].primaryType : '',
        isContent: true,
      });
    }
  }

  shouldComponentUpdate(nextProps: any) {
    return nextProps.isOpen || this.props.isOpen !== nextProps.isOpen;
  }

  handleSubmit = () => {
    const targetShouldHaveAFAssociation =
      getTargetShouldHaveAFAssociationList(this.props.targetAfDetails);
    const afCounts = this.state.targets.map((target: any) => {
      // For targets that do not need AF association, fakes AF count.
      const value = target.secondaryType ? `${target.primaryType}+${target.secondaryType}` : target.primaryType;
      if (targetShouldHaveAFAssociation.includes(value) === false) return 100;

      // If target does not require AF, fake AF count
      const { targetDefaultAFSelection, warrantCase } = this.props;
      if (afSelectionRequired(targetDefaultAFSelection, warrantCase, target.primaryType) === false) return 100;

      // For targets need AF association, make sure there are at least one AF
      const afCount = target.afs && target.afs.afIds ? target.afs.afIds.length : 0;
      const afGCount = target.afs && target.afs.afGroups ? target.afs.afGroups.length : 0;
      const afawGCount = target.afs && target.afs.afAutowireGroups
        ? target.afs.afAutowireGroups.length : 0;
      const afTCount = target.afs && target.afs.afAutowireTypes
        ? target.afs.afAutowireTypes.length : 0;
      return afCount + afGCount + afTCount + afawGCount;
    });
    const errors: any = [];

    this.validTargetCCC(errors);
    if (afCounts.includes(0)) {
      errors.push('Error: Each target needs to have at least one AF selected');
    }
    if (errors.length !== 0) {
      showErrors(errors);
    } else {
      this.dismissErrors();
      this.props.handleSubmit(this.state.targets);
    }
  }

  validTargetCCC = (errors : any[]) => {
    const { targets } = this.state;
    validateTargetCCC(targets, this.props.targetAfDetails,
      this.props.isSurvOptCombined, this.props.caseType, errors);
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    const readOnly = (stateMode !== CONFIG_FORM_MODE.Edit);
    this.setState({ readOnly, currentStateMode: stateMode });
  }

  getTitle = (currentStateMode: CONFIG_FORM_MODE) => {
    return (currentStateMode === CONFIG_FORM_MODE.Edit) ? 'Edit Advanced Provisioning' : 'View Advanced Provisioning';
  }

  gridReady = (event: any) => {
    event.api.sizeColumnsToFit();
    event.api.forEachNode((node: any) => (node.rowIndex ? 0 : node.setSelected(true)));
  }

  private targetGridColDefs: any = [
    {
      headerName: 'Id', field: 'id', hide: true, suppressMenu: true,
    },
    {
      headerName: 'Target Type', field: 'displayName', suppressMenu: true,
    },
    {
      headerName: 'Target Value', field: 'value', suppressMenu: true, resizable: true,
    },
  ];

  handleSelection = (e: any) => {
    const selectedRow = e.api.getSelectedRows();
    const selectedAfs = JSON.parse(JSON.stringify(
      this.state.targets
        .filter((item: any) => {
          return (item.id === selectedRow[0].id);
        }),
    ));
    this.setState({
      selectedAfs: selectedAfs[0],
      selectedTargetId: selectedAfs[0].id,
      selectedTargetType: selectedAfs[0].primaryType,
      selectedTargetAf: undefined,
    });
  }

  handleAccessFunctionUpdate = (fieldDirty: boolean, newFields: any) => {
    this.dismissErrors();
    const newTargets = JSON.parse(JSON.stringify(this.state.targets));
    const index = newTargets.findIndex((nt: any) => nt.id === newFields.id);
    newTargets[index] = { ...newFields };

    this.setState({ fieldDirty, selectedAfs: newFields, targets: newTargets });
  }

  handleTargetcccClick = async (afDetails: any) => {
    const { selectedDF } = this.props;
    const response = await getAFDataInterfaces(selectedDF === undefined ? '' : selectedDF, afDetails.id).payload;

    // *** create default TargetInfo if there isn't one exist.
    // look for the target
    let newTargetInfoHashItem:TargetInfoHashItem | null = null;
    const targetIndex: number = this.state.targets.findIndex((target: any) =>
      (target.id === this.state.selectedAfs.id));
    // look for the target info for the AF type
    const targetInfoHashIndex: number = this.state.targets[targetIndex].targetInfoHash ?
      this.state.targets[targetIndex].targetInfoHash.findIndex((targetInfoHashItem: any) =>
        (targetInfoHashItem.targetInfoType === afDetails.x3TargetAssociations)) : -1;
    // if there isn't a target info for the AF type, create a default one
    if (targetInfoHashIndex === -1 ||
      !this.state.targets[targetIndex].targetInfoHash[targetInfoHashIndex].targetInfos ||
      this.state.targets[targetIndex].targetInfoHash[targetInfoHashIndex].targetInfos.length <= 0) {
      newTargetInfoHashItem =
        createDefaultTargetInfoHashItem(afDetails, afDetails.x3TargetAssociations, this.state.selectedTargetType,
          this.props.warrantCase.services);
    }

    let newTargets : any[] | null = null;
    // if there is a new default target info, add to the corresponding target
    if (newTargetInfoHashItem) {
      newTargets = [...this.state.targets];
      newTargets[targetIndex] = {
        ...this.state.targets[targetIndex],
        targetInfoHash: !this.state.targets[targetIndex].targetInfoHash ? [newTargetInfoHashItem] :
          [...this.state.targets[targetIndex].targetInfoHash, newTargetInfoHashItem],
      };

      // Add to change tracker
      trackTargetInfoUpdate(this.props.changeTracker, newTargetInfoHashItem,
        this.props.warrantCase?.id, this.state.selectedTargetId, null);
    }

    const newState = {
      fieldDirty: newTargetInfoHashItem == null ? this.state.fieldDirty : true,
      selectedAfs: newTargetInfoHashItem == null || newTargets == null ?
        this.state.selectedAfs : newTargets[targetIndex],
      targets: newTargetInfoHashItem == null || newTargets == null ?
        this.state.targets : newTargets,
      selectedTargetAf: afDetails,
      afDIDetails: response.data.data,
    };

    this.setState(newState);
  }

  handleTargetcccUpdate = (fieldDirty: boolean, fields: any, isValid: boolean) => {
    const { selectedTargetAf } = this.state;
    const options: any = [];
    Object.keys(fields).forEach((key: any) => {
      const keyValue = { key, value: fields[key] };
      options.push(keyValue);
    });

    const afCCCSettings = {
      afids: [selectedTargetAf.id],
      isValid,
      options,
      autowired: false,
    };
    const afCCC = {
      afType: selectedTargetAf.type,
      afSettings: [afCCCSettings],
    };
    const selectedAfsFromState = { ...this.state.selectedAfs };
    if (selectedAfsFromState && selectedAfsFromState.afs
      && selectedAfsFromState.afs.afccc && selectedAfsFromState.afs.afccc.length !== 0) {
      const afCCCInfo = selectedAfsFromState.afs.afccc
        .find((af: any) => af.afType === selectedTargetAf.type);
      if (afCCCInfo) {
        const targetCCCSettings = afCCCInfo && afCCCInfo.afSettings;
        if (targetCCCSettings) {
          const targetCCC = afCCCInfo.afSettings.length !== 0
            ? afCCCInfo.afSettings.find((af: any) => af.afids[0] === selectedTargetAf.id)
            : undefined;

          if (targetCCC) {
            targetCCC.options = Object.assign([], afCCCSettings.options);
            targetCCC.isValid = afCCCSettings.isValid;
          } else { afCCCInfo.afSettings.push(afCCCSettings); }
        } else {
          afCCCInfo.afSettings = [afCCCSettings];
        }
      } else { selectedAfsFromState.afs.afccc.push(afCCC); }
    } else {
      selectedAfsFromState.afs.afccc.push(afCCC);
    }
    const preValueTarget = this.state.targets
      .find((nt: any) => nt.id === this.state.selectedTargetId);
    const preValue = preValueTarget && preValueTarget.afs && preValueTarget.afs.afccc
      ? preValueTarget.afs.afccc : undefined;
    const newTargets = JSON.parse(JSON.stringify(this.state.targets));
    const index = newTargets.findIndex((nt: any) => nt.id === this.state.selectedTargetId);
    newTargets[index] = { ...selectedAfsFromState };

    this.setState({ fieldDirty, selectedAfs: selectedAfsFromState, targets: newTargets });

    // Add to change tracker
    if (this.props.changeTracker && this.state.selectedTargetId && selectedTargetAf.type) {
      this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${this.state.selectedTargetId}.afs.afccc.${selectedTargetAf.type}`,
        afCCC, null, preValue);
    }
  }

  isAccessFunctionTargetDetailValid = (afDetails: any): boolean => {
    const {
      warrantCase,
    } = this.props;

    const {
      targets,
      selectedAfs,
    } = this.state;

    const isSurvOptCombined = warrantCase?.options?.circuitSwitchedVoiceCombined;
    const target = targets.find((entry: any) => (entry.id === selectedAfs.id));
    const allSelectedAfCCC = target.afs.afccc;

    const result = isAdditionalTargetInfoValid(
      afDetails,
      [afDetails],
      allSelectedAfCCC,
      target,
      isSurvOptCombined ?? false,
      warrantCase.type,
      [],
      '',
    );

    return result;
  }

  handleClose = () => {
    this.dismissErrors();
    this.props.handleClose();
  }

  dismissErrors = () => {
    AppToaster.dismiss(this.errorKey);
  }

  handleTargetInfoUpdate = (newTargetInfoHashItem: TargetInfoHashItem) => {
    const index = this.state.targets
      .findIndex((nt: any) => nt.id === this.state.selectedTargetId);
    const preValueTarget = this.state.targets[index];
    const preValue : TargetInfoHashItem = preValueTarget?.targetInfoHash?.find(
      (aTargetInfoHashItem:TargetInfoHashItem) =>
        (aTargetInfoHashItem.targetInfoType === newTargetInfoHashItem.targetInfoType));

    const newTargets = [...this.state.targets];
    newTargets[index] = cloneDeep(preValueTarget);
    let bfound = false;
    if (newTargets[index].targetInfoHash) {
      newTargets[index].targetInfoHash.some((targetInfoHashItem:TargetInfoHashItem, i: number) => {
        if (targetInfoHashItem.targetInfoType === newTargetInfoHashItem.targetInfoType) {
          newTargets[index].targetInfoHash[i] = newTargetInfoHashItem;
          bfound = true;
          return true;
        }
        return false;
      });
    }
    if (bfound === false) {
      if (newTargets[index].targetInfoHash) {
        newTargets[index].targetInfoHash.push(newTargetInfoHashItem);
      } else {
        newTargets[index].targetInfoHash = [newTargetInfoHashItem];
      }
    }
    // Add to change tracker
    trackTargetInfoUpdate(this.props.changeTracker, newTargetInfoHashItem,
      this.props.warrantCase?.id, this.state.selectedTargetId, preValue);

    this.setState({ fieldDirty: true, selectedAfs: newTargets[index], targets: newTargets });
  }

  render() {
    const { privileges } = this.props;
    const hasPrivilegesToEdit =
      privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.Edit);
    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: this.state.currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: this.props.isOpen,
      width: '1000px',
      height: 'auto',
      doNotDisplayEdit: true,
    };

    const labelValue = this.state.selectedTargetAf && this.state.selectedTargetAf.name
      ? `Target Info - ${this.state.selectedTargetAf.name}` : 'Target Info';

    const isContent = this.props.caseType === 'ALL' || this.props.caseType === 'CC';
    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          {this.props.isOpen ? (
            <div style={{
              margin: '10px 0 15px',
              minHeight: '200px',
            }}
            >
              <div className="row">
                <div className="adv-prov-target-div">
                  <div className="ag-theme-balham">
                    <AgGridReact
                      onGridReady={this.gridReady}
                      columnDefs={this.targetGridColDefs}
                      domLayout="autoHeight"
                      rowData={this.state.targetRowData}
                      suppressCellSelection
                      context={this}
                      rowSelection={ROWSELECTION.SINGLE}
                      onRowSelected={this.handleSelection}
                    />
                  </div>
                </div>
                <div className="adv-prov-div">
                  <div style={{
                    overflowY: 'auto',
                    minHeight: '180px',
                    maxHeight: '500px',
                  }}
                  >
                    <AccessFunctions
                      selectedAfs={this.state.selectedAfs}
                      currentStateMode={this.state.currentStateMode}
                      allAfGroups={this.state.allAfGroups}
                      targetId={this.state.selectedTargetId}
                      accessFunctionUpdateSet={this.handleAccessFunctionUpdate}
                      targetAfDetails={this.props.targetAfDetails}
                      targetType={this.state.selectedTargetType}
                      drvirtual={this.props.drvirtual}
                      onTargetcccClick={this.handleTargetcccClick}
                      warrantCase={this.props.warrantCase}
                      isAccessFunctionTargetDetailValid={this.isAccessFunctionTargetDetailValid}
                    />

                  </div>
                </div>
                { // selectedTargetAf is not null when CCC is pressed
                this.state.selectedTargetAf && this.state.selectedTargetType && (
                <div className="adv-prov-div">
                  <div style={{
                    overflowY: 'auto',
                    minHeight: '180px',
                    maxHeight: '500px',
                  }}
                  >
                    <FormGroup label={labelValue}>
                      {
                     AxsgpTargetInfo.isSupported(this.state.selectedTargetAf) ? (
                       <AxsgpTargetInfo
                         ref={(ref: any) => { this.targetCCCInfoRef = ref; }}
                         afDetails={this.state.selectedTargetAf}
                         targetType={this.state.selectedTargetType}
                         handleUpdate={this.handleTargetInfoUpdate}
                         targetInfoHashItem={AxsgpTargetInfo.getTargetInfoHashItem(
                           this.state.selectedAfs,
                         )}
                       />
                     ) : <Fragment />
                    }
                      {TargetCCCInfo.isSupportedAFType(this.state.selectedTargetAf) ? (
                        <Fragment>
                          <TargetCCCInfo
                            ref={(ref: any) => { this.targetCCCInfoRef = ref; }}
                            currentStateMode={this.state.currentStateMode}
                            targetId={this.state.selectedTargetId}
                            afDetails={this.state.selectedTargetAf}
                            targetType={this.state.selectedTargetType}
                            handleUpdate={this.handleTargetcccUpdate}
                            selectedAfs={this.state.selectedAfs}
                            afDIDetails={this.state.afDIDetails}
                            afid={this.state.selectedTargetAf ?
                              this.state.selectedTargetAf.id : undefined}
                          />

                          { isContent && this.state.selectedAfs && this.state.selectedAfs.afs &&
                          this.state.selectedTargetAf.x3TargetAssociations &&
                          this.state.selectedTargetAf.x3TargetAssociations
                          === TARGET_INFO_TYPE.MTXCFCI
                            ? (
                              <TargetCCCInfoNortelMtxCCR
                                name={this.state.selectedTargetAf.id}
                                ref={(ref) => { this.targetCCCSummaryRef = ref; }}
                                selectedTargetAf={this.state.selectedTargetAf}
                                isSurvOptCombined={this.props.isSurvOptCombined}
                                handleFieldUpdate={this.handleTargetInfoUpdate}
                                targetInfoHashItem={getTargetInfoHashItem(this.state.selectedAfs,
                                  TARGET_INFO_TYPE.MTXCFCI)}
                                currentStateMode={this.state.currentStateMode}
                                targetId={this.state.selectedTargetId}
                                afDetails={this.state.selectedTargetAf}
                                caseId={this.props.warrantCase.id}
                                afid={this.state.selectedTargetAf ?
                                  this.state.selectedTargetAf.id : undefined}
                              />
                            )
                            : <Fragment /> }
                        </Fragment>
                      )
                        : <Fragment />}
                    </FormGroup>
                  </div>
                </div>
                )
}
              </div>
            </div>
          )
            : <Fragment />}
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  global: { selectedDF },
  authentication: { privileges },
  warrants: {
    changeTracker,
    customizationconfig: { targetDefaultAFSelection },
  },
  cases: { cases, selectedIndex: selectedCaseIndex },
}: any) => ({
  selectedDF,
  privileges,
  changeTracker,
  cases,
  selectedCaseIndex,
  targetDefaultAFSelection,
});

export default connect(mapStateToProps, null)(AdvancedProvisioningWithoutConnect);
