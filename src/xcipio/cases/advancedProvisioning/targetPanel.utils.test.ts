import cloneDeep from 'lodash.clonedeep';
import { CaseModel } from 'data/case/case.types';
import { mockCustomization } from '__test__/mockCustomization';
import { mockCases } from '__test__/mockCases';
import { TargetDefaultAFSelection } from 'data/customization/customization.types';
import { afSelectionRequired } from './targetPanel.utils';

describe('afSelectionRequired', () => {
  let aCase:Partial<CaseModel>;
  let targetDefaultAFSelection: TargetDefaultAFSelection;
  beforeEach(() => {
    const index = 0;
    aCase = cloneDeep(mockCases[index]);
    if (mockCustomization.targetDefaultAFSelection) {
      targetDefaultAFSelection = cloneDeep(mockCustomization.targetDefaultAFSelection);
    }
  });

  it('should return true if case.services are undefined', () => {
    aCase.services = undefined;
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return true if case.services is empty', () => {
    aCase.services = [];
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return true if targetDefaultAFSelection is undefined', () => {
    expect(afSelectionRequired(undefined, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return true if service not found customization', () => {
    aCase.services = ['5G-DATA'];
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);

    aCase.services = ['VOLTE-4G-VOICE', 'LTE-4G-DATA'];
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return true if target not found customization', () => {
    aCase.services = ['GSMCDMA-2G-VOICE'];
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'MSISDN')).toBe(true);
  });

  it('should return true if case only has one service and it required AF', () => {
    aCase.services = ['GSMCDMA-2G-VOICE'];
    targetDefaultAFSelection['GSMCDMA-2G-VOICE'].CASEIDMDN.selectionRequired = true;
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return false if case only has one service and it does not required AF', () => {
    aCase.services = ['GSMCDMA-2G-VOICE'];
    targetDefaultAFSelection['GSMCDMA-2G-VOICE'].CASEIDMDN.selectionRequired = false;
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(false);
  });

  it('should return true if case has many services and one of the case services required AF', () => {
    aCase.services = ['GSMCDMA-2G-VOICE', 'CDMA2000-3G-DATA'];
    targetDefaultAFSelection['GSMCDMA-2G-VOICE'].CASEIDMDN.selectionRequired = false;
    targetDefaultAFSelection['CDMA2000-3G-DATA"'] = {
      CASEIDMDN: {
        selectionMethods: [{ method: 'selectNone' }],
        selectionRequired: true,
      },
    };
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });

  it('should return true if case has many services and one of the case services does not define selectionRequired', () => {
    aCase.services = ['GSMCDMA-2G-VOICE', 'CDMA2000-3G-DATA'];
    targetDefaultAFSelection['GSMCDMA-2G-VOICE'].CASEIDMDN.selectionRequired = false;
    targetDefaultAFSelection['CDMA2000-3G-DATA"'] = {
      CASEIDMDN: {
        selectionMethods: [{ method: 'selectNone' }],
      },
    };
    expect(afSelectionRequired(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBe(true);
  });
});
