import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import cloneDeep from 'lodash.clonedeep';
import { checkProps } from '../../../shared/__test__/testUtils';
import { TargetPanel } from './targetPanel';
import { endeavorTargetsData } from './__data__/targetPanel-test-data';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

const defaultProps = {
  targets: endeavorTargetsData,
  currentStateMode: CONFIG_FORM_MODE.Create,
  handleSubmit: jest.fn(() => {}),
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (setupProps = defaultProps) => {
  return shallow<TargetPanel>(<TargetPanel {...setupProps} />);
};

test('0. does not throw warning with expected props', () => {
  checkProps(TargetPanel, defaultProps);
});

describe('A. test TargetInfoHash update handler', () => {
  test(' test adding TargetInfo in target', () => {
    const props: any = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    props.targets[0].targetInfoHash = undefined;
    const wrapper = setup(props);

    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };
    wrapper.instance().handleTargetInfoUpdate(newTargetInfoHashItem);
    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });

  test(' test adding TargetInfo in target that has other targetInfos', () => {
    const props = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    props.targets[0].targetInfoHash = [
      {
        targetInfoType: 'MTXCFCI',
        targetInfos: [{
          ifid: null,
          options: [{ key: 'KEY', value: 'VALUE' }],
        },
        ],
      },
    ];
    const wrapper = setup(props);

    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };
    wrapper.instance().handleTargetInfoUpdate(newTargetInfoHashItem);

    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });

  test(' test changing TargetInfo in target', () => {
    const newTargetInfoHashItem = {
      targetInfoType: 'GPTARGETINFO',
      targetInfos: [{
        ifid: null,
        options: [{ key: 'SERVICE', value: 'SMS_PVOIP' }],
      },
      ],
    };

    const props = {
      ...defaultProps,
      targets: [...endeavorTargetsData],
    };
    props.targets[0].targetInfoHash = [cloneDeep(newTargetInfoHashItem)];

    const wrapper = setup(props);

    newTargetInfoHashItem.targetInfos[0].options[0].value = 'SMS_SIGTRAN';
    wrapper.instance().handleTargetInfoUpdate(cloneDeep(newTargetInfoHashItem));

    expect(wrapper.update().state('targets')[0].targetInfoHash).toContainEqual(newTargetInfoHashItem);
  });
});
