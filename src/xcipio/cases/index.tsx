import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllCases,
  getCaseById,
  createCase,
  updateCase,
  patchCase,
  deleteCase,
} from './case-actions';
import { getUserSettings, putUserSettings } from '../../users/users-actions';
import ModuleRoutes from './case-routes';

const Cases = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  authentication: state.authentication,
  cases: state.cases,
  global: state.global,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllCases,
  getCaseById,
  createCase,
  updateCase,
  patchCase,
  deleteCase,
  getUserSettings,
  putUserSettings,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Cases));
