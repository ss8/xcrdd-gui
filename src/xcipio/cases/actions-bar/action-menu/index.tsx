import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button } from '@blueprintjs/core';
import { CASE_PRIVILEGE } from 'xcipio/cases/case-enums';
import WarrantRetryPopup from 'routes/Warrant/components/WarrantRetryPopup';
import { RootState } from 'data/root.reducers';
import CaseActionsHandler from '../../case-actions-handler';
import { COMMON_GRID_ACTION, COMMON_GRID_ACTION_DISPLAY } from '../../../../shared/commonGrid/grid-enums';
import showActionMenuItem from '../../grid/utils';

interface IActionMenuProps {
  selectedCases: any[],
  bArchiveCase: boolean,
  handleClose?: (action: string) => void,
  handleEdit: () => void,
  privileges: string[],
  onReloadCase?: () => void
  isWarrantResumeActionEnabled: boolean,
  isWarrantPauseActionEnabled: boolean,
}
const allActions = [
  COMMON_GRID_ACTION.Start,
  COMMON_GRID_ACTION.Stop,
  COMMON_GRID_ACTION.Reschedule,
  COMMON_GRID_ACTION.Resume,
  COMMON_GRID_ACTION.Pause,
  COMMON_GRID_ACTION.Delete,
  COMMON_GRID_ACTION.Edit,
  COMMON_GRID_ACTION.Reprovision,
];

const ActionMenuBar = ({
  selectedCases,
  handleClose,
  handleEdit,
  privileges,
  onReloadCase,
  isWarrantResumeActionEnabled,
  isWarrantPauseActionEnabled,
}: IActionMenuProps) => {
  const [action, setAction] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [visibleActions, setVisibleActions] = useState([]);
  const [isWarrantRetryPopupOpen, setIsWarrantRetryPopupOpen] = useState(false);
  const onClose = (warrantAction: string) => {
    setAction('');
    setIsOpen(false);
    setIsWarrantRetryPopupOpen(false);
    if (handleClose) {
      handleClose(warrantAction);
    }
  };
  const handleAction = (warrantAction: string) => {
    if (warrantAction === COMMON_GRID_ACTION.Edit) {
      handleEdit();
    } else if (warrantAction === COMMON_GRID_ACTION.Reprovision) {
      setIsWarrantRetryPopupOpen(true);
    } else {
      setAction(warrantAction);
      setIsOpen(true);
    }
  };
  const calculateVisibleActions = () => {
    let nextVisibleActions: any = [];
    nextVisibleActions = allActions.filter((warrantAction: string) => {
      if (!isActionValidBasedOnPrivileges(warrantAction)) return false;
      if (!isActionValidBasedOnNumberOfSelectedCases(warrantAction)) return false;
      return selectedCases.every((warrantCase: any) => {
        return showActionMenuItem(
          warrantAction,
          warrantCase,
          isWarrantResumeActionEnabled,
          isWarrantPauseActionEnabled,
        );
      });
    });
    return nextVisibleActions;
  };

  const isActionValidBasedOnNumberOfSelectedCases = (warrantAction: string): boolean => {
    // these actions are only applicable to one warrantCase
    if (warrantAction === COMMON_GRID_ACTION.Edit
      || warrantAction === COMMON_GRID_ACTION.View
      || warrantAction === COMMON_GRID_ACTION.Reprovision) {
      return (selectedCases?.length === 1);
    }
    return true;
  };

  const isActionValidBasedOnPrivileges = (warrantAction: string) => {
    if (warrantAction === COMMON_GRID_ACTION.Delete && privileges.includes(CASE_PRIVILEGE.Delete)) return true;
    if (privileges.includes(CASE_PRIVILEGE.Edit)) return true;
    return false;
  };

  useEffect(() => {
    const nextVisibleActions = calculateVisibleActions();
    setVisibleActions(nextVisibleActions);
  }, [selectedCases]);

  if (selectedCases.length === 0) {
    return null;
  }

  return (
    <Fragment>
      <div style={{ display: 'flex', float: 'left' }}>
        {visibleActions.map((visibleAction: string, index: number) => {
          return (
            <Button
              key={index}
              data-test={`${visibleAction.toLowerCase()}-button`}
              style={{ paddingTop: '5px', marginLeft: '10px' }}
              text={(COMMON_GRID_ACTION_DISPLAY as any)[visibleAction]}
              onClick={() => handleAction(visibleAction)}
            />
          );
        })}
      </div>
      <CaseActionsHandler
        data-test="update-case-status"
        action={action}
        onClose={onClose.bind(null, action)}
        isOpen={isOpen}
        onReloadCase={onReloadCase}
      />
      <WarrantRetryPopup
        isOpen={isWarrantRetryPopupOpen}
        handleConfirm={onClose.bind(null, COMMON_GRID_ACTION.Reprovision)}
        warrant={selectedCases[0].warrantId}
        isCalledfromCase
        onReload={onReloadCase}
      />
    </Fragment>
  );
};

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Case_bArchiveCase: bArchiveCase = false,
      isWarrantResumeActionEnabled = false,
      isWarrantPauseActionEnabled = false,
    },
  },
  cases: { selectedCases },
  authentication: {
    privileges,
  },
}: RootState) => ({
  bArchiveCase,
  selectedCases,
  privileges,
  isWarrantResumeActionEnabled,
  isWarrantPauseActionEnabled,
});

export default connect(mapStateToProps)(ActionMenuBar);
