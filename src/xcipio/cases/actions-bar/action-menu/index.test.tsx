import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import store from 'data/store';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import { BROADCAST_SELECTED_CASES } from 'xcipio/cases/case-actions';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import AuthActions from 'data/authentication/authentication.types';
import { CaseModel } from 'data/case/case.types';
import { CASE_RESSTATE } from 'xcipio/cases/case-enums';
import { mockCases } from '__test__/mockCases';
import ActionMenuBar from './index';

describe('<ActionMenuBar />', () => {
  let caseEntry: CaseModel;

  beforeEach(() => {
    [caseEntry] = cloneDeep(mockCases);

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: BROADCAST_SELECTED_CASES,
      selectedCases: [caseEntry],
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });
  });

  it('should not show any button if no warrant selected', () => {
    store.dispatch({
      type: BROADCAST_SELECTED_CASES,
      selectedCases: [],
    });

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeNull();
    expect(screen.queryByText('Edit')).toBeNull();
    expect(screen.queryByText('Provisioning Retry')).toBeNull();
  });

  it('should show expected buttons if warrant is active', () => {
    caseEntry.subStatus = CASE_RESSTATE.Active;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeVisible();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is paused', () => {
    caseEntry.subStatus = CASE_RESSTATE.Paused;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeVisible();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is stopped', () => {
    caseEntry.subStatus = CASE_RESSTATE.Stopped;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeVisible();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is expired', () => {
    caseEntry.subStatus = CASE_RESSTATE.Expired;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeVisible();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is pending', () => {
    caseEntry.subStatus = CASE_RESSTATE.PENDING;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Pause button if warrant is active but Pause action is not enabled', () => {
    const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
    discoverySettings.appSettings.forEach((entry) => {
      if (entry.name === 'isWarrantPauseActionEnabled') {
        entry.setting = 'false';
      }
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([discoverySettings]),
    });

    caseEntry.subStatus = CASE_RESSTATE.Active;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Resume button if warrant is paused but resume action is not enabled', () => {
    const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
    discoverySettings.appSettings.forEach((entry) => {
      if (entry.name === 'isWarrantResumeActionEnabled') {
        entry.setting = 'false';
      }
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([discoverySettings]),
    });

    caseEntry.subStatus = CASE_RESSTATE.Paused;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Provisioning Retry and Edit buttons if more than one warrant selected', () => {
    caseEntry.subStatus = CASE_RESSTATE.Active;

    store.dispatch({
      type: BROADCAST_SELECTED_CASES,
      selectedCases: [caseEntry, caseEntry],
    });

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeVisible();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeNull();
    expect(screen.queryByText('Provisioning Retry')).toBeNull();
  });
});
