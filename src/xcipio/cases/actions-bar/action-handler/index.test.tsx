import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { AxiosError } from 'axios';
import { ServerResponse } from 'data/types';
import {
  ActionHandler,
  IActionHandlerProps,
  IActionHandlerStatus,
} from './index';

describe('<ActionHandler />', () => {
  let props: IActionHandlerProps;
  let wrapper: ShallowWrapper<IActionHandlerProps, IActionHandlerStatus, ActionHandler>;

  beforeEach(() => {
    props = {
      isOpen: true,
      selectedCases: [],
      handleAction: jest.fn(),
      onClose: jest.fn(),
    };

    wrapper = shallow<ActionHandler, IActionHandlerProps, IActionHandlerStatus>(
      <ActionHandler {...props} />,
    );
  });

  describe('handleAction()', () => {
    it('should call handleAction from props with case', async () => {
      const warrantCase = { name: 'case' };
      await wrapper.instance().handleAction(warrantCase);
      expect(props.handleAction).toBeCalledWith(warrantCase);
    });

    it('should update counter in state on request success', async () => {
      const warrantCase = { name: 'case' };
      props.handleAction.mockImplementation(() => Promise.resolve());
      await wrapper.instance().handleAction(warrantCase);
      expect(wrapper.instance().state.counter).toEqual(1);
    });

    it('should update errors in state on request failed', async () => {
      const warrantCase = { name: 'case' };
      const error: AxiosError<ServerResponse<void>> = {
        config: {},
        name: 'erro1',
        message: 'some error message',
        isAxiosError: true,
        toJSON: jest.fn(),
        response: {
          status: 500,
          statusText: '',
          headers: [],
          config: {},
          data: {
            data: null,
            metadata: null,
            success: false,
            error: {
              status: 500,
              code: 'INTERNAL_SERVER_ERROR',
              message: 'some server error message',
            },
            errors: [],
            message: '',
            status: '',
          },
        },
      };

      props.handleAction.mockImplementation(() => Promise.reject(error));
      await wrapper.instance().handleAction(warrantCase);
      expect(wrapper.instance().state.errors).toEqual([{
        warrantCase,
        error: 'some server error message',
      }]);
    });
  });
});
