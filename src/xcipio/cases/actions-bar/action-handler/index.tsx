import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  deleteCase,
  caseUpdateProvStatus,
} from '../../case-actions';
import { extractErrorMessage } from '../../../../utils';

import ProgressBarIndicator from '../../../../components/progress-bar-indicator';

export interface IActionHandlerStatus {
  isOpen: boolean,
  counter: number,
  errors: Array<any>,
  inProgress: boolean,
}

export interface IActionHandlerProps {
  isOpen: boolean,
  selectedCases: Array<any>,
  handleAction: any,
  onClose: any
}

export class ActionHandler extends React.Component<IActionHandlerProps, IActionHandlerStatus> {
  constructor(props: any) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      isOpen: false,
      counter: 0,
      errors: [],
      inProgress: false,
    };
  };

  componentDidUpdate() {
    if (this.props.isOpen !== this.state.isOpen) {
      this.setState({ isOpen: this.props.isOpen }, () => {
        this.initProcess();
      });
    }
  }

  initProcess = () => {
    if (this.state.isOpen) {
      this.setState({ inProgress: true }, () => {
        this.handleProcess().then(() => {
          this.setState({ inProgress: false });
        });
      });
    }
  };

  handleProcess = async () => {
    for (
      let index = 0;
      index < this.props.selectedCases.length && this.state.inProgress;
      index += 1
    ) {
      const warrantCase = this.props.selectedCases[index];
      if (warrantCase.autoFormValidation && !warrantCase.autoFormValidation.isValid) {
        this.processError(warrantCase, warrantCase.autoFormValidation.errorMessage);
      } else await this.handleAction(warrantCase);
    }
  };

  handleAction = async (warrantCase: any) => {
    try {
      await this.props.handleAction(warrantCase);
      this.setState({ counter: this.state.counter + 1 });
    } catch (e) {
      this.processError(warrantCase, extractErrorMessage(e));
    }
  };

  processError = (warrantCase: any, error: string) => {
    const { errors } = this.state;
    errors.push({ warrantCase, error });
    this.setState({ errors });
  };

  handleConfirm = () => {
    if (this.state.inProgress) {
      this.setState({ inProgress: false });
    } else {
      this.setState({ ...this.getInitialState() });
      this.props.onClose();
    }
  };

  render() {
    const totalProcessed = this.state.counter + this.state.errors.length;
    const errors = this.state.errors.map((err: any) => {
      return `${err.warrantCase.name}:  ${err.error}`;
    });
    const { isOpen, inProgress, counter } = this.state;
    return (
      <ProgressBarIndicator
        isOpen={isOpen}
        handleConfirm={this.handleConfirm}
        inProgress={inProgress}
        errors={errors}
        counter={counter}
        totalSelected={this.props.selectedCases.length}
        totalProcessed={totalProcessed}
      />
    );
  }
}

const mapStateToProps = ({ global, cases: { selectedCases } }: any) => ({
  global,
  selectedCases,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  caseUpdateProvStatus,
  deleteCase,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ActionHandler);
