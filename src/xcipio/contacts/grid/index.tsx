import React, { Fragment } from 'react';
import '../contacts.scss';
import CommonGrid, { ICommonGridProperties } from '../../../shared/commonGrid';
import { ROWSELECTION } from '../../../shared/commonGrid/grid-enums';

class ContactsGrid extends React.Component<any, any> {
  api = null;

  columnDefs = [
    { headerName: 'Nickname', field: 'nickname' },
    { headerName: 'Name', field: 'name' },
    { headerName: 'Full Name', field: 'fullname' },
    { headerName: 'Organization', field: 'organization' },
    { headerName: 'Phone', field: 'phone' },
    { headerName: 'Pager', field: 'pager' },
    { headerName: 'Mobile', field: 'mobile' },
    { headerName: 'Fax', field: 'fax' },
    { headerName: 'Email', field: 'email' },
    { headerName: 'Address', field: 'address' },
  ]

  state = {
    selection: [],
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
  }

  componentDidMount() {
    this.props.getAllContacts(this.props.global.selectedDF);
  }

  handleCreateNew = () => {
    //  TODO: Implement this
  }

  render() {
    const noResults = (!this.props.contacts || !this.props.contacts.data.length);
    const commonGridProps: ICommonGridProperties = {
      onSelectionChanged: this.handleSelection,
      rowSelection: ROWSELECTION.MULTIPLE,
      enableSorting: true,
      enableFilter: true,
      columnDefs: this.columnDefs,
      rowData: this.props.contacts.data,
      handleCreateNew: this.handleCreateNew,
      createNewText: 'New Contact',
      noResults,
      hasPrivilegeToCreateNew: true,
    };
    return (
      <Fragment>
        <div className="grid-container">
          <CommonGrid {...commonGridProps} />
        </div>
      </Fragment>
    );
  }
}

export default ContactsGrid;
