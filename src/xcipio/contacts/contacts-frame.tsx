import React, { Fragment } from 'react';
import {
  Popover, Button, Position,
} from '@blueprintjs/core';
import ContactsGrid from './grid';
import ActionMenu from './contacts-action-menu';
import './contacts.scss';
import { AppToaster } from '../../shared/toaster';

class ContactsFrame extends React.Component<any, any> {
  handleChange = (e: any) => {
    this.setState({ selectedTab: e });
  }

  handleViewChange = (e: any) => {
    this.setState({ selectedView: e.currentTarget.id });
  }

  componentDidUpdate() {
    const { error } = this.props.contacts;
    if (error) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${error}`,
      });
      this.props.clearErrors();
    }
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/authentication/login');
    }
  }

  render() {
    return (
      <Fragment>
        <div className="">
          <div className="row">
            <div
              className="col-md-12"
              style={{
                marginBottom: '10px', display: 'flex', justifyContent: 'space-between', padding: '35px',
              }}
            >
              <div>
                <Popover
                  minimal
                  content={(
                    <ActionMenu
                      onChange={this.handleViewChange}
                    />
                )}
                  position={Position.BOTTOM_LEFT}
                >
                  <Button intent="primary" rightIcon="caret-down" text="Actions" />
                </Popover>
                <Button
                  onClick={
                    () => { this.props.history.push('/contacts/create'); }
                  }
                  intent="none"
                  style={{ marginLeft: '5px' }}
                  rightIcon="new-object"
                  text="New"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <ContactsGrid {...this.props} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ContactsFrame;
