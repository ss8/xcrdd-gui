import React from 'react';
import DynamicForm from '../../../shared/form';
import contactsCreate from './contacts-create-form.json';
import PopUp from '../../../shared/popup/dialog';

class ContactsCreate extends React.Component<any, any> {
  private ref: any;

  constructor(props: any) {
    super(props);
    this.ref = React.createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    const payload = this.ref.current.getValues();
    const { selectedDF } = this.props.global;
    this.props.createContact(selectedDF, payload).then(() => this.props.history.goBack());
  }

  render() {
    return (
      <PopUp title="Create New Contact" isOpen onSubmit={this.handleSubmit} width="500px" {...this.props}>
        <DynamicForm
          name={contactsCreate.key}
          ref={this.ref}
          fields={contactsCreate.forms.caseNotes.fields}
          layout={contactsCreate.forms.caseNotes.metadata.layout.double}
          colWidth={6}
          onSubmit={this.handleSubmit}
          {...this.props}
        />
      </PopUp>
    );
  }
}

export default ContactsCreate;
