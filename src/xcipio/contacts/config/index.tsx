import React, { Fragment } from 'react';
import { withDialog } from '../../../shared/popup';
import DynamicForm from '../../../shared/form';
import contactsConfig from './contacts-configuration-form.json';

class ContactsConfig extends React.Component<any, any> {
  render() {
    return (
      <Fragment>
        <DynamicForm
          name={contactsConfig.key}
          fields={contactsConfig.forms.contacts.fields}
          layout={contactsConfig.forms.contacts.metadata.layout.double}
          colWidth={6}
          {...this.props}
        />
      </Fragment>
    );
  }
}

export default withDialog(ContactsConfig, { title: 'Configure Contacts' });
