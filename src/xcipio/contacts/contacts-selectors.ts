import { RootState } from 'data/root.reducers';
import { Contact } from 'data/types';

// eslint-disable-next-line import/prefer-default-export
export const getContacts = (state: RootState): Contact[] => state.contacts.contacts;
