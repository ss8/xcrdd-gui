import {
  GET_ALL_CONTACTS_FULFILLED,
  // GET_CONTACTS_BY_ID_FULFILLED,
  // CREATE_CONTACT_FULFILLED,
  // PATCH_CONTACT_FULFILLED,
  // UPDATE_CONTACT_FULFILLED,
  // DELETE_CONTACT_FULFILLED
  CONTACTS_CLEAR_ERRORS,
} from './contacts-actions';

const defaultState = {
  contacts: [],
  selectedIndex: null,
  error: null,
};

const moduleReducer = (state = defaultState, action: any) => {
  if (/CONTACT.*_REJECTED/.test(action.type)) {
    return { ...state, error: action.payload.message };
  }
  switch (action.type) {
    case GET_ALL_CONTACTS_FULFILLED:
      return { ...state, contacts: action.payload.data.data };
    case CONTACTS_CLEAR_ERRORS:
      return { ...state, error: null };
    default:
      return state;
  }
};
export default moduleReducer;
