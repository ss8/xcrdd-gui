import configureStore, { MockStoreCreator } from 'redux-mock-store';
import rootReducer, { RootState } from 'data/root.reducers';
import { Contact } from 'data/types';
import {
  getContacts,
} from './contacts-selectors';
import { GET_ALL_CONTACTS_FULFILLED } from './contacts-actions';

describe('contacts-selectors', () => {
  let contacts: Contact[];
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    contacts = [{
      id: 'id1',
      name: 'Name 1',
    }, {
      id: 'id2',
      name: 'Name 2',
    }];

    mockStore = configureStore<RootState>([]);
  });

  it('should return the list of securityInfos', () => {
    const action = {
      type: GET_ALL_CONTACTS_FULFILLED,
      payload: { data: { data: contacts } },
    };
    store = mockStore(rootReducer(undefined, action));
    expect(getContacts(store.getState())).toEqual(contacts);
  });
});
