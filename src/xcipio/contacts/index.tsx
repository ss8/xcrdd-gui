import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllContacts,
  getContactById,
  createContact,
  updateContact,
  patchContact,
  deleteContact,
  clearErrors,
} from './contacts-actions';
import ModuleRoutes from './contacts-routes';
import './contacts.scss';

const Contacts = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  contacts: state.contacts.contacts,
  authentication: state.authentication,
  global: state.global,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllContacts,
  getContactById,
  createContact,
  updateContact,
  patchContact,
  deleteContact,
  clearErrors,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Contacts));
