import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import ContactsFrame from './contacts-frame';
import ContactsCreate from './create';
import ContactsDetail from './detail';

const moduleRoot = '/contacts';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <ContactsFrame {...props} />}
    />
    <Switch>
      <Route
        exact
        path={`${moduleRoot}/create`}
        render={() => <ContactsCreate {...props} />}
      />
      <Route
        path={`${moduleRoot}/:id(\\d+)`}
        render={() => <ContactsDetail {...props} />}
      />
    </Switch>
  </Fragment>
);

export default ModuleRoutes;
