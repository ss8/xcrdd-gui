import React, { Fragment } from 'react';
import { Button, ButtonGroup } from '@blueprintjs/core';
import { Resizable } from 're-resizable';
import '../contacts.scss';
import DynamicForm from '../../../shared/form';
import contactEdit from './contacts-edit-form.json';

class ContactsDetail extends React.Component<any, any> {
  state = {
    activeTab: 'overview',
    isMaximized: false,
  }

  handleChange = (tab: string) => {
    this.setState({ activeTab: tab });
  }

  getWidth = () => {
    if (this.state.isMaximized) {
      return window.outerWidth - 15; // this.props.containerWidth; // container width
    }
    return '700px';
  }

  render() {
    return (
      <div className="detail-container">
        <Resizable style={{ padding: '20px' }} defaultSize={{ width: this.getWidth(), height: 1600 }}>
          <div className="detail-header">
            <div>
              <ButtonGroup>
                <Button icon="maximize" onClick={() => this.setState({ isMaximized: !this.state.isMaximized })} />
                <Button
                  icon="cross"
                  onClick={() => this.props.history.push('/contacts')}
                />
              </ButtonGroup>
            </div>
          </div>
          <h2>
            <span style={{ color: '#444', top: '100' }}>Detail for Contact {this.props.location.state.displayName}</span>
          </h2>
          <div>
            <Fragment>
              <DynamicForm
                name={this.props.location.pathname}
                fields={contactEdit.forms.caseNotes.fields}
                layout={contactEdit.forms.caseNotes.metadata.layout.double}
                defaults={this.props.location.state}
                colWidth={6}
                {...this.props}
              />
            </Fragment>
          </div>
        </Resizable>
      </div>
    );
  }
}

export default ContactsDetail;
