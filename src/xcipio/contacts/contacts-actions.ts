import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_CONTACTS = 'GET_ALL_CONTACTS';
export const GET_CONTACT_BY_ID = 'GET_CONTACT_BY_ID';
export const UPDATE_CONTACT = 'UPDATE_CONTACT';
export const PATCH_CONTACT = 'PATCH_CONTACT';
export const DELETE_CONTACT = 'DELETE_CONTACT';
export const CREATE_CONTACT = 'CREATE_CONTACT';
export const GET_ALL_CONTACTS_FULFILLED = 'GET_ALL_CONTACTS_FULFILLED';
export const GET_CONTACT_BY_ID_FULFILLED = 'GET_CONTACT_BY_ID_FULFILLED';
export const UPDATE_CONTACT_FULFILLED = 'UPDATE_CONTACT_FULFILLED';
export const PATCH_CONTACT_FULFILLED = 'PATCH_CONTACT_FULFILLED';
export const DELETE_CONTACT_FULFILLED = 'DELETE_CONTACT_FULFILLED';
export const CREATE_CONTACT_FULFILLED = 'CREATE_CONTACT_FULFILLED';
export const CONTACTS_CLEAR_ERRORS = 'CONTACTS_CLEAR_ERRORS';

// action creators
export const getAllContacts = (dfId: string) => ({ type: GET_ALL_CONTACTS, payload: http.get(`/contacts/${dfId}`) });
export const getContactById = (dfId: string, id: number) => ({ type: GET_CONTACT_BY_ID, payload: http.get(`/contacts/${dfId}/${id}`) });
export const createContact = (dfId: string, body: any) => ({ type: CREATE_CONTACT, payload: http.post(`/contacts/${dfId}`, body) });
export const updateContact = (dfId: string, id: number, body: any) => ({ type: UPDATE_CONTACT, payload: http.put(`/contacts/${dfId}/${id}`, body) });
export const patchContact = (dfId: string, id: number, body: any) => ({ type: PATCH_CONTACT, payload: http.patch(`/contacts/${dfId}/${id}`, body) });
export const deleteContact = (dfId: string, id: number) => ({ type: DELETE_CONTACT, payload: http.delete(`/contacts/${dfId}/${id}`) });

//
export const clearErrors = () => ({ type: CONTACTS_CLEAR_ERRORS, payload: null });
