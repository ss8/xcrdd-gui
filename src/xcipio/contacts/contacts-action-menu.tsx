import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';

export default class ActionMenu extends React.Component<any, any> {
  isActive = (item: string) => (item === this.props.activeItem)

  render() {
    return (
      <Menu>
        <MenuItem id="new" onClick={this.props.onChange} active={this.isActive('')} icon="plus" text="New" />
        <MenuItem id="edit" onClick={this.props.onChange} active={this.isActive('')} icon="edit" text="Edit" />
        <MenuItem id="delete" onClick={this.props.onChange} active={this.isActive('')} icon="minus" text="Delete" />
      </Menu>
    );
  }
}
