import configureStore, { MockStoreCreator } from 'redux-mock-store';
import { AnyAction } from 'redux';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import rootReducer, { RootState } from 'data/root.reducers';
import {
  getAlertNotificationPollingTime,
  getAlertNotificationRowNumbers,
  getDiscoveryDeployment,
  getDiscoverySettingLanguage,
  getWarrantDefaultExpireDays,
  isAlertNotificationAuditDiscrepancyEnabled,
  isAlertNotificationBufferingEnabled,
  isAlertNotificationEnabled,
  isAlertNotificationInterceptDynamicIPLookupEnabled,
  isAlertNotificationNewIPEnabled,
  isDiscoverySettingAuditEnabled,
  isWarrantDefaultExpireEnabled,
  shouldAutomaticallyGenerateCaseName,
  shouldSelectAllHi3Interfaces,
} from './discovery-xcipio-selectors';
import { GET_DX_APP_SETTINGS_FULFILLED } from './discovery-xcipio-actions';

describe('discovery-xcipio-selectors', () => {
  let mockStore: MockStoreCreator<RootState>;
  let store: ReturnType<typeof mockStore>;

  beforeEach(() => {
    const action: AnyAction = {
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    };
    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, action));
  });

  it('should return the deployment', () => {
    expect(getDiscoveryDeployment(store.getState())).toEqual('andromeda');
  });

  it('should return the language', () => {
    expect(getDiscoverySettingLanguage(store.getState())).toEqual('en_US');
  });

  it('should return as empty string if language not available', () => {
    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, {}));
    expect(getDiscoverySettingLanguage(store.getState())).toEqual('');
  });

  it('should return if audit is enabled', () => {
    expect(isDiscoverySettingAuditEnabled(store.getState())).toEqual(true);
  });

  it('should return if should automatically generate case name', () => {
    expect(shouldAutomaticallyGenerateCaseName(store.getState())).toEqual(false);
  });

  it('should return true even if audit is not available', () => {
    mockStore = configureStore<RootState>([]);
    store = mockStore(rootReducer(undefined, {}));
    expect(isDiscoverySettingAuditEnabled(store.getState())).toEqual(true);
  });

  it('should return the value for SelectAllHi3Interfaces', () => {
    expect(shouldSelectAllHi3Interfaces(store.getState())).toEqual(true);
  });

  it('should return the value for isWarrantDefaultExpireEnabled', () => {
    expect(isWarrantDefaultExpireEnabled(store.getState())).toBe(true);
  });

  it('should return the value for warrantDefaultExpireDays', () => {
    expect(getWarrantDefaultExpireDays(store.getState())).toEqual('60');
  });

  it('should return the value for isAlertNotificationEnabled', () => {
    expect(isAlertNotificationEnabled(store.getState())).toBe(true);
  });

  it('should return the value for alertNotificationRowNumbers', () => {
    expect(getAlertNotificationRowNumbers(store.getState())).toEqual('10');
  });

  it('should return the value for alertNotificationPollingTime', () => {
    expect(getAlertNotificationPollingTime(store.getState())).toEqual('30');
  });

  it('should return the value for isAlertNotificationNewIPEnabled', () => {
    expect(isAlertNotificationNewIPEnabled(store.getState())).toBe(true);
  });

  it('should return the value for isAlertNotificationBufferingEnabled', () => {
    expect(isAlertNotificationBufferingEnabled(store.getState())).toBe(true);
  });

  it('should return the value for isAlertNotificationAuditDiscrepancyEnabled', () => {
    expect(isAlertNotificationAuditDiscrepancyEnabled(store.getState())).toBe(true);
  });

  it('should return the value for isAlertNotificationInterceptDynamicIPLookupEnabled', () => {
    expect(isAlertNotificationInterceptDynamicIPLookupEnabled(store.getState())).toBe(true);
  });
});
