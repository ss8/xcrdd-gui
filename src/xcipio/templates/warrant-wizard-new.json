{
  "version": "1.0.0",
  "name": "Default",
  "key": "wizardWarrantTemplate",
  "case": {
    "name": "Case Default",
    "key": "wizardCaseTemplate",
    "general": {
      "metadata": {
        "layout": {
          "rows": [
            [
              "$empty",
              "$empty",
              "$empty",
              "includeInWarrant"
            ],
            [
              "liId",
              "entryDateTime",
              "device",
              "services"
            ],
            [
              "startDateTime",
              "stopDateTime",
              "timeZone",
              "$empty"
            ],
            [
              "templateUsed",
              "type",
              "cfId",
              "interface"
            ],
            [
              "optionDetails",
              "interfaceDetails",
              "$empty",
              "$empty"
            ]
          ]
        }
      },
      "fields": {
        "liId": {
          "label": "Case Name",
          "type": "text",
          "validation": {
            "required": true
          }
        },
        "startDateTime": {
          "label": "Start Time",
          "icon": "time",
          "type": "datetime",
          "timePrecision": "minute"
        },
        "stopDateTime": {
          "label": "Stop Time",
          "icon": "time",
          "type": "datetime",
          "validation": {
            "required": true
          },
          "timePrecision": "minute"
        },
        "timeZone": {
          "label": "Time Zone",
          "type": "timezone",
          "disabled": true
        },
        "entryDateTime": {
          "label": "Entry Time",
          "type": "datetime",
          "disabled": true
        },
        "device": {
          "label": "Device",
          "type": "text",
          "readOnly": true
        },
        "services": {
          "label": "Services",
          "type": "multiselectTypeahead",
          "readOnly": true,
          "options": []
        },
        "type": {
          "label": "Intercept Type",
          "type": "select",
          "readOnlyIfOneOption": false,
          "options": [
            {
              "label": "Select",
              "value": ""
            },
            {
              "label": "Data Only",
              "value": "CD"
            },
            {
              "label": "Content Only",
              "value": "CC"
            },
            {
              "label": "Data and Content",
              "value": "ALL"
            }
          ],
          "validation": {
            "required": true
          }
        },
        "cfId": {
          "label": "CF",
          "type": "select",
          "readOnlyIfOneOption": true,
          "options": [],
          "validation": {
            "required": true
          }
        },
        "templateUsed": {
          "label": "Case Options Template",
          "type": "select",
          "readOnlyIfOneOption": false,
          "options": [],
          "validation": {
            "required": true
          }
        },
        "optionDetails": {
          "type": "detailsButton",
          "initial": "Case Options",
          "className": "case-options"
        },
        "interface": {
          "label": "Interfaces",
          "type": "autoGrowText",
          "initial": "",
          "renderAsEmptyIfHidden": true
        },
        "interfaceDetails": {
          "type": "detailsButton",
          "initial": "Details",
          "className": "case-interface"
        },
        "includeInWarrant": {
          "label": "Include Case In Warrant",
          "type": "checkbox",
          "initial": true,
          "className": "include-case"
        }
      }
    }
  },
  "general": {
    "metadata": {
      "layout": {
        "rows": [
          [
            "name",
            "receivedDateTime",
            "startDateTime",
            "stopDateTime",
            "timeZone"
          ],
          [
            "judge",
            "region",
            "province",
            "city",
            "contact"
          ],
          [
            "dxOwner",
            "lea",
            "caseType",
            "caseOptTemplateUsed",
            "attachments"
          ],
          [
            "department",
            "caseOptionDetails",
            "$empty",
            "$empty",
            "$empty"
          ],
          [
            "comments"
          ]
        ]
      }
    },
    "fields": {
      "name": {
        "label": "Warrant Name",
        "type": "text",
        "validation": {
          "required": true,
          "validators": [
            {
              "type": "regexp",
              "regexp": "^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?/]{1,32}$",
              "errorMessage": "Must be between 1-32 characters.  Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter."
            },
            {
              "type": "length",
              "length": 32,
              "errorMessage": "Exceeds maximum length, 32.  Please re-enter."
            }
          ]
        }
      },
      "receivedDateTime": {
        "label": "Received Time",
        "icon": "time",
        "type": "datetime",
        "timePrecision": "minute"
      },
      "startDateTime": {
        "label": "Start Time",
        "icon": "time",
        "type": "datetime",
        "timePrecision": "minute"
      },
      "stopDateTime": {
        "label": "Stop Time",
        "icon": "time",
        "type": "datetime",
        "validation": {
          "required": true
        },
        "timePrecision": "minute"
      },
      "timeZone": {
        "label": "Time Zone",
        "type": "timezone",
        "validation": {
          "required": true
        }
      },
      "judge": {
        "label": "Judge",
        "type": "text",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 24,
              "errorMessage": "Exceeds maximum length, 24.  Please re-enter."
            }
          ]
        }
      },
      "region": {
        "label": "Region",
        "type": "text",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 32,
              "errorMessage": "Exceeds maximum length, 32.  Please re-enter."
            }
          ]
        }
      },
      "city": {
        "label": "City",
        "type": "text",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 32,
              "errorMessage": "Exceeds maximum length, 32.  Please re-enter."
            }
          ]
        }
      },
      "contact": {
        "label": "Contact",
        "type": "select",
        "readOnlyIfOneOption": false,
        "options": [],
        "validation": {
          "required": false
        }
      },
      "dxOwner": {
        "label": "Visibility",
        "type": "select",
        "readOnlyIfOneOption": false,
        "options": []
      },
      "attachments": {
        "initial": "Attachments",
        "type": "detailsButton"
      },
      "comments": {
        "label": "Comments",
        "type": "text",
        "validation": {
          "required": false,
          "validators": [
            {
              "type": "length",
              "length": 256,
              "errorMessage": "Exceeds maximum length, 256.  Please re-enter."
            }
          ]
        }
      },
      "lea": {
        "label": "LEA",
        "type": "select",
        "readOnlyIfOneOption": false,
        "options": [],
        "validation": {
          "required": true
        }
      },
      "caseType": {
        "label": "Intercept Type",
        "type": "select",
        "readOnlyIfOneOption": false,
        "options": [
          {
            "label": "Select",
            "value": ""
          },
          {
            "label": "Data Only",
            "value": "CD"
          },
          {
            "label": "Content Only",
            "value": "CC"
          },
          {
            "label": "Data and Content",
            "value": "ALL"
          }
        ],
        "validation": {
          "required": true
        }
      },
      "caseOptTemplateUsed": {
        "label": "Case Options Template",
        "type": "select",
        "readOnlyIfOneOption": false,
        "options": [],
        "validation": {
          "required": true
        }
      },
      "province": {
        "label": "Province",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 32,
              "errorMessage": "Exceeds maximum length, 32.  Please re-enter."
            }
          ]
        }
      },
      "department": {
        "label": "Department",
        "type": "text",
        "initial": "",
        "validation": {
          "validators": [
            {
              "type": "length",
              "length": 32,
              "errorMessage": "Exceeds maximum length, 32.  Please re-enter."
            }
          ]
        }
      },
      "caseOptionDetails": {
        "type": "detailsButton",
        "initial": "Case Options"
      }
    }
  }
}