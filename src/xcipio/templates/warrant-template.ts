import newTemplate from './warrant-new.json';
import newWizardTemplate from './warrant-wizard-new.json';

const DefaultTemplate = { name: newTemplate.name, template: newTemplate, id: 'default' };
const DefaultWizardTemplate = { name: newWizardTemplate.name, template: newWizardTemplate, id: 'default' };

interface TemplateObject {
  name: string,
  template: Record<string, unknown>,
  id: string,
}

export default class WarrantTemplate {
  static CATEGORY_WARRANT = 'warrants';

  static CATEGORY_WARRANT_WIZARD = 'warrants-wizard';

  // id to name mapping
  templateIdToNameMap: any = {};

  // templates for warrant classic
  warrantTemplates: any = [];

  // templates for warrant wizard
  warrantWizardTemplates: any = [];

  constructor(templates: any) {
    this.initializeTemplateDataStructure(templates);
  }

  initializeTemplateDataStructure = (rawTemplates: any) => {
    rawTemplates.forEach((rawTemplate: any) => {
      if ((rawTemplate.category === WarrantTemplate.CATEGORY_WARRANT) ||
          (rawTemplate.category === WarrantTemplate.CATEGORY_WARRANT_WIZARD)) {
        this.templateIdToNameMap[rawTemplate.id] = rawTemplate.name;

        let templates = this.warrantTemplates;
        if (rawTemplate.category === WarrantTemplate.CATEGORY_WARRANT_WIZARD) {
          templates = this.warrantWizardTemplates;
        }

        const newTemplateObject: TemplateObject = {
          name: rawTemplate.name,
          template: JSON.parse(rawTemplate.template),
          id: rawTemplate.id,
        };

        if (rawTemplate.categoryDefault) {
          templates.unshift(newTemplateObject);
        } else {
          templates.push(newTemplateObject);
        }
      }
    });

    if (this.warrantTemplates.length === 0) {
      this.warrantTemplates = [DefaultTemplate];
    }

    if (this.warrantWizardTemplates.length === 0) {
      this.warrantWizardTemplates = [DefaultWizardTemplate];
    }
  }

  getTemplateList = (category: string) => {
    let templates = this.warrantTemplates;
    if (category === WarrantTemplate.CATEGORY_WARRANT_WIZARD) {
      templates = this.warrantWizardTemplates;
    }
    return templates;
  }

  getByName(name: string, category: string) {
    const templates = this.getTemplateList(category);
    return templates.filter((template: any) => template.name === name)[0];
  }

  getById(id: string, category: string) {
    const templateName = this.templateIdToNameMap[id];
    const templates = this.getTemplateList(category);

    if (!id) return templates[0];

    if (!templateName) {
      console.log(`Error missing ${category} template with name ${templateName} with id '${id}'`);
      return templates[0];
    }

    return templates.filter((template: any) => template.name === templateName)[0];
  }

  getDropdownOptions(category: string) {
    const templates = this.getTemplateList(category);
    return templates.map((template: any, i: number) => ({
      value: template.id,
      label: template.name,
    }));
  }
}
