import cloneDeep from 'lodash.clonedeep';
import { FormTemplate, FormTemplateOption, FormTemplateSection } from 'data/template/template.types';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { CaseModel } from 'data/case/case.types';
import { TEMPORARY_CASE_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import Templates from './warrant-template';
import { checkTimeValidationInComputerTimezone } from '../../shared/constants/checkTimeValidation';
import CaseCreateOptions from '../cases/options/case-options.json';

export function getFormTemplateForCase(
  templates: any, templateUsed: string, category: string,
) {
  const warrantTemplates = new Templates(templates);
  const formTemplate = cloneDeep(
    warrantTemplates.getById(templateUsed, category),
  );
  const caseFormTemplate = formTemplate && formTemplate.template && formTemplate.template.case ?
    formTemplate.template.case : [];

  return caseFormTemplate;
}

export function initializeWarrantCase(
  formTemplate: any,
  warrantCase: any, // this is the warrant case ID
  changeTracker: ChangeTracker | null,
  showCaseOptions: any,
  showHI23Interface: any,
  templates: any,
  cases: any,
  serviceOptions: FormTemplateOption[],
  mode: string,
  caseInstance?: CaseModel,
  handleRemoveCase?: ()=> void,
): FormTemplate {
  formTemplate.key = `Key_${new Date().getTime()}`;
  if (changeTracker && !changeTracker.isNewCase(warrantCase.id)) {
    if (formTemplate.general.fields.services) {
      formTemplate.general.fields.services.disabled = true;
    }
    // Dates were already converted to computer timezone when loaded
    if (checkTimeValidationInComputerTimezone(warrantCase.startDateTime, warrantCase.stopDateTime)) {
      formTemplate.general.fields.startDateTime.disabled = true;
    }
  }

  // If there is only one case, no need to show the includeInWarrant checkbox
  // because a warrant should have at least one case.
  if (cases.length === 1 && formTemplate.general.fields.includeInWarrant) {
    formTemplate.general.fields.includeInWarrant.hide = 'true';
  }

  setServiceOptions(formTemplate, serviceOptions);
  setCaseOptions(formTemplate, showCaseOptions, templates);
  setInterfaceDetails(formTemplate, showHI23Interface);

  if (mode === CONFIG_FORM_MODE.Create) {
    setTemplateFieldReadOnly(formTemplate, 'services');

  } else if (mode === CONFIG_FORM_MODE.Edit) {
    if (!caseInstance?.id.startsWith(TEMPORARY_CASE_ID_PREFIX)) {
      setStatusFieldToTemplate(formTemplate, 'status', caseInstance?.status || '');
      setStatusFieldToTemplate(formTemplate, 'subStatus', caseInstance?.subStatus || '');
      setTemplateFieldReadOnly(formTemplate, 'liId');
    }

    setTemplateFieldReadOnly(formTemplate, 'services');
    setDeleteButtonToEditingCase(formTemplate, cases.length, caseInstance, handleRemoveCase);

  } else if (mode === CONFIG_FORM_MODE.View) {
    setTemplateFieldHide(formTemplate, 'includeInWarrant');
    setStatusFieldToTemplate(formTemplate, 'status', caseInstance?.status || '');
    setStatusFieldToTemplate(formTemplate, 'subStatus', caseInstance?.subStatus || '');
  }

  return formTemplate;
}

function setServiceOptions(formTemplate: FormTemplate, serviceOptions: FormTemplateOption[]): void {
  (formTemplate.general as FormTemplateSection).fields.services.options?.push(...serviceOptions);
}

function setCaseOptions(formTemplate: any, showCaseOptions: any, templates: any) {
  if (formTemplate.general.fields.templateUsed) {
    formTemplate.general.fields.templateUsed.options
      = getAllCaseOptionTemplates(templates);
  }
  if (formTemplate.general.fields.optionDetails) {
    formTemplate.general.fields.optionDetails.handleClick = showCaseOptions;
  }
}

function setInterfaceDetails(formTemplate: any, showHI23Interface: any) {
  if (formTemplate.general.fields.interfaceDetails) {
    formTemplate.general.fields.interfaceDetails.handleClick = showHI23Interface;
  }
}

function getAllCaseOptionTemplates(templates: any) {
  if (templates === undefined) templates = CaseCreateOptions;
  if (templates.length === 0) {
    templates = CaseCreateOptions;
  }
  return templates.filter((template: any) => template.category === 'CaseOptions').map((template: any) =>
    ({ label: template.name, value: template.id }));
}

function setStatusFieldToTemplate(formTemplate: FormTemplate, field: string, value:string): void {
  if (!(formTemplate.general as FormTemplateSection).fields[field]) {
    return;
  }

  (formTemplate.general as FormTemplateSection).fields[field].hide = 'false';
  (formTemplate.general as FormTemplateSection).fields[field].initial = value;
}

function setTemplateFieldReadOnly(formTemplate:FormTemplate, field:string, readOnly = true): void {
  if ((formTemplate.general as FormTemplateSection).fields[field]) {
    (formTemplate.general as FormTemplateSection).fields[field].readOnly = readOnly;
  }
}

function isNewCase(warrantCaseId = ''): boolean {
  return warrantCaseId.startsWith(TEMPORARY_CASE_ID_PREFIX) ?? true;
}

function setDeleteButtonToEditingCase(
  formTemplate:FormTemplate,
  casesLength: number,
  caseInstance?:CaseModel,
  handleRemoveCase?: () => void,
): void {
  if (!(formTemplate.general as FormTemplateSection).fields.removeWarrantAction) {
    return;
  }

  if (!isNewCase(caseInstance?.id) && handleRemoveCase) {

    setFirstRowsFormTemplate((formTemplate.general as FormTemplateSection).metadata.layout?.rows || [[]]);

    const hideRemoveWarrantAction = casesLength === 1 ? 'true' : 'false';

    (formTemplate.general as FormTemplateSection).fields.removeWarrantAction.hide = hideRemoveWarrantAction;
    (formTemplate.general as FormTemplateSection).fields.removeWarrantAction.handleClick = handleRemoveCase;
    (formTemplate.general as FormTemplateSection).fields.includeInWarrant.hide = 'true';

  }
}

function setFirstRowsFormTemplate(rows: string[][]) {
  rows[0] = [
    '$empty',
    '$empty',
    'includeInWarrant',
    'removeWarrantAction',
  ];
}

function setTemplateFieldHide(formTemplate:FormTemplate, field:string, hide = true): void {
  if ((formTemplate.general as FormTemplateSection).fields[field]) {
    const hideValue = hide ? 'true' : 'false';
    (formTemplate.general as FormTemplateSection).fields[field].hide = hideValue;
  }
}
