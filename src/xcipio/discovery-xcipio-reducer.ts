import {
  GET_DX_APP_SETTINGS_FULFILLED,
} from './discovery-xcipio-actions';
import { DiscoveryXcipioState } from './discovery-xcipio.types';

const MB = 1048576;
const defaultState: DiscoveryXcipioState = {
  // Warrant_bSelectAllHI3Interfaces - if true, select ALL HI3 interfaces in the Case
  // panel by default, otherwise select the first HI3 Interface.
  discoveryXcipioSettings: {
    Warrant_bSelectAllHI3Interfaces: true,
    // Attachment_Size_Limit - the maximum attachment size that can be uploaded. unit is in bytes
    Attachment_Size_Limit: 25 * MB,
    // Warrant_bAttachments_enabled - if true, attachment feature is enabled.
    Warrant_bAttachments_enabled: false,
    /**  Set to true enable Multirow selection for warrant grid    */
    isBulkActionsEnabled: false,
    isAdditionalWarrantStateEnabled: false,
    Warrant_bArchiveWarrant: false,
    isAuditEnabled: true,
    isExternalDashboardEnabled: false,
    isSecurityGatewayEnabled: false,
    externalDashboardIP: '',
    logiOAuthClientId: '',
    logiOAuthClientName: '',
    logiAdminAccount: '',
    logiAdminUserName: '',
    logiAdminPswd: '',
    logiSupervisorUserName: '',
    logiSupervisorPswd: '',
    language: '',
    // indicate the user deployment.  At this time, it can be V or Endeavor
    deployment: 'andromeda',
  },
};

const moduleReducer = (state = defaultState, action: any): DiscoveryXcipioState => {
  switch (action.type) {
    case GET_DX_APP_SETTINGS_FULFILLED:
      if (!action.payload.data.data || action.payload.data.data.length < 1) {
        return state;
      }
      const newDiscoveryXcipioSettings: any = {};

      if (action.payload.data.data[0].appSettings) {
        action.payload.data.data[0].appSettings.forEach((dxSetting: any) => {
          if (newDiscoveryXcipioSettings[dxSetting.name] === undefined) {
            if (dxSetting.type === 'boolean') {
              newDiscoveryXcipioSettings[dxSetting.name] = (dxSetting.setting === 'true');
            } else { // default is text
              newDiscoveryXcipioSettings[dxSetting.name] = dxSetting.setting;
            }
          }
        });
      }
      // TODO: Remove "isAuditEnabled" bit implementation from the code. User Audit is always enabled.
      // For now hardcoding it to true to ignore the server value.
      newDiscoveryXcipioSettings.isAuditEnabled = true;

      return {
        ...state,
        discoveryXcipioSettings: {
          ...state.discoveryXcipioSettings,
          ...newDiscoveryXcipioSettings,
        },
      };
    default:
      return state;
  }
};
export default moduleReducer;
