import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { AuditActionType } from 'shared/userAudit/audit-constants';
import { OPERATOR } from '../../../shared/form/validators/operatorValidator';

export const STOP_FORM = {
  stopDateTime: {
    label: 'Stop Time',
    icon: 'time',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
    },
  },
};

export const RESTORE_FORM = {
  newName: {
    label: 'New Name',
    icon: '',
    type: 'text',
    initial: '',
    validation: {
      required: true,
    },
  },
  startDateTime: {
    label: 'Start Time',
    icon: 'time',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
    },
  },
  stopDateTime: {
    label: 'Stop Time',
    icon: 'time',
    type: 'datetime',
    initial: '',
    validation: {
      required: true,
      validators: [
        {
          type: 'Operator',
          operator: OPERATOR.GREATER_THAN,
          fieldName: 'startDateTime',
          errorMessage: 'Stop time should be greater than the start time',
        },
      ],
    },
  },
};

export const formatAction = (action: string) => {
  return action
    ? action[0].toUpperCase() + action.slice(1)
    : '';
};

export const adaptToAuditActionType = (action: string): AuditActionType => {
  let auditActionType: AuditActionType = AuditActionType.WARRANT_EDIT;
  switch (action) {
    case COMMON_GRID_ACTION.Delete: auditActionType = AuditActionType.WARRANT_DELETE; break;
    case COMMON_GRID_ACTION.Restore: auditActionType = AuditActionType.WARRANT_RESTORE; break;
    case COMMON_GRID_ACTION.Trash: auditActionType = AuditActionType.WARRANT_TRASH; break;
    default: break;
  }
  return auditActionType;
};

export const isUserAuditEnabledForAction = (action: string): boolean => {
  return (action === COMMON_GRID_ACTION.Delete
          || action === COMMON_GRID_ACTION.Start
          || action === COMMON_GRID_ACTION.Stop
          || action === COMMON_GRID_ACTION.Restore
          || action === COMMON_GRID_ACTION.Trash);
};

export const getUserAuditModeForAction = (action: string): string => {
  return (action === COMMON_GRID_ACTION.Delete || action === COMMON_GRID_ACTION.Trash) ? 'delete' : 'edit';
};

export function getSuccessMessageForAction(name: string, action: string): string {
  switch (action) {
    case COMMON_GRID_ACTION.Start:
      return `${name} successfully started.`;
    case COMMON_GRID_ACTION.Stop:
      return `${name} successfully stopped.`;
    case COMMON_GRID_ACTION.Pause:
      return `${name} successfully paused.`;
    case COMMON_GRID_ACTION.Resume:
      return `${name} successfully resumed.`;
    case COMMON_GRID_ACTION.Reschedule:
      return `${name} successfully rescheduled.`;
    case COMMON_GRID_ACTION.Trash:
      return `${name} successfully trashed.`;
    case COMMON_GRID_ACTION.Delete:
      return `${name} successfully deleted.`;
    case COMMON_GRID_ACTION.Untrash:
    case COMMON_GRID_ACTION.Restore:
      return `${name} successfully restored.`;
    default:
      return 'Success.';
  }
}
