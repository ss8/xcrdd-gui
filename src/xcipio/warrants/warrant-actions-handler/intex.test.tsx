import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import {
  WarrantActionsHandler,
  IWarrantActionsHandlerProps,
  IWarrantActionsHandlerStatus,
} from './index';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

describe('<WarrantActionsHandler />', () => {
  let testMocks: any;
  let wrapper: ShallowWrapper<IWarrantActionsHandlerProps, IWarrantActionsHandlerStatus, WarrantActionsHandler>;
  let props: IWarrantActionsHandlerProps;

  beforeEach(() => {
    testMocks = {};

    testMocks.warrant = {
      attachments: [],
      caseOptTemplateUsed: '7',
      caseOptions: {},
      caseType: 'ALL',
      cases: [],
      cfGroup: { id: 'MA', name: '' },
      cfPool: [],
      city: '',
      comments: '',
      contact: { id: '', name: '' },
      department: '',
      dxAccess: '',
      dxOwner: '',
      id: '1234',
      judge: '',
      name: 'warrant name',
      num_of_attachments: 0,
      province: '',
      receivedDateTime: '',
      region: '',
      startDateTime: '2020-02-20T01:41:00Z',
      state: 'EXPIRED',
      stopDateTime: '2020-02-29T08:00:00Z',
      timeZone: 'America/New_York',
    };

    props = {
      action: '',
      authentication: {},
      bArchiveWarrant: {},
      clearSelectedWarrants: jest.fn(),
      deleteWarrant: jest.fn(),
      isAuditEnabled: false,
      global: {
        selectedDF: 'some df id',
      },
      isOpen: true,
      onClose: jest.fn(),
      selectedWarrants: [testMocks.warrant],
      warrantUpdateProvStatus: jest.fn(),
      templates: [],
      broadcastSelectedWarrants: jest.fn(),
    };

    wrapper = shallow(<WarrantActionsHandler {...props} />);
  });

  describe('shouldAdaptDateTimeToTimezone()', () => {
    it('should return true if action is Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Start });
      expect(wrapper.instance().shouldAdaptDateTimeToTimezone()).toBe(true);
    });

    it('should return true if action is Restore', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Restore });
      expect(wrapper.instance().shouldAdaptDateTimeToTimezone()).toBe(true);
    });

    it('should return false if action is not Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Delete });
      expect(wrapper.instance().shouldAdaptDateTimeToTimezone()).toBe(false);
    });
  });

  describe('adaptDateTimeToTimezone()', () => {
    it('should return the data with the date/time field converted to the given timezone for Start action', () => {
      const data = {
        action: COMMON_GRID_ACTION.Start,
        stopDateTime: '2020-07-30T12:20:00.000Z',
      };
      expect(WarrantActionsHandler.adaptDateTimeToTimezone(data, 'America/New_York'))
        .toEqual({
          action: COMMON_GRID_ACTION.Start,
          stopDateTime: '2020-07-30T09:20:00.000Z',
        });
    });

    it('should return the data with the date/time field converted to the given timezone for Restore action', () => {
      const data = {
        action: COMMON_GRID_ACTION.Restore,
        startDateTime: '2020-07-29T07:20:00.000Z',
        stopDateTime: '2020-07-30T12:20:00.000Z',
      };
      expect(WarrantActionsHandler.adaptDateTimeToTimezone(data, 'America/New_York'))
        .toEqual({
          action: COMMON_GRID_ACTION.Restore,
          startDateTime: '2020-07-29T04:20:00.000Z',
          stopDateTime: '2020-07-30T09:20:00.000Z',
        });
    });
  });

  describe('handleAction()', () => {
    beforeEach(() => {
      jest.spyOn(wrapper.instance(), 'handleDeleteTrash');
    });

    it('should call warrantUpdateProvStatus with adapted dates if action is Start', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Start });
      wrapper.instance().formData = { action: 'START', stopDateTime: '2020-07-30T12:20:00.000Z' };
      wrapper.instance().handleAction(testMocks.warrant);
      expect(props.warrantUpdateProvStatus).toBeCalledWith(
        'some df id',
        '1234',
        {
          auditDetails: null,
          data: {
            action: 'START',
            stopDateTime: '2020-07-30T09:20:00.000Z',
          },
        },
      );
    });

    it('should call warrantUpdateProvStatus with data as is if action is Stop', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Stop });
      wrapper.instance().formData = { action: 'STOP' };
      wrapper.instance().handleAction(testMocks.warrant);
      expect(props.warrantUpdateProvStatus).toBeCalledWith(
        'some df id',
        '1234',
        {
          auditDetails: null,
          data: {
            action: 'STOP',
          },
        },
      );
    });

    it('should call handleDeleteTrash with warrant if action is Delete', () => {
      wrapper.setProps({ action: COMMON_GRID_ACTION.Delete });
      wrapper.instance().handleAction(testMocks.warrant);
      expect(wrapper.instance().handleDeleteTrash).toBeCalledWith(testMocks.warrant, null);
    });
  });
});
