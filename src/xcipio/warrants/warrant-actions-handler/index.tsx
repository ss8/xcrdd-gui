import React, { Fragment, ReactElement } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { RootState } from 'data/root.reducers';
import { Warrant } from 'data/warrant/warrant.types';
import ModalDialog, { DONT_CLOSE_ON_SUBMIT } from '../../../shared/modal/dialog';
import DynamicForm, { ChangedField } from '../../../shared/form';
import { WARRANT_PROVSTATE } from '../warrant-enums';
import {
  warrantUpdateProvStatus,
  deleteWarrant,
  clearSelectedWarrants,
  broadcastSelectedWarrants,
} from '../warrant-actions';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';
import ActionHandler from '../actions-bar/action-handler';
import {
  AuditService, AuditActionType, getAuditDetails, getAuditFieldInfo,
} from '../../../shared/userAudit/audit-constants';
import UserAudit from '../../../shared/userAudit';
import {
  STOP_FORM, RESTORE_FORM,
  formatAction,
  adaptToAuditActionType,
  isUserAuditEnabledForAction,
  getUserAuditModeForAction,
  getSuccessMessageForAction,
} from './utils';
import { getStopTimeValidatorFromTemplate } from '../reschedule/utils';
import { AppToaster, showSuccessMessage, TOAST_TIMEOUT } from '../../../shared/toaster';
import { extractErrorMessage } from '../../../utils';
import { validateAutoForm } from '../../../components/auto-form';
import {
  checkGivenTimeWithCurrentTime,
} from '../../../shared/constants/checkTimeValidation';
import ShowStatusMessage from '../forms/updateWarrant/show-status';

export interface IWarrantActionsHandlerStatus {
  action: string,
  displayMessage: string,
  form: any,
  formFields: any,
  layout: any,
  selectedWarrants: Array<any>,
  width: string,
  isOpenProgressBarIndicartor: boolean,
  errors: string[],
}

export interface IWarrantActionsHandlerProps {
  action: string,
  authentication: any,
  bArchiveWarrant: any,
  clearSelectedWarrants: any,
  deleteWarrant: any,
  isAuditEnabled: any,
  global: any,
  isOpen: any,
  onClose: any,
  selectedWarrants: Array<any>,
  warrantUpdateProvStatus: any,
  templates: any,
  broadcastSelectedWarrants: any,
  isWarrantDefaultExpireEnabled?: boolean,
  warrantDefaultExpireDays?: string,
  onReloadWarrant?: () => void,
  onWarrantAction?: (action: COMMON_GRID_ACTION) => void,
}

export class WarrantActionsHandler extends React.Component<IWarrantActionsHandlerProps, IWarrantActionsHandlerStatus> {
  formRef: any;

  userAuditformRef: any;

  formData: any = {};

  auditData: any = {};

  constructor(props: any) {
    super(props);
    this.state = this.defaultState();
  }

  defaultState = () =>
    ({
      displayMessage: '',
      form: {},
      formFields: undefined,
      layout: undefined,
      width: '500px',
      selectedWarrants: [],
      action: '',
      isOpenProgressBarIndicartor: false,
      payload: {},
      auditFields: {},
      errors: [],
    })

  componentDidUpdate = () => {

    const { action, selectedWarrants } = this.props;
    if (action &&
      (this.state.selectedWarrants !== selectedWarrants ||
        this.state.action !== action
      )) {
      let displayMessage = '';
      if (selectedWarrants.length) {
        if (selectedWarrants.length > 1) {
          displayMessage = `Are you sure you want to ${action} ${selectedWarrants.length} warrants?`;
        } else {
          const { name } = selectedWarrants[0];
          displayMessage = `Are you sure you want to ${action} warrant '${name}' ?`;
        }
      }
      this.setState({
        ...this.defaultState(),
        displayMessage,
        selectedWarrants,
        action,
      }, this.modifyFormBasedOnAction);
    }
  }

  modifyFormBasedOnAction = () => {
    const formState: any = {};
    if (this.state.action === COMMON_GRID_ACTION.Start) {
      formState.formFields = cloneDeep(STOP_FORM);
      this.augmentStopTimeValidators(formState.formFields.stopDateTime);
      formState.layout = [Object.keys(formState.formFields)];
    } else if (this.state.action === COMMON_GRID_ACTION.Restore) {
      formState.formFields = RESTORE_FORM;
      formState.width = '800px';
      formState.layout = [Object.keys(formState.formFields)];
    } else formState.formFields = null;
    this.setState({ ...formState });
  }

  augmentStopTimeValidators = (stopDateTimeField: any) => {
    const { selectedWarrants } = this.props;
    // For single warrant selection, we would process additional requested validation as part of UI validation.
    // For multi-select, we let Bulk component handle validation error.
    if (selectedWarrants.length === 1) {
      stopDateTimeField.initial = selectedWarrants[0].stopDateTime;
      if (!stopDateTimeField.validation.validators) {
        stopDateTimeField.validation.validators = [];
      }
      const validator = getStopTimeValidatorFromTemplate(this.props.templates, selectedWarrants[0]);
      if (validator) {
        stopDateTimeField.validation.validators.push(validator);
      }
    }
  }

  fieldUpdateHandler = (changedField: ChangedField) => {
    const { name, value } = changedField;
    const form = cloneDeep(this.state.form);
    form[name] = value;
    const errors = this.validateDateTimeFields(form);
    this.setState({
      form,
      errors,
    });
  }

  getCustomComponent = (): ReactElement => {
    const {
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      selectedWarrants,
      isAuditEnabled,
    } = this.props;

    const {
      action,
      formFields,
      layout,
      errors,
    } = this.state;

    let warrant: Partial<Warrant> = {};
    if (selectedWarrants.length === 1) {
      warrant = { ...selectedWarrants[0] };
    }

    if (isWarrantDefaultExpireEnabled) {
      WarrantAdapter.setWarrantDefaultStopTime(warrant, warrantDefaultExpireDays);
    }

    return (
      <Fragment>
        {(formFields) && (
          <DynamicForm
            ref={(ref) => { this.formRef = ref; }}
            name="warrant status update form"
            fields={formFields}
            layout={layout}
            originalValues={warrant}
            defaults={warrant}
            fieldUpdateHandler={this.fieldUpdateHandler}
          />
        )}
        {
          (isUserAuditEnabledForAction(action)
            && isAuditEnabled) && (
            <UserAudit
              data-test="userAudit"
              ref={(ref: any) => { this.userAuditformRef = ref; }}
              mode={getUserAuditModeForAction(action)}
            />
          )
        }
        {
          errors.length > 0 &&
          <ShowStatusMessage hasErrors msgs={errors} />
        }
        {
          (action === COMMON_GRID_ACTION.Restore) && (
            <div style={{ padding: '7px' }}>
              Note:  Restoring may take a few minutes before reflected in your query results.
            </div>
          )
        }
      </Fragment>
    );
  }

  getAuditDetailsInfo = (warrant: any, actionType: AuditActionType) => {
    if (this.props.isAuditEnabled) {
      const name = warrant?.name;
      const userId = this.props.authentication.userDBId;
      const userName = this.props.authentication.userId;
      const isEdit = actionType === AuditActionType.WARRANT_EDIT;
      const data = isEdit ? this.formData : null;
      const auditFieldDetails = getAuditFieldInfo(data, this.auditData, isEdit, warrant);

      const auditDetails = getAuditDetails(
        this.props.isAuditEnabled,
        userId,
        userName,
        auditFieldDetails,
        name,
        AuditService.WARRANTS,
        actionType,
      );
      return auditDetails;
    }
    return null;
  }

  validateDateTimeFields(formFields: any) {
    const { action } = this.state;
    // For bulk actions it validates based on first selected warrant timezone
    const { timeZone } = this.state.selectedWarrants[0];
    let errors: string[] = [];

    if (action === COMMON_GRID_ACTION.Start && formFields.stopDateTime) {
      if (checkGivenTimeWithCurrentTime(formFields.stopDateTime, timeZone)) {
        errors = ['Stop Time should be after the current time. Please select based on warrant time zone'];
      }
    } else if (action === COMMON_GRID_ACTION.Restore && formFields.startDateTime) {
      if (checkGivenTimeWithCurrentTime(formFields.startDateTime, timeZone)) {
        errors = ['Start Time should be after the current time. Please select based on warrant time zone'];
      }
    }
    return errors;
  }

  handleSubmit = () => {
    let formFields = {};
    let auditFields = {};
    let isValid = true;
    if (this.formRef) {
      if (!this.formRef.isValid(true, true)) {
        isValid = false;
      }
      formFields = this.formRef.getValues();
      const errors = this.validateDateTimeFields(formFields);
      if (errors.length > 0) {
        isValid = false;
      }
      this.setState({ errors });
    }
    if (this.userAuditformRef) {
      if (this.userAuditformRef.isValid(true, true)) {
        auditFields = this.userAuditformRef?.getValue();
      } else {
        isValid = false;
      }
    }

    if (!isValid) {
      return DONT_CLOSE_ON_SUBMIT;
    }
    const data: any = {
      action: (WARRANT_PROVSTATE as any)[formatAction(this.state.action)],
      ...formFields,
    };

    this.formData = data;
    this.auditData = auditFields;

    if (this.state.selectedWarrants.length === 1) {
      const warrant = this.state.selectedWarrants[0];

      this.handleAction(warrant)
        .then(() => {
          const { onReloadWarrant, onWarrantAction } = this.props;

          if (WarrantActionsHandler.isStoppingOrEditing(data.action) && !!onReloadWarrant) {
            onReloadWarrant();
          } else {
            this.props.clearSelectedWarrants();
          }

          if (data.action && onWarrantAction) {
            onWarrantAction(data.action);
          }

          const { action } = this.state;
          const message = getSuccessMessageForAction(warrant.name, action);
          showSuccessMessage(message, TOAST_TIMEOUT);
        })
        // TODO: REZA: handle error in reducer so we don't have to handle per component
        .catch((error: any) => {
          AppToaster.show({
            icon: 'error',
            intent: 'danger',
            timeout: 0,
            message: `Error: ${extractErrorMessage(error)}`,
          });
        });
    } else this.handleBulkAction();
  }

  shouldAdaptDateTimeToTimezone() {
    const { action } = this.state;
    return action === COMMON_GRID_ACTION.Start || action === COMMON_GRID_ACTION.Restore;
  }

  static isStoppingOrEditing(action: string): boolean {
    return (
      action?.toLowerCase() === COMMON_GRID_ACTION.Start
      || action?.toLowerCase() === COMMON_GRID_ACTION.Stop
      || action?.toLowerCase() === COMMON_GRID_ACTION.Pause
      || action?.toLowerCase() === COMMON_GRID_ACTION.Resume
      || action?.toLowerCase() === COMMON_GRID_ACTION.Reschedule
    );
  }

  /**
   * Convert the warrant date/time fields to the warrant timezone
   *
   * @param warrant warrant data
   * @param timeZone the timezone to use
   * @return the warrant adapted
   */
  static adaptDateTimeToTimezone(warrant: any, timeZone: string): any {
    const { timeZone: timeZoneToRemove, ...adaptedWarrant } =
      WarrantAdapter.adaptWarrantDateTimeFieldsToSubmit({ ...warrant, timeZone });
    return adaptedWarrant;
  }

  handleAction = async (warrant: any) => {
    const { selectedDF: dfId } = this.props.global;
    const { id, timeZone } = warrant;

    let data = {
      ...this.formData,
    };

    if (this.shouldAdaptDateTimeToTimezone()) {
      data = WarrantActionsHandler.adaptDateTimeToTimezone(data, timeZone);
    }

    const { action } = this.state;
    const auditActionType: AuditActionType = adaptToAuditActionType(action);
    const auditDetails = this.getAuditDetailsInfo(warrant, auditActionType);

    const payload = {
      data,
      auditDetails,
    };
    if (action === COMMON_GRID_ACTION.Delete) {
      const { onWarrantAction } = this.props;
      await this.handleDeleteTrash(warrant, auditDetails);

      if (onWarrantAction) {
        onWarrantAction(action);
      }

    } else await this.props.warrantUpdateProvStatus(dfId, id, payload);
  }

  handleDeleteTrash = async (warrant: any, auditDetails: any) => {
    const { selectedDF: dfId } = this.props.global;
    const { id }: any = warrant;
    await this.props.deleteWarrant(dfId, id, auditDetails);
  }

  handleBulkAction = () => {
    // apply validation to each warrant, let bulk handle errors
    if (this.state.action === COMMON_GRID_ACTION.Start) {
      const { formFields } = this.state;
      if (formFields != null && formFields.stopDateTime) {
        const formInputs: any = { stopDateTime: { autoValidate: [] } };
        const validatedSelectedWarrants = this.props.selectedWarrants.map((warrant: any) => {
          const validator = getStopTimeValidatorFromTemplate(this.props.templates, warrant);
          if (validator) {
            formInputs.stopDateTime.label = formFields.stopDateTime.label;
            formInputs.stopDateTime.isRequired = formFields.stopDateTime.validation?.required;
            formInputs.stopDateTime.value = this.state.form.stopDateTime;
            formInputs.stopDateTime.originalValue = warrant.stopDateTime;
            formInputs.stopDateTime.autoValidate.push(validator);
          }
          const { isValid, errorMessage } = validateAutoForm(formInputs);
          warrant.autoFormValidation = { isValid, errorMessage };
          return warrant;
        });
        this.props.broadcastSelectedWarrants(validatedSelectedWarrants);
      }
    }
    this.setState({ isOpenProgressBarIndicartor: true });
  };

  handleClose = () => {
    this.setState({ isOpenProgressBarIndicartor: false });
    this.props.onClose();
    this.props.clearSelectedWarrants();
  };

  render() {

    return (
      <Fragment>
        <ModalDialog
          onSubmit={this.handleSubmit}
          customComponent={this.getCustomComponent}
          width={this.state.width}
          actionText={formatAction(this.state.action)}
          displayMessage={this.state.displayMessage}
          onClose={this.props.onClose}
          isOpen={this.props.isOpen}
        />
        <ActionHandler
          isOpen={this.state.isOpenProgressBarIndicartor}
          onClose={this.handleClose}
          handleAction={this.handleAction}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  authentication,
  global,
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bArchiveWarrant: bArchiveWarrant,
      isAuditEnabled,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
    },
  },
  warrants: { selectedWarrants, templates },
}: RootState) => ({
  authentication,
  global,
  bArchiveWarrant,
  isAuditEnabled,
  selectedWarrants,
  templates,
  isWarrantDefaultExpireEnabled,
  warrantDefaultExpireDays,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  deleteWarrant,
  warrantUpdateProvStatus,
  clearSelectedWarrants,
  broadcastSelectedWarrants,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WarrantActionsHandler);
