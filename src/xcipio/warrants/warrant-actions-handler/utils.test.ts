import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { AuditActionType } from 'shared/userAudit/audit-constants';
import {
  adaptToAuditActionType,
  getSuccessMessageForAction,
  getUserAuditModeForAction,
  isUserAuditEnabledForAction,
} from './utils';

describe('action-handler->utils.ts tests', () => {
  it(' verify adaptToAuditActionType function', () => {
    let actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Trash);
    expect(actionType).toEqual(AuditActionType.WARRANT_TRASH);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Delete);
    expect(actionType).toEqual(AuditActionType.WARRANT_DELETE);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Restore);
    expect(actionType).toEqual(AuditActionType.WARRANT_RESTORE);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Edit);
    expect(actionType).toEqual(AuditActionType.WARRANT_EDIT);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Stop);
    expect(actionType).toEqual(AuditActionType.WARRANT_EDIT);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Start);
    expect(actionType).toEqual(AuditActionType.WARRANT_EDIT);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Reschedule);
    expect(actionType).toEqual(AuditActionType.WARRANT_EDIT);
    actionType = adaptToAuditActionType(COMMON_GRID_ACTION.Resume);
    expect(actionType).toEqual(AuditActionType.WARRANT_EDIT);
  });

  it(' verify isUserAuditEnabledForAction function', () => {
    let showUserAudit = isUserAuditEnabledForAction(COMMON_GRID_ACTION.Delete);
    expect(showUserAudit).toBe(true);
    showUserAudit = isUserAuditEnabledForAction(COMMON_GRID_ACTION.Start);
    expect(showUserAudit).toBe(true);
    showUserAudit = isUserAuditEnabledForAction(COMMON_GRID_ACTION.Stop);
    expect(showUserAudit).toBe(true);
    showUserAudit = isUserAuditEnabledForAction(COMMON_GRID_ACTION.Restore);
    expect(showUserAudit).toBe(true);
    showUserAudit = isUserAuditEnabledForAction(COMMON_GRID_ACTION.Trash);
    expect(showUserAudit).toBe(true);
  });

  it(' verify getUserAuditModeForAction function', () => {
    let auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Trash);
    expect(auditMode).toEqual('delete');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Delete);
    expect(auditMode).toEqual('delete');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Restore);
    expect(auditMode).toEqual('edit');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Edit);
    expect(auditMode).toEqual('edit');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Stop);
    expect(auditMode).toEqual('edit');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Start);
    expect(auditMode).toEqual('edit');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Reschedule);
    expect(auditMode).toEqual('edit');
    auditMode = getUserAuditModeForAction(COMMON_GRID_ACTION.Resume);
    expect(auditMode).toEqual('edit');
  });

  describe('getSuccessMessageForAction()', () => {
    it('should return the error message for the action', () => {
      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Start)).toEqual(
        'Name1 successfully started.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Stop)).toEqual(
        'Name1 successfully stopped.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Pause)).toEqual(
        'Name1 successfully paused.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Resume)).toEqual(
        'Name1 successfully resumed.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Reschedule)).toEqual(
        'Name1 successfully rescheduled.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Delete)).toEqual(
        'Name1 successfully deleted.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Restore)).toEqual(
        'Name1 successfully restored.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Untrash)).toEqual(
        'Name1 successfully restored.',
      );

      expect(getSuccessMessageForAction('Name1', COMMON_GRID_ACTION.Trash)).toEqual(
        'Name1 successfully trashed.',
      );
    });
  });
});
