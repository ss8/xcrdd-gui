import React, { FunctionComponent } from 'react';
import {
  Button, Menu, MenuItem, Popover, Position,
} from '@blueprintjs/core';
import { Warrant } from 'data/warrant/warrant.types';
import './warrant-classic-mode-menu.scss';

type WarrantClassicModeMenuProps = {
  hasPrivilegeToCreate: boolean,
  hasPrivilegeToView: boolean,
  selectedWarrants: Warrant[],
  displayTrashWarrants: boolean,
  onClassicModeCreate: () => void,
  onClassicModeEdit: (warrant: Warrant) => void,
  onClassicModeView: () => void,
}

export const WarrantClassicModeMenu: FunctionComponent<WarrantClassicModeMenuProps> = ({
  hasPrivilegeToCreate,
  hasPrivilegeToView,
  selectedWarrants,
  displayTrashWarrants,
  onClassicModeCreate,
  onClassicModeEdit,
  onClassicModeView,
}: WarrantClassicModeMenuProps) => {

  const handleClassicModeEdit = () => {
    onClassicModeEdit(selectedWarrants[0]);
  };

  const renderCreateMenuItem = () => {
    if (!hasPrivilegeToCreate) {
      return null;
    }

    return <MenuItem icon="add" onClick={onClassicModeCreate} text="New Warrant" />;
  };

  const renderEditMenuItem = () => {
    if (!hasPrivilegeToCreate || displayTrashWarrants) {
      return null;
    }

    let editDisabled = true;
    if (selectedWarrants != null && selectedWarrants.length === 1) {
      editDisabled = false;
    }

    return <MenuItem disabled={editDisabled} icon="edit" onClick={handleClassicModeEdit} text="Edit" />;
  };

  const renderViewMenuItem = () => {
    if (!hasPrivilegeToView) {
      return null;
    }

    let viewDisabled = true;
    if (selectedWarrants != null && selectedWarrants.length === 1) {
      viewDisabled = false;
    }

    return <MenuItem disabled={viewDisabled} icon="eye-open" onClick={onClassicModeView} text="View" />;
  };

  const renderClassicModeMenu = () => {
    return (
      <Menu>
        {renderCreateMenuItem()}
        {renderEditMenuItem()}
        {renderViewMenuItem()}
      </Menu>
    );
  };

  return (
    <Popover className="warrant-classic-mode-menu" content={renderClassicModeMenu()} position={Position.BOTTOM_RIGHT}>
      <Button intent="primary" text="Classic Mode" minimal rightIcon="caret-down" />
    </Popover>
  );
};

export default WarrantClassicModeMenu;
