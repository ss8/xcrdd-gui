import { WarrantState } from 'data/warrant/warrant.types';
import {
  GET_WARRANT_BY_ID_FULFILLED,
  GET_ALL_WARRANTS_FULFILLED,
  DELETE_WARRANT_FULFILLED,
  UPDATE_TEMPLATE,
  WARRANT_CLEAR_ERRORS,
  GET_ALL_TEMPLATES_FULFILLED,
  GET_WARRANT_FORM_CONFIG_FULFILLED,
  GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
  SET_WARRANTEDIT_CHANGETRACKER,
  CLEAR_WARRANTEDIT_CHANGETRACKER,
  UPDATE_WARRANT_STATE,
  UPDATE_WARRANT_AND_AUDIT_STATE,
  SET_WARRANT_FORM_CONFIG_INSTANCE,
  CLEAR_WARRANT_STATE,
  UPDATE_TEMPLATE_USED,
  CLEAR_TEMPLATE_USED,
  BROADCAST_SELECTED_WARRANTS,
  CLEAR_SELECTED_WARRANTS,
  SET_DISPLAY_TRASH_WARRANTS,
  BROADCAST_SELECTED_WARRANT_CASE,
  CLEAR_SELECTED_WARRANT_CASE,
} from './warrant-actions';

export const defaultState: WarrantState = {
  warrant: null,
  warrants: [],
  selectedWarrants: [],
  error: null,
  cfs: [],
  action: '',
  config: undefined,
  customizationconfig: { derivedTargets: {} },
  templates: [],
  template: null,
  changeTracker: null,
  warrantConfigInstance: null,
  templateUsed: null,
  userAudit: undefined,
  selectionLastCleared: null,
  selectedCase: null,
  displayTrashWarrants: false,
};

const moduleReducer = (state = defaultState, action: any): WarrantState => {
  /*   if (/WARRANT.*_REJECTED/.test(action.type)) {
      if (action.payload.response.status === 401) {
        return Object.assign({}, state, { error: null }); // 401's handled elsewhere
      }
      return Object.assign({}, state, { error: action.payload.message });
    } */
  if (action.payload && action.payload.data && !action.payload.data.success) {
    // unsuccessful request that was not rejected (200)
    if (action.payload.data.error && action.payload.data.error.message) {
      return { ...state, error: action.payload.data.error.message };
    }
  }
  switch (action.type) {
    case GET_ALL_WARRANTS_FULFILLED:
      return {
        ...state,
        warrants: action.payload.data.data,
      };
    case GET_WARRANT_BY_ID_FULFILLED: {
      const warrant = action.payload.data.data[0];
      return { ...state, warrant };
    }
    case UPDATE_TEMPLATE:
      return {
        ...state,
        template: action.payload,
        action: action.type,
      };
    case WARRANT_CLEAR_ERRORS:
      return { ...state, error: null };
    case GET_ALL_TEMPLATES_FULFILLED:
      return { ...state, templates: action.payload.data.data };
    case DELETE_WARRANT_FULFILLED:
      const { warrants } = state;
      const index = warrants.findIndex((x: any) => x.id === action.meta.wId);
      return { ...state, warrants: [...warrants.slice(0, index), ...warrants.slice(index + 1)] };
    case GET_WARRANT_FORM_CONFIG_FULFILLED:
      return { ...state, config: action.payload.data.data[0] };
    case GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED:
      return { ...state, customizationconfig: action.payload.data.data[0] };
    case SET_WARRANTEDIT_CHANGETRACKER:
      return { ...state, changeTracker: action.payload };
    case CLEAR_WARRANTEDIT_CHANGETRACKER:
      return { ...state, changeTracker: null };
    // case GET_TEMPLATE_BY_ID:
    //   return Object.assign({}, state, { template: action.payload.data.data });
    case UPDATE_WARRANT_STATE:
      return { ...state, warrant: action.payload };
    case UPDATE_WARRANT_AND_AUDIT_STATE:
      return { ...state, warrant: action.payload.warrant, userAudit: action.payload.userAudit };
    case CLEAR_WARRANT_STATE:
      return { ...state, warrant: null, userAudit: null };
    case SET_WARRANT_FORM_CONFIG_INSTANCE:
      return { ...state, warrantConfigInstance: action.payload };
    case UPDATE_TEMPLATE_USED:
      return { ...state, templateUsed: action.payload };
    case CLEAR_TEMPLATE_USED:
      return { ...state, templateUsed: null };
    case BROADCAST_SELECTED_WARRANTS:
      return {
        ...state,
        selectedWarrants: action.selectedWarrants,
      };
    case CLEAR_SELECTED_WARRANTS:
      return {
        ...state,
        selectedWarrants: [],
        selectionLastCleared: new Date(),
      };
    case SET_DISPLAY_TRASH_WARRANTS:
      return {
        ...state,
        displayTrashWarrants: action.displayTrashWarrants,
      };
    case BROADCAST_SELECTED_WARRANT_CASE:
      return {
        ...state,
        selectedCase: action.selectedCase,
      };
    case CLEAR_SELECTED_WARRANT_CASE:
      return {
        ...state,
        selectedCase: null,
      };
    default:
      return state;
  }
};
export default moduleReducer;
