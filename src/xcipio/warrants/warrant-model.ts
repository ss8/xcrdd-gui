import { Warrant } from 'data/warrant/warrant.types';

// eslint-disable-next-line import/prefer-default-export
export const warrantModel: Warrant = {
  id: '',
  templateUsed: '',
  dxOwner: '',
  dxAccess: '',
  name: '',
  city: '',
  comments: '',
  contact: null,
  judge: '',
  receivedDateTime: '',
  region: '',
  startDateTime: '',
  stopDateTime: '',
  timeZone: '',
  cases: [],
  attachments: [],
  province: '',
  department: '',
};
