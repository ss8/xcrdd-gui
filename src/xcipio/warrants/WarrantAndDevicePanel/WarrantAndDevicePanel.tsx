import React, { Component } from 'react';
import { FormPanel } from 'shared/wizard/with-discovery-wizard-step-panel';
import WarrantPanel, { WarrantPanel as WarrantPanelClass } from 'xcipio/warrants/forms/warrant-panel';
import DevicePanel, { DevicePanel as DevicePanelClass } from 'xcipio/devices';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import './warrant-and-devices.scss';

export type WarrantAndDevicePanelProps = {
  mode: string
  onDevicesReady?: () => void
  warrantId?: string | null
  onDeleteCase?: (caseId: string)=> void
}

export default class WarrantAndDevicePanel extends Component<WarrantAndDevicePanelProps> implements FormPanel {
  warrantPanelRef: React.RefObject<WarrantPanelClass>
  devicePanelRef: React.RefObject<DevicePanelClass>

  constructor(props: WarrantAndDevicePanelProps) {
    super(props);
    this.warrantPanelRef = React.createRef<WarrantPanelClass>();
    this.devicePanelRef = React.createRef<DevicePanelClass>();
  }

  isValid(errors: string[]): boolean {
    const isWarrantPanelValid = this.warrantPanelRef.current?.isValid(errors) ?? false;
    const isDevicePanelValid = this.devicePanelRef.current?.isValid(errors) ?? false;

    return isWarrantPanelValid && isDevicePanelValid;
  }

  isWarrantAndCasesValid(errors: string[]): [boolean, number | null] {
    return this.warrantPanelRef.current?.isWarrantAndCasesValid(errors) ?? [false, null];
  }

  async submit(force = false): Promise<boolean> {
    await this.warrantPanelRef.current?.submit();
    await this.devicePanelRef.current?.submit(force);

    return true;
  }

  hasAnyDeviceChange(): boolean {
    return this.devicePanelRef.current?.hasAnyChange() ?? false;
  }

  hasConfiguredDevices = (): boolean => {
    return this.devicePanelRef.current?.hasConfiguredDevices() ?? false;
  }

  renderInstructionalText(): JSX.Element {
    const { mode } = this.props;

    if (mode === CONFIG_FORM_MODE.Edit) {
      return (
        <p className="instruction-text">
          The service list for a device is based on your LEA choice. Services cannot be added or removed
          from existing devices.
          If that is needed, please remove the device and add a new one with the desired services.
          When adding a new device, cases will be auto-generated when switching tabs or clicking on Save.
        </p>
      );
    }

    if (mode === CONFIG_FORM_MODE.View) {
      return (
        <p className="instruction-text">
          To view the cases created based on the devices and services, check the Cases tab.
        </p>
      );
    }

    return (
      <p className="instruction-text">
        The service list for a device is based on your LEA choice. Changing the LEA will reset the
        device&apos;s list.
      </p>
    );
  }

  render(): JSX.Element {
    const {
      mode,
      warrantId,
      onDevicesReady,
    } = this.props;

    return (
      <div className="warrant-and-devices">
        <h3>Warrant Information</h3>
        <WarrantPanel
          mode={mode}
          warrantId={warrantId}
          hasConfiguredDevices={this.hasConfiguredDevices}
          ref={this.warrantPanelRef}
        />
        <h3>Devices</h3>
        {this.renderInstructionalText()}
        <DevicePanel
          ref={this.devicePanelRef}
          mode={mode}
          onDevicesReady={onDevicesReady}
        />
      </div>
    );
  }
}
