import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Warrant from 'routes/Warrant';
import WarrantFrame from './warrant-frame';
import WarrantForm from './forms';
import WarrantCaseDelete from '../cases/forms/case-delete';
import WarrantWizard from './wizard';

const moduleRoot = '/warrants';

const ModuleRoutes = (props: any) => {
  return (
    <Fragment>
      <Route
        exact
        path={`${moduleRoot}/`}
        render={() => <WarrantFrame {...props} />}
      />
      {/* modals */}
      <Route
        exact
        path={`${moduleRoot}/create`}
        render={() => <WarrantForm {...props} />}
      />
      <Route
        exact
        path={`${moduleRoot}/createWizard`}
        render={() => <WarrantWizard {...props} />}
      />
      <Route
        exact
        path={`${moduleRoot}/edit`}
        render={() => <WarrantForm {...props} />}
      />
      <Route
        exact
        path={`${moduleRoot}/view`}
        render={() => <WarrantForm {...props} />}
      />
      <Route
        path={`${moduleRoot}/cases/delete`}
        render={() => <WarrantCaseDelete {...props} />}
      />
      <Warrant />
    </Fragment>
  );
};
export default ModuleRoutes;
