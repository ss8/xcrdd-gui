import moxios from 'moxios';
import Http from '../../shared/http';

import { storeFactory, findInMockHttpRequests } from '../../shared/__test__/testUtils';
import { uploadAttachment, broadcastSelectedWarrants, BROADCAST_SELECTED_WARRANTS } from './warrant-actions';

describe('test warrant-action ', () => {
  let axiosInstance = null;
  let store = null;
  beforeEach(() => {
    const reduxState = {
      authentication: {
        isAuthenticated: true, userId: '', privileges: [], errorStatus: '', beforeLogout: false, passwordUpdate: false, loginMessage: '',
      },
    };
    store = storeFactory(reduxState);
    Http.init(store);
    axiosInstance = Http.getHttp();
    moxios.install(axiosInstance);
  });
  afterEach(() => {
    moxios.uninstall(axiosInstance);
  });
  test('uploadAttachment is sending proper url, header and formData', async () => {
    const url = '/warrants/1/MjE0NzM1MTc4NQ/attachments';

    const attachment = {
      name: 'AttachmentName1', id: 'AttachmentID1', file: new File(['text in file'], 'file1.txt'), description: 'this is file 1',
    };

    moxios.wait(() => {
      const requests = findInMockHttpRequests(moxios.requests, url);

      // Find the request
      // if there are more than one request matching the url,
      // use a different url to distinguish this test from another test
      expect(requests.length).toBe(1);
      const request = requests[0];

      // Verify URL
      expect(request.url).toEqual(url);

      // Verify header is multipart/form-data
      expect(request.headers).toMatchObject(
        expect.objectContaining({
          'Content-Type': 'multipart/form-data',
        }),
      );

      // Todo: Verify formData - do not know how to do, work on feature first.

      request.respondWith({
        status: 200,
        response: [],
      });
    });

    await uploadAttachment(url, attachment);
  });

  it('should create an action to broadcast selected warrants', () => {
    const selectedWarrants = [{
      id: '0',
      name: 'Name A',
    }, {
      id: '2',
      name: 'Name B',
    }];
    const expectedAction = { type: BROADCAST_SELECTED_WARRANTS, selectedWarrants };
    expect(broadcastSelectedWarrants(selectedWarrants)).toEqual(expectedAction);
  });
});
