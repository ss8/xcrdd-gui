import { History } from 'history';

import { TEMPORARY_CASE_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import Http from '../../shared/http';
import { WARRANT_ROUTE } from '../../constants';

const http = Http.getHttp();

// action export constants
export const GET_ALL_WARRANTS = 'GET_ALL_WARRANTS';
export const GET_WARRANT_BY_ID = 'GET_WARRANT_BY_ID';
export const UPDATE_WARRANT = 'UPDATE_WARRANT';
export const PATCH_WARRANT = 'PATCH_WARRANT';
export const DELETE_WARRANT = 'DELETE_WARRANT';
export const CREATE_WARRANT = 'CREATE_WARRANT';
export const GET_ALL_WARRANTS_FULFILLED = 'GET_ALL_WARRANTS_FULFILLED';
export const GET_WARRANT_BY_ID_FULFILLED = 'GET_WARRANT_BY_ID_FULFILLED';
export const UPDATE_WARRANT_FULFILLED = 'UPDATE_WARRANT_FULFILLED';
export const PATCH_WARRANT_FULFILLED = 'PATCH_WARRANT_FULFILLED';
export const DELETE_WARRANT_FULFILLED = 'DELETE_WARRANT_FULFILLED';
export const CREATE_WARRANT_FULFILLED = 'CREATE_WARRANT_FULFILLED';
export const WARRANT_CLEAR_ERRORS = 'WARRANT_CLEAR_ERRORS';
export const WARRANT_CLEAR_ERRORS_FULFILLED = 'WARRANT_CLEAR_ERRORS_FULFILLED';
export const SET_WARRANTEDIT_CHANGETRACKER = 'SET_WARRANTEDIT_CHANGETRACKER';
export const CLEAR_WARRANTEDIT_CHANGETRACKER = 'CLEAR_WARRANTEDIT_CHANGETRACKER';

export const GET_TARGET_BY_ID = 'GET_TARGET_BY_ID';
export const GET_TARGET_BY_ID_FULFILLED = 'GET_TARGET_BY_ID_FULFILLED';

export const GET_ALL_TEMPLATES = 'GET_ALL_TEMPLATES';
export const GET_ALL_TEMPLATES_FULFILLED = 'GET_ALL_TEMPLATES_FULFILLED';
export const GET_TEMPLATE_BY_ID = 'GET_TEMPLATE_BY_ID';
export const UPDATE_TEMPLATE_BY_ID = 'UPDATE_TEMPLATE_BY_ID';
export const DELETE_TEMPLATE_BY_ID = 'DELETE_TEMPLATE_BY_ID';
export const CREATE_TEMPLATE_BY_ID = 'CREATE_TEMPLATE_BY_ID';

export const GET_WARRANT_GRID_SETTING = 'GET_WARRANT_GRID_COLUMN_SETTING';
export const GET_WARRANT_GRID_SETTING_FULFILLED = 'GET_WARRANT_GRID_COLUMN_SETTING_FULFILLED';
export const PUT_WARRANT_GRID_SETTING = 'PUT_WARRANT_GRID_COLUMN_SETTING';

export const GET_WARRANT_FORM_CONFIG = 'GET_WARRANT_FORM_CONFIG';
export const GET_WARRANT_FORM_CONFIG_FULFILLED = 'GET_WARRANT_FORM_CONFIG_FULFILLED';
export const GET_WARRANT_FORM_CUSTOMIZATION_CONFIG = 'GET_WARRANT_FORM_CUSTOMIZATION_CONFIG';
export const GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED = 'GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED';
export const SET_WARRANT_FORM_CONFIG_INSTANCE = 'SET_WARRANT_FORM_CONFIG_INSTANCE';
export const UPDATE_WARRANT_STATE = 'UPDATE_WARRANT_STATE';
export const UPDATE_WARRANT_AND_AUDIT_STATE = 'UPDATE_WARRANT_AND_AUDIT_STATE';
export const CLEAR_WARRANT_STATE = 'CLEAR_WARRANT_STATE';
export const UPDATE_TEMPLATE_USED = 'UPDATE_TEMPLATE_USED';
export const CLEAR_TEMPLATE_USED = 'CLEAR_TEMPLATE_USED';
export const UPDATE_WARRANT_PROVISIONING_STATUS = 'UPDATE_WARRANT_PROVISIONING_STATUS';
export const BROADCAST_SELECTED_WARRANTS = 'BROADCAST_SELECTED_WARRANTS';
export const CLEAR_SELECTED_WARRANTS = 'CLEAR_SELECTED_WARRANTS';
export const SET_DISPLAY_TRASH_WARRANTS = 'SET_DISPLAY_TRASH_WARRANTS';
export const BROADCAST_SELECTED_WARRANT_CASE = 'BROADCAST_SELECTED_WARRANT_CASE';
export const CLEAR_SELECTED_WARRANT_CASE = 'CLEAR_SELECTED_WARRANT_CASE';
export const UPDATE_WARRANT_RETRY = 'UPDATE_WARRANT_RETRY';
export const UPDATE_WARRANT_RETRY_FULFILLED = 'UPDATE_WARRANT_RETRY_FULFILLED';

export function goToWarrantList(history: History): () => void {
  return () => {
    history.push(`${WARRANT_ROUTE}`);
  };
}

export function goToWarrantEdit(history: History): () => void {
  return () => {
    history.push(`${WARRANT_ROUTE}/editWizard`);
  };
}

export function goToWarrantView(history: History): () => void {
  return () => {
    history.push(`${WARRANT_ROUTE}/viewWizard`);
  };
}

// action creators
export const getAllWarrants = (dfId: string, body = '') => {
  return ({ type: GET_ALL_WARRANTS, payload: http.get(`/warrants/${dfId}?${body}`) });
};
export const getWarrantById = (dfId: string, wId: string) =>
  ({ type: GET_WARRANT_BY_ID, payload: http.get(`/warrants/${dfId}/${wId}`) });

export const createWarrant = (urlAction: string, body: any) => {
  cleanUpWarrantFormData(body?.data);
  const res = http.post(urlAction, body);
  return res;
};

// utilizes redux thunk middleware + promise middleware to chain
export const createWarrantAndRefresh = (dfId: string, body: any) => {
  return async (dispatch: any) => {
    cleanUpWarrantFormData(body);
    await dispatch(createWarrant(dfId, body));
    dispatch(getAllWarrants(dfId));
  };
};
export const updateWarrant = (dfId: string, wId: string, body: any) =>
  ({ type: UPDATE_WARRANT, payload: http.put(`/warrants/${dfId}/${wId}`, body) });
export const updateWarrantAndRefresh = (dfId: string, wId: string, body: any) => {
  return async (dispatch: any) => {
    await dispatch(updateWarrant(dfId, wId, body));
    dispatch(getAllWarrants(dfId));
  };
};
export const setWarrantTracker = (changeTracker: any) =>
  ({ type: SET_WARRANTEDIT_CHANGETRACKER, payload: changeTracker });
export const clearWarrantTracker = () => ({ type: CLEAR_WARRANTEDIT_CHANGETRACKER });
export const patchWarrant = (dfId: string, wId: string, body: any) =>
  ({ type: PATCH_WARRANT, payload: http.patch(`/warrants/${dfId}/${wId}`, body) });

export const deleteWarrant = (dfId: string, wId: string, body: any = null) =>
  ({ type: DELETE_WARRANT, payload: http.delete(`/warrants/${dfId}/${wId}`, { data: body }), meta: { wId } });
export const deleteWarrantAndRefresh = (dfId: string, wId: string) => {
  return async (dispatch: any) => {
    await dispatch(deleteWarrant(dfId, wId));
    dispatch(getAllWarrants(dfId, '$top=%20200'));
  };
};

export const getTargetById = (dfId: string, wId: string, cId: string, tId: string) =>
  ({ type: GET_TARGET_BY_ID, payload: http.get(`/warrants/${dfId}/${wId}/cases/${cId}/targets/${tId}`) });
export const updateTemplateById = (tId: number, body: any) =>
  ({ type: UPDATE_TEMPLATE_BY_ID, payload: http.put(`/templates/${tId}`, body) });
export const deleteTemplateById = (tId: number) =>
  ({ type: DELETE_TEMPLATE_BY_ID, payload: http.delete(`/templates/${tId}`) });
export const createTemplate = (body: any) =>
  ({ type: CREATE_TEMPLATE_BY_ID, payload: http.post('/templates', body) });

// non-http

// templates
export const getTemplateById = (tId: number) =>
  ({ type: GET_TEMPLATE_BY_ID, payload: http.get(`/templates/${tId}`) });
export const getAllTemplates = () =>
  ({ type: GET_ALL_TEMPLATES, payload: http.get('/templates') });
export const UPDATE_TEMPLATE = 'UPDATE_TEMPLATE';
export const setTemplate = (template: Record<string, unknown>) =>
  ({ type: UPDATE_TEMPLATE, payload: template });

export const clearErrors = () =>
  ({ type: WARRANT_CLEAR_ERRORS, payload: null });
export const getWarrantFormConfig = (dfId: string) =>
  ({ type: GET_WARRANT_FORM_CONFIG, payload: http.get(`/config/supported-features/${dfId}`) });
export const getWarrantFormCustomizationConfig = (dfId: string) =>
  ({ type: GET_WARRANT_FORM_CUSTOMIZATION_CONFIG, payload: http.get(`/config/customizations/${dfId}`) });

// warrant table column setting
export const getWarrantGridSettings = (userId: string) => {
  return ({ type: GET_WARRANT_GRID_SETTING, payload: http.get(`/users/${userId}/settings?name=WarrantGridSettings`) });
};
export const putWarrantGridSettings = (userId: any, warrantGridSettings: any) => {
  // this.updateWarrantGridSettingState(warrantGridSettings);

  // converts double quotes to escape single quote
  const settingInJson = JSON.stringify(warrantGridSettings);
  const warrantGridSettingsData = {
    name: 'WarrantGridSettings',
    type: 'json',
    user: userId,
    setting: settingInJson,
  };
  const requestBody = [warrantGridSettingsData];
  return ({ type: PUT_WARRANT_GRID_SETTING, payload: http.put(`/users/${userId}/settings`, requestBody) });
};

export const editWarrant = (method: string, action: any, body: any = '') => {
  switch (method) {
    case 'PUT':
      return (http.put(action, body));
    case 'POST':
      if (action.includes('attachment')) {
        return uploadAttachment(action, body.data);
      }
      return (http.post(action, body));
    case 'DELETE':
      return (http.delete(action, { data: body?.auditDetails }));
    default:
  }
};

export const uploadAttachment = (url: any, attachment: any) => {
  const formData = new FormData();
  formData.append('attachment', attachment.file);
  const warrantURLParts = url.split('/');
  const jsonBody = {
    name: attachment.name,
    warrantId: {
      id: warrantURLParts[3],
    },
    description: attachment.description,
    size: attachment.size,
    owner: attachment.owner,
  };
  formData.append('info', JSON.stringify(jsonBody));
  const config: any = {
    headers: { 'Content-Type': 'multipart/form-data' },
  };
  return (http.post(url, formData, config));
};

export const updateWarrantState = (warrant: any) => ({
  type: UPDATE_WARRANT_STATE, payload: warrant,
});
export const updateWarrantAuditState = (warrantAndAudit: any) => ({
  type: UPDATE_WARRANT_AND_AUDIT_STATE, payload: warrantAndAudit,
});
export const setWarrantConfigInstance = (instance: any) => {
  return { type: SET_WARRANT_FORM_CONFIG_INSTANCE, payload: instance };
};
export const clearWarrantState = () => (
  { type: CLEAR_WARRANT_STATE, payload: null });
export const updateFormTemplateUsed = (templateDetails: any) => (
  { type: UPDATE_TEMPLATE_USED, payload: templateDetails });
export const clearFormTemplateUsed = () => (
  { type: CLEAR_TEMPLATE_USED, payload: null });

export const warrantUpdateProvStatus = (dfId: string, wId: string, payload: any) => (
  {
    type: UPDATE_WARRANT_PROVISIONING_STATUS,
    payload: http.put(`/warrants/${dfId}/${wId}/state`, payload),
  }
);
export const warrantReprovision = (dfId: string, wId: string, payload: any) => (
  {
    type: UPDATE_WARRANT_RETRY,
    payload: http.put(`/warrants/${dfId}/${wId}/retry`, payload),
  }
);

export const cleanUpWarrantFormData = (formdata: any) => {
  if (!formdata.cases) return;
  formdata.cases.forEach((wc: any) => {
    delete wc.cfOptions;
    delete wc.allTargetAfs;
    delete wc.hi3Data;
    if (!wc.targets) return;
    const targets: any[] = [];
    wc.targets.forEach((target: any) => {
      if (target.primaryType === 'CASEIDMIN' || target.primaryType === 'CASEIDMDN') {
        target.primaryType = 'CASEID';
      }
      if (target.primaryType === 'MDNwService') {
        target.primaryType = 'MDN';
      }
      // Only include AFs that requires AFWT to be created
      if (target.afs && target.afs.afIds) {
        const afIds: any[] = [];
        target.afs.afIds.forEach((afDetail: any) => {
          if (afDetail.bAFAssociation === undefined) {
            afIds.push({ id: afDetail.id, name: afDetail.name });
          } else if (afDetail.bAFAssociation === true) {
            afIds.push({ id: afDetail.id, name: afDetail.name });
          }
        });
        target.afs.afIds = afIds;
      }
      if (target.primaryValue || target.secondaryValue) {
        targets.push(target);
      }

      if (target.caseId?.id?.startsWith(TEMPORARY_CASE_ID_PREFIX)) {
        delete target.caseId;
      }

    });
    wc.targets = targets;
  });
};

export const broadcastSelectedWarrants = (selectedWarrants?: any) => (
  { type: BROADCAST_SELECTED_WARRANTS, selectedWarrants });
export const clearSelectedWarrants = () => (
  { type: CLEAR_SELECTED_WARRANTS });
export const setDisplayTrashWarrants = (displayTrashWarrants: boolean) => (
  { type: SET_DISPLAY_TRASH_WARRANTS, displayTrashWarrants });
export const broadcastSelectedWarrantCase = (selectedCase?: any) => (
  { type: BROADCAST_SELECTED_WARRANT_CASE, selectedCase });
export const clearSelectedWarrantCase = () => (
  { type: CLEAR_SELECTED_WARRANT_CASE });
