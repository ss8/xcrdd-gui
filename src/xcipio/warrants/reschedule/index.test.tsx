import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import {
  RescheduleDialog,
  IRescheduleDialogProps,
  IRescheduleDialogStatus,
} from './index';

describe('<RescheduleDialog />', () => {
  let testMocks: any;
  let wrapper: ShallowWrapper<IRescheduleDialogProps, IRescheduleDialogStatus, RescheduleDialog>;
  let props: IRescheduleDialogProps;

  beforeEach(() => {
    testMocks = {};

    testMocks.warrant = {
      attachments: [],
      caseOptTemplateUsed: '7',
      caseOptions: {},
      caseType: 'ALL',
      cases: [],
      cfGroup: { id: 'MA', name: '' },
      cfPool: [],
      city: '',
      comments: '',
      contact: { id: '', name: '' },
      department: '',
      dxAccess: '',
      dxOwner: '',
      id: '1234',
      judge: '',
      name: 'warrant name',
      num_of_attachments: 0,
      province: '',
      receivedDateTime: '2020-07-29T12:20:00.000Z',
      region: '',
      startDateTime: '2020-07-29T13:20:00.000Z',
      state: 'EXPIRED',
      stopDateTime: '2020-07-30T09:20:00.000Z',
      timeZone: 'America/New_York',
    };

    props = {
      timezones: ['America/New_York'],
      isAuditEnabled: false,
      isOpen: true,
      selectedWarrants: [testMocks.warrant],
      onClose: jest.fn(),
      authentication: {},
      global: {
        selectedDF: 'some df id',
      },
      clearSelectedWarrants: jest.fn(),
      warrantUpdateProvStatus: jest.fn(),
      templates: [],
      broadcastSelectedWarrants: jest.fn(),
    };

    wrapper = shallow(<RescheduleDialog {...props} />);
  });

  describe('componentDidUpdate()', () => {
    it('should convert the date/time fields to the computer timezone', () => {
      wrapper.setState({});
      expect(wrapper.instance().state.formInputs.startDateTime.value).toEqual('2020-07-29T16:20:00.000Z');
      expect(wrapper.instance().state.formInputs.stopDateTime.value).toEqual('2020-07-30T12:20:00.000Z');
    });
  });

  describe('handleSubmit()', () => {
    it('should convert the date/time fields to the warrant timezone', () => {
      jest.spyOn(wrapper.instance(), 'validateForm').mockImplementation(() => true);
      wrapper.setState({});
      wrapper.instance().handleSubmit();
      expect(wrapper.instance().formData).toEqual({
        action: 'SCHEDULE',
        startDateTime: '2020-07-29T13:20:00.000Z',
        stopDateTime: '2020-07-30T09:20:00.000Z',
        timeZone: 'America/New_York',
      });
    });
  });
});
