import React, { Fragment } from 'react';
import { TimePrecision } from '@blueprintjs/datetime';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { Warrant } from 'data/warrant/warrant.types';
import { RootState } from 'data/root.reducers';
import { getSortedTimeZoneValueLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import {
  AutoForm,
  SupportedTypes,
  updateAutoFormState,
  validateAutoForm,
  resetAutoForm,
  getAutoFormValue,
} from '../../../components/auto-form';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { WARRANT_PRIVILEGE, WARRANT_PROVSTATE } from '../warrant-enums';
import { ISOFormat, extractErrorMessage } from '../../../utils';
import {
  broadcastSelectedWarrants,
  clearSelectedWarrants,
  warrantUpdateProvStatus,
} from '../warrant-actions';
import { getStopTimeValidatorFromTemplate } from './utils';
import ActionHandler from '../actions-bar/action-handler';
import UserAudit from '../../../shared/userAudit';
import {
  AuditService, AuditActionType, getAuditDetails, getAuditFieldInfo,
} from '../../../shared/userAudit/audit-constants';
import { AppToaster, showSuccessMessage, TOAST_TIMEOUT } from '../../../shared/toaster';
import { getSuccessMessageForAction } from '../warrant-actions-handler/utils';

export interface IRescheduleDialogStatus {
  formInputs: any,
  isOpen: boolean,
  selectedWarrants: any,
  timezones: Array<any>
  fieldDirty: boolean,
  isOpenProgressBarIndicartor: boolean,
  isMultiSelect: boolean
}

export interface IRescheduleDialogProps {
  timezones: Array<any>,
  isAuditEnabled?: boolean,
  isOpen: boolean,
  selectedWarrants: any,
  onClose: any,
  authentication: any,
  global: any,
  clearSelectedWarrants: any,
  warrantUpdateProvStatus: any,
  templates: Array<any>,
  broadcastSelectedWarrants: any
  keepSelectedWarrantOnCancelReschedule?:boolean,
  isWarrantDefaultExpireEnabled?: boolean,
  warrantDefaultExpireDays?: string,
  onReloadWarrant?: () => void
}

export class RescheduleDialog extends React.Component<IRescheduleDialogProps, IRescheduleDialogStatus> {
  formRef: any;

  formData: any = {};

  auditData: any = {};

  constructor(props: IRescheduleDialogProps) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      isOpen: false,
      fieldDirty: false,
      selectedWarrants: {},
      timezones: [],
      auditFields: {},
      isOpenProgressBarIndicartor: false,
      isMultiSelect: false,
      formInputs: {
        startDateTime: {
          type: SupportedTypes.DatePicker,
          onChange: this.dateInputHandler('startDateTime'),
          timePrecision: TimePrecision.MINUTE,
          id: 'startDateTime',
          isRequired: false,
          helperText: '',
          label: 'Start',
          value: '',
          autoValidate: [
            {
              type: 'checkGivenTimeWithCurrentTime',
              field: 'timeZone',
              errorMessage: 'Start Time should be after the current time',
            },
          ],
        },
        stopDateTime: {
          type: SupportedTypes.DatePicker,
          onChange: this.dateInputHandler('stopDateTime'),
          timePrecision: TimePrecision.MINUTE,
          id: 'stopDateTime',
          isRequired: true,
          helperText: '',
          label: 'Stop',
          value: '',
          autoValidate: this.getStopTimeValidators(),
        },
        timeZone: {
          type: SupportedTypes.HTMLSelect,
          onChange: this.handleChange,
          options: getSortedTimeZoneValueLabels(new Date(), this.props.timezones),
          fill: true,
          id: 'timeZone',
          isRequired: true,
          helperText: '',
          label: 'Time Zone',
          value: '',
        },
      },
    };
  }

  getStopTimeValidators = () => {
    const { selectedWarrants } = this.props;
    const stopTimeValidators:any = [
      {
        type: 'mustBeBiggerThan',
        field: 'startDateTime',
        errorMessage: 'Stop Time should be same or after the Start Time',
      },
      {
        type: 'checkGivenTimeWithCurrentTime',
        field: 'timeZone',
        errorMessage: 'Stop Time should be after the current time',
      },
    ];
    // For single warrant selection, we would process additional requested validation as part of UI validation.
    // For multi-select, we let Bulk component handle validation error.
    if (selectedWarrants.length === 1) {
      const validator = getStopTimeValidatorFromTemplate(this.props.templates, selectedWarrants[0]);
      if (validator) {
        stopTimeValidators.push(validator);
      }
    }
    return stopTimeValidators;
  }

  private dateInputHandler = (fieldId: string) => (selectedDate: Date | null) => {
    let value = '';
    const intent = '';
    const helperText = '';
    if (selectedDate) {
      value = selectedDate.toString();
    }
    this.setState({
      fieldDirty: true,
      formInputs: {
        ...this.state.formInputs,
        [fieldId]: {
          ...this.state.formInputs[fieldId],
          value,
          intent,
          helperText,
        },
      },
    }, () => {
      // Reset Stop date-time
      if (fieldId === this.state.formInputs.startDateTime.id) {
        this.setState({
          formInputs: {
            ...this.state.formInputs,
            stopDateTime: {
              ...this.state.formInputs.stopDateTime,
              helperText: '',
              intent: '',
            },
          },
        });
      }
    });
  }

  componentDidUpdate(): void {
    const {
      selectedWarrants,
      isOpen,
      timezones,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
    } = this.props;
    const isMultiSelect = selectedWarrants.length > 1;

    if (isOpen !== this.state.isOpen) {
      this.setState({ isOpen });
    }

    if (selectedWarrants.length &&
      selectedWarrants !== this.state.selectedWarrants) {
      let startDateTime: Date | string = new Date();
      let stopDateTime: Date | string = new Date();
      let timeZone = '';
      let warrant: Partial<Warrant> = {};
      if (!isMultiSelect) {
        // Convert date/time fields to computer timezone.
        // Note that they will be converted back to warrant timezone before submit.
        warrant = WarrantAdapter.adaptWarrantToLoad(selectedWarrants[0]);

        startDateTime = warrant.startDateTime ?? '';
        timeZone = warrant.timeZone ?? '';

      } else {
        // if timezone for all selected warrants are the same, then show as default
        const allTimezonsAreTheSame = selectedWarrants.map((selectedWarrant: any) => {
          return selectedWarrant.timeZone;
        }).every((timezone: string, i: number, timezoneArray:Array<string>) =>
          timezone === timezoneArray[0]);
        timeZone = allTimezonsAreTheSame ? selectedWarrants[0].timeZone : '';
      }

      if (isWarrantDefaultExpireEnabled) {
        WarrantAdapter.setWarrantDefaultStopTime(warrant, warrantDefaultExpireDays);
      }

      stopDateTime = warrant.stopDateTime ?? '';

      this.setState({
        fieldDirty: isMultiSelect || this.props.isAuditEnabled === true,
        isMultiSelect,
        selectedWarrants,
        formInputs: {
          ...this.state.formInputs,
          startDateTime: {
            ...this.state.formInputs.startDateTime,
            value: startDateTime,
          },
          stopDateTime: {
            ...this.state.formInputs.stopDateTime,
            value: stopDateTime,
            originalValue: stopDateTime,
            autoValidate: this.getStopTimeValidators(),
          },
          timeZone: {
            ...this.state.formInputs.timeZone,
            value: timeZone,
          },
        },
      });
    }

    if (timezones && timezones !== this.state.timezones) {
      this.setState({ timezones }, () => {
        this.setState({
          formInputs: {
            ...this.state.formInputs,
            timeZone: {
              ...this.state.formInputs.timeZone,
              options: getSortedTimeZoneValueLabels(new Date(), this.state.timezones),
            },
          },
        });
      });
    }
  }

  handleOpen = () => this.setState({ isOpen: true });

  handleChange = (el: any) => {
    this.setState(updateAutoFormState(el, this.state));
  };

  handleClose = () => {
    const { keepSelectedWarrantOnCancelReschedule } = this.props;

    if (this.state.fieldDirty) {
      resetAutoForm(this.state.formInputs, this.getInitialState().formInputs);
      this.setState({ ...this.getInitialState() });
      if (!keepSelectedWarrantOnCancelReschedule) {
        this.props.clearSelectedWarrants();
      }
    }
    this.props.onClose();
  };

  validateForm = () => {
    const validate = validateAutoForm(this.state.formInputs);
    this.setState({ formInputs: validate.formInputs });
    return validate.isValid;
  };

  handleSubmit = () => {
    let isAuditValid = true;
    if (this.formRef && this.props.isAuditEnabled) {
      if (this.formRef.isValid(true, true)) {
        const addAuditFields = this.formRef?.getValue();
        this.auditData = getAuditFieldInfo(null, addAuditFields);
      } else isAuditValid = false;
    }

    if (this.validateForm() && isAuditValid) {
      const req = getAutoFormValue(this.state.formInputs);
      const data = {
        action: WARRANT_PROVSTATE.Schedule,
        ...req,
        startDateTime: ISOFormat(req.startDateTime),
        stopDateTime: ISOFormat(req.stopDateTime),
      };

      // Convert date/time fields to the warrant timezone
      this.formData = WarrantAdapter.adaptWarrantDateTimeFieldsToSubmit(data);

      if (!this.state.isMultiSelect) {
        const warrant = this.state.selectedWarrants[0];

        this.handleAction(warrant)
          .then(() => {
            const { onReloadWarrant, onClose } = this.props;
            if (onReloadWarrant) {
              onReloadWarrant();
              onClose();
            } else {
              this.handleClose();
            }

            const message = getSuccessMessageForAction(warrant.name, COMMON_GRID_ACTION.Reschedule);
            showSuccessMessage(message, TOAST_TIMEOUT);
          })
          .catch((e: any) => {
            AppToaster.show({
              icon: 'error',
              intent: 'danger',
              timeout: 0,
              message: `Error: ${extractErrorMessage(e)}`,
            });
          });
      } else this.handleBulkAction();
    }
  };

  handleAction = async (warrant: any) => {
    const { selectedDF: dfId } = this.props.global;
    const { id } : any = warrant;
    let auditDetails = null;
    if (this.props.isAuditEnabled) {
      const name = warrant?.name;
      const userId = this.props.authentication.userDBId;
      const userName = this.props.authentication.userId;
      auditDetails = getAuditDetails(
        this.props.isAuditEnabled,
        userId,
        userName,
        this.auditData,
        name,
        AuditService.WARRANTS,
        AuditActionType.WARRANT_RESCHEDULE,
      );
    }
    const payload = { data: this.formData, auditDetails };
    await this.props.warrantUpdateProvStatus(dfId, id, payload);
  }

  handleBulkAction = () => {
    // apply validation to each warrant, let bulk handle errors
    const { formInputs } = this.state;
    const validatedSelectedWarrants = this.props.selectedWarrants.map((warrant:any) => {
      const validator = getStopTimeValidatorFromTemplate(this.props.templates, warrant);
      if (validator) {
        formInputs.stopDateTime.value = this.formData.stopDateTime;
        formInputs.stopDateTime.originalValue = warrant.stopDateTime;
        formInputs.stopDateTime.autoValidate[0] = validator;
      }
      const { isValid, errorMessage } = validateAutoForm(formInputs);
      warrant.autoFormValidation = { isValid, errorMessage };
      return warrant;
    });
    this.props.broadcastSelectedWarrants(validatedSelectedWarrants);
    this.setState({ isOpenProgressBarIndicartor: true });
  };

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToEdit = privileges !== undefined
           && privileges.includes(WARRANT_PRIVILEGE.Edit);

    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: CONFIG_FORM_MODE.Edit,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.handleClose,
      onSubmit: this.handleSubmit,
      getTitle: () => `Schedule Warrant${this.state.isMultiSelect ? 's' : `: ${this.state.selectedWarrants[0]?.name}`}`,
      isOpen: this.state.isOpen,
      doNotDisplayEdit: true,
      width: 'auto',
      height: 'auto',
      overflow: 'auto',
      className: 'configFormPopup',
    };
    return (
      <Fragment>
        <ConfigFormPopup {...configFormPopupProperties}>
          <AutoForm formInputs={this.state.formInputs} />
          {this.props.isAuditEnabled && (
          <UserAudit
            data-test="userAudit"
            ref={(ref: any) => { this.formRef = ref; }}
            mode={CONFIG_FORM_MODE.Edit}
          />
          )}
        </ConfigFormPopup>
        <ActionHandler
          isOpen={this.state.isOpenProgressBarIndicartor}
          onClose={this.handleClose}
          handleAction={this.handleAction}
        />
      </Fragment>

    );
  }
}

const mapStateToProps = ({
  authentication,
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isAuditEnabled,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
    },
  },
  global,
  users: { timezones },
  warrants: { selectedWarrants, templates },
}: RootState) => ({
  authentication,
  isAuditEnabled,
  global,
  selectedWarrants,
  templates,
  timezones,
  isWarrantDefaultExpireEnabled,
  warrantDefaultExpireDays,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  broadcastSelectedWarrants,
  clearSelectedWarrants,
  warrantUpdateProvStatus,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RescheduleDialog);
