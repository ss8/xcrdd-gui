export const getStopTimeValidatorFromTemplate = (templates: any, warrant: any) => {
  let validation: any;
  validation = templates.find(
    (template:any) => template.id === warrant.templateUsed,
  );
  if (validation) {
    try {
      validation = JSON.parse(validation.template);
      return validation.general?.fields?.stopDateTime?.validation?.validators?.find(
       (validator:any) => validator.type === 'dateRangeBasedOnLastSavedValue');
    } catch (error) {
      return false;
    }
  }
  return false;
};
