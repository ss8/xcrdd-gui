import React, { ReactNode } from 'react';
import { Contact } from 'data/types';
import { CaseModel } from 'data/case/case.types';
import { TargetModel, TargetModelMap } from 'data/target/target.types';
import { connect } from 'react-redux';
import { Team } from 'data/team/team.types';
import { Warrant } from 'data/warrant/warrant.types';
import { RootState } from 'data/root.reducers';
import { FormTemplateSection, Template } from 'data/template/template.types';
import { dateTimeFormatter } from '../../../shared/constants';
import { FormPanel } from '../../../shared/wizard/with-discovery-wizard-step-panel';
import './warrant-wizard.scss';
import { DevicePanel } from '../../devices';

interface SummaryProps {
  editMode: boolean; // true if show original value
  changeTracker?: any;
  templateUsed?: Template | null;
  warrant?: any;
  cases?: any;
  derivedTargets?:any;
  warrantConfigInstance?:any;
  contacts?: Contact[];
  teams?: Team[];
  targetsById: TargetModelMap;
}

interface Field {
  group: string;
  field: string;
  label: any;
  value: any;
  original: any;
}

interface FieldMap {
  name: { label: string, value: string },
  template: { label: string, value: string },
  receivedDateTime: { label: string, value: string },
  startDateTime: { label: string, value: string },
  stopDateTime: { label: string, value: string },
  timeZone: { label: string, value: string },
  caseType: { label: string, value: string },
  lea: { label: string, value: string },
  caseOptTemplateUsed: { label: string, value: string },
  contact: { label: string, value: string },
  visibility: { label: string, value: string },
  attachments: { label: string, value: string[] }
}

export class Summmary extends React.Component<SummaryProps, any> implements FormPanel {
  warrantFields:Field[] = [];

  serviceOptions:{label:string, value:string}[] = [];

  devices: any;

  cases:any = {};

  afcccMap: any = {};

  includedAfMap: any = {};

  includedAfAutoTypeMap: any = {};

  includedAfAutoGroupMap: any = {};

  includedAfGroupMap: any = {};

  guiListId = 0;

  constructor(props:SummaryProps) {
    super(props);
    this.initializeData();
    this.serviceOptions = this.props.warrantConfigInstance.getServiceDropDownOptions();
    this.state = {
      valid: true,
    };
  }

  isNewCase = (aCase:any) => (this.props.changeTracker.isNewCase(aCase.id) || !this.props.editMode);

  static getContact = (contacts?: Contact[], warrant?:Warrant) => {
    if (!contacts || !warrant) {
      return null;
    }

    const contactFound = contacts.find((contact) => contact.id === warrant.contact?.id);
    if (contactFound) {
      return contactFound.name;
    }

    return '';
  }

  static getVisibility = (teams?: Team[], warrant?:Warrant) => {
    if (!teams || !warrant) {
      return null;
    }

    const teamFound = teams.find((team) => team.id === warrant.dxOwner);

    if (teamFound) {
      return teamFound.name;
    }

    return '';
  }

  initializeData = () => {
    // warrant
    this.warrantFields.push({
      group: `warrant.${this.props.changeTracker.getWarrantId()}`, field: 'name', label: 'Warrant Name', value: this.props.warrant.name, original: null,
    });
    this.warrantFields.push({
      group: `warrant.${this.props.changeTracker.getWarrantId()}`, field: 'template', label: 'Template', value: this.props.templateUsed?.name, original: null,
    });
    this.warrantFields.push({
      group: `warrant.${this.props.changeTracker.getWarrantId()}`, field: 'contact', label: 'Contact', value: Summmary.getContact(this.props.contacts, this.props.warrant), original: null,
    });
    this.warrantFields.push({
      group: `warrant.${this.props.changeTracker.getWarrantId()}`, field: 'visibility', label: 'Visibility', value: Summmary.getVisibility(this.props.teams, this.props.warrant), original: null,
    });

    const warrantCaseOptionFields:any[] = [];

    // Create a data structure to be used for displaying the summary.
    this.props.cases.forEach((aCase:any) => {
      // this.cases is a data structure to keep track of the case related fields for display.
      this.cases[aCase.id] = {
        // eslint-disable-next-line max-len
        id: aCase.id, case: aCase, liIdField: null, hi3InterfaceFields: null, fields: [],
      };

      // for warrant creation, collect the list of AF Group, AF Type and AF Autowire Group
      // from the case itself.
      if (this.props.editMode === false) {
        aCase.targets.forEach((target:any) => {
          target.afs.afIds.forEach((af:any) => {
            this.addAfLabel(aCase.id, target.id, af.name, this.includedAfMap);
          });

          target.afs.afGroups.forEach((afGroup:any) => {
            this.addAfLabel(aCase.id, target.id, afGroup.name, this.includedAfGroupMap);
          });

          target.afs.afAutowireTypes.forEach((afAutowireType:any) => {
            this.addAfLabel(aCase.id, target.id, afAutowireType.name, this.includedAfAutoTypeMap);
          });

          target.afs.afAutowireGroups.forEach((afAutowireGroup:any) => {
            this.addAfLabel(aCase.id, target.id, afAutowireGroup.name, this.includedAfAutoGroupMap);
          });
        });
      }
    });

    const fieldSummary = this.props.changeTracker.getSummary();

    fieldSummary.forEach((field: any) => {
      // warrant
      if (field.group.startsWith('warrant.')) {
        if (field.field === 'name') {
          this.warrantFields.shift(); // remove the first item that is the case name.
          this.warrantFields.unshift(field);
        } else if (field.field.startsWith('caseOptTemplateUsed.')) {
          // display case option template name before the option parameters.
          warrantCaseOptionFields.unshift(field);
        } else {
          this.warrantFields.push(field);
        }

      // warrant case options
      } else if (field.group.startsWith('case.undefined') && field.field.startsWith('caseOption.')) {
        warrantCaseOptionFields.push(field);

      // afccc
      } else if (field.group.startsWith('case.') && field.field.startsWith('af.') && field.field.indexOf('.afccc.') > -1) {
        this.addAfCccField(field);

      // cases
      } else if (field.group.startsWith('case.') && !(field.field.startsWith('af.') || field.field.startsWith('afGrp.') || field.field.startsWith('afAutoType.') || field.field.startsWith('afAutoGrp.'))) {
        this.addCaseField(field);
      }
    });
    if (this.props.editMode === false) { // create mode
      const { attachments } = this.props.warrant;
      if (attachments && attachments.length > 0) {
        const field = {
          group: 'warrant.NEWID+0', field: 'attachments', original: '', value: [], action: '', label: 'Attachments',
        };
        field.value = attachments.map((attachment:any) => attachment.name);
        this.warrantFields.push(field);
      }
    }
    this.warrantFields = this.warrantFields.concat(warrantCaseOptionFields);

    this.devices = DevicePanel.getDevices(
      this.props.warrant,
      this.props.cases,
      this.props.derivedTargets,
      this.props.targetsById,
    );
  }

  addCaseField = (changeTrackerField: any) => {
    const parts:string[] = changeTrackerField.group.split('.');

    // no case in redux matches the case in changeTracker
    const aCase = this.cases[parts[1]];
    if (!aCase) return;

    // do not display field includeInWarrant
    if (changeTrackerField.field === 'includeInWarrant') return;

    // Case name was modified, use the changeTrackerField
    if (changeTrackerField.field === 'liId') {
      aCase.fields.shift(); // remove the first item that is the case name.
      aCase.fields.unshift(changeTrackerField); // add the case name change record
      return;
    }

    // for warrant edit
    if (this.props.editMode) {
      if (changeTrackerField.field === 'liId') {
        aCase.liIdField = changeTrackerField;
      } else {
        aCase.fields.push(changeTrackerField);
      }

    // for warrant creation:
    } else if (changeTrackerField.field.startsWith('HI3Interfaces.')) {
      // display all HI3 interfaces if there is a modification
      if (aCase.hi3InterfaceFields === null
        && aCase.case.hi3Interfaces
        && aCase.case.hi3Interfaces.length > 0) {
        aCase.hi3InterfaceFields = {
          group: `case.${parts[1]}`,
          field: 'HI3Interfaces',
          label: (aCase.case.hi3Interfaces && aCase.case.hi3Interfaces.length > 1) ? 'Communication Content Interfaces' : 'Communication Content Interface',
          original: null,
          value: aCase.case.hi3Interfaces.map(
            (hi3Interface:any) => (hi3Interface.name),
          ),
        };
        aCase.bAddedHI3InterfaceField = true;
      }
    } else if (changeTrackerField.field === 'target'
      || changeTrackerField.field === 'cfId'
      || changeTrackerField.field === 'services') {
      // ignore because target, CF, services is already added in initializeData
    } else {
      aCase.fields.push(changeTrackerField);
    }
  }

  addAfCccField = (field:any) => {
    const parts: string[] = field.group.split('.');
    let targets = this.afcccMap[parts[1]];
    if (!targets) {
      targets = {};
      this.afcccMap[parts[1]] = targets;
    }
    let afMap = targets[parts[3]];
    if (!afMap) {
      afMap = {};
      targets[parts[3]] = afMap;
    }
    const fieldParts: string[] = field.field.split('.');
    let afcccList: any[] = afMap[fieldParts[1]];
    if (!afcccList) {
      afcccList = [];
      afMap[fieldParts[1]] = afcccList;
    }
    afcccList.push(field);
  }

  addAfLabel = (caseId:string, targetId: string, fieldLabel:string, excludeMap: any) => {
    let targets = excludeMap[caseId];
    if (!targets) {
      targets = {};
      excludeMap[caseId] = targets;
    }
    let excludedItems = targets[targetId];
    if (!excludedItems) {
      excludedItems = [];
      targets[targetId] = excludedItems;
    }
    excludedItems.push(fieldLabel);
  }

  isValid = (errors:string[]) => {
    return this.state.valid === true;
  }

  submit = async () => {
    return true;
  }

  isFieldVisibleInTemplate(fieldName: string): boolean {
    const {
      templateUsed,
    } = this.props;

    if (templateUsed == null) {
      return false;
    }

    const formTemplateSection = templateUsed.template.general as FormTemplateSection;
    const field = formTemplateSection?.fields?.[fieldName];
    if (field == null) {
      return false;
    }

    return field.hide !== 'true' ?? false;
  }

  toTargetLabel = (targetName: string) => {
    return this.props.warrantConfigInstance.getConfiguration().config.targets[targetName].title;
  };

  toServiceLabel = (serviceValue: string) => {
    const findItem: { label: string; value: string } | undefined = this.serviceOptions.find(
      (item: { label: string; value: string }) => item.value === serviceValue,
    );
    if (findItem) {
      return findItem.label;
    }
    return serviceValue;
  };

  static toDateString(value: string | Date): string {
    if (value == null || value === '') {
      return '';
    }

    if (typeof value === 'string') {
      value = new Date(value);
    }

    return dateTimeFormatter(value);
  }

  toString = (value:any) => {
    if (value === null || value === undefined) return '';
    if (value instanceof Date) {
      return dateTimeFormatter(value);
    }

    return value.toString();
  }

  nextId = () => { this.guiListId += 1; return this.guiListId; }

  renderTargetCCC = (afccc: any, afKey: any, target: any, aCase: any, i: number) => {
    return (
      <React.Fragment key={this.nextId()}>
        {i === 0 ? (
          <div key={this.nextId()}>
            <div style={{ textAlign: 'left' }}>Target Info for {this.afcccMap[aCase.id][target.id][afKey][0].label.afLabel}</div>
          </div>
        ) : null}
        <li key={this.nextId()}>
          {this.renderSummaryField(afccc.label.fieldLabel, afccc.value)}
        </li>
      </React.Fragment>
    );
  };

  renderTargetCCCList = (aCase: any, target: any) => {
    const afKeys: any[] = Object.keys(this.afcccMap[aCase.id][target.id]);
    if (afKeys && afKeys.length > 0) {
      return (
        <React.Fragment key={this.nextId()}>
          {afKeys.map((afKey: any) =>
            (this.afcccMap[aCase.id][target.id][afKey].length > 0
              ? this.afcccMap[aCase.id][target.id][afKey].map((afccc: any, i: number) =>
                this.renderTargetCCC(afccc, afKey, target, aCase, i))
              : null))}
        </React.Fragment>
      );
    }
    return null;
  };

  limitStringLength = (str:string, maxLength:number) => {
    if (!str) return str;
    if (str.length > maxLength) {
      return `${str.substring(0, maxLength - 3)}...`;
    }
    return str;
  }

  renderItemMap = (itemMap: any, aCase: any, target: any, label: string) => {
    return aCase && itemMap[aCase.id] && itemMap[aCase.id][target.id]
    && itemMap[aCase.id][target.id].length > 0 ? (
      <tr key={this.nextId()}>
        <th style={{ textAlign: 'left' }}>{label}:</th>
        <td style={{ wordWrap: 'break-word', whiteSpace: 'normal' }}>
          {this.limitStringLength(itemMap[aCase.id][target.id].join(', '), 1000)}
        </td>
      </tr>
      ) : null;
  };

  renderSummaryItemMap = (itemMap: any, aCase: any, target: any, label: string) => {
    return aCase && itemMap[aCase.id] && itemMap[aCase.id][target.id]
    && itemMap[aCase.id][target.id].length > 0 ? (
      <li key={this.nextId()}>
        {this.renderSummaryField(label, this.limitStringLength(itemMap[aCase.id][target.id].join(', '), 1000))}
      </li>
      ) : null;
  };

  hasAfOrCCC = (aCase: any, target: any) =>
    aCase &&
    ((this.afcccMap[aCase.id] &&
      this.afcccMap[aCase.id][target.id] &&
      Object.keys(this.afcccMap[aCase.id][target.id]).length > 0) ||
      (aCase &&
        this.includedAfMap[aCase.id] &&
        this.includedAfMap[aCase.id][target.id] &&
        this.includedAfMap[aCase.id][target.id].length > 0) ||
      (aCase &&
        this.includedAfGroupMap[aCase.id] &&
        this.includedAfGroupMap[aCase.id][target.id] &&
        this.includedAfGroupMap[aCase.id][target.id].length > 0) ||
      (aCase &&
        this.includedAfAutoTypeMap[aCase.id] &&
        this.includedAfAutoTypeMap[aCase.id][target.id] &&
        this.includedAfAutoTypeMap[aCase.id][target.id].length > 0) ||
      (aCase &&
        this.includedAfAutoGroupMap[aCase.id] &&
        this.includedAfAutoGroupMap[aCase.id][target.id] &&
        this.includedAfAutoGroupMap[aCase.id][target.id].length > 0));

  renderSummaryField = (label:string, value: number | string) => {
    return (
      <div className="summary-card-field">
        <strong className="summary-card-field-label">{label}: </strong>
        <span className="summary-card-field-value">{this.toString(value)}</span>
      </div>
    );
  }

  renderSummaryFieldEllipsis(label: string, value: string): ReactNode {
    return (
      <div className="summary-card-field summary-card-field-ellipsis">
        <strong className="summary-card-field-label">{label}: </strong>
        <span className="summary-card-field-value" title={value}>{this.toString(value)}</span>
      </div>
    );
  }

  renderTargets = (target: any, aCase: any) => {
    return (
      <React.Fragment key={this.nextId()}>
        {target.primaryValue ?
          this.renderSummaryField(`${this.toTargetLabel(target.primaryType)}`, target.primaryValue) : null}
        {target.secondaryValue ?
          this.renderSummaryField(`${this.toTargetLabel(target.secondaryType)}`, target.secondaryValue) : null}
        {(target.primaryValue || target.secondaryValue) && this.hasAfOrCCC(aCase, target) ? (
          <ul className="summary-card-target-sub-items" key={this.nextId()}>
            {this.renderSummaryItemMap(this.includedAfMap, aCase, target, 'AFs')}
            {this.renderSummaryItemMap(this.includedAfGroupMap, aCase, target, 'AF Groups')}
            {this.renderSummaryItemMap(this.includedAfAutoTypeMap, aCase, target, 'AF Autowire Types')}
            {this.renderSummaryItemMap(this.includedAfAutoGroupMap, aCase, target, 'AF Autowire Groups')}
            {aCase && this.afcccMap[aCase.id] && this.afcccMap[aCase.id][target.id]
              ? this.renderTargetCCCList(aCase, target)
              : null}
          </ul>
        ) : null}
      </React.Fragment>
    );
  };

  renderDeviceItem = (aDevice:any) => {
    const targetTypes:any[] = Object.keys(aDevice.types);
    return (
      <div className="summary-card">
        <div className="summary-card-info">
          {this.renderSummaryField('Device', this.toString(aDevice.device))}
          {this.renderSummaryField('Service', aDevice.services.map((service: string) => this.toServiceLabel(service)).join(', '))}
          { (targetTypes && targetTypes.length > 0) ?
            targetTypes.map((type:any) =>
              aDevice.types[type].map((element:any) =>
                (element.display ? (this.renderTargets(element.target, null))
                  : null))) : null}
        </div>
      </div>
    );
  }

  renderCaseCF(cfId: {name: string, id: string}) {
    if (!cfId) {
      return null;
    }

    const cfName = cfId.name || cfId.id;

    return this.renderSummaryField('CF', cfName);
  }

  renderHi3InterfaceFields(hi3InterfaceFields: Field): JSX.Element | null {
    if (!hi3InterfaceFields || !hi3InterfaceFields.label) {
      return null;
    }

    return this.renderSummaryField(hi3InterfaceFields.label, hi3InterfaceFields.value);
  }

  renderCaseField = (field: Field) => {
    if (!field.label) return null;

    const aFieldLabel = field.label.fieldLabel || field.label;
    const { value } = field;

    return this.renderSummaryField(aFieldLabel, value);

  }

  renderCaseFields(fields: Field[]) {

    if (!fields?.length) {
      return null;
    }

    return fields.map(this.renderCaseField);
  }

  renderCaseTargets= (caseModel: CaseModel) => {
    if (!caseModel.targets || !caseModel.targets.length) {
      return null;
    }

    return caseModel.targets.map((target: TargetModel) => this.renderTargets(target, caseModel));
  }

  renderNewCase(aCase:any) {
    return (
      <div key={this.nextId()} className="summary-card summary-card-case">
        {this.renderSummaryField('Name', this.toString(aCase.case.liId))}
        {this.renderSummaryField('Service', aCase.case.services.map((service: string) => this.toServiceLabel(service)).join(', '))}
        <div>
          {this.renderCaseCF(aCase.case.cfId)}
          {this.renderHi3InterfaceFields(aCase.hi3InterfaceFields)}
          {this.renderCaseFields(aCase.fields)}
          {this.renderCaseTargets(aCase.case)}
        </div>
      </div>
    );
  }

  renderField = (field: {label:any, value:any, original:any},
    index:number) => {
    if (!field.label) return null;
    let aFieldLabel = field.label;
    if (field.label.fieldLabel) aFieldLabel = field.label.fieldLabel;
    return (
      <tr key={this.nextId()}>
        <th className={index === 0 ? 'summary-table-first-row' : undefined}>{aFieldLabel}:</th>
        <td className={index === 0 ? 'summary-table-first-row' : undefined}>{this.toString(field.value)}</td>
        { (this.props.editMode) ?
          <td className={index === 0 ? 'summary-table-first-row' : undefined} style={{ color: 'lightSlateGray', textDecoration: 'line-through', textAlign: 'left' }}> {this.toString(field.original)}</td> : null }
      </tr>
    );
  }

  static getWarrantFieldsMap = (warrantFields:Field[]):FieldMap => {
    const initialValue = {
      name: { label: '', value: '' },
      template: { label: '', value: '' },
      receivedDateTime: { label: '', value: '' },
      startDateTime: { label: '', value: '' },
      stopDateTime: { label: '', value: '' },
      timeZone: { label: '', value: '' },
      caseType: { label: '', value: '' },
      lea: { label: '', value: '' },
      caseOptTemplateUsed: { label: '', value: '' },
      contact: { label: '', value: '' },
      visibility: { label: '', value: '' },
      attachments: { label: '', value: [] },
    };
    return warrantFields.reduce((previous, fieldItem) => ({ ...previous, [fieldItem.field]: fieldItem }), initialValue);
  }

  renderWarrant = () => {

    const warrantFieldsMap = Summmary.getWarrantFieldsMap(this.warrantFields);

    return (
      <div>
        <div className="summary-title">Warrant Information</div>
        <div id="summary" className="summary-card summary-warrant">
          <div className="summary-card-info">
            { this.renderSummaryField(warrantFieldsMap.name.label, this.toString(warrantFieldsMap.name.value)) }
            { this.renderSummaryField(warrantFieldsMap.template.label, this.toString(warrantFieldsMap.template.value)) }
            { this.isFieldVisibleInTemplate('receivedDateTime') && (
              this.renderSummaryField(
                warrantFieldsMap.receivedDateTime.label || 'Received Time',
                Summmary.toDateString(warrantFieldsMap.receivedDateTime.value),
              )
            )}
          </div>

          <div className="summary-card-info">
            {
              this.renderSummaryField(
                warrantFieldsMap.startDateTime.label || 'Start Time',
                Summmary.toDateString(warrantFieldsMap.startDateTime.value),
              )
            }
            {
              this.renderSummaryField(
                warrantFieldsMap.stopDateTime.label,
                Summmary.toDateString(warrantFieldsMap.stopDateTime.value),
              )
            }
            { this.renderSummaryField(warrantFieldsMap.timeZone.label || 'Time Zone', this.toString(warrantFieldsMap.timeZone.value)) }
          </div>
          <div className="summary-card-info">
            {this.renderSummaryField(
              warrantFieldsMap.contact.label,
              this.toString(warrantFieldsMap.contact.value),
            )}
            {this.renderSummaryField(
              warrantFieldsMap.visibility.label,
              this.toString(warrantFieldsMap.visibility.value),
            )}
            {this.renderSummaryField(
              warrantFieldsMap.lea.label,
              this.toString(warrantFieldsMap.lea.value),
            )}
          </div>
          <div className="summary-card-info">
            {this.renderSummaryField(
              warrantFieldsMap.caseType.label,
              this.toString(warrantFieldsMap.caseType.value),
            )}
            {this.renderSummaryField(
              warrantFieldsMap.caseOptTemplateUsed.label,
              this.toString(warrantFieldsMap.caseOptTemplateUsed.value),
            )}
            {this.isFieldVisibleInTemplate('attachmentsDetails') && (
              this.renderSummaryFieldEllipsis(
                warrantFieldsMap.attachments.label || 'Attachments',
                this.toString(warrantFieldsMap.attachments.value.join(', ')),
              )
            )}
          </div>
        </div>
      </div>
    );
  }

  renderCase = (aCase:any) => {
    if (!aCase) {
      return;
    }

    if (aCase.case.includeInWarrant === false) return;

    // render new case
    if (this.isNewCase(aCase)) {
      return this.renderNewCase(aCase);
    }

    // render existing case
    return (
      <div key={this.nextId()} className="summary-card">
        <tbody>
          { /* render case name first */ }
          { aCase.liIdField ? this.renderField(aCase.liIdField, 0) : this.renderField({ label: 'Case Name', value: aCase.case.liId, original: null }, 0)}
          { /* render other case fields first */ }
          { aCase.fields.map((field:any, i:number) => this.renderField(field, i + 1)) }
        </tbody>
      </div>
    );
  }

  renderCases= () => {
    const caseIds:string[] = Object.keys(this.cases);
    if (caseIds && caseIds.length > 0) {
      return (
        <div>
          <div className="summary-title">Cases</div>
          <div className="summary-cards">
            {caseIds.map((caseId:any, i:number) => this.renderCase(this.cases[caseId]))}
          </div>
        </div>
      );
    }
    return null;
  }

  renderDevices = () => {
    const deviceKeys:string[] = Object.keys(this.devices);
    return (
      deviceKeys.length > 0 ? (
        <div>
          <div className="summary-title">Devices</div>
          <div className="summary-cards">
            { deviceKeys.map((key:any) => this.renderDeviceItem(this.devices[key])) }
          </div>
        </div>
      ) : null
    );
  }

  renderSummaryAdvertise = () => {
    return (
      <p className="summary-advertise">
        Please review the Warrant, Devices and Cases information. You can go back and update any information you need.{' '}
        If all is correct, go ahead and finish the process and the warrant and cases will be created.
      </p>
    );
  }

  render() {
    return (
      <div style={{ width: '100%', marginTop: '5px' }}>
        { this.renderSummaryAdvertise()}
        { this.renderWarrant() }
        { this.renderDevices() }
        { this.renderCases() }
      </div>
    );
  }
}

const mapStateToProps = ({
  warrants: {
    warrantConfigInstance, warrant, templateUsed, customizationconfig, changeTracker,
  },
  cases: { cases },
  targets: { byId: targetsById },
  contacts: { contacts },
  users: { teams },
}: RootState) => ({
  warrantConfigInstance,
  templateUsed,
  warrant,
  changeTracker,
  derivedTargets: customizationconfig.derivedTargets,
  cases,
  contacts,
  teams,
  targetsById,
});

export default connect(mapStateToProps, null, null,
  { forwardRef: true })(Summmary);
