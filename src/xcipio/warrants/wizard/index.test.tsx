import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import {
  fireEvent,
  getByPlaceholderText,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import moment from 'moment';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as discoveryActions from 'xcipio/discovery-xcipio-actions';
import { getAccessFunctionsByFilter } from 'data/accessFunction/accessFunction.api';
import AuthActions from 'data/authentication/authentication.types';
import { mockTemplates } from '__test__/mockTemplates';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockTeams } from '__test__/mockTeams';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockCustomization } from '__test__/mockCustomization';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import {
  mockAccessFunctions1,
  mockAccessFunctions2,
  mockCollectionFunctions,
} from 'routes/Warrant/containers/WarrantEdit/__test__/mockWarrantEditData';
import WarrantWizard from './index';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<WarrantWizard />', () => {
  let warrantConfigurationInstance: WarrantConfiguration;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['tp_access'],
    };

    warrantConfigurationInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: (...args) => getAccessFunctionsByFilter(...args).then(({ data }) => ({
        value: { data },
      })),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: contactActions.GET_ALL_CONTACTS_FULFILLED,
      payload: buildAxiosServerResponse(mockContacts),
    });

    store.dispatch({
      type: userActions.GET_USER_TEAMS_FULFILLED,
      payload: buildAxiosServerResponse(mockTeams),
    });

    store.dispatch({
      type: warrantActions.SET_WARRANT_FORM_CONFIG_INSTANCE,
      payload: warrantConfigurationInstance,
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockCustomization]),
    });

    axiosMock.onGet('/af-groups/0').reply(200, { data: [] });
    axiosMock.onGet('/cfs/0').reply(200, { data: mockCollectionFunctions });
    axiosMock.onGet('/afs/1?$filter=tag eq \'NSN_SDM\' or tag eq \'ERK_SMF\' or tag eq \'MVNR_5GSMSC\' or tag eq \'NOK_5GAMF\' or tag eq \'NOK_5GSMSF\' or tag eq \'NOK_5GUDM\' or tag eq \'NOK_5GSMF\' or tag eq \'ERK_5GAMF\'').reply(200, { data: mockAccessFunctions1 });
    axiosMock.onGet('/afs/1?$filter=tag eq \'LU3G_CSCF\' or tag eq \'ACME_X123\' or tag eq \'GENVOIP\' or tag eq \'NSN_CSCF\' or tag eq \'SONUS_SBC\' or tag eq \'PVOIP\' or tag eq \'ALU_DSS\' or tag eq \'NORTEL_MTX\' or tag eq \'NORTEL_HLR\' or tag eq \'SS8_DF\' or tag eq \'NORTEL_MSC\' or tag eq \'NORTEL_DF\' or tag eq \'HP_HLR\' or tag eq \'SAMSUNG_MSC\' or tag eq \'MVNR_SBC\' or tag eq \'MVNR_HSS\' or tag eq \'MITEL_TAS\' or tag eq \'MVNR_CSCF\' or tag eq \'MVNR_PGW\' or tag eq \'EPS\' or tag eq \'ERK_SMF\' or tag eq \'ERK_VMME\' or tag eq \'ALU_MME\' or tag eq \'PCRF\' or tag eq \'CISCOVPCRF\' or tag eq \'HP_AAA\' or tag eq \'ALU_HLR\' or tag eq \'NSN_SAEGW\' or tag eq \'MV_SMSIWF\' or tag eq \'STA_GGSN\' or tag eq \'NSNHLR_SDM_PRGW\' or tag eq \'MVNR_5GSMSC\' or tag eq \'NOK_5GAMF\' or tag eq \'NOK_5GSMSF\' or tag eq \'NSN_SDM\' or tag eq \'NOK_5GUDM\' or tag eq \'NOK_5GSMF\' or tag eq \'ERK_5GAMF\' or tag eq \'MVNR_RCS_MSTORE\' or tag eq \'ICS_RCS\' or tag eq \'MVNR_RCS_AS\' or tag eq \'ALU_SGW\' or tag eq \'ERK_SGW\' or tag eq \'STA_PDSN\' or tag eq \'STARENT_EHA\' or tag eq \'BRW_AAA\' or tag eq \'ERK_SCEF\' or tag eq \'KODIAK_PTT\' or tag eq \'GIZMO\' or tag eq \'ETSI_GIZMO\'').reply(200, { data: mockAccessFunctions2 });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      /** *************** */
      // IMPORTANT: if test fails after you have modified the mockSupportedFeatures, most likely you need
      // to update one of the axiosMock.onGet() lines above. Here is how to fix it:
      //   1. Uncomment the console.log(config) line below
      //   2. Run the failed test
      //   3. The console.log(config) will output an unhandled HTTP request.  Copy the request from the console.log
      //      output into one of the axiosMock.onGet() lines above that has the matching prefix
      // console.log(config);

      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Next buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());

    expect(screen.getByText('Warrant Information')).toBeVisible();
    expect(screen.getByText('Devices')).toBeVisible();

    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Next');
    expect(items).toHaveLength(2);
  });

  it('should load the Warrant Information', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    expect(screen.getByLabelText('Warrant Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Start Time:')).toHaveValue('');
    expect(screen.getByLabelText('Stop Time:*')).toHaveValue('');
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Visibility:')).toHaveDisplayValue('Unrestricted');
    expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Intercept Type:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Case Options Template:*')).toHaveDisplayValue('Select');
    expect(screen.getByText('Case Options')).toBeVisible();
    expect(screen.getByLabelText('Comments:')).toHaveValue('');
  });

  it('should load the Warrant Information with the default stop time if preference is set', async () => {
    const dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));

    store.dispatch({
      type: discoveryActions.GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    expect(screen.getByLabelText('Warrant Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Start Time:')).toHaveValue('');
    expect(screen.getByLabelText('Stop Time:*')).toHaveValue('06/27/21 11:59:00 PM');
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Visibility:')).toHaveDisplayValue('Unrestricted');
    expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Intercept Type:*')).toHaveDisplayValue('Select');
    expect(screen.getByLabelText('Case Options Template:*')).toHaveDisplayValue('Select');
    expect(screen.getByText('Case Options')).toBeVisible();
    expect(screen.getByLabelText('Comments:')).toHaveValue('');

    dateMock.mockRestore();
  });

  it('should keep the stop time as configured when going to cases and back', async () => {
    const dateMock = jest.spyOn(Date, 'now').mockImplementation(() => +new Date('2021-04-28 12:00:00.000'));

    const date = moment(new Date()).add('1', 'M').format('MM/DD/YY hh:mm:ss A');

    store.dispatch({
      type: discoveryActions.GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Warrant Name:*'), { target: { value: 'Warrant name' } });

    expect(screen.getByLabelText('Stop Time:*')).toHaveValue('06/27/21 11:59:00 PM');
    fireEvent.change(screen.getByLabelText('Stop Time:*'), { target: { value: date } });
    expect(screen.getByLabelText('Stop Time:*')).toHaveValue(date);

    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/Los_Angeles' } });
    fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'Any' } });
    fireEvent.change(screen.getByLabelText('Intercept Type:*'), { target: { value: 'CD' } });
    fireEvent.change(screen.getByLabelText('Case Options Template:*'), { target: { value: '3' } });

    fireEvent.click(screen.getAllByText('Next')[0]);

    await waitFor(() => expect(screen.getByText('No cases because no devices were entered')).toBeVisible());

    fireEvent.click(screen.getAllByText('Back')[0]);

    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    expect(screen.getByLabelText('Stop Time:*')).toHaveValue(date);

    dateMock.mockRestore();
  });

  it('should keep the Case editions when back to Warrant and next again to cases', async () => {
    const date = moment(new Date()).add('1', 'M').format('MM/DD/YY hh:mm:ss A');

    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Warrant Name:*'), { target: { value: 'Warrant name' } });
    fireEvent.change(screen.getByLabelText('Stop Time:*'), { target: { value: date } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/Los_Angeles' } });
    fireEvent.change(screen.getByLabelText('Intercept Type:*'), { target: { value: 'CD' } });
    fireEvent.change(screen.getByLabelText('Case Options Template:*'), { target: { value: '4' } });
    fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'Any' } });

    const devicesContainer = screen.getByTestId('WarrantDevices');

    await waitFor(() => expect(getByPlaceholderText(devicesContainer, 'Search...')).toBeEnabled());

    fireEvent.change(getByPlaceholderText(devicesContainer, 'Search...'), { target: { value: '5G data' } });
    fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[0];
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');
    expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

    await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
    const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

    fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

    fireEvent.click(screen.getAllByText('Next')[0]);

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    expect(screen.getByLabelText('Case Name:*')).toBeEnabled();
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('');
    fireEvent.change(screen.getByLabelText('Case Name:*'), { target: { value: 'Case 1' } });
    await waitFor(() => expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1'));

    fireEvent.click(screen.getAllByText('Back')[0]);

    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.click(screen.getAllByText('Next')[0]);

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');
  });

  it('should not display any Case error when clicking in Back', async () => {
    const date = moment(new Date()).add('1', 'M').format('MM/DD/YY hh:mm:ss A');

    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Warrant Name:*'), { target: { value: 'Warrant name' } });
    fireEvent.change(screen.getByLabelText('Stop Time:*'), { target: { value: date } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/Los_Angeles' } });
    fireEvent.change(screen.getByLabelText('Intercept Type:*'), { target: { value: 'CD' } });
    fireEvent.change(screen.getByLabelText('Case Options Template:*'), { target: { value: '4' } });
    fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'Any' } });

    const devicesContainer = screen.getByTestId('WarrantDevices');

    await waitFor(() => expect(getByPlaceholderText(devicesContainer, 'Search...')).toBeEnabled());

    fireEvent.change(getByPlaceholderText(devicesContainer, 'Search...'), { target: { value: '5G data' } });
    fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[0];
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');
    expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

    await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
    const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

    fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

    fireEvent.click(screen.getAllByText('Next')[0]);

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    fireEvent.click(screen.getAllByText('Back')[0]);

    expect(screen.queryByText('Selected Case :: Required field values missing or invalid')).toBeNull();
  });

  it('should display Case error when clicking in Next', async () => {
    const date = moment(new Date()).add('1', 'M').format('MM/DD/YY hh:mm:ss A');

    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantWizard />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Create Warrant Wizard')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Warrant Name:*'), { target: { value: 'Warrant name' } });
    fireEvent.change(screen.getByLabelText('Stop Time:*'), { target: { value: date } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/Los_Angeles' } });
    fireEvent.change(screen.getByLabelText('Intercept Type:*'), { target: { value: 'CD' } });
    fireEvent.change(screen.getByLabelText('Case Options Template:*'), { target: { value: '4' } });
    fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'Any' } });

    const devicesContainer = screen.getByTestId('WarrantDevices');

    await waitFor(() => expect(getByPlaceholderText(devicesContainer, 'Search...')).toBeEnabled());

    fireEvent.change(getByPlaceholderText(devicesContainer, 'Search...'), { target: { value: '5G data' } });
    fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[0];
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');
    expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

    await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
    const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

    fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

    fireEvent.click(screen.getAllByText('Next')[0]);

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    fireEvent.click(screen.getAllByText('Next')[0]);

    expect(screen.getByText('Case Information')).toBeVisible();

    expect(screen.queryByText('Selected Case :: Required field values missing or invalid')).toBeVisible();
  });
});
