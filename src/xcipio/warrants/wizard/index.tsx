// eslint-disable-next-line max-classes-per-file
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'data/root.reducers';
import { withDiscoveryWizardStepPanel } from '../../../shared/wizard/with-discovery-wizard-step-panel';
import DiscoveryWizard, { DiscoveryWizardProps, PROGRESS_STATUS } from '../../../shared/wizard/discovery-wizard';
import CasePanel from '../../cases/forms/case-panel';
import { clearCasesState } from '../../cases/case-actions';
import {
  setWarrantTracker, clearWarrantState, clearFormTemplateUsed, createWarrantAndRefresh,
  clearWarrantTracker,
} from '../warrant-actions';
import { CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { AppToaster, showErrors } from '../../../shared/toaster';
import ChangeTracker from '../forms/warrant-change-tracker';
import ModalDialog from '../../../shared/modal/dialog';
import WarrantAndDevicePanel from '../WarrantAndDevicePanel';
import SummaryAndFinal from '../SummaryAndFinalStep';

// Make sure the following lines (HOCs) are not inside the render method
// to avoid unmount and mount each time render is called.
// See https://reactjs.org/docs/higher-order-components.html for detail
// explanation.
const WarrantAndDevicePanelStep = withDiscoveryWizardStepPanel(WarrantAndDevicePanel);
const CaseDetailsStep = withDiscoveryWizardStepPanel(CasePanel);
const SummaryAndFinalStep = withDiscoveryWizardStepPanel(SummaryAndFinal);

const popUp = {
  message: '',
  actionText: '',
  isOpen: false,
  handleClose: () => null,
  handleSubmit: () => null,
};
// Issue: wizard does not reset back to first step upon reopen

type RouterParams = {
  history: string
}

const mapState = ({
  warrants: { warrant, userAudit },
  global: { selectedDF },
  cases: { cases },
  warrants: {
    changeTracker,
  },
  discoveryXcipio: {
    discoveryXcipioSettings: {
      isAuditEnabled,
    },
  },
  authentication,
}: RootState) => ({
  warrant,
  cases,
  changeTracker,
  selectedDF,
  userAudit,
  isAuditEnabled,
  authentication,
});

const mapDispatch = {
  clearCasesState,
  clearWarrantState,
  clearWarrantTracker,
  clearFormTemplateUsed,
  setWarrantTracker,
  createWarrantAndRefresh,
};

const connector = connect(mapState, mapDispatch, null, { forwardRef: true });

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type WarrantWizardProps = PropsFromRedux & PropsFromRouter;

export type WarrantWizardState = {
  currentStep: number
  subStep: number
  isSubmitSuccessful: boolean
  progressStatus: PROGRESS_STATUS
  abortOperation: boolean
  popUp: {
    message: string
    actionText: string
    isOpen: boolean
    handleClose: () => void
    handleSubmit: () => void
  },
}

class WarrantWizard extends React.Component<WarrantWizardProps, WarrantWizardState> {
    private warrantId:string|undefined|null;

    private stepPanel:Array<any> = Array(3).fill(null);

    initialState: WarrantWizardState = {
      currentStep: 1, // based 1 index
      subStep: 1, // based 1 index
      isSubmitSuccessful: false,
      progressStatus: PROGRESS_STATUS.InProgress,
      abortOperation: false,
      popUp,
    };

    constructor(props: WarrantWizardProps) {
      super(props);
      this.state = { ...this.initialState };
      this.clearData();

      this.initializeChangeTracker();
    }

    initializeChangeTracker() {
      const propsToSend = {
        idDF: this.props.selectedDF,
        idWarrant: null, // null because a new warrant does not have an id
      };
      const changeTracker = new ChangeTracker(propsToSend);
      this.props.setWarrantTracker(changeTracker);
      this.warrantId = changeTracker.addAdded('warrant');
      changeTracker.setIdName(this.warrantId, '');
    }

    onNext = async () => {
      AppToaster.clear();
      if (this.state.currentStep < this.stepPanel.length) {
        const errors: string[] = [];
        // this.stepPanel is an array with based zero index.
        // this.state.currentStep is based 1 index
        // So current step is this.state.currentStep - 1
        const valid: boolean = (this.stepPanel[this.state.currentStep - 1].isValid) ?
          this.stepPanel[this.state.currentStep - 1].isValid(errors) : true;
        if (valid) {
          if (this.stepPanel[this.state.currentStep - 1].submit) {
            this.stepPanel[this.state.currentStep - 1].submit()
              .then((value:boolean) => {
                if (value === true) {
                // this.state.currentStep is based 1 index,
                // so next step is this.state.currentStep + 1
                  this.setState({ currentStep: this.state.currentStep + 1, subStep: 1 });
                }
              });
          }
        } else {
          showErrors(errors);
        }
      }
    }

    onSubStep = (subStep: number) => {
      AppToaster.clear();
      const errors: string[] = [];
      const valid: boolean = this.stepPanel[this.state.currentStep - 1].isValid ?
        this.stepPanel[this.state.currentStep - 1].isValid(errors) : true;
      if (valid) {
        if (this.stepPanel[this.state.currentStep - 1].submitSubStep) {
          this.stepPanel[this.state.currentStep - 1].submitSubStep();
        }
        this.setState({ subStep });
      } else {
        showErrors(errors);
      }
    }

    onAbort = () => {
      this.setState({ abortOperation: true });
    }

    onCancel = () => {
      if (this.props.changeTracker?.isChanged()) {
        const message = 'Any changes will be lost. Click \'Ok\' to continue.';
        const nextpopUp = {
          message,
          actionText: 'Ok',
          isOpen: true,
          handleClose: () => {
            const newPopUp = { ...this.state.popUp };
            newPopUp.isOpen = false;
            this.setState({
              popUp: newPopUp,

            });
          },
          handleSubmit: () => {
            const newPopUp = { ...this.state.popUp };
            newPopUp.isOpen = false;
            this.setState({
              popUp: newPopUp,
            });
            AppToaster.clear();
            this.clearData();
            this.props.history.replace('/warrants');
          },
        };
        this.setState({ popUp: nextpopUp });
      } else {
        AppToaster.clear();
        this.clearData();
        this.props.history.replace('/warrants');
      }
    }

    onBack = async () => {
      AppToaster.clear();

      const {
        currentStep,
      } = this.state;

      if (currentStep < this.stepPanel.length) {
        // this.stepPanel is an array with based zero index.
        // currentStep is based 1 index
        // So current step is currentStep - 1
        if (this.stepPanel[currentStep - 1].submit) {
          await this.stepPanel[currentStep - 1].submit(false);
        }
      }

      // currentStep is based 1 index, so back step is currentStep - 1
      this.setState({
        currentStep: currentStep - 1,
        subStep: 1,
      });
    }

    onSubmit = () => {
      AppToaster.clear();
      if (this.state.currentStep === this.stepPanel.length) {
        const errors: string[] = [];
        // this.stepPanel is an array with based zero index.
        // this.state.currentStep is based 1 index
        // So current step is this.state.currentStep - 1
        const valid: boolean = (this.stepPanel[this.state.currentStep - 1].isValid) ?
          this.stepPanel[this.state.currentStep - 1].isValid(errors) : true;
        if (valid) {
          if (this.stepPanel[this.state.currentStep - 1].submit) {
            this.stepPanel[this.state.currentStep - 1].submit()
              .then((value:boolean) => {
                if (value === true) {
                  // this.state.currentStep is based 1 index,
                  // so next step is this.state.currentStep + 1
                  this.setState({
                    currentStep: this.state.currentStep,
                    subStep: 1,
                    progressStatus: PROGRESS_STATUS.InProgress,
                  });
                }
              });
          }
        }
      }
    }

    onSubmitComplete = (completeStatus: PROGRESS_STATUS, warrantSuccessfulCreated?:boolean) => {
      if (warrantSuccessfulCreated) {
        this.setState({
          progressStatus: completeStatus,
        });
        this.clearData();
      } else {
        this.setState({
          progressStatus: completeStatus,
        });
      }
    }

    jumpToStep = (evt:any) => {
      AppToaster.clear();
      if (this.stepPanel) {
        const totalStep:number = this.stepPanel.length;
        if (evt.currentTarget.value + 1 === totalStep && this.state.currentStep === totalStep) {
          this.setState({ currentStep: totalStep });
        } else {
          this.setState({ currentStep: evt.currentTarget.value + 1 });
        }
      }
    }

    isSummaryStep = (step:number) => step !== 0 && step === this.stepPanel.length;

    clearData = () => {
      this.props.clearWarrantState();
      this.props.clearCasesState();
      this.props.clearFormTemplateUsed();
      this.props.clearWarrantTracker();
    }

    getTitle = () => {
      return 'Create Warrant';
    }

    render() {
      const discoveryWizardProps: DiscoveryWizardProps = {
        onNext: this.onNext,
        onBack: this.onBack,
        onCancel: this.onCancel,
        onSubmit: this.onSubmit,
        hasNextStep: (step:number) => step <= (this.stepPanel.length - 1),
        isSummaryStep: this.isSummaryStep,
        isProgressStep: (step:number) => (step !== 0 && step > this.stepPanel.length),
        submitButtonLabel: 'Create',
        progressStatus: this.state.progressStatus,
        getTitle: this.getTitle,
        currentStep: this.state.currentStep,
        isOpen: true,
      };

      return (
        <DiscoveryWizard {...discoveryWizardProps}>
          <WarrantAndDevicePanelStep
            ref={(ref) => { this.stepPanel[0] = ref; }}
            show={this.state.currentStep === 1}
            mode={CONFIG_FORM_MODE.Create}
            stepTextLabel="Warrant & Devices"
            warrantId={this.warrantId}
          />
          <CaseDetailsStep
            ref={(ref:any) => { this.stepPanel[1] = ref; }}
            show={this.state.currentStep === 2}
            stepTextLabel="Cases"
            index={this.state.subStep - 1}
            mode={CONFIG_FORM_MODE.Create}
            dataArray={this.props.cases}
            onSubStep={this.onSubStep}
            subStep={this.state.subStep}
          />
          <SummaryAndFinalStep
            ref={(ref:any) => { this.stepPanel[2] = ref; }}
            show={this.state.currentStep === 3}
            stepTextLabel="Summary"
            editMode={false}
            mode={CONFIG_FORM_MODE.Create}
            userAudit={this.props.userAudit}
            userId={this.props.authentication.userDBId}
            userName={this.props.authentication.userId}
            auditEnabled={this.props.isAuditEnabled}
            isOpen
            abortOperation={this.state.abortOperation}
            history={this.props.history}
            isDialog={false}
            onSubmitComplete={this.onSubmitComplete}
          />
          <ModalDialog
            displayMessage={this.state.popUp.message}
            isOpen={this.state.popUp.isOpen}
            onSubmit={this.state.popUp.handleSubmit}
            onClose={this.state.popUp.handleClose}
            actionText={this.state.popUp.actionText}
          />
        </DiscoveryWizard>
      );
    }
}

export default withRouter(connector(WarrantWizard));
