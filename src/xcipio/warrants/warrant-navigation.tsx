/* eslint-disable max-classes-per-file */
import React from 'react';
import {
  Navbar, Menu, MenuItem, MenuDivider, HTMLSelect, NavbarDivider, NavbarGroup,
  NavbarHeading, Button, Alignment, Position, Popover,
} from '@blueprintjs/core';

import { withRouter } from 'react-router-dom';
import { navigateToConfig } from '../../root-subnav';

class WarrantConfigMenu extends React.Component<any, any> {
  render() {
    return (
      <Menu>
        <MenuItem
          onClick={() => navigateToConfig(this.props.location.pathname, this, '')}
          icon="settings"
          text="Preferences..."
        />
        <MenuItem
          onClick={() => navigateToConfig(this.props.location.pathname, this, 'targets')}
          icon="circle-arrow-right"
          text="Set Enabled Targets..."
        />
        <MenuItem
          onClick={() => navigateToConfig(this.props.location.pathname, this, 'af-templates')}
          icon="document"
          text="AF Templates..."
        />
        <MenuDivider />
        <MenuItem
          onClick={() => navigateToConfig(this.props.location.pathname, this, 'jarea-templates')}
          icon="map"
          text="Jarea Templates..."
        />
        <MenuItem
          onClick={() => navigateToConfig(this.props.location.pathname, this, 'jarea')}
          icon="globe"
          text="Jarea..."
        />

      </Menu>
    );
  }
}

@(withRouter as any)
class WarrantNavigation extends React.Component<any, any> {
  render() {
    const { template } = this.props.warrants;
    return (
      <div data-scroll-to-id="templates">
        <NavbarGroup align={Alignment.LEFT} />
        <NavbarGroup align={Alignment.RIGHT} />
      </div>
    );
  }
}

export default WarrantNavigation;
