import {
  broadcastSelectedWarrants,
} from './warrant-actions';

import WarrantReducer, { defaultState } from './warrants-reducer';

describe('Warrant Reducer', () => {
  const selectedWarrants = [{
    id: '0',
    name: 'Name A',
  }, {
    id: '2',
    name: 'Name B',
  }];
  it('should return the inital state', () => {
    expect(WarrantReducer(undefined, {})).toEqual(defaultState);
  });
  it('should handle broadcastSelectedWarrants action', () => {
    expect(WarrantReducer(defaultState, broadcastSelectedWarrants(selectedWarrants)))
      .toEqual({ ...defaultState, selectedWarrants });
  });
});
