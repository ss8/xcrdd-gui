import React, { Component, Fragment } from 'react';
import { FormPanel } from 'shared/wizard/with-discovery-wizard-step-panel';
import Summary, { Summmary as SummaryClass } from '../wizard/summary';
import ConnectedEditWarrantSubmit, { EditWarrantSubmit as ConnectedEditWarrantSubmitClass } from '../forms/warrantSubmit/warrant-edit-submit';

export type SummaryAndFinalProps = {
  mode: string,
  editMode: boolean
  userAudit: any
  userId: any
  userName: any
  auditEnabled: any
  isOpen: any
  abortOperation: any
  history: any
  isDialog: any
  onSubmitComplete: any
};

export default class SummaryAndFinal extends Component<SummaryAndFinalProps> implements FormPanel {
  summaryRef: React.RefObject<SummaryClass>
  finalStepRef: React.RefObject<ConnectedEditWarrantSubmitClass>

  constructor(props: SummaryAndFinalProps) {
    super(props);
    this.summaryRef = React.createRef<SummaryClass>();
    this.finalStepRef = React.createRef<ConnectedEditWarrantSubmitClass>();
  }

  isValid(errors: string[]): boolean {
    const isSummaryValid = this.summaryRef.current?.isValid([]) ?? false;
    return isSummaryValid;
  }

  async submit(): Promise<boolean> {
    await this.summaryRef.current?.submit();
    await this.finalStepRef.current?.createCompleteWarrant();

    return true;
  }

  render(): JSX.Element {
    return (
      <Fragment>
        <Summary ref={this.summaryRef} editMode={this.props.editMode} />
        <ConnectedEditWarrantSubmit
          ref={this.finalStepRef}
          userAudit={this.props.userAudit}
          userId={this.props.userId}
          userName={this.props.userName}
          auditEnabled={this.props.auditEnabled}
          abortOperation={this.props.abortOperation}
          history={this.props.history}
          onSubmitComplete={this.props.onSubmitComplete}
          isOpen
          isDialog={false}
        />
      </Fragment>
    );
  }
}
