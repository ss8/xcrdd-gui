import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { getAllAFsByFilter } from 'data/accessFunction/accessFunction.actions';
import { fetchCollectionFunctionListByFilter, fetchCollectionFunctionGroupList } from 'data/collectionFunction/collectionFunction.actions';
import {
  getAllWarrants,
  getWarrantById,
  createWarrantAndRefresh,
  updateWarrant,
  patchWarrant,
  getTargetById,
  clearErrors,
  updateWarrantAndRefresh,
  setTemplate,
  getWarrantGridSettings,
  putWarrantGridSettings,
  deleteWarrantAndRefresh,
  getAllTemplates,
  getWarrantFormConfig,
  getWarrantFormCustomizationConfig,
  setWarrantTracker,
  clearWarrantTracker,
  setWarrantConfigInstance,
} from './warrant-actions';
import { getUserTeams, getTimezones } from '../../users/users-actions';
import { getAllContacts } from '../contacts/contacts-actions';
import { getAllCasesByCaseIds } from '../cases/case-actions';
import { getAllTargetsByTargetIds } from '../targets/targets-actions';
import { getDXSettings } from '../discovery-xcipio-actions';
import { getAllAttachments } from '../attachments/attachments-actions';
import ModuleRoutes from './warrant-routes';
import './warrants.scss';

const Warrants = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  warrants: state.warrants,
  cases: state.cases,
  contacts: state.contacts,
  users: state.users,
  authentication: state.authentication,
  global: state.global,
  discoveryXcipio: state.discoveryXcipio,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllWarrants,
  getWarrantById,
  createWarrantAndRefresh,
  updateWarrant,
  patchWarrant,
  getAllTargetsByTargetIds,
  getTargetById,
  clearErrors,
  updateWarrantAndRefresh,
  setTemplate,
  getWarrantGridSettings,
  putWarrantGridSettings,
  getUserTeams,
  getAllContacts,
  getAllCasesByCaseIds,
  fetchCollectionFunctionListByFilter,
  getAllAFsByFilter,
  deleteWarrantAndRefresh,
  getAllTemplates,
  getWarrantFormConfig,
  setWarrantTracker,
  clearWarrantTracker,
  getTimezones,
  setWarrantConfigInstance,
  fetchCollectionFunctionGroupList,
  getWarrantFormCustomizationConfig,
  getDXSettings,
  getAllAttachments,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Warrants));
