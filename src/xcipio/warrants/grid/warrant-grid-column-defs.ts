import { ColDef } from '@ag-grid-enterprise/all-modules';
import { dateFilterParams } from '../../../shared/constants/index';

export const warrantGridStateColumnFilters = ['ACTIVE', 'EXPIRED', 'PAUSED', 'PENDING', 'STOPPED'];

export const warrantGridStateColumnTrashedFilters = ['TRASHED'];

const WarrantGridColumnDefs:ColDef[] = [
  {
    headerName: 'State',
    field: 'state',
    filter: 'agSetColumnFilter',
    sortable: false,
    tooltipField: 'state',
    filterParams: {
      values: warrantGridStateColumnFilters,
    },
  },
  {
    headerName: 'Warrant Name', field: 'name', filter: 'agTextColumnFilter', tooltipField: 'name',
  },
  {
    headerName: 'Start Time',
    field: 'startDateTime',
    filter: 'agDateColumnFilter',

    tooltipField: 'startDateTime',
    filterParams: dateFilterParams,
    cellRenderer: 'DateTimeWithTimezoneCellRenderer',
  },
  {
    headerName: 'Stop Time',
    field: 'stopDateTime',

    tooltipField: 'stopDateTime',
    filter: 'agDateColumnFilter',
    filterParams: dateFilterParams,
    cellRenderer: 'DateTimeWithTimezoneCellRenderer',
  },
  {
    headerName: 'Contact', field: 'contact.name', filter: 'agTextColumnFilter', tooltipField: 'contact.name',
  },
  {
    headerName: 'Comments', field: 'comments', filter: 'agTextColumnFilter', tooltipField: 'comments',
  },
  {
    headerName: 'Judge', field: 'judge', filter: 'agTextColumnFilter', tooltipField: 'judge',
  },
  {
    headerName: 'Received Time',
    field: 'receivedDateTime',

    filter: 'agDateColumnFilter',

    tooltipField: 'receivedDateTime',
    filterParams: dateFilterParams,
    cellRenderer: 'DateTimeWithTimezoneCellRenderer',
  },
  {
    headerName: 'City', field: 'city', filter: 'agTextColumnFilter', tooltipField: 'city',
  },
  {
    headerName: 'Geographical State', field: 'province', filter: 'agTextColumnFilter', tooltipField: 'province',
  },
  {
    headerName: 'Region', field: 'region', filter: 'agTextColumnFilter', tooltipField: 'region',
  },
  {
    headerName: 'Time Zone',
    field: 'timeZone',
    sortable: false,
    tooltipField: 'timeZone',
    cellRenderer: 'TimeZoneCellRenderer',
  },
  {
    headerName: 'Number of attachments', field: 'num_of_attachments', filter: 'agNumberColumnFilter', tooltipField: 'num_of_attachments',
  },
];

export default WarrantGridColumnDefs;
