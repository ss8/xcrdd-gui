import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { WarrantState } from 'data/warrant/warrant.types';
import WarrantsGrid from './index';
import { findByTestAttr, checkProps } from '../../../shared/__test__/testUtils';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';
import { WARRANTS_TO_TEST_STATE_CHANGE } from './test.data';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

Enzyme.configure({
  adapter: new EnzymeAdapter(),
});

const mockStore = configureStore([]);
const defaultProps: Partial<RootState> = {
  authentication: {
    isAuthenticated: true,
    userId: '',
    privileges: [],
    errorStatus: '',
    beforeLogout: false,
    passwordUpdate: false,
    loginMessage: '',
    logiTokenUpdated: false,
    userDBId: '',
    sessionTimeout: '',
    isAuthenticationExpired: false,
  },
  warrants: {
    warrant: null,
    warrants: [],
    selectedWarrants: [],
    error: null,
    cfs: [],
    action: '',
    config: undefined,
    customizationconfig: { derivedTargets: {} },
    templates: [],
    template: null,
    changeTracker: null,
    warrantConfigInstance: null,
    templateUsed: null,
    userAudit: undefined,
    selectionLastCleared: null,
    selectedCase: null,
    displayTrashWarrants: false,
  },
  global: {
    selectedDF: null,
    dfs: [],
    error: null,
  },
};

const initialState = {
  discoveryXcipio: {
    discoveryXcipioSettings: { Warrant_bArchiveWarrant: true },
  },
  users: {
    timezones: [],
  },
  warrants: { selectionLastCleared: null },
};

/**
* Factory function to create a ShallowWrapper for the WarrantsGrid component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (props = {}) => {
  return shallow(<WarrantsGrid {...props} />);
};

test('does not throw warning with expected props - warrants grid', () => {
  checkProps(WarrantsGrid, defaultProps);
});

test('warrant grid component do not show a Trash Warrant switch when App Setting False', () => {
  const initialFalseState = { ...initialState };
  initialFalseState.discoveryXcipio = {
    discoveryXcipioSettings: { Warrant_bArchiveWarrant: false },
  };
  const store = mockStore(initialFalseState);
  const setupProps = { ...defaultProps, store };
  const wrapperNew = setup({ ...setupProps });
  const warrantGrid = wrapperNew.dive().dive().dive();
  const grid = findByTestAttr(warrantGrid, 'warrants-grid-main');
  expect(grid.length).toBe(1);
  const trashWarrantsSwitch = findByTestAttr(warrantGrid, 'trash-warrants-switch');
  expect(trashWarrantsSwitch.length).toBe(0);
});

describe('if TrashWarrants App setting is enabled/true', () => {
  let wrapper: any;
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
    const props = cloneDeep(defaultProps);
    (props.warrants as WarrantState).warrants = WARRANTS_TO_TEST_STATE_CHANGE.slice();
    const setupProps = { ...props, store };
    wrapper = setup({ ...setupProps });

    axiosMock.onAny().reply(200, { data: [] });
  });

  test('warrant grid component shows a Trash Warrant switch', () => {
    const warrantGrid = wrapper.dive().dive().dive();
    const grid = findByTestAttr(warrantGrid, 'warrants-grid-main');
    expect(grid.length).toBe(1);
    const trashWarrantsSwitch = findByTestAttr(warrantGrid, 'trash-warrants-switch');
    expect(trashWarrantsSwitch.length).toBe(1);
  });

  test('warrant grid component updates state on trash Warrant switch click', () => {
    const warrantGrid = wrapper.dive().dive().dive();
    warrantGrid.setState({
      displayTrashedWarrants: false,
    });
    const trashWarrantsSwitch = findByTestAttr(warrantGrid, 'trash-warrants-switch');
    trashWarrantsSwitch.simulate('change');
    const warrantsCommonGrid = findByTestAttr(warrantGrid, 'warrants-common-grid');
    expect(warrantsCommonGrid.length).toBe(1);
    expect(warrantGrid.state('displayTrashedWarrants')).toBe(true);
  });

  test('from commonGrid table, select a warrant and restore, restore warrant pop-up is display', () => {
    const warrantCommonGrid = wrapper.dive().dive().dive();
    warrantCommonGrid.setState({
      selection: [WARRANTS_TO_TEST_STATE_CHANGE[0]],
    });
    warrantCommonGrid.instance().onActionMenuItemClick({ type: COMMON_GRID_ACTION.Restore });
    expect(warrantCommonGrid.state('stateUpdateDialog').isOpen).toBe(true);
  });

  // TODO: Reza: Update the following code accordingly
  // test('on restore warrant popup show modal with cancel and save', () => {
  //   const warrantGrid = wrapper.dive().dive().dive();
  //   warrantGrid.setState({
  //     selection: [WARRANTS_TO_TEST_STATE_CHANGE[0]],
  //   });
  //   warrantGrid.instance().handleUpdateWarrantStatus('Restore', 'test_restored');
  //   const restoreWarrant = findByTestAttr(warrantGrid, 'update-warrant-status');
  //   expect(restoreWarrant.length).toBe(1);
  //   expect(warrantGrid.state('stateUpdateDialog').isOpen).toBe(true);
  //   const modalDialog = restoreWarrant.dive().find(ModalDialog).dive();
  //   const saveButton = findByTestAttr(modalDialog, 'modalDialog-button-save');
  //   expect(saveButton.length).toBe(1);
  //   const cancelButton = findByTestAttr(modalDialog, 'modalDialog-button-cancel');
  //   expect(cancelButton.length).toBe(1);
  // });

  // test('restore warrant simulate Save Button click, popup closes', () => {
  //   const warrantGrid = wrapper.dive().dive().dive();
  //   warrantGrid.setState({
  //     stateUpdateDialog: { action: 'Restore', isOpen: true },
  //     selection: [WARRANTS_TO_TEST_STATE_CHANGE[0]],
  //   });
  //   const restoreWarrant = findByTestAttr(warrantGrid, 'update-warrant-status');
  //   const modalDialog = restoreWarrant.dive().find(ModalDialog).dive();
  //   const saveButton = findByTestAttr(modalDialog, 'modalDialog-button-save');
  //   saveButton.simulate('click');
  //   expect(warrantGrid.state('stateUpdateDialog').isOpen).toBe(false);
  // });

  // TODO: implement this method after the XREST returns correct data. For now added a place holder.
  test('CommonGrid component: given props, CommonGrid render proper data on Display trashed warrant click', () => {
  });
});

describe('when user select a warrant and re-schedule from from commonGrid table', () => {
  let wrapper: any;
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
    const setupProps = { ...defaultProps, store };
    wrapper = setup({ ...setupProps });

    axiosMock.onAny().reply(200, { data: [] });
  });

  test(' re-schedule warrant pop-up is displayed', () => {
    const warrantCommonGrid = wrapper.dive().dive().dive();
    warrantCommonGrid.instance().onActionMenuItemClick({ type: COMMON_GRID_ACTION.Reschedule });
    expect(warrantCommonGrid.state('rescheduleDialogIsOpen')).toBe(true);
  });

  test('on restore warrant popup show modal with cancel and save', () => {
    const warrantGrid = wrapper.dive().dive().dive();
    warrantGrid.instance().toggleRescheduleDialog();
    const rescheduleWarrant = findByTestAttr(warrantGrid, 'warrants-reschedule');
    expect(rescheduleWarrant.length).toBe(1);
    expect(warrantGrid.state('rescheduleDialogIsOpen')).toBe(true);
  });
});
