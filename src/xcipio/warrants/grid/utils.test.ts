import { Warrant } from 'data/warrant/warrant.types';
import cloneDeep from 'lodash.clonedeep';
import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { mockWarrants } from '__test__/mockWarrants';
import { WARRANT_RESSTATE } from '../warrant-enums';
import showActionMenuItem from './utils';

describe('utils', () => {
  describe('showActionMenuItem()', () => {
    let warrant: Warrant;

    beforeEach(() => {
      warrant = cloneDeep(mockWarrants[0]);
    });

    it('should return true for Pause if warrant is active', () => {
      warrant.state = WARRANT_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Pause, warrant, true, true)).toBe(true);
    });

    it('should return false for Pause if warrant is active but pause action is not enabled', () => {
      warrant.state = WARRANT_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Pause, warrant, false, false)).toBe(false);
    });

    it('should return true for Resume if warrant is paused', () => {
      warrant.state = WARRANT_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Resume, warrant, true, true)).toBe(true);
    });

    it('should return false for Resume if warrant is paused but resume action is not enabled', () => {
      warrant.state = WARRANT_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Resume, warrant, false, false)).toBe(false);
    });

    it('should return true for Start if warrant is stopped or expired', () => {
      warrant.state = WARRANT_RESSTATE.Stopped;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Start, warrant, true, true)).toBe(true);
      warrant.state = WARRANT_RESSTATE.Expired;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Start, warrant, true, true)).toBe(true);
    });

    it('should return true for Stop if warrant is active or paused', () => {
      warrant.state = WARRANT_RESSTATE.Active;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Stop, warrant, true, true)).toBe(true);
      warrant.state = WARRANT_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Stop, warrant, true, true)).toBe(true);
    });

    it('should return true for Reschedule if warrant is pending, stopped or expired', () => {
      warrant.state = WARRANT_RESSTATE.Pending;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, warrant, true, true)).toBe(true);
      warrant.state = WARRANT_RESSTATE.Stopped;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, warrant, true, true)).toBe(true);
      warrant.state = WARRANT_RESSTATE.Expired;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Reschedule, warrant, true, true)).toBe(true);
    });

    it('should return true for Edit if warrant is other than Trash', () => {
      warrant.state = WARRANT_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Edit, warrant, true, true)).toBe(true);

      warrant.state = WARRANT_RESSTATE.Trash;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Edit, warrant, true, true)).toBe(false);
    });

    it('should return true for Trash if warrant is other than Trash', () => {
      warrant.state = WARRANT_RESSTATE.Paused;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Trash, warrant, true, true)).toBe(true);

      warrant.state = WARRANT_RESSTATE.Trash;
      expect(showActionMenuItem(COMMON_GRID_ACTION.Trash, warrant, true, true)).toBe(false);
    });
  });
});
