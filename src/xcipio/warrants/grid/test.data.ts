import cloneDeep from 'lodash.clonedeep';

const WARRANT = {
  id: 'MTkxNzE5ODYzOA',
  dxOwner: '',
  dxAccess: '',
  name: '1010',
  caseType: 'ALL',
  cfGroup: {
    id: 'MA',
    name: '',
  },
  cfPool: [],
  city: '',
  comments: '',
  contact: {
    id: '',
    name: '',
  },
  judge: '',
  department: '',
  receivedDateTime: '',
  region: '',
  startDateTime: '2020-05-20T07:00:00Z',
  province: '',
  stopDateTime: '2020-05-31T07:00:00Z',
  timeZone: 'America/Los_Angeles',
  templateUsed: '1',
  caseOptTemplateUsed: '',
  caseOptions: {
    kpi: false,
    traceLevel: 0,
    configureOptions: false,
    packetEnvelopeMessage: false,
    packetEnvelopeContent: false,
    realTimeText: false,
    callPartyNumberDisplay: false,
    directDigitExtractionData: false,
    monitoringReplacementPartiesAllowed: false,
    circuitSwitchedVoiceCombined: false,
    includeInBandOutBoundSignalling: false,
    encryptionType: '',
    encryptionKey: '',
    provideTargetIdentityInCcChannel: '',
    cccBillingType: '',
    cccBillingNumber: '',
    deactivationMode: '',
    location: false,
    target: false,
    sms: false,
    callIndependentSupplementaryServicesContent: false,
    packetSummaryCount: 0,
    packetSummaryTimer: 0,
    generatePacketDataHeaderReport: false,
    packetHeaderInformationReportFilter: '',
    callTraceLogging: 0,
    featureStatusInterval: 0,
    surveillanceStatusInterval: 0,
  },
  state: 'ACTIVE',
  cases: [
    {
      id: 'eENPXzE5MTcxOTg2Mzg',
      name: 'xCO_1917198638',
    },
  ],
  attachments: [],
};
const modifyObject = (object:any, attribute:any, value:any) => {
  const newObject = cloneDeep(object);
  newObject[attribute] = value;
  return newObject;
};
export const WARRANT_ACTIVE = modifyObject(WARRANT, 'state', 'ACTIVE');
export const WARRANT_EXPIRED = modifyObject(WARRANT, 'state', 'EXPIRED');
export const WARRANT_PAUSED = modifyObject(WARRANT, 'state', 'PAUSED');
export const WARRANTS_TO_TEST_STATE_CHANGE = [WARRANT_ACTIVE, WARRANT_EXPIRED, WARRANT_PAUSED];
