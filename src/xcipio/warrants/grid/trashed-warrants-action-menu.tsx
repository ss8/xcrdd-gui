import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

export default class TrashedWarrantsActionMenu extends React.Component<any, any> {
  render() {
    const ctx = this.props.context;
    const menuItemDisabled = !ctx.state.selection.length;

    const showDeleteActionMenu = (
      ((ctx.props.showActionMenuItem !== undefined
        && ctx.showActionMenuItem(COMMON_GRID_ACTION.Delete, this.props.data))
        && (ctx.props.hasPrivilegeToDelete !== undefined && ctx.props.hasPrivilegeToDelete))
        ||
        (ctx.props.showActionMenuItem === undefined
          && ctx.props.hasPrivilegeToDelete !== undefined
          && ctx.props.hasPrivilegeToDelete));

    const showRestoreActionMenu = (
      ((ctx.props.showActionMenuItem !== undefined
              && ctx.showActionMenuItem(COMMON_GRID_ACTION.Restore, this.props.data))
              && (ctx.props.hasPrivilegeToDelete !== undefined && ctx.props.hasPrivilegeToDelete))
              ||
              (ctx.props.showActionMenuItem === undefined
                && ctx.props.hasPrivilegeToDelete !== undefined
                && ctx.props.hasPrivilegeToDelete));

    return (
      <Menu>
        {
          showDeleteActionMenu
            && (
            <MenuItem
              id={COMMON_GRID_ACTION.Delete}
              onClick={ctx.onActionMenuItemClick}
              disabled={menuItemDisabled}
              text="Delete"
            />
            )
        }
        {
            showRestoreActionMenu
            && (
            <MenuItem
              id={COMMON_GRID_ACTION.Restore}
              onClick={ctx.onActionMenuItemClick}
              disabled={menuItemDisabled}
              text="Restore"
            />
            )
        }

      </Menu>
    );
  }
}
