import React, { Fragment, ReactNode } from 'react';
import { ColDef } from '@ag-grid-community/core';
import { connect } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import { CaseModel } from 'data/case/case.types';
import '../warrants.scss';
import { Switch } from '@blueprintjs/core';
import { Warrant } from 'data/warrant/warrant.types';
import { RootState } from 'data/root.reducers';
import { getSortedTimeZoneLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { FilterModel } from 'shared/commonGrid/common-grid.types';
import { adaptToServerValueFilterModel } from 'shared/commonGrid/filterModel.adapter';
import WarrantGridColumnDefs, {
  warrantGridStateColumnFilters, warrantGridStateColumnTrashedFilters,
} from './warrant-grid-column-defs';
import CommonGrid, { ICommonGridProperties } from '../../../shared/commonGrid';
import { WARRANT_PRIVILEGE } from '../warrant-enums';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../../shared/commonGrid/grid-enums';
import ActionMenuCellRenderer from '../../../shared/commonGrid/action-menu-cell-renderer';
import DateTimeCellRenderer from '../../../shared/commonGrid/date-cell-renderer';
import TrashedWarrantsActionMenu from './trashed-warrants-action-menu';
import WarrantActionsHandler from '../warrant-actions-handler';
import * as warrantActions from '../warrant-actions';
import { getUserSettings } from '../../../users/users-actions';
import ActionMenuBar from '../actions-bar/action-menu';
import RescheduleDialog from '../reschedule';
import showActionMenuItem from './utils';
import DateTimeWithTimezoneCellRenderer from '../../../shared/commonGrid/DateTimeWithTimezoneCellRenderer';
import { TimeZoneCellRenderer } from '../../../shared/commonGrid/TimeZoneCellRenderer';
import WarrantClassicModeMenu from '../WarrantClassicModeMenu';

const TRASHED_FILTER_VALUE = '%20(state%20%20eq%20%27%25TRASH%25%27)';

class WarrantsGrid extends React.Component<any, any> {
  commonGridRef: any;

  state: any = {
    selection: [],
    selectionLastCleared: null,
    displayTrashedWarrants: false,
    additionalFilterSettings: null,
    rescheduleDialogIsOpen: false,
    stateUpdateDialog: {
      isOpen: false,
      action: undefined,
    },
    columnDefs: WarrantGridColumnDefs,
  }

  constructor(props: any) {
    super(props);

    const {
      displayTrashWarrants,
    } = this.props;

    let additionalFilterSettings: string | null = null;
    if (displayTrashWarrants === true) {
      additionalFilterSettings = TRASHED_FILTER_VALUE;
    }

    this.state = {
      ...this.state,
      displayTrashedWarrants: displayTrashWarrants,
      additionalFilterSettings,
    };
  }

  initializeColumnDef = () => {
    const {
      attachmentEnabled,
      timezones,
    } = this.props;

    let newColumnDef: ColDef[] = cloneDeep(WarrantGridColumnDefs);

    if (attachmentEnabled === false) {
      newColumnDef = newColumnDef.filter(
        (columnDef: any) => columnDef.field !== 'num_of_attachments',
      );
    }

    newColumnDef = newColumnDef.map((colDef) => {
      if (colDef.field === 'timeZone') {
        return {
          ...colDef,
          filter: 'agSetColumnFilter',
          filterParams: getSortedTimeZoneLabels(timezones),
        };
      }
      return colDef;
    });

    this.setState({
      columnDefs: newColumnDef,
    });

  }

  componentDidMount() {
    const {
      clearSelectedWarrants,
    } = this.props;

    clearSelectedWarrants();

    this.initializeColumnDef();
  }

  componentDidUpdate() {
    if (this.props.selectionLastCleared !== this.state.selectionLastCleared) {
      this.setState({ selectionLastCleared: this.props.selectionLastCleared }, () => {
        this.reloadGridDataFromServer();
      });
    }
  }

  saveGridSettings = (warrantGridSettings: {[key: string]: any}) => {
    if (warrantGridSettings !== null) {
      this.props.putWarrantGridSettings(this.props.authentication.userId,
        warrantGridSettings);
    }
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
    this.props.broadcastSelectedWarrants(e.api.getSelectedRows());
  }

  getDataPath = (data: any) => data.cases.map((item: any) => item.name) // data.cases;

  handleGetResultsFromServer = (body: any) => {
    return this.props.getAllWarrants(this.props.global.selectedDF, body);
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getWarrantGridSettings(this.props.authentication.userId);
  }

  handleClone = () => {
    const { selectedDF: dfId } = this.props.global;
    const payload: any = this.state.selection[0];
    delete payload.id;
    delete payload.case;
    payload.dxAccess = '1';
    payload.dxOwner = '2';
    payload.name = `${payload.name}-cloned-${Math.floor(Math.random() * 100)}`;
    this.props.createWarrantAndRefresh(dfId, payload);
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    switch (type) {
      case COMMON_GRID_ACTION.Edit:
        this.handleEditWarrantWizard();
        break;
      case COMMON_GRID_ACTION.Reschedule:
        this.toggleRescheduleDialog();
        break;
      case COMMON_GRID_ACTION.View:
        this.handleViewWarrantWizard();
        break;
      case COMMON_GRID_ACTION.Delete:
      case COMMON_GRID_ACTION.Restore:
      case COMMON_GRID_ACTION.Pause:
      case COMMON_GRID_ACTION.Resume:
      case COMMON_GRID_ACTION.Start:
      case COMMON_GRID_ACTION.Stop:
        this.handleUpdateWarrantStatus(type);
        break;
      default:
    }
  }

  handleViewWarrant = () => {
    const { id, cases } : any = this.state.selection[0];
    const { selectedDF: dfId } = this.props.global;
    const caseIds = cases.map((c: any) => {
      return c.id;
    });
    if (caseIds.length === 0) {
      Promise.all(
        [
          this.props.getWarrantById(dfId, id),
        ],
      ).then(() => {
        this.props.history.replace({
          pathname: '/warrants/view',
          action: 'view',
          warrant: this.props.warrants.warrant,
          cases: [],
          handleEditWarrant: this.handleEditWarrant,
          handleViewWarrant: this.handleViewWarrant,
        });
      });
    } else {
      Promise.all(
        [
          this.props.getWarrantById(dfId, id),
          this.props.getAllCasesByCaseIds(dfId, id, caseIds.join(',')),
        ],
      ).then(() => {
        this.getTargetsAndAddToCases()
          .then(() => {
            this.props.history.replace({
              pathname: '/warrants/view',
              action: 'view',
              warrant: this.props.warrants.warrant,
              cases: this.props.cases.cases,
              handleEditWarrant: this.handleEditWarrant,
              handleViewWarrant: this.handleViewWarrant,
            });
          });
      });
    }
  }

  handleEditWarrant = (warrant?:Warrant, casesParam?:CaseModel[]) => {
    const { id, cases } : any = this.state.selection[0];
    const { selectedDF: dfId } = this.props.global;
    const caseIds = cases.map((c: any) => {
      return c.id;
    });
    if (caseIds.length === 0) {
      Promise.all(
        [
          this.props.getWarrantById(dfId, id),
        ],
      ).then(() => {
        this.props.history.replace({
          pathname: '/warrants/edit',
          action: 'edit',
          warrant: warrant || this.props.warrants.warrant,
          cases: [],
        });
      });
    } else {
      Promise.all(
        [
          this.props.getWarrantById(dfId, id),
          this.props.getAllCasesByCaseIds(dfId, id, caseIds.join(',')),
        ],
      ).then(() => {
        this.getTargetsAndAddToCases()
          .then(() => {
            this.props.history.replace({
              pathname: '/warrants/edit',
              action: 'edit',
              warrant: warrant || this.props.warrants.warrant,
              cases: casesParam || this.props.cases.cases,
            });
          });
      });
    }
  }

  handleEditWarrantWizard = (): void => {
    const {
      history,
      goToWarrantEdit,
    } = this.props;

    goToWarrantEdit(history);
  }

  handleViewWarrantWizard = (): void => {
    const {
      history,
      goToWarrantView,
    } = this.props;

    goToWarrantView(history);
  }

  toggleRescheduleDialog = () => {
    this.setState({ rescheduleDialogIsOpen: !this.state.rescheduleDialogIsOpen });
  }

  handleUpdateWarrantStatus = (action: string) => {
    const stateUpdateDialog = {
      isOpen: true,
      action,
    };
    this.setState({
      stateUpdateDialog,
    });
  }

  handleWarrantStateUpdateClose = () => {
    this.setState({
      stateUpdateDialog: {
        isOpen: false,
      },
    });
  }

  reloadGridDataFromServer = () => {
    if (this.commonGridRef) {
      this.commonGridRef.handleGridReloadFromServer(
        this.handleGetResultsFromServer,
        this.state.additionalFilterSettings,
      );
    }
  }

  updateStateColumnFilterValues = (displayTrashedWarrants: boolean) => {
    const stateFilterComponent = this.commonGridRef?.gridApi?.getFilterInstance('state');
    if (stateFilterComponent == null) {
      return;
    }

    if (displayTrashedWarrants) {
      stateFilterComponent.setFilterValues(warrantGridStateColumnTrashedFilters);
    } else {
      stateFilterComponent.setFilterValues(warrantGridStateColumnFilters);
    }

  }

  getTargetsAndAddToCases = async () => {
    const { cases } = this.props.cases;
    for (let i = 0; i < cases.length; i += 1) {
      const warrantCase = cases[i];
      await this.getTargets(warrantCase);
    }
  }

  getTargets = async (warrantCase: any) => {
    if (warrantCase.targets.length === 0) return;
    const wid = this.props.warrants.warrant.id;
    const { selectedDF: dfId } = this.props.global;
    const targetIds = warrantCase.targets.map((t: any) => {
      return t.id;
    });
    const promise = this.props.getAllTargetsByTargetIds(dfId, wid, warrantCase.id, targetIds.join(','));
    const res = await promise;
    warrantCase.targets = res.value.data.data;
    return warrantCase;
  }

  handleCreateNew = () => {
    const { selectedDF: dfId } = this.props.global;
    this.props.history.replace({
      pathname: '/warrants/create',
      action: 'create',
      warrant: undefined,
      cases: [],
    });
  }

  handleCreateNewWizard = () => {
    this.props.history.replace({
      pathname: '/warrants/createWizard',
      action: 'createWizard',
      warrant: undefined,
      cases: [],
    });
  }

  adaptToGridFilterModel = (filterModel:FilterModel): FilterModel => {
    return adaptToServerValueFilterModel(filterModel, 'timeZone', this.props.timezones);
  }

  showActionMenuItem = (menuId: string, rowData: Warrant): boolean => {
    const {
      isWarrantResumeActionEnabled,
      isWarrantPauseActionEnabled,
    } = this.props;

    return showActionMenuItem(
      menuId,
      rowData,
      isWarrantResumeActionEnabled,
      isWarrantPauseActionEnabled,
    );
  }

  toggleTrashWarrantDisplay = () => {
    const { displayTrashedWarrants } = this.state;
    const { setDisplayTrashWarrants, clearSelectedWarrants } = this.props;

    const shouldDisplayTrashedWarrants = !displayTrashedWarrants;

    this.updateStateColumnFilterValues(shouldDisplayTrashedWarrants);

    setDisplayTrashWarrants(shouldDisplayTrashedWarrants);
    clearSelectedWarrants();

    let additionalFilterSettings: string | null = null;
    if (shouldDisplayTrashedWarrants) {
      additionalFilterSettings = TRASHED_FILTER_VALUE;
    }

    this.setState({
      displayTrashedWarrants: shouldDisplayTrashedWarrants,
      additionalFilterSettings,
    }, () => {
      this.reloadGridDataFromServer();
    });
  }

  renderExtraButtons = (): ReactNode => {
    const {
      selectedWarrants,
      authentication: {
        privileges,
      },
    } = this.props;

    const {
      displayTrashedWarrants,
    } = this.state;

    const hasPrivilegesToCreate = privileges?.includes(WARRANT_PRIVILEGE.Edit) ?? false;
    const hasPrivilegeToView = privileges?.includes(WARRANT_PRIVILEGE.View) ?? false;

    return (
      <WarrantClassicModeMenu
        hasPrivilegeToCreate={hasPrivilegesToCreate}
        hasPrivilegeToView={hasPrivilegeToView}
        selectedWarrants={selectedWarrants}
        displayTrashWarrants={displayTrashedWarrants}
        onClassicModeCreate={this.handleCreateNew}
        onClassicModeEdit={this.handleEditWarrant}
        onClassicModeView={this.handleViewWarrant}
      />
    );
  }

  render() {
    const { privileges } = this.props.authentication;
    const hasPrivilegesToCreate =
      privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.Edit);
    const hasPrivilegeToDelete =
      privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.Delete);
    const hasPrivilegetoView =
      privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.View);

    const frameworkComponents = {
      actionMenuCellRenderer: ActionMenuCellRenderer,
      datetimeCellRenderer: DateTimeCellRenderer,
      DateTimeWithTimezoneCellRenderer,
      TimeZoneCellRenderer,
    };
    const warrants = [...this.props.warrants.warrants];
    if (this.props.attachmentEnabled) {
      warrants.forEach((warrnt:any) => {
        warrnt.num_of_attachments = warrnt.attachments.length;
      });
    }
    const commonGridProps: ICommonGridProperties = {
      getDataPath: this.getDataPath,
      onSelectionChanged: this.handleSelection,
      onActionMenuItemClick: this.onActionMenuItemClick,
      rowSelection: this.props.isBulkActionsEnabled ?
        ROWSELECTION.MULTIPLE :
        ROWSELECTION.SINGLE,
      defaultColDef: {
        sortable: true,
        resizable: true,
      },
      columnDefs: this.state.columnDefs,
      enableBrowserTooltips: true,
      rowData: warrants,
      context: this,
      pagination: true,
      handleCreateNew: this.handleCreateNewWizard,
      noResults: false,
      createNewText: 'New Warrant Wizard',
      hasPrivilegeToCreateNew: hasPrivilegesToCreate,
      hasPrivilegeToDelete,
      hasPrivilegetoView,
      hasPrivilegeToUpdateWarrantState: hasPrivilegesToCreate,
      getGridResultsFromServer: this.handleGetResultsFromServer,
      adaptToFilterModel: this.adaptToGridFilterModel,
      renderDataFromServer: true,
      renderAttachmentField: this.props.attachmentEnabled,
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
      frameworkComponents,
      additionalFilterSettings: this.state.additionalFilterSettings,
      actionMenuClass: this.state.displayTrashedWarrants
        ? TrashedWarrantsActionMenu : undefined,
      showActionMenuItem: this.showActionMenuItem,
      renderExtraButtons: this.renderExtraButtons,
    };

    return (
      <Fragment>
        <RescheduleDialog
          data-test="warrants-reschedule"
          isOpen={this.state.rescheduleDialogIsOpen}
          onClose={this.toggleRescheduleDialog}
        />

        <div data-test="warrants-grid-main" className="warrants-grid-container" style={{ height: 'inherit' }}>
          {this.props.bArchiveWarrant && this.props.bArchiveWarrant === true && (
            <div style={{ float: 'right', paddingTop: '5px', marginLeft: '10px' }}>
              <Switch
                data-test="trash-warrants-switch"
                label="Display Trashed Warrants"
                onChange={this.toggleTrashWarrantDisplay}
                checked={!!this.state.displayTrashedWarrants}
              />
            </div>
          )}
          <CommonGrid
            data-test="warrants-common-grid"
            ref={(ref: any) => { this.commonGridRef = ref; }}
            {...commonGridProps}
          >
            <ActionMenuBar
              handleEdit={this.handleEditWarrantWizard}
            />
          </CommonGrid>
          <WarrantActionsHandler
            data-test="update-warrant-status"
            action={this.state.stateUpdateDialog?.action}
            onClose={this.handleWarrantStateUpdateClose}
            isOpen={this.state.stateUpdateDialog.isOpen}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bArchiveWarrant: bArchiveWarrant,
      Warrant_bAttachments_enabled: attachmentEnabled,
      isBulkActionsEnabled,
      isWarrantResumeActionEnabled = false,
      isWarrantPauseActionEnabled = false,
    },
  },
  warrants: {
    selectionLastCleared,
    selectedWarrants,
    displayTrashWarrants,
  },
  users: {
    timezones,
  },
}: RootState) => ({
  bArchiveWarrant,
  attachmentEnabled,
  isBulkActionsEnabled,
  selectionLastCleared,
  selectedWarrants,
  displayTrashWarrants,
  timezones,
  isWarrantResumeActionEnabled,
  isWarrantPauseActionEnabled,
});

const mapDispatchToProps = {
  broadcastSelectedWarrants: warrantActions.broadcastSelectedWarrants,
  clearSelectedWarrants: warrantActions.clearSelectedWarrants,
  deleteWarrant: warrantActions.deleteWarrant,
  getUserSettings,
  setDisplayTrashWarrants: warrantActions.setDisplayTrashWarrants,
  goToWarrantEdit: warrantActions.goToWarrantEdit,
  goToWarrantView: warrantActions.goToWarrantView,
};

export default connect(mapStateToProps,
  mapDispatchToProps, null, { forwardRef: true })(WarrantsGrid);
