import { Warrant } from 'data/warrant/warrant.types';
import { WARRANT_RESSTATE } from '../warrant-enums';
import { COMMON_GRID_ACTION } from '../../../shared/commonGrid/grid-enums';

const showActionMenuItem = (
  menuId: string,
  rowData: Warrant,
  isWarrantResumeActionEnabled: boolean,
  isWarrantPauseActionEnabled: boolean,
): boolean => {
  if (!rowData) return false;
  switch (menuId) {
    case COMMON_GRID_ACTION.Pause: {
      if (isWarrantPauseActionEnabled === true && rowData.state === WARRANT_RESSTATE.Active) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Resume: {
      if (isWarrantResumeActionEnabled === true && rowData.state === WARRANT_RESSTATE.Paused) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Start: {
      if (rowData.state === WARRANT_RESSTATE.Stopped ||
            rowData.state === WARRANT_RESSTATE.Expired) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Stop: {
      if (rowData.state === WARRANT_RESSTATE.Active ||
            rowData.state === WARRANT_RESSTATE.Paused) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Reschedule: {
      if (rowData.state === WARRANT_RESSTATE.Pending ||
          rowData.state === WARRANT_RESSTATE.Expired ||
          rowData.state === WARRANT_RESSTATE.Stopped) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Edit: {
      if (rowData.state !== WARRANT_RESSTATE.Trash) {
        return true;
      }
      break;
    }
    case COMMON_GRID_ACTION.Trash: {
      if (rowData.state !== WARRANT_RESSTATE.Trash) {
        return true;
      }
      break;
    }
    default: return true;
  }

  return false;
};

export default showActionMenuItem;
