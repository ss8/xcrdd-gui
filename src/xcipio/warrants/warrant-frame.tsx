import React, { Fragment } from 'react';
import { SupportedFeatures } from 'data/supportedFeatures/supportedFeatures.types';
import WarrantGrid from './grid';
import './warrants.scss';
import { AppToaster } from '../../shared/toaster';
import WarrantConfiguration from '../config/warrant-config';

type WarrantFrameProps = {
  authentication: any
  global: {
    selectedDF: string
  }
  getAllTemplates: any
  getAllContacts: any
  getUserTeams: any
  getWarrantFormCustomizationConfig: any
  getWarrantFormConfig: any
  setWarrantConfigInstance: any
  getTimezones: any
  fetchCollectionFunctionGroupList: any
  warrants: {
    config: SupportedFeatures
    error: any
  }
  clearErrors: any
  history: any
  fetchCollectionFunctionListByFilter: (deliveryFunctionId: string, filter: string) => Promise<any>
  getAllAFsByFilter: (deliveryFunctionId: string, filter: string) => Promise<any>
  discoveryXcipio: {
    discoveryXcipioSettings: {
      deployment: string,
    }
  },
}

type WarrantFrameState = {
  selectedTab: string
  selectedView: string
  viewType: string
}

class WarrantFrame extends React.Component<WarrantFrameProps, WarrantFrameState> {
  private gridRef: any;

  constructor(props: WarrantFrameProps) {
    super(props);
    this.gridRef = React.createRef();
    this.state = {
      selectedTab: 'events',
      selectedView: 'grid',
      viewType: 'split',
    };
  }

  handleChange = (e: any) => {
    this.setState({ selectedTab: e });
  }

  handleViewChange = (e: any) => {
    this.setState({ selectedView: e.currentTarget.id });
  }

  handleNavViewChange = (value: string) => {
    this.setState({ viewType: value });
  }

  handleAction = () => {
  }

  componentDidMount() {
    const { userId } = this.props.authentication;
    const { selectedDF: dfId } = this.props.global;
    this.props.getAllTemplates();
    this.props.getAllContacts(dfId);
    this.props.getUserTeams(userId);
    this.props.getWarrantFormCustomizationConfig(dfId);
    this.props.getWarrantFormConfig(dfId).then(() => {
      this.props.setWarrantConfigInstance(new WarrantConfiguration(this.props));
    });
    this.props.getTimezones();
    this.props.fetchCollectionFunctionGroupList(dfId);
  }

  componentDidUpdate() {
    const { error } = this.props.warrants;
    if (error) {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${error}`,
      });
      this.props.clearErrors();
    }
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/authentication/login');
    }
  }

  componentWillUnmount() {
    this.props.clearErrors();
  }

  render() {
    return (
      <Fragment>
        <div className="col-md-12" style={{ height: 'inherit' }}>
          <WarrantGrid ref={this.gridRef} {...this.props} />
        </div>
      </Fragment>
    );
  }
}

export default WarrantFrame;
