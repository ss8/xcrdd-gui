// eslint-disable-next-line import/prefer-default-export
export enum WARRANT_PRIVILEGE {
    View = 'warrant_view',
    Edit = 'warrant_write',
    Delete = 'warrant_delete',
}

export enum WARRANT_PROVSTATE {
    Trash = 'TRASH',
    Restore = 'UNTRASH',
    Start = 'START',
    Stop = 'STOP',
    Pause = 'PAUSE',
    Resume = 'RESUME',
    Schedule = 'SCHEDULE',
}

/*
State attribute from Warrant's Response object
*/
export enum WARRANT_RESSTATE {
    Active = 'ACTIVE',
    Trash = 'TRASH',
    Pending = 'PENDING',
    Expired = 'EXPIRED',
    Stopped = 'STOPPED',
    Paused = 'PAUSED',
}
