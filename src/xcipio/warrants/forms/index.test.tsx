import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { mockSupportedFeatures } from '../../../__test__/mockSupportedFeatures';
import { WarrantForm } from './index';
import { broadcastSelectedWarrants } from '../warrant-actions';

describe('<WarrantForm />', () => {
  let testMocks: any;
  let wrapper: ShallowWrapper<any, any, WarrantForm>;
  let props: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.warrant = {
      attachments: [],
      caseOptTemplateUsed: '7',
      caseOptions: {},
      caseType: 'ALL',
      cases: [],
      cfGroup: { id: 'MA', name: '' },
      cfPool: [],
      city: '',
      comments: '',
      contact: { id: '', name: '' },
      department: '',
      dxAccess: '',
      dxOwner: '',
      id: '1234',
      judge: '',
      name: 'warrant name',
      num_of_attachments: 0,
      province: '',
      receivedDateTime: '2020-07-29T12:20:00.000Z',
      region: '',
      startDateTime: '2020-07-29T13:20:00.000Z',
      state: 'EXPIRED',
      stopDateTime: '2020-07-30T09:20:00.000Z',
      timeZone: 'America/New_York',
    };

    testMocks.case = {
      cfId: { id: 'Q29va0NudHktMzMxMDg', name: 'CookCnty-33108' },
      device: '',
      dxAccess: '',
      dxOwner: '',
      entryDateTime: '2020-07-03T12:17:13.058Z',
      genccc: {},
      hi3Interfaces: [],
      isValid: true,
      liId: 'case1',
      name: '',
      options: {},
      services: ['LTE-IOT'],
      startDateTime: '2020-07-29T13:20:00.000Z',
      stopDateTime: '2020-07-30T09:20:00.000Z',
      targets: [{}],
      templateUsed: null,
      timeZone: 'America/New_York',
      type: 'CD',
    };

    props = {
      authentication: {
        userDBId: 'someUserDBId',
        userId: 'someUserId',
      },
      warrants: {
        config: mockSupportedFeatures,
        templates: [],
      },
      users: {
        timezones: ['America/New_York'],
        teams: [],
      },
      contacts: {
        contacts: [],
      },
      location: {
        warrant: testMocks.warrant,
        cases: [testMocks.case],
      },
      isAuditEnabled: true,
      setWarrantTracker: jest.fn(),
      broadcastSelectedWarrants,
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    };

    wrapper = shallow(<WarrantForm {...props} />);
  });

  describe('constructor()', () => {
    it('should set the warrant in the state with adapted date/time to computer timezone', () => {
      expect(wrapper.instance().state.warrant).toEqual({
        ...testMocks.warrant,
        receivedDateTime: '2020-07-29T15:20:00.000Z',
        startDateTime: '2020-07-29T16:20:00.000Z',
        stopDateTime: '2020-07-30T12:20:00.000Z',
      });
    });

    it('should set the cases in the state with adapted date/time to computer timezone', () => {
      expect(wrapper.instance().state.cases).toEqual([{
        ...testMocks.case,
        startDateTime: '2020-07-29T16:20:00.000Z',
        stopDateTime: '2020-07-30T12:20:00.000Z',
      }]);
    });
  });
});
