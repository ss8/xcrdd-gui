import { checkProps } from '../../../shared/__test__/testUtils';
import WarrantChangeTracker from './warrant-change-tracker';

// need to mock functions in props.
const defaultProps = {
  idDF: 0,
  idWarrant: null,
};

/**
  * Factory function to create a ShallowWrapper for the GuessedWords component.
  * @function setup
  * @param {object} props - Component props specific to this setup.
  * @returns {ShallowWrapper}
  */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return new WarrantChangeTracker(setupProps);
};

test('does not throw warning with expected props', () => {
  checkProps(WarrantChangeTracker, defaultProps);
});

/**
 * Add attachment tests
 */
describe('when adding an attachment to an existing warrant', () => {
  let warrantChangeTracker = null;
  const formData = {
    attachments: [
      {
        name: 'Uploaded Attachment 1', id: 'FAFSF13213DASDFA213', file: new File(['text in file'], 'file3.txt'), description: 'this is file 3',
      },
      {
        name: 'AttachmentName1', id: 'NEWID+0', file: new File(['text in file'], 'addedfile1.txt'), description: 'this is file 1',
      },
      {
        name: 'Uploaded Attachment 2', id: 'FAFSFDASDFAFDAF2123', file: new File(['text in file'], 'file2.txt'), description: 'this is file 2',
      },
    ],
  };

  beforeEach(() => {
    warrantChangeTracker = setup({ idWarrant: 'WART01' });
    const idAttachment = warrantChangeTracker.addAdded('warrant.attachment', 'addedfile1.txt', 'Attachment');
    warrantChangeTracker.setIdName(idAttachment, 'addedfile1.txt');
  });

  test('attachment shows up in the tasks', () => {
    const actions = warrantChangeTracker.getTasks(formData);
    const fileData = formData.attachments.filter((data) => (data.id === 'NEWID+0'));
    const expectedAction = [{
      caseId: undefined,
      data: fileData[0],
      displayName: 'AttachmentName1',
      id: 'NEWID+0',
      key: 'warrant.attachment.NEWID+0',
      method: 'POST',
      url: '/warrants/0/WART01/attachments',
    }];
    expect(actions).toStrictEqual(expect.arrayContaining(expectedAction)); // match types and values
  });
});

/**
 * Delete attachment tests
 */
describe('when deleting an attachment from an existing warrant', () => {
  let warrantChangeTracker = null;
  beforeEach(() => {
    warrantChangeTracker = setup({ idWarrant: 'WART01' });
    warrantChangeTracker.addDeleted('warrant.attachment.attachmentID', 'other_attach-pdf', 'Attachment');
  });

  test('the attachment is marked to be deleted in the tasks', () => {
    const formData = {};
    const actions = warrantChangeTracker.getTasks(formData);
    const expectedAction = [{
      caseId: undefined, data: undefined, displayName: 'other_attach-pdf', id: 'attachmentID', key: 'warrant.attachment.attachmentID', method: 'DELETE', url: '/warrants/0/WART01/attachments/attachmentID',
    }];
    expect(actions).toEqual(expect.arrayContaining(expectedAction));
  });
});

/**
 * Add more than one attachment tests
 */
describe('when adding multiple attachments to an existing warrant', () => {
  let warrantChangeTracker = null;
  const formData = {
    attachments: [
      {
        name: 'Uploaded Attachment 1', id: 'FAFSF13213DASDFA213', file: new File(['text in file'], 'file3.txt'), description: 'this is file 3',
      },
      {
        name: 'Uploaded Attachment 2', id: 'FAFSFDASDFAFDAF2123', file: new File(['text in file'], 'file4.txt'), description: 'this is file 4',
      },
      {
        name: 'AttachmentName1', id: 'replaced by assigned id', file: new File(['text in file'], 'addedfile1.txt'), description: 'this is file 1',
      },
      {
        name: 'AttachmentName2', id: 'replaced by assigned id', file: new File(['text in file'], 'addedfile2.txt'), description: 'this is file 2',
      },
    ],
  };

  beforeEach(() => {
    warrantChangeTracker = setup({ idWarrant: 'WART01' });
    let idAttachment = warrantChangeTracker.addAdded('warrant.attachment', 'addedfile1.txt', 'Attachment');
    warrantChangeTracker.setIdName(idAttachment, 'addedfile1.txt');
    formData.attachments[2].id = idAttachment;
    idAttachment = warrantChangeTracker.addAdded('warrant.attachment', 'addedfile2.txt', 'Attachment');
    warrantChangeTracker.setIdName(idAttachment, 'addedfile2.txt');
    formData.attachments[3].id = idAttachment;
  });

  test('attachments show up in the tasks', () => {
    const actions = warrantChangeTracker.getTasks(formData);
    const fileData1 = formData.attachments.filter(
      (data) => (data.id === formData.attachments[2].id),
    );
    const fileData2 = formData.attachments.filter(
      (data) => (data.id === formData.attachments[3].id),
    );
    const expectedAction = [{
      caseId: undefined,
      data: fileData1[0],
      displayName: 'AttachmentName1',
      id: formData.attachments[2].id,
      key: `warrant.attachment.${formData.attachments[2].id}`,
      method: 'POST',
      url: '/warrants/0/WART01/attachments',
    },
    {
      caseId: undefined,
      data: fileData2[0],
      displayName: 'AttachmentName2',
      id: formData.attachments[3].id,
      key: `warrant.attachment.${formData.attachments[3].id}`,
      method: 'POST',
      url: '/warrants/0/WART01/attachments',
    }];
    expect(actions).toEqual(expect.arrayContaining(expectedAction)); // match types and values
  });
});

/**
 * Add more than one attachment tests
 */
describe('when adding multiple attachments to an existing warrant and delete one of them', () => {
  let warrantChangeTracker = null;
  const formData = {
    attachments: [
      {
        name: 'Uploaded Attachment 1', id: 'FAFSF13213DASDFA213', file: new File(['text in file'], 'file3.txt'), description: 'this is file 3',
      },
      {
        name: 'Uploaded Attachment 2', id: 'FAFSFDASDFAFDAF2123', file: new File(['text in file'], 'file4.txt'), description: 'this is file 4',
      },
      {
        name: 'AttachmentName2', id: 'replaced by assigned id', file: new File(['text in file'], 'addedfile2.txt'), description: 'this is file 2',
      },
    ],
  };

  beforeEach(() => {
    warrantChangeTracker = setup({ idWarrant: 'WART01' });
    const idAttachment1 = warrantChangeTracker.addAdded('warrant.attachment', 'addedfile1.txt', 'Attachment');
    warrantChangeTracker.setIdName(idAttachment1, 'addedfile1.txt');
    const idAttachment2 = warrantChangeTracker.addAdded('warrant.attachment', 'addedfile2.txt', 'Attachment');
    warrantChangeTracker.setIdName(idAttachment2, 'addedfile2.txt');
    warrantChangeTracker.addDeleted(`warrant.attachment.${idAttachment1}`, 'addedfile1.txt', 'Attachment');
    formData.attachments[2].id = idAttachment2;
  });

  test('only the truly added attachment show in the tasks', () => {
    const actions = warrantChangeTracker.getTasks(formData);
    const fileData2 = formData.attachments.filter(
      (data) => (data.id === formData.attachments[2].id),
    );
    const expectedAction = [
      {
        caseId: undefined,
        data: fileData2[0],
        displayName: 'AttachmentName2',
        id: formData.attachments[2].id,
        key: `warrant.attachment.${formData.attachments[2].id}`,
        method: 'POST',
        url: '/warrants/0/WART01/attachments',
      }];
    expect(actions).toEqual(expect.arrayContaining(expectedAction)); // match types and values
  });
});
