import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { Warrant } from 'data/warrant/warrant.types';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import DynamicForm, { ChangedField } from '../../../shared/form';

type WarrantDetailsProps = {
  shouldWarrantDetailUpdate: boolean
  defaults: Warrant
  formTemplate: FormTemplate
  originalValues?: Warrant
  fieldUpdateHandler: (changedField: ChangedField) => void
  mode: string
}

class WarrantDetails extends React.Component<WarrantDetailsProps> {
  formRef: DynamicForm | null = null

  modifyContact = (defaults: any): void => {
    if (defaults && defaults.contact) {
      defaults.contact = defaults.contact.id;
    }
  }

  // when view existing warrant, set attachments button clickable
  modifyTemplateInViewMode = (fields: any): void => {
    if (this.props.mode === 'view') {
      Object.keys(fields).forEach((key:any) => {
        if (key === 'attachments') {
          fields[key].className = 'formEnabled';
        }
      });
      setAllTemplateFieldsAsReadOnly(fields);
    }
  }

  isValid(): boolean {
    if (this.formRef) {
      return this.formRef.isValid(true, true);
    }
    return true;
  }

  shouldComponentUpdate(nextProps: WarrantDetailsProps): boolean {
    return nextProps.shouldWarrantDetailUpdate;
  }

  render(): JSX.Element {
    const defaults = cloneDeep(this.props.defaults);
    const { fields } = cloneDeep(this.props.formTemplate.general as FormTemplateSection);
    this.modifyTemplateInViewMode(fields);
    this.modifyContact(defaults);
    return (
      <div>
        <DynamicForm
          ref={(ref) => { this.formRef = ref; }}
          name={this.props.formTemplate.key}
          fields={fields}
          layout={((this.props.formTemplate.general as FormTemplateSection).metadata).layout?.rows}
          defaults={defaults}
          originalValues={this.props.originalValues}
          fieldUpdateHandler={this.props.fieldUpdateHandler}
        />
      </div>
    );
  }
}

export default WarrantDetails;
