import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Dialog, Button, Label, HTMLSelect,
} from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import ActionMenuBar from 'xcipio/warrants/actions-bar/action-menu';
import { CaseModel as CaseModelType } from 'data/case/case.types';
import { Warrant } from 'data/warrant/warrant.types';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { RootState } from 'data/root.reducers';
import { setTimezoneOptions } from 'routes/Warrant/utils/warrantTemplate.adapter';
import { createWarrantBasedOnTemplate } from 'routes/Warrant/utils/warrantTemplate.factory';
import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import WarrantDetails from './warrant-details';
import CasesSummary from '../../cases/forms';
import Templates from '../../templates/warrant-template';
import WarrantConfiguration from '../../config/warrant-config';
import { warrantModel } from '../warrant-model';
import CaseModel from '../../cases/case-model.json';
import ChangeTracker from './warrant-change-tracker';
import './warrant-create.scss';
import { AppToaster, showError } from '../../../shared/toaster';
import { EditWarrantSubmit } from './warrantSubmit/warrant-edit-submit';
import { trimJsonObj } from '../../../shared/dataUtils/json-utils';
import { checkGivenTimeWithCurrentTime, areTimesEqual, checkTimeValidationInComputerTimezone }
  from '../../../shared/constants/checkTimeValidation';
import { ChangedField } from '../../../shared/form';
import * as warrantActions from '../warrant-actions';
import * as caseActions from '../../cases/case-actions';
import * as targetActions from '../../targets/targets-actions';
import { validateTargetCCC } from '../../cases/advancedProvisioning/index';
import ManageAttachments from '../../attachments/index';
import UserAudit from '../../../shared/userAudit';
import ModalDialog from '../../../shared/modal/dialog';

const newWarrant = cloneDeep(warrantModel);
const newCase = cloneDeep(CaseModel);
const fieldsPassedToCases = ['startDateTime', 'stopDateTime', 'timeZone', 'dxOwner'];
const defaultOption = {
  label: 'Select',
  value: '',
};
const popUp = {
  message: '',
  actionText: '',
  isOpen: false,
  handleClose: null,
  handleSubmit: null,
};

export const hideAttachmentFieldFromTemplate = (formTemplate: any, attachmentEnabled: boolean) => {
  if (attachmentEnabled === false && formTemplate.general.fields.attachmentsDetails) {
    formTemplate.general.fields.attachmentsDetails.hide = 'true';
  }
};

export class WarrantForm extends React.Component<any, any> {
  formTitle = ''

  submitButtonTitle = ''

  warrantDetailRef: any

  userAuditRef: any

  casesSummaryRef: any

  templates: any

  config: any

  shouldWarrantUpdate = true;

  warrantDetailsBeforeEdit: any;

  caseDetailsBeforeEdit: any;

  constructor(props: any) {
    super(props);
    this.templates = new Templates(props.warrants.templates);
    this.config = new WarrantConfiguration(props);

    // Edit warrants: Create Warrant tracker object and pass as props to components below
    let warrant = cloneDeep(props.location.warrant);
    let cases = cloneDeep(props.location.cases);

    let formTemplate = this.templates.getById(warrant?.templateUsed ?? '', Templates.CATEGORY_WARRANT);
    formTemplate = this.getFormTemplate(formTemplate);

    if (warrant == null) {
      warrant = createWarrantBasedOnTemplate(formTemplate.template);
    }

    this.warrantDetailsBeforeEdit = cloneDeep(props.location.warrant);
    this.caseDetailsBeforeEdit = cloneDeep(props.location.cases);

    // Convert date/time fields to computer timezone.
    // Note that they will be converted back to warrant timezone before submit.
    warrant = WarrantAdapter.adaptWarrantToLoad(warrant);
    cases = WarrantAdapter.adaptCasesToLoad(cases);

    if (props.location.action === CONFIG_FORM_MODE.Create && props.isWarrantDefaultExpireEnabled) {
      WarrantAdapter.setWarrantDefaultStopTime(warrant, props.warrantDefaultExpireDays);
    }

    this.state = {
      popUp,
      warrant,
      userAudit: cloneDeep(props.userAudit),
      cases,
      mode: props.location.action,
      formTemplate,
      startDateTime: undefined,
      stopDateTime: undefined,
      timeZone: undefined,
      dxOwner: undefined,
      shouldWarrantDetailUpdate: true,
      editPropsToSend: {},
      isManageAttachmentVisible: false,
      formKeyVersion: 0,
    };
    this.initialiseMode();
  }

  initialiseMode = () => {
    switch (this.state.mode) {
      case 'edit': this.initialiseEdit(); break;
      case 'view': this.initialiseView(); break;
      case 'create': this.initialiseCreate(); break;
      default: break;
    }
  }

  initialiseEdit = () => {
    this.formTitle = `Edit Warrant - ${this.state.warrant.name}`;
    this.submitButtonTitle = 'Update';
  }

  initialiseView = () => {
    this.formTitle = `View Warrant - ${this.state.warrant.name}`;
    this.submitButtonTitle = 'Ok';
  }

  initialiseCreate = () => {
    const { warrant } = this.state;

    this.formTitle = 'New Warrant';
    this.submitButtonTitle = 'Save';
    const newWarrantCase: any = cloneDeep(newCase);
    newWarrantCase.entryDateTime = new Date();
    newWarrantCase.stopDateTime = warrant.stopDateTime;
    this.state.cases.push(newWarrantCase);
  }

  static shouldUpdateWarrantFormOnStartStop(mode: string, newWarrantState: Warrant, currentWarrantState: Warrant) {
    return mode === 'view' && !!newWarrantState && !!currentWarrantState && newWarrantState?.state !== currentWarrantState?.state;
  }

  static shouldUpdateCasesOnStartStop(mode: string, currentCases: CaseModelType[], newCases: CaseModelType[]) {

    if (!currentCases?.length || !newCases?.length) {
      return false;
    }

    const casesChanged = currentCases.every((caseItem) => {
      const newCaseStatus = newCases.find((newCaseItem) => newCaseItem.id === caseItem.id)?.status;
      return caseItem.status !== newCaseStatus;
    });

    return mode === 'view' && casesChanged;
  }

  componentDidMount() {
    // We will use the EditWarrantSubmit to create warrant with attachment also.
    // so the following needs to be available when mode is create
    if (this.props.location.warrant) {
      this.props.broadcastSelectedWarrants([this.props.location.warrant]);
    }
    const propsToSend = {
      idDF: this.props.selectedDF,
      idWarrant: this.props.warrants && this.props.warrants.warrant ?
        this.props.warrants.warrant.id : null,
    };

    const changeTracker = new ChangeTracker(propsToSend);
    this.props.setWarrantTracker(changeTracker);
    if (this.props.location.action === 'create') {
      const warrantId = changeTracker.addAdded('warrant');
      changeTracker.setIdName(warrantId, '');
      this.setState({ warrant: { ...this.state.warrant, id: warrantId } });
    }
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    const newWarrantState = prevProps?.warrants?.warrant;
    const { warrant } = this.state;

    if (WarrantForm.shouldUpdateWarrantFormOnStartStop(this.state?.mode, newWarrantState, warrant)) {
      // Edit warrants: Create Warrant tracker object and pass as props to components below
      let updatedWarrant = { ...newWarrant, ...cloneDeep(newWarrantState) };
      // Convert date/time fields to computer timezone.
      // Note that they will be converted back to warrant timezone before submit.
      updatedWarrant = WarrantAdapter.adaptWarrantToLoad(updatedWarrant);

      this.props.location.warrant = updatedWarrant;

      this.warrantDetailsBeforeEdit = cloneDeep(newWarrantState);

      this.props.broadcastSelectedWarrants([updatedWarrant]);

      const formTemplate = cloneDeep(this.state.formTemplate);

      const newFormKeyVersion = this.state.formKeyVersion + 1;

      formTemplate.template.key = `warrantAndromeda.${newFormKeyVersion}`;

      this.setState({
        formTemplate,
        warrant: updatedWarrant,
        startDateTime: updatedWarrant.startDateTime,
        stopDateTime: updatedWarrant.stopDateTime,
        shouldWarrantDetailUpdate: true,
        formKeyVersion: newFormKeyVersion,
      });
    }

    const { location: { cases } } = this.props;

    if (WarrantForm.shouldUpdateCasesOnStartStop(this.state?.mode, cases, prevProps.cases?.cases)) {

      const updatedCases = prevProps.cases?.cases.map((warrantCase: CaseModelType) => {
        return {
          ...warrantCase,
          targets: this.state.cases.find((caseItem: CaseModelType) => caseItem.id === warrantCase.id).targets,
        };
      });

      this.props.location.cases = updatedCases;

      this.setState({
        cases: WarrantAdapter.adaptCasesToLoad(updatedCases),
      });

    }

  }

  getFormTemplate = (formTemplate: any) => {
    hideAttachmentFieldFromTemplate(formTemplate.template, this.props.attachmentEnabled);
    setTimezoneOptions(formTemplate.template, this.props.location.warrant, this.props.users.timezones);
    this.setContactOptions(formTemplate.template, this.props.contacts);
    this.setVisibityOptions(formTemplate.template, this.props.users);
    this.setManageAttachmentHandler(formTemplate.template);
    this.setServiceOptions(formTemplate.template.case, this.config.getServiceDropDownOptions());
    this.setFormTemplateFieldReadOnly(formTemplate, this.props.location.warrant);
    return formTemplate;
  }

  setContactOptions = (formTemplate: any, data: any) => {
    const warrantFormTemplate = formTemplate;
    const contactOptions = data.contacts.map((contact: any) => {
      return {
        label: contact.name,
        value: contact.id,
      };
    });
    contactOptions.unshift(defaultOption);
    warrantFormTemplate.general.fields.contact.options = contactOptions;
  }

  setVisibityOptions = (formTemplate: any, data: any) => {
    const warrantFormTemplate = formTemplate;
    const visibilityOptions = data.teams.map((team: any) => {
      return {
        label: team.name,
        value: team.id,
      };
    });
    warrantFormTemplate.general.fields.dxOwner.options = visibilityOptions;
  }

  setManageAttachmentHandler = (formTemplate: any) => {
    if (formTemplate.general.fields.attachmentsDetails) {
      let attachmentsLength = 0;
      if (this.props.location.warrant && this.props.location.warrant.attachments) {
        attachmentsLength = this.props.location.warrant.attachments.length;
      }
      formTemplate.general.fields.attachmentsDetails.handleClick = this.handleManageAttachmentClick;
      formTemplate.general.fields.attachmentsDetails.initial = `${attachmentsLength} Attachments`;
    }
  }

  setServiceOptions = (formTemplate: any, options: []) => {
    const caseFormTemplate = formTemplate;
    caseFormTemplate.general.fields.services.options = options;
  }

  setTimezoneOptions = (formTemplate: any, options: any[]) => {
    if (options.length > 0) {
      formTemplate.general.fields.timeZone.type = 'select';
      const timezoneOptions = options.map((timezone: string) => {
        return {
          label: timezone,
          value: timezone,
        };
      });
      timezoneOptions.unshift(defaultOption);
      if (this.props.location.warrant
        && this.props.location.warrant.timeZone
        && !options.includes(this.props.location.warrant.timeZone)) {
        timezoneOptions.push({
          label: this.props.location.warrant.timeZone,
          value: this.props.location.warrant.timeZone,
        });
      }
      formTemplate.general.fields.timeZone.options = timezoneOptions;
    }
  }

  // if startTime< curtime< stopTime, startTime & timeZone read only.
  setFormTemplateFieldReadOnly = (formTemplate: any, warrant: any) => {
    // Dates were already converted to computer timezone when loaded
    if (warrant && checkTimeValidationInComputerTimezone(warrant.startDateTime, warrant.stopDateTime)) {
      formTemplate.template.general.fields.startDateTime.disabled = true;
      formTemplate.template.general.fields.timeZone.disabled = true;
    }
  }

  handleTemplateChange = (elem: any) => {
    const id = elem.currentTarget.value;
    const template = this.templates.getById(id);
    const warrant = cloneDeep(this.state.warrant);
    const cases = this.state.cases.slice();
    const formTemplate = this.getFormTemplate(template);

    this.setWarrantFromTemplate(warrant, formTemplate);

    this.setState({
      formTemplate,
      warrant,
      shouldWarrantDetailUpdate: true,
      cases,
    });

    this.shouldWarrantUpdate = true;

    // when warrant template update, fire changeTracker warrant
    if (this.props.warrants.changeTracker) {
      this.props.warrants.changeTracker.addUpdated('warrant');
    }
  }

  setWarrantFromTemplate = (warrant: any, template: any) => {
    const { fields }: any = template.template.general;
    Object.keys(fields).forEach((field: any) => {
      const { initial } = fields[field];
      // Dates were already converted to computer timezone when loaded
      if (initial !== undefined && !(this.state.mode === 'edit' && field === 'startDateTime' &&
        checkTimeValidationInComputerTimezone(warrant.startDateTime, warrant.stopDateTime))) {
        warrant[field] = initial;
      }
    });
  }

  setWarrantCaseType = (warrant: any, caseType = 'ALL') => {
    if (!warrant.caseType) {
      warrant.caseType = caseType;
    }
  }

  handleUpdateClose = () => {
    this.setState({ editPropsToSend: {} });
  }

  updateWarrant = () => {
    let formData: any = {};
    AppToaster.clear();
    formData = cloneDeep(this.state.warrant);
    formData.templateUsed = this.state.formTemplate.id;
    formData.cases = this.casesSummaryRef.getCases();
    this.setWarrantCaseType(formData);
    formData = trimJsonObj(formData);
    const errors: any[] = [];

    this.checkWarrantValid(errors);
    this.validateInterfieldRules(errors, formData);

    if (errors.length > 0) {
      errors.forEach((error: any) => {
        showError(error);
      });
    } else {
      this.shouldWarrantUpdate = true;
      const { selectedDF } = this.props;
      // Must assign attachments to formData here because the above trimJsonObj() changes
      // the file object in the attachment and breaks the upload file.
      // Since edit submit does not change the attachments object, we do not need to do a deep copy.
      formData.attachments = this.state.warrant.attachments;
      let userAudit: any = {};
      if (this.props.isAuditEnabled && this.userAuditRef) {
        userAudit = this.userAuditRef.getValue();
      }
      const editPropsToSend = {
        selectedDF,
        formData,
        userAudit,
        isOpen: true,
      };
      this.setState({ editPropsToSend });
    }
  }

  handleManageAttachmentClick = () => {
    this.shouldWarrantUpdate = true;
    this.setState({ isManageAttachmentVisible: true });
  }

  onSubmitPopUpClose = () => {
    this.setState({ isSubmitDialogOpen: false });
  }

  saveWarrant = () => {
    let formData: any = {};
    AppToaster.clear();
    formData = cloneDeep(this.state.warrant);
    formData.templateUsed = this.state.formTemplate.id;
    formData.cases = this.casesSummaryRef.getCases();
    this.setWarrantCaseType(formData);
    formData = trimJsonObj(formData);
    const { selectedDF } = this.props;
    const errors: any[] = [];
    this.checkWarrantValid(errors);
    this.validateInterfieldRules(errors, formData);
    if (errors.length > 0) {
      errors.forEach((error: any) => {
        showError(error);
      });
    } else {
      this.shouldWarrantUpdate = true;
      // Must assign attachments to formData here because the above trimJsonObj() changes
      // the file object in the attachment and breaks the upload file.
      // Since edit submit does not change the attachments object, we do not need to do a deep copy.
      formData.attachments = this.state.warrant.attachments;
      let userAudit: any = {};
      if (this.props.isAuditEnabled && this.userAuditRef) {
        userAudit = this.userAuditRef.getValue();
      }
      const editPropsToSend = {
        selectedDF,
        formData,
        userAudit,
        isOpen: true,
      };
      this.setState({ editPropsToSend });
    }
  }

  checkWarrantValid = (errors: any[]) => {
    if (!this.warrantDetailRef.isValid()) {
      errors.push('Warrant :: Required field values missing or invalid');
    }
    const cases = this.casesSummaryRef.getCases();
    if (!this.casesSummaryRef.isCurrentCaseValid(true)) {
      errors.push('Selected Case :: Required field values missing or invalid');
    }
    cases.forEach((warrantCase: any, index: number) => {
      if (this.casesSummaryRef.getCurrentCaseIndex() !== index && warrantCase.isValid === false) {
        errors.push(`Case :: ${warrantCase.liId}- Required field values missing or invalid`);
      }
      this.validateCaseTargets(warrantCase, errors);
    });
    if (this.userAuditRef && !this.userAuditRef.isValid()) {
      errors.push('audit :: Required field values missing or invalid');
    }
  }

  validateCaseTargets = (warrantCase: any, errors: any) => {
    const targetAfDetails = this.config.getTargetWithAllAfs();
    const isSurOptCombined = warrantCase && warrantCase.options
      && warrantCase.options.circuitSwitchedVoiceCombined;
    const { targets } = warrantCase;
    validateTargetCCC(targets, targetAfDetails, isSurOptCombined,
      warrantCase?.type, errors, `Error- ${warrantCase.name}`);
  }

  validateInterfieldRules = (errors: any[], formData: any) => {
    const warrantStartDateTimeInDate = (!formData.startDateTime || formData.startDateTime === '') ? new Date() : new Date(formData.startDateTime);
    const warrantStopDateTimeInDate = new Date(formData.stopDateTime);

    if (this.props.warrants.warrant == null) { // new warrant
      if (checkGivenTimeWithCurrentTime(formData.startDateTime, formData.timeZone)) {
        errors.push('Warrant : Start Time should be after the current time');
        return;
      }
      if (checkGivenTimeWithCurrentTime(formData.stopDateTime, formData.timeZone)) {
        errors.push('Warrant : Stop Time should be after the current time');
        return;
      }
      if (warrantStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
        errors.push('Warrant : Stop Time should be same or after the Start Time');
        return;
      }
    } else if (this.props.warrants.warrant && this.props.warrants.warrant.timeZone) {
      const originTimeZone = this.props.warrants.warrant.timeZone;
      const updatedTimeZone = formData.timeZone;
      const warrantOriginStartTime = this.props.warrants.warrant.startDateTime;
      const warrantOriginStopTime = this.props.warrants.warrant.stopDateTime;

      const hasAnyTimeChange = !areTimesEqual(
        warrantOriginStartTime,
        warrantOriginStopTime,
        originTimeZone,
        formData.startDateTime,
        formData.stopDateTime,
        updatedTimeZone,
      );

      if (hasAnyTimeChange) {
        // Dates were already converted to computer timezone when loaded
        const timeValidation = checkTimeValidationInComputerTimezone(
          warrantOriginStartTime,
          warrantOriginStopTime,
        );

        if (!timeValidation && checkGivenTimeWithCurrentTime(formData.startDateTime, formData.timeZone)) {
          errors.push('Warrant : Start Time and Stop Time should be after the current time');
          return;
        }

        if (checkGivenTimeWithCurrentTime(formData.stopDateTime, formData.timeZone)) {
          errors.push('Warrant : Stop Time should be after the current time');
          return;
        }

        if (warrantStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
          errors.push('Warrant : Stop Time should be same or after the Start Time');
          return;
        }
      }
    }
    const originCases = cloneDeep(this.props.location.cases);
    for (let i = 0; i < formData.cases.length; i += 1) {
      let originCase: any;
      const updateCase = formData.cases[i];
      let j: number;
      for (j = 0; j < originCases.length; j += 1) {
        if (formData.cases[i].liId === originCases[j].liId) {
          originCase = originCases[j];
          break;
        }
      }
      if (originCase === 'undefined' || originCase === undefined) { // new case
        this.checkTimeValidationRule(updateCase, formData, errors);
        if (checkGivenTimeWithCurrentTime(updateCase.startDateTime,
          updateCase.timeZone)) {
          errors.push(`Case -${updateCase.liId} : Start Time should be after the current time`);
          return;
        }
      } else if (originCase) { // existing case
        const hasAnyTimeChange = !areTimesEqual(
          originCase.startDateTime,
          originCase.stopDateTime,
          originCase.timeZone,
          updateCase.startDateTime,
          updateCase.stopDateTime,
          updateCase.timeZone,
        );

        if (hasAnyTimeChange) {
          // Dates were already converted to computer timezone when loaded
          const timeValidation = checkTimeValidationInComputerTimezone(
            originCase.startDateTime,
            originCase.stopDateTime,
          );

          this.checkTimeValidationRule(updateCase, formData, errors);
          if (!timeValidation && checkGivenTimeWithCurrentTime(updateCase.startDateTime,
            updateCase.timeZone)) {
            errors.push(`Case -${updateCase.liId} : Start Time and Stop Time should be after the current time`);
            return;
          }
        }
      }
    }
  }

  checkTimeValidationRule = (updateCase: any, formData: any, errors: any) => {
    const warrantStartDateTimeInDate = (!formData.startDateTime || formData.startDateTime === '') ? new Date() : new Date(formData.startDateTime);
    const warrantStopDateTimeInDate = new Date(formData.stopDateTime);
    const caseStartDateTimeInDate = (!updateCase.startDateTime || updateCase.startDateTime === '') ? new Date() : new Date(updateCase.startDateTime);
    const caseStopDateTimeInDate = new Date(updateCase.stopDateTime);
    if (caseStartDateTimeInDate.getTime() > caseStopDateTimeInDate.getTime()) {
      errors.push(`Case -${updateCase.liId} : Stop Time should be same or after the Start Time`);
      return;
    }
    if (caseStartDateTimeInDate.getTime() < warrantStartDateTimeInDate.getTime() ||
      caseStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
      errors.push(`Case -${updateCase.liId} : Start Time should be within Warrant's Start Time and Stop Time`);
      return;
    }
    if (caseStopDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime() ||
      caseStopDateTimeInDate.getTime() < warrantStartDateTimeInDate.getTime()) {
      errors.push(`Case -${updateCase.liId} : Stop Time should be within Warrant's Start Time and Stop Time`);
      return;
    }
    if (checkGivenTimeWithCurrentTime(updateCase.stopDateTime, updateCase.timeZone)) {
      errors.push(`Case -${updateCase.liId} : Stop Time should be after the current time`);
    }
  }

  warrantDetailsFieldUpdateHandler = (changedField: ChangedField) => {
    const {
      name, value,
    } = changedField;
    // Edit warrants: Update Warrant
    const warrant = cloneDeep(this.state.warrant);
    warrant[name] = value;
    this.shouldWarrantUpdate = false;
    let fieldPassedToCases = '';
    if (fieldsPassedToCases.includes(name)) {
      fieldPassedToCases = name;
    }
    if (name === 'contact') {
      if (value === 'undefined') {
        warrant[name] = null;
      } else {
        warrant[name] = {
          id: value,
          name: value,
        };
      }
    }
    const newState: any = {
      warrant,
      shouldWarrantDetailUpdate: false,
    };
    if (fieldPassedToCases) {
      newState[fieldPassedToCases] = value;
      this.shouldWarrantUpdate = true;
    }
    this.setState(newState);
    // when WarrantDetails fields update, fire changeTracker warrant
    if (this.props.warrants.changeTracker) {
      this.props.warrants.changeTracker.addUpdated('warrant');
    }
  }

  handleManageAttachmentClose = () => {
    this.setState({ isManageAttachmentVisible: false });
  }

  handleManageAttachmentSubmit = (attachments: any) => {
    const warrant = cloneDeep(this.state.warrant);
    warrant.attachments = attachments;
    const formTemplate = { ...this.state.formTemplate };
    formTemplate.template.general.fields.attachmentsDetails.initial = `${attachments.length} Attachments`;
    this.setState({
      warrant,
      formTemplate,
      isManageAttachmentVisible: false,
      shouldWarrantDetailUpdate: true,
    });
  }

  handleCancel = () => {
    if (this.props.warrants.changeTracker.isChanged()) {
      this.shouldWarrantUpdate = true;
      const message = 'Any changes will be lost. Click \'Ok\' to continue.';
      const nextpopUp = {
        message,
        actionText: 'Ok',
        isOpen: true,
        handleClose: () => {
          const newPopUp = { ...this.state.popUp };
          newPopUp.isOpen = false;
          this.shouldWarrantUpdate = true;
          this.setState({
            popUp: newPopUp,

          });
        },
        handleSubmit: () => {
          const newPopUp = { ...this.state.popUp };
          newPopUp.isOpen = false;
          this.shouldWarrantUpdate = true;
          this.setState({
            popUp: newPopUp,
          });
          AppToaster.clear();
          this.props.history.replace('/warrants');
        },
      };
      this.setState({ popUp: nextpopUp });
    } else {
      AppToaster.clear();
      this.props.history.replace('/warrants');
    }
  }

  handleOk = () => {
    AppToaster.clear();
    this.props.history.replace('/warrants');
  }

  handleEditOnView = () => {
    AppToaster.clear();
    this.props.location.handleEditWarrant();
  }

  shouldComponentUpdate = () => {
    return this.shouldWarrantUpdate;
  }

  componentWillUnmount(): void {
    const {
      clearWarrantState,
      clearCasesState,
      clearTargetState,
      clearSelectedWarrants,
    } = this.props;

    clearWarrantState();
    clearCasesState();
    clearTargetState();
    clearSelectedWarrants();
  }

  handleActionPopupClose = async (action: string) => {
    if (
      action.toLowerCase() === COMMON_GRID_ACTION.Start
      || action.toLowerCase() === COMMON_GRID_ACTION.Stop
      || action.toLowerCase() === COMMON_GRID_ACTION.Pause
      || action.toLowerCase() === COMMON_GRID_ACTION.Resume
      || action.toLowerCase() === COMMON_GRID_ACTION.Reschedule
      || action.toLowerCase() === COMMON_GRID_ACTION.Reprovision
    ) {
      AppToaster.clear();
    } else {
      this.handleOk();
      // this.props.location.handleViewWarrant();
    }
  }

  handleEditWarrant = () => {
    // pass the cases
    this.props.location.handleEditWarrant(this.state.warrant, this.state.cases);
  }

  handleEditWarrantWizard = (): void => {
    const {
      history,
      goToWarrantEdit,
    } = this.props;

    goToWarrantEdit(history);
  }

  handleViewWarrantWizard = (): void => {
    const {
      history,
      goToWarrantView,
    } = this.props;

    goToWarrantView(history);
  }

  handleReloadWarrant = () => {
    const { selectedDF: dfId } = this.props.global;
    const caseIds = this.props.location.cases.map((c: any) => c.id);

    this.props.getWarrantById(dfId, this.state.warrant?.id)
      .then(() => this.props.getAllCasesByCaseIds(dfId, this.state.warrant?.id, caseIds.join(',')));
  }

  render() {
    return (
      <Dialog
        isOpen
        style={{
          // padding is changed to % to fix the DX-1094 issue
          background: 'white', padding: '1%', width: '80%', height: 'fit-content',
        }}
      >
        <div className="newWarrantHeader">
          <h2 className="newWarrantTitle">{this.formTitle}</h2>
          <span className="button-actions">
            {this.state.mode === 'view' && (
              <ActionMenuBar
                handleEdit={this.handleEditWarrant}
                handleClose={this.handleActionPopupClose}
                onReloadWarrant={this.handleReloadWarrant}
              />
            )}
          </span>
          <span>
            {this.state.mode === 'view' ? (
              <Button
                className="actionButton"
                text="Back"
                intent="primary"
                onClick={this.handleOk}
              />
            ) : (
              <Button
                className="actionButton"
                text="Cancel"
                intent="none"
                rightIcon="cross"
                onClick={this.handleCancel}
              />
            )}
          </span>
          <span>
            {this.state.mode !== 'view' && (
              <Button
                className="actionButton"
                text={this.submitButtonTitle}
                intent="primary"
                rightIcon="arrow-right"
                active
                onClick={this.state.mode === 'create' ? this.saveWarrant : this.updateWarrant}
              />
            )}
          </span>
        </div>
        <div className={this.state.mode === 'view' ? 'formDisabled' : 'formEnabled'}>
          <Label className="bp3-inline">
            Template :
            <HTMLSelect
              options={this.templates.getDropdownOptions(Templates.CATEGORY_WARRANT)}
              onChange={this.handleTemplateChange}
              value={this.state.formTemplate.id}
            />
          </Label>
          <Label className="bp3-inline">
            State : {this.state.warrant.state}
          </Label>
        </div>
        <EditWarrantSubmit
          selectedDF={this.state.editPropsToSend.selectedDF}
          formData={this.state.editPropsToSend.formData}
          userAudit={this.state.editPropsToSend.userAudit}
          userId={this.props.authentication.userDBId}
          userName={this.props.authentication.userId}
          auditEnabled={this.props.isAuditEnabled}
          history={this.props.history}
          changeTracker={this.props.warrants.changeTracker}
          isOpen={this.state.editPropsToSend.isOpen}
          onClose={this.handleUpdateClose}
          mode={this.state.mode}
          isDialog
          warrantBeforeFieldValues={this.warrantDetailsBeforeEdit}
          caseBeforeFieldValues={this.caseDetailsBeforeEdit}
        />
        <WarrantDetails
          ref={(ref: any) => { this.warrantDetailRef = ref; }}
          formTemplate={this.state.formTemplate.template}
          defaults={this.state.warrant}
          shouldWarrantDetailUpdate={this.state.shouldWarrantDetailUpdate}
          fieldUpdateHandler={this.warrantDetailsFieldUpdateHandler}
          mode={this.state.mode}
          originalValues={this.props.location.warrant}
        />
        <ManageAttachments
          isOpen={this.state.isManageAttachmentVisible}
          warrantAttachments={this.state.warrant.attachments}
          handleClose={this.handleManageAttachmentClose}
          handleSubmit={this.handleManageAttachmentSubmit}
          history={this.props.history}
          mode={this.state.mode}
          changeTracker={this.props.warrants.changeTracker}
        />
        {this.props.isAuditEnabled &&
          (this.state.mode === 'create' || this.state.mode === 'edit') && (
            <UserAudit
              data-test="userAudit"
              ref={(ref: any) => { this.userAuditRef = ref; }}
              defaults={this.state.userAudit}
              mode={this.state.mode}
            />
        )}
        <CasesSummary
          ref={(ref) => { this.casesSummaryRef = ref; }}
          cases={this.state.cases}
          warrant={this.state.warrant}
          originalValues={this.props.location.cases}
          customizationconfig={this.props.customizationconfig}
          startDateTime={this.state.startDateTime}
          stopDateTime={this.state.stopDateTime}
          timeZone={this.state.timeZone}
          dxOwner={this.state.dxOwner}
          mode={this.state.mode}
          formTemplate={this.state.formTemplate.template.case}
          config={this.config}
          global={this.props.global}
          templates={this.props.warrants.templates}
          getAllTargetsByTargetIds={this.props.getAllTargetsByTargetIds}
          shouldCaseSummaryUpdate={this.state.shouldCaseSummaryUpdate}
          warrantId={this.state.warrant ? this.state.warrant.id : undefined}
          changeTracker={this.props.warrants.changeTracker}
        />
        <ModalDialog
          displayMessage={this.state.popUp.message}
          isOpen={this.state.popUp.isOpen}
          onSubmit={this.state.popUp.handleSubmit}
          onClose={this.state.popUp.handleClose}
          actionText={this.state.popUp.actionText}
        />
      </Dialog>
    );
  }
}

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bAttachments_enabled: attachmentEnabled,
      isAuditEnabled,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
    },
  },
  warrants: { customizationconfig, userAudit },
  global: { selectedDF },
  authentication,
}: RootState) => ({
  attachmentEnabled,
  isAuditEnabled,
  customizationconfig,
  userAudit,
  selectedDF,
  authentication,
  isWarrantDefaultExpireEnabled,
  warrantDefaultExpireDays,
});

const mapDispatchToProps = {
  clearWarrantState: warrantActions.clearWarrantState,
  clearCasesState: caseActions.clearCasesState,
  clearTargetState: targetActions.clearTargetState,
  broadcastSelectedWarrants: warrantActions.broadcastSelectedWarrants,
  clearSelectedWarrants: warrantActions.clearSelectedWarrants,
  goToWarrantEdit: warrantActions.goToWarrantEdit,
  goToWarrantView: warrantActions.goToWarrantView,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WarrantForm));
