import { Text } from '@blueprintjs/core';
import React, { ReactNode } from 'react';
import { AppToaster } from 'shared/toaster';

type Props = {
  hasErrors: boolean
  msgs: string[]
  mode?: string
  isDialog?: boolean
  className?: string
}

export default class ShowStatusMessage extends React.Component<Props> {

  static renderErrorMessage(msgs: string[]): ReactNode {
    const [first, ...rest] = msgs;

    return (
      <React.Fragment>
        <p><strong>{first}</strong></p>
        {
          rest.map((msg) => <p>{msg}</p>)
        }
      </React.Fragment>
    );
  }

  shouldComponentUpdate(nextProps: Props): boolean {
    return nextProps.msgs && nextProps.msgs.length > 0;
  }

  componentDidUpdate(): void {
    AppToaster.clear();
    const {
      hasErrors,
      msgs,
      mode,
      isDialog,
    } = this.props;

    if (!hasErrors || !msgs || mode === 'edit' || isDialog) {
      return;
    }

    AppToaster.show({
      icon: 'error',
      intent: 'danger',
      timeout: 0,
      className: 'show-status-message-bp3-toast',
      message: ShowStatusMessage.renderErrorMessage(msgs),
    });
  }

  renderEditMode(): JSX.Element {
    const {
      hasErrors,
      msgs,
      className,
    } = this.props;

    const colorClassName = hasErrors && msgs ? 'error' : 'success';
    return (
      <div className={`status-form-container ${className}`}>
        {msgs.map((item) => {
          return (
            <div key={item} className={`row status-container ${colorClassName} `}>
              <Text className="col-md-12">
                {item}
              </Text>
            </div>
          );
        })}
      </div>
    );
  }

  render(): ReactNode {
    const { mode, isDialog } = this.props;

    if (mode === 'edit' || isDialog) {
      return this.renderEditMode();
    }

    return null;
  }
}
