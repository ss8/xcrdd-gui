export const warrantAddBeforeMaskData = {
  templateUsed: 'default',
  dxOwner: '',
  dxAccess: '',
  name: 'Test warrant audit',
  city: '',
  comments: '',
  contact: null,
  judge: '',
  receivedDateTime: '',
  region: '',
  startDateTime: '2020-09-25T07:00:00.000Z',
  stopDateTime: '2020-12-18T08:00:00.000Z',
  timeZone: 'America/Adak',
  cases: [
    {
      device: '',
      dxAccess: '',
      dxOwner: '',
      liId: 'case 1',
      name: '',
      cfId: {
        id: 'Q0EtRE9KQk5FLTMzMTAx',
        name: 'CA-DOJBNE-33108_3',
      },
      options: {
        kpi: false,
        traceLevel: 0,
        configureOptions: false,
        packetEnvelopeMessage: false,
        packetEnvelopeContent: false,
        realTimeText: false,
        callPartyNumberDisplay: false,
        directDigitExtractionData: false,
        monitoringReplacementPartiesAllowed: false,
        circuitSwitchedVoiceCombined: false,
        includeInBandOutBoundSignalling: false,
        encryptionType: 'None',
        encryptionKey: '',
        provideTargetIdentityInCcChannel: '',
        cccBillingType: '',
        cccBillingNumber: '',
        deactivationMode: '',
        location: false,
        target: false,
        sms: false,
        callIndependentSupplementaryServicesContent: false,
        packetSummaryCount: 0,
        packetSummaryTimer: 0,
        generatePacketDataHeaderReport: false,
        packetHeaderInformationReportFilter: 'NONE',
        callTraceLogging: 0,
        featureStatusInterval: 0,
        surveillanceStatusInterval: 0,
      },
      services: [
        'LTE-IOT',
      ],
      genccc: {

      },
      startDateTime: '2020-09-25T07:00:00.000Z',
      stopDateTime: '2020-12-18T08:00:00.000Z',
      targets: [
        {
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [

            ],
            afGroups: [

            ],
            afAutowireTypes: [
              {
                id: 'ERK_SCEF',
                name: 'ERK_SCEF',
              },
            ],
            afAutowireGroups: [

            ],
            afccc: [

            ],
          },
          primaryType: 'MSISDN',
          primaryValue: '4567845674567',
          secondaryType: '',
          secondaryValue: '',
          options: [

          ],
          id: 'MSISDN_1592853200548',
          isRequired: true,
        },
      ],
      timeZone: 'America/Adak',
      type: 'ALL',
      isValid: true,
      templateUsed: null,
      entryDateTime: '2020-06-22T19:12:38.122Z',
      hi3Interfaces: [

      ],
    },
  ],
  attachments: [

  ],
  province: '',
  department: '',
  id: 'NEWID+0',
  caseType: 'ALL',
};

export const warrantAddAfterMaskData = {
  templateUsed: 'default',
  dxOwner: '',
  dxAccess: '',
  name: 'Test warrant audit',
  city: '',
  comments: '',
  contact: null,
  judge: '',
  receivedDateTime: '',
  region: '',
  startDateTime: '2020-09-25T07:00:00.000Z',
  stopDateTime: '2020-12-18T08:00:00.000Z',
  timeZone: 'America/Adak',
  cases: [
    {
      device: '',
      dxAccess: '',
      dxOwner: '',
      liId: 'case 1',
      name: '',
      cfId: {
        id: 'Q0EtRE9KQk5FLTMzMTAx',
        name: 'CA-DOJBNE-33108_3',
      },
      options: {
        kpi: false,
        traceLevel: 0,
        configureOptions: false,
        packetEnvelopeMessage: false,
        packetEnvelopeContent: false,
        realTimeText: false,
        callPartyNumberDisplay: false,
        directDigitExtractionData: false,
        monitoringReplacementPartiesAllowed: false,
        circuitSwitchedVoiceCombined: false,
        includeInBandOutBoundSignalling: false,
        encryptionType: 'None',
        encryptionKey: '',
        provideTargetIdentityInCcChannel: '',
        cccBillingType: '',
        cccBillingNumber: '',
        deactivationMode: '',
        location: false,
        target: false,
        sms: false,
        callIndependentSupplementaryServicesContent: false,
        packetSummaryCount: 0,
        packetSummaryTimer: 0,
        generatePacketDataHeaderReport: false,
        packetHeaderInformationReportFilter: 'NONE',
        callTraceLogging: 0,
        featureStatusInterval: 0,
        surveillanceStatusInterval: 0,
      },
      services: [
        'LTE-IOT',
      ],
      genccc: {

      },
      startDateTime: '2020-09-25T07:00:00.000Z',
      stopDateTime: '2020-12-18T08:00:00.000Z',
      targets: [
        {
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [

            ],
            afGroups: [

            ],
            afAutowireTypes: [
              {
                id: 'ERK_SCEF',
                name: 'ERK_SCEF',
              },
            ],
            afAutowireGroups: [

            ],
            afccc: [

            ],
          },
          primaryType: 'MSISDN',
          primaryValue: '*****',
          secondaryType: '',
          secondaryValue: '',
          options: [

          ],
          id: 'MSISDN_1592853200548',
          isRequired: true,
        },
      ],
      timeZone: 'America/Adak',
      type: 'ALL',
      isValid: true,
      templateUsed: null,
      entryDateTime: '2020-06-22T19:12:38.122Z',
      hi3Interfaces: [

      ],
    },
  ],
  attachments: [

  ],
  province: '',
  department: '',
  id: 'NEWID+0',
  caseType: 'ALL',
};
