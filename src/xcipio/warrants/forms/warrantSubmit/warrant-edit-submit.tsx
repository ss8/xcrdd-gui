/* eslint-disable max-len */
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Dialog, Classes, Button } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { AppToaster, showSuccessMessage } from '../../../../shared/toaster';
import {
  editWarrant,
  createWarrant,
} from '../../warrant-actions';
import ShowStatusMessage from '../updateWarrant/show-status';
import ShowProgress from '../updateWarrant/show-progress';
import convertErrorCodeToMessage from '../../../../shared/errorCodeConvert/convertErrorCodeToMessage';
import { PROGRESS_STATUS } from '../../../../shared/wizard/discovery-wizard';
import {
  AuditService, AuditActionType, getAuditDetails, getAuditFieldInfo,
} from '../../../../shared/userAudit/audit-constants';

const maskedValue = '*****';

const maskKeys = ['primaryValue', 'secondaryValue'];

export class EditWarrantSubmit extends React.Component<any, any> {
  private hasErrors = false;

  constructor(props: any) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen,
      progress: [],
      msgs: [],
    };
  }

  async componentDidMount() {
    if (this.props.isOpen && this.props.isDialog === false) {
      this.resetSubmit(this.props.isOpen);

      if (this.props.mode === 'edit') {
        this.updateWarrant();
      }
    }
  }

  componentDidUpdate(prevProps: any): void {
    const {
      isDialog,
      isOpen,
      mode,
    } = this.props;

    if (isOpen && !prevProps.isOpen && isDialog) {
      this.resetSubmit(isOpen);

      if (mode === CONFIG_FORM_MODE.Edit) {
        this.updateWarrant();
      } else {
        this.createCompleteWarrant();
      }
    }
  }

  getErrorMessage = (error:any) => {
    let errorMessage = error;
    if (error) {
      if (error.response && error.response.data && error.response.data.message) {
        errorMessage = error.response.data.message;
      } else if (error.response && error.response.statusText) {
        errorMessage = error.response.statusText;
      } else if (error.message) {
        errorMessage = error.message;
      }
    }
    return errorMessage;
  }

  replaceOldIds = (item:any) => {
    const tracker = this.props.changeTracker;
    const urlParts = item.url.split('/');
    for (let i = 0; i < urlParts.length - 1; i += 1) {
      const part = urlParts[i];
      if (part.startsWith('case')) {
        const caseId = urlParts[i + 1];
        const newCaseId = tracker.getNewCaseIdByOldCaseId(caseId);
        if (newCaseId) {
          item.url = item.url.replace(caseId, newCaseId);
          item.caseId = newCaseId;
        }
      } else if (part.startsWith('target')) {
        const targetId = urlParts[i + 1];
        const newTargetId = tracker.getNewTargetIdByOldTargetId(item.caseId, targetId);
        if (newTargetId) {
          item.url = item.url.replace(targetId, newTargetId);
        }
      } else if (part.startsWith('warrants') && i < urlParts.length - 2) {
        const warrantId = urlParts[i + 2];
        const newWarrantId = tracker.getWarrantId();
        if (newWarrantId) {
          item.url = item.url.replace(warrantId, newWarrantId);
        }
      }
    }
  }

  isCreateWarrantAction = (url:string) => {
    const urlParts = url.split('/'); // this returns ['', 'warrants', 'NEW+01'] for warrant
    return (urlParts.length === 3 && urlParts[1] === 'warrants');
  }

  getAllAuditDetails = (recordName: string, service: string, actionType: string, fields: any) => {
    const userId = this.props.authentication ? this.props.authentication.userDBId : this.props.userId;
    const userName = this.props.authentication ? this.props.authentication.userId : this.props.userName;
    return getAuditDetails(
      this.props.auditEnabled,
      userId,
      userName,
      fields,
      recordName,
      service,
      actionType,
    );
  }

  maskTargetValues = (warrantData: any) => {
    if (!warrantData.cases) return warrantData;
    warrantData.cases.forEach((caseDetail: any) => {
      const { targets } = caseDetail;
      if (targets) {
        targets.forEach((target: any) => {
          Object.keys(target).forEach((key: any) => {
            if (maskKeys.includes(key) && target[key] && target[key] !== '') {
              target[key] = maskedValue;
            }
          });
        });
      }
    });
    return warrantData;
  }

  createCompleteWarrant = async () => {
    let { formData, userAudit } = this.props;
    const { auditEnabled } = this.props;
    if (formData === undefined) {
      formData = cloneDeep(this.props.warrant);
      const casesToInclude = this.props.cases.filter((caseInfo : any) => (caseInfo.includeInWarrant === undefined || caseInfo.includeInWarrant === true));
      formData.cases = cloneDeep(casesToInclude.slice());
    }

    // Convert date/time fields to the warrant timezone
    formData = WarrantAdapter.adaptWarrantToSubmit(formData);
    if (userAudit) {
      const { timeZone } = formData;
      userAudit = WarrantAdapter.adaptAuditToSubmit(userAudit, timeZone);
    }

    const tracker = this.props.changeTracker;

    let actions = tracker.getTasks(formData);
    // only include create warrant action and create attachment actions
    actions = actions.filter((action:any) => {
      return action.url.includes('attachments') || this.isCreateWarrantAction(action.url);
    });
    // add create warrant action
    // const createWarrantAction = {
    //   method: 'POST', url: `/warrants/${selectedDF}`, id: 'NEW+01', data: formData,
    // caseId: null, key: 'warrant.NEW+01', displayName: 'warrant',
    // };
    // actions.unshift(createWarrantAction);

    let completeStatus: PROGRESS_STATUS | null = null;
    let numOperationCompletedSuccessfully = 0;
    // Upload attachments.  The actions should only have attachments
    // for creation scenario
    for (let i = 0; i < actions.length; i += 1) {
      const item = actions[i];
      const taskDescription = tracker.describeTask(item, formData);

      try {
        if (this.props.abortOperation) {
          const msgList = [...this.state.msgs];
          if (numOperationCompletedSuccessfully > 1) {
            completeStatus = PROGRESS_STATUS.AbortedMustExit;
            msgList.push('Operation Aborted.');
            msgList.push('Warrant is already created, but some attachments are not uploaded');
            msgList.push('Please close this popup. Then edit the warrant to make any additional changes');
          } else {
            completeStatus = PROGRESS_STATUS.AbortedCanModify;
            msgList.push('Operation Aborted. Warrant is not created.');
            msgList.push('If you would need to make any changes, please click the Back button.');
          }
          this.setState({
            msgs: msgList,
          });
          this.hasErrors = true;
          break;
        }
        if (item.status !== 200) {
          this.replaceOldIds(item);

          // progress bar for UI
          const progressList = cloneDeep(this.state.progress);
          progressList.push({ id: item.id, text: taskDescription, loading: true });
          item.loading = true;
          this.setState({ progress: progressList });

          // create warrant
          let res: any = null;
          const isCreateMode = this.isCreateWarrantAction(item.url);
          const dataWithMaskedTargetValues = this.maskTargetValues(cloneDeep(item.data));
          const fieldInfo = getAuditFieldInfo(dataWithMaskedTargetValues, userAudit);

          const auditDetails = auditEnabled
            ? this.getAllAuditDetails(item.data.name,
              AuditService.WARRANTS,
              isCreateMode ? AuditActionType.WARRANT_ADD : AuditActionType.WARRANT_EDIT,
              fieldInfo)
            : null;

          if (isCreateMode) {
            const body = { data: item.data, auditDetails };
            res = await createWarrant(item.url, body);
            if (res.status === 200 && res.data && res.data.success === true &&
              res.data.metadata && res.data.metadata.idCreated) {
              tracker.setWarrantId(res.data.metadata.idCreated);
            }
          } else {
            res = await editWarrant(item.method, item.url, { data: item.data, auditDetails });
          }
          // Submit to the server
          item.Status = res.status;
          this.updateProgressList(item);
          tracker.completeTask(item);
          numOperationCompletedSuccessfully += 1;
        }
      } catch (error) {
        this.hasErrors = true;
        item.Status = 'FAILED';
        this.updateProgressList(item, true);
        let message = '';
        try {
          const errorFieldName = JSON.parse(error.response.config.data).name;
          const errorCode = error.response.data.error.code;
          const errorMessage = error.response.data.error.message;
          const curError = { errorFieldName, errorCode, errorMessage };
          message = convertErrorCodeToMessage(curError);
        } catch (e) {
          message = error.message;
        }

        let msgList = [`${taskDescription} - FAILED`];
        msgList.push(message);
        if (numOperationCompletedSuccessfully > 1) {
          completeStatus = PROGRESS_STATUS.FailedMustExit;
          // if it is not on a dialog, it is on a wizard panel where there is
          if (actions.length > numOperationCompletedSuccessfully) {
            msgList.push('Warrant is already created, but some attachments are not uploaded');
            msgList.push('Please close this popup. Then edit the warrant to make any additional changes');
          }
        } else {
          completeStatus = PROGRESS_STATUS.FailedCanModify;
          if (this.props.isDialog === false) {
            msgList = [];
            msgList.push('Warrant not created.');
            msgList.push(message);
            msgList.push('Press Back button to make any adjustments needed.');
          }
        }
        this.setState({
          msgs: msgList,
        });
        break;
      }
    }

    if (!this.hasErrors) {
      completeStatus = PROGRESS_STATUS.Successful;
      const msgList = ['Warrant created successfully.'];
      this.setState({ msgs: msgList });

      if (!this.props.isDialog) {
        showSuccessMessage(msgList[0], 5000);
        this.props.history.replace('/warrants');
      }
    }

    if (this.props.onSubmitComplete) {
      this.props.onSubmitComplete(completeStatus, !this.hasErrors);
    }
  }

  getActionType = (item: any) => {
    const { method, url } = item;
    switch (method) {
      case 'POST':
        return url.includes('target') ? AuditActionType.TARGET_ADD : AuditActionType.CASE_ADD;
      case 'PUT':
        return url.includes('target')
          ? AuditActionType.TARGET_EDIT
          : url.includes('case') ? AuditActionType.CASE_EDIT : AuditActionType.WARRANT_EDIT;
      case 'DELETE':
        return url.includes('target') ? AuditActionType.TARGET_DELETE : AuditActionType.CASE_DELETE;
      default:
        return '';
    }
  }

  getFieldData = (item: any) => {
    const { url, data } = item;
    const fieldData = cloneDeep(data);
    if (url.includes('target')) {
      // do nothing
    } else if (url.includes('case')) {
      delete fieldData.target; // Case edit does not require target info
    } else {
      delete fieldData.cases;
    }
    return fieldData;
  }

  getBeforeData = (item : any, casesBeforeFields: any) => {
    if (item.url.includes('target')) {
      const caseId = item.url.split('/')?.[5]; // On edit warrant caseId is stored at this location on the url
      const caseDetails = casesBeforeFields.filter((caseInfo : any) => caseInfo.id === caseId)?.[0];
      const targetDetails = caseDetails?.targets.filter((target: any) => target.id === item.id)?.[0];
      return targetDetails;
    }
    const caseDetails = casesBeforeFields.filter((caseInfo: any) => caseInfo.id === item.id)?.[0];
    const caseBefore = cloneDeep(caseDetails);
    caseBefore.targets = [];
    return caseBefore;
  }

  resetSubmit(isOpen = false): void {
    this.setState({
      isOpen,
      progress: [],
      msgs: [],
    });

    this.hasErrors = false;
  }

  updateWarrant = async (): Promise<void> => {
    let {
      formData,
      userAudit,
    } = this.props;

    const {
      changeTracker: tracker,
      warrantBeforeFieldValues,
      caseBeforeFieldValues,
      auditEnabled,
      onNoChangeToUpdate,
      onProgressStart,
      onProgressUpdate,
      onProgressEnd,
      history,
    } = this.props;

    // Convert date/time fields to the warrant timezone
    formData = WarrantAdapter.adaptWarrantToSubmit(formData);
    if (userAudit) {
      const { timeZone } = formData;
      userAudit = WarrantAdapter.adaptAuditToSubmit(userAudit, timeZone);
    }

    const actions = tracker.getTasks(formData);

    if (actions.length === 0) {
      if (onNoChangeToUpdate) {
        onNoChangeToUpdate();
      } else {
        history.replace('/warrants');
      }

      this.showMessage('No changes to update');
      return;
    }

    if (onProgressStart) {
      onProgressStart(actions.length);
    }

    for (let i = 0; i < actions.length; i += 1) {
      const item = actions[i];
      const taskDescription = tracker.describeTask(item, formData);
      const actionType = this.getActionType(item);
      const isActionEdit = (item.method === 'PUT');
      const fieldData = isActionEdit ? this.getFieldData(item) : cloneDeep(item.data);
      const beforeFieldData = isActionEdit
        ? item.url.includes('case') || item.url.includes('target')
          ? this.getBeforeData(item, caseBeforeFieldValues)
          : warrantBeforeFieldValues
        : null;

      const fieldInfo = getAuditFieldInfo(fieldData, userAudit, isActionEdit, beforeFieldData);
      if (item.url.includes('target')) {
        fieldInfo.forEach((field: any) => {
          if (maskKeys.includes(field.field)) {
            if (field.before) field.before = maskedValue;
            if (field.after) field.after = maskedValue;
            if (field.value) field.value = maskedValue;
          }
        });
      }
      const auditDetails = auditEnabled
        ? this.getAllAuditDetails(
          item.displayName,
          AuditService.WARRANTS,
          actionType,
          fieldInfo,
        )
        : null;

      try {
        if (item.status !== 200) {
          this.replaceOldIds(item);

          // progress bar for UI
          const progressList = cloneDeep(this.state.progress);
          progressList.push({ id: item.id, text: taskDescription, loading: true });

          if (onProgressUpdate != null) {
            onProgressUpdate(progressList);
          }

          item.loading = true;
          this.setState({ progress: progressList });
          // Submit to the server
          const res = await editWarrant(item.method, item.url, { data: item.data, auditDetails });
          item.Status = res?.status;
          this.updateProgressList(item);
          tracker.completeTask(item);

          // Item created, put newly created Id in tracker
          if (item.method === 'POST' && !item.url.includes('target') && res?.status === 200) {
            const oldCaseId = item.id;
            tracker.addNewCasesWithId(oldCaseId, res.data.metadata.idCreated);
          }

          // Target created, update target id in tracker to the newly created target id
          if (item.method === 'POST' && item.url.includes('target') && res?.status === 200) {
            tracker.addNewTargetWithId(item.caseId, item.id, res.data.metadata.idCreated);
          }
        }
      } catch (error) {
        this.hasErrors = true;
        item.Status = 'FAILED';
        this.updateProgressList(item, true);
        let message = '';
        try {
          const errorFieldName = JSON.parse(error.response.config.data).name;
          const errorCode = error.response.data.error.code;
          const errorMessage = error.response.data.error.message;
          const curError = { errorFieldName, errorCode, errorMessage };
          message = convertErrorCodeToMessage(curError);
        } catch (e) {
          message = error.message;
        }

        const msgList = [`${taskDescription} - FAILED`];
        msgList.push(`Error: ${message}`);
        this.setState({
          msgs: msgList,
        });

        if (onProgressEnd) {
          onProgressEnd(msgList, true);
        }

        break;
      }
    }
    if (!this.hasErrors) {
      if (this.props.mode === 'create' && !this.props.isDialog) {
        showSuccessMessage('Warrant updated successfully.', 5000);
        this.props.history.replace('/warrants');
      } else {
        const msgList = ['Warrant updated successfully.'];
        this.setState({ msgs: msgList });

        if (onProgressEnd) {
          onProgressEnd(msgList);
        }
      }
    }
  }

  updateProgressList = (item: any, failed = false) => {
    let updateProgressList = this.state.progress;
    updateProgressList = updateProgressList.map((element: any) => {
      if (element.id === item.id) {
        element.loading = false;
        element.failed = failed;
      }
      return element;
    });
    this.setState({ progress: updateProgressList });
  }

  showMessage = (message: string) => {
    AppToaster.clear();
    AppToaster.show({
      icon: 'tick',
      intent: 'primary',
      timeout: 2000,
      message: `${message}`,
    });
  }

  closeDialog = () => {
    this.setState({ isOpen: false });
    if (!this.hasErrors) {
      this.props.history.replace('/warrants');
    }
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  render() {
    const {
      hideProgress,
    } = this.props;

    if (hideProgress) {
      return null;
    }

    const content = (
      <div className="config-form-body" style={{ marginBottom: '20px' }}>
        <div style={{ border: '1px', paddingTop: '20px' }}>
          {this.state.progress
            ? <ShowProgress progress={this.state.progress} mode={this.props.mode} isDialog={this.props.isDialog} />
            : <Fragment />}
        </div>
        <div style={{ border: '1px', paddingTop: '20px' }}>
          {this.state.msgs
            ? (
              <ShowStatusMessage
                hasErrors={this.hasErrors}
                msgs={this.state.msgs}
                mode={this.props.mode}
                isDialog={this.props.isDialog}
              />
            )
            : <Fragment />}
        </div>
      </div>
    );

    if (this.props.isDialog) {
      return (
        <Dialog
          title={this.props.mode === 'create' ? 'Create Warrant' : 'Update Warrant'}
          isOpen={this.state.isOpen}
          canEscapeKeyClose={false}
          canOutsideClickClose={false}
          isCloseButtonShown={false}
          style={{
            maxHeight: '600px',
            width: 'auto',
            minWidth: '400px',
            maxWidth: '750px',
          }}
        >
          {content}
          <div className={Classes.DIALOG_FOOTER_ACTIONS} style={{ paddingRight: '20px', justifyContent: 'center' }}>
            <Button className="actionButton" onClick={this.closeDialog} intent="primary" active text="OK" />
          </div>
        </Dialog>
      );
    }
    return (
      <div>
        {(content)}
      </div>
    );
  }
}

const mapStateToProps = ({
  warrants: { warrant },
  global: { selectedDF },
  cases: { cases },
  warrants: {
    changeTracker,
  },
}: any) => ({
  warrant,
  cases,
  changeTracker,
  selectedDF,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(EditWarrantSubmit);
