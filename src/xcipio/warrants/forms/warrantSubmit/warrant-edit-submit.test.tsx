import { shallow } from 'enzyme';
import React from 'react';
import {
  warrantAddAfterMaskData,
  warrantAddBeforeMaskData,
} from './__test__/warrantEdit-test-data';
import { EditWarrantSubmit } from './warrant-edit-submit';

/**
 * test mask target data
 */
describe('when adding a new warrant the target info is masked in audit', () => {
  const wrapper: any = shallow(<EditWarrantSubmit />);

  test('target info is masked in warrant create audit', () => {
    const formData = warrantAddBeforeMaskData;
    const afterValue = wrapper.instance().maskTargetValues(formData);
    expect(afterValue).toEqual(warrantAddAfterMaskData);
  });
});
