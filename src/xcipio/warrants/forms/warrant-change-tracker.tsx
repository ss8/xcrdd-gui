import cloneDeep from 'lodash.clonedeep';

/*
 * NOTE:
 * Keys added to the tracker should be one of the following forms:
 * - 'warrant'
 * - 'warrant.attachments.{idA}
 * - 'case.{idC}'
 * - 'case.{idC}.target.{idT}'
 * For existing cases and targets, use the value of the field "id" for the
 * idC or idT values.  For newly added cases and targets, use the array
 * offset (0-based) as the index.  For example, when adding the third case,
 * use idC = 2.  (Just for uniformity - not enforced)  Or, better still,
 * use the return value from addAdded( ).
 * ADDENDUM:
 * We want to track all changes to each field now.  A field is presented as
 * one of the following keys:
 * - 'warrant.{field}'
 * - 'case.{idC}.{field}'
 * - 'case.{idC}.target.{idT}.{field}'
 * The object-tracking key is derived from the field key.  The object key is
 * tracked separately from the field key.  Fields track changes to its value
 * for auditing purposes.
 */

interface ChangeTrackerProps {
   idDF: string;
   idWarrant?: string|null;
}

export default class ChangeTracker {
    private added:Set<string>;

    private deleted:Map<string, string>;

    private updated:Set<string>;

    private fields:Map<string, {action: string, value: string|null|undefined
      original: string|null|undefined, label:string|null|undefined }>;

    private idDF: string;

    private idWarrant: string|null|undefined;

    private idNext = 0;

    private IdNameMap:Map<string, string>;

    private newCaseIds:Map<string, string>;

    private newTargetIds : Map<string, string> = new Map();

    private hI3Interfaces : any = new Set();

    private addedCases:Set<string>;

    private METHOD_MAP: any = {
      GET: 'Read', POST: 'Add', PUT: 'Update', DELETE: 'Delete',
    };

    constructor(props: ChangeTrackerProps) {
      this.added = new Set();
      this.deleted = new Map();
      this.updated = new Set();
      this.fields = new Map();
      this.idDF = props.idDF;
      this.idWarrant = props.idWarrant;
      this.idNext = 0;
      this.IdNameMap = new Map();
      this.newCaseIds = new Map();
      this.addedCases = new Set();
    }

    setWarrantId = (idWarrant:any) => { this.idWarrant = idWarrant; }

    getWarrantId = () => (this.idWarrant);

    getObjectKey = (key:string) => {
      // Split this into parts and return the object's key from the initial parts
      let objKey = null; // key not understood
      const parts = key.split('.');

      if (parts[0] === 'warrant') {
        if ((parts.length >= 2) && (parts[1] === 'attachment')) {
          // This is an attachment (with a possible attachment ID)
          objKey = (`${parts[0]}.${parts[1]}${(parts.length >= 3) ? (`.${parts[2]}`) : ''}`);
        } else {
          // This is a warrant field (with a possible warrant ID)
          objKey = (parts[0] + ((parts.length >= 2) ? (`.${parts[1]}`) : ''));
        }
      } else if (parts[0] === 'case') {
        // Determine whether this is a case field or a target field
        if ((parts.length >= 3) && (parts[2] === 'target')) {
          // This is a case+target field
          objKey = (`${parts[0]}.${parts[1]}.${parts[2]}${(parts.length >= 4) ? (`.${parts[3]}`) : ''}`);
        } else {
          // This is a case field (with a possible case ID)
          objKey = (parts[0] + ((parts.length >= 2) ? (`.${parts[1]}`) : ''));
        }
      }

      return (objKey);
    }

    getFieldName = (prefix:string, field:string) => {
      // Return the field name relative to the prefix (eg: for warrant.contact, return contact)
      let name = field;
      if (field.startsWith(prefix)) {
        name = field.substring(prefix.length + 1); // skip the prefix and the dot
      }
      return (name);
    }

    insert = (originalSet: any, value: any) => {
      // Add the indicated item to the front of the set
      const result = new Set([value]);
      originalSet.forEach((item: any) => {
        result.add(item);
      });
      return (result);
    }

    insertMap = (originalMap: any, key: any, value: any) => {
      const result = new Map();
      result.set(key, value);
      originalMap.forEach((item: any) => {
        if (!result.has(key)) {
          result.set(item.key, item.value);
        }
      });
      return result;
    }

    setIdName = (id: any, name: any) => {
      this.IdNameMap.set(id, name);
    }

    nextId = () => {
      // Generate a new ID using a sequential number and a constant string
      const id = (`NEWID+${this.idNext}`);
      this.idNext += 1;
      return (id);
    }

    isChanged = () => {
      // Determine whether there are any changes recorded as yet
      return ((this.added.size > 0) || (this.deleted.size > 0) || (this.updated.size > 0));
    }

    addAdded = (fieldKey: string, newValue:string|null = null, label:any|null = null) => {
      // Keep track of this added item - allocate an ID as needed

      // Check whether a "new" id is provided and create one if not
      let id = null;
      let key = this.getObjectKey(fieldKey);

      if ((key != null) && (key.length === fieldKey.length)) {
        const parts = key.split('.');
        // Creating a new object (eg: case or target), so track the addition
        if (key === 'warrant') {
          this.idWarrant = this.nextId();
          id = this.idWarrant;
        } else if (key === 'warrant.attachment') {
          id = this.nextId();
          key += (`.${id}`);
        } else if (parts.length % 2 !== 0) {
          // Odd number of parts ==> created ID is missing
          id = this.nextId();
          key += (`.${id}`);
        } else {
          // even number of parts ==> ID is the last element
          id = parts[parts.length - 1];
        }
        // It is safe to assume that this is not part of the other sets
        if ((key.indexOf('.target.') > 0) || (key.indexOf('.attachment') > 0)) {
          this.added.add(key); // append
        } else /* if (key !== 'warrant') */ {
          this.added = this.insert(this.added, key); // prepend
        }

        if (!key.includes('.target.') && !key.includes('.attachment')) {
          if (!this.addedCases.has(id)) { this.addedCases.add(id); }
        }
      } else if (key != null) {
        // The object (should) already exist, so track an update
        this.addUpdated(key, null, null);
      } else {
        // eslint-disable-next-line no-console
        console.error(`fieldKey '${fieldKey}' produced null key`);
      }

      if ((newValue != null) && ((key == null) || (key.length < fieldKey.length))) {
        // We need to track this field added
        const fieldTrack = {
          action: 'add', value: newValue, original: null, label,
        };
        this.fields.set(fieldKey, fieldTrack); // always replaced
      }

      return (id);
    }

    addDeleted = (fieldKey: any, itemName: string, label: string|null = null) => {
      // Add this item to the deleted list, dropping it from the updated and added lists.
      const key = this.getObjectKey(fieldKey);

      if (key !== null) {
        if (key.length === fieldKey.length) {
          // The whole object was deleted
          if (this.added.has(key)) {
            this.added.delete(key); // pretend it never happened
          } else {
            if (key.indexOf('.target.') > 0) {
              this.deleted = this.insertMap(this.deleted, key, itemName); // prepend
            } else { this.deleted.set(key, itemName); } // append
            this.updated.delete(key); // pretend it never happened
          }
          // Delete any field-tracking records impacted by deleting this object
          const deleted:string[] = [];
          // push the field.key into deleted if field.key starts with key
          this.fields.forEach((_value, akey, _map) => {
            if (akey.startsWith(key)) deleted.push(akey); // cannot delete while looping
          });

          deleted.forEach((field:string) => {
            this.fields.delete(field);
          });
        } else {
          // The object (should) already exist, so track an update
          this.addUpdated(key, null, null);
        }
      }

      if ((key == null) || (key.length < fieldKey.length)) {
        // We need to track this field deleted
        const fieldTrack = {
          action: 'delete', value: itemName, original: null, label,
        };
        this.fields.set(fieldKey, fieldTrack); // always replaced
      }
    }

    addUpdated = (fieldKey:string, newValue:any,
      label:any|null|undefined = null, oldValue:any = null) => {
      // Only add an update for this item if it is not already being added
      const key = this.getObjectKey(fieldKey);
      if (key != null) {
        // Any change to a field of this object triggers a change to the object
        if (this.added.has(key) === false) {
          this.updated.add(key);
        }
      }

      // Handle tracking for this updated field
      if ((key == null) || (key.length < fieldKey.length)) {
        if (this.fields.has(fieldKey)) {
          // Field key already being tracked, so just replace its value
          const fieldTrack = this.fields.get(fieldKey);
          if (fieldTrack) {
            fieldTrack.value = newValue;
            this.fields.set(fieldKey, fieldTrack);
          }
        } else {
          // New field key, so add it to our tracking
          const fieldTrack = {
            action: 'update', value: newValue, original: oldValue, label,
          };
          this.fields.set(fieldKey, fieldTrack);
        }
      }
    }

    deleteDependentActions(fieldKey: string): void {
      const key = this.getObjectKey(fieldKey);

      if (key == null) {
        return;
      }

      Array.from(this.added).forEach((addedKey) => {
        if (addedKey.startsWith(key) && addedKey !== key) {
          this.added.delete(addedKey);
        }
      });

      Array.from(this.updated).forEach((updatedKey) => {
        if (updatedKey.startsWith(key) && updatedKey !== key) {
          this.updated.delete(updatedKey);
        }
      });

      Array.from(this.deleted).forEach(([deletedKey]) => {
        if (deletedKey.startsWith(key) && deletedKey !== key) {
          this.deleted.delete(deletedKey);
        }
      });
    }

    toAction = (action: any, ID: any, formData: any = undefined, itemName = '') => {
      /* Split the ID into smaller components to determine what work needs to be done.
      Use the IDs to populate the service URL.  Then, determine the HTTP METHOD based
      on the action.  Return an object with the method and the URL. */

      const parts = ID.split('.');
      const urlWarrant = (`/warrants/${this.idDF}`);
      let url = (`${urlWarrant}/${this.idWarrant}`);
      let objectId:string|null|undefined = '';
      let data: any;
      let caseId;
      let name;

      if ((parts.length >= 2) && (parts[1] === 'attachment')) {
        if (action === 'add') {
          // This is an attachment added to the warrant
          url += '/attachments';
          if (parts.length >= 3) [, , objectId] = parts;
          if (formData && formData.attachments) {
            // get attachment that has the same id
            const attachments:any[] = Object.values(formData.attachments).filter(
              (attachment:any) => {
                return (attachment.id === objectId);
              },
            );
            // there should only be one attachment matching the id
            if (attachments && attachments.length >= 1) {
              [data] = attachments;
            }
          }
          name = data ? data.name : objectId;
        } else if (action === 'delete') {
          // This is a DELETE to an attachment
          url += (`/attachments/${parts[2]}`);
          [, , objectId] = parts;
        }
      } else if (parts[0] === 'warrant') {
        if (!objectId) objectId = this.idWarrant;
        data = formData ? this.getWarrantData(formData) : undefined;
        name = data ? data.name : objectId;
        if (action === 'add') { // Add a warrant
          // This is a brand-new warrant, so POST to /warrants
          // need to keep whole warrant formData.
          url = urlWarrant;
          data = formData || undefined;
        }
      } else if (parts[0] === 'case') {
        // Add the cases URL component(s)
        url += '/cases';
        if (parts.length > 1) {
          [, objectId] = parts;
          data = formData ? this.getWarrantCaseData(formData, objectId) : undefined;
          name = data ? data.liId : objectId;
          if (parts.length > 2) {
            // This is handling targets on a specific case
            url += (`/${parts[1]}/targets`);
            caseId = objectId; // CaseId that is stored above
            objectId = ((parts.length > 3) ? parts[3] : this.nextId());
            data = formData ? this.getWarrantCaseTargetData(formData, caseId, objectId) : undefined;
            name = data ? data.primaryType : objectId;
          }
          if (action !== 'add') url += (`/${objectId}`);
        } else {
          // We should only get here for a case-add
          if (action !== 'add') return (null);
          objectId = this.nextId();
          data = formData ? this.getWarrantCaseData(formData, objectId) : undefined;
        }
      }

      let method = 'PUT';
      switch (action) {
        case 'delete':
          method = 'DELETE';
          name = itemName;
          break;
        case 'add':
          method = 'POST';
          break;
        default:
          break;
      }

      const result = {
        method, url, id: objectId, data, caseId, key: ID, displayName: name,
      };
      return (result);
    }

    summarizeField = (group:string, field:string) => {
      // Create a summary record for this field change
      const fieldTrack = this.fields.get(field);
      const summary = {
        group,
        field: this.getFieldName(group, field),
        original: (fieldTrack) ? fieldTrack.original : null,
        value: (fieldTrack) ? fieldTrack.value : null,
        action: (fieldTrack) ? fieldTrack.action : null,
        label: (fieldTrack) ? fieldTrack.label : null,
      };
      return (summary);
    }

    getSummary = () => {
      // Return the list of changes being made to the warrant, cases, and targets
      const results:any = [];

      // First, organize the changes by key
      const changes:any = { };
      this.fields.forEach((_value, aKey, _map) => {
        let key:string|null = this.getObjectKey(aKey);
        if (key == null) key = 'other';
        if (!changes[key]) changes[key] = [];
        changes[key].push(aKey);
      });
      // Now, add each of the warrant fields before the others
      if (changes.warrant) {
        changes.warrant.forEach((field:any) => {
          results.push(this.summarizeField('warrant', field));
        });
      }
      // Then, add the remaining fields
      const orderedChanges = Object.keys(changes).sort();
      orderedChanges.forEach((group:any) => {
        if (group !== 'warrant') {
          changes[group].forEach((field:any) => {
            results.push(this.summarizeField(group, field));
          });
        }
      });

      return (results);
    }

    getWarrantData = (formData: any) => {
      const warrantData = cloneDeep(formData);
      if (warrantData.cases) {
        warrantData.cases = [];
      }
      if (warrantData.attachments) {
        warrantData.attachments = [];
      }
      return warrantData;
    }

    getWarrantCaseData = (formData: any, caseId: any) => {
      const caseDetails = cloneDeep(formData.cases.find((item: any) => item.id === caseId));
      if (caseDetails && caseDetails.targets) {
        caseDetails.targets = [];
      }
      return caseDetails;
    }

    getWarrantCaseTargetData = (formData: any, caseId: any, targetId: any) => {
      const caseDetails = formData.cases.find((item: any) => item.id === caseId);
      const targetDetails = caseDetails ?
        cloneDeep(caseDetails.targets.find((item: any) => item.id === targetId)) : [];
      return targetDetails;
    }

    getTasks = (formData: any = '') => {
      // Return the list of tasks recarded so far, ordered as deleted, updated, added
      const tasks: any = [];

      if (this.added.has('warrant')) {
        // The POST for create warrant consists of just one self-contained request
        tasks.push(this.toAction('add', 'warrant', formData));
        // Followed by the attachment POSTS'
        this.added.forEach((item:any) => {
          if (item.startsWith('warrant.attachment')) {
            tasks.push(this.toAction('add', item, formData));
          }
        });
      } else {
        this.deleted.forEach((value: any, key: any) => {
          tasks.push(this.toAction('delete', key, undefined, value));
        });

        // Sending warrant update ahead of all other updates
        const toAddTasks: any = [];
        this.updated.forEach((item: any) => {
          const action = this.toAction('update', item, formData);
          if (action && action.key.includes('warrant')) {
            tasks.push(action);
          } else if (action) {
            toAddTasks.push(action);
          }
        });

        toAddTasks.forEach((item: any) => {
          tasks.push(item);
        });

        this.added.forEach((item: any) =>
          tasks.push(this.toAction('add', item, formData)));
      }
      return (tasks);
    }

    describeId = (id:string) => {
      // Convert the field ID into a human-readable description of the object
      const parts = id.split('.');
      let container = '';
      let object = 'warrant';
      let objectId = null;
      if ((parts.length >= 3) && (parts[2] === 'target')) {
        let caseID:string|undefined = parts[1];
        if (this.IdNameMap.has(caseID)) caseID = this.IdNameMap.get(caseID);
        object = 'target';
        container = (`case ${caseID}`);
        if (parts.length >= 4) [, , objectId] = parts;
      } else if (parts[0] === 'case') {
        object = 'case';
        container = 'warrant';
        if (parts.length >= 2) [, objectId] = parts;
      } else if ((parts.length >= 2) && (parts[0] === 'warrant') && (parts[1] === 'attachment')) {
        object = 'attachment';
        container = 'warrant';
        if (parts.length >= 3) [, objectId] = parts;
      }
      let desc;
      if (object === 'warrant') {
        desc = object;
      } else {
        if (!objectId) objectId = '(new)';
        const name = (this.IdNameMap.has(objectId) ? this.IdNameMap.get(objectId) : objectId);
        desc = (`${object} "${name}"`);
        if (container) { desc += (` for ${container}`); }
      }

      return (desc);
    }

    describeTask = (task: any, formData: any = '') => {
      // Convert the task record into a human-readable description of the task
      const action = this.METHOD_MAP[task.method];
      const parts = task.url.split('/');
      let container = '';
      let object = 'warrant';

      if (task.url.indexOf('targets') > 0) {
        let caseID = parts[parts.length - ((task.method === 'POST') ? 2 : 3)];
        const caseName = this.getCaseNameById(caseID, formData);
        if (this.IdNameMap.has(caseID)) caseID = this.IdNameMap.get(caseID);
        object = 'target';
        container = (`case ${caseName}`);
      } else if (task.url.indexOf('cases') > 0) {
        object = 'case';
        container = 'warrant';
      } else if (task.url.indexOf('attachments') > 0) {
        object = 'attachment';
        container = 'warrant';
      }

      let name = task.displayName;
      if (this.IdNameMap.has(task.id)) name = this.IdNameMap.get(task.id);
      let desc = name && name.length > 0 ? (`${action} ${object} '${name}'`) : (`${action} ${object}`);
      if (container) desc += (` for ${container}`);

      return (desc);
    }

    getCaseNameById =(caseId: any, formData: any) => {
      const caseDetails = this.getWarrantCaseData(formData, caseId);
      return formData && caseDetails ? caseDetails.liId : caseId;
    }

    join = (collection: any, delim: any) => {
      let result = '';
      collection.forEach((item: any) => {
        if (result) result += delim;
        result += item;
      });
      return result;
    }

    showTasks = () => {
      const taskList = this.getTasks();
      const tasks: any = [];
      taskList.forEach((task: any) => {
        tasks.push(`${task.method}: ${task.url}   = ${this.describeTask(task)}`);
      });
      return (this.join(tasks, '\n'));
    }

    addNewCasesWithId = (oldCaseId: any, newCaseId: any) => {
      this.newCaseIds.set(oldCaseId, newCaseId);
    }

    getNewCaseIdByOldCaseId = (caseId:any) => {
      return this.newCaseIds.has(caseId)
        ? this.newCaseIds.get(caseId) : undefined;
    }

    addNewTargetWithId = (caseId: string, oldTargetId: string, newTargetId: string) => {
      this.newTargetIds.set(`${caseId}+${oldTargetId}`, newTargetId);
    }

    getNewTargetIdByOldTargetId = (caseId: string, oldTargetId: string) => {
      return this.newTargetIds.has(`${caseId}+${oldTargetId}`)
        ? this.newTargetIds.get(`${caseId}+${oldTargetId}`) : undefined;
    }

    addHI3Interface = (caseId: any) => {
      if (!this.hI3Interfaces.has(caseId)) this.hI3Interfaces.add(caseId);
    }

    hasUpdatedHI3 = (caseId: any) => {
      return this.hI3Interfaces.has(caseId);
    }

    isNewCase = (caseId: any) => {
      return this.addedCases.has(caseId) || caseId === undefined;
    }

    completeTask = (task: any) => {
      let isOK = false;
      // Determine which list this item comes from and remove it (using the key)
      switch (task.method) {
        case 'DELETE':
          isOK = this.deleted.delete(task.key);
          break;
        case 'PUT':
          isOK = this.updated.delete(task.key);
          break;
        case 'POST':
          if (this.addedCases.has(task.key)) {
            this.addedCases.delete(task.key);
          }
          isOK = this.added.delete(task.key);
          if (isOK && (task.key === 'warrant')) {
            // Once the warrant has POSTed, there's nothing left to do
            this.deleted.clear();
            this.updated.clear();
            // Drop everything added and put back the attachments
            const attachments:any[] = [];
            this.added.forEach((item:any) => {
              if (item === 'warrant.attachment') {
                attachments.push(item);
              }
            });
            this.added.clear();
            attachments.forEach((item:any) => {
              this.added.add(item);
            });
          }
          break;
        default:
          break;
      }
      return (isOK);
    }

    completeTasks = (nDone:number) => {
      const taskList = this.getTasks();
      for (let iTask = 0; (iTask < nDone && (iTask < taskList.length)); iTask += 1) {
        this.completeTask(taskList[iTask]);
      }
    }

    showSummary = () => {
      const result:any[] = [];
      const summary = this.getSummary();
      summary.forEach((field:any) => {
        const idDesc = this.describeId(field.group);
        const label = (field.label ? ` ( '${field.label}' )` : '');
        result.push(`${field.action} ${idDesc} [${field.group}].${field.field} ${label}: new='${field.value}' original='${field.original}'`);
      });
      return (result.join('\n'));
    }
}
