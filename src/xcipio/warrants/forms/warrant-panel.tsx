import React, { Fragment } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import {
  Label, HTMLSelect, Classes, Dialog, Button,
} from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { Warrant, WARRANT_LEA_ANY } from 'data/warrant/warrant.types';
import { FormTemplate, FormTemplateSection, Template } from 'data/template/template.types';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { CaseModel, CaseOptions as CaseOptionsType } from 'data/case/case.types';
import { adaptTemplate } from 'routes/Warrant/utils/warrantTemplate.adapter';
import { RetrievingDataProgress } from 'components';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { updateHI2Interfaces, updateHI3Interfaces } from 'xcipio/devices/forms/devices-to-cases';
import { TEMPORARY_CASE_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import { CasePanel } from 'xcipio/cases/forms/case-panel';
import * as globalSelectors from 'global/global-selectors';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as caseActions from 'xcipio/cases/case-actions';
import * as caseSelectors from 'data/case/case.selectors';
import * as collectionFunctionSelectors from 'data/collectionFunction/collectionFunction.selectors';
import * as collectionFunctionActions from 'data/collectionFunction/collectionFunction.actions';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as templateSelectors from 'data/template/template.selectors';
import * as contactSelectors from 'xcipio/contacts/contacts-selectors';
import * as userSelectors from 'users/users-selectors';
import { createWarrantBasedOnTemplate } from 'routes/Warrant/utils/warrantTemplate.factory';
import WarrantDetails from './warrant-details';
import Templates from '../../templates/warrant-template';
import {
  checkGivenTimeWithCurrentTime,
  areTimesEqual,
  checkTimeValidationInComputerTimezone,
} from '../../../shared/constants/checkTimeValidation';
import CaseOptions from '../../cases/options/case-options';
import CaseCreateOptions from '../../cases/options/case-options.json';
import { ChangedField } from '../../../shared/form';
import ManageAttachments from '../../attachments/index';
import UserAudit from '../../../shared/userAudit';
import { trimJsonObj } from '../../../shared/dataUtils/json-utils';
import './warrant-create.scss';

const fieldsPassedToCases = ['startDateTime', 'stopDateTime', 'timeZone', 'dxOwner'];

export const getCaseOptionTemplateValue = (templates: any, selectedTemplateId: string) => {
  let result = templates.filter((template: any) => template.id === selectedTemplateId)?.[0];
  if (!result) {
    [result] = CaseCreateOptions;
  }
  const selectCaseOptionTemplate = JSON.parse((result.template));
  const fields:any = { };
  Object.keys(selectCaseOptionTemplate.fields).forEach((field: string) => {
    fields[field] = selectCaseOptionTemplate.fields[field].initial;
  });
  return fields;
};

const mapState = (state: RootState) => ({
  templates: templateSelectors.getRawTemplates(state),
  warrant: warrantSelectors.getWarrant(state),
  cases: caseSelectors.getCases(state),
  selectedDF: globalSelectors.getSelectedDeliveryFunctionId(state),
  changeTracker: warrantSelectors.getChangeTrackerInstance(state),
  warrantConfigInstance: warrantSelectors.getWarrantConfigurationInstance(state),
  userAudit: warrantSelectors.getWarrantUserAudit(state),
  timezones: userSelectors.getTimezones(state),
  teams: userSelectors.getTeams(state),
  contacts: contactSelectors.getContacts(state),
  cfGroups: collectionFunctionSelectors.getCollectionFunctionGroupList(state),
  cfs: collectionFunctionSelectors.getCollectionFunctionList(state),
  attachmentEnabled: discoverySelectors.isAttachmentEnabled(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  selectAllHI3Interfaces: discoverySelectors.shouldSelectAllHi3Interfaces(state),
  isWarrantDefaultExpireEnabled: discoverySelectors.isWarrantDefaultExpireEnabled(state),
  warrantDefaultExpireDays: discoverySelectors.getWarrantDefaultExpireDays(state),
  targetDefaultAFSelection: warrantSelectors.getTargetDefaultAFSelection(state),
});

const mapDispatch = {
  updateWarrantState: warrantActions.updateWarrantState,
  updateCasesState: caseActions.updateCasesState,
  clearWarrantState: warrantActions.clearWarrantState,
  updateWarrantAuditState: warrantActions.updateWarrantAuditState,
  updateFormTemplateUsed: warrantActions.updateFormTemplateUsed,
  clearCasesState: caseActions.clearCasesState,
  fetchCollectionFunctionListByIds: collectionFunctionActions.fetchCollectionFunctionListByIds,
  fetchCollectionFunctionList: collectionFunctionActions.fetchCollectionFunctionList,
};

const connector = connect(mapState, mapDispatch, null, { forwardRef: true });

type PropsFromRedux = ConnectedProps<typeof connector>;

export type WarrantPanelProps = PropsFromRedux & {
  mode: string
  hasConfiguredDevices: () => boolean
  warrantId?: string | null
};

export type WarrantPanelState = {
  formTemplate: Template,
  warrant: any,
  originalWarrant: Warrant | null,
  shouldWarrantDetailUpdate: boolean,
  isManageAttachmentVisible: boolean,
  isCaseOptionsVisible: boolean,
  isLEAChangeConfirmOpen: boolean,
  newLeaValue: string,
  isRetrievingCfsServices: boolean
}

type AssignableWarrantFields = 'startDateTime' | 'stopDateTime' | 'timeZone' | 'caseType' | 'dxOwner' | 'caseOptions' | 'caseOptTemplateUsed';
type AssignableCaseFields = 'startDateTime' | 'stopDateTime' | 'timeZone' | 'type' | 'dxOwner' | 'options' | 'templateUsed';

const WARRANT_TO_CASE_FIELD_NAME: { [key: string]: AssignableCaseFields } = {
  startDateTime: 'startDateTime',
  stopDateTime: 'stopDateTime',
  timeZone: 'timeZone',
  caseType: 'type',
  dxOwner: 'dxOwner',
  caseOptions: 'options',
  caseOptTemplateUsed: 'templateUsed',
};

type UpdateExistingCaseType = {
  name: AssignableWarrantFields
  value: string | CaseOptionsType
}[]

export class WarrantPanel extends React.Component<WarrantPanelProps, WarrantPanelState> {
  warrantDetailRef: WarrantDetails | null = null
  auditRef: any
  templates: Templates
  shouldWarrantUpdate = true;

  constructor(props: WarrantPanelProps) {
    super(props);
    this.templates = new Templates(props.templates);

    const {
      warrantId,
      mode,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      changeTracker,
      updateFormTemplateUsed,
    } = this.props;

    let { warrant } = this.props;

    let formTemplate = this.templates.getById(warrant?.templateUsed ?? '', Templates.CATEGORY_WARRANT_WIZARD);
    formTemplate = this.getFormTemplate(formTemplate);

    if (warrant == null) {
      warrant = createWarrantBasedOnTemplate(formTemplate.template);
    }

    if (warrant.templateUsed == null || warrant.templateUsed === '') {
      // Set the template used if it is not already set
      warrant.templateUsed = formTemplate.id;
    }

    updateFormTemplateUsed(formTemplate);

    warrant = WarrantAdapter.adaptWarrantToForm(
      warrant,
      mode,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      changeTracker,
      warrantId,
    );

    if (mode === CONFIG_FORM_MODE.Edit || mode === CONFIG_FORM_MODE.View) {
      this.configureForEditMode(warrant);
    }

    const originalWarrant = cloneDeep(warrant);

    this.state = {
      warrant,
      originalWarrant,
      formTemplate,
      shouldWarrantDetailUpdate: true,
      isCaseOptionsVisible: false,
      isManageAttachmentVisible: false,
      isLEAChangeConfirmOpen: false,
      newLeaValue: '',
      isRetrievingCfsServices: false,
    };
  }

  componentDidUpdate(prevProps: WarrantPanelProps): void {
    const { mode, warrant } = this.props;

    if (WarrantPanel.shouldUpdateWarrantForm(mode, warrant, prevProps?.warrant)) {
      this.updateWarrantInformationForm();
    }
  }

  updateWarrantInformationForm = (): void => {
    const {
      warrantId,
      mode,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      changeTracker,
    } = this.props;

    let { warrant } = this.props;

    const {
      formTemplate,
    } = this.state;

    warrant = WarrantAdapter.adaptWarrantToForm(
      warrant,
      mode,
      isWarrantDefaultExpireEnabled,
      warrantDefaultExpireDays,
      changeTracker,
      warrantId,
    );

    this.configureForEditMode(warrant);

    WarrantPanel.updateKey({ ...formTemplate });

    this.setState({
      shouldWarrantDetailUpdate: true,
      formTemplate,
      warrant,
    });
  }

  static shouldUpdateWarrantForm(
    mode:string,
    newWarrantState:Warrant | null,
    currentWarrantState:Warrant | null,
  ):boolean {
    if (!newWarrantState || !currentWarrantState) {
      return false;
    }

    const isViveMode = mode === CONFIG_FORM_MODE.View;

    const hasStatusChanged = newWarrantState?.state !== currentWarrantState?.state;
    const hasScheduleChanged =
      (newWarrantState?.startDateTime !== currentWarrantState?.startDateTime) ||
      (newWarrantState?.stopDateTime !== currentWarrantState?.stopDateTime);

    return isViveMode && (hasStatusChanged || hasScheduleChanged);
  }

  getFormTemplate(formTemplate: Template): Template {
    const {
      warrant,
      mode,
      templates,
      attachmentEnabled,
      timezones,
      teams,
      contacts,
      cfGroups,
    } = this.props;

    const adaptedFormTemplate = adaptTemplate(
      formTemplate,
      mode,
      warrant,
      templates,
      attachmentEnabled ?? false,
      timezones,
      contacts,
      teams,
      cfGroups,
      this.handleManageAttachmentClick,
      this.showCaseOptions,
    );

    return adaptedFormTemplate;
  }

  configureForEditMode(warrant: Warrant): void {
    if (warrant.lea != null) {
      this.fetchCollectionFunctionsAndComputeServices(warrant.lea);
    }

    this.updateWarrantState(warrant);
  }

  isValid(errors: string[]): boolean {
    if (!errors) {
      errors = [];
    }

    const { warrant } = this.state;

    this.validateInterfieldRules(errors, warrant);

    const isValid = this.warrantDetailRef?.isValid() ?? false;

    if (this.auditRef) {
      const isAuditValid = this.auditRef.isValid();
      return (errors.length === 0 && isValid && isAuditValid);
    }

    return (errors.length === 0 && isValid);
  }

  isWarrantAndCasesValid(errors: string[]): [boolean, number | null] {
    const {
      cases,
      warrantConfigInstance,
    } = this.props;

    const {
      warrant,
      formTemplate,
    } = this.state;

    if (warrant == null || warrantConfigInstance == null) {
      return [false, null];
    }

    const { targetDefaultAFSelection } = this.props;
    const [areAllCasesValid, caseIndex] = CasePanel.areAllValid(
      warrant,
      cases,
      formTemplate.template.case as unknown as FormTemplate,
      warrantConfigInstance,
      errors,
      targetDefaultAFSelection,
    );

    return [areAllCasesValid, caseIndex];
  }

  validateInterfieldRules(errors: string[], formData: Warrant): void {
    const { mode } = this.props;
    const { originalWarrant } = this.state;

    const warrantStartDateTimeInDate = (!formData.startDateTime || formData.startDateTime === '') ? new Date() : new Date(formData.startDateTime);
    const warrantStopDateTimeInDate = new Date(formData.stopDateTime);

    if (mode === CONFIG_FORM_MODE.Create) {
      if (checkGivenTimeWithCurrentTime(formData.startDateTime, formData.timeZone)) {
        errors.push('Warrant : Start Time should be after the current time');
        return;
      }

      if (checkGivenTimeWithCurrentTime(formData.stopDateTime, formData.timeZone)) {
        errors.push('Warrant : Stop Time should be after the current time');
        return;
      }

      if (warrantStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
        errors.push('Warrant : Stop Time should be same or after the Start Time');
      }

    } else if (mode === CONFIG_FORM_MODE.Edit) {
      const hasAnyTimeChange = !areTimesEqual(
        originalWarrant?.startDateTime,
        originalWarrant?.stopDateTime,
        originalWarrant?.timeZone,
        formData.startDateTime,
        formData.stopDateTime,
        formData.timeZone,
      );

      if (hasAnyTimeChange) {
        const timeValidation = checkTimeValidationInComputerTimezone(
          originalWarrant?.startDateTime,
          originalWarrant?.stopDateTime as string,
        );

        if (!timeValidation && checkGivenTimeWithCurrentTime(formData.startDateTime, formData.timeZone)) {
          errors.push('Warrant : Start Time and Stop Time should be after the current time');
          return;
        }

        if (checkGivenTimeWithCurrentTime(formData.stopDateTime, formData.timeZone)) {
          errors.push('Warrant : Stop Time should be after the current time');
          return;
        }

        if (warrantStartDateTimeInDate.getTime() > warrantStopDateTimeInDate.getTime()) {
          errors.push('Warrant : Stop Time should be same or after the Start Time');
        }
      }
    }
  }

  static shouldUpdateExistingCases(fieldName: string): boolean {
    return !!WARRANT_TO_CASE_FIELD_NAME[fieldName];
  }

  async updateExistingCases(fields: UpdateExistingCaseType): Promise<void> {
    const {
      cases,
      changeTracker,
      updateCasesState,
    } = this.props;

    const clonedCases = cloneDeep(cases);

    const updatingCasePromises: Promise<void>[] = [];

    clonedCases
      .filter((entry) => entry.id != null && !entry.id.startsWith(TEMPORARY_CASE_ID_PREFIX))
      .forEach((entry) => {
        fields.forEach(({ name, value }) => {
          const caseFieldName = WARRANT_TO_CASE_FIELD_NAME[name];
          const oldFieldValue = entry[caseFieldName];
          if (caseFieldName === 'options') {
            entry[caseFieldName] = value as CaseOptionsType;
          } else {
            entry[caseFieldName] = value as string;
          }

          if (caseFieldName === 'type') {
            const updateCasePromise = this.updateExistingCasesType(value as string, entry);
            updatingCasePromises.push(updateCasePromise);
          }

          if (changeTracker != null) {
            changeTracker.addUpdated(`case.${entry.id}`, value, caseFieldName, oldFieldValue);
          }
        });
      });

    await Promise.all(updatingCasePromises);

    updateCasesState(clonedCases);
  }

  async updateExistingCasesType(type: string, warrantCase: CaseModel): Promise<void> {
    const {
      changeTracker,
      selectAllHI3Interfaces,
      mode,
      selectedDF,
    } = this.props;

    if (warrantCase.hi2Data == null || warrantCase.hi3Data == null) {
      warrantCase.hi2Data = await updateHI2Interfaces(warrantCase.cfId.id, selectedDF);
      warrantCase.hi3Data = await updateHI3Interfaces(warrantCase.cfId.id, selectedDF);
    }

    const hi3Interfaces = CasePanel.createHI3Interfaces(type, warrantCase, selectAllHI3Interfaces ?? false, mode);
    warrantCase.hi3Interfaces = hi3Interfaces;

    if (changeTracker) {
      changeTracker.addHI3Interface(warrantCase.id);
    }

    if (type !== 'CC' && type !== 'ALL') {
      CasePanel.clearTargetsCCCvaluesCCRType(warrantCase, changeTracker);
    }
  }

  showCaseOptions = () => {
    this.shouldWarrantUpdate = true;
    this.setState({ isCaseOptionsVisible: true });
  }

  handleCaseOptionsClose = () => {
    this.setState({ isCaseOptionsVisible: false });
  }

  static updateKey = (formTemplate: any) => {
    formTemplate.template.key = `Key_${new Date().getTime()}`;
  }

  handleCaseOptionsSubmit = (options: any, templateId: any) => {
    const {
      updateWarrantState,
    } = this.props;

    const {
      warrant,
      formTemplate,
    } = this.state;

    const clonedOptions = cloneDeep(options);
    clonedOptions.configureOptions = true;

    const clonedWarrant = cloneDeep(warrant);
    clonedWarrant.caseOptions = clonedOptions;
    clonedWarrant.caseOptTemplateUsed = templateId;

    WarrantPanel.updateKey({ ...formTemplate });

    this.setState({
      warrant: clonedWarrant,
      isCaseOptionsVisible: false,
      shouldWarrantDetailUpdate: true,
      formTemplate,
    });

    if (WarrantPanel.shouldUpdateExistingCases('caseOptions') || WarrantPanel.shouldUpdateExistingCases('caseOptTemplateUsed')) {
      this.updateExistingCases([
        { name: 'caseOptions', value: clonedOptions },
        { name: 'caseOptTemplateUsed', value: templateId },
      ]);
    }

    updateWarrantState(clonedWarrant);
  }

  handleTemplateChange = (elem: any) => {
    const prevTemplateId = this.state.formTemplate.id;
    const prevTemplate = this.templates.getById(prevTemplateId, Templates.CATEGORY_WARRANT_WIZARD);

    const selectedTemplateId = elem.currentTarget.value;
    const selectedTemplate = this.templates.getById(selectedTemplateId, Templates.CATEGORY_WARRANT_WIZARD);
    const formTemplate = this.getFormTemplate(selectedTemplate);

    const warrant = cloneDeep(this.state.warrant);

    this.setWarrantFromTemplate(warrant, formTemplate);
    warrant.templateUsed = selectedTemplateId;

    this.setState({
      formTemplate,
      warrant,
      shouldWarrantDetailUpdate: true,
    });
    this.props.updateFormTemplateUsed(formTemplate);
    this.shouldWarrantUpdate = true;
    // when warrant template update, fire changeTracker warrant
    if (this.props.changeTracker) {
      // ChangeTracker takes the original value from the first time addUpdated is called on a field.
      // Subsequent invocation of addUpdated on the same field will not change the original value.
      // Therefore, we can keep passing in the new prevTemplateId even after we have provided
      // the original value in the first invocation.
      this.props.changeTracker.addUpdated(`warrant.${this.props.warrantId}.template`, selectedTemplate.name, 'Template', prevTemplate.name);
    }
  }

  setWarrantFromTemplate = (warrant: any, template: Template): void => {
    const { mode } = this.props;

    const { fields } = template.template.general as FormTemplateSection;
    Object.keys(fields).forEach((field) => {
      const { initial } = fields[field];
      // Dates were already converted to computer timezone when loaded
      if (initial !== undefined && !(mode === 'edit' && field === 'startDateTime' &&
        checkTimeValidationInComputerTimezone(warrant.startDateTime, warrant.stopDateTime))) {
        warrant[field] = initial;
      }
    });
  }

  warrantDetailsFieldUpdateHandler = async (changedField: ChangedField): Promise<void> => {
    const {
      name, value, label, valueLabel,
    } = changedField;
    // Edit warrants: Update Warrant
    const warrant = cloneDeep(this.state.warrant);
    const prevValue = warrant[name];
    const prevDisplayValue = prevValue; // todo: map prevValue to prevDisplayValue.

    this.shouldWarrantUpdate = false;

    if (name === 'lea') {
      if (this.hasToConfirmLeaChange(prevValue, value)) {
        this.confirmLeaChange(value);
        return;
      }
      await this.leaChangeHandler(value);
    }

    warrant[name] = value;
    let fieldPassedToCases = '';
    if (fieldsPassedToCases.includes(name)) {
      fieldPassedToCases = name;
    }
    if (name === 'startDateTime' || name === 'stopDateTime' || name === 'receivedDateTime') {
      warrant[name] = trimJsonObj(value);
    }
    if (name === 'contact') {
      if (value === 'undefined') {
        warrant[name] = null;
      } else {
        warrant[name] = {
          id: value,
          name: value,
        };
      }
    }

    if (name === 'caseOptTemplateUsed') {
      warrant.caseOptions = getCaseOptionTemplateValue(this.props.templates, value);
    }
    const newState: any = {
      warrant,
      shouldWarrantDetailUpdate: false,
    };
    if (fieldPassedToCases) {
      newState[fieldPassedToCases] = value;
      this.shouldWarrantUpdate = true;
    }
    this.setState(newState);
    // when WarrantDetails fields update, fire changeTracker warrant
    if (this.props.changeTracker) {
      // ChangeTracker takes the original value from the first time addUpdated is called on a field.
      // Subsequent invocation of addUpdated on the same field will not change the original value.
      // Therefore, we can keep passing in the new prevValue even after we have provided
      // the original value in the first invocation.
      this.props.changeTracker.addUpdated(`warrant.${this.props.warrantId}.${name}`, valueLabel, label, prevDisplayValue);
    }

    if (WarrantPanel.shouldUpdateExistingCases(name)) {
      if (name === 'caseOptTemplateUsed') {
        const clonedOptions = cloneDeep(warrant.caseOptions);
        this.updateExistingCases([
          { name: 'caseOptions', value: clonedOptions },
          { name: 'caseOptTemplateUsed', value },
        ]);

      } else {
        this.updateExistingCases([{ name: name as AssignableWarrantFields, value }]);
      }
    }

    this.updateWarrantState(warrant);
  }

  handleManageAttachmentClick = (): void => {
    this.shouldWarrantUpdate = true;
    this.setState({ isManageAttachmentVisible: true });
  }

  handleManageAttachmentClose = (): void => {
    this.setState({ isManageAttachmentVisible: false });
  }

  handleManageAttachmentSubmit = (attachments: any): void => {
    const warrant = cloneDeep(this.state.warrant);
    warrant.attachments = attachments;
    const formTemplate = { ...this.state.formTemplate };
    (formTemplate.template.general as FormTemplateSection).fields.attachmentsDetails.initial = `${attachments.length} Attachments`;
    this.setState({
      warrant,
      formTemplate,
      isManageAttachmentVisible: false,
      shouldWarrantDetailUpdate: true,
    });
  }

  submit = async (): Promise<boolean> => {
    const {
      warrant,
    } = this.state;

    const clonedWarrant = cloneDeep(warrant);

    this.updateWarrantState(clonedWarrant);

    return true;
  }

  updateWarrantState(warrant: Warrant): void {
    const {
      isAuditEnabled,
      updateWarrantAuditState,
      updateWarrantState,
    } = this.props;

    if (isAuditEnabled && this.auditRef) { // update reducer
      const userAudit = this.auditRef.getValue();
      updateWarrantAuditState({ warrant, userAudit });

    } else {
      updateWarrantState(warrant);
    }
  }

  async fetchCollectionFunctionsAndComputeServices(lea: string): Promise<void> {
    await this.fetchCollectionFunctions(lea);
    await this.computeEnabledServicesForDevicePanel();
  }

  async fetchCollectionFunctions(lea: string): Promise<void> {
    const {
      fetchCollectionFunctionList,
      selectedDF,
      cfGroups,
      fetchCollectionFunctionListByIds,
    } = this.props;

    if (lea === WARRANT_LEA_ANY) {
      await fetchCollectionFunctionList(selectedDF);

    } else {
      const cfg = cfGroups.find((cfGroup: any) => {
        return cfGroup.id === lea;
      });

      const ids = cfg?.cfs.map((obj: any) => {
        return obj.id;
      });

      if (ids) {
        await fetchCollectionFunctionListByIds(selectedDF, ids.join(','));
      }
    }
  }

  async computeEnabledServicesForDevicePanel(): Promise<void> {
    const { cfs, warrantConfigInstance } = this.props;
    const cfTypes = WarrantPanel.cfsToTypes(cfs);
    await warrantConfigInstance?.computeEnabledServicesForDevicePanel(cfTypes);
  }

  static cfsToTypes = (cfs: any[]) => {
    const types = new Set<string>();
    cfs.forEach((cf: any) => {
      types.add(cf.tag);
    });
    return [...types];
  }

  hasToConfirmLeaChange(previousLeaValue: string, nextLeaValue: string): boolean {
    const {
      hasConfiguredDevices,
    } = this.props;

    const leaChanged = previousLeaValue != null && previousLeaValue !== '' && previousLeaValue !== nextLeaValue;

    return hasConfiguredDevices() && leaChanged;
  }

  async leaChangeHandler(lea: string): Promise<void> {
    const {
      mode,
      clearCasesState,
    } = this.props;

    if (mode === CONFIG_FORM_MODE.Create) {
      clearCasesState();
    }

    this.shouldWarrantUpdate = true;
    this.setState({ isRetrievingCfsServices: true });
    await this.fetchCollectionFunctionsAndComputeServices(lea);
    this.setState({ isRetrievingCfsServices: false });
  }

  confirmLeaChange(newLeaValue: string): void {
    this.setState({
      isLEAChangeConfirmOpen: true,
      newLeaValue,
    });
    this.shouldWarrantUpdate = true;
  }

  leaChangeCancelHandler = (): void => {
    const {
      formTemplate,
    } = this.state;

    const clonedFormTemplate = cloneDeep(formTemplate);
    WarrantPanel.updateKey(clonedFormTemplate);

    this.setState({
      formTemplate: clonedFormTemplate,
      isLEAChangeConfirmOpen: false,
      shouldWarrantDetailUpdate: true,
    });
    this.shouldWarrantUpdate = true;
  }

  leaChangeConfirmHandler = async (): Promise<void> => {
    const {
      warrant,
      newLeaValue,
    } = this.state;

    const clonedWarrant = cloneDeep(warrant);
    clonedWarrant.lea = newLeaValue;

    this.setState({
      warrant: clonedWarrant,
      isLEAChangeConfirmOpen: false,
    });
    this.shouldWarrantUpdate = true;

    await this.leaChangeHandler(newLeaValue);
    this.updateWarrantState(clonedWarrant);
  }

  shouldComponentUpdate = (): boolean => {
    return this.shouldWarrantUpdate;
  }

  renderWarrantState(): JSX.Element | null {
    const { mode } = this.props;
    const { warrant } = this.state;

    if (mode !== CONFIG_FORM_MODE.Edit && mode !== CONFIG_FORM_MODE.View) {
      return null;
    }

    return (
      <div className="field">
        <Label className="bp3-inline">
          State: <span className="warrant-state">{warrant.state}</span>
        </Label>
      </div>
    );
  }

  render(): JSX.Element {
    const {
      formTemplate,
      warrant,
      shouldWarrantDetailUpdate,
      isManageAttachmentVisible,
      isCaseOptionsVisible,
      isLEAChangeConfirmOpen,
      isRetrievingCfsServices,
    } = this.state;

    const {
      mode,
      changeTracker,
      isAuditEnabled,
      userAudit,
      templates,
      warrantId,
    } = this.props;
    return (
      <Fragment>
        <div className={mode === 'view' ? '' : 'formEnabled'}>
          <div className="form-flex-row-css">
            <div className="field">
              <Label className="bp3-inline">
                Template:
                <HTMLSelect
                  options={this.templates.getDropdownOptions(Templates.CATEGORY_WARRANT_WIZARD)}
                  onChange={this.handleTemplateChange}
                  value={formTemplate.id}
                  disabled={mode === CONFIG_FORM_MODE.View}
                />
              </Label>
            </div>
            {this.renderWarrantState()}
            <div className="field" />
            <div className="field" />
            <div className="field" />
          </div>
        </div>
        <WarrantDetails
          ref={(ref) => { this.warrantDetailRef = ref; }}
          formTemplate={formTemplate.template}
          defaults={warrant}
          shouldWarrantDetailUpdate={shouldWarrantDetailUpdate}
          fieldUpdateHandler={this.warrantDetailsFieldUpdateHandler}
          mode={mode}
        />
        {isAuditEnabled && (
          <UserAudit
            data-test="userAudit"
            ref={(ref: any) => { this.auditRef = ref; }}
            defaults={userAudit}
            mode={mode}
          />
        )}
        <ManageAttachments
          isOpen={isManageAttachmentVisible}
          warrantAttachments={warrant.attachments}
          handleClose={this.handleManageAttachmentClose}
          handleSubmit={this.handleManageAttachmentSubmit}
          mode={mode}
          changeTracker={changeTracker}
        />
        <CaseOptions
          isOpen={isCaseOptionsVisible}
          currentStateMode={mode}
          handleClose={this.handleCaseOptionsClose}
          inputCaseOptions={warrant.caseOptions ?? null}
          handleSubmit={this.handleCaseOptionsSubmit}
          caseId={undefined}
          warrantId={warrantId}
          templateUsed={warrant.caseOptTemplateUsed ?? null}
          templates={templates}
        />
        <Dialog
          autoFocus
          isOpen={isLEAChangeConfirmOpen}
          title="Warning"
          className=""
          onClose={this.leaChangeCancelHandler}
        >
          <div className={Classes.DIALOG_BODY}>
            <p>
              Changing LEA may affect the availability of services on Devices.
              <br /><br />
              Are you sure you want to change LEA value?
            </p>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button data-testid="ConfirmLEAChangedCancelButton" onClick={this.leaChangeCancelHandler}>Cancel</Button>
              <Button data-testid="ConfirmLEAChangedConfirmButton" intent="primary" onClick={this.leaChangeConfirmHandler}>Yes, change it</Button>
            </div>
          </div>
        </Dialog>
        <RetrievingDataProgress isOpen={isRetrievingCfsServices} message="Retrieving services..." />
      </Fragment>
    );
  }
}

export default connector(WarrantPanel);
