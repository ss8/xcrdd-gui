import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { AxiosError } from 'axios';
import { ServerResponse } from 'data/types';
import {
  ActionHandler,
  IActionHandlerProps,
  IActionHandlerStatus,
} from './index';

describe('<ActionHandler />', () => {
  let props: IActionHandlerProps;
  let wrapper: ShallowWrapper<IActionHandlerProps, IActionHandlerStatus, ActionHandler>;

  beforeEach(() => {
    props = {
      isOpen: true,
      selectedWarrants: [],
      handleAction: jest.fn(),
      onClose: jest.fn(),
    };

    wrapper = shallow<ActionHandler, IActionHandlerProps, IActionHandlerStatus>(
      <ActionHandler {...props} />,
    );
  });

  describe('handleAction()', () => {
    it('should call handleAction from props with warrant', async () => {
      const warrant = { name: 'warrant' };
      await wrapper.instance().handleAction(warrant);
      expect(props.handleAction).toBeCalledWith(warrant);
    });

    it('should update counter in state on request success', async () => {
      const warrant = { name: 'warrant' };
      props.handleAction.mockImplementation(() => Promise.resolve());
      await wrapper.instance().handleAction(warrant);
      expect(wrapper.instance().state.counter).toEqual(1);
    });

    it('should update errors in state on request failed', async () => {
      const warrant = { name: 'warrant' };
      const error: AxiosError<ServerResponse<void>> = {
        config: {},
        name: 'erro1',
        message: 'some error message',
        isAxiosError: true,
        toJSON: jest.fn(),
        response: {
          status: 500,
          statusText: '',
          headers: [],
          config: {},
          data: {
            data: null,
            metadata: null,
            success: false,
            error: {
              status: 500,
              code: 'INTERNAL_SERVER_ERROR',
              message: 'some server error message',
            },
            errors: [],
            message: '',
            status: '',
          },
        },
      };

      props.handleAction.mockImplementation(() => Promise.reject(error));
      await wrapper.instance().handleAction(warrant);
      expect(wrapper.instance().state.errors).toEqual([{
        warrant,
        error: 'some server error message',
      }]);
    });
  });
});
