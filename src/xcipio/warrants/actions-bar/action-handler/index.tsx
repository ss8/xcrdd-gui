import React from 'react';
import { connect } from 'react-redux';
import { RootState } from 'data/root.reducers';
import { extractErrorMessage } from '../../../../utils';
import ProgressBarIndicator from '../../../../components/progress-bar-indicator';

export interface IActionHandlerStatus {
  isOpen: boolean,
  counter: number,
  errors: Array<any>,
  inProgress: boolean,
}

export interface IActionHandlerProps {
  isOpen: boolean,
  selectedWarrants: Array<any>,
  handleAction: any,
  onClose: any
}

export class ActionHandler extends React.Component<IActionHandlerProps, IActionHandlerStatus> {
  constructor(props: IActionHandlerProps) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      isOpen: false,
      counter: 0,
      errors: [],
      inProgress: false,
    };
  };

  componentDidUpdate() {
    if (this.props.isOpen !== this.state.isOpen) {
      this.setState({ isOpen: this.props.isOpen }, () => {
        this.initProcess();
      });
    }
  }

  initProcess = () => {
    if (this.state.isOpen) {
      this.setState({ inProgress: true }, () => {
        this.handleProcess().then(() => {
          this.setState({ inProgress: false });
        });
      });
    }
  };

  handleProcess = async () => {
    for (
      let index = 0;
      index < this.props.selectedWarrants.length && this.state.inProgress;
      index += 1
    ) {
      const warrant = this.props.selectedWarrants[index];
      if (warrant.autoFormValidation && !warrant.autoFormValidation.isValid) {
        this.processError(warrant, warrant.autoFormValidation.errorMessage);
      } else await this.handleAction(warrant);
    }
  };

  handleAction = async (warrant: any) => {
    try {
      await this.props.handleAction(warrant);
      this.setState({ counter: this.state.counter + 1 });
    } catch (e) {
      this.processError(warrant, extractErrorMessage(e));
    }
  };

  processError = (warrant: any, error: string) => {
    const { errors } = this.state;
    errors.push({ warrant, error });
    this.setState({ errors });
  };

  handleConfirm = () => {
    if (this.state.inProgress) {
      this.setState({ inProgress: false });
    } else {
      this.setState({ ...this.getInitialState() });
      this.props.onClose();
    }
  };

  render() {
    const totalProcessed = this.state.counter + this.state.errors.length;
    const errors = this.state.errors.map((err: any) => {
      return `${err.warrant.name}:  ${err.error}`;
    });
    const { isOpen, inProgress, counter } = this.state;
    return (
      <ProgressBarIndicator
        isOpen={isOpen}
        handleConfirm={this.handleConfirm}
        inProgress={inProgress}
        errors={errors}
        counter={counter}
        totalSelected={this.props.selectedWarrants.length}
        totalProcessed={totalProcessed}
      />
    );
  }
}

const mapStateToProps = ({ warrants: { selectedWarrants } }: RootState) => ({
  selectedWarrants,
});

export default connect(mapStateToProps)(ActionHandler);
