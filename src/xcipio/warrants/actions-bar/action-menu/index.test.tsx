import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import { mockWarrants } from '__test__/mockWarrants';
import { Warrant } from 'data/warrant/warrant.types';
import store from 'data/store';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import { BROADCAST_SELECTED_WARRANTS } from 'xcipio/warrants/warrant-actions';
import AuthActions from 'data/authentication/authentication.types';
import { WARRANT_RESSTATE } from 'xcipio/warrants/warrant-enums';
import ActionMenuBar from './index';

describe('<ActionMenuBar />', () => {
  let warrant: Warrant;

  beforeEach(() => {
    [warrant] = cloneDeep(mockWarrants);

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: [warrant],
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });
  });

  it('should not show any button if no warrant selected', () => {
    store.dispatch({
      type: BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: [],
    });

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeNull();
    expect(screen.queryByText('Edit')).toBeNull();
    expect(screen.queryByText('Provisioning Retry')).toBeNull();
  });

  it('should show expected buttons if warrant is active', () => {
    warrant.state = WARRANT_RESSTATE.Active;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeVisible();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is paused', () => {
    warrant.state = WARRANT_RESSTATE.Paused;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeVisible();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is stopped', () => {
    warrant.state = WARRANT_RESSTATE.Stopped;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeVisible();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is expired', () => {
    warrant.state = WARRANT_RESSTATE.Expired;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeVisible();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should show expected buttons if warrant is pending', () => {
    warrant.state = WARRANT_RESSTATE.Pending;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeNull();
    expect(screen.queryByText('Reschedule')).toBeVisible();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Pause button if warrant is active but Pause action is not enabled', () => {
    const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
    discoverySettings.appSettings.forEach((entry) => {
      if (entry.name === 'isWarrantPauseActionEnabled') {
        entry.setting = 'false';
      }
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([discoverySettings]),
    });

    warrant.state = WARRANT_RESSTATE.Active;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Resume button if warrant is paused but resume action is not enabled', () => {
    const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
    discoverySettings.appSettings.forEach((entry) => {
      if (entry.name === 'isWarrantResumeActionEnabled') {
        entry.setting = 'false';
      }
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([discoverySettings]),
    });

    warrant.state = WARRANT_RESSTATE.Paused;

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeNull();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeVisible();
    expect(screen.queryByText('Provisioning Retry')).toBeVisible();
  });

  it('should not show Provisioning Retry and Edit buttons if more than one warrant selected', () => {
    warrant.state = WARRANT_RESSTATE.Active;

    store.dispatch({
      type: BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: [warrant, warrant],
    });

    render(
      <Provider store={store}>
        <ActionMenuBar
          handleClose={jest.fn()}
          handleEdit={jest.fn()}
          onReloadWarrant={jest.fn()}
          onWarrantAction={jest.fn()}
          keepSelectedWarrantOnCancelReschedule
        />
      </Provider>,
    );

    expect(screen.queryByText('Start')).toBeNull();
    expect(screen.queryByText('Stop')).toBeVisible();
    expect(screen.queryByText('Reschedule')).toBeNull();
    expect(screen.queryByText('Pause')).toBeVisible();
    expect(screen.queryByText('Resume')).toBeNull();
    expect(screen.queryByText('Delete')).toBeVisible();
    expect(screen.queryByText('Edit')).toBeNull();
    expect(screen.queryByText('Provisioning Retry')).toBeNull();
  });
});
