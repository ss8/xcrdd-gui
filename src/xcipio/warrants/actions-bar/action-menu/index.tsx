import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button } from '@blueprintjs/core';
import { WARRANT_PRIVILEGE } from 'xcipio/warrants/warrant-enums';
import { RootState } from 'data/root.reducers';
import WarrantRetryPopup from 'routes/Warrant/components/WarrantRetryPopup';
import WarrantActionsHandler from '../../warrant-actions-handler';
import { COMMON_GRID_ACTION, COMMON_GRID_ACTION_DISPLAY } from '../../../../shared/commonGrid/grid-enums';
import showActionMenuItem from '../../grid/utils';
import RescheduleDialog from '../../reschedule';

interface IActionMenuProps {
  selectedWarrants: any[],
  bArchiveWarrant: boolean,
  displayTrashWarrants: boolean,
  handleClose?: (action: string) => void,
  handleEdit: () => void,
  privileges: string[],
  onReloadWarrant?: () => void
  onWarrantAction?: (action: COMMON_GRID_ACTION) => void
  keepSelectedWarrantOnCancelReschedule?: boolean,
  isWarrantResumeActionEnabled: boolean,
  isWarrantPauseActionEnabled: boolean,
}
const allActions = [
  COMMON_GRID_ACTION.Start,
  COMMON_GRID_ACTION.Stop,
  COMMON_GRID_ACTION.Reschedule,
  COMMON_GRID_ACTION.Resume,
  COMMON_GRID_ACTION.Pause,
  COMMON_GRID_ACTION.Trash,
  COMMON_GRID_ACTION.Delete,
  COMMON_GRID_ACTION.Restore,
  COMMON_GRID_ACTION.Edit,
  COMMON_GRID_ACTION.Reprovision,
];

const ActionMenuBar = ({
  selectedWarrants,
  bArchiveWarrant,
  displayTrashWarrants,
  handleClose,
  handleEdit,
  privileges,
  onReloadWarrant,
  onWarrantAction,
  keepSelectedWarrantOnCancelReschedule,
  isWarrantResumeActionEnabled,
  isWarrantPauseActionEnabled,
}: IActionMenuProps): JSX.Element | null => {
  const [action, setAction] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [isRescheduleDialodOpen, setIsRescheduleDialodOpen] = useState(false);
  const [isWarrantRetryPopupOpen, setIsWarrantRetryPopupOpen] = useState(false);
  const [visibleActions, setVisibleActions] = useState([]);
  const onClose = (warrantAction: string) => {
    setAction('');
    setIsOpen(false);
    setIsRescheduleDialodOpen(false);
    setIsWarrantRetryPopupOpen(false);
    if (handleClose) {
      handleClose(warrantAction);
    }
  };
  const handleAction = (warrantAction: string) => {
    if (warrantAction === COMMON_GRID_ACTION.Reschedule) {
      setIsRescheduleDialodOpen(true);
    } else if (warrantAction === COMMON_GRID_ACTION.Edit) {
      handleEdit();
    } else if (warrantAction === COMMON_GRID_ACTION.Reprovision) {
      setIsWarrantRetryPopupOpen(true);
    } else {
      setAction(warrantAction);
      setIsOpen(true);
    }
  };
  const calculateVisibleActions = () => {
    let nextVisibleActions: any = [];
    nextVisibleActions = allActions.filter((warrantAction: string) => {
      if (!isActionValidBasedOnTrashFeatureEnabled(warrantAction)) return false;
      if (!isActionValidBasedOnPrivileges(warrantAction)) return false;
      if (!isActionValidBasedOnNumberOfSelectedWarrants(warrantAction)) return false;
      return selectedWarrants.every((warrant: any) => {
        return showActionMenuItem(
          warrantAction,
          warrant,
          isWarrantResumeActionEnabled,
          isWarrantPauseActionEnabled,
        );
      });
    });
    return nextVisibleActions;
  };

  const isActionValidBasedOnNumberOfSelectedWarrants = (warrantAction: string): boolean => {
    // these actions are only applicable to one warrant
    if (warrantAction === COMMON_GRID_ACTION.Edit || warrantAction === COMMON_GRID_ACTION.Download ||
      warrantAction === COMMON_GRID_ACTION.View || warrantAction === COMMON_GRID_ACTION.Reprovision) {
      return (selectedWarrants?.length === 1);
    }
    return true;
  };

  const isActionValidBasedOnTrashFeatureEnabled = (warrantAction: string) => {
    if (bArchiveWarrant === false) {
      if (warrantAction === COMMON_GRID_ACTION.Restore || warrantAction === COMMON_GRID_ACTION.Trash) return false;
    } else if (displayTrashWarrants === false) {
      if (warrantAction === COMMON_GRID_ACTION.Restore || warrantAction === COMMON_GRID_ACTION.Delete) return false;
    } else if (warrantAction === COMMON_GRID_ACTION.Trash || warrantAction === COMMON_GRID_ACTION.Reprovision) {
      return false;
    }
    return true;
  };

  const isActionValidBasedOnPrivileges = (warrantAction: string) => {
    if (warrantAction === COMMON_GRID_ACTION.Delete && privileges.includes(WARRANT_PRIVILEGE.Delete)) return true;
    if (privileges.includes(WARRANT_PRIVILEGE.Edit)) return true;
    return false;
  };

  useEffect(() => {
    const nextVisibleActions = calculateVisibleActions();
    setVisibleActions(nextVisibleActions);
  }, [selectedWarrants]);

  if (selectedWarrants.length === 0) {
    return null;
  }

  return (
    <Fragment>
      <div style={{ display: 'flex', float: 'left' }}>
        {visibleActions.map((visibleAction: string, index: number) => {
          return (
            <Button
              key={index}
              data-test={`${visibleAction?.toLowerCase()}-button`}
              style={{ paddingTop: '5px', marginLeft: '10px' }}
              text={(COMMON_GRID_ACTION_DISPLAY as any)[visibleAction]}
              onClick={() => handleAction(visibleAction)}
            />
          );
        })}
      </div>
      <WarrantActionsHandler
        data-test="update-warrant-status"
        action={action}
        onClose={onClose.bind(null, action)}
        isOpen={isOpen}
        onReloadWarrant={onReloadWarrant}
        onWarrantAction={onWarrantAction}
      />
      <RescheduleDialog
        data-test="warrants-reschedule"
        isOpen={isRescheduleDialodOpen}
        keepSelectedWarrantOnCancelReschedule={keepSelectedWarrantOnCancelReschedule}
        onReloadWarrant={onReloadWarrant}
        onClose={onClose.bind(null, COMMON_GRID_ACTION.Reschedule)}
      />
      <WarrantRetryPopup
        isOpen={isWarrantRetryPopupOpen}
        handleConfirm={onClose.bind(null, COMMON_GRID_ACTION.Reprovision)}
        warrant={selectedWarrants[0]}
        onReload={onReloadWarrant}
        onWarrantAction={onWarrantAction?.bind(null, COMMON_GRID_ACTION.Reprovision)}
      />
    </Fragment>
  );
};

const mapStateToProps = ({
  discoveryXcipio: {
    discoveryXcipioSettings: {
      Warrant_bArchiveWarrant: bArchiveWarrant = false,
      isWarrantResumeActionEnabled = false,
      isWarrantPauseActionEnabled = false,
    },
  },
  warrants: { selectedWarrants, displayTrashWarrants },
  authentication: {
    privileges,
  },
}: RootState) => ({
  bArchiveWarrant,
  displayTrashWarrants,
  selectedWarrants,
  privileges,
  isWarrantResumeActionEnabled,
  isWarrantPauseActionEnabled,
});

export default connect(mapStateToProps)(ActionMenuBar);
