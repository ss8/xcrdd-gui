import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, { Fragment, ReactElement } from 'react';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import {
  getAllAttachments,
  getAttachmentById,
  createAttachment,
  deleteAttachment,
  clearAllAttachments,
  downloadAttachment,
} from './attachments-actions';
import { putUserSettings, getUserSettings } from '../../users/users-actions';
import ConfigFormPopup, { CONFIG_FORM_MODE } from '../../shared/popup/config-form-popup';
import CommonGrid from '../../shared/commonGrid';
import { COMMON_GRID_ACTION, ROWSELECTION } from '../../shared/commonGrid/grid-enums';
import AttachmentsGridColumnDefs from './attachment-grid-column-defs';
import UploadAttachment from './uploadAttachments';
import DeleteAttachmentConfirmPopup from './delete/attachment-delete';
import ViewAttachment from './viewAttachment';
import ChangeTracker from '../warrants/forms/warrant-change-tracker';
import { setWarrantTracker } from '../warrants/warrant-actions';
import { WARRANT_PRIVILEGE } from '../warrants/warrant-enums';
import AttachmentActionMenu from './attachment-grid-action-menu';

const manageAttachmentTitle = 'Manage Attachments';

const uploadFilePopUpTitle = 'Upload File for Attachments';

const viewAttachmentFilePopUpTitle = 'View Attachment';

export class ManageAttachments extends React.Component<any, any> {
  ref: any;

  localChangeTracker: ChangeTracker|undefined;

  attachmentsGridApi: any;

  ATTACHMENTS_GRID_SETTINGS = 'attachmentsGridSettings';

  columnDefs = AttachmentsGridColumnDefs;

  constructor(props: any) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen,
      fieldDirty: false,
      currentStateMode: this.props.mode,
      isViewAttachmentVisible: false,
      isUploadFileVisible: false,
      isDeleteAttachmentPopupVisible: false,
      attachmentfiles: this.props.warrantAttachments ?
        this.props.warrantAttachments : [],
      selectedFile: this.props.warrantAttachments ?
        this.props.warrantAttachments[0] : undefined,
      selectedFileContent: {},
    };
    // Need a localChangeTracker because users can 'cancel' out of
    // this popup.  Once user presses OK in this popup, we can
    // set the global ChangeTracker to this localChangeTracker
    if (this.props.changeTracker) {
      this.localChangeTracker = cloneDeep(this.props.changeTracker);
    }
  }

  componentDidUpdate = (prevProps:any) => {
    if (!prevProps.isOpen && this.props.isOpen) {
      this.setState({
        isOpen: this.props.isOpen,
        currentStateMode: this.props.mode,
        attachmentfiles: this.props.warrantAttachments ?
          this.props.warrantAttachments : [],
        selectedFile: this.props.warrantAttachments ?
          this.props.warrantAttachments[0] : undefined,

      });
      // Need a localChangeTracker because users can 'cancel' out of
      // this popup.  Once user presses OK in this popup, we can
      // set the global ChangeTracker to this localChangeTracker
      if (this.props.changeTracker) {
        this.localChangeTracker = cloneDeep(this.props.changeTracker);
      }
    }
  }

  handleClose = () => {
    this.props.handleClose();
    this.setState({
      isOpen: false, fieldDirty: false,
    });
  }

  handleSubmit = () => {
    // user is committing the change, so we can set the global ChangeTracker to
    // this localChangeTracker
    if (this.localChangeTracker) {
      this.props.setWarrantTracker(this.localChangeTracker);
    }
    this.props.handleSubmit(this.state.attachmentfiles);
    this.setState({
      isOpen: false,
      fieldDirty: false,
      currentStateMode: undefined,
      attachmentfiles: undefined,
      selectedFile: undefined,
    });
  }

  getTitle = () => manageAttachmentTitle;

  handleFileUploadVisible = () => {
    if (this.state.currentStateMode === CONFIG_FORM_MODE.Edit ||
      this.state.currentStateMode === CONFIG_FORM_MODE.Create) {
      this.setState({
        isUploadFileVisible: true,
        currentStateMode: this.props.mode,
      });
    }
  }

  handleUploadFilePopUpClose = () => {
    this.setState({ isUploadFileVisible: false });
  }

  handleUploadFilePopUpSubmit = (attachmentsToUpload: any) => {
    let newAttachmentFiles : any = [];
    if (this.state.attachmentfiles) {
      newAttachmentFiles = [...this.state.attachmentfiles];
    }
    if (attachmentsToUpload) {
      attachmentsToUpload.forEach((attachmentDetail: any) => {
        let attachmentId = attachmentDetail.file.name;
        if (this.localChangeTracker) {
          attachmentId = this.localChangeTracker.addAdded('warrant.attachment', attachmentDetail.file.name, 'Attachment');
        }
        const attachment = {
          name: attachmentDetail.file.name,
          size: attachmentDetail.file.size,
          description: attachmentDetail.description,
          id: attachmentId,
          herf: null,
          file: attachmentDetail.file,
          newAddedAttachment: true,
          owner: {
            id: this.props.authentication.userDBId,
            name: this.props.authentication.userId,
          },
        };
        newAttachmentFiles.push(attachment);
      });
    }
    this.setState({
      isUploadFileVisible: false,
      fieldDirty: true,
      attachmentfiles: newAttachmentFiles,
    });
  }

  handleDeleteAttachmentPopupClose = () => {
    this.setState({ isDeleteAttachmentPopupVisible: false });
  }

  handleDeleteAttachmentPopupSubmit = () => {
    const attachmentfiles = [...this.state.attachmentfiles].filter((attach: any) => {
      return attach.name !== this.state.selectedFile.name;
    });
    // add change tracker when edit a warrant
    if (this.localChangeTracker && this.state.selectedFile) {
      this.localChangeTracker.addDeleted(`warrant.attachment.${this.state.selectedFile.id}`,
        this.state.selectedFile.name, 'Attachment');
    }
    this.setState({
      attachmentfiles,
      fieldDirty: true,
      isDeleteAttachmentPopupVisible: false,
      selectedFile: undefined,
    });
  }

  getUploadFilePopUpTitle = () => uploadFilePopUpTitle;

  getAttachmentFilePopUpTitle = () => viewAttachmentFilePopUpTitle;

  handleViewAttachmentFilePopUpClose = () => {
    this.setState({ isViewAttachmentVisible: false });
  }

  saveGridSettings = (attachmentsGridSettings: {[key: string]: any}) => {
    if (attachmentsGridSettings !== null) {
      this.props.putUserSettings(this.ATTACHMENTS_GRID_SETTINGS,
        this.props.authentication.userId, attachmentsGridSettings);
    }
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getUserSettings(this.ATTACHMENTS_GRID_SETTINGS,
      this.props.authentication.userId);
  }

  onActionMenuItemClick = (e: any) => {
    const { type } = e;
    const attachment = { ...this.state.selectedFile };
    const warrantId = this.props.warrant ? this.props.warrant.id :
      (this.props.history && this.props.history.location.warrant) ?
        this.props.history.location.warrant.id : undefined;
    const df = this.props.selectedDF;
    const attachmentId = attachment.id;
    switch (type) {
      case COMMON_GRID_ACTION.Delete: {
        this.setState({ isDeleteAttachmentPopupVisible: true });
        break;
      }
      case COMMON_GRID_ACTION.Download: {
        this.props.downloadAttachment(df, warrantId, attachmentId).then((response:any) => {
          this.handleAttachment(true, response, attachment.name);
        });
        break;
      }
      case COMMON_GRID_ACTION.View: {
        this.props.downloadAttachment(df, warrantId, attachmentId).then((response:any) => {
          this.handleAttachment(false, response, attachment.name);
        });
        break;
      }
      default:
    }
  }

  handleAttachment = (isDownload:any, response:any, name:string) => {
    const attachment = new Blob([response.value.data], { type: response.value.headers['content-type'] });
    const url = window.URL.createObjectURL(attachment);
    if (isDownload) {
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', name);
      document.body.appendChild(link);
      link.click();
    } else {
      window.open(url);
    }
    setTimeout(() => {
      window.URL.revokeObjectURL(url);
    }, 100);
  }

  onSelectionChanged = (e:any) => {
    this.setState({ selectedFile: e.api.getSelectedRows()[0] });
  }

  render(): ReactElement {
    const {
      authentication: {
        privileges,
        beforeLogout,
      },
      warrant,
      limit,
    } = this.props;

    const {
      currentStateMode,
      isUploadFileVisible,
      fieldDirty,
      isOpen,
      isViewAttachmentVisible,
      selectedFile,
      isDeleteAttachmentPopupVisible,
      attachmentfiles,
    } = this.state;

    const hasPrivilegesToEdit = privileges?.includes(WARRANT_PRIVILEGE.Edit);
    const hasPrivilegesToView = privileges?.includes(WARRANT_PRIVILEGE.View) &&
      currentStateMode !== CONFIG_FORM_MODE.Create;
    const hasPrivilegeToDownload = hasPrivilegesToEdit && currentStateMode === CONFIG_FORM_MODE.Edit;
    const hasPrivilegeToCreateNew = hasPrivilegesToEdit && currentStateMode !== CONFIG_FORM_MODE.View;

    return (
      <Fragment>
        <ConfigFormPopup
          currentStateMode={currentStateMode}
          hasPrivilegesToEdit={hasPrivilegesToEdit}
          isFirstChildLeftOfEditButton={false}
          fieldDirty={fieldDirty}
          onClose={this.handleClose}
          onSubmit={this.handleSubmit}
          getTitle={this.getTitle}
          isOpen={isOpen}
          doNotDisplayEdit
          width="60%"
          height="auto"
          overflow="auto"
        >
          <UploadAttachment
            data-test="manageAttachment-uploadAttachment"
            warrant={warrant}
            isOpen={isUploadFileVisible}
            onClose={this.handleUploadFilePopUpClose}
            onSubmit={this.handleUploadFilePopUpSubmit}
            currentStateMode={currentStateMode}
            fieldDirty={fieldDirty}
            hasPrivilegesToEdit={hasPrivilegesToEdit}
            isFirstChildLeftOfEditButton={false}
            getTitle={this.getUploadFilePopUpTitle}
            width="50%"
            limit={limit}
          />
          <ViewAttachment
            isOpen={isViewAttachmentVisible}
            onClose={this.handleViewAttachmentFilePopUpClose}
            onSubmit={this.handleViewAttachmentFilePopUpClose}
            currentStateMode={currentStateMode}
            attachmentFile={selectedFile}
            isFirstChildLeftOfEditButton={false}
            fieldDirty={false}
            getTitle={this.getAttachmentFilePopUpTitle}
            width="50%"
          />
          <DeleteAttachmentConfirmPopup
            data-test="manageAttachment-deleteAttachmentConfirmPopup"
            isOpen={isDeleteAttachmentPopupVisible}
            onClose={this.handleDeleteAttachmentPopupClose}
            onSubmit={this.handleDeleteAttachmentPopupSubmit}
            selection={selectedFile}
          />
          <div style={{ height: '200px' }}>
            <CommonGrid
              data-test="manageAttachment-commonGrid"
              onSelectionChanged={this.onSelectionChanged}
              onActionMenuItemClick={this.onActionMenuItemClick}
              actionMenuClass={AttachmentActionMenu}
              columnDefs={this.columnDefs}
              rowData={attachmentfiles}
              context={this}
              rowSelection={ROWSELECTION.SINGLE}
              pagination={false}
              enableSorting
              enableFilter
              enableBrowserTooltips
              hasPrivilegetoView={hasPrivilegesToView}
              hasPrivilegeToDownload={hasPrivilegeToDownload}
              hasPrivilegeToCreateNew={hasPrivilegeToCreateNew}
              hasPrivilegeToDelete={hasPrivilegeToCreateNew}
              createNewText={hasPrivilegeToCreateNew ? '+' : ''}
              handleCreateNew={hasPrivilegeToCreateNew ?
                this.handleFileUploadVisible : undefined}
              getGridSettingsFromServer={this.handleGetGridSettingsFromServer}
              isBeforeLogout={beforeLogout}
              saveGridSettings={this.saveGridSettings}
              noResults={!attachmentfiles}
            />
          </div>
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  warrants: {
    warrant,
    changeTracker,
  },
  attachments,
  global: { selectedDF },
  authentication,
  discoveryXcipio: {
    discoveryXcipioSettings: { Attachment_Size_Limit: limit },
  },
}: RootState) => ({
  warrant,
  changeTracker,
  selectedDF,
  attachments,
  authentication,
  limit,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  setWarrantTracker,
  getAllAttachments,
  getAttachmentById,
  createAttachment,
  deleteAttachment,
  putUserSettings,
  getUserSettings,
  clearAllAttachments,
  downloadAttachment,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ManageAttachments);
