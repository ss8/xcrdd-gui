import { COMMON_GRID_COLUMN_FIELD_NAMES } from '../../shared/commonGrid/grid-enums';
import { stringArrayComparator } from '../../shared/commonGrid/grid-util';

export default [
  { headerName: '', field: COMMON_GRID_COLUMN_FIELD_NAMES.Action, cellRenderer: 'actionMenuCellRenderer' },
  {
    headerName: 'Name', field: 'name', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'name',
  },
  {
    headerName: 'Description', field: 'description', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'description',
  },
  {
    headerName: 'Size', field: 'size', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'description',
  },
  {
    headerName: 'Owner', field: 'owner.name', filter: 'agTextColumnFilter', sortable: true, resizable: true, tooltipField: 'description',
  },
];
