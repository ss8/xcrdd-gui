import {
  GET_ALL_ATTACHMENTS_FULFILLED,
  GET_ATTACHMENT_BY_ID_FULFILLED,
  CREATE_ATTACHMENT_FULFILLED,
  DELETE_ATTACHMENT_FULFILLED,
  CLEAR_ATTACHMENTS,
  CLEAR_ERRORS,
} from './attachments-actions';

const defaultState = {
  attachments: null,
  attachment: {},
  attachmentContent: {},
  error: { errorMessage: null, errorCode: null, errorFieldName: '' },
};

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED

  if (/ATTACHMENTS.*_REJECTED/.test(action.type)) {
    let errorFieldName = '';
    let errorCode = action.payload.response.status;
    let errorMessage = action.payload.response.statusText;
    if (action.payload.response.data && action.payload.response.data !== '') {
      errorFieldName = JSON.parse(action.payload.response.config.data).name;
      errorCode = action.payload.response.data.error.code;
      errorMessage = action.payload.response.data.error.message;
    }
    return { ...state, error: { errorMessage, errorCode, errorFieldName } };
  }
  switch (action.type) {
    case GET_ALL_ATTACHMENTS_FULFILLED:
      const attachments = action.payload.data.data;
      return { ...state, attachments };
    // case GET_ATTACHMENT_BY_ID_FULFILLED:
    //   const attachment = action.payload.data.data;
    //   return { ...state, attachment };
    // case CREATE_ATTACHMENT_FULFILLED:
    //   return state;
    // case DELETE_ATTACHMENT_FULFILLED:
    //   return state;
    case CLEAR_ATTACHMENTS:
      return {
        attachments: null,
        attachment: {},
        attachmentContent: {},
        error: { errorMessage: null, errorCode: null, errorFieldName: '' },
      };
    // case CLEAR_ERRORS:
    //   return {
    //     ...state,
    //     attachments: [],
    //     attachment: {},
    //     error: { errorMessage: null, errorCode: null, errorFieldName: '' },
    //   };
    default:
      return state;
  }
};
export default moduleReducer;
