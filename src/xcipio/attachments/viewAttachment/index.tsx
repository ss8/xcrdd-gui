import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '@blueprintjs/core';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { showSuccessMessage, showError } from '../../../shared/toaster';

function ViewAttachment(props: any) {
  const name = props.attachmentFile ? props.attachmentFile.name : '';
  const description = props.attachmentFile ? props.attachmentFile.description : '';
  const size = props.attachmentFile ? props.attachmentFile.size : '';
  const owner = props.attachmentFile ? props.attachmentFile.uid : '';

  return (
    <div>
      <ConfigFormPopup
        isOpen={props.isOpen}
        onClose={props.onClose}
        onSubmit={props.onSubmit}
        currentStateMode={props.currentStateMode}
        hasPrivilegesToEdit={false}
        isFirstChildLeftOfEditButton={props.isFirstChildLeftOfEditButton}
        fieldDirty={props.fieldDirty}
        getTitle={props.getTitle}
        width={props.width}
      >
        <p>File Name: {name}</p>
        <p>Description: {description} </p>
        <p>size: {size}</p>
        <p>owner: {owner}</p>
      </ConfigFormPopup>
    </div>
  );
}

export default ViewAttachment;
