import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { findByTestAttr, checkProps } from '../../../shared/__test__/testUtils';
import DeleteAttachmentConfirmPopup from './attachment-delete';

const mockOnClose = jest.fn();
const mockOnSubmit = jest.fn();
const testingFile = 'testingfile';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

// mocked props for ManageAttachments
const defaultProps = {
  isOpen: true,
  onClose: mockOnClose,
  onSubmit: mockOnSubmit,
  selection: { name: testingFile },
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/

test('does not throw warning with expected props', () => {
  checkProps(DeleteAttachmentConfirmPopup, defaultProps);
});

describe('when deleteAttachment confirmation popup is displayed', () => {
  let wrapper: any;

  beforeEach(() => {
    const setupProps = { ...defaultProps };
    wrapper = shallow(<DeleteAttachmentConfirmPopup {...setupProps} />);
  });

  test('DeleteAttachmentConfirmPopup: displayed confirmation message contains delete file name', () => {
    const deleteAttachment = wrapper.dive().dive();
    const confirmMessage = findByTestAttr(deleteAttachment, 'modalDialog-body');
    expect(confirmMessage.text()).toMatch('testingfile');
  });

  test('DeleteAttachmentConfirmPopup: display two buttons', () => {
    const deleteAttachment = wrapper.dive().dive();
    const buttonCancel = findByTestAttr(deleteAttachment, 'modalDialog-button-cancel');
    const buttonSave = findByTestAttr(deleteAttachment, 'modalDialog-button-save');
    expect(buttonCancel.length).toBe(1);
    expect(buttonSave.length).toBe(1);
  });

  test('DeleteAttachmentConfirmPopup: cancel button click onClose handler called', () => {
    const deleteAttachment = wrapper.dive().dive();
    const buttonCancel = findByTestAttr(deleteAttachment, 'modalDialog-button-cancel');
    buttonCancel.simulate('click');
    expect(mockOnClose).toHaveBeenCalledTimes(1);
  });

  test('DeleteAttachmentConfirmPopup: save button click onSubmit handler called', () => {
    const deleteAttachment = wrapper.dive().dive();
    const buttonSave = findByTestAttr(deleteAttachment, 'modalDialog-button-save');
    buttonSave.simulate('click');
    expect(mockOnSubmit).toHaveBeenCalledTimes(1);
  });
});
