import React from 'react';
import ModalDialog from '../../../shared/modal/dialog';

function DeleteAttachmentConfirmPopup(props: any) {
  const name = props.selection ? props.selection.name : '';
  const displayMessage = `Are you sure you want to delete attachment "${name}" ?`;

  return (
    <ModalDialog
      onSubmit={props.onSubmit}
      onClose={props.onClose}
      width="500px"
      {...props}
      actionText="Delete"
      displayMessage={displayMessage}
    />
  );
}

export default DeleteAttachmentConfirmPopup;
