import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { findByTestAttr, checkProps } from '../../shared/__test__/testUtils';
import ManageAttachments from './index';
import ConfigFormPopup from '../../shared/popup/config-form-popup';
import ChangeTracker from '../warrants/forms/warrant-change-tracker';
import { COMMON_GRID_ACTION } from '../../shared/commonGrid/grid-enums';
import AttachmentActionMenu from './attachment-grid-action-menu';
import dummyData from './attachments-dummy.json';

const mockOnClose = jest.fn();
const mockOnSubmit = jest.fn();
const MB = 1048576;
// mock redux store for attachments
export const initialState = {
  warrants: {
    warrant: {}, changeTracker: {},
  },
  attachments: dummyData,
  global: { selectedDF: {} },
  authentication: {},
  discoveryXcipio: {
    discoveryXcipioSettings: { Attachment_Size_Limit: 25 * MB },
  },
};
const mockStore = configureStore([]);

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

// mocked props for ManageAttachments
export const defaultProps = {
  isOpen: true,
  handleClose: mockOnClose,
  handleSubmit: mockOnSubmit,
  history: {
    length: 11,
    action: 'REPLACE',
    location: {
      pathname: '/warrants/create',
      action: 'create',
      cases: [],
      search: '',
      hash: '',
      key: 'yghmcf',
    },
  },
  mode: 'create',
  changeTracker: new ChangeTracker({ idWarrant: 'WART01', idDF: '0' }),
  warrantAttachments: dummyData,
  AttachmentActionMenu,
  authentication: {
    isAuthenticated: true, userId: '', privileges: [], errorStatus: '', beforeLogout: false, passwordUpdate: false, loginMessage: '',
  },
};

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/

test('does not throw warning with expected props', () => {
  checkProps(ManageAttachments, defaultProps);
});

describe('when ManagementAttachments popup is displayed', () => {
  let wrapper: any;
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
    const setupProps = { ...defaultProps, store };
    wrapper = shallow(<ManageAttachments {...setupProps} />);
  });

  test('ManageAttachments component shows an Ok Button', () => {
    const manageAttachment = wrapper.dive().dive();
    const configFormPopup = manageAttachment.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    expect(button.length).toBe(1);
  });

  test('ManageAttachments: simulate Ok Button click, ManageAttachment popup close', () => {
    const manageAttachment = wrapper.dive().dive();
    const configFormPopup = manageAttachment.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    button.simulate('click');
    expect(mockOnClose).toHaveBeenCalledTimes(1);
    expect(manageAttachment.state('isOpen')).toBe(false);
    expect(manageAttachment.state('fieldDirty')).toBe(false);
  });

  test('ManageAttachments has "isUploadFileVisible" variable false ', () => {
    const manageAttachment = wrapper.dive().dive();
    expect(manageAttachment.state('isUploadFileVisible')).toBe(false);
  });

  test('ManageAttachments has "isDeleteAttachmentPopupVisible" variable false ', () => {
    const manageAttachment = wrapper.dive().dive();
    expect(manageAttachment.state('isDeleteAttachmentPopupVisible')).toBe(false);
  });

  test('ManageAttachments render CommonGrid component', () => {
    const commonGrid = findByTestAttr(wrapper.dive().dive(), 'manageAttachment-commonGrid');
    expect(commonGrid.length).toBe(1);
  });

  test('ManageAttachments: when isUploadFileVisible method invoked, upload attachment popup display', () => {
    const manageAttachment = wrapper.dive().dive();
    manageAttachment.instance().handleFileUploadVisible();
    const uploadAttachment = findByTestAttr(manageAttachment,
      'manageAttachment-uploadAttachment');
    expect(manageAttachment.state('isUploadFileVisible')).toBe(true);
    expect(manageAttachment.state('fieldDirty')).toBe(false);
    expect(uploadAttachment.prop('isOpen')).toBe(true);
    expect(uploadAttachment.prop('fieldDirty')).toBe(false);
    const configFormPopup = manageAttachment.find(ConfigFormPopup).dive();
    const okButton = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    expect(okButton.length).toBe(1);
  });

  test('ManageAttachments: from commonGrid table, select an attachment and delete, DeleteAttachmentConfirmPopup display', () => {
    const manageAttachment = wrapper.dive().dive();
    manageAttachment.instance().onActionMenuItemClick({ type: COMMON_GRID_ACTION.Delete });
    expect(manageAttachment.state('isDeleteAttachmentPopupVisible')).toBe(true);
  });

  test('ManageAttachments: simulate Cancel Button click, manage attachment popup close', () => {
    const manageAttachment = wrapper.dive().dive();
    manageAttachment.setState({
      isUploadFileVisible: true,
      fieldDirty: true,
      currentStateMode: 'create',
    });
    const configFormPopup = manageAttachment.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    button.simulate('click');
    expect(mockOnClose).toHaveBeenCalled();
    expect(manageAttachment.state('isOpen')).toBe(false);
    expect(manageAttachment.state('fieldDirty')).toBe(false);
  });

  test('ManageAttachments: simulate Save Button click, manage attachment popup close', () => {
    const manageAttachment = wrapper.dive().dive();
    manageAttachment.setState({
      isUploadFileVisible: true,
      fieldDirty: true,
      currentStateMode: 'create',
    });
    const configFormPopup = manageAttachment.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-Save');
    expect(button.length).toBe(1);
    button.simulate('click');
    expect(mockOnSubmit).toHaveBeenCalled();
    expect(manageAttachment.state('isOpen')).toBe(false);
    expect(manageAttachment.state('fieldDirty')).toBe(false);
    expect(manageAttachment.state('currentStateMode')).toBe(undefined);
    expect(manageAttachment.state('attachmentfiles')).toBe(undefined);
    expect(manageAttachment.state('selectedFile')).toBe(undefined);
  });

  test('CommonGrid component: given props, CommonGrid render proper data', () => {
    const manageAttachment = wrapper.dive().dive();
    manageAttachment.setState({ attachmentfiles: dummyData, selectedFile: dummyData[0] });
    const commonGrid = findByTestAttr(manageAttachment, 'manageAttachment-commonGrid');
    expect(commonGrid.prop('rowData')).toBe(dummyData);
  });
});
