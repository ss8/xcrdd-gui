import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { ManageAttachments } from './index';
import { initialState, defaultProps } from './index.test';
import AttachmentActionMenu from './attachment-grid-action-menu';
import dummyData from './attachments-dummy.json';

const mockStore = configureStore([]);

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

jest.mock('../../shared/commonGrid/index', () => {
  return {
    // must specify this for ES6
    __esModule: true,
    ICommonGridProperties: {},
    // eslint-disable-next-line react/display-name
    default: () => {
      return <div>CommonGrid Mock</div>;
    },
  };
});

describe('when UploadAttachment popup is open', () => {
  let managementAttachmentWrapper: any;
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
    const setupProps = {
      ...defaultProps, store, attachments: { attachments: dummyData },
    };
    managementAttachmentWrapper = mount(<ManageAttachments {...setupProps} />);
    managementAttachmentWrapper.instance().handleFileUploadVisible();
  });

  test('Clicking on Ok button will close the popup', () => {
    const manageAttachment = managementAttachmentWrapper.instance();
    const spy = jest.spyOn(manageAttachment, 'handleUploadFilePopUpClose');
    // this must be done after spy.
    manageAttachment.forceUpdate();
    manageAttachment.handleUploadFilePopUpClose();
    expect(managementAttachmentWrapper.state('isUploadFileVisible')).toBe(false);
    expect(spy).toBeCalledTimes(1);
  });
});

// DO NOT Unmock
// jest.unmock('../../shared/commonGrid/index');

/**
 * These sets of tests use Unconnected ManagementAttachment
 */
describe('when ManagementAttachments popup is displayed', () => {
  let wrapper: any;
  let store;
  let attachmentFiles: any;

  beforeEach(() => {
    store = mockStore(initialState);
    const mockHandleSubmit = jest.fn((attFiles) => {
      attachmentFiles = attFiles;
    });
    const setupProps = {
      ...defaultProps,
      store,
      handleSubmit: mockHandleSubmit,
      setWarrantTracker: jest.fn(),
      warrantAttachments: attachmentFiles,
      AttachmentActionMenu,

    };
    wrapper = shallow(<ManageAttachments {...setupProps} />);
  });

  test('and when new files are added to the list by Upload Popup, the changeTracker has the correct file list', () => {
    const instance = wrapper.instance();
    const addedFiles = [
      {
        name: 'AttachmentName1', id: 'AttachmentID1', file: new File(['text in file'], 'file1.txt'), description: 'this is file 1',
      },
      {
        name: 'AttachmentName2', id: 'AttachmentID2', file: new File(['text in file'], 'file2.txt'), description: 'this is file 1',
      },
    ];
    instance.handleUploadFilePopUpSubmit(addedFiles);
    const tasks = instance.localChangeTracker.getTasks({});

    // Number of POST attachment tasks should be 2
    const filteredTasks = tasks.filter((task: any) => (task.key && task.key.includes('warrant.attachment') > 0 && task.method === 'POST'));
    expect(filteredTasks.length).toEqual(2);

    // The POST attachments should exist in formData.  formData has the location of
    // the files to be uploaded
    instance.handleSubmit();
    expect(attachmentFiles).toBeDefined();
    filteredTasks.forEach((task: any) => {
      const matchedAttachmentInFormData = attachmentFiles.filter(
        (attachment: any) => (attachment.id === task.id),
      );
      expect(matchedAttachmentInFormData.length).toEqual(1);
    });
  });

  test('and when a file is deleted from the attachment list shown in the grid, the changeTracker marks the file as deleted', () => {
    const instance = wrapper.instance();
    instance.setState({
      selectedFile: { id: 'AttachmentID3', name: 'AttachmentName3' },
    });
    instance.handleDeleteAttachmentPopupSubmit();
    const tasks = instance.localChangeTracker.getTasks({});

    // Number of POST attachment tasks should be 1
    const filteredTasks = tasks.filter((task: any) => (task.key && task.key.includes('warrant.attachment.AttachmentID3') > 0
      && task.id === 'AttachmentID3' && task.method === 'DELETE'));
    expect(filteredTasks.length).toEqual(1);
  });
});
