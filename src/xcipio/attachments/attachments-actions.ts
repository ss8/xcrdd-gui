import Http from '../../shared/http';

// const http = Http.init();
const http = Http.getHttp();

// action export constants
export const GET_ALL_ATTACHMENTS = 'GET_ALL_ATTACHMENTS';
export const GET_ATTACHMENT_BY_ID = 'GET_ATTACHMENT_BY_ID';
export const CREATE_ATTACHMENT = 'CREATE_ATTACHMENT';
export const DELETE_ATTACHMENT = 'DELETE_ATTACHMENT';
export const GET_ALL_ATTACHMENTS_FULFILLED = 'GET_ALL_ATTACHMENTS_FULFILLED';
export const GET_ATTACHMENT_BY_ID_FULFILLED = 'GET_ATTACHMENT_BY_ID_FULFILLED';
export const CREATE_ATTACHMENT_FULFILLED = 'CREATE_ATTACHMENT_FULFILLED';
export const DELETE_ATTACHMENT_FULFILLED = 'DELETE_ATTACHMENT_FULFILLED';
export const CLEAR_ATTACHMENTS = 'CLEAR_ATTACHMENTS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const DOWNLOAD_ATTACHMENT = 'DOWNLOAD_ATTACHMENT';
export const DOWNLOAD_ATTACHMENT_FULFILLED = 'DOWNLOAD_ATTACHMENT_FULFILLED';

// action creators
export const getAllAttachments = (dfId: string, wId: string) => {
  return ({ type: GET_ALL_ATTACHMENTS, payload: http.get(`/warrants/${dfId}/${wId}/attachments`) });
};
export const getAttachmentById = (dfId: string, wId: string, aId:string) =>
  ({ type: GET_ATTACHMENT_BY_ID, payload: http.get(`/warrants/${dfId}/${wId}/attachments/${aId}/contents`) });
export const createAttachment = (dfId: string, wId: string, body: any) =>
  ({ type: CREATE_ATTACHMENT, payload: http.post(`/warrants/${dfId}/${wId}/attachments/`, body) });
export const deleteAttachment = (dfId: string, wId: string, aId:string) =>
  ({ type: DELETE_ATTACHMENT, payload: http.delete(`/warrants/${dfId}/${wId}/attachments/${aId}`) });
export const clearAllAttachments = () => ({ type: CLEAR_ATTACHMENTS });
export const downloadAttachment = (dfId: string, wId: string, aId:string) =>
  ({ type: DOWNLOAD_ATTACHMENT, payload: http.get(`/warrants/${dfId}/${wId}/attachments/${aId}/contents`, { responseType: 'blob' }) });
