import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import { COMMON_GRID_ACTION } from '../../shared/commonGrid/grid-enums';

class AttachmentActionMenu extends React.Component<any, any> {
  render() {
    const ctx = this.props.context;
    const menuItemDisabled = !ctx.state.selection.length;

    const showViewActionMenu = (
      (ctx.props.showActionMenuItem === undefined
          && ctx.props.hasPrivilegetoView !== undefined
          && ctx.props.hasPrivilegetoView && this.props.data.newAddedAttachment !== true));
    const showDownloadActionMenu = (
      (ctx.props.showActionMenuItem === undefined
          && ctx.props.hasPrivilegeToDownload !== undefined
          && ctx.props.hasPrivilegeToDownload && this.props.data.newAddedAttachment !== true));
    const showDeleteActionMenu = (
      (ctx.props.showActionMenuItem === undefined
          && ctx.props.hasPrivilegeToDelete !== undefined
          && ctx.props.hasPrivilegeToDelete));

    return (
      <Menu>
        {
          // eslint-disable-next-line max-len
          showViewActionMenu && (
          <MenuItem
            id={COMMON_GRID_ACTION.View}
            onClick={ctx.onActionMenuItemClick}
            disabled={menuItemDisabled}
            text="View"
          />
          )
}
        {
          showDownloadActionMenu && (
            <MenuItem
              id={COMMON_GRID_ACTION.Download}
              onClick={ctx.onActionMenuItemClick}
              disabled={menuItemDisabled}
              text="Download"
            />
          )
}
        {
          showDeleteActionMenu && (
          <MenuItem
            id={COMMON_GRID_ACTION.Delete}
            onClick={ctx.onActionMenuItemClick}
            disabled={menuItemDisabled}
            text="Delete"
          />
          )
}
      </Menu>
    );
  }
}

export default AttachmentActionMenu;
