import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import UploadAttachment from './index';
import { findByTestAttr, checkProps } from '../../../shared/__test__/testUtils';
import ConfigFormPopup from '../../../shared/popup/config-form-popup';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true,
});

const mockOnClose = jest.fn();
const mockOnSubmit = jest.fn();

const defaultProps = {
  isOpen: true,
  warrant: null,
  onClose: mockOnClose,
  onSubmit: mockOnSubmit,
  currentStateMode: 'create',
  fieldDirty: false,
  hasPrivilegesToEdit: true,
  isFirstChildLeftOfEditButton: false,
  getTitle: jest.fn(),
  width: '50%',
};

const selectFiles = {
  0: { name: 'myfile2.docx', size: 11938 },
  1: { name: 'mytestfile1.xlsx', size: 31507 },
  2: { name: 'mytestfile4.xlsx', size: 20004 },
};

const fileListToSubmit = [
  { file: { name: 'myfile2.docx', size: 11938 }, description: 'file 1' },
  { file: { name: 'mytestfile1.xlsx', size: 31507 }, description: 'file 2' },
  { file: { name: 'mytestfile4.xlsx', size: 20004 }, description: 'file 3' },
];

const attachmentsToUpload = [
  { description: '', file: { name: 'myfile2.docx', size: 11938 } },
  { description: '', file: { name: 'mytestfile1.xlsx', size: 31507 } },
  { description: '', file: { name: 'mytestfile4.xlsx', size: 20004 } },
];

/**
* Factory function to create a ShallowWrapper for the GuessedWords component.
* @function setup
* @param {object} props - Component props specific to this setup.
* @returns {ShallowWrapper}
*/
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<UploadAttachment {...setupProps} />);
};

test('does not throw warning with expected props', () => {
  checkProps(UploadAttachment, defaultProps);
});

describe('when UploadAttachment popup is displayed', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = setup({});
  });

  test('UploadAttachment component shows a Choose Files Button', () => {
    const button = findByTestAttr(wrapper, 'uploadAttachment-input');
    expect(button.length).toBe(1);
    expect(wrapper.state('attachmentsToUpload').length).toBe(0);
    expect(wrapper.state('fieldDirty')).toBe(false);
  });

  test('UploadAttachment component shows a Ok Button', () => {
    const configFormPopup = wrapper.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    expect(button.length).toBe(1);
  });

  test('UploadAttachment component: simulate ‘Choose File’ button click; UploadAttachment shows the selected files', () => {
    const uploadAttachment = wrapper;
    expect(wrapper.state('attachmentsToUpload').length).toBe(0);
    expect(uploadAttachment.state('fieldDirty')).toBe(false);
    const itemBeforeClick = findByTestAttr(wrapper, 'uploadAttachment-input-');
    expect(itemBeforeClick.length).toBe(0);
    const input = findByTestAttr(wrapper, 'uploadAttachment-input');
    input.simulate('change', { target: { files: selectFiles } });
    expect(uploadAttachment.state('attachmentsToUpload')).toStrictEqual(attachmentsToUpload);
    expect(uploadAttachment.state('fieldDirty')).toBe(true);
    attachmentsToUpload.forEach((attachment) => {
      const item = findByTestAttr(wrapper, `uploadAttachment-input-${attachment.file.name}`);
      expect(item.length).toBe(1);
    });
    const configFormPopup = wrapper.find(ConfigFormPopup).dive();
    const cancelButton = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    const saveButton = findByTestAttr(configFormPopup, 'configFormPopup-Save');
    expect(cancelButton.length).toBe(1);
    expect(saveButton.length).toBe(1);
  });

  test('UploadAttachment component: simulate Ok Button click', () => {
    const configFormPopup = wrapper.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    button.simulate('click');
    expect(mockOnClose).toHaveBeenCalledTimes(1);
    expect(wrapper.state('attachmentsToUpload').length).toBe(0);
    expect(wrapper.state('fieldDirty')).toBe(false);
  });

  test('UploadAttachment component: simulate Cancel button click', () => {
    wrapper.setState({ attachmentsToUpload: fileListToSubmit, fieldDirty: true });
    const configFormPopup = wrapper.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-CancelOrOk');
    expect(button.length).toBe(1);
    button.simulate('click');
    expect(wrapper.state('attachmentsToUpload').length).toBe(0);
    expect(wrapper.state('fieldDirty')).toBe(false);
    expect(mockOnClose).toHaveBeenCalled();
  });

  test('UploadAttachment component: simulate Save button click', () => {
    wrapper.setState({ attachmentsToUpload: fileListToSubmit, fieldDirty: true });
    const configFormPopup = wrapper.find(ConfigFormPopup).dive();
    const button = findByTestAttr(configFormPopup, 'configFormPopup-Save');
    expect(button.length).toBe(1);
    button.simulate('click');
    expect(wrapper.state('attachmentsToUpload').length).toBe(0);
    expect(wrapper.state('fieldDirty')).toBe(false);
    expect(mockOnSubmit).toHaveBeenCalled();
  });
});
