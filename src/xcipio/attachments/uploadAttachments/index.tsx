import React from 'react';
import ConfigFormPopup from '../../../shared/popup/config-form-popup';
import DynamicForm, { ChangedField } from '../../../shared/form';
import uploadAttachmentField from './upload-attachment-form.json';
import { showError } from '../../../shared/toaster';

class UploadAttachment extends React.Component<any, any> {
  formRef:any;

  constructor(props: any) {
    super(props);
    this.state = {
      attachmentsToUpload: [],
      fieldDirty: false,
    };
  }

  onChangeFileSelectHandler = (event:any) => {
    const selectedFiles = event.target.files ? event.target.files : {};
    const attachmentsToUpload:any[] = [];
    Object.keys(selectedFiles).forEach((key: any) => {
      const file = selectedFiles[key];
      if (file.size > this.props.limit) {
        showError(`File size exceeded - Cannot add ${file.name}`, 3000);
      } else {
        attachmentsToUpload.push({
          file,
          description: '',
        });
      }
    });
    this.setState({ attachmentsToUpload, fieldDirty: true });
  }

  handleFileUploadClose = () => {
    this.props.onClose();
    this.setState({ attachmentsToUpload: [], fieldDirty: false });
  }

  handleFileUploadSubmit = () => {
    this.props.onSubmit(this.state.attachmentsToUpload);
    this.setState({ attachmentsToUpload: [], fieldDirty: false });
  }

  handleChange = (attachment:any) => (changedField: ChangedField) => {
    const { value } = changedField;
    const attachmentsToUpload = [...this.state.attachmentsToUpload];
    const fileName = attachment.file.name;
    Object.keys(attachmentsToUpload).forEach((key: any) => {
      if (attachmentsToUpload[key].file.name === fileName) {
        attachmentsToUpload[key].description = value;
      }
    });
    this.setState({ attachmentsToUpload });
  }

  render() {
    const uploadfile = (
      <div>
        <input
          type="file"
          id="input"
          data-test="uploadAttachment-input"
          multiple
          onChange={this.onChangeFileSelectHandler}
        />

        { this.state.attachmentsToUpload.map((attachment:any) => {
          const data = {
            name: attachment.file.name,
            size: attachment.file.size,
            description: attachment.description,
          };
          return (
            <div key={attachment.file.name} data-test={`uploadAttachment-input-${attachment.file.name}`}>
              <DynamicForm
                ref={(ref:any) => { this.formRef = ref; }}
                name=""
                fields={uploadAttachmentField.forms.caseNotes.fields}
                layout={uploadAttachmentField.forms.caseNotes.metadata.layout.rows}
                defaults={data}
                fieldUpdateHandler={this.handleChange(attachment)}
              />
            </div>
          );
        })}
      </div>
    );
    return (
      <div>
        <ConfigFormPopup
          data-test="uploadAttachment-configFormPopup"
          isOpen={this.props.isOpen}
          onClose={this.handleFileUploadClose}
          onSubmit={this.handleFileUploadSubmit}
          currentStateMode={this.props.currentStateMode}
          hasPrivilegesToEdit={this.props.hasPrivilegesToEdit}
          isFirstChildLeftOfEditButton={this.props.isFirstChildLeftOfEditButton}
          doNotDisplayEdit
          fieldDirty={this.state.fieldDirty}
          getTitle={this.props.getTitle}
          width={this.props.width}
        >
          {uploadfile}
        </ConfigFormPopup>
      </div>
    );
  }
}

export default UploadAttachment;
