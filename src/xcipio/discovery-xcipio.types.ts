import { Deployment } from 'data/types';

export type DiscoveryXcipioData = {
  appSettings: {
    expiredPopulatedForEndDate?: string
    id: string
    appName: string
    dateUpdated: string
    name: string
    type: 'string' | 'number' | 'boolean'
    setting: string
  }[]
  appProperties: unknown[]
}

export interface DiscoveryXcipioState {
  discoveryXcipioSettings: DiscoveryXcipioSettings
}

/* eslint-disable camelcase */
export type DiscoveryXcipioSettings = {
  Warrant_bSelectAllHI3Interfaces?: boolean
  Attachment_Size_Limit?: number
  Warrant_bAttachments_enabled?: boolean
  isBulkActionsEnabled?: boolean
  isAdditionalWarrantStateEnabled?: boolean
  isWarrantResumeActionEnabled?: boolean
  isWarrantPauseActionEnabled?: boolean
  Warrant_bArchiveWarrant?: boolean
  isAuditEnabled?: boolean
  isExternalDashboardEnabled?: boolean
  isSecurityGatewayEnabled?: boolean
  externalDashboardIP?: string
  logiOAuthClientId?: string
  logiOAuthClientName?: string
  logiAdminAccount?: string
  logiAdminUserName?: string
  logiAdminPswd?: string
  logiSupervisorUserName?: string
  logiSupervisorPswd?: string
  language?: string
  deployment?: Deployment
  Case_bArchiveCase?: boolean
  isWarrantDefaultExpireEnabled?: boolean
  warrantDefaultExpireDays?: string
  isAlertNotificationEnabled?: boolean
  alertNotificationRowNumbers?: string
  alertNotificationPollingTime?: string
  isAlertNotificationNewIPEnabled?: boolean
  isAlertNotificationBufferingEnabled?: boolean
  isAlertNotificationAuditDiscrepancyEnabled?: boolean
  isAlertNotificationInterceptDynamicIPLookupEnabled?: boolean
  shouldAutomaticallyGenerateCaseName?: boolean
}
