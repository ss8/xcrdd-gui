import { RootState } from 'data/root.reducers';
import { Deployment } from 'data/types';

export const getDiscoveryDeployment = (state: RootState): Deployment =>
  state.discoveryXcipio.discoveryXcipioSettings.deployment ?? 'andromeda';
export const getDiscoverySettingLanguage = (state: RootState): string =>
  state.discoveryXcipio.discoveryXcipioSettings.language ?? '';
export const isDiscoverySettingAuditEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAuditEnabled ?? false;
export const shouldAutomaticallyGenerateCaseName = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.shouldAutomaticallyGenerateCaseName ?? false;
export const shouldSelectAllHi3Interfaces = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.Warrant_bSelectAllHI3Interfaces ?? false;
export const isAttachmentEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.Warrant_bAttachments_enabled ?? false;
export const isWarrantDefaultExpireEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isWarrantDefaultExpireEnabled ?? false;
export const getWarrantDefaultExpireDays = (state: RootState): string =>
  state.discoveryXcipio.discoveryXcipioSettings.warrantDefaultExpireDays ?? '';
export const isAlertNotificationEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAlertNotificationEnabled ?? false;
export const getAlertNotificationRowNumbers = (state: RootState): string =>
  state.discoveryXcipio.discoveryXcipioSettings.alertNotificationRowNumbers ?? '';
export const getAlertNotificationPollingTime = (state: RootState): string =>
  state.discoveryXcipio.discoveryXcipioSettings.alertNotificationPollingTime ?? '';
export const isAlertNotificationNewIPEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAlertNotificationNewIPEnabled ?? false;
export const isAlertNotificationBufferingEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAlertNotificationBufferingEnabled ?? false;
export const isAlertNotificationAuditDiscrepancyEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAlertNotificationAuditDiscrepancyEnabled ?? false;
export const isAlertNotificationInterceptDynamicIPLookupEnabled = (state: RootState): boolean =>
  state.discoveryXcipio.discoveryXcipioSettings.isAlertNotificationInterceptDynamicIPLookupEnabled ?? false;
