import React, { Fragment } from 'react';
import WarrantsModule from '../warrants';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <div>
      <p className="notsupport-text">Feature will soon be available</p>
    </div>
    <WarrantsModule />
  </Fragment>
);

export default ModuleRoutes;
