import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  HTMLSelect, Navbar, NavbarHeading,
  Classes, NavbarDivider, Alignment,
  NavbarGroup, Button, Menu, MenuItem,
} from '@blueprintjs/core';
import {
  getAllIntercepts,
  getInterceptById,
  createIntercept,
  updateIntercept,
  patchIntercept,
  deleteIntercept,
} from './intercept-actions';
import ModuleRoutes from './intercept-routes';
import './intercept.scss';

class Intercepts extends React.Component <any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      selected: '',
    };
  }

  handleClick = (e:any) => {
    this.props.history.push('/warrants');
  }

  render() {
    return (
      <div>
        <ModuleRoutes {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  intercept: state.intercept,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllIntercepts,
  getInterceptById,
  createIntercept,
  updateIntercept,
  patchIntercept,
  deleteIntercept,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Intercepts));
