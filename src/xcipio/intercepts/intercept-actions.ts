import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_INTERCEPTS = 'GET_ALL_INTERCEPTS';
export const GET_INTERCEPT_BY_ID = 'GET_INTERCEPT_BY_ID';
export const UPDATE_INTERCEPT = 'UPDATE_INTERCEPT';
export const PATCH_INTERCEPT = 'PATCH_INTERCEPT';
export const DELETE_INTERCEPT = 'DELETE_INTERCEPT';
export const CREATE_INTERCEPT = 'CREATE_INTERCEPT';
export const GET_ALL_INTERCEPTS_FULFILLED = 'GET_ALL_INTERCEPTS_FULFILLED';
export const GET_INTERCEPT_BY_ID_FULFILLED = 'GET_INTERCEPT_BY_ID_FULFILLED';
export const UPDATE_INTERCEPT_FULFILLED = 'UPDATE_INTERCEPT_FULFILLED';
export const PATCH_INTERCEPT_FULFILLED = 'PATCH_INTERCEPT_FULFILLED';
export const DELETE_INTERCEPT_FULFILLED = 'DELETE_INTERCEPT_FULFILLED';
export const CREATE_INTERCEPT_FULFILLED = 'CREATE_INTERCEPT_FULFILLED';

// action creators
export const getAllIntercepts = () => ({ type: GET_ALL_INTERCEPTS, payload: http.get('/surveillance') });
export const getInterceptById = (id: number) => ({ type: GET_INTERCEPT_BY_ID, payload: http.get(`/intercepts/${id}`) });
export const createIntercept = (body: any) => ({ type: CREATE_INTERCEPT, payload: http.post('/intercepts', body) });
export const updateIntercept = (id: number, body: any) => ({ type: UPDATE_INTERCEPT, payload: http.put(`/intercepts/${id}`, body) });
export const patchIntercept = (id: number, body: any) => ({ type: PATCH_INTERCEPT, payload: http.patch(`/intercepts/${id}`, body) });
export const deleteIntercept = (id: number) => ({ type: DELETE_INTERCEPT, payload: http.delete(`/intercepts/${id}`) });
