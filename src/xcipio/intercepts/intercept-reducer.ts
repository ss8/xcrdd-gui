import {
  GET_ALL_INTERCEPTS_FULFILLED,
} from './intercept-actions';

const defaultState = { intercepts: [] };

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_INTERCEPTS_FULFILLED:
      return { ...state, intercepts: action.payload.data.data };
    default:
      return state;
  }
};
export default moduleReducer;
