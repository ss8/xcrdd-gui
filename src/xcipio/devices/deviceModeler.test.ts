import DeviceModeler from './deviceModeler';
import {
  CREATING_WARRANT,
  CASES_LTE_4G_DATA,
  EXPECTED_MODEL_FOR_LTE_4G_DATA,
  CASES_FOR_GSMCDMA_2G_VOICE,
  EXPECTED_MODEL_FOR_GSMCDMA_2G_VOICE,
  CASES_FOR_LTE_GIZMO,
  EXPECTED_MODEL_FOR_LTE_GIZMO,
  CASES_FOR_LTE_IOT,
  EXPECTED_MODEL_FOR_LTE_IOT,
  CASES_FOR_PTT,
  EXPECTED_MODEL_FOR_PTT,
  CASES_FOR_UMTS_3G_VOICE,
  EXPECTED_MODEL_FOR_UMTS_3G_VOICE,
  CASES_FOR_VOLTE_4G_VOICE,
  EXPECTED_MODEL_FOR_VOLTE_4G_VOICE,
  CASES_FOR_VOLTE_RCS,
  EXPECTED_MODEL_FOR_VOLTE_RCS,
  CASES_FOR_VZ_VISIBLE,
  EXPECTED_MODEL_FOR_VZ_VISIBLE,
  CUSTOMER_TARGETS,
  CASES_FOR_3G_PD,
  EXPECTED_MODEL_FOR_3G_PD,
  CASES_FOR_4G_PD_AND_ETSI_GIZMO,
  EXPECTED_MODEL_FOR_4G_PD_AND_ETSI_GIZMO,
} from './forms/__test__/deviceModeler-data-mock';

const buildDeviceModelerBasedOnCases = (
  warrantCases: any[],
  creatingWarrant: any,
  customerTargets: any,
) => {
  const deviceModeler = new DeviceModeler(customerTargets);
  deviceModeler.setWarrantData(creatingWarrant);
  warrantCases.forEach((wc: any) => {
    const warrantCase: any = wc;
    deviceModeler.setCaseData(warrantCase);
    warrantCase.targets.forEach((target: any) => {
      deviceModeler.setTargetData(target);
    });
  });

  return deviceModeler.buildModel();
};

describe('DeviceModeler', () => {
  let testMocks: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.warrantJson = {
      id: 'warrant_id1',
    };
    testMocks.caseJson = {
      id: 'case_id1',
      name: 'case_1',
    };
    testMocks.caseJsonUpdated = {
      id: 'case_id1',
      name: 'case_1_updated',
    };

    testMocks.warrant = {
      id: 'warrant1',
      name: 'warrant1',
      lea: 'Mg',
    };

    testMocks.caseTarget = { id: 'DX-CASE-1', name: 'Case1' };

    testMocks.target = {
      dxOwner: '',
      dxAccess: '',
      name: '',
      afs: {
        afIds: [],
        afGroups: [],
        afAutowireTypes: [
          { id: 'MVNR_SBC', name: 'MVNR_SBC' },
          { id: 'MVNR_CSCF', name: 'MVNR_CSCF' },
        ],
        afAutowireGroups: [],
        afccc: [],
      },
      primaryType: 'MSISDN',
      primaryValue: '14085551212',
      secondaryType: '',
      secondaryValue: '',
      options: [],
      id: 'DX-TARGET-1',
      isRequired: true,
      caseId: { id: 'DX-CASE-1', name: 'Case1' },
    };

    testMocks.targetMappServices = ['VZ-VISIBLE'];

    testMocks.targetMappServices2 = ['CASEIDMDN'];

    testMocks.customerTargets = {
      CASEIDMDN: {
        'GSMCDMA-2G-VOICE': {
          pattern: '{MDN}',
          parent: 'MDN',
          target: 'CASEID',
        },
      },
      CASEIDMIN: {
        'GSMCDMA-2G-VOICE': {
          pattern: '{MIN}',
          parent: 'MIN',
          target: 'CASEID',
        },
      },
      'SIP-URI': { '*': { pattern: 'sip:+1{MDN}@vzims.com', parent: 'MDN' } },
      'TEL-URI': { '*': { pattern: 'tel:+1{MDN}', parent: 'MDN' } },
      MSISDN: { '*': { pattern: '1{MDN}', parent: 'MDN' } },
    };

    testMocks.altMappings = {
      0: { id: 0 },
      1: { id: 1 },
    };
  });

  describe('setWarrantData()', () => {
    it('should set warrant and idWarrant from a warrantJson from type object', () => {
      const deviceModeler = new DeviceModeler({});
      deviceModeler.setWarrantData(testMocks.warrantJson);
      expect(deviceModeler.warrant).toEqual(testMocks.warrantJson);
      expect(deviceModeler.idWarrant).toBe(testMocks.warrantJson.id);
    });
  });

  describe('setCaseData()', () => {
    it('should update the deviceModeler case object according the caseJson id passed (object version)  ', () => {
      const deviceModeler = new DeviceModeler({});
      deviceModeler.setCaseData(testMocks.caseJson);
      expect(deviceModeler.cases).toEqual({ case_id1: testMocks.caseJson });
    });

    it('should update a existing deviceMOduler case object', () => {
      const deviceModeler = new DeviceModeler({});
      // set the new Case
      deviceModeler.setCaseData(testMocks.caseJson);

      // update the existing case

      deviceModeler.setCaseData(testMocks.caseJsonUpdated);
      expect(deviceModeler.cases).toEqual({
        case_id1: testMocks.caseJsonUpdated,
      });
    });

    it('should not update the deviceModeler case object when passed caseJson has undefined id', () => {
      const deviceModeler = new DeviceModeler({});
      testMocks.caseJson.id = null;
      deviceModeler.setCaseData(testMocks.caseJson);
      expect(deviceModeler.cases).toEqual({});
    });
  });

  describe('setTargetData()', () => {
    describe('when target  CASE is also present in and deviceModeler instance', () => {
      it('should target be inserted in deviceModeler targets ', () => {
        const deviceModeler = new DeviceModeler({});
        // target  contains this case
        deviceModeler.setCaseData(testMocks.caseTarget);

        deviceModeler.setTargetData(testMocks.target);
        expect(deviceModeler.targets).toEqual({
          [testMocks.target.id]: testMocks.target,
        });
      });
    });

    describe('when target  CASE is also present in and deviceModeler instance', () => {
      it('should target be inserted in deviceModeler targets ', () => {
        const deviceModeler = new DeviceModeler({});
        // target doesnt contains this case
        deviceModeler.setCaseData(testMocks.caseJson);
        deviceModeler.setTargetData(testMocks.target);
        expect(deviceModeler.targets).toEqual({});
      });
    });
  });

  describe('getTargetDirectMapping()', () => {
    describe('When deviceModeler customerTarget contains a target service ', () => {
      it('should return a mapping according the target primaryType', () => {
        const deviceModeler = new DeviceModeler(testMocks.customerTargets);

        const mapping = deviceModeler.getTargetDirectMapping(
          testMocks.target,
          'primaryType',
          testMocks.targetMappServices,
        );
        expect(mapping).toEqual(
          testMocks.customerTargets[testMocks.target.primaryType],
        );
      });
    });
  });

  describe('isETSIGizmoTarget()', () => {
    it('should return true if target is associated with ETSI_Gizmo', () => {
      const target = {
        id: 'Ui1HNC1HaXptbyMz',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [],
          afccc: [],
          afGroups: [],
          afAutowireTypes: [
            { id: 'ETSI_GIZMO', name: 'ETSI_GIZMO', tag: 'ETSI_GIZMO' },
          ],
          afAutowireGroups: [],
        },
        caseId: { id: 'Ui1HNC1HaXptbw', name: 'R-G4-Gizmo' },
        options: [
          { key: 'SERVICETYPE', value: 'voice' },
        ],
        primaryType: 'MDN',
        primaryValue: '4333333333',
        secondaryType: '',
        secondaryValue: '',
      };

      expect(DeviceModeler.isETSIGizmoTarget(target)).toBe(true);
    });

    it('should return false if target is not associated with ETSI_Gizmo', () => {
      const target = {
        id: 'Ui1HNC1HaXptbyMy',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [],
          afccc: [],
          afGroups: [],
          afAutowireTypes: [
            { id: 'ALU_HLR', name: 'ALU_HLR', tag: 'ALU_HLR' },
            { id: 'ALU_MME', name: 'ALU_MME', tag: 'ALU_MME' },
            { id: 'CISCOVPCRF', name: 'CISCOVPCRF', tag: 'CISCOVPCRF' },
            { id: 'ERK_SMF', name: 'ERK_SMF', tag: 'ERK_SMF' },
            { id: 'ERK_VMME', name: 'ERK_VMME', tag: 'ERK_VMME' },
            { id: 'ETSI103221', name: 'ETSI103221', tag: 'ETSI103221' },
            { id: 'HP_AAA', name: 'HP_AAA', tag: 'HP_AAA' },
            { id: 'MV_SMSIWF', name: 'MV_SMSIWF', tag: 'MV_SMSIWF' },
            { id: 'NSNHLR_SDM_PRGW', name: 'NSNHLR_SDM_PRGW', tag: 'NSNHLR_SDM_PRGW' },
            { id: 'NSN_SAEGW', name: 'NSN_SAEGW', tag: 'NSN_SAEGW' },
            { id: 'PCRF', name: 'PCRF', tag: 'PCRF' },
            { id: 'STA_GGSN', name: 'STA_GGSN', tag: 'STA_GGSN' },
          ],
          afAutowireGroups: [],
        },
        caseId: { id: 'Ui1HNC1HaXptbw', name: 'R-G4-Gizmo' },
        options: [],
        primaryType: 'IMSI',
        primaryValue: '422222222222222',
        secondaryType: '',
        secondaryValue: '',
      };

      expect(DeviceModeler.isETSIGizmoTarget(target)).toBe(false);
    });
  });

  describe('adaptTarget()', () => {
    it('should adapt the target when MDN and associated with ETSI_Gizmo', () => {
      const target = {
        id: 'Ui1HNC1HaXptbyMz',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [],
          afccc: [],
          afGroups: [],
          afAutowireTypes: [
            { id: 'ETSI_GIZMO', name: 'ETSI_GIZMO', tag: 'ETSI_GIZMO' },
          ],
          afAutowireGroups: [],
        },
        caseId: { id: 'Ui1HNC1HaXptbw', name: 'R-G4-Gizmo' },
        options: [
          { key: 'SERVICETYPE', value: 'voice' },
        ],
        primaryType: 'MDN',
        primaryValue: '4333333333',
        secondaryType: '',
        secondaryValue: '',
      };

      expect(DeviceModeler.adaptTarget(target)).toEqual({
        ...target,
        primaryType: 'MDNwService',
      });
    });

    it('should do nothing to target otherwise', () => {
      const target = {
        id: 'Ui1HNC1HaXptbyMy',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [],
          afccc: [],
          afGroups: [],
          afAutowireTypes: [
            { id: 'ALU_HLR', name: 'ALU_HLR', tag: 'ALU_HLR' },
            { id: 'ALU_MME', name: 'ALU_MME', tag: 'ALU_MME' },
            { id: 'CISCOVPCRF', name: 'CISCOVPCRF', tag: 'CISCOVPCRF' },
            { id: 'ERK_SMF', name: 'ERK_SMF', tag: 'ERK_SMF' },
            { id: 'ERK_VMME', name: 'ERK_VMME', tag: 'ERK_VMME' },
            { id: 'ETSI103221', name: 'ETSI103221', tag: 'ETSI103221' },
            { id: 'HP_AAA', name: 'HP_AAA', tag: 'HP_AAA' },
            { id: 'MV_SMSIWF', name: 'MV_SMSIWF', tag: 'MV_SMSIWF' },
            { id: 'NSNHLR_SDM_PRGW', name: 'NSNHLR_SDM_PRGW', tag: 'NSNHLR_SDM_PRGW' },
            { id: 'NSN_SAEGW', name: 'NSN_SAEGW', tag: 'NSN_SAEGW' },
            { id: 'PCRF', name: 'PCRF', tag: 'PCRF' },
            { id: 'STA_GGSN', name: 'STA_GGSN', tag: 'STA_GGSN' },
          ],
          afAutowireGroups: [],
        },
        caseId: { id: 'Ui1HNC1HaXptbw', name: 'R-G4-Gizmo' },
        options: [],
        primaryType: 'IMSI',
        primaryValue: '422222222222222',
        secondaryType: '',
        secondaryValue: '',
      };

      expect(DeviceModeler.adaptTarget(target)).toEqual({
        ...target,
      });
    });
  });

  describe('when: cases are using CASES_LTE_4G_DATA', () => {
    it('should create a device modeler model to LTE-4G-DATA', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_LTE_4G_DATA,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_LTE_4G_DATA);
    });
  });

  describe('when: cases are using CASES_FOR_LTE_GIZMO', () => {
    it('should create a device modeler model to LTE_GIZMO', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_LTE_GIZMO,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_LTE_GIZMO);
    });
  });

  describe('when: cases are using CASES_FOR_VZ_VISIBLE', () => {
    it('should create a device modeler model to VZ_VISIBLE', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_VZ_VISIBLE,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_VZ_VISIBLE);
    });
  });

  describe('when: cases are using CASES_FOR_GSMCDMA_2G_VOICE', () => {
    it('should create a device modeler model to GSMCDMA_2G_VOICE', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_GSMCDMA_2G_VOICE,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_GSMCDMA_2G_VOICE);
    });
  });

  describe('when: cases are using CASES_FOR_LTE_IOT', () => {
    it('should create a device modeler model to LTE_IOT', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_LTE_IOT,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_LTE_IOT);
    });
  });

  describe('when: cases are using CASES_FOR_UMTS_3G_VOICE', () => {
    it('should create a device modeler model to UMTS_3G_VOICE', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_UMTS_3G_VOICE,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_UMTS_3G_VOICE);
    });
  });

  describe('when: cases are using CASES_FOR_VOLTE_4G_VOICE', () => {
    it('should create a device modeler model to LTE-4G-DATA', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_VOLTE_4G_VOICE,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_VOLTE_4G_VOICE);
    });
  });

  describe('when: cases are using CASES_FOR_VOLTE_RCS', () => {
    it('should create a device modeler model to VOLTE_RCS', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_VOLTE_RCS,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_VOLTE_RCS);
    });
  });

  describe('when: cases are using CASES_FOR_PTT', () => {
    it('should create a device modeler model to PTT', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_PTT,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_PTT);
    });
  });

  describe('when: cases are using CASES_FOR_3G_PD', () => {
    it('should create a device modeler model to 3G_PD', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_3G_PD,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_3G_PD);
    });
  });

  describe('when cases are using CASES_FOR_3G_PD', () => {
    it('should create a device modeler model to 3G_PD', () => {
      const deviceModel = buildDeviceModelerBasedOnCases(
        CASES_FOR_4G_PD_AND_ETSI_GIZMO,
        CREATING_WARRANT,
        CUSTOMER_TARGETS,
      );
      expect(deviceModel).toEqual(EXPECTED_MODEL_FOR_4G_PD_AND_ETSI_GIZMO);
    });
  });
});
