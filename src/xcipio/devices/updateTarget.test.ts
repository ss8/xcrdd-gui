import {
  nortelMtxSamsungMSCDeviceModel,
  samsungMSCDeviceModel,
  nortelMtxDeviceModel,
  noNortelMtxAndSamsungMSCDeviceModel,
  caseIDNotMatchCASEIDMDNOrCASEIDMINDeviceModel,
  alreadyHasCASEIDMINAndCASEIDMDNDeviceModel,
  extraCaseIDDeviceModel,
} from '__test__/mockDeviceModel';
import cloneDeep from 'lodash.clonedeep';
import { TargetModelAccessFunctions } from 'data/target/target.types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { updateCaseIdTarget, afOntologyHasTarget } from './updateTarget';

describe('updateTarget', () => {
  let warrantConfigInstance: WarrantConfiguration;
  let noAFs: TargetModelAccessFunctions;
  beforeEach(() => {
    warrantConfigInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: jest.fn(),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    noAFs = {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    };
  });

  describe('updateCaseIdTarget', () => {
    it('return device with CASEIDMIN and CASEIDMDN when device has CASEID that matches the MIN/MDN values and MIN/MDN are associated to SAMSUNG MSC and NORTEL MTX', () => {
      const deviceModel = cloneDeep(nortelMtxSamsungMSCDeviceModel);
      const expectedCASEIDMDN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMDN.target.primaryType = 'CASEIDMDN';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[2]);
      const expectedMIN = cloneDeep(deviceModel.types.MIN);
      const expectedMSISDN = cloneDeep(deviceModel.types.MSISDN);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEIDMDN).toEqual([expectedCASEIDMDN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual(expectedMIN);
      expect(deviceModel.types.MSISDN).toEqual(expectedMSISDN);
    });

    it('return device with CASEIDMIN and CASEIDMDN when device has CASEID that matches the MDN values and MSISDN is associated to SAMSUNG MSC ', () => {
      const deviceModel = cloneDeep(samsungMSCDeviceModel);
      const expectedCASEIDMDN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMDN.target.primaryType = 'CASEIDMDN';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[2]);

      const expectedMIN = {
        target: {
          id: null,
          caseId: { ...expectedCASEIDMIN.target.caseId },
          options: [],
          primaryType: 'MIN',
          primaryValue: expectedCASEIDMIN.target.primaryValue,
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      };

      const expectedMSISDN = cloneDeep(deviceModel.types.MSISDN);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toEqual([expectedCASEIDMDN]);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual([expectedMIN]);
      expect(deviceModel.types.MSISDN).toEqual(expectedMSISDN);
    });

    it('return device with CASEIDMIN when device has CASEID that matches the MIN values and MIN is associated to NORTEL MTX ', () => {
      const deviceModel = cloneDeep(nortelMtxDeviceModel);

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[1]);
      const expectedMIN = cloneDeep(deviceModel.types.MIN);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual(expectedMIN);
    });

    it('return device without CASEIDMIN and CASEIDMDN when device has CASEID that matches the MIN/MDN values but MSISDN is NOT associated to SAMSUNG MSC and MIN is not associated to NORTEL MTX', () => {
      const deviceModel = cloneDeep(noNortelMtxAndSamsungMSCDeviceModel);

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[0]);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEIDMIN).toBeUndefined();
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
    });

    it('return device with CASEIDMIN when device has CASEID and MIN is associated to NORTEL MTX, but the CASEID does not associate to NORTEL MTX', () => {
      const deviceModel = cloneDeep(nortelMtxDeviceModel);
      // remove Nortel from CASEID
      deviceModel.types.CASEID[0].target.afs = noAFs;

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[1]);
      const expectedMIN = cloneDeep(deviceModel.types.MIN);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual(expectedMIN);
    });

    it('return device with CASEIDMIN when device has CASEID and MSISDN is associated to SAMSUNG MSC, but the CASEID does not associate to SAMSUNG MSC', () => {
      const deviceModel = cloneDeep(samsungMSCDeviceModel);
      // remove Samsung MSC from CASEID - CASEIDMDN
      deviceModel.types.CASEID[0].target.afs = noAFs;
      // remove Samsung MSC from CASEID - CASEIDMIN
      deviceModel.types.CASEID[1].target.afs = noAFs;

      const expectedCASEIDMDN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMDN.target.primaryType = 'CASEIDMDN';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[2]);

      const expectedMIN = {
        target: {
          id: null,
          caseId: { ...expectedCASEIDMIN.target.caseId },
          options: [],
          primaryType: 'MIN',
          primaryValue: expectedCASEIDMIN.target.primaryValue,
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      };

      const expectedMSISDN = cloneDeep(deviceModel.types.MSISDN);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toEqual([expectedCASEIDMDN]);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual([expectedMIN]);
      expect(deviceModel.types.MSISDN).toEqual(expectedMSISDN);
    });

    it('return device without CASEIDMIN and CASEIDMDN when device has CASEID that matches the MSISDN values but MSISDN is NOT associated to SAMSUNG MSC ', () => {
      const deviceModel = cloneDeep(samsungMSCDeviceModel);
      deviceModel.types.CASEID[0].target.afs = noAFs;
      deviceModel.types.CASEID[1].target.afs = noAFs;

      const expectedCASEIDMDN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMDN.target.primaryType = 'CASEIDMDN';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[2]);

      const expectedMIN = {
        target: {
          id: null,
          caseId: { ...expectedCASEIDMIN.target.caseId },
          options: [],
          primaryType: 'MIN',
          primaryValue: expectedCASEIDMIN.target.primaryValue,
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      };

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toEqual([expectedCASEIDMDN]);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual([expectedMIN]);
    });

    it('return device without CASEIDMIN when device has CASEID that matches the MIN values but MIN is NOT associated to NORTEL MTX ', () => {
      const deviceModel = cloneDeep(nortelMtxDeviceModel);
      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[0]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[1]);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
    });

    it('return device without CASEIDMIN and CASEIDMDN when device does not have CASEID', () => {
      const deviceModel = cloneDeep(nortelMtxSamsungMSCDeviceModel);
      deviceModel.types.CASEID = [];

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMIN).toBeUndefined();
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEID).toEqual([]);

      deviceModel.types = {
        MSISDN: deviceModel.types.MSISDN,
        MIN: deviceModel.types.MIN,
        MDN: deviceModel.types.MDN,
      };

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMIN).toBeUndefined();
      expect(deviceModel.types.CASEIDMDN).toBeUndefined();
      expect(deviceModel.types.CASEID).toBeUndefined();
    });

    it('return device without CASEIDMIN when SAMSUNG MSC has no CASEIDMIN in Ontology definition', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = ['MSISDN', 'CASEIDMDN'];
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = [];

      const deviceModel = cloneDeep(samsungMSCDeviceModel);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.CASEIDMIN).toBeUndefined();
    });

    it('return proper device target when CASEID values do not match MIN or MSISDN', () => {
      const deviceModel = cloneDeep(caseIDNotMatchCASEIDMDNOrCASEIDMINDeviceModel);

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[0]);

      const expectedCASEIDMDN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMDN.target.primaryType = 'CASEIDMDN';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[2]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedMIN = cloneDeep(deviceModel.types.MIN[0]);
      const expectedMSISDN = cloneDeep(deviceModel.types.MSISDN[0]);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.MSISDN).toEqual([expectedMSISDN]);
      expect(deviceModel.types.CASEIDMDN).toEqual([expectedCASEIDMDN]);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual([expectedMIN]);
    });

    it('return proper device target when CASEIDMIN or CASEIDMDN already exists in deviceModel', () => {
      const deviceModel = cloneDeep(alreadyHasCASEIDMINAndCASEIDMDNDeviceModel);
      const expectedTypes = cloneDeep(alreadyHasCASEIDMINAndCASEIDMDNDeviceModel.types);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types).toEqual(expectedTypes);
    });

    it('return proper device target when deviceModel has 4G & Circuit Switch service', () => {
      const deviceModel = cloneDeep(extraCaseIDDeviceModel);

      const expectedCASEID = cloneDeep(deviceModel.types.CASEID[2]);
      expectedCASEID.target.primaryType = 'CASEID';

      const expectedCASEIDMIN = cloneDeep(deviceModel.types.CASEID[1]);
      expectedCASEIDMIN.target.primaryType = 'CASEIDMIN';

      const expectedMIN = cloneDeep(deviceModel.types.MIN[0]);
      const expectedMSISDN = cloneDeep(deviceModel.types.MSISDN[0]);
      const expectedIMSI = cloneDeep(deviceModel.types.IMSI[0]);
      const expectedIMEI = cloneDeep(deviceModel.types.IMEI[0]);

      updateCaseIdTarget(deviceModel, warrantConfigInstance);
      expect(deviceModel.types.MSISDN).toEqual([expectedMSISDN]);
      expect(deviceModel.types.CASEIDMIN).toEqual([expectedCASEIDMIN]);
      expect(deviceModel.types.CASEID).toEqual([expectedCASEID]);
      expect(deviceModel.types.MIN).toEqual([expectedMIN]);
      expect(deviceModel.types.IMSI).toEqual([expectedIMSI]);
      expect(deviceModel.types.IMEI).toEqual([expectedIMEI]);

    });
  });

  describe('afOntologyHasTarget', () => {
    it('return true if af has the target', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = ['MSISDN', 'CASEIDMDN'];
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = ['CASEIDMIN'];
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'CASEIDMIN')).toBe(true);

      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = ['MSISDN', 'CASEIDMIN'];
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = ['CASEIDMDN'];
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'CASEIDMIN')).toBe(true);
    });

    it('return false if af does not have the target', () => {
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'NO_SUCH_TARGET')).toBe(false);
    });

    it('return false if requiredTargets is undefined and target is not an optional target', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = undefined;
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = ['CASEIDMIN'];
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'NO_SUCH_TARGET')).toBe(false);
    });

    it('return true if requiredTargets is undefined and target is an optional target', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = undefined;
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = ['CASEIDMIN'];
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'CASEIDMIN')).toBe(true);
    });

    it('return false if optionalTarget is undefined and target is not an required target', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = ['CASEIDMIN'];
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = undefined;
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'NO_SUCH_TARGET')).toBe(false);
    });

    it('return true if optionalTarget is undefined and target is an required target', () => {
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.requiredTargets = ['CASEIDMIN'];
      warrantConfigInstance.configuration.config.accessFunctions.SAMSUNG_MSC.optionalTargets = undefined;
      expect(afOntologyHasTarget(warrantConfigInstance, 'SAMSUNG_MSC', 'CASEIDMIN')).toBe(true);
    });
  });
});
