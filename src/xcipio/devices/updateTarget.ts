import WarrantConfiguration from 'xcipio/config/warrant-config';
import { DeviceModel } from './device.types';

/*
* If a case is associated to a Nortel MTX AF or Samsung MSC, this function change the CASEID target type
* to CASEIDMIN in the case that is being passed in
*/
export function updateCaseIdTarget(device: DeviceModel, warrantConfig: WarrantConfiguration): void {
  // Since at this time, NORTEL_MTX AFs and Samsung MSC AFs have derived targets that need
  // to be converted from CASEID back to CASEIDMIN.
  // Samsung MSC have derived targets that need to be converted from CASEID back to CASEIDMDN.
  // The solution below is going to try to convert it assuming that only if there is
  // a MIN target, the MIN target has NORTEL_MTX AFs or Samsung MSC AFs, and CASEID is same as MIN target
  // that has NORTEL_MTX AFs or Samsung MSC AFs.
  // Similarily, if there is a MDN target, the MDN target has Samsung MSC AFs and CASEID is same as MDN target
  // that has Samsung SMC AFs.
  // Note that this is not the best solution and a new solution will be implemented
  // in DX-2163.

  // Process only when device has CASEID targets and service is Circuit Switch
  const hasCASEID = device.types.CASEID !== undefined && device.types.CASEID.length > 0;
  if (device.services.indexOf('GSMCDMA-2G-VOICE') < 0 || hasCASEID === false) {
    return;
  }

  // We should use customization file to do the look up.  But for now just hardcode.
  const targets: {sameValueTarget:string, afType: string, derivedTarget:string}[] = [];

  if (afOntologyHasTarget(warrantConfig, 'NORTEL_MTX', 'MIN') && afOntologyHasTarget(warrantConfig, 'NORTEL_MTX', 'CASEIDMIN')) {
    targets.push({
      sameValueTarget: 'MIN',
      afType: 'NORTEL_MTX',
      derivedTarget: 'CASEIDMIN',
    });
  }
  if (afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'MSISDN') && afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'CASEIDMDN')) {
    targets.push({
      sameValueTarget: 'MSISDN',
      afType: 'SAMSUNG_MSC',
      derivedTarget: 'CASEIDMDN',
    });
  }

  targets.forEach(({
    sameValueTarget, afType, derivedTarget,
  }) => {
    // if derived target already exists in device, no need to add it.
    if (device.types[derivedTarget] !== undefined && device.types[derivedTarget].length > 0) {
      return;
    }

    let derivedCaseIDTargetIndex = -1;

    // find the CASEID that matches MIN/MDN value and if MIN/MSISDN has the corresponding AF type,
    // make the CASEID a derivedTarget
    if (sameValueTarget && device.types[sameValueTarget]) {
      // Can safely assume there is only one MIN or one MDN per device
      const minOrMdnTarget = device.types[sameValueTarget][0].target;

      // get CASEID that has same value as MIN/MDN
      const matchingCaseIDTargetIndex = device.types.CASEID.findIndex(({ target: caseTarget }) => {
        return (minOrMdnTarget.primaryValue === caseTarget.primaryValue);
      });
      // if matching value CASEID is found and if there is an actualTarget, make the CASEID a derivedTarget
      if (matchingCaseIDTargetIndex >= 0) {
        const actualTarget = device.types[sameValueTarget][0].target;
        const hasNeededAFType = actualTarget.afs?.afIds.find(({ tag }) => (afType === tag));
        if (hasNeededAFType) {
          derivedCaseIDTargetIndex = matchingCaseIDTargetIndex;
        }
      }
    }
    if (derivedCaseIDTargetIndex >= 0) {
      const chosenType = device.types.CASEID[derivedCaseIDTargetIndex];
      chosenType.target.primaryType = derivedTarget;

      device.types[derivedTarget] = [chosenType];
      device.types.CASEID.splice(derivedCaseIDTargetIndex, 1);
    }
  });

  // The following is to take care of the case when users misprovision and did not put the same MSISDN value
  // in CASEIDMDN for SAMSUNG_MSC or did not put the same MIN valeu in CASEMIN for NORTEL MTX
  if (device.types.CASEID.length > 0) {
    targets.forEach(({
      sameValueTarget, afType, derivedTarget,
    }) => {
      const hasDerivedTarget = (device.types[derivedTarget] !== undefined && device.types[derivedTarget].length > 0);
      if (device.types.CASEID.length === 0 || hasDerivedTarget === true) return;

      let sameValueTargetHasAF = false;
      if (device.types[sameValueTarget]) {
        sameValueTargetHasAF = device.types[sameValueTarget].some(({ target }) => {
          return (target.afs?.afIds.find(({ tag }) => (tag === afType)) !== undefined);
        });
      }

      // If there is an MSISDN for SAMSUNG_MSC in the device, one of the CASEID targets could be CASEIDMDN
      // Simiarily if there is MIN for NORTEL_MTX in the device, one of the CASEID targets could be CASEIDMIN
      if (sameValueTargetHasAF) {
        const matchingCaseIDTargetIndex = device.types.CASEID.findIndex(({ target: caseTarget }) => {
          const caseIDHasAFType = (caseTarget.afs?.afIds.find(({ tag }) => (tag === afType)) !== undefined);
          if (caseIDHasAFType || caseTarget.afs?.afIds.length === 0) {
            return true;
          }
          return false;
        });

        if (matchingCaseIDTargetIndex >= 0) {
          const chosenType = device.types.CASEID[matchingCaseIDTargetIndex];
          chosenType.target.primaryType = derivedTarget;
          device.types[derivedTarget] = [chosenType];
          device.types.CASEID.splice(matchingCaseIDTargetIndex, 1);
        }
      }
    });
  }

  // Following really hardcoded for SAMSUNG_MSC AF
  // SAMSUNG_MSC AF has required targets, MSISDN and CASEIDMDN which are derived from MDN.
  // The loop above takes care of creating CASEIDMDN.
  // SAMSUNG_MSC AF could have an optional CASEIDMIN target that is derived from MIN
  // but MIN is not a SAMSUNG_MSC target.  The following code is to figure out if we need to create a CASEIDMIN
  // for SAMSUNG_MSC
  // If deviceModel has an MSISDN target that has SAMSUNG_MSC AF and there are CASEID left in deviceModel,
  // the CASEID could be CASEIDMIN.
  let msisdnTargetHasSamsungMSC = false;
  if (device.types.MSISDN) {
    msisdnTargetHasSamsungMSC = device.types.MSISDN.some(({ target: msisdnTarget }) => {
      return (msisdnTarget.afs?.afIds.find(({ tag }) => (tag === 'SAMSUNG_MSC')) !== undefined);
    });
  }
  const noCASEIDMIN = device.types.CASEIDMIN === undefined || device.types.CASEIDMIN.length === 0;
  if (device.types.CASEID.length > 0 && msisdnTargetHasSamsungMSC &&
       afOntologyHasTarget(warrantConfig, 'SAMSUNG_MSC', 'CASEIDMIN') && noCASEIDMIN) {
    // Since there is an MSISDN for SAMSUNG_MSC in the device, one of the CASEID targets could be CASEIDMIN
    if (msisdnTargetHasSamsungMSC) {
      const matchingCaseIDTargetIndex = device.types.CASEID.findIndex(({ target: caseTarget }) => {
        const hasSamsungMSC = caseTarget.afs?.afIds.find(({ tag }) => (tag === 'SAMSUNG_MSC'));
        if (hasSamsungMSC || caseTarget.afs?.afIds.length === 0) {
          return true;
        }
        return false;
      });
      if (matchingCaseIDTargetIndex >= 0) {
        const chosenType = device.types.CASEID[matchingCaseIDTargetIndex];
        chosenType.target.primaryType = 'CASEIDMIN';
        device.types.CASEIDMIN = [chosenType];

        device.types.CASEID.splice(matchingCaseIDTargetIndex, 1);
        // create a synthesized MIN
        if (!device.types.MIN) {
          device.types.MIN = [{
            target: {
              id: null,
              caseId: { ...chosenType.target.caseId },
              options: [],
              primaryType: 'MIN',
              primaryValue: chosenType.target.primaryValue,
              secondaryType: '',
              secondaryValue: '',
              isRequired: false,
              synthesized: true,
            },
            display: true,
            synthesized: true,
            ids: [],
          }];
        }
      }
    }
  }

  // If there are still more than one CASEID left.  The original configuration is wrong. Remove
  // The CASEID that does not have an AF.
  if (device.types.CASEID.length > 1) {
    const caseIDWithoutAF = device.types.CASEID.findIndex(({ target: caseTarget }) => {
      return (caseTarget.afs === undefined ||
        (caseTarget.afs.afIds.length === 0 && caseTarget.afs.afGroups.length === 0 &&
        caseTarget.afs.afAutowireTypes.length === 0 && caseTarget.afs.afAutowireGroups.length === 0));
    });
    if (caseIDWithoutAF >= 0) {
      device.types.CASEID.splice(caseIDWithoutAF, 1);
    }
  }
}

export function afOntologyHasTarget(warrantConfig: WarrantConfiguration, afTag:string, targetTag:string): boolean {
  if (warrantConfig.configuration.config.accessFunctions[afTag]) {
    const afOntology = warrantConfig.configuration.config.accessFunctions[afTag];
    if (afOntology.requiredTargets && afOntology.requiredTargets.indexOf(targetTag) >= 0) {
      return true;
    }
    if (afOntology.optionalTargets && afOntology.optionalTargets.indexOf(targetTag) >= 0) {
      return true;
    }
  }
  return false;

}
