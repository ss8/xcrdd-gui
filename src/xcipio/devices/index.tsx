import React from 'react';
import { Button, Intent } from '@blueprintjs/core';
import { connect, ConnectedProps } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import isEqual from 'lodash.isequal';
import { RootState } from 'data/root.reducers';
import { CustomizationDerivedTargetMap } from 'data/customization/customization.types';
import { CaseModel } from 'data/case/case.types';
import { Warrant } from 'data/warrant/warrant.types';
import * as globalSelectors from 'global/global-selectors';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as caseActions from 'xcipio/cases/case-actions';
import * as caseSelectors from 'data/case/case.selectors';
import * as targetSelectors from 'data/target/target.selectors';
import { TargetModel, TargetModelMap } from 'data/target/target.types';
import * as collectionFunctionSelectors from 'data/collectionFunction/collectionFunction.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import DeviceDetail from './forms/device-detail';
import { FormPanel } from '../../shared/wizard/with-discovery-wizard-step-panel';
import DeviceModeler from './deviceModeler';
import getAllCasesFromDevicesModel from './forms/devices-to-cases';
import WarrantPanel from '../warrants/forms/warrant-panel';
import { DeviceModel, DeviceModelMap, DeviceModelType } from './device.types';
import DevicesFormList from './forms/DevicesFormList';
import { mergeTargets } from './utils/mergeTargets';
import CaseModeler from './forms/case-modeler';
import processCasesChanges from './utils/processCasesChanges';
import { updateCaseIdTarget } from './updateTarget';
import './device-panel.scss';

const deviceModel: DeviceModel = {
  device: '',
  caseId: '',
  services: [],
  types: {},
  allTargetAfs: [],
};

const mapState = (state: RootState) => ({
  warrantConfigInstance: warrantSelectors.getWarrantConfigurationInstance(state),
  changeTracker: warrantSelectors.getChangeTrackerInstance(state),
  derivedTargets: warrantSelectors.getCustomizationDerivedTargets(state),
  targetDefaultAFSelection: warrantSelectors.getTargetDefaultAFSelection(state),
  warrant: warrantSelectors.getWarrant(state),
  cases: caseSelectors.getCases(state),
  targetsById: targetSelectors.getTargetsById(state),
  selectedDF: globalSelectors.getSelectedDeliveryFunctionId(state),
  cfGroups: collectionFunctionSelectors.getCollectionFunctionGroupList(state),
  templateUsed: warrantSelectors.getTemplateUsed(state),
  cfs: collectionFunctionSelectors.getCollectionFunctionList(state),
  bSelectAllHI3Interfaces: discoverySelectors.shouldSelectAllHi3Interfaces(state),
  shouldAutomaticallyGenerateCaseName: discoverySelectors.shouldAutomaticallyGenerateCaseName(state),
});

const mapDispatch = {
  updateCasesState: caseActions.updateCasesState,
};

const connector = connect(mapState, mapDispatch, null, { forwardRef: true });

type PropsFromRedux = ConnectedProps<typeof connector>;

export type DevicePanelProps = PropsFromRedux & {
  mode: string
  onDevicesReady?: () => void
};

export type DevicePanelState = {
  devices: DeviceModelMap,
}

export class DevicePanel extends React.Component<DevicePanelProps, DevicePanelState> implements FormPanel {
  private deviceRefs: { [deviceId: string]: DeviceDetail | null } = {}
  private maxId = 0;
  private originalDevices: DeviceModelMap | null = null;

  constructor(props: DevicePanelProps) {
    super(props);

    const devices = this.initializeDevice();

    this.state = {
      devices,
    };
  }

  async componentDidUpdate(prevProps: DevicePanelProps): Promise<void> {
    const {
      warrant,
      cfs,
      mode,
      onDevicesReady,
    } = this.props;

    if (mode !== CONFIG_FORM_MODE.View && DevicePanel.shouldInitializeDevices(warrant, prevProps.warrant)) {
      const devices = this.initializeDevice();

      if (this.shouldAutoSelectSingleOption()) {
        this.autoSelectSingleOption(devices);
        await this.updateDeviceMapTargets(devices);
      }

      this.setState({
        devices,
      });
    }

    if (!isEqual(cfs, prevProps.cfs)) {
      const { devices } = this.state;
      const clonedDevices = cloneDeep(devices);

      await this.updateDeviceMapTargets(clonedDevices);

      onDevicesReady && onDevicesReady();

      this.setState({
        devices: clonedDevices,
      });

      this.originalDevices = cloneDeep(clonedDevices);
    }
  }

  static shouldInitializeDevices(warrant: Warrant | null, prevWarrant: Warrant | null): boolean {
    if (warrant == null) {
      // The warrant is not defined yet, no need to initialize
      return false;
    }

    if (!DevicePanel.isReady(warrant) && DevicePanel.isReady(prevWarrant)) {
      // The Lea was removed, so devices should be initialize
      return true;
    }

    if (warrant.lea !== prevWarrant?.lea) {
      // The Lea was changed, so devices should be initialize
      return true;
    }

    return false;
  }

  initializeDevice(): DeviceModelMap {
    const {
      cases,
      warrant,
      derivedTargets,
      targetsById,
    } = this.props;

    this.maxId = DevicePanel.getMaxDeviceId(cases);

    let devices = DevicePanel.getDevices(warrant, cases, derivedTargets, targetsById);

    if (Object.keys(devices).length === 0) {
      const newDevice = this.createDevice();
      devices = {
        [newDevice.device]: newDevice,
      };
    }

    this.originalDevices = cloneDeep(devices);

    return devices;
  }

  shouldAutoSelectSingleOption(): boolean {
    const { warrantConfigInstance, warrant } = this.props;
    const availableServiceOptions = warrantConfigInstance?.getServiceDropDownOptions() ?? [];

    return availableServiceOptions.length === 1 && DevicePanel.isReady(warrant);
  }

  autoSelectSingleOption(devices: DeviceModelMap): void {
    const { warrantConfigInstance } = this.props;
    const availableServiceOptions = warrantConfigInstance?.getServiceDropDownOptions() ?? [];

    Object.values(devices).forEach((device) => {
      device.services = [availableServiceOptions[0].value];
    });
  }

  static getMaxDeviceId(cases: Partial<CaseModel>[]): number {
    let max = 0;
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < cases.length; i++) {
      const deviceIdString = cases[i].device;
      if (deviceIdString) {
        const deviceId = parseInt(deviceIdString, 10);
        if (deviceId > max) {
          max = deviceId;
        }
      }
    }
    return max;
  }

  static getDevices(
    warrant: Warrant | null,
    cases: CaseModel[],
    derivedTargets: CustomizationDerivedTargetMap,
    targetsById: TargetModelMap,
  ): DeviceModelMap {
    if (warrant == null) {
      return {};
    }

    const model = new DeviceModeler(derivedTargets);
    model.setWarrantData(cloneDeep(warrant));
    cases.forEach((wc) => {
      const warrantCase = cloneDeep(wc);
      model.setCaseData(warrantCase);
      warrantCase.targets.forEach((target) => {
        if (target.caseId != null) {
          model.setTargetData(target);
        } else {
          model.setTargetData(targetsById[target.id as string]);
        }
      });
    });

    return model.buildModel();
  }

  static getHeadersFromDeviceTargets(devices: DeviceModelMap): string[] {
    const headers = new Set<string>();

    Object.keys(devices).forEach((id) => {
      Object.keys(devices[id].types).forEach((type) => {
        const ets = devices[id].types[type];
        const display = ets.some((et) => {
          return et.display;
        });

        if (display) {
          headers.add(type);
        }
      });
    });

    const headerArray = [...headers];
    headerArray.unshift('Services');
    headerArray.unshift('Device');
    headerArray.unshift('');

    return headerArray;
  }

  static isReady(warrant: Warrant | null): boolean {
    return warrant?.lea != null && warrant?.lea !== '';
  }

  createDevice(): DeviceModel {
    const newDevice = cloneDeep(deviceModel);

    this.maxId += 1;
    newDevice.device = this.maxId.toString();

    return newDevice;
  }

  deleteDevice = (deviceId: string): void => {
    const { devices } = this.state;

    const clonedDevices = cloneDeep(devices);
    delete clonedDevices[deviceId];
    delete this.deviceRefs[deviceId];

    this.setState({
      devices: clonedDevices,
    });
  }

  addDevice = (): void => {
    const { devices } = this.state;

    const newDevice = this.createDevice();

    const clonedDevices = cloneDeep(devices);
    clonedDevices[newDevice.device] = newDevice;

    this.setState({
      devices: clonedDevices,
    });
  }

  isValid(errors: string[]): boolean {
    let areDevicesValid = true;

    Object.keys(this.deviceRefs).forEach((key) => {
      if (!this.deviceRefs[key]) {
        delete this.deviceRefs[key];
      } else {
        const deviceValid = this.deviceRefs[key]?.isValid() ?? false;
        areDevicesValid = areDevicesValid && deviceValid;
      }
    });

    if (areDevicesValid) {
      const { devices } = this.state;

      const hasAtLeastOneTargetDefinedForEachDevice = DevicePanel.hasAtLeastOneTargetDefinedForEachDevice(
        devices,
        errors,
      );

      areDevicesValid = areDevicesValid && hasAtLeastOneTargetDefinedForEachDevice;
    }

    return areDevicesValid;
  }

  async submit(force = false): Promise<boolean> {

    if (!force && !this.hasAnyChange()) {
      return true;
    }

    const {
      warrantConfigInstance,
      warrant,
      derivedTargets,
      targetDefaultAFSelection,
      cfs,
      templateUsed,
      selectedDF,
      bSelectAllHI3Interfaces,
      cases,
      updateCasesState,
      mode,
      targetsById,
      changeTracker,
      shouldAutomaticallyGenerateCaseName,
    } = this.props;

    const {
      devices,
    } = this.state;

    if (warrantConfigInstance == null) {
      return false;
    }

    const ontology = warrantConfigInstance.configuration.config;
    const serviceMapping = warrantConfigInstance.afcfByServiceMap;

    const newCases = await getAllCasesFromDevicesModel(
      warrant,
      serviceMapping,
      derivedTargets,
      targetDefaultAFSelection,
      devices,
      cfs,
      templateUsed,
      selectedDF,
      bSelectAllHI3Interfaces ?? false,
      cases,
      targetsById,
      ontology,
      shouldAutomaticallyGenerateCaseName,
    );
    updateCasesState(newCases);

    if (mode === CONFIG_FORM_MODE.Edit) {
      processCasesChanges(changeTracker, cases, newCases, targetsById);
    }

    return true;
  }

  static hasAtLeastOneTargetDefinedForEachDevice(devices: DeviceModelMap, errors: string[]): boolean {
    if (Object.values(devices).length === 0) {
      return true;
    }

    return Object.values(devices).every((device) => {
      const hasAtLeastOneTargetDefined = DevicePanel.hasAtLeastOneTargetDefined(device);

      if (!hasAtLeastOneTargetDefined) {
        errors.push(`Missing information. Enter at least 1 target value for device ${device.device}.`);
      }

      return hasAtLeastOneTargetDefined;
    });
  }

  static hasAtLeastOneTargetDefined({ services, types }: DeviceModel): boolean {
    if (services.length === 0) {
      return true;
    }

    return Object.values(types).some((targetType) => {
      return targetType.some(({ target }) => !!target.primaryValue);
    });
  }

  hasAnyChange(): boolean {
    const {
      devices,
    } = this.state;

    return !isEqual(devices, this.originalDevices);
  }

  hasConfiguredDevices(): boolean {
    const { devices } = this.state;

    if (Object.keys(devices).length === 0) {
      return false;
    }

    return Object.entries(devices).some(([_deviceId, device]) => device.services.length > 0);
  }

  deviceServiceUpdateHandler = async (deviceId: string, services: string[]): Promise<void> => {
    const { devices } = this.state;

    if (devices[deviceId] == null) {
      return;
    }

    const clonedDevices = cloneDeep(devices);
    const clonedDevice = clonedDevices[deviceId];

    clonedDevice.services = services;

    await this.updateDeviceTargets(clonedDevice);

    this.setState({
      devices: clonedDevices,
    });
  }

  deviceTargetUpdateHandler = (
    deviceId: string,
    target: TargetModel,
    targetType: string,
    targetIndex: number,
  ): void => {
    const { devices } = this.state;

    if (devices[deviceId] == null) {
      return;
    }

    const clonedDevices = cloneDeep(devices);
    const clonedDevice = clonedDevices[deviceId];

    const clonedTarget = cloneDeep(target);

    const deviceTarget = clonedDevice.types[targetType][targetIndex].target;
    deviceTarget.primaryValue = clonedTarget.primaryValue;
    deviceTarget.secondaryValue = clonedTarget.secondaryValue;
    deviceTarget.options = clonedTarget.options;

    this.setState({
      devices: clonedDevices,
    });
  }

  async updateDeviceMapTargets(devices: DeviceModelMap): Promise<void> {
    const deviceEntries = Object.values(devices);

    // eslint-disable-next-line no-restricted-syntax
    for (const device of deviceEntries) {
      await this.updateDeviceTargets(device);
    }
  }

  async updateDeviceTargets(device: DeviceModel): Promise<void> {
    const {
      warrantConfigInstance,
      cfs,
      derivedTargets,
      cases,
    } = this.props;

    if (warrantConfigInstance == null) {
      return;
    }

    let possibleCollectionFunctions = cfs;

    if (!CaseModeler.isNewDevice(device)) {
      // For existing devices we should filter the CFs by ones used in the cases instead
      // of all available CFs. This is to make sure that the targets to be generated
      // for the device doesn't contain any target from removed cases (CFs)
      const casesForDevice = CaseModeler.getCasesForDevice(device, cases);
      const cfTagsFromCases = casesForDevice
        .map((entry) => {
          const collectionFunctionFound = cfs.find(({ id }) => id === entry.cfId.id);
          return collectionFunctionFound?.tag ?? '';
        })
        .filter((entry) => entry !== '');

      possibleCollectionFunctions = cfs.filter(({ tag }) => cfTagsFromCases.indexOf(tag) !== -1);
    }

    const cfTypes = WarrantPanel.cfsToTypes(possibleCollectionFunctions);

    await warrantConfigInstance.computeSelectedAfTypesForDevices(device.services, cfTypes);

    updateCaseIdTarget(device, warrantConfigInstance);

    let targetTypes: DeviceModelType = {};

    if (WarrantConfiguration.hasUnsupportedServices(device.services)) {
      targetTypes = device.types;

      // For warrants created in XMS, each device will be mapped to a single case
      const [warrantCase] = CaseModeler.getCasesForDevice(device, cases);
      const targets = this.getTargetsForCase(warrantCase);
      if (warrantCase.cfId.tag != null) {
        const cfId = {
          tag: warrantCase.cfId.tag,
        };
        await warrantConfigInstance.computeAfsBasedOnCfAndTargets(cfId, targets);
      }

    } else {
      warrantConfigInstance.computePossibleTargets(device.services);

      // get derivedTargets based upon possible targets and customization config in redux
      const possibleTypes = warrantConfigInstance.getDerivedTypes(device.services, derivedTargets);

      // Existing target that are not in the possible targets should be
      // merged only for existing devices. New devices should follow what
      // is defined in the ontology.
      const shouldMergeExistingTargets = !CaseModeler.isNewDevice(device);

      // merge the targets that are supported by the selected services
      // and the targets already assigned to the device.
      targetTypes = mergeTargets(possibleTypes, device.types, shouldMergeExistingTargets);
    }

    device.types = targetTypes;
    device.allTargetAfs = warrantConfigInstance.getTargetWithAllAfs();
    device.afMapping = cloneDeep(warrantConfigInstance.afsByTypeMap);
  }

  getTargetsForCase(caseEntry: CaseModel): TargetModel[] {
    const {
      targetsById,
    } = this.props;

    return caseEntry.targets.map((target) => {
      return targetsById[target.id as string];
    });
  }

  renderAddDeviceButton(isReady:boolean): JSX.Element | null{
    const {
      mode,
    } = this.props;
    if (mode === CONFIG_FORM_MODE.View) {
      return null;
    }

    return (
      <Button
        minimal
        disabled={!isReady}
        intent={Intent.PRIMARY}
        icon="add"
        text="Add Device"
        onClick={this.addDevice}
      />
    );
  }

  render(): JSX.Element {
    this.deviceRefs = {};

    const {
      warrantConfigInstance,
      warrant,
      mode,
      templateUsed,
    } = this.props;

    const {
      devices,
    } = this.state;

    const headers = DevicePanel.getHeadersFromDeviceTargets(devices);
    const availableServiceOptions = warrantConfigInstance?.getServiceDropDownOptions() ?? [];
    const isReady = DevicePanel.isReady(warrant) || mode === CONFIG_FORM_MODE.Edit;

    return (
      <div className="devicesContainer" data-testid="WarrantDevices">
        <DevicesFormList
          deviceRefs={this.deviceRefs}
          isReady={isReady}
          devices={devices}
          mode={mode}
          headers={headers}
          availableServiceOptions={availableServiceOptions}
          templateUsed={templateUsed}
          warrantConfigInstance={warrantConfigInstance}
          onDeleteDevice={this.deleteDevice}
          onDeviceServiceUpdate={this.deviceServiceUpdateHandler}
          onDeviceTargetUpdate={this.deviceTargetUpdateHandler}
        />
        {this.renderAddDeviceButton(isReady)}
        <span className="device-panel-note">
          NOTE: when all targets are optional, for proper service configuration enter at least 1 target value.
        </span>
      </div>
    );
  }
}

export default connector(DevicePanel);
