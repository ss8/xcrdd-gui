/* eslint-disable no-continue */
/* eslint-disable guard-for-in */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-restricted-syntax */
import cloneDeep from 'lodash.clonedeep';
import { CustomizationDerivedTargetMap, CustomizationDerivedTargetService } from 'data/customization/customization.types';
import { Warrant } from 'data/warrant/warrant.types';
import { CaseModel, CaseModelMap } from 'data/case/case.types';
import { TargetModel, TargetModelMap, TargetModelWrapper } from 'data/target/target.types';
import { DeviceModel, DeviceModelMap } from './device.types';

/*
**  **************************************
**  CODE FOR DEVICE DATA MODEL STARTS HERE
**  **************************************
*/
// constant values
const ALL = '*';

function getIDList(json: CaseModel, field: 'targets') {
  let ids: string[] = [];
  if (json.hasOwnProperty(field)) {
    // Get the ID field from identity object and return as a list
    ids = [];
    const idList = json[field];
    for (const identity of idList) {
      if (identity.id != null) {
        ids.push(identity.id);
      }
    }
  }

  return (ids);
}

function findTargetByValue(device: DeviceModel, ttype?: string | null, tvalue?: string | null) {
  // Return the target record whose value matches the given value
  let targetMatch: TargetModelWrapper | null = null;

  if (ttype != null && device.types.hasOwnProperty(ttype)) {
    // Look through this target-type's targets for a matching value
    for (const target of device.types[ttype]) {
      if (target.target.primaryValue === tvalue) {
        // Found the matching target, so return it
        targetMatch = target;
        break;
      }
    }
  }

  return (targetMatch);
}

export default class DeviceModeler {
  customerTargets: CustomizationDerivedTargetMap;
  warrant: { id: string } | null;
  idWarrant: string | null;
  cases: CaseModelMap;
  targets: TargetModelMap;
  nextId: number;

  /*
  * NOTE:
  *  Read the CREST data for a warrant, including its cases and targets.  Then
  *  associate its cases and targets with devices by matching the device ID
  *  value set in each case.  Then aggregate the target types and IDs into each
  *  device.  (Two cases may have the same device ID.)  Mark each target as
  *  visible or not-visible based on whether the target type is one of the
  *  customer's supported types.  If no device ID value is set, indicate that
  *  the devices view is not available.
  */
  constructor(customerTargets: CustomizationDerivedTargetMap) {
    this.customerTargets = customerTargets;
    this.warrant = null;
    this.idWarrant = null;
    this.cases = { };
    this.targets = { };
    this.nextId = 0;
  }

  /*
  * setWarrantData:  PUBLIC
  * Pass the main warrant data to this data modeler.  At this point, only the
  * warrant ID is used.
  */
  setWarrantData(warrantJson: Warrant): void {
    // Record this warrant's data
    this.warrant = warrantJson;
    this.idWarrant = this.warrant.id;
  }

  /*
  * setCaseData:  PUBLIC
  * Pass each case's data for the warrant to this data modeler.  If a case lacks
  * the device ID, a device ID will be assigned to it (one device per case).
  */
  setCaseData(caseJson: CaseModel): string {
    // Record the indicated case, indexed by its caseId value from the JSON data
    const caseId = caseJson.id;
    if (caseId != null) {
      // Allow cases lacking device-ID - handled when building the model
      this.cases[caseId] = caseJson;
    }
    return (caseId);
  }

  /*
  * setTargetData:  PUBLIC
  * Pass each target's data for each case to this data modeler.  We check that
  * the target's case ID has already been added to the modeler before accepting
  * the data.
  */
  setTargetData(targetJson: TargetModel): string | null {
    // Record the indicated target, indexed by its caseId value from the JSON data
    // We assume that the case containing the target was recorded first
    const targetId = targetJson.id;
    if (targetId != null) {
      const caseId = targetJson.caseId?.id;
      if ((caseId != null) && this.cases.hasOwnProperty(caseId)) {
        this.targets[targetId] = cloneDeep(targetJson);
      }
    }
    return (targetId);
  }

  getTargetDirectMapping(json: TargetModel, targetPrefix: 'primaryType', services: string[]): CustomizationDerivedTargetService | null {
    let mapping: CustomizationDerivedTargetService | null = null;

    // Locate the mapping for this target type (primary/secondary) in the customerTargets
    if (json[targetPrefix] != null) {
      const ttype = json[targetPrefix];

      if (ttype != null && this.customerTargets != null) {
        // See if this target-type is configured with a mapping
        if (this.customerTargets.hasOwnProperty(ttype)) {
          const map = this.customerTargets[ttype];
          const hasAll = map.hasOwnProperty(ALL);
          let hasSvc = false;
          // If ALL is not mapped, then look for a matching service
          if ((!hasAll) && (services != null)) {
            // Does this mapping contain a service-specific pattern?
            for (const service in map) {
              if (services.indexOf(service) >= 0) {
                hasSvc = true;
                break;
              }
            }
          }
          if (hasAll || hasSvc) {
            mapping = map;
          }
        }
      }
    }

    return (mapping);
  }

  getTargetMapping(target: TargetModel, services: string[]): CustomizationDerivedTargetService | null {
    // Select the best mapping for this target, starting with the alternate mappings
    return this.getTargetDirectMapping(target, 'primaryType', services);
  }

  getTargetDisplay(target: TargetModel, services: string[]): boolean {
    // The target is displayed only when there is no mapping for the target
    const hasMapping = (this.getTargetMapping(target, services) != null);

    return (!hasMapping);
  }

  getTargetPattern(services: string[], target: TargetModel): string | null {
    const ttype = target.primaryType;
    let pattern: string | null = null;

    // Return the pattern matching the first service from the list
    if (ttype) {
      const mapping = this.getTargetMapping(target, services);
      if (mapping != null) {
        // Found the matching target mapping, look for the service's pattern
        for (const patternSvc in mapping) {
          if (services.indexOf(patternSvc) >= 0) {
            // Found a service-specific pattern
            pattern = mapping[patternSvc].pattern;
            break;
          }
        }
        if ((!pattern) && mapping.hasOwnProperty(ALL)) {
          // Use the default pattern for this target-type
          pattern = mapping[ALL].pattern;
        }
      }
    }

    return (pattern);
  }

  getPatternValue(services: string[], target: TargetModel): string | null {
    let value: string | null = null;
    const pattern = this.getTargetPattern(services, target);

    // Look in the target pattern for the braces and return the corresponding section from the value.
    if (pattern) {
      const iLeftB = pattern.indexOf('{');
      const iRightB = pattern.indexOf('}');
      const trailing = (pattern.length - (iRightB + 1)); // count of pattern bytes after the '}'
      const primaryValueLength = target.primaryValue?.length ?? 0;

      // If there is a target pattern defined for this target, return the target value matching the pattern
      if ((iLeftB >= 0) && (iLeftB < iRightB) && ((iLeftB + trailing) < primaryValueLength)) {
        // Extract the substring in the value that starts where the '{' starts and ends where the ending
        // of the pattern begins
        value = target.primaryValue?.substring(iLeftB, (target.primaryValue.length - trailing)) ?? null;
      }
    }

    return (value);
  }

  getTargetDepend(services: string[], target: TargetModel): string | null {
    // NOTE: Only the primary target type can depend upon another target
    let depend: string | null = null;
    const pattern = this.getTargetPattern(services, target);

    // Look to see if this target's pattern is defined using a different target's value
    if (pattern) {
      const iLeftB = pattern.indexOf('{');
      const iRightB = pattern.indexOf('}');
      // If there is a target pattern defined for this target, return the dependent target type
      if ((iLeftB >= 0) && (iLeftB < iRightB)) {
        depend = pattern.substring((iLeftB + 1), iRightB);
      }
    }

    return (depend);
  }

  buildTarget(json: TargetModel, services: string[]): TargetModelWrapper | null {
    let device: TargetModelWrapper | null = null;

    if (json.hasOwnProperty('primaryType')) {
      // Return an object with the target-json and display fields populated
      const display = this.getTargetDisplay(json, services); // ignoring secondaryType
      device = {
        target: json,
        display,
        ids: [],
      };
    }

    return (device);
  }

  nextDeviceId(): string {
    // Return a useful device ID to distinguish this case's device from others
    this.nextId += 1;
    return (`DX${this.nextId}`);
  }

  /*
  Groups incoming next targets based upon target type and afs inside that target
  targets: Is the Base targets result where accumulation is happening
  nextTargets : Are incoming targets getting added to resultant
  */
  static addAllTargetAfs(targets: TargetModel[], nextTargets?: TargetModel[]): void {
    const targetByTypes: { [targetType: string]: TargetModel } = {};
    targets.forEach((target) => {
      const key = target.primaryType;
      if (key != null) {
        targetByTypes[key] = target;
      }
    });

    nextTargets?.forEach((nextTarget) => {
      if (nextTarget.primaryType == null) {
        return;
      }

      const target = targetByTypes[nextTarget.primaryType];
      if (target) {
        DeviceModeler.mergeTargetAfs(target, nextTarget);
      } else {
        targets.push(nextTarget);
      }
    });
  }

  static mergeTargetAfs(target: TargetModel, nextTarget: TargetModel): void {
    const targetAftypes1 = new Set<string>();
    target.afs?.afIds.forEach((obj) => { targetAftypes1.add(obj.type as string); });

    nextTarget.afs?.afIds.forEach((obj) => {
      if (!targetAftypes1.has(obj.type as string)) {
        target.afs?.afIds.push(obj);
        targetAftypes1.add(obj.type as string);
      }
    });

    const targetAftypes2 = new Set();
    target.afs?.afAutowireTypes.forEach((obj) => { targetAftypes2.add(obj.name); });

    nextTarget.afs?.afAutowireTypes.forEach((obj) => {
      if (!targetAftypes2.has(obj.name)) {
        target.afs?.afAutowireTypes.push(obj);
        targetAftypes2.add(obj.name);
      }
    });
  }

  createCaseTarget(
    targetType: string,
    targetValue: string | null,
    targetIsRequired: boolean | undefined,
    caseId: string,
  ): TargetModelWrapper {
    // Create a target object (like the JSON) to represent the type and value
    const targetCase = this.cases[caseId];
    const target: TargetModel = {
      id: null,
      caseId: { id: caseId, name: targetCase.name },
      options: [],
      primaryType: targetType,
      primaryValue: targetValue ?? undefined,
      secondaryType: '',
      secondaryValue: '',
      isRequired: targetIsRequired ?? false,
      synthesized: true,
    };

    return {
      target,
      display: true,
      synthesized: true,
      ids: [],
    };
  }

  static isETSIGizmoTarget(target: TargetModel): boolean {
    return target.afs?.afAutowireTypes.some(({ tag }) => tag === 'ETSI_GIZMO') ?? false;
  }

  /**
   * This function is going to try to convert the target type back to the type that is used in the
   * front-end.
   *
   * - MDN associated with ETSI_GIZMO will be converted to MDNwService;
   *
   * Note that this is not the best solution and a new solution will be implemented in DX-3330.
   *
   * @param target the target to adapt
   * @returns the adapted target
   */
  static adaptTarget(target: TargetModel): TargetModel {
    const targetType = target.primaryType ?? '';

    if (targetType === 'MDN' && DeviceModeler.isETSIGizmoTarget(target)) {
      target = cloneDeep(target);
      target.primaryType = 'MDNwService';
    }

    return target;
  }

  /*
  * buildModel:  PUBLIC
  * Once all the cases and targets have been added to the model, construct the
  * device-based model using the cases and targets.  If a case lacks a device
  * ID, assign it a new device ID.  Aggregate the targets for a single device.
  * If any of the targets requires data from another target (eg: MDN generates
  * SIP-URI), then check whether the dependent target is present.  If not,
  * introduce a place-holder target for the dependent target (with no targetID
  * or AFs).  If any target type for a device is repeated (with distinct
  * values), then reject the model and return NULL.
  */
  buildModel(): DeviceModelMap {
    // If there are no cases recorded, then there is no model to be generated
    if (Object.keys(this.cases).length === 0) {
      return {};
    }

    // Step through each case and add a device record for its device (if not already added)
    // Track any "real" targets here (missing targets added later)
    const devices: DeviceModelMap = {};
    for (const caseId in this.cases) {
      let dev = this.cases[caseId].device;

      if (!dev) {
        dev = this.nextDeviceId();
        this.cases[caseId].device = dev;
      }

      if (!devices.hasOwnProperty(dev)) {
        devices[dev] = {
          device: dev,
          caseIds: [],
          allTargetAfs: [],
          afMapping: undefined,
          serviceSet: new Set(),
          services: [],
          types: {},
        };
      }

      const device = devices[dev];
      device.caseIds?.push(caseId);

      // Add this case's services to the set of services in the device's model
      const { includeInWarrant, services = [] } = this.cases[caseId];
      if (includeInWarrant !== false) {
        for (const service of services) {
          device.serviceSet?.add(service);
        }
      }
    }

    // Build the service list (array) from the Set of aggegated services
    for (const dev in devices) {
      devices[dev].services = Array.from(devices[dev].serviceSet ?? []);
      delete devices[dev].serviceSet; // this set is used only during aggregation
    }

    for (const caseId in this.cases) {
      const dev = this.cases[caseId].device;
      const device = devices[dev];
      const { services } = device;
      // Keep track of this case's allTargetAfs setting (only when present)
      if (this.cases[caseId].hasOwnProperty('allTargetAfs')) {
        DeviceModeler.addAllTargetAfs(device.allTargetAfs, this.cases[caseId].allTargetAfs);
      }

      const { afMapping } = this.cases[caseId];
      if (afMapping != null && device.afMapping == null) {
        device.afMapping = afMapping;
      }

      // Add each target for this case to the set of targets in the device's model
      const targetIDs = getIDList(this.cases[caseId], 'targets'); // get the list of target IDs

      for (const id of targetIDs) {
        // Make sure the device includes this target+value, and track its target ID
        if (!this.targets.hasOwnProperty(id)) {
          continue; // this is not expected
        }

        const adaptedTarget = DeviceModeler.adaptTarget(this.targets[id]);

        const targetType = adaptedTarget.primaryType;
        const targetValue = adaptedTarget.primaryValue;

        if (targetType != null && device.types[targetType] == null) {
          device.types[targetType] = [];
        }

        let targetHolder = findTargetByValue(device, targetType, targetValue);

        if (targetHolder == null && targetType != null) {
          targetHolder = this.buildTarget(adaptedTarget, services);

          if (targetHolder == null) {
            continue; // missing primaryType
          }

          device.types[targetType].push(targetHolder);

        } else if (targetHolder != null) {
          DeviceModeler.mergeTargetAfs(targetHolder.target, adaptedTarget);
        }

        targetHolder?.ids.push(id);
      }
    }

    // Add any required targets to the device list for the undisplayed/derived targets
    // First we build the list of those missing target values, then add each to the list
    for (const dev in devices) {
      // Can't add to device targets while iterating over targets
      const missing: {[key: string]: TargetModelWrapper} = {};
      const device = devices[dev];
      const services = device.services ?? [];

      for (const ttype in device.types) {
        for (const target of device.types[ttype]) {
          const depend = this.getTargetDepend(services, target.target);

          if (depend && (!device.types.hasOwnProperty(depend))) { // matches only primary types
            // We are missing the target that this one depends upon
            missing[depend] = target;
          }
        }
      }
      // Now, add in the missing targets needed for pattern generation
      for (const miss in missing) {
        if (miss.startsWith('case.') || miss.startsWith('warrant.')) {
          continue; // These dependencies are taken from the warrant/case values
        }

        if (device.caseIds == null) {
          continue; // This is not expected
        }

        // Get the value from the dependent's type pattern
        const value = this.getPatternValue(services, missing[miss].target);
        const target = this.createCaseTarget(miss, value, missing[miss].target.isRequired, device.caseIds[0]);

        // we don't want to have a new field for case ID, etc.
        if (!device.types.hasOwnProperty(miss)) {
          device.types[miss] = [];
        }

        if (findTargetByValue(device, miss, value) == null) {
          device.types[miss].push(target);
        }
      }
    }

    return devices;
  }
}
