import isEqual from 'lodash.isequal';
import { CaseModel } from 'data/case/case.types';
import { TargetModel, TargetModelMap } from 'data/target/target.types';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';
import CaseModeler from '../forms/case-modeler';

/**
 * Responsible for check if it is required to add/update/delete the cases
 * based in the cases generated from the devices.
 *
 * @param changeTracker the change tracker
 * @param oldCases the previous cases - could be from server or last time generated cases
 * @param newCases the new cases generate from the devices
 */
export default function processCasesChanges(
  changeTracker: ChangeTracker | null,
  oldCases: CaseModel[],
  newCases: CaseModel[],
  targetsById: TargetModelMap,
): void {
  if (changeTracker == null) {
    return;
  }

  // Check for any added or updated cases
  newCases.forEach((newCaseEntry) => {
    if (newCaseEntry.includeInWarrant === false) {
      // Nothing to be done here, it will be done once the case is added back
      return;
    }

    const oldCaseEntry = oldCases.find(({ id }) => id === newCaseEntry.id);

    if (oldCaseEntry == null) {
      changeTracker.addAdded(`case.${newCaseEntry.id}`);
      processTargetChanges(changeTracker, newCaseEntry, [], newCaseEntry.targets);

    } else {
      const oldTargets = oldCaseEntry.targets.map((target) => {
        if (target.caseId != null) {
          return target;
        }
        return targetsById[target.id as string];
      });
      const newTargets = newCaseEntry.targets;

      const hasAnyChange = processTargetChanges(changeTracker, newCaseEntry, oldTargets, newTargets);

      if (hasAnyChange) {
        changeTracker.addUpdated(`case.${newCaseEntry.id}`, null);
      }
    }
  });

  // Check for deleted cases
  oldCases.forEach((oldCaseEntry) => {
    const newCaseEntry = newCases.find(({ id }) => id === oldCaseEntry.id);

    if (newCaseEntry == null) {
      changeTracker.addDeleted(`case.${oldCaseEntry.id}`, oldCaseEntry.liId as string);

      if (CaseModeler.isNewCase(oldCaseEntry)) {
        processTargetChanges(changeTracker, oldCaseEntry, oldCaseEntry.targets, []);
      }
    }
  });
}

function processTargetChanges(
  changeTracker: ChangeTracker | null,
  caseEntry: CaseModel,
  oldTargets: TargetModel[],
  newTargets: TargetModel[],
): boolean {
  if (changeTracker == null) {
    return false;
  }

  let hasAnyChange = false;

  // Check for added or updated targets
  newTargets?.forEach((newTarget) => {
    const oldTarget = oldTargets.find(({ id }) => id === newTarget.id);

    if (oldTarget == null) {
      if (newTarget.primaryValue?.trim().length === 0) {
        // Nothing to be done, it is just an empty target
      } else {
        changeTracker.addAdded(`case.${caseEntry.id}.target.${newTarget.id}`);
        hasAnyChange = true;
      }

    } else if (!isEqual(newTarget, oldTarget)) {
      if (newTarget.primaryValue?.trim().length === 0) {
        changeTracker.addDeleted(`case.${caseEntry.id}.target.${newTarget.id}`, newTarget.primaryType as string);

      } else if (oldTarget.primaryValue?.trim().length === 0) {
        changeTracker.addAdded(`case.${caseEntry.id}.target.${newTarget.id}`, newTarget.primaryValue);

      } else {
        changeTracker.addUpdated(`case.${caseEntry.id}.target.${newTarget.id}`, newTarget.primaryValue);
      }

      hasAnyChange = true;
    }
  });

  // Check for deleted targets
  oldTargets.forEach((oldTarget) => {
    const newTarget = newTargets.find(({ id }) => id === oldTarget.id);

    if (newTarget == null) {
      if (oldTarget.primaryValue?.trim().length !== 0) {
        changeTracker.addDeleted(`case.${caseEntry.id}.target.${oldTarget.id}`, oldTarget.primaryType as string);
        hasAnyChange = true;
      }
    }
  });

  return hasAnyChange;
}
