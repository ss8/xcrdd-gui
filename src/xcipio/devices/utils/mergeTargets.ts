import { TargetModel, TargetModelWrapper } from 'data/target/target.types';
import { DeviceModelType } from '../device.types';

// eslint-disable-next-line import/prefer-default-export
export function mergeTargets(
  possibleTypes: DeviceModelType,
  existingTypes: DeviceModelType,
  shouldMergeExistingTypes: boolean,
): DeviceModelType {
  const nextTypes: DeviceModelType = {};

  Object.keys(possibleTypes).forEach((key) => {
    nextTypes[key] = [];
    const pt = possibleTypes[key];
    const et = existingTypes[key];

    if (et) {
      copyTargetTypes(et, pt);
      nextTypes[key] = et;

    } else {
      nextTypes[key] = pt;
    }
  });

  if (shouldMergeExistingTypes) {
    // Add existing targets that are not included in the possible targets
    Object.keys(existingTypes).forEach((key) => {
      if (nextTypes[key] != null) {
        // The type is already included in the possible targets
        return;
      }

      nextTypes[key] = existingTypes[key];
    });
  }

  return nextTypes;
}

// Copies necessary values from generated possible type to existing type
function copyTargetTypes(etypes: TargetModelWrapper[], ptypes: TargetModelWrapper[]) {
  etypes.forEach((et) => {
    et.display = ptypes[0].display;
    et.synthesized = ptypes[0].synthesized;
    copyTargetValues(et.target, ptypes[0].target);
  });
}

function copyTargetValues(et: TargetModel, pt: TargetModel) {
  et.afs = pt.afs;
  et.isRequired = pt.isRequired;
  et.synthesized = pt.synthesized;
}
