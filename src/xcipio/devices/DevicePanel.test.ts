import DevicePanel from './index';
import {
  CUSTOMER_TARGETS,
  CREATING_WARRANT,
  CASES_LTE_4G_DATA,
  EXPECTED_MODEL_FOR_LTE_4G_DATA,
  CASES_FOR_GSMCDMA_2G_VOICE,
  EXPECTED_MODEL_FOR_GSMCDMA_2G_VOICE,
  CASES_FOR_LTE_GIZMO,
  EXPECTED_MODEL_FOR_LTE_GIZMO,
  CASES_FOR_LTE_IOT,
  EXPECTED_MODEL_FOR_LTE_IOT,
  CASES_FOR_PTT,
  EXPECTED_MODEL_FOR_PTT,
  CASES_FOR_UMTS_3G_VOICE,
  EXPECTED_MODEL_FOR_UMTS_3G_VOICE,
  CASES_FOR_VOLTE_4G_VOICE,
  EXPECTED_MODEL_FOR_VOLTE_4G_VOICE,
  CASES_FOR_VOLTE_RCS,
  EXPECTED_MODEL_FOR_VOLTE_RCS,
  CASES_FOR_VZ_VISIBLE,
  EXPECTED_MODEL_FOR_VZ_VISIBLE,
  CASES_FOR_3G_PD,
  EXPECTED_MODEL_FOR_3G_PD,
} from './forms/__test__/deviceModeler-data-mock';

describe('DevicePanel', () => {
  let testMocks: any;

  beforeEach(() => {
    testMocks = {};

    testMocks.warrantJson = {
      id: 'warrant_id1',
    };
    testMocks.simpleDevicesList = [
      { device: '1' },
      { device: '3' },
      { device: '2' },
    ];
  });

  describe('getMaxDeviceId()', () => {
    it('should get Max Device Id from a device list where the highest value is in the middle position index', () => {
      expect(DevicePanel.getMaxDeviceId(testMocks.simpleDevicesList)).toBe(3);
    });

    it('should get the first Id as Max Device Id from a device list with only one element', () => {
      expect(DevicePanel.getMaxDeviceId([{ device: '0' }])).toBe(0);
    });

    it('should get Max Device Id from a device list where the only device has a falsy value', () => {
      const caseModel: any = { device: null };
      expect(DevicePanel.getMaxDeviceId([caseModel])).toBe(0);
    });
  });

  describe('getDevices()', () => {
    describe('when cases list is empty', () => {
      it('should return an empty devices object', () => {
        expect(DevicePanel.getDevices(CREATING_WARRANT, [], {}, {})).toEqual({});
      });
    });

    describe('when there are a valid cases list of LTE_4G_DATA', () => {
      it('should return the devices associated to LTE_4G_DATA', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_LTE_4G_DATA,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_LTE_4G_DATA);
      });
    });

    describe('when there are a valid cases list of GSMCDMA_2G_VOICE', () => {
      it('should return the devices associated to GSMCDMA_2G_VOICE', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_GSMCDMA_2G_VOICE,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_GSMCDMA_2G_VOICE);
      });
    });

    describe('when there are a valid cases list of LTE_GIZMO', () => {
      it('should return the devices associated to LTE_GIZMO', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_LTE_GIZMO,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_LTE_GIZMO);
      });
    });

    describe('when there are a valid cases list of LTE_IOT', () => {
      it('should return the devices associated to LTE_IOT', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_LTE_IOT,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_LTE_IOT);
      });
    });

    describe('when there are a valid cases list of PTT', () => {
      it('should return the devices associated to PTT', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_PTT,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_PTT);
      });
    });

    describe('when there are a valid cases list of UMTS_3G_VOICE', () => {
      it('should return the devices associated to UMTS_3G_VOICE', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_UMTS_3G_VOICE,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_UMTS_3G_VOICE);
      });
    });

    describe('when there are a valid cases list of VOLTE_4G_VOICE', () => {
      it('should return the devices associated to VOLTE_4G_VOICE', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_VOLTE_4G_VOICE,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_VOLTE_4G_VOICE);
      });
    });

    describe('when there are a valid cases list of VOLTE_RCS', () => {
      it('should return the devices associated to VOLTE_RCS', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_VOLTE_RCS,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_VOLTE_RCS);
      });
    });

    describe('when there are a valid cases list of VZ_VISIBLE', () => {
      it('should return the devices associated to VZ_VISIBLE', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_VZ_VISIBLE,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_VZ_VISIBLE);
      });
    });

    describe('when there are a valid cases list of 3G_PD', () => {
      it('should return the devices associated to 3G_PD', () => {
        expect(
          DevicePanel.getDevices(
            CREATING_WARRANT,
            CASES_FOR_3G_PD,
            CUSTOMER_TARGETS,
            {},
          ),
        ).toEqual(EXPECTED_MODEL_FOR_3G_PD);
      });
    });
  });
});
