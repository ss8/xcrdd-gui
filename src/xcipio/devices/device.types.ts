import { AccessFunctionMap } from 'data/accessFunction/accessFunction.types';
import { TargetModel, TargetModelWrapper } from 'data/target/target.types';
import { FormTemplateField } from 'data/template/template.types';

export type DeviceModel = {
  device: string
  caseId?: string
  caseIds?: string[]
  services: string[]
  serviceSet?: Set<string>
  afMapping?: AccessFunctionMap
  allTargetAfs: TargetModel[]
  types: DeviceModelType
}

export type DeviceModelType = {
  [targetType: string]: TargetModelWrapper[]
}

export type DeviceModelMap = {
  [deviceId: string]: DeviceModel
}

export type WarrantCaseModel = {
  cfs: string[]
  afs: string[]
  targets: TargetModel[]
  ignore?: boolean
  services: string[]
}

export type AccessFunctionsByCollectionFunctionTypeMap = {
  [collectionFunctionType: string]: Set<string>
}

export type ServicesByCollectionFunctionTypeMap = {
  [collectionFunctionType: string]: Set<string>
}

export type AccessFunctionsAndCollectionFunctionsByServiceMap = {
  [service: string]: {
    accessFunctions: string[]
    collectionFunctions: {
      [collectionFunctionType: string]: {
        accessFunctions: string[]
      }
    }
  }
}

export type TargetFieldMap = {
  [type: string]: TargetField
}

export type TargetField = {
  title: string
  subFields: {
    [field: string]: FormTemplateField
  }
}
