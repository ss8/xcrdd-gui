import React from 'react';
import {
  Label,
  Button,
  Tag,
  Intent,
} from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import isEqual from 'lodash.isequal';
import WarrantConfiguration, { UNSPECIFIED_SERVICE } from 'xcipio/config/warrant-config';
import {
  FormTemplate,
  FormTemplateFieldMap,
  FormTemplateOption,
  FormTemplateSection,
  Template,
} from 'data/template/template.types';
import { TargetModel } from 'data/target/target.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import DynamicForm, { ChangedField } from '../../../shared/form';
import TargetDetail from './target-detail';
import { DeviceModel } from '../device.types';
import './devices-form-list.scss';
import { TEMPORARY_CASE_ID_PREFIX } from './case-modeler';

type DeviceDetailsProps = {
  id: string
  warrantConfigInstance: WarrantConfiguration | null
  mode: string
  device: DeviceModel
  headers: string[]
  templateUsed: Template | null
  availableServiceOptions: FormTemplateOption[]
  disabled?: boolean
  delete: (id: string) => void
  onDeviceServiceUpdate: (deviceId: string, services: string[]) => void
  onDeviceTargetUpdate: (deviceId: string, target: TargetModel, targetType: string, targetIndex: number) => void
}

type DeviceDetailsState = {
  fields: FormTemplateFieldMap
}

export default class DeviceDetails extends React.Component<DeviceDetailsProps, DeviceDetailsState> {
  targetRefs: any = {};

  constructor(props: DeviceDetailsProps) {
    super(props);

    const {
      availableServiceOptions,
      disabled,
    } = this.props;

    const fields = this.getServiceFields(availableServiceOptions, disabled ?? false);

    this.state = {
      fields,
    };
  }

  async componentDidUpdate(prevProps: DeviceDetailsProps): Promise<void> {
    const {
      disabled,
      availableServiceOptions,
    } = this.props;

    if ((!isEqual(availableServiceOptions, prevProps.availableServiceOptions)) ||
        (disabled !== prevProps.disabled)) {
      const fields = this.getServiceFields(availableServiceOptions, disabled ?? false);

      this.setState({
        fields,
      });
    }
  }

  shouldDisableServices(disabled?: boolean): boolean {
    if (disabled === true) {
      return disabled;
    }

    // Check that all related case ids are not temporary ids
    return this.isEditMode() && !this.isNewDevice();
  }

  isEditMode(): boolean {
    const { mode } = this.props;
    return mode === CONFIG_FORM_MODE.Edit;
  }

  isNewDevice(): boolean {
    const { device } = this.props;
    return device.caseIds?.every((caseId) => caseId.startsWith(TEMPORARY_CASE_ID_PREFIX)) ?? true;
  }

  getServiceFields(availableServiceOptions: FormTemplateOption[], disabled: boolean): FormTemplateFieldMap {
    const {
      templateUsed,
      device: {
        services,
      },
    } = this.props;

    if (templateUsed == null) {
      return {};
    }

    const caseTemplate = templateUsed.template.case as unknown as FormTemplate;

    const fields: FormTemplateFieldMap = cloneDeep({
      services: (caseTemplate.general as FormTemplateSection).fields.services,
    });

    if (disabled) {
      fields.services.options = [];

    } else if (WarrantConfiguration.hasUnsupportedServices(services)) {
      fields.services.options = [UNSPECIFIED_SERVICE];

    } else {
      fields.services.options = [...availableServiceOptions];
    }

    fields.services.disabled = this.shouldDisableServices(disabled);
    fields.services.label = undefined;

    return fields;
  }

  isValid(): boolean {
    let isdeviceValid = true;
    Object.keys(this.targetRefs).forEach((key:any) => {
      if (this.targetRefs[key]) {
        const targetValid = this.targetRefs[key].isValid();
        isdeviceValid = isdeviceValid && targetValid;
      } else {
        delete this.targetRefs[key];
      }
    });
    return isdeviceValid;
  }

  servicesFieldUpdateHandler = (changedField: ChangedField): void => {
    const {
      value,
    } = changedField;

    const {
      id,
      onDeviceServiceUpdate,
    } = this.props;

    onDeviceServiceUpdate(id, value);
  }

  targetFieldUpdateHandler = (target: TargetModel, targetType: string, targetIndex: number): void => {
    const {
      id,
      onDeviceTargetUpdate,
    } = this.props;

    onDeviceTargetUpdate(id, target, targetType, targetIndex);
  }

  deleteDevice = (): void => {
    this.props.delete(this.props.id);
  }

  static getFormTemplateFields = (
    formTemplateFields: FormTemplateFieldMap,
    mode: string,
  ): FormTemplateFieldMap => {

    if (mode === CONFIG_FORM_MODE.View) {
      setAllTemplateFieldsAsReadOnly(formTemplateFields);
    }

    return formTemplateFields;
  }

  renderDeviceIndex(): JSX.Element {
    const {
      device: {
        device: deviceId,
      },
    } = this.props;

    return (
      <Label>
        {deviceId}
        {this.isNewDevice() && this.isEditMode() && (
          <div className="devices-form-list-body-row-device-new-tag">
            <Tag intent={Intent.SUCCESS}>NEW</Tag>
          </div>
        )}
      </Label>
    );
  }

  renderDeleteButton(disabled: boolean | undefined, mode: string): JSX.Element | null{
    if (mode === CONFIG_FORM_MODE.View) {
      return null;
    }

    return (
      <td className="devices-form-list-body-row-data devices-form-list-body-row-data-small devices-form-list-body-row-delete-icon">
        <Button
          title="Delete"
          icon="trash"
          intent="none"
          minimal
          onClick={this.deleteDevice}
          disabled={disabled}
        />
      </td>
    );
  }

  render(): JSX.Element {
    this.targetRefs = {};

    const {
      headers,
      device: {
        services,
        types,
      },
      warrantConfigInstance,
      disabled,
      mode,
    } = this.props;

    const {
      fields,
    } = this.state;

    let additionalClassName = '';
    if (this.isNewDevice() && this.isEditMode()) {
      additionalClassName = 'devices-form-list-body-row-new-device';
    }

    return (
      <tr data-test="deviceDetail-device" className={`devices-form-list-body-row ${additionalClassName}`} data-testid="DeviceDetailRow">
        {this.renderDeleteButton(disabled, mode)}
        <td className="devices-form-list-body-row-data devices-form-list-body-row-data-small devices-form-list-body-row-device-index" data-testid="DeviceDetailIndex">
          {this.renderDeviceIndex()}
        </td>
        <td className="devices-form-list-body-row-data" data-testid="DeviceDetailService">
          <div className="devices-form-list-service-form-input">
            <DynamicForm
              data-test="deviceDetail-form"
              name={`target_${new Date().getTime()}`}
              fields={DeviceDetails.getFormTemplateFields(fields, mode)}
              layout={[['services']]}
              defaults={{ services }}
              fieldUpdateHandler={this.servicesFieldUpdateHandler}
            />
          </div>
        </td>
        {
          headers.slice(3).map((header) => {
            const type = types[header];
            if (type) {
              return (
                type.map((tgt, index) => {
                  const nextfields = (warrantConfigInstance?.getTargetFields([tgt.target]))?.[header].subFields;
                  if (tgt.display) {
                    return (
                      <TargetDetail
                        data-testid={`targetDetails-${header}`}
                        ref={(ref) => { this.targetRefs[header + index] = ref; }}
                        key={header + index}
                        fields={nextfields}
                        targetType={header}
                        targetIndex={index}
                        target={tgt.target}
                        onTargetUpdate={this.targetFieldUpdateHandler}
                        mode={mode}
                      />
                    );
                  }
                  return <td key={header + index} />;
                }));
            }
            return <td key={header} />;
          })
        }
      </tr>
    );
  }
}
