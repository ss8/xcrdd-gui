export const DEVICE_GIZMO = {
  2: {
    device: '2',
    caseId: '',
    services: [
      'LTE-GIZMO',
    ],
    types: {
      MDN: [
        {
          target: {
            dxOwner: '',
            dxAccess: '',
            name: '',
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'GIZMO',
                  name: 'GIZMO',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            primaryType: 'MDN',
            primaryValue: '5353454345',
            secondaryType: '',
            secondaryValue: '',
            options: [
              {
                key: 'InterceptType',
                value: 'ChatOnly',
              },
            ],
            id: 'MDN_1607468060450',
            isRequired: true,
          },
          display: true,
          isFirst: true,
          ids: [],
        },
      ],
    },
    allTargetAfs: [
      {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'R2l6bW8',
              name: 'Gizmo',
              type: 'GIZMO',
              tag: 'GIZMO',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'GIZMO',
              name: 'GIZMO',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'MDN_1607468060450',
        isRequired: true,
      },
    ],
    afMapping: {
      GIZMO: [
        {
          id: 'R2l6bW8',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'Gizmo',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,

          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'GIZMO',
          city: '',
          stateName: '',
          country: '',
          tag: 'GIZMO',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
  },
};

export const EXPECTED_CASE_FOR_DEVICE_GIZMO = {
  services: [
    'LTE-GIZMO',
  ],
  cfOptions: expect.arrayContaining([
    expect.objectContaining({ label: 'CA-DOJBNE-33108' }),
    expect.objectContaining({ label: 'CookCounty-22108' }),
    expect.objectContaining({ label: 'CookCounty-33108' }),
  ]),
  allTargetAfs: [
    {
      afs: {
        afIds: expect.arrayContaining([
          expect.objectContaining({ tag: 'GIZMO' }),
        ]),
        afAutowireTypes: expect.arrayContaining([
          expect.objectContaining({ id: 'GIZMO' }),
        ]),
      },
      primaryType: 'MDN',
      isRequired: true,
    },
  ],
};

export const EXPECTED_TARGETS_FOR_DEVICE_GIZMO = {
  'DX-TARGET-1': {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [], afGroups: [], afAutowireTypes: [{ id: 'GIZMO', name: 'GIZMO' }], afAutowireGroups: [], afccc: [],
    },
    primaryType: 'MDN',
    primaryValue: '5353454345',
    secondaryType: '',
    secondaryValue: '',
    options: [{ key: 'InterceptType', value: 'ChatOnly' }],
    id: 'DX-TARGET-1',
    isRequired: true,
    caseId: { id: 'DX-CASE-1', name: 'Case 1' },
  },
};
