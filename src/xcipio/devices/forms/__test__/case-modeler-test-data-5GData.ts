export const DEVICE_5GDATA = {
  1: {
    device: '1',
    caseId: '',
    services: [
      '5G-DATA',
    ],
    types: {
      IMSI: [
        {
          target: {
            dxOwner: '',
            dxAccess: '',
            name: '',
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ERK_SMF',
                  name: 'ERK_SMF',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            primaryType: 'IMSI',
            primaryValue: '',
            secondaryType: '',
            secondaryValue: '',
            options: [],
            id: 'IMSI_1607423651818',
            isRequired: false,
          },
          display: true,
          isFirst: true,
          ids: [],
        },
      ],
      IMEI: [
        {
          target: {
            dxOwner: '',
            dxAccess: '',
            name: '',
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ERK_SMF',
                  name: 'ERK_SMF',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            primaryType: 'IMEI',
            primaryValue: '',
            secondaryType: '',
            secondaryValue: '',
            options: [],
            id: 'IMEI_1607423651818',
            isRequired: false,
          },
          display: true,
          isFirst: true,
          ids: [],
        },
      ],
      MSISDN: [
        {
          target: {
            dxOwner: '',
            dxAccess: '',
            name: '',
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ERK_SMF',
                  name: 'ERK_SMF',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            primaryType: 'MSISDN',
            primaryValue: '',
            secondaryType: '',
            secondaryValue: '',
            options: [],
            id: 'MSISDN_1607423651818',
            isRequired: false,
          },
          display: true,
          isFirst: true,
          ids: [],
        },
      ],
    },
    allTargetAfs: [
      {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'ZXJrc21mX2FmMQ',
              name: 'erksmf_af1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
            {
              id: 'ZXJrc21mX3gyTGlzbnIx',
              name: 'erksmf_x2Lisnr1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMSI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'IMSI_1607423651818',
        isRequired: false,
      },
      {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'ZXJrc21mX2FmMQ',
              name: 'erksmf_af1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
            {
              id: 'ZXJrc21mX3gyTGlzbnIx',
              name: 'erksmf_x2Lisnr1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMEI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'IMEI_1607423651818',
        isRequired: false,
      },
      {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'ZXJrc21mX2FmMQ',
              name: 'erksmf_af1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
            {
              id: 'ZXJrc21mX3gyTGlzbnIx',
              name: 'erksmf_x2Lisnr1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'MSISDN_1607423651818',
        isRequired: false,
      },
    ],
    afMapping: {
      ERK_SMF: [
        {
          id: 'ZXJrc21mX2FmMQ',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'erksmf_af1',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,

          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ERK_SMF',
          city: '',
          stateName: '',
          country: '',
          tag: 'ERK_SMF',
          version: '1.4',
          provisioningInterfaces: [
            {
              id: 'ZXJrc21mX2FmMSMx',
              name: 'ZXJrc21mX2FmMSMx',
              options: [],
            },
          ],
          dataInterfaces: [],
          contentInterfaces: [],
        },
        {
          id: 'ZXJrc21mX3gyTGlzbnIx',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'erksmf_x2Lisnr1',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,

          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ERK_SMF',
          city: '',
          stateName: '',
          country: '',
          tag: 'ERK_SMF',
          version: '1.4',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
  },
};

export const EXPECTED_CASE_FOR_DEVICE_5GDATA = {
  services: [
    '5G-DATA',
  ],
  cfOptions: expect.arrayContaining([
    expect.objectContaining({ label: 'CA-DOJBNE-5GCF' }),
    expect.objectContaining({ label: 'CA-DOJBNE-ETSI' }),
    expect.objectContaining({ label: 'CookCounty-5GCF' }),
    expect.objectContaining({ label: 'CookCounty-ETSI' }),
  ]),
  allTargetAfs: [
    {
      afs: {
        afIds: expect.arrayContaining([
          expect.objectContaining({ tag: 'ERK_SMF' }),
        ]),
        afAutowireTypes: [
          {
            id: 'ERK_SMF',
            name: 'ERK_SMF',
          },
        ],
      },
      primaryType: 'IMSI',
      isRequired: false,
    },
    {
      afs: {
        afIds: expect.arrayContaining([
          expect.objectContaining({ tag: 'ERK_SMF' }),
        ]),
        afAutowireTypes: [
          {
            id: 'ERK_SMF',
            name: 'ERK_SMF',
          },
        ],
      },
      primaryType: 'IMEI',
      isRequired: false,
    },
    {
      dxOwner: '',
      dxAccess: '',
      name: '',
      afs: {
        afIds: expect.arrayContaining([
          expect.objectContaining({ tag: 'ERK_SMF' }),
        ]),
        afAutowireTypes: [
          {
            id: 'ERK_SMF',
            name: 'ERK_SMF',
          },
        ],
      },
      primaryType: 'MSISDN',
      isRequired: false,
    },
  ],
};

export const EXPECTED_TARGETS_FOR_DEVICE_5GDATA = {
  'DX-TARGET-1': {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [], afGroups: [], afAutowireTypes: [{ id: 'ERK_SMF', name: 'ERK_SMF' }], afAutowireGroups: [], afccc: [],
    },
    primaryType: 'IMSI',
    primaryValue: '',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'DX-TARGET-1',
    isRequired: false,
    caseId: { id: 'DX-CASE-1', name: 'Case 1' },
  },
  'DX-TARGET-2': {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [], afGroups: [], afAutowireTypes: [{ id: 'ERK_SMF', name: 'ERK_SMF' }], afAutowireGroups: [], afccc: [],
    },
    primaryType: 'IMEI',
    primaryValue: '',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'DX-TARGET-2',
    isRequired: false,
    caseId: { id: 'DX-CASE-1', name: 'Case 1' },
  },
  'DX-TARGET-3': {
    dxOwner: '',
    dxAccess: '',
    name: '',
    afs: {
      afIds: [], afGroups: [], afAutowireTypes: [{ id: 'ERK_SMF', name: 'ERK_SMF' }], afAutowireGroups: [], afccc: [],
    },
    primaryType: 'MSISDN',
    primaryValue: '',
    secondaryType: '',
    secondaryValue: '',
    options: [],
    id: 'DX-TARGET-3',
    isRequired: false,
    caseId: { id: 'DX-CASE-1', name: 'Case 1' },
  },
};
