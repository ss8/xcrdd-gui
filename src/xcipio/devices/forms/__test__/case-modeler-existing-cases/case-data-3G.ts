import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_3G: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSAzRw',
    ],
    allTargetAfs: [
      {
        id: 'LOGIN_1611078693434',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'Q2lzY29TdGFyZW50UERT',
              name: 'CiscoStarentPDSN',
              type: 'STA_PDSN',
              tag: 'STA_PDSN',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'Q2lzY29TdGFyZW50RUhB',
              name: 'CiscoStarentEHA',
              type: 'STARENT_EHA',
              tag: 'STARENT_EHA',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'LOGIN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'MDN_1611078693434',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'Q2lzY29TdGFyZW50UERT',
              name: 'CiscoStarentPDSN',
              type: 'STA_PDSN',
              tag: 'STA_PDSN',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'Q2lzY29TdGFyZW50RUhB',
              name: 'CiscoStarentEHA',
              type: 'STARENT_EHA',
              tag: 'STARENT_EHA',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'IP4_1611078693434',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'Q2lzY29TdGFyZW50UERT',
              name: 'CiscoStarentPDSN',
              type: 'STA_PDSN',
              tag: 'STA_PDSN',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'Q2lzY29TdGFyZW50RUhB',
              name: 'CiscoStarentEHA',
              type: 'STARENT_EHA',
              tag: 'STARENT_EHA',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IP4',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    afMapping: {
      STARENT_EHA: [
        {
          id: 'Q2lzY29TdGFyZW50RUhB',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'CiscoStarentEHA',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'STARENT_EHA',
          city: '',
          stateName: '',
          country: '',
          tag: 'STARENT_EHA',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
      STA_PDSN: [
        {
          id: 'Q2lzY29TdGFyZW50UERT',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'CiscoStarentPDSN',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'STA_PDSN',
          city: '',
          stateName: '',
          country: '',
          tag: 'STA_PDSN',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'CDMA2000-3G-DATA',
    ],
    types: {
      LOGIN: [
        {
          target: {
            id: 'MXQgLSAzRyMx',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'Q2lzY29TdGFyZW50UERT',
                  name: 'CiscoStarentPDSN',
                  type: 'STA_PDSN',
                  tag: 'STA_PDSN',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
                {
                  id: 'Q2lzY29TdGFyZW50RUhB',
                  name: 'CiscoStarentEHA',
                  type: 'STARENT_EHA',
                  tag: 'STARENT_EHA',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSAzRw',
              name: '1t - 3G',
            },
            options: [],
            primaryType: 'LOGIN',
            primaryValue: 'login',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: true,
          ids: [
            'MXQgLSAzRyMx',
          ],
        },
      ],
      MDN: [
        {
          target: {
            id: 'MXQgLSAzRyMy',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'Q2lzY29TdGFyZW50UERT',
                  name: 'CiscoStarentPDSN',
                  type: 'STA_PDSN',
                  tag: 'STA_PDSN',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
                {
                  id: 'Q2lzY29TdGFyZW50RUhB',
                  name: 'CiscoStarentEHA',
                  type: 'STARENT_EHA',
                  tag: 'STARENT_EHA',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSAzRw',
              name: '1t - 3G',
            },
            options: [
              {
                key: 'InterceptType',
                value: 'VoiceOnly',
              },
            ],
            primaryType: 'MDN',
            primaryValue: '9111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: true,
          ids: [
            'MXQgLSAzRyMy',
          ],
        },
      ],
      IP4: [
        {
          target: {
            id: 'MXQgLSAzRyMz',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'Q2lzY29TdGFyZW50UERT',
                  name: 'CiscoStarentPDSN',
                  type: 'STA_PDSN',
                  tag: 'STA_PDSN',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
                {
                  id: 'Q2lzY29TdGFyZW50RUhB',
                  name: 'CiscoStarentEHA',
                  type: 'STARENT_EHA',
                  tag: 'STARENT_EHA',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSAzRw',
              name: '1t - 3G',
            },
            options: [],
            primaryType: 'IP4',
            primaryValue: '1.1.1.1',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSAzRyMz',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_3G: CaseModel[] = [
  {
    id: 'MXQgLSAzRw',
    dxOwner: '',
    dxAccess: '',
    name: '1t - 3G',
    warrantId: {
      id: 'MTAyNjA4NzM4MQ',
      name: '1t - 3G',
    },
    liId: 'Case 1',
    coId: '1t - 3G',
    device: '1',
    services: [
      'CDMA2000-3G-DATA',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLVRJQTEw',
      name: 'CA-DOJBNE-TIA1072',
      cfId: 'CA-DOJBNE-TIA10',
      tag: 'CF-TIA-1072',
    },
    entryDateTime: '2021-01-19T17:51:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLVRJQTEwIzE',
        name: 'Q0EtRE9KQk5FLVRJQTEwIzE',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSAzRyMx',
        name: 'MXQgLSAzRyMx',
      },
      {
        id: 'MXQgLSAzRyMy',
        name: 'MXQgLSAzRyMy',
      },
      {
        id: 'MXQgLSAzRyMz',
        name: 'MXQgLSAzRyMz',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_3G: TargetModel[] = [
  {
    id: 'MXQgLSAzRyMx',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'Q2lzY29TdGFyZW50RUhB',
          name: 'CiscoStarentEHA',
          tag: 'STARENT_EHA',
        },
        {
          id: 'Q2lzY29TdGFyZW50UERT',
          name: 'CiscoStarentPDSN',
          tag: 'STA_PDSN',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSAzRw',
      name: '1t - 3G',
    },
    options: [],
    primaryType: 'LOGIN',
    primaryValue: 'login',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSAzRyMy',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'Q2lzY29TdGFyZW50RUhB',
          name: 'CiscoStarentEHA',
          tag: 'STARENT_EHA',
        },
        {
          id: 'Q2lzY29TdGFyZW50UERT',
          name: 'CiscoStarentPDSN',
          tag: 'STA_PDSN',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSAzRw',
      name: '1t - 3G',
    },
    options: [
      {
        key: 'InterceptType',
        value: 'VoiceOnly',
      },
    ],
    primaryType: 'MDN',
    primaryValue: '9111111111',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSAzRyMz',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'Q2lzY29TdGFyZW50RUhB',
          name: 'CiscoStarentEHA',
          tag: 'STARENT_EHA',
        },
        {
          id: 'Q2lzY29TdGFyZW50UERT',
          name: 'CiscoStarentPDSN',
          tag: 'STA_PDSN',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSAzRw',
      name: '1t - 3G',
    },
    options: [],
    primaryType: 'IP4',
    primaryValue: '1.1.1.1',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_3G = {
  [CASE_DATA_3G[0].id]: {
    ...CASE_DATA_3G[0],
    afMapping: DEVICE_DATA_3G[1].afMapping,
    allTargetAfs: DEVICE_DATA_3G[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_3G[1].afMapping ?? {})),
  },
};
