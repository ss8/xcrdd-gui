import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_VOICE: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSBWb2ljZQ',
    ],
    allTargetAfs: [
      {
        id: 'SIP-URI_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'SIP-URI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'DN_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'DN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'PARTIAL_DN_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'PARTIAL_DN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'TEL-URI_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'TEL-URI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'IMSI_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMSI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'IMEI_1611091436683',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMEI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    afMapping: {
      ACME_X123: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'ACMEOracleX123',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ACME_X123',
          city: '',
          stateName: '',
          country: '',
          tag: 'ACME_X123',
          version: '1.0',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'VOIP-VOICE',
    ],
    types: {
      MDN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'MDN',
            primaryValue: '8222222222',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      'SIP-URI': [
        {
          target: {
            id: 'MXQgLSBWb2ljZSMx',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'SIP-URI',
            primaryValue: 'sip:+18222222222@vzims.com',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: false,
          ids: [
            'MXQgLSBWb2ljZSMx',
          ],
        },
      ],
      'TEL-URI': [
        {
          target: {
            id: 'MXQgLSBWb2ljZSMy',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'TEL-URI',
            primaryValue: 'tel:+18222222222',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: false,
          ids: [
            'MXQgLSBWb2ljZSMy',
          ],
        },
      ],
      DN: [
        {
          target: {
            id: 'MXQgLSBWb2ljZSMz',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'DN',
            primaryValue: '833333333333333',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBWb2ljZSMz',
          ],
        },
      ],
      PARTIAL_DN: [
        {
          target: {
            id: 'MXQgLSBWb2ljZSM0',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'PARTIAL_DN',
            primaryValue: '844444444444',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBWb2ljZSM0',
          ],
        },
      ],
      IMSI: [
        {
          target: {
            id: 'MXQgLSBWb2ljZSM1',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'IMSI',
            primaryValue: '855555555555555',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBWb2ljZSM1',
          ],
        },
      ],
      IMEI: [
        {
          target: {
            id: 'MXQgLSBWb2ljZSM2',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBWb2ljZQ',
              name: '1t - Voice',
            },
            options: [],
            primaryType: 'IMEI',
            primaryValue: '8666666666666666',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBWb2ljZSM2',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_VOICE: CaseModel[] = [
  {
    id: 'MXQgLSBWb2ljZQ',
    dxOwner: '',
    dxAccess: '',
    name: '1t - Voice',
    warrantId: {
      id: 'NjY4MTUwNzE1',
      name: '1t - Voice',
    },
    liId: 'Case 1',
    coId: '1t - Voice',
    device: '1',
    services: [
      'VOIP-VOICE',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLTEwMDA2',
      name: 'CA-DOJBNE-1000678',
      cfId: 'CA-DOJBNE-10006',
      tag: 'CF-ATIS-1000678',
    },
    entryDateTime: '2021-01-19T21:23:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLTEwMDA2IzE',
        name: 'Q0EtRE9KQk5FLTEwMDA2IzE',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSBWb2ljZSMx',
        name: 'MXQgLSBWb2ljZSMx',
      },
      {
        id: 'MXQgLSBWb2ljZSMy',
        name: 'MXQgLSBWb2ljZSMy',
      },
      {
        id: 'MXQgLSBWb2ljZSMz',
        name: 'MXQgLSBWb2ljZSMz',
      },
      {
        id: 'MXQgLSBWb2ljZSM0',
        name: 'MXQgLSBWb2ljZSM0',
      },
      {
        id: 'MXQgLSBWb2ljZSM1',
        name: 'MXQgLSBWb2ljZSM1',
      },
      {
        id: 'MXQgLSBWb2ljZSM2',
        name: 'MXQgLSBWb2ljZSM2',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_VOICE: TargetModel[] = [
  {
    id: 'MXQgLSBWb2ljZSMx',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'SIP-URI',
    primaryValue: 'sip:+18222222222@vzims.com',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBWb2ljZSMy',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'TEL-URI',
    primaryValue: 'tel:+18222222222',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBWb2ljZSMz',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'DN',
    primaryValue: '833333333333333',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBWb2ljZSM0',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'PARTIAL_DN',
    primaryValue: '844444444444',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBWb2ljZSM1',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'IMSI',
    primaryValue: '855555555555555',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBWb2ljZSM2',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          name: 'ACMEOracleX123',
          tag: 'ACME_X123',
        },
      ],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBWb2ljZQ',
      name: '1t - Voice',
    },
    options: [],
    primaryType: 'IMEI',
    primaryValue: '8666666666666666',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_VOICE = {
  [CASE_DATA_VOICE[0].id]: {
    ...CASE_DATA_VOICE[0],
    afMapping: DEVICE_DATA_VOICE[1].afMapping,
    allTargetAfs: DEVICE_DATA_VOICE[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_VOICE[1].afMapping ?? {})),
  },
};
