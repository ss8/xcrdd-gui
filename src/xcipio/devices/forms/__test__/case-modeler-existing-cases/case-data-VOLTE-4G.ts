import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_VOLTE_4G: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'NEcgdm9pY2U',
      'NEcgdm9pY2UwMA',
    ],
    allTargetAfs: [
      {
        id: 'SIP-URI_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'TlNOQ1NDRg',
              name: 'NSNCSCF',
              type: 'NSN_CSCF',
              tag: 'NSN_CSCF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'QWNtZVBhY2tldEdlblZv',
              name: 'AcmePacketGenVoIP',
              type: 'GENVOIP',
              tag: 'GENVOIP',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'U29udXNTQkM',
              name: 'SonusSBC',
              type: 'GENVOIP',
              tag: 'SONUS_SBC',
              model: 'SONUS_SBC',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'NSN_CSCF',
              name: 'NSN_CSCF',
            },
            {
              id: 'GENVOIP',
              name: 'GENVOIP',
            },
            {
              id: 'SONUS_SBC',
              name: 'SONUS_SBC',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'SIP-URI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'DN_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'DN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'PARTIAL_DN_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'PARTIAL_DN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'TEL-URI_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'TlNOQ1NDRg',
              name: 'NSNCSCF',
              type: 'NSN_CSCF',
              tag: 'NSN_CSCF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'QWNtZVBhY2tldEdlblZv',
              name: 'AcmePacketGenVoIP',
              type: 'GENVOIP',
              tag: 'GENVOIP',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'U29udXNTQkM',
              name: 'SonusSBC',
              type: 'GENVOIP',
              tag: 'SONUS_SBC',
              model: 'SONUS_SBC',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'NSN_CSCF',
              name: 'NSN_CSCF',
            },
            {
              id: 'GENVOIP',
              name: 'GENVOIP',
            },
            {
              id: 'SONUS_SBC',
              name: 'SONUS_SBC',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'TEL-URI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'IMSI_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMSI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'IMEI_1611081699594',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'QUNNRU9yYWNsZVgxMjM',
              name: 'ACMEOracleX123',
              type: 'ACME_X123',
              tag: 'ACME_X123',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMEI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    afMapping: {
      ACME_X123: [
        {
          id: 'QUNNRU9yYWNsZVgxMjM',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'ACMEOracleX123',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ACME_X123',
          city: '',
          stateName: '',
          country: '',
          tag: 'ACME_X123',
          version: '1.0',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
      GENVOIP: [
        {
          id: 'QWNtZVBhY2tldEdlblZv',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'AcmePacketGenVoIP',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'GENVOIP',
          city: '',
          stateName: '',
          country: '',
          tag: 'GENVOIP',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
      NSN_CSCF: [
        {
          id: 'TlNOQ1NDRg',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'NSNCSCF',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'NSN_CSCF',
          city: '',
          stateName: '',
          country: '',
          tag: 'NSN_CSCF',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
      SONUS_SBC: [
        {
          id: 'U29udXNTQkM',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'SonusSBC',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: 'SONUS_SBC',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'GENVOIP',
          city: '',
          stateName: '',
          country: '',
          tag: 'SONUS_SBC',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'VOLTE-4G-VOICE',
    ],
    types: {
      MDN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'NEcgdm9pY2U',
              name: '4G voice',
            },
            options: [],
            primaryType: 'MDN',
            primaryValue: '1231211111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      'SIP-URI': [
        {
          target: {
            id: 'NEcgdm9pY2UjMQ',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'NSN_CSCF',
                  name: 'NSN_CSCF',
                },
                {
                  id: 'GENVOIP',
                  name: 'GENVOIP',
                },
                {
                  id: 'SONUS_SBC',
                  name: 'SONUS_SBC',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2U',
              name: '4G voice',
            },
            options: [],
            primaryType: 'SIP-URI',
            primaryValue: 'sip:+11231211111@vzims.com',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: false,
          ids: [
            'NEcgdm9pY2UjMQ',
            'NEcgdm9pY2UwMCMx',
          ],
        },
      ],
      'TEL-URI': [
        {
          target: {
            id: 'NEcgdm9pY2UjMg',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'NSN_CSCF',
                  name: 'NSN_CSCF',
                },
                {
                  id: 'GENVOIP',
                  name: 'GENVOIP',
                },
                {
                  id: 'SONUS_SBC',
                  name: 'SONUS_SBC',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2U',
              name: '4G voice',
            },
            options: [],
            primaryType: 'TEL-URI',
            primaryValue: 'tel:+11231211111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: false,
          ids: [
            'NEcgdm9pY2UjMg',
            'NEcgdm9pY2UwMCMy',
          ],
        },
      ],
      DN: [
        {
          target: {
            id: 'NEcgdm9pY2UwMCMz',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2UwMA',
              name: '4G voice00',
            },
            options: [],
            primaryType: 'DN',
            primaryValue: '222222222222222',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'NEcgdm9pY2UwMCMz',
          ],
        },
      ],
      PARTIAL_DN: [
        {
          target: {
            id: 'NEcgdm9pY2UwMCM0',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2UwMA',
              name: '4G voice00',
            },
            options: [],
            primaryType: 'PARTIAL_DN',
            primaryValue: '333333333333',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'NEcgdm9pY2UwMCM0',
          ],
        },
      ],
      IMSI: [
        {
          target: {
            id: 'NEcgdm9pY2UwMCM1',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2UwMA',
              name: '4G voice00',
            },
            options: [],
            primaryType: 'IMSI',
            primaryValue: '444444444444444',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'NEcgdm9pY2UwMCM1',
          ],
        },
      ],
      IMEI: [
        {
          target: {
            id: 'NEcgdm9pY2UwMCM2',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [
                {
                  id: 'QUNNRU9yYWNsZVgxMjM',
                  name: 'ACMEOracleX123',
                  type: 'ACME_X123',
                  tag: 'ACME_X123',
                  model: '',
                  x3TargetAssociations: null,
                  bAFAssociation: true,
                  contentInterfaces: [],
                },
              ],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'NEcgdm9pY2UwMA',
              name: '4G voice00',
            },
            options: [],
            primaryType: 'IMEI',
            primaryValue: '5555555555555555',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'NEcgdm9pY2UwMCM2',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_VOLTE_4G: CaseModel[] = [
  {
    id: 'NEcgdm9pY2U',
    dxOwner: '',
    dxAccess: '',
    name: '4G voice',
    warrantId: {
      id: 'NDEzODc3ODg0',
      name: '4G voice',
    },
    liId: 'Case 1',
    coId: '4G voice',
    device: '1',
    services: [
      'VOLTE-4G-VOICE',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLUFUSVM',
      name: 'CA-DOJBNE-ATIS',
      cfId: 'CA-DOJBNE-ATIS',
      tag: 'CF-ATIS-0700005',
    },
    entryDateTime: '2021-01-18T21:22:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLUFUSVMjMQ',
        name: 'Q0EtRE9KQk5FLUFUSVMjMQ',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'NEcgdm9pY2UjMQ',
        name: 'NEcgdm9pY2UjMQ',
      },
      {
        id: 'NEcgdm9pY2UjMg',
        name: 'NEcgdm9pY2UjMg',
      },
    ],
    reporting: {},
  },
  {
    id: 'NEcgdm9pY2UwMA',
    dxOwner: '',
    dxAccess: '',
    name: '4G voice00',
    warrantId: {
      id: 'NDEzODc3ODg0',
      name: '4G voice',
    },
    liId: 'Case 2',
    coId: '4G voice00',
    device: '1',
    services: [
      'VOLTE-4G-VOICE',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLTVHQ0Y',
      name: 'CA-DOJBNE-5GCF',
      cfId: 'CA-DOJBNE-5GCF',
      tag: 'CF-ETSI-102232',
    },
    entryDateTime: '2021-01-18T21:22:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLTVHQ0YjMQ',
        name: 'Q0EtRE9KQk5FLTVHQ0YjMQ',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'NEcgdm9pY2UwMCMx',
        name: 'NEcgdm9pY2UwMCMx',
      },
      {
        id: 'NEcgdm9pY2UwMCMy',
        name: 'NEcgdm9pY2UwMCMy',
      },
      {
        id: 'NEcgdm9pY2UwMCMz',
        name: 'NEcgdm9pY2UwMCMz',
      },
      {
        id: 'NEcgdm9pY2UwMCM0',
        name: 'NEcgdm9pY2UwMCM0',
      },
      {
        id: 'NEcgdm9pY2UwMCM1',
        name: 'NEcgdm9pY2UwMCM1',
      },
      {
        id: 'NEcgdm9pY2UwMCM2',
        name: 'NEcgdm9pY2UwMCM2',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_VOLTE_4G: TargetModel[] = [
  {
    id: 'NEcgdm9pY2UjMQ',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'GENVOIP',
          name: 'GENVOIP',
          tag: 'GENVOIP',
        },
        {
          id: 'NSN_CSCF',
          name: 'NSN_CSCF',
          tag: 'NSN_CSCF',
        },
        {
          id: 'SONUS_SBC',
          name: 'SONUS_SBC',
          tag: 'SONUS_SBC',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'NEcgdm9pY2U',
      name: '4G voice',
    },
    options: [],
    primaryType: 'SIP-URI',
    primaryValue: 'sip:+11231211111@vzims.com',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'NEcgdm9pY2UjMg',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'GENVOIP',
          name: 'GENVOIP',
          tag: 'GENVOIP',
        },
        {
          id: 'NSN_CSCF',
          name: 'NSN_CSCF',
          tag: 'NSN_CSCF',
        },
        {
          id: 'SONUS_SBC',
          name: 'SONUS_SBC',
          tag: 'SONUS_SBC',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'NEcgdm9pY2U',
      name: '4G voice',
    },
    options: [],
    primaryType: 'TEL-URI',
    primaryValue: 'tel:+11231211111',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_VOLTE_4G = {
  [CASE_DATA_VOLTE_4G[0].id]: {
    ...CASE_DATA_VOLTE_4G[0],
    afMapping: DEVICE_DATA_VOLTE_4G[1].afMapping,
    allTargetAfs: [{
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0],
      afs: {
        ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs,
        afAutowireTypes: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs?.afAutowireTypes[0],
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs?.afAutowireTypes[2],
        ],
        afIds: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs?.afIds[1],
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs?.afIds[3],
        ],
      },
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3],
      afs: {
        ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs,
        afAutowireTypes: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs?.afAutowireTypes[0],
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs?.afAutowireTypes[2],
        ],
        afIds: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs?.afIds[1],
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs?.afIds[3],
        ],
      },
    }],
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(['NSN_CSCF', 'SONUS_SBC']),
  },
  [CASE_DATA_VOLTE_4G[1].id]: {
    ...CASE_DATA_VOLTE_4G[1],
    afMapping: DEVICE_DATA_VOLTE_4G[1].afMapping,
    allTargetAfs: [{
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0],
      afs: {
        ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs,
        afAutowireTypes: [],
        afIds: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[0].afs?.afIds[0],
        ],
      },
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[1],
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[2],
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3],
      afs: {
        ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs,
        afAutowireTypes: [],
        afIds: [
          DEVICE_DATA_VOLTE_4G[1].allTargetAfs[3].afs?.afIds[0],
        ],
      },
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[4],
    }, {
      ...DEVICE_DATA_VOLTE_4G[1].allTargetAfs[5],
    }],
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(['ACME_X123']),
  },
};
