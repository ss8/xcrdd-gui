import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_GIZMO: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSBHaXptbw',
    ],
    allTargetAfs: [
      {
        id: 'MDN_1611090991055',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'R2l6bW8',
              name: 'Gizmo',
              type: 'GIZMO',
              tag: 'GIZMO',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'GIZMO',
              name: 'GIZMO',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
    ],
    afMapping: {
      GIZMO: [
        {
          id: 'R2l6bW8',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'Gizmo',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'GIZMO',
          city: '',
          stateName: '',
          country: '',
          tag: 'GIZMO',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'LTE-GIZMO',
    ],
    types: {
      MDN: [
        {
          target: {
            id: 'MXQgLSBHaXptbyMx',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'GIZMO',
                  name: 'GIZMO',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBHaXptbw',
              name: '1t - Gizmo',
            },
            options: [
              {
                key: 'InterceptType',
                value: 'VoiceOnly',
              },
            ],
            primaryType: 'MDN',
            primaryValue: '8111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: true,
          ids: [
            'MXQgLSBHaXptbyMx',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_GIZMO: CaseModel[] = [
  {
    id: 'MXQgLSBHaXptbw',
    dxOwner: '',
    dxAccess: '',
    name: '1t - Gizmo',
    warrantId: {
      id: 'MjA4MjE4ODM3OQ',
      name: '1t - Gizmo',
    },
    liId: 'Case 1',
    coId: '1t - Gizmo',
    device: '1',
    services: [
      'LTE-GIZMO',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLTMzMTA4',
      name: 'CA-DOJBNE-33108',
      cfId: 'CA-DOJBNE-33108',
      tag: 'CF-3GPP-33108',
    },
    entryDateTime: '2021-01-19T21:16:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLTMzMTA4IzE',
        name: 'Q0EtRE9KQk5FLTMzMTA4IzE',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSBHaXptbyMx',
        name: 'MXQgLSBHaXptbyMx',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_GIZMO: TargetModel[] = [
  {
    id: 'MXQgLSBHaXptbyMx',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'GIZMO',
          name: 'GIZMO',
          tag: 'GIZMO',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBHaXptbw',
      name: '1t - Gizmo',
    },
    options: [
      {
        key: 'InterceptType',
        value: 'VoiceOnly',
      },
    ],
    primaryType: 'MDN',
    primaryValue: '8111111111',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_GIZMO = {
  [CASE_DATA_GIZMO[0].id]: {
    ...CASE_DATA_GIZMO[0],
    afMapping: DEVICE_DATA_GIZMO[1].afMapping,
    allTargetAfs: DEVICE_DATA_GIZMO[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_GIZMO[1].afMapping ?? {})),
  },
};
