import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_UMTS: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSBVVE1T',
    ],
    allTargetAfs: [
      {
        id: 'IMSI_1611079105701',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'MQ',
              name: 'ALUDSS',
              type: 'ALU_DSS',
              tag: 'ALU_DSS',
              model: 'SGS-IWF',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ALU_DSS',
              name: 'ALU_DSS',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMSI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'IMEI_1611079105701',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'MQ',
              name: 'ALUDSS',
              type: 'ALU_DSS',
              tag: 'ALU_DSS',
              model: 'SGS-IWF',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ALU_DSS',
              name: 'ALU_DSS',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMEI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'MSISDN_1611079105701',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'MQ',
              name: 'ALUDSS',
              type: 'ALU_DSS',
              tag: 'ALU_DSS',
              model: 'SGS-IWF',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ALU_DSS',
              name: 'ALU_DSS',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    afMapping: {
      ALU_DSS: [
        {
          id: 'MQ',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'ALUDSS',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'ALL',
          model: 'SGS-IWF',
          preprovisioningLead: '000:00',
          serialNumber: '45345345',
          timeZone: 'America/Tijuana',
          type: 'ALU_DSS',
          city: '',
          stateName: '',
          country: '',
          tag: 'ALU_DSS',
          version: '0302',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'UMTS-3G-VOICE',
    ],
    types: {
      MDN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'MXQgLSBVVE1T',
              name: '1t - UTMS',
            },
            options: [],
            primaryType: 'MDN',
            primaryValue: '9111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      MSISDN: [
        {
          target: {
            id: 'MXQgLSBVVE1TIzE',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ALU_DSS',
                  name: 'ALU_DSS',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBVVE1T',
              name: '1t - UTMS',
            },
            options: [],
            primaryType: 'MSISDN',
            primaryValue: '19111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: false,
          ids: [
            'MXQgLSBVVE1TIzE',
          ],
        },
      ],
      IMSI: [
        {
          target: {
            id: 'MXQgLSBVVE1TIzI',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ALU_DSS',
                  name: 'ALU_DSS',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBVVE1T',
              name: '1t - UTMS',
            },
            options: [],
            primaryType: 'IMSI',
            primaryValue: '922222222222222',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: true,
          ids: [
            'MXQgLSBVVE1TIzI',
          ],
        },
      ],
      IMEI: [
        {
          target: {
            id: 'MXQgLSBVVE1TIzM',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ALU_DSS',
                  name: 'ALU_DSS',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBVVE1T',
              name: '1t - UTMS',
            },
            options: [],
            primaryType: 'IMEI',
            primaryValue: '9333333333333333',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBVVE1TIzM',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_UMTS: CaseModel[] = [
  {
    id: 'MXQgLSBVVE1T',
    dxOwner: '',
    dxAccess: '',
    name: '1t - UTMS',
    warrantId: {
      id: 'MjEyNTc2MjExNQ',
      name: '1t - UTMS',
    },
    liId: 'Case 1',
    coId: '1t - UTMS',
    device: '1',
    services: [
      'UMTS-3G-VOICE',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLUoyNUE',
      name: 'CA-DOJBNE-J25A',
      cfId: 'CA-DOJBNE-J25A',
      tag: 'CF-JSTD025A',
    },
    entryDateTime: '2021-01-19T17:58:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLUoyNUEjMQ',
        name: 'Q0EtRE9KQk5FLUoyNUEjMQ',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSBVVE1TIzE',
        name: 'MXQgLSBVVE1TIzE',
      },
      {
        id: 'MXQgLSBVVE1TIzI',
        name: 'MXQgLSBVVE1TIzI',
      },
      {
        id: 'MXQgLSBVVE1TIzM',
        name: 'MXQgLSBVVE1TIzM',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_UMTS: TargetModel[] = [
  {
    id: 'MXQgLSBVVE1TIzE',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'ALU_DSS',
          name: 'ALU_DSS',
          tag: 'ALU_DSS',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBVVE1T',
      name: '1t - UTMS',
    },
    options: [],
    primaryType: 'MSISDN',
    primaryValue: '19111111111',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBVVE1TIzI',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'ALU_DSS',
          name: 'ALU_DSS',
          tag: 'ALU_DSS',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBVVE1T',
      name: '1t - UTMS',
    },
    options: [],
    primaryType: 'IMSI',
    primaryValue: '922222222222222',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBVVE1TIzM',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'ALU_DSS',
          name: 'ALU_DSS',
          tag: 'ALU_DSS',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBVVE1T',
      name: '1t - UTMS',
    },
    options: [],
    primaryType: 'IMEI',
    primaryValue: '9333333333333333',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_UMTS = {
  [CASE_DATA_UMTS[0].id]: {
    ...CASE_DATA_UMTS[0],
    afMapping: DEVICE_DATA_UMTS[1].afMapping,
    allTargetAfs: DEVICE_DATA_UMTS[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_UMTS[1].afMapping ?? {})),
  },
};
