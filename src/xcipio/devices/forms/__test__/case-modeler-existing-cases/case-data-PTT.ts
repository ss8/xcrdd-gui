import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_PTT: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSBQVFQ',
    ],
    allTargetAfs: [
      {
        id: 'MSISDN_1611091307891',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'S29kaWFrUFRU',
              name: 'KodiakPTT',
              type: 'KODIAK_PTT',
              tag: 'KODIAK_PTT',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'KODIAK_PTT',
              name: 'KODIAK_PTT',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
    ],
    afMapping: {
      KODIAK_PTT: [
        {
          id: 'S29kaWFrUFRU',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'KodiakPTT',
          contact: {
            id: '',
            name: '',
          },
          insideNat: true,
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'KODIAK_PTT',
          city: '',
          stateName: '',
          country: '',
          tag: 'KODIAK_PTT',
          version: '2',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'PTT',
    ],
    types: {
      MDN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'MXQgLSBQVFQ',
              name: '1t - PTT',
            },
            options: [],
            primaryType: 'MDN',
            primaryValue: '8333333333',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      MSISDN: [
        {
          target: {
            id: 'MXQgLSBQVFQjMQ',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'KODIAK_PTT',
                  name: 'KODIAK_PTT',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBQVFQ',
              name: '1t - PTT',
            },
            options: [],
            primaryType: 'MSISDN',
            primaryValue: '18333333333',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: false,
          ids: [
            'MXQgLSBQVFQjMQ',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_PTT: CaseModel[] = [
  {
    id: 'MXQgLSBQVFQ',
    dxOwner: '',
    dxAccess: '',
    name: '1t - PTT',
    warrantId: {
      id: 'MTU0NDIwNjExMA',
      name: '1t - PTT',
    },
    liId: 'Case 1',
    coId: '1t - PTT',
    device: '1',
    services: [
      'PTT',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLVRJQTEw',
      name: 'CA-DOJBNE-TIA1072',
      cfId: 'CA-DOJBNE-TIA10',
      tag: 'CF-TIA-1072',
    },
    entryDateTime: '2021-01-19T21:21:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLVRJQTEwIzE',
        name: 'Q0EtRE9KQk5FLVRJQTEwIzE',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSBQVFQjMQ',
        name: 'MXQgLSBQVFQjMQ',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_PTT: TargetModel[] = [
  {
    id: 'MXQgLSBQVFQjMQ',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'KODIAK_PTT',
          name: 'KODIAK_PTT',
          tag: 'KODIAK_PTT',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBQVFQ',
      name: '1t - PTT',
    },
    options: [],
    primaryType: 'MSISDN',
    primaryValue: '18333333333',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_PTT = {
  [CASE_DATA_PTT[0].id]: {
    ...CASE_DATA_PTT[0],
    afMapping: DEVICE_DATA_PTT[1].afMapping,
    allTargetAfs: DEVICE_DATA_PTT[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_PTT[1].afMapping ?? {})),
  },
};
