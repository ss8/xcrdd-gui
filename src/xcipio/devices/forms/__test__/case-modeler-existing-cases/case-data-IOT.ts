import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { DeviceModelMap } from 'xcipio/devices/device.types';

export const DEVICE_DATA_IOT: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSBJb1Q',
    ],
    allTargetAfs: [
      {
        id: 'MSISDN_1611091126477',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'RXJpY3Nzb25TQ0VGSU4',
              name: 'EricssonSCEFINI2',
              type: 'ERK_SCEF',
              tag: 'ERK_SCEF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'RXJpY3Nzb25TQ0VGSU5J',
              name: 'EricssonSCEFINI1',
              type: 'ERK_SCEF',
              tag: 'ERK_SCEF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SCEF',
              name: 'ERK_SCEF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: true,
      },
      {
        id: 'EXTERNAL_ID_1611091126477',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'RXJpY3Nzb25TQ0VGSU4',
              name: 'EricssonSCEFINI2',
              type: 'ERK_SCEF',
              tag: 'ERK_SCEF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'RXJpY3Nzb25TQ0VGSU5J',
              name: 'EricssonSCEFINI1',
              type: 'ERK_SCEF',
              tag: 'ERK_SCEF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SCEF',
              name: 'ERK_SCEF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'EXTERNAL_ID',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    afMapping: {
      ERK_SCEF: [
        {
          id: 'RXJpY3Nzb25TQ0VGSU4',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'EricssonSCEFINI2',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'INI2',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ERK_SCEF',
          city: '',
          stateName: '',
          country: '',
          tag: 'ERK_SCEF',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
        {
          id: 'RXJpY3Nzb25TQ0VGSU5J',
          dxOwner: 'dummy_dxOwner',
          dxAccess: 'dummy_dxAccess',
          name: 'EricssonSCEFINI1',
          contact: {
            id: '',
            name: '',
          },
          insideNat: false,
          liInterface: 'INI1',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          timeZone: 'America/Tijuana',
          type: 'ERK_SCEF',
          city: '',
          stateName: '',
          country: '',
          tag: 'ERK_SCEF',
          version: '',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        },
      ],
    },
    services: [
      'LTE-IOT',
    ],
    types: {
      MDN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'MXQgLSBJb1Q',
              name: '1t - IoT',
            },
            options: [],
            primaryType: 'MDN',
            primaryValue: '8111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      MSISDN: [
        {
          target: {
            id: 'MXQgLSBJb1QjMQ',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ERK_SCEF',
                  name: 'ERK_SCEF',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBJb1Q',
              name: '1t - IoT',
            },
            options: [],
            primaryType: 'MSISDN',
            primaryValue: '18111111111',
            secondaryType: '',
            secondaryValue: '',
            isRequired: true,
          },
          display: false,
          ids: [
            'MXQgLSBJb1QjMQ',
          ],
        },
      ],
      EXTERNAL_ID: [
        {
          target: {
            id: 'MXQgLSBJb1QjMg',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
                {
                  id: 'ERK_SCEF',
                  name: 'ERK_SCEF',
                },
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSBJb1Q',
              name: '1t - IoT',
            },
            options: [],
            primaryType: 'EXTERNAL_ID',
            primaryValue: '82',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: true,
          ids: [
            'MXQgLSBJb1QjMg',
          ],
        },
      ],
    },
  },
};

export const CASE_DATA_IOT: CaseModel[] = [
  {
    id: 'MXQgLSBJb1Q',
    dxOwner: '',
    dxAccess: '',
    name: '1t - IoT',
    warrantId: {
      id: 'NDYwODc2ODEw',
      name: '1t - IoT',
    },
    liId: 'Case 1',
    coId: '1t - IoT',
    device: '1',
    services: [
      'LTE-IOT',
    ],
    cfId: {
      id: 'Q0EtRE9KQk5FLTMzMTA4',
      name: 'CA-DOJBNE-33108',
      cfId: 'CA-DOJBNE-33108',
      tag: 'CF-3GPP-33108',
    },
    entryDateTime: '2021-01-19T21:18:00Z',
    warrantStartDateTime: '2021-01-24T08:00:00Z',
    warrantStopDateTime: '2021-01-30T08:00:00Z',
    hi2Interfaces: [
      {
        id: 'Q0EtRE9KQk5FLTMzMTA4IzE',
        name: 'Q0EtRE9KQk5FLTMzMTA4IzE',
      },
    ],
    hi3Interfaces: [],
    genccc: {
      leaphone1: '',
      leaphone2: '',
    },
    type: 'CD',
    startDateTime: '2021-01-24T08:00:00Z',
    stopDateTime: '2021-01-30T08:00:00Z',
    status: 'INACTIVE',
    subStatus: 'PENDING',
    timeZone: 'America/Los_Angeles',
    templateUsed: '11',
    options: {
      kpi: true,
      traceLevel: 0,
      configureOptions: true,
      packetEnvelopeMessage: true,
      packetEnvelopeContent: false,
      realTimeText: true,
      callPartyNumberDisplay: true,
      directDigitExtractionData: true,
      monitoringReplacementPartiesAllowed: true,
      circuitSwitchedVoiceCombined: true,
      includeInBandOutBoundSignalling: true,
      encryptionType: 'NONE',
      encryptionKey: '',
      provideTargetIdentityInCcChannel: 'EMPTY',
      cccBillingType: 'FLATRATE',
      cccBillingNumber: 'CCC Billing',
      deactivationMode: 'NORMAL',
      location: true,
      target: true,
      sms: true,
      callIndependentSupplementaryServicesContent: true,
      packetSummaryCount: 0,
      packetSummaryTimer: 0,
      generatePacketDataHeaderReport: false,
      packetHeaderInformationReportFilter: 'ALL',
      callTraceLogging: 0,
      featureStatusInterval: 0,
      surveillanceStatusInterval: 0,
    },
    targets: [
      {
        id: 'MXQgLSBJb1QjMQ',
        name: 'MXQgLSBJb1QjMQ',
      },
      {
        id: 'MXQgLSBJb1QjMg',
        name: 'MXQgLSBJb1QjMg',
      },
    ],
    reporting: {},
  },
];

export const TARGET_DATA_IOT: TargetModel[] = [
  {
    id: 'MXQgLSBJb1QjMQ',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'ERK_SCEF',
          name: 'ERK_SCEF',
          tag: 'ERK_SCEF',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBJb1Q',
      name: '1t - IoT',
    },
    options: [],
    primaryType: 'MSISDN',
    primaryValue: '18111111111',
    secondaryType: '',
    secondaryValue: '',
  },
  {
    id: 'MXQgLSBJb1QjMg',
    dxOwner: '',
    dxAccess: '',
    targetInfoHash: [],
    afs: {
      afIds: [],
      afccc: [],
      afGroups: [],
      afAutowireTypes: [
        {
          id: 'ERK_SCEF',
          name: 'ERK_SCEF',
          tag: 'ERK_SCEF',
        },
      ],
      afAutowireGroups: [],
    },
    caseId: {
      id: 'MXQgLSBJb1Q',
      name: '1t - IoT',
    },
    options: [],
    primaryType: 'EXTERNAL_ID',
    primaryValue: '82',
    secondaryType: '',
    secondaryValue: '',
  },
];

export const CASE_DATA_RESULT_IOT = {
  [CASE_DATA_IOT[0].id]: {
    ...CASE_DATA_IOT[0],
    afMapping: DEVICE_DATA_IOT[1].afMapping,
    allTargetAfs: DEVICE_DATA_IOT[1].allTargetAfs,
    cfs: expect.any(Array),
    cfOptions: expect.any(Array),
    validAfs: new Set(Object.keys(DEVICE_DATA_IOT[1].afMapping ?? {})),
  },
};
