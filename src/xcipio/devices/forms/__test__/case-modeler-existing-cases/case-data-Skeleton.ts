import { DeviceModelMap } from 'xcipio/devices/device.types';

// eslint-disable-next-line import/prefer-default-export
export const DEVICE_DATA_SKELETON: DeviceModelMap = {
  1: {
    device: '1',
    caseIds: [
      'MXQgLSA1Rw',
    ],
    allTargetAfs: [
      {
        id: 'DN_1611073044331',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'RXJpY3Nzb24gU01GIDE',
              name: 'Ericsson SMF 1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'ZXJrc21mX2FmMQ',
              name: 'erksmf_af1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'ZXJrc21mX3gyTGlzbnIx',
              name: 'erksmf_x2Lisnr1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'DN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
      {
        id: 'MSISDN_1611073044331',
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [
            {
              id: 'RXJpY3Nzb24gU01GIDE',
              name: 'Ericsson SMF 1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'ZXJrc21mX2FmMQ',
              name: 'erksmf_af1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
            {
              id: 'ZXJrc21mX3gyTGlzbnIx',
              name: 'erksmf_x2Lisnr1',
              type: 'ERK_SMF',
              tag: 'ERK_SMF',
              model: '',
              x3TargetAssociations: null,
              bAFAssociation: true,
              contentInterfaces: [],
            },
          ],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        isRequired: false,
      },
    ],
    services: [
      'MOBILE',
      'VOICEMAIL',
    ],
    types: {
      DN: [
        {
          target: {
            id: null,
            caseId: {
              id: 'MXQgLSA1Rw',
              name: 'case1',
            },
            options: [],
            primaryType: 'DN',
            primaryValue: '44114411144',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
            synthesized: true,
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [],
              afAutowireGroups: [],
              afccc: [],
            },
          },
          display: true,
          synthesized: true,
          ids: [],
        },
      ],
      MSISDN: [
        {
          target: {
            id: 'MXQgLSA1RyMx',
            dxOwner: '',
            dxAccess: '',
            targetInfoHash: [],
            afs: {
              afIds: [],
              afGroups: [],
              afAutowireTypes: [
              ],
              afAutowireGroups: [],
              afccc: [],
            },
            caseId: {
              id: 'MXQgLSA1Rw',
              name: 'case1',
            },
            options: [],
            primaryType: 'MSISDN',
            primaryValue: '441234467844',
            secondaryType: '',
            secondaryValue: '',
            isRequired: false,
          },
          display: false,
          ids: [
            'MXQgLSA1RyMx',
          ],
        },
      ],
    },
  },
};
