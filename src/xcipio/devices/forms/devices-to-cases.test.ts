import { getDefaultHI3Interfaces } from 'xcipio/devices/forms/devices-to-cases';
import { HI2Interface, HI3Interface, Option } from 'data/collectionFunction/collectionFunction.types';

describe('getDefaultHI3Interfaces', () => {
  const hi2Data: HI2Interface[] = [
    {
      id: 'Q0YtTkFSREktMiMx',
      name: 'IP:127.0.0.1 PORT:1234',
      transportType: 'TCP',
      options: [],
    },
    {
      id: 'Q0YtTkFSREktMiMy',
      name: 'IP:127.0.0.1 PORT:4567',
      transportType: 'TCP',
      options: [],
    },
  ];

  const hi3Data: HI3Interface[] = [
    {
      id: 'Q29va0NvdW50eS1BVElTIzE',
      name: 'IP:56.56.56.56 PORT:6765',
      transportType: 'TCP',
      options: [
        {
          key: 'filter',
          value: 'VO',
        },
        {
          key: 'transport',
          value: 'TCP',
        },
        {
          key: 'destPort',
          value: '6765',
        },
        {
          key: 'destIPAddr',
          value: '56.56.56.56',
        },
      ],
    },
    {
      id: 'Q29va0NvdW50eS1BVElTIzI',
      name: 'IP:78.78.78.78 PORT:6755',
      transportType: 'TCP',
      options: [
        {
          key: 'filter',
          value: 'ALL',
        },
        {
          key: 'transport',
          value: 'TCP',
        },
        {
          key: 'destPort',
          value: '6755',
        },
        {
          key: 'destIPAddr',
          value: '78.78.78.78',
        },
      ],
    },
  ];
  it('if caseType has Call Content or Data & Content, return the HI3 template that has filter=ALL', () => {
    const hi3Interfaces = getDefaultHI3Interfaces('CC', hi2Data, hi3Data, false, false);
    expect(hi3Interfaces.length).toEqual(1);
    expect(hi3Interfaces[0]).toEqual(
      expect.objectContaining(
        {
          id: hi3Data[1].id,
          name: hi3Data[1].name,
          operation: 'CREATE',
        },
      ),
    );
  });

  it('if caseType has Call Content or Data & Content and HI3 template does not have filter attribute, return first template', () => {
    const hi3DataNoFilter = hi3Data.map((item) => ({
      ...item,
      options: item.options?.filter((option: Option) => (option.key !== 'filter')),
    }));
    const hi3Interfaces = getDefaultHI3Interfaces('CC', hi2Data, hi3DataNoFilter, false, false);

    expect(hi3Interfaces.length).toEqual(1);

    expect(hi3Interfaces[0]).toEqual(
      expect.objectContaining({
        id: hi3DataNoFilter[0].id,
        name: hi3DataNoFilter[0].name,
        operation: 'CREATE',
      }),
    );
  });

  it('if caseType has Call Data and case option has packet-data-header-report selected return first HI2', () => {
    const hi3Interfaces = getDefaultHI3Interfaces('CD', hi2Data, hi3Data, true, false);
    expect(hi3Interfaces).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: hi2Data[0].id,
          name: hi2Data[0].name,
          operation: 'CREATE',
        }),
      ]),
    );
  });

  it('if caseType has no value, return empty array', () => {
    const hi3Interfaces = getDefaultHI3Interfaces(undefined, hi2Data, hi3Data, true, false);
    expect(hi3Interfaces).toEqual([]);
  });
});
