import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import setAllTemplateFieldsAsReadOnly from 'utils/formTemplate/setAllTemplateFieldsAsReadOnly';
import { FormTemplateFieldMap } from 'data/template/template.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import DynamicForm, { ChangedField } from '../../../shared/form';

export default class TargetDetail extends React.Component<any, any> {
  formRef : any;

  constructor(props:any) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(props: any, state: any) {
    if (!props.fields) {
      return state;
    }
    // cloneDeep the fields into state only when 'required' atrribute changed.
    if (props.fields && state.fields) {
      const propsFieldRequired = props.fields.primaryValue.validation.required;
      const stateFieldRequired = state.fields.primaryValue.validation.required;
      if (propsFieldRequired === stateFieldRequired) {
        return state;
      }
    }
    const fields = cloneDeep(props.fields);
    const keys = Object.keys(fields);
    if (keys.length === 1) {
      fields[keys[0]].label = undefined;
    }
    state = {
      fields,
      target: props.target,
    };
    return state;
  }

  isValid = () => {
    return this.formRef.isValid(true, true);
  }

  customModifyTargetInputByType = (trgt: any) => {
    const target = cloneDeep(trgt);
    switch (target.primaryType) {
      case 'MDNwService':
      case 'MDN': {
        const { options } = target;
        if (options.length > 0) {
          target.options = options[0].value;
        } else {
          target.options = '';
        }
        break;
      }
      default: break;
    }
    return target;
  }

  fieldUpdateHandler = (changedField: ChangedField) => {
    const {
      targetType,
      targetIndex,
      onTargetUpdate,
    } = this.props;

    const {
      name, value,
    } = changedField;
    const { target } = this.state;
    target[name] = value;
    switch (target.primaryType) {
      case 'MDN': {
        if (name === 'options') {
          target[name] = [{
            key: 'InterceptType',
            value,
          }];
        }
        break;
      }

      case 'MDNwService':
        if (name === 'options') {
          target[name] = [{
            key: 'SERVICETYPE',
            value,
          }];
        }
        break;

      default: break;
    }
    this.setState({ target });

    if (onTargetUpdate != null) {
      onTargetUpdate(target, targetType, targetIndex);
    }
  }

  static getFormTemplateFields = (
    formTemplateFields: FormTemplateFieldMap,
    mode:string,
  ): FormTemplateFieldMap => {

    if (mode === CONFIG_FORM_MODE.View) {
      setAllTemplateFieldsAsReadOnly(formTemplateFields);
    }

    return formTemplateFields;
  }

  render(): JSX.Element {
    const { fields } = this.state;

    const { 'data-testid': dataTestId, mode } = this.props;

    return (
      <td data-testid={dataTestId} className="devices-form-list-body-row-data">
        <DynamicForm
          ref={(ref:any) => { this.formRef = ref; }}
          name={`target_${new Date().getTime()}`}
          fields={TargetDetail.getFormTemplateFields(fields, mode)}
          layout={[Object.keys(fields)]}
          defaults={this.customModifyTargetInputByType(this.state.target)}
          fieldUpdateHandler={this.fieldUpdateHandler}
        />
      </td>
    );
  }
}
