/* eslint-disable no-continue */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-prototype-builtins */
import { CollectionFunction, CollectionFunctionMap, CFTag } from 'data/collectionFunction/collectionFunction.types';
import { CustomizationDerivedTargetMap, TargetDefaultAFSelection } from 'data/customization/customization.types';

import cloneDeep from 'lodash.clonedeep';
import { Warrant } from 'data/warrant/warrant.types';
import { Ontology } from 'data/supportedFeatures/supportedFeatures.types';
import { Identity } from 'data/types';
import { CaseModel, CaseModelMap } from 'data/case/case.types';
import { TargetModel, TargetModelMap, TargetModelWrapper } from 'data/target/target.types';
import { AccessFunctionMap } from 'data/accessFunction/accessFunction.types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { filterSelectedAfBasedOnSelectionMethod } from './filterSelectedAfBasedOnSelectionMethod';
import {
  DeviceModel,
  DeviceModelMap,
  AccessFunctionsByCollectionFunctionTypeMap,
  ServicesByCollectionFunctionTypeMap,
  WarrantCaseModel,
  AccessFunctionsAndCollectionFunctionsByServiceMap,
} from '../device.types';

type CaseModelerProps = {
  warrant: Warrant
  serviceMap: AccessFunctionsAndCollectionFunctionsByServiceMap
  customerTargets: CustomizationDerivedTargetMap
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  cfPool: CollectionFunction[]
  ontology: Ontology
  shouldAutomaticallyGenerateCaseName: boolean
}

export const TEMPORARY_CASE_ID_PREFIX = 'DX-CASE-';
export const TEMPORARY_TARGET_ID_PREFIX = 'DX-TARGET-';

export default class CaseModeler {
  /*
  * Warrant is the warrant object
  * serviceMap is the (configuration) mapping from services to supported CFs and AFs
  * customerTargets defines the patterns used to derive target values from other targets
  */

  warrant: Warrant;
  serviceMap: AccessFunctionsAndCollectionFunctionsByServiceMap;
  customerTargets: CustomizationDerivedTargetMap;
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined;
  CFPool: CollectionFunctionMap | null = null;
  cfPool: CollectionFunction[];
  cases: CaseModelMap | null = null;
  targets: TargetModelMap = {};
  nextCaseId = 0;
  nextTargetId = 0;
  caseNamePrefix: string;
  ontology: Ontology;
  shouldAutomaticallyGenerateCaseName = false;

  constructor(props: CaseModelerProps) {
    this.warrant = props.warrant;
    this.serviceMap = props.serviceMap;
    this.customerTargets = props.customerTargets;
    this.targetDefaultAFSelection = props.targetDefaultAFSelection;
    this.cfPool = props.cfPool;
    this.CFPool = null;
    this.cases = null;
    this.targets = { };
    this.nextCaseId = 0;
    this.nextTargetId = 0;
    this.caseNamePrefix = 'Case';
    this.ontology = props.ontology;
    this.shouldAutomaticallyGenerateCaseName = props.shouldAutomaticallyGenerateCaseName ?? false;
  }

  static copyTarget(target: TargetModel, newId: string): TargetModel {
    // This copy isn't deep enough
    const copy = cloneDeep<TargetModel>(target);
    copy.id = newId;
    return (copy);
  }

  static isFakeTarget(target: TargetModelWrapper): boolean {
    let isFake = false;
    if (target.hasOwnProperty('synthesized')) {
      isFake = target.synthesized === true;
    }
    return (isFake);
  }

  static getIDList(json: CaseModel | null, field: 'targets'): string[] | null {
    let ids: string[] | null = null;
    let idList = null;

    if (json?.hasOwnProperty(field)) {
      // Get the ID field from identity object and return as a list
      ids = [];
      idList = json[field];
      for (const identity of idList) {
        if (identity.id != null) {
          ids.push(identity.id);
        }
      }
    }

    return (ids);
  }

  static buildIdentity(entity: Identity): Identity {
    const identity: Identity = {};

    if (entity.hasOwnProperty('id')) identity.id = entity.id;
    if (entity.hasOwnProperty('name')) identity.name = entity.name;

    return (identity);
  }

  static findTarget(caseDef: CaseModel | null, target: TargetModelWrapper): string | null {
    // Look through the list of target IDs for this target to see if it is in the case
    let targetID: string | null = null;
    const caseTargets = CaseModeler.getIDList(caseDef, 'targets');

    if ((caseTargets != null) && (caseTargets.length > 0)) {
      for (const id of target.ids) {
        if (caseTargets.indexOf(id) >= 0) {
          targetID = id;
          break;
        }
      }
    }

    return (targetID);
  }

  getCFServices(cfTags: string[]): string[] {
    const cfSvcs:Set<string> = new Set();
    cfTags.forEach((cfTag) => {
      for (const service in this.serviceMap) {
        if (this.serviceMap[service].collectionFunctions.hasOwnProperty(cfTag)) {
          cfSvcs.add(service);
        }
      }
    });

    return Array.from(cfSvcs);
  }

  getCFAccessFns(cfTag: string): string[] {
    const cfAfs: string[] = [];
    for (const service in this.serviceMap) {
      if (this.serviceMap[service].collectionFunctions.hasOwnProperty(cfTag)) {
        // Record all of the acces functions for this service's matching CF
        for (const af of this.serviceMap[service].collectionFunctions[cfTag].accessFunctions) cfAfs.push(af);
      }
    }
    return (cfAfs);
  }

  /*
  * CFPool is the (array of) CF objects returned by CREST for the CFs selected by the LEA/CF-group
  * or chosen by the user cases is the existing list of cases for the warrant (should be empty for new warrants)
  * caseNamePrefix is the (optional) prefix for case name (and LIID)
  */
  setConfiguration(
    CFPool: CollectionFunction[],
    cases: CaseModel[],
    targets: TargetModel[],
    caseNamePrefix: string | null = null,
  ): void {

    // Record the CF pool
    this.CFPool = { };

    if (typeof (CFPool) === 'string') CFPool = JSON.parse(CFPool);
    for (const cf of CFPool) {
      const cfID = cf.id;
      if (!cfID) continue;
      cf.services = this.getCFServices([cf.tag]);
      cf.afs = this.getCFAccessFns(cf.tag);
      this.CFPool[cfID] = cf;
    }
    // Record the cases (if any)
    this.cases = { };
    if (cases != null) {
      for (const item of cases) {
        const caseID = item.id;
        if (!caseID) continue;
        this.cases[caseID] = item;
        // Keep track of the highest case name ending number found so far
        const found = this.extractEndingNumber(item);
        if (found) {
          const id = +found[0];
          if (id > this.nextCaseId) this.nextCaseId = id;
        }
      }
    }
    if (targets != null) {
      for (const item of targets) {
        const targetID = item.id;
        if (!targetID) continue;
        this.targets[targetID] = item;
        // Keep track of the highest target name ending number found so far
        const found = item.id?.match(/\d+$/); // extract the ending number
        if (found) {
          const id = +found[0];
          if (id > this.nextTargetId) this.nextTargetId = id;
        }
      }
    }
    if (caseNamePrefix) this.caseNamePrefix = caseNamePrefix;
  }

  extractEndingNumber(caseEntry: CaseModel): RegExpMatchArray | null {
    if (this.shouldAutomaticallyGenerateCaseName) {
      return caseEntry.liId.match(/\d+$/);
    }

    return caseEntry.id.match(/\d+$/);
  }

  newTargetId(): string {
    // Return a useful case ID to distinguish this case from others
    this.nextTargetId += 1;
    return (`${TEMPORARY_TARGET_ID_PREFIX}${this.nextTargetId}`);
  }

  newCaseId(): string {
    // Return a useful case ID to distinguish this case from others
    this.nextCaseId += 1;
    return (`${TEMPORARY_CASE_ID_PREFIX}${this.nextCaseId}`);
  }

  buildCaseName(): string {
    // Construct a (unique) case name from the warrant's name and the current case ID
    return `${this.caseNamePrefix} ${this.nextCaseId}`;
  }

  buildCaseLiId(caseName: string): string {
    if (this.shouldAutomaticallyGenerateCaseName) {
      return caseName;
    }

    return '';
  }

  getTargetCustomization(services: string[], ttype: string): {
    pattern:string | null,
    parent: string | null,
  } {
    // Look for a matching (map-ified) target type + service
    let pattern = null;
    let parent = null;
    const ALL = '*';

    // Return the pattern matching the first service from the list
    if (ttype) {
      if (this.customerTargets.hasOwnProperty(ttype)) {
        // Found a target-type in the pattern list
        for (const patternSvc in this.customerTargets[ttype]) {
          if (services.indexOf(patternSvc) >= 0) {
            // Found a service-specific pattern
            pattern = this.customerTargets[ttype][patternSvc].pattern;
            parent = this.customerTargets[ttype][patternSvc].parent;
            break;
          }
        }
        if ((!pattern) && this.customerTargets[ttype].hasOwnProperty(ALL)) {
          // Use the default pattern for this target-type
          pattern = this.customerTargets[ttype][ALL].pattern;
          parent = this.customerTargets[ttype][ALL].parent;
        }
      }
    }

    return { pattern, parent };
  }

  static calculateWarrantCase(
    cfTags: string[],
    availableAFsByCFMap: AccessFunctionsByCollectionFunctionTypeMap,
    servicesByCFMap: ServicesByCollectionFunctionTypeMap,
  ): WarrantCaseModel {
    // Assumption: compatible CFs will have the same set of AF.  This is true for the current
    // usage scenario.  Volte 4G that has 1066 and ATIS.  The Volte 4G AFs are the same for 1066 and ATIS.
    // 5G Data AF is the same 33108 and ETIS.
    // Group CFs by compatibility
    const afTypes:string[] = Array.from(availableAFsByCFMap[cfTags[0]]);

    const serviceSet: Set<string> = new Set();
    cfTags.forEach((cfTag) => {
      Array.from(servicesByCFMap[cfTag]).forEach((service) => serviceSet.add(service));
    });

    const warrantCase: WarrantCaseModel = {
      cfs: [...cfTags],
      afs: [...afTypes],
      targets: [],
      services: Array.from(serviceSet),
    };

    return warrantCase;
  }

  /**
   * @returns the CF tag that has all AF type in the case.
   */
  static getIncludedCollectionFunction(
    warrantCase: WarrantCaseModel,
    cfAfMap: AccessFunctionsByCollectionFunctionTypeMap,
  ): string[] {
    const includedAccessFunctions = warrantCase.afs;

    // get all CFs that have the AF in the case
    const includedCollectionFunctions = Object.entries(cfAfMap).filter(([_, accessFunctions]) => {
      return Array.from(accessFunctions).every((accessFunction) => includedAccessFunctions.includes(accessFunction));
    });

    return includedCollectionFunctions.map(([collectionFunction]) => collectionFunction);
  }

  /**
   * This returns groups of compatible CFs based on the CF supersedes list in the ontology file.
   * In the ontology file, supersedes list in a CF tag section indicates the list of CF tags that
   * are compatible to the CF tag.  When automatically generating a case, compatible CF tags should have
   * 1 associated case.  In the case, users can select CFs of the compatible CF tags.
   * @param availableAFsByCFMap is CF tags maps to a set of AF tags.
   * @param ontology is the Ontology file
   * @returns groups of compatible CF tags.
   */
  static getGroupsOfCompatibleCFs(
    availableAFsByCFMap:AccessFunctionsByCollectionFunctionTypeMap,
    ontology:Ontology,
  ): CFTag[][] {
    const orderedGroupsOfCompatibleCFTags:CFTag[][] = [];
    const includedCFTags:CFTag[] = [];

    Object.keys(availableAFsByCFMap).forEach((cfTag:CFTag) => {
      const { supersedes } = ontology.collectionFunctions[cfTag];
      if (supersedes && supersedes.length > 0) {
        // filter out CF that are not belonging to selected services
        const compatibleCFs = supersedes.filter(
          (compatibleCFTag) => (Object.keys(availableAFsByCFMap).includes(compatibleCFTag)),
        );

        // put compatible CFs in one group
        orderedGroupsOfCompatibleCFTags.push([
          cfTag,
          ...compatibleCFs,
        ]);

        // mark the compatible CF as included
        includedCFTags.push(cfTag);
        compatibleCFs.forEach((compatibleCFTag) => { includedCFTags.push(compatibleCFTag); });
      }
    });

    Object.keys(availableAFsByCFMap).forEach((cfTag:string) => {
      if (includedCFTags.includes(cfTag) === false) {
        orderedGroupsOfCompatibleCFTags.push([cfTag]);
        includedCFTags.push(cfTag);
      }
    });

    return orderedGroupsOfCompatibleCFTags;
  }

  /**
   * Get the available AF types and CF types based on the services selected in device
   * Also get services supported by each CF
   */
  static getAvailableAFsByCFMapAndServicesByCFMap(
    services: string[],
    serviceMap: AccessFunctionsAndCollectionFunctionsByServiceMap,
    availableCFsByTagMap: { [collectionFunctionTag: string]: boolean },
  ): [AccessFunctionsByCollectionFunctionTypeMap, ServicesByCollectionFunctionTypeMap] {
    const availableAFsByCFMap: AccessFunctionsByCollectionFunctionTypeMap = {};
    const servicesByCFMap: ServicesByCollectionFunctionTypeMap = {};

    if (services === null) {
      return [availableAFsByCFMap, servicesByCFMap];
    }

    for (const service of services) {
      // Look up this service in the serviceMap to determine which CF-tags support it
      if (!serviceMap.hasOwnProperty(service)) continue;

      const enabledCollectionFunctions = serviceMap[service].collectionFunctions;

      Object.keys(enabledCollectionFunctions).forEach((enabledCFType: string) => {
        if (!availableCFsByTagMap[enabledCFType]) {
          // There isn't an available CF with the CF type, so it should not
          // be considered when creating the cases.
          return;
        }

        if (!availableAFsByCFMap[enabledCFType]) {
          availableAFsByCFMap[enabledCFType] = new Set<string>();
        }

        const { accessFunctions } = enabledCollectionFunctions[enabledCFType];
        accessFunctions.forEach((accessFunctionType:string) => {
          availableAFsByCFMap[enabledCFType].add(accessFunctionType);
        });

        if (accessFunctions.length > 0) {
          if (!servicesByCFMap[enabledCFType]) {
            servicesByCFMap[enabledCFType] = new Set<string>();
          }
          servicesByCFMap[enabledCFType].add(service);
        }
      });
    }

    return [availableAFsByCFMap, servicesByCFMap];
  }

  getAvailableCFsByTagMap(): { [collectionFunctionTag: string]: boolean } {
    if (this.CFPool == null) {
      return {};
    }

    const collectionFunctionMap = this.CFPool;

    return Object.keys(collectionFunctionMap).reduce<{[key: string]: boolean}>((map, collectionFunctionId) => ({
      ...map,
      [collectionFunctionMap[collectionFunctionId].tag]: true,
    }), {});
  }

  static getCasesModelForDevice(
    services: string[],
    orderedGroupsOfCompatibleCFTags: CFTag[][],
    availableAFsByCFMap: AccessFunctionsByCollectionFunctionTypeMap,
    servicesByCFMap: ServicesByCollectionFunctionTypeMap,
  ): WarrantCaseModel[] {
    if (services == null) {
      return [];
    }

    const cases: WarrantCaseModel[] = [];
    // Create a case for each group of compatible CF Types.  The compatible CF types are in order of
    // preference.  The most preferred CF types are at the beginning of the array.  Thus the Case Panel will
    // show the CFs with the preferred type on top.
    orderedGroupsOfCompatibleCFTags.forEach((cfTags:CFTag[]) => {
      const warrantCase = CaseModeler.calculateWarrantCase(
        cfTags,
        availableAFsByCFMap,
        servicesByCFMap,
      );
      cases.push(warrantCase);
    });

    return cases;
  }

  buildNewCase(
    services: string[],
    cfs: CollectionFunction[],
    device: DeviceModel,
    signature: string,
  ): CaseModel {
    // Create a new case from warrant data, selected CF, and device data
    const cfOptions = cfs.map((cf) => ({ label: cf.name, value: cf.id }));
    const cfIdentity = { name: cfOptions[0].label, id: cfOptions[0].value };

    const warrantIdentity = CaseModeler.buildIdentity(this.warrant);
    const now = (new Date()).toISOString();
    const caseID = this.newCaseId();
    const caseName = this.buildCaseName();
    const liId = this.buildCaseLiId(caseName);

    const caseDef: CaseModel = {
      id: caseID,
      dxOwner: this.warrant.dxOwner,
      dxAccess: null,
      name: caseName,
      warrantId: warrantIdentity,
      liId,
      device: device.device,
      services,
      cfId: cfIdentity,
      entryDateTime: now,
      type: this.warrant.caseType,
      startDateTime: this.warrant.startDateTime,
      stopDateTime: this.warrant.stopDateTime,
      timeZone: this.warrant.timeZone,
      targets: [],
    };

    // These fields will be added by the UI code after receiving the data model:
    //    options, genccc, hi2Interfaces, hi3Interfaces, templateUsed
    caseDef.cfOptions = cfOptions;
    caseDef.cfs = cfs;
    caseDef.templateUsed = null;
    caseDef.genccc = null;
    caseDef.hi2Interfaces = null;
    caseDef.hi3Interfaces = null;
    caseDef.signature = signature;

    return caseDef;
  }

  setTargetValue(target: TargetModel, device: DeviceModel, ttype: string): void {
    // Set this target's value based on another target's value and its pattern
    const { pattern, parent } = this.getTargetCustomization(device.services, ttype);
    if (pattern) {
      const iLeftB = pattern.indexOf('{');
      const iRightB = pattern.indexOf('}');
      // If there is a target pattern defined for this target, return the dependent target type
      if ((iLeftB >= 0) && (iLeftB < iRightB)) {
        const depend = pattern.substring((iLeftB + 1), iRightB);
        if (device.types[depend]) {
          // Take the first value to substitute into the pattern
          const value = device.types[depend][0].target.primaryValue;
          if (value && value.trim().length > 0) {
            const prefix = pattern.substring(0, iLeftB);
            const suffix = pattern.substring(iRightB + 1);
            target.primaryValue = (prefix + value + suffix);
          } else {
            target.primaryValue = '';
          }
        } // else leave it blank (dependency not found)
      } else if (pattern.startsWith(':s') && parent) {
        const parts = pattern.split('/');
        if (device.types[parent]) {
          const value = device.types[parent][0].target.primaryValue;
          if (value && value.trim().length > 0) {
            target.primaryValue = value.replace(new RegExp(parts[1]), parts[2]);
          } else {
            target.primaryValue = '';
          }
        } // else leave it blank (parent not found)
      } else {
        target.primaryValue = pattern; // set a constant string
      }
    }
  }

  static filterAfsFromTarget(target: TargetModel, validAfs = new Set<string>()): boolean {
    if (target.afs == null) {
      return false;
    }

    const afIds = target.afs.afIds.filter((obj) => {
      return validAfs.has(obj.tag ?? '');
    });

    const afAutowireTypes = target.afs.afAutowireTypes.filter((obj) => {
      return validAfs.has(obj.name ?? '');
    });

    target.afs.afIds = afIds;
    target.afs.afAutowireTypes = afAutowireTypes;

    if (afIds.length === 0 && afAutowireTypes.length === 0) {
      return false;
    }

    return true;
  }

  static cleanAfsBasedOnSelectedCF(
    warrantCase: CaseModel,
    allTargetAfs: TargetModel[],
    validAfs: Set<string>,
    afMapping: AccessFunctionMap,
  ): void {
    for (let j = allTargetAfs.length - 1; j >= 0; j -= 1) {
      const target = allTargetAfs[j];
      const validTarget = CaseModeler.filterAfsFromTarget(target, validAfs);
      if (!validTarget) {
        allTargetAfs.splice(j, 1);
      }
    }
    warrantCase.allTargetAfs = allTargetAfs;
    warrantCase.afMapping = afMapping;
    warrantCase.validAfs = validAfs;
  }

  getMatchingPrevCase = (signature:string): CaseModel | null => {
    if (this.cases == null) {
      return null;
    }

    let matchedCase = null;
    Object.keys(this.cases).forEach((id) => {
      if (this.cases?.[id].signature === signature) {
        matchedCase = cloneDeep(this.cases[id]);
      }
    });

    return matchedCase;
  }

  static serializeStringArray(arr: string[]): string {
    arr.sort();
    return arr.join();
  }

  /*
  * devices is the revised device modeler object after allowing the user to fiddle with the values
  * After calling this method, the following still needs to be done for each case:
  * - add case options
  * - add hi2interfaces
  * - add hi3interfaces
  * - add templateUsed
  * - add genccc data
  */
  generateCases(devices: DeviceModelMap): void {
    if ((this.CFPool == null) || (this.cases == null) || (devices == null)) {
      return; // needed data has not been set
    }

    // Determine which CFs are needed to satisfy the services for each device
    // Step through each device and add any missing cases, tracking all targets
    const newCases: CaseModelMap = {};
    const newTargets: TargetModelMap = {};

    for (const dev in devices) {
      const device = devices[dev];

      const [
        availableAFsByCFMap,
        servicesByCFMap,
      ] = CaseModeler.getAvailableAFsByCFMapAndServicesByCFMap(
        device.services,
        this.serviceMap,
        this.getAvailableCFsByTagMap(),
      );

      const orderedGroupsOfCompatibleCFTags:CFTag[][] =
        CaseModeler.getGroupsOfCompatibleCFs(availableAFsByCFMap, this.ontology);

      if (CaseModeler.isNewDevice(device)) {
        const cases = CaseModeler.getCasesModelForDevice(device.services,
          orderedGroupsOfCompatibleCFTags,
          availableAFsByCFMap,
          servicesByCFMap); // may include duplicates

        if (cases.length === 0) {
          continue; // couldn't create case
        }

        for (const warrantCase of cases) {
          const signature = `${CaseModeler.serializeStringArray(warrantCase.cfs)}:${CaseModeler.serializeStringArray(warrantCase.afs)}:${dev}`;
          // find an existing case that matches warrantCase.  The warrantCase was one of the cases
          // generated by a device.
          let caseDef = this.getMatchingPrevCase(signature);

          // if no existing case matching the warrantCase, create one.
          if (caseDef == null) {
            // get CFs for the warrantCase.  The CFs must be in the order which
            // the CF Tagas are listed in the warrantCase.
            let cfs: CollectionFunction[] = [];
            warrantCase.cfs.forEach((cfTag) => {
              cfs = cfs.concat(this.cfPool.filter((cf) => (cf.tag === cfTag)));
            });

            if (cfs.length === 0) {
              continue;
            }

            // the purpose of warrantCase.includedCfTypes is to get service names. warrantCase.includedCfs is a CF tag.
            caseDef = this.buildNewCase(warrantCase.services, cfs, device, signature);
            const allTargetAfs = cloneDeep(device.allTargetAfs);
            const afMapping = cloneDeep(device.afMapping ?? {});
            const validAfs = new Set<string>();

            Object.keys(afMapping).forEach((afType) => {
              if (warrantCase.afs.includes(afType)) {
                validAfs.add(afType);
              }
            });

            CaseModeler.cleanAfsBasedOnSelectedCF(caseDef, allTargetAfs, validAfs, afMapping);

          } else {
            // Replace the service as it could have change
            caseDef.services = warrantCase.services;
          }

          this.generateCaseTargets(device, caseDef, newTargets);

          newCases[caseDef.id as string] = caseDef;
        }

      } else {
        const cases = CaseModeler.getCasesForDevice(device, Object.values(this.cases));

        cases.forEach((caseDef) => {
          const allTargetAfs = cloneDeep(device.allTargetAfs);
          const afMapping = cloneDeep(device.afMapping ?? {});

          if (WarrantConfiguration.hasUnsupportedServices(device.services)) {
            caseDef.allTargetAfs = allTargetAfs;
            caseDef.afMapping = afMapping;

          } else {
            const validAfs = this.getValidAfsForExistentCase(device, caseDef);
            CaseModeler.cleanAfsBasedOnSelectedCF(caseDef, allTargetAfs, validAfs, afMapping);
          }

          this.generateCaseCfsOptions(caseDef, this.cfPool, orderedGroupsOfCompatibleCFTags);
          this.generateCaseTargets(device, caseDef, newTargets);

          newCases[caseDef.id as string] = caseDef;
        });
      }
    }

    this.cases = newCases;
    this.targets = newTargets;
  }

  /**
   * Returns an array of CF tags that is compatible to the selected service and CF tag
   * If no warrantCaseCFTag is provided, this returns a list of CF tag compatible to the selected
   * services.
   */
  static getCompatibleCFTagGroupBasedOnServicesAndCF(
    warrantCaseServices: string[],
    warrantCaseCFTag: string | undefined,
    orderedGroupsOfCompatibleCFTags: CFTag[][],
    serviceMap: AccessFunctionsAndCollectionFunctionsByServiceMap,
  ): CFTag[] {

    // Get CF tags that are applicable to the warrant case services
    let compatibleCFTagGroup:CFTag[];

    let applicableCFTags: CFTag[] = [];
    if (warrantCaseServices.length > 0) {
      warrantCaseServices.forEach((service) => {
        if (!serviceMap[service]) {
          console.error('Unknown service when looking up CF tag for services');
          return;
        }
        if (applicableCFTags.length === 0) {
          applicableCFTags.push(...Object.keys(serviceMap[service].collectionFunctions));
        } else {
          // only keep the CF Tags that are in both applicableCFTags and serviceMap[service].collectionFunctions
          applicableCFTags = applicableCFTags.filter((cfTag) =>
            Object.keys(serviceMap[service].collectionFunctions).includes(cfTag));
        }
      });
    }

    if (warrantCaseCFTag) {
      // find compatible CF Tag group that has the warrant case CF Tag
      // and is a subset of applicableCFTags
      const aCompatibleCFTagGroup = orderedGroupsOfCompatibleCFTags.find((cfTags:CFTag[]) => {
        if (cfTags.includes(warrantCaseCFTag)) {
          const matchingCFTags = cfTags.filter((cfTag) => applicableCFTags.includes(cfTag));
          if (matchingCFTags.length >= cfTags.length) {
            return true;
          }
        }
        return false;
      }) as CFTag[] | undefined;

      if (!aCompatibleCFTagGroup || aCompatibleCFTagGroup.length === 0) {
        compatibleCFTagGroup = [warrantCaseCFTag];
      } else {
        compatibleCFTagGroup = aCompatibleCFTagGroup;
      }
    } else {
      // If there is no service, the warrant was not created using Discovery.
      // We will just use display only CFs has the same CF tag as the warrant case CF.
      compatibleCFTagGroup = applicableCFTags;
    }

    return compatibleCFTagGroup;
  }

  generateCaseTargets(device: DeviceModel, caseDef: CaseModel, newTargets: TargetModelMap): void {
    // Look for added targets
    for (const ttype in device.types) {
      const ttypeList = device.types[ttype];

      for (const target of ttypeList) {
        // Keep track of this target in our repository
        if (CaseModeler.isFakeTarget(target)) {
          continue;
        }

        // Set the target's value based on other target values (using the device's target-type)
        if (!target.hasOwnProperty('ids')) {
          target.ids = [];
        }

        if (!target.display) {
          this.setTargetValue(target.target, device, ttype);
        }

        // Add any new targets to the case
        const targetId = CaseModeler.findTarget(caseDef, target);
        if (!targetId) { // This is a new target
          const newId = this.newTargetId();
          const newTarget: TargetModel = CaseModeler.copyTarget(target.target, newId);
          const validTarget = CaseModeler.filterAfsFromTarget(newTarget, caseDef.validAfs);

          if (validTarget) {
            filterSelectedAfBasedOnSelectionMethod(newTarget, caseDef, this.targetDefaultAFSelection);
            // Add the target to the case's targets
            const identity = { id: newId, name: newId };
            caseDef?.targets.push(identity);
            target.ids.push(newId);
            newTarget.caseId = CaseModeler.buildIdentity(caseDef);
            newTargets[newId] = newTarget;
          }

        } else if (this.targets.hasOwnProperty(targetId)) {
          newTargets[targetId] = cloneDeep(this.targets[targetId]);
          newTargets[targetId].primaryValue = target.target.primaryValue;

          if ((target.target.primaryType === 'CASEIDMIN') ||
              (target.target.primaryType === 'CASEIDMDN') ||
              (target.target.primaryType === 'MDNwService')) {
            newTargets[targetId].primaryType = target.target.primaryType;
          }

          if (target.target.hasOwnProperty('secondaryType')) {
            newTargets[targetId].secondaryType = target.target.secondaryType;
            newTargets[targetId].secondaryValue = target.target.secondaryValue;
          }

          if (target.target.options != null) {
            newTargets[targetId].options = target.target.options;
          }

        } else {
          // Record a copy of this (existing) target in our list
          newTargets[targetId] = CaseModeler.copyTarget(target.target, targetId);
        }
      }
    }
  }

  getValidAfsForExistentCase(device: DeviceModel, caseDef: CaseModel): Set<string> {
    const [availableAFsByCFMap] = CaseModeler.getAvailableAFsByCFMapAndServicesByCFMap(
      device.services,
      this.serviceMap,
      this.getAvailableCFsByTagMap(),
    );

    const validAfs = new Set<string>();

    const cfId = caseDef.cfId.id;
    const warrantCaseCf = this.cfPool.find((cf) => cf.id === cfId);
    if (!warrantCaseCf) {
      return validAfs;
    }

    const afs = availableAFsByCFMap[warrantCaseCf.tag];
    const accessFunctionMap = device.afMapping ?? {};
    Object.keys(accessFunctionMap).forEach((afType) => {
      if (afs?.has(afType)) {
        validAfs.add(afType);
      }
    });

    return validAfs;
  }

  generateCaseCfsOptions(
    warrantCase: CaseModel,
    cfPool: CollectionFunction[],
    orderedGroupsOfCompatibleCFTags:CFTag[][],
  ): void {
    if ((warrantCase == null) || cfPool == null) {
      return; // needed data has not been set
    }

    const cfId = warrantCase.cfId.id;
    const warrantCaseCf = this.cfPool.find((cf) => cf.id === cfId);

    // Get CF tags that are applicable to the warrant case services
    const compatibleCFTagGroup:CFTag[] = CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
      warrantCase.services,
      warrantCaseCf?.tag,
      orderedGroupsOfCompatibleCFTags,
      this.serviceMap,
    );

    // get CFs that match the CF Tag in the compatibleCFTagGroup
    const cfs = cfPool.filter((cf) => compatibleCFTagGroup?.includes(cf.tag));
    const cfOptions = cfs.map((cf) => ({ label: cf.name, value: cf.id }));
    warrantCase.cfs = cfs;
    warrantCase.cfOptions = cfOptions;
  }

  static getCasesForDevice(device: DeviceModel, cases: CaseModel[] | null): CaseModel[] {
    if (cases == null) {
      return [];
    }

    const caseIds = device.caseIds ?? [];

    const deviceCases = cases.filter(({ id }) => caseIds.includes(id));

    return cloneDeep(deviceCases);
  }

  static isNewDevice(device: DeviceModel): boolean {
    if (!device.caseIds?.length) {
      return true;
    }

    return device.caseIds.every((caseId) => caseId.startsWith(TEMPORARY_CASE_ID_PREFIX));
  }

  static isNewCase(caseEntry: CaseModel): boolean {
    return caseEntry.id.startsWith(TEMPORARY_CASE_ID_PREFIX);
  }

  static sortCases(cases: CaseModel[]): CaseModel[] {
    const sortedCases = cases.sort(CaseModeler.sortByExistingCases);
    sortedCases.forEach(CaseModeler.setCaseIndex);
    return sortedCases;
  }

  static sortByExistingCases(caseA: CaseModel, caseB: CaseModel): number {
    // Ordering to make existing cases came first them new cases
    if (!CaseModeler.isNewCase(caseA) && CaseModeler.isNewCase(caseB)) {
      return -1;
    }

    if (CaseModeler.isNewCase(caseA) && !CaseModeler.isNewCase(caseB)) {
      return 1;
    }

    // sort both existing/new cases by name
    return caseA.name.toLowerCase().localeCompare(caseB.name.toLowerCase(), undefined, { numeric: true, sensitivity: 'base' });
  }

  static setCaseIndex(caseEntry: CaseModel, index: number): void {
    caseEntry.caseIndex = index + 1;
  }

  /**
   * Flag the cases that has all targets as empty and set includeInWarrant to false.
   * If all targets are empty, the case will not be displayed in the UI or send to the server.
   *
   * @param cases the cases to flag
   * @returns cases
   */
  static flagCasesWithEmptyTargets(cases: CaseModel[]): CaseModel[] {
    cases.forEach((entry) => {
      const hasSomeTarget = entry.targets.some(({ primaryValue }) => primaryValue != null && primaryValue !== '');
      if (!hasSomeTarget) {
        // This case has no target, hide it from view and prevent it to be included in the warrant
        entry.hideFromView = true;
        entry.includeInWarrant = false;

      } else if (entry.hideFromView === true) {
        // This case has target now, show it and set it to be included in the warrant
        entry.hideFromView = false;
        entry.includeInWarrant = true;
      }
    });

    return cases;
  }
}
