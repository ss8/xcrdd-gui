import cloneDeep from 'lodash.clonedeep';
import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';
import { mockCustomization } from '__test__/mockCustomization';
import { mockCases } from '__test__/mockCases';
import { mockTargets } from '__test__/mockTargets';
import { TargetDefaultAFSelection, TargetDefaultAFSelectionMethod } from 'data/customization/customization.types';
import { getAFSelectionMethods, filterSelectedAfBasedOnSelectionMethod } from './filterSelectedAfBasedOnSelectionMethod';

describe('filterSelectedAfBasedOnSelectionMethod', () => {
  let aCase:CaseModel;
  let aTarget: TargetModel;
  let targetDefaultAFSelection: TargetDefaultAFSelection;

  beforeEach(() => {
    const index = 0;
    aCase = cloneDeep(mockCases[index]);
    aTarget = cloneDeep(mockTargets[index]);
    if (mockCustomization.targetDefaultAFSelection) {
      targetDefaultAFSelection = cloneDeep(mockCustomization.targetDefaultAFSelection);
    }
  });

  it('should not change selected AFs when parameters are null or undefined', () => {
    let expectedTarget = cloneDeep(aTarget);
    filterSelectedAfBasedOnSelectionMethod(aTarget, aCase, undefined);
    expect(aTarget).toEqual(expectedTarget);

    aTarget.afs = undefined;
    expectedTarget.afs = undefined;
    filterSelectedAfBasedOnSelectionMethod(aTarget, aCase, targetDefaultAFSelection);
    expect(aTarget).toEqual(expectedTarget);

    aTarget.primaryType = undefined;
    expectedTarget = cloneDeep(aTarget);
    expectedTarget.primaryType = undefined;
    filterSelectedAfBasedOnSelectionMethod(aTarget, aCase, targetDefaultAFSelection);
    expect(aTarget).toEqual(expectedTarget);
  });

  it('should return no selected AFs when selectionMethod is "selectNone"', () => {
    aCase.services = ['GSMCDMA-2G-VOICE'];
    aTarget.primaryType = 'CASEIDMDN';
    const expectedTargetAFs = {
      ...aTarget.afs,
      afIds: [],
      afGroups: [],
      afAutowireTypes: [],
      afAutowireGroups: [],
    };
    filterSelectedAfBasedOnSelectionMethod(aTarget, aCase, targetDefaultAFSelection);
    expect(aTarget.afs).toEqual(expectedTargetAFs);
  });
});

describe('getAFSelectionMethod', () => {
  let aCase:CaseModel;
  let targetDefaultAFSelection: TargetDefaultAFSelection;
  beforeEach(() => {
    const index = 0;
    aCase = cloneDeep(mockCases[index]);
    if (mockCustomization.targetDefaultAFSelection) {
      targetDefaultAFSelection = cloneDeep(mockCustomization.targetDefaultAFSelection);
    }
  });

  it('should return undefined when parameters are null or undefined', () => {
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, undefined)).toBeUndefined();

    expect(getAFSelectionMethods(undefined, aCase, 'MDN')).toBeUndefined();
  });

  it('should return the selectionMethod matching the service and target when case has one service', () => {
    const expectedSelectionMethods: Set<TargetDefaultAFSelectionMethod> = new Set();
    expectedSelectionMethods.add({ method: 'selectNone' });
    aCase.services = ['GSMCDMA-2G-VOICE'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toEqual(expectedSelectionMethods);
  });

  it('should return the selectionMethod matching the service and target when case has multiple services and one of the services is not defined in TargetDefaultAFSelectionMethod', () => {
    const expectedSelectionMethods: Set<TargetDefaultAFSelectionMethod> = new Set();
    expectedSelectionMethods.add({ method: 'selectNone' });
    aCase.services = ['GSMCDMA-2G-VOICE', 'NEVER_DEFINED_SERVICE'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toEqual(expectedSelectionMethods);
  });

  it('should return the selectionMethod matching the service and target when case has multiple services and both services have same AF selectionMethod', () => {
    const expectedSelectionMethods: Set<TargetDefaultAFSelectionMethod> = new Set();
    expectedSelectionMethods.add({ method: 'selectNone' });
    aCase.services = ['GSMCDMA-2G-VOICE', 'CDMA2000-3G-VOICE'];
    targetDefaultAFSelection['CDMA2000-3G-VOICE'] = targetDefaultAFSelection['GSMCDMA-2G-VOICE'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toEqual(expectedSelectionMethods);
  });

  it('should return undefined when no matching the service and case has one service', () => {
    aCase.services = ['NEVER_DEFINED_SERVICE'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBeUndefined();
  });

  it('should return undefined when no matching the service and target, and case has one service', () => {
    aCase.services = ['NEVER_DEFINED_SERVICE'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'NEVER_DEFINED_TARGET')).toBeUndefined();
  });

  it('should return undefined when no matching the service, and case has multiple services', () => {
    aCase.services = ['NEVER_DEFINED_SERVICE1', 'NEVER_DEFINED_SERVICE2'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBeUndefined();
  });

  it('should return undefined when no matching the service and target, and case has multiple services', () => {
    aCase.services = ['NEVER_DEFINED_SERVICE1', 'NEVER_DEFINED_SERVICE2'];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'NEVER_DEFINED_TARGET')).toBeUndefined();
  });

  it('should return undefined when case services has a "selectNone" selectionMethod and other selectionMethod for the same target', () => {
    aCase.services = ['GSMCDMA-2G-VOICE', 'CDMA2000-3G-VOICE'];
    targetDefaultAFSelection['CDMA2000-3G-VOICE'] = cloneDeep(targetDefaultAFSelection['GSMCDMA-2G-VOICE']);
    targetDefaultAFSelection['CDMA2000-3G-VOICE'].CASEIDMDN.selectionMethods = [{ method: 'selectAFGroupMatchingTargetValue', regExp: '' }];
    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toBeUndefined();
  });

  it('should return the union of unque selection methods when case services has selectionMethods for the same target', () => {
    const selectionMethods: TargetDefaultAFSelectionMethod[] = [
      { method: 'selectAFGroupMatchingTargetValue', regExp: '' },
      { method: 'selectAFGroups', afGroupNames: ['AFGroup1', 'AFGroup2'] },
      { method: 'selectAFs', afNames: ['AF1', 'AF2'] },
    ];

    aCase.services = ['LTE-4G-DATA', 'CDMA2000-3G-VOICE'];
    targetDefaultAFSelection['CDMA2000-3G-VOICE'] = cloneDeep(targetDefaultAFSelection['GSMCDMA-2G-VOICE']);
    targetDefaultAFSelection['CDMA2000-3G-VOICE'].CASEIDMDN.selectionMethods = [selectionMethods[0]];
    targetDefaultAFSelection['LTE-4G-DATA'] = cloneDeep(targetDefaultAFSelection['GSMCDMA-2G-VOICE']);
    targetDefaultAFSelection['LTE-4G-DATA'].CASEIDMDN.selectionMethods = [selectionMethods[1], selectionMethods[2]];

    const expectedSelectionMethods: Set<TargetDefaultAFSelectionMethod> = new Set();
    expectedSelectionMethods.add(selectionMethods[0]);
    expectedSelectionMethods.add(selectionMethods[1]);
    expectedSelectionMethods.add(selectionMethods[2]);

    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toEqual(expectedSelectionMethods);
  });

  it('should return the union of unque selection methods when case services has some same selectionMethods for the same target', () => {
    const selectionMethods: TargetDefaultAFSelectionMethod[] = [
      { method: 'selectAFGroupMatchingTargetValue', regExp: '' },
      { method: 'selectAFGroups', afGroupNames: ['AFGroup1', 'AFGroup2'] },
      { method: 'selectAFs', afNames: ['AF1', 'AF2'] },
    ];

    aCase.services = ['LTE-4G-DATA', 'CDMA2000-3G-VOICE'];
    targetDefaultAFSelection['CDMA2000-3G-VOICE'] = cloneDeep(targetDefaultAFSelection['GSMCDMA-2G-VOICE']);
    targetDefaultAFSelection['CDMA2000-3G-VOICE'].CASEIDMDN.selectionMethods = [selectionMethods[0], selectionMethods[1]];
    targetDefaultAFSelection['LTE-4G-DATA'] = cloneDeep(targetDefaultAFSelection['GSMCDMA-2G-VOICE']);
    targetDefaultAFSelection['LTE-4G-DATA'].CASEIDMDN.selectionMethods = [selectionMethods[1], selectionMethods[2]];

    const expectedSelectionMethods: Set<TargetDefaultAFSelectionMethod> = new Set();
    expectedSelectionMethods.add(selectionMethods[0]);
    expectedSelectionMethods.add(selectionMethods[1]);
    expectedSelectionMethods.add(selectionMethods[2]);

    expect(getAFSelectionMethods(targetDefaultAFSelection, aCase, 'CASEIDMDN')).toEqual(expectedSelectionMethods);
  });
});
