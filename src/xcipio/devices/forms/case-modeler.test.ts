import cloneDeep from 'lodash.clonedeep';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { TargetModel } from 'data/target/target.types';
import { CaseModel } from 'data/case/case.types';
import { mockCases } from '__test__/mockCases';
import { mockCustomization } from '__test__/mockCustomization';
import { Ontology } from '../../../data/supportedFeatures/supportedFeatures.types';
import {
  AccessFunctionsByCollectionFunctionTypeMap,
} from '../device.types';
import CaseModeler, { TEMPORARY_CASE_ID_PREFIX } from './case-modeler';
import serviceMap from './__test__/afTypeAndCFTypeByServiceMap-test-data.json';
import {
  CUSTOMER_TARGETS,
} from './__test__/deviceModeler-data-mock';
import {
  CF_LIST,
  WARRANT,
  DEVICE_5GDATA,
  EXPECTED_CASE_FOR_DEVICE_5GDATA,
  EXPECTED_TARGETS_FOR_DEVICE_5GDATA,
  DEVICE_4GPD,
  EXPECTED_CASE_FOR_DEVICE_4GPD,
  EXPECTED_TARGETS_FOR_DEVICE_4GPD,
  DEVICE_3GPD,
  EXPECTED_CASE_FOR_DEVICE_3GPD,
  EXPECTED_TARGETS_FOR_DEVICE_3GPD,
  DEVICE_CIRCUITSWITCH,
  EXPECTED_CASE_FOR_DEVICE_CIRCUITSWITCH,
  EXPECTED_TARGETS_FOR_DEVICE_CIRCUITSWITCH,
  DEVICE_GIZMO,
  EXPECTED_CASE_FOR_DEVICE_GIZMO,
  EXPECTED_TARGETS_FOR_DEVICE_GIZMO,
  DEVICE_VOLTE4G,
  EXPECTED_CASE_FOR_DEVICE_VOLTE4G,
  EXPECTED_TARGETS_FOR_DEVICE_VOLTE4G,
} from './__test__/case-modeler-test-data';
import {
  CASE_DATA_3G,
  CASE_DATA_4G,
  CASE_DATA_5G,
  CASE_DATA_CS,
  CASE_DATA_GIZMO,
  CASE_DATA_IOT,
  CASE_DATA_PTT,
  CASE_DATA_RCS,
  CASE_DATA_VOICE,
  CASE_DATA_VOLTE_4G,
  CASE_DATA_UMTS,
  CASE_DATA_VISIBLE,
  CASE_DATA_RESULT_3G,
  CASE_DATA_RESULT_4G,
  CASE_DATA_RESULT_5G,
  CASE_DATA_RESULT_CS,
  CASE_DATA_RESULT_GIZMO,
  CASE_DATA_RESULT_IOT,
  CASE_DATA_RESULT_PTT,
  CASE_DATA_RESULT_RCS,
  CASE_DATA_RESULT_VOICE,
  CASE_DATA_RESULT_VOLTE_4G,
  CASE_DATA_RESULT_UMTS,
  CASE_DATA_RESULT_VISIBLE,
  DEVICE_DATA_3G,
  DEVICE_DATA_4G,
  DEVICE_DATA_5G,
  DEVICE_DATA_CS,
  DEVICE_DATA_GIZMO,
  DEVICE_DATA_IOT,
  DEVICE_DATA_PTT,
  DEVICE_DATA_RCS,
  DEVICE_DATA_VOICE,
  DEVICE_DATA_VOLTE_4G,
  DEVICE_DATA_UMTS,
  DEVICE_DATA_VISIBLE,
  DEVICE_DATA_SKELETON,
  TARGET_DATA_3G,
  TARGET_DATA_4G,
  TARGET_DATA_5G,
  TARGET_DATA_CS,
  TARGET_DATA_GIZMO,
  TARGET_DATA_IOT,
  TARGET_DATA_PTT,
  TARGET_DATA_RCS,
  TARGET_DATA_VOICE,
  TARGET_DATA_VOLTE_4G,
  TARGET_DATA_UMTS,
  TARGET_DATA_VISIBLE,
} from './__test__/case-modeler-existing-cases';

describe('CaseModeler', () => {
  const ontology: Ontology = mockSupportedFeatures.config;
  let accessFunctionsFor5G: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsFor4G: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsFor4GVoice: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsFor3G: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForCircuitSwitch: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForRCS: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForGizmo: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForIoT: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForPTT: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForUMTS: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForVOIP: AccessFunctionsByCollectionFunctionTypeMap;
  let accessFunctionsForVisible: AccessFunctionsByCollectionFunctionTypeMap;

  beforeEach(() => {
    accessFunctionsFor5G = {
      'CF-ETSI-102232': new Set([
        'ERK_SMF',
      ]),
    };

    accessFunctionsFor4G = {
      'CF-3GPP-33108': new Set([
        'EPS',
        'ERK_SMF',
        'ERK_VMME',
        'ALU_MME',
        'PCRF',
        'HP_AAA',
        'ALU_HLR',
        'ALU_SGW',
        'MV_SMSIWF',
        'ERK_SGW',
        'STA_GGSN',
        'NSNHLR_SDM_PRGW',
      ]),
      'CF-ETSI-102232': new Set([
        'ERK_SMF',
      ]),
    };

    accessFunctionsFor4GVoice = {
      'CF-ATIS-0700005': new Set([
        'NSN_CSCF',
        'SONUS_SBC',
      ]),
      'CF-ETSI-102232': new Set([
        'ACME_X123',
      ]),
      'CF-TIA-1066': new Set([
        'NSN_CSCF',
        'SONUS_SBC',
      ]),
    };

    accessFunctionsFor3G = {
      'CF-JSTD025B': new Set([
        'STA_PDSN',
        'STARENT_EHA',
        'BRW_AAA',
      ]),
      'CF-TIA-1072': new Set([
        'STA_PDSN',
        'STARENT_EHA',
      ]),
    };

    accessFunctionsForCircuitSwitch = {
      'CF-JSTD025A': new Set([
        'NORTEL_MTX',
        'NORTEL_HLR',
        'SS8_DF',
        'NORTEL_MSC',
        'NORTEL_DF',
        'HP_HLR',
        'SAMSUNG_MSC',
      ]),
    };

    accessFunctionsForRCS = {
      'CF-3GPP-33108': new Set([
        'MVNR_RCS_MSTORE',
      ]),
      'CF-ETSI-102232': new Set([
        'NSN_CSCF',
        'ICS_RCS',
        'MVNR_RCS_AS',
      ]),
    };

    accessFunctionsForGizmo = {
      'CF-3GPP-33108': new Set([
        'GIZMO',
      ]),
    };

    accessFunctionsForIoT = {
      'CF-3GPP-33108': new Set([
        'ERK_SCEF',
      ]),
    };

    accessFunctionsForPTT = {
      'CF-TIA-1072': new Set([
        'KODIAK_PTT',
      ]),
    };

    accessFunctionsForUMTS = {
      'CF-JSTD025A': new Set([
        'ALU_DSS',
      ]),
    };

    accessFunctionsForVOIP = {
      'CF-ATIS-1000678': new Set([
        'ACME_X123',
      ]),
    };

    accessFunctionsForVisible = {
      'CF-3GPP-33108': new Set([
        'MVNR_HSS',
        'MVNR_PGW',
      ]),
      'CF-ATIS-0700005': new Set([
        'MVNR_SBC',
        'MITEL_TAS',
        'MVNR_CSCF',
      ]),
    };
  });

  describe('getCompatibleCFTagGroupBasedOnServicesAndCF()', () => {
    it('when CF Tag not specified', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-3GPP-33108'],
        ['CF-ETSI-102232'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['LTE-4G-DATA'],
        undefined,
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-3GPP-33108', 'CF-ETSI-102232'],
      );
    });

    it('when services not specified', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-3GPP-33108'],
        ['CF-ETSI-102232'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        [],
        'CF-ETSI-102232',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ETSI-102232'],
      );
    });

    it('when the case CF tag does not have other compatible CF tags', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-3GPP-33108'],
        ['CF-ETSI-102232'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['LTE-4G-DATA'],
        'CF-3GPP-33108',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-3GPP-33108'],
      );
    });

    it('when the case CF tag has other compatible CF tags', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-ATIS-0700005', 'CF-TIA-1066'],
        ['CF-ETSI-102232'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE'],
        'CF-ATIS-0700005',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ATIS-0700005', 'CF-TIA-1066'],
      );
    });

    it('when services have only one CF tag in common, e.g. Volte-4G and Visible only has CF-ATIS-0700005 in common', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-ATIS-0700005', 'CF-TIA-1066'],
        ['CF-ETSI-102232'],
        ['CF-3GPP-33108'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE', 'VZ-VISIBLE'],
        'CF-ATIS-0700005',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ATIS-0700005'],
      );
    });

    it('when services have more than one CF tag in common, e.g. Volte-4G and RCS can be provisioned on CF-ETSI-102232, CF-3GPP-33108', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-ATIS-0700005', 'CF-TIA-1066'],
        ['CF-ETSI-102232'],
        ['CF-3GPP-33108'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE', 'VOLTE-RCS'],
        'CF-ETSI-102232',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ETSI-102232'],
      );

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE', 'VOLTE-RCS'],
        'CF-ATIS-0700005',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ATIS-0700005'],
      );
    });

    it('when services have more than one CF tag in common, e.g. Volte-4G, RCS, 4GPD can be provisioned on CF-ETSI-102232, CF-3GPP-33108', () => {
      const orderedGroupsOfCompatibleCFTags = [
        ['CF-ATIS-0700005', 'CF-TIA-1066'],
        ['CF-ETSI-102232'],
        ['CF-3GPP-33108'],
      ];

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE', 'VOLTE-RCS', 'LTE-4G-DATA'],
        'CF-ETSI-102232',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-ETSI-102232'],
      );

      expect(CaseModeler.getCompatibleCFTagGroupBasedOnServicesAndCF(
        ['VOLTE-4G-VOICE', 'VOLTE-RCS', 'LTE-4G-DATA'],
        'CF-TIA-1066',
        orderedGroupsOfCompatibleCFTags,
        serviceMap,
      )).toEqual(
        ['CF-TIA-1066'],
      );
    });

  });

  describe('getCasesModelForDevice()', () => {
    describe('when access function set is empty', () => {
      it('should return an empty array', () => {

        const services:string[] = [];
        const cases = CaseModeler.getCasesModelForDevice(
          services,
          [],
          {},
          {},
        );

        expect(cases).toEqual([]);
      });
    });

    describe('when service is 5G data', () => {
      it('should return the corresponding case', () => {
        const services:string[] = ['5G-DATA'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ETSI-102232': new Set(['5G-DATA']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsFor5G,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsFor5G['CF-ETSI-102232']],
          cfs: [
            'CF-ETSI-102232',
          ],
          targets: [],
          services: [
            '5G-DATA',
          ],
        }]);
      });
    });

    describe('when service is 4G Voice', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['VOLTE-4G-VOICE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-0700005': new Set(['VOLTE-4G-VOICE']),
          'CF-TIA-1066': new Set(['VOLTE-4G-VOICE']),
          'CF-ETSI-102232': new Set(['VOLTE-4G-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = [
          ['CF-ATIS-0700005', 'CF-TIA-1066'],
          ['CF-ETSI-102232'],
        ];

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsFor4GVoice,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([
          {
            afs: [...accessFunctionsFor4GVoice['CF-TIA-1066']],
            cfs: [
              'CF-ATIS-0700005',
              'CF-TIA-1066',
            ],
            targets: [],
            services,
          }, {
            afs: [...accessFunctionsFor4GVoice['CF-ETSI-102232']],
            cfs: [
              'CF-ETSI-102232',
            ],
            targets: [],
            services,
          },
        ]);
      });
    });

    describe('when services are 4G PD and 5G data', () => {
      it('should return the corresponding case', () => {
        const services:string[] = ['LTE-4G-DATA', '5G-DATA'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-3GPP-33108': new Set(['LTE-4G-DATA']),
          'CF-ETSI-102232': new Set(['LTE-4G-DATA', '5G-DATA']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsFor4G,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsFor4G['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services: [
            'LTE-4G-DATA',
          ],
        },
        {
          afs: [...accessFunctionsFor5G['CF-ETSI-102232']],
          cfs: [
            'CF-ETSI-102232',
          ],
          targets: [],
          services: [
            'LTE-4G-DATA',
            '5G-DATA',
          ],
        }]);
      });
    });

    describe('when service is 3G PD', () => {
      it('should return the corresponding case', () => {
        const services:string[] = ['CDMA2000-3G-DATA'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-JSTD025B': new Set(['CDMA2000-3G-DATA']),
          'CF-TIA-1072': new Set(['CDMA2000-3G-DATA']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsFor3G,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([
          {
            afs: [...accessFunctionsFor3G['CF-JSTD025B']],
            cfs: [
              'CF-JSTD025B',
            ],
            targets: [],
            services,
          },
          {
            afs: [...accessFunctionsFor3G['CF-TIA-1072']],
            cfs: [
              'CF-TIA-1072',
            ],
            targets: [],
            services,
          },
        ]);
      });
    });

    describe('when services are 4G PD and 3G PD', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['CDMA2000-3G-DATA', 'LTE-4G-DATA'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-JSTD025B': new Set(['CDMA2000-3G-DATA']),
          'CF-TIA-1072': new Set(['CDMA2000-3G-DATA']),
          'CF-3GPP-33108': new Set(['LTE-4G-DATA']),
          'CF-ETSI-102232': new Set(['LTE-4G-DATA']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          { ...accessFunctionsFor3G, ...accessFunctionsFor4G },
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsFor3G['CF-JSTD025B']],
          cfs: [
            'CF-JSTD025B',
          ],
          targets: [],
          services: ['CDMA2000-3G-DATA'],
        }, {
          afs: [...accessFunctionsFor3G['CF-TIA-1072']],
          cfs: [
            'CF-TIA-1072',
          ],
          targets: [],
          services: ['CDMA2000-3G-DATA'],
        }, {
          afs: [...accessFunctionsFor4G['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services: ['LTE-4G-DATA'],
        },
        {
          afs: [...accessFunctionsFor4G['CF-ETSI-102232']],
          cfs: [
            'CF-ETSI-102232',
          ],
          targets: [],
          services: ['LTE-4G-DATA'],
        },
        ]);
      });
    });

    describe('when service is Circuit Switch', () => {
      it('should return the corresponding case', () => {
        const services:string[] = ['GSMCDMA-2G-VOICE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-JSTD025A': new Set(['GSMCDMA-2G-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForCircuitSwitch,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForCircuitSwitch['CF-JSTD025A']],
          cfs: [
            'CF-JSTD025A',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is RCS and 4G Voice', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['VOLTE-RCS', 'VOLTE-4G-VOICE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-0700005': new Set(['VOLTE-4G-VOICE']),
          'CF-TIA-1066': new Set(['VOLTE-4G-VOICE']),
          'CF-3GPP-33108': new Set(['VOLTE-RCS']),
          'CF-ETSI-102232': new Set(['VOLTE-RCS', 'VOLTE-4G-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = [
          ['CF-ATIS-0700005', 'CF-TIA-1066'],
          ['CF-3GPP-33108'],
          ['CF-ETSI-102232'],
        ];

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          {
            'CF-ATIS-0700005': accessFunctionsFor4GVoice['CF-ATIS-0700005'],
            'CF-TIA-1066': accessFunctionsFor4GVoice['CF-TIA-1066'],
            'CF-3GPP-33108': accessFunctionsForRCS['CF-3GPP-33108'],
            'CF-ETSI-102232': new Set([...accessFunctionsForRCS['CF-ETSI-102232'], ...accessFunctionsFor4GVoice['CF-ETSI-102232']]),
          },
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsFor4GVoice['CF-TIA-1066']],
          cfs: [
            'CF-ATIS-0700005',
            'CF-TIA-1066',
          ],
          targets: [],
          services: [
            'VOLTE-4G-VOICE',
          ],
        }, {
          afs: [...accessFunctionsForRCS['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services: [
            'VOLTE-RCS',
          ],
        }, {
          afs: [
            ...accessFunctionsForRCS['CF-ETSI-102232'],
            ...accessFunctionsFor4GVoice['CF-ETSI-102232'],
          ],
          cfs: [
            'CF-ETSI-102232',
          ],
          targets: [],
          services: [
            'VOLTE-RCS',
            'VOLTE-4G-VOICE',
          ],
        }]);
      });
    });

    describe('when service is RCS', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['VOLTE-RCS'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-3GPP-33108': new Set(['VOLTE-RCS']),
          'CF-ETSI-102232': new Set(['VOLTE-RCS']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForRCS,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([
          {
            afs: [...accessFunctionsForRCS['CF-3GPP-33108']],
            cfs: [
              'CF-3GPP-33108',
            ],
            targets: [],
            services,
          }, {
            afs: [...accessFunctionsForRCS['CF-ETSI-102232']],
            cfs: [
              'CF-ETSI-102232',
            ],
            targets: [],
            services,
          },
        ]);
      });
    });

    describe('when service is Gizmo', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['LTE-GIZMO'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-3GPP-33108': new Set(['LTE-GIZMO']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForGizmo,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForGizmo['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is IoT', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['LTE-IOT'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-3GPP-33108': new Set(['LTE-IOT']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForIoT,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForIoT['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is PTT', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['PTT'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-TIA-1072': new Set(['PTT']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForPTT,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForPTT['CF-TIA-1072']],
          cfs: [
            'CF-TIA-1072',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is UMTS', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['UMTS-3G-VOICE'];

        const serviceByCollectionFunctionTypeMap = {
          'CF-JSTD025A': new Set(['UMTS-3G-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForUMTS,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForUMTS['CF-JSTD025A']],
          cfs: [
            'CF-JSTD025A',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is VOIP', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['VOIP-VOICE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-1000678': new Set(['VOIP-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForVOIP,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForVOIP['CF-ATIS-1000678']],
          cfs: [
            'CF-ATIS-1000678',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when service is Visible', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['VZ-VISIBLE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-0700005': new Set(['VZ-VISIBLE']),
          'CF-3GPP-33108': new Set(['VZ-VISIBLE']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          accessFunctionsForVisible,
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForVisible['CF-ATIS-0700005']],
          cfs: [
            'CF-ATIS-0700005',
          ],
          targets: [],
          services,
        }, {
          afs: [...accessFunctionsForVisible['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services,
        }]);
      });
    });

    describe('when services are Gizmo, IoT, RCS and Visible', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['LTE-GIZMO', 'LTE-IOT', 'VOLTE-RCS', 'VZ-VISIBLE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-0700005': new Set(['VZ-VISIBLE']),
          'CF-3GPP-33108': new Set(['LTE-GIZMO', 'LTE-IOT', 'VOLTE-RCS', 'VZ-VISIBLE']),
          'CF-ETSI-102232': new Set(['VOLTE-RCS']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          {
            'CF-3GPP-33108': new Set([
              ...accessFunctionsForGizmo['CF-3GPP-33108'],
              ...accessFunctionsForIoT['CF-3GPP-33108'],
              ...accessFunctionsForRCS['CF-3GPP-33108'],
              ...accessFunctionsForVisible['CF-3GPP-33108'],
            ]),
            'CF-ETSI-102232': accessFunctionsForRCS['CF-ETSI-102232'],
            'CF-ATIS-0700005': accessFunctionsForVisible['CF-ATIS-0700005'],
          },
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForVisible['CF-ATIS-0700005']],
          cfs: [
            'CF-ATIS-0700005',
          ],
          targets: [],
          services: [
            'VZ-VISIBLE',
          ],
        }, {
          afs: [
            ...accessFunctionsForGizmo['CF-3GPP-33108'],
            ...accessFunctionsForIoT['CF-3GPP-33108'],
            ...accessFunctionsForRCS['CF-3GPP-33108'],
            ...accessFunctionsForVisible['CF-3GPP-33108'],
          ],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services: [
            'LTE-GIZMO',
            'LTE-IOT',
            'VOLTE-RCS',
            'VZ-VISIBLE',
          ],
        }, {
          afs: [...accessFunctionsForRCS['CF-ETSI-102232']],
          cfs: [
            'CF-ETSI-102232',
          ],
          targets: [],
          services: [
            'VOLTE-RCS',
          ],
        }]);
      });
    });

    describe('when services are 5G data, 3G PD, Circuit Switch, 4G PD, 4G Voice', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['5G-DATA', 'CDMA2000-3G-DATA', 'GSMCDMA-2G-VOICE',
          'LTE-4G-DATA', 'VOLTE-4G-VOICE'];

        const serviceByCollectionFunctionTypeMap = {
          'CF-ATIS-0700005': new Set(['VOLTE-4G-VOICE']),
          'CF-TIA-1066': new Set(['VOLTE-4G-VOICE']),
          'CF-ETSI-102232': new Set(['5G-DATA', 'LTE-4G-DATA', 'VOLTE-4G-VOICE']),
          'CF-JSTD025B': new Set(['CDMA2000-3G-DATA']),
          'CF-TIA-1072': new Set(['CDMA2000-3G-DATA']),
          'CF-JSTD025A': new Set(['GSMCDMA-2G-VOICE']),
          'CF-3GPP-33108': new Set(['LTE-4G-DATA']),
        };
        const orderedGroupsOfCompatibleCFTags = [
          ['CF-ATIS-0700005', 'CF-TIA-1066'],
          ['CF-ETSI-102232'],
          ['CF-JSTD025B'],
          ['CF-TIA-1072'],
          ['CF-JSTD025A'],
          ['CF-3GPP-33108'],
        ];

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          {
            'CF-ATIS-0700005': accessFunctionsFor4GVoice['CF-ATIS-0700005'],
            'CF-TIA-1066': accessFunctionsFor4GVoice['CF-TIA-1066'],
            'CF-ETSI-102232': new Set([...accessFunctionsFor5G['CF-ETSI-102232'], ...accessFunctionsFor4GVoice['CF-ETSI-102232'], ...accessFunctionsFor4G['CF-ETSI-102232']]),
            ...accessFunctionsFor3G,
            ...accessFunctionsForCircuitSwitch,
            'CF-3GPP-33108': accessFunctionsFor4G['CF-3GPP-33108'],
          },
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsFor4GVoice['CF-TIA-1066']],
          cfs: [
            'CF-ATIS-0700005',
            'CF-TIA-1066',
          ],
          targets: [],
          services: ['VOLTE-4G-VOICE'],
        }, {
          afs: expect.arrayContaining([
            ...accessFunctionsFor5G['CF-ETSI-102232'],
            ...accessFunctionsFor4GVoice['CF-ETSI-102232'],
          ]),
          cfs: [
            'CF-ETSI-102232',
          ],
          services: [
            '5G-DATA',
            'LTE-4G-DATA',
            'VOLTE-4G-VOICE',
          ],
          targets: [],
        }, {
          afs: expect.arrayContaining([
            ...accessFunctionsFor3G['CF-JSTD025B'],
          ]),
          cfs: [
            'CF-JSTD025B',
          ],
          targets: [],
          services: ['CDMA2000-3G-DATA'],
        }, {
          afs: [...accessFunctionsFor3G['CF-TIA-1072']],
          cfs: [
            'CF-TIA-1072',
          ],
          targets: [],
          services: ['CDMA2000-3G-DATA'],
        }, {
          afs: [...accessFunctionsForCircuitSwitch['CF-JSTD025A']],
          cfs: [
            'CF-JSTD025A',
          ],
          targets: [],
          services: ['GSMCDMA-2G-VOICE'],
        }, {
          afs: [...accessFunctionsFor4G['CF-3GPP-33108']],
          cfs: [
            'CF-3GPP-33108',
          ],
          targets: [],
          services: ['LTE-4G-DATA'],
        }]);
      });
    });

    describe('when services are PTT, UMTS and VOIP', () => {
      it('should return the corresponding cases', () => {
        const services:string[] = ['PTT', 'UMTS-3G-VOICE', 'VOIP-VOICE'];
        const serviceByCollectionFunctionTypeMap = {
          'CF-TIA-1072': new Set(['PTT']),
          'CF-JSTD025A': new Set(['UMTS-3G-VOICE']),
          'CF-ATIS-1000678': new Set(['VOIP-VOICE']),
        };
        const orderedGroupsOfCompatibleCFTags = Object.keys(serviceByCollectionFunctionTypeMap).map((cfTag) => [cfTag]);

        const caseModel = CaseModeler.getCasesModelForDevice(
          services,
          orderedGroupsOfCompatibleCFTags,
          { ...accessFunctionsForPTT, ...accessFunctionsForUMTS, ...accessFunctionsForVOIP },
          serviceByCollectionFunctionTypeMap,
        );

        expect(caseModel).toEqual([{
          afs: [...accessFunctionsForPTT['CF-TIA-1072']],
          cfs: [
            'CF-TIA-1072',
          ],
          targets: [],
          services: ['PTT'],
        }, {
          afs: [...accessFunctionsForUMTS['CF-JSTD025A']],
          cfs: [
            'CF-JSTD025A',
          ],
          targets: [],
          services: ['UMTS-3G-VOICE'],
        }, {
          afs: [...accessFunctionsForVOIP['CF-ATIS-1000678']],
          cfs: [
            'CF-ATIS-1000678',
          ],
          targets: [],
          services: ['VOIP-VOICE'],
        }]);
      });
    });
  });

  /**
   * Testing generateCases because we also need to make sure targets are generated properly
   */
  describe('generateCases()', () => {

    let caseModeler: CaseModeler| null = null;

    beforeEach(() => {
      const propsToSend = {
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: true,
      };

      const allTargets : any[] = [];
      const caseNamePrefix = 'Case';
      caseModeler = new CaseModeler(propsToSend);
      caseModeler.setConfiguration(CF_LIST, [], allTargets, caseNamePrefix);

    });

    it('when service is 5G Data', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_5GDATA);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(1);
      const aCase = caseModeler.cases[keys[0]];
      expect(aCase).toMatchObject(EXPECTED_CASE_FOR_DEVICE_5GDATA);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_5GDATA);
    });

    it('when service is 4G PD ', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_4GPD);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(2);
      const aCase = caseModeler.cases[keys[0]];
      expect(aCase).toMatchObject(EXPECTED_CASE_FOR_DEVICE_4GPD);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_4GPD);
    });

    it('when service is 3G PD ', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_3GPD);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(1);
      const aCase = caseModeler.cases[keys[0]];
      expect(aCase).toMatchObject(EXPECTED_CASE_FOR_DEVICE_3GPD);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_3GPD);
    });

    it('when service is Circuit Switch ', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_CIRCUITSWITCH);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(1);
      const aCase = caseModeler.cases[keys[0]];
      expect(aCase).toMatchObject(EXPECTED_CASE_FOR_DEVICE_CIRCUITSWITCH);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_CIRCUITSWITCH);
    });

    it('when service is Gizmo ', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_GIZMO);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(1);
      const aCase = caseModeler.cases[keys[0]];
      expect(aCase).toMatchObject(EXPECTED_CASE_FOR_DEVICE_GIZMO);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_GIZMO);
    });

    it('when service is Volte 4G ', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      caseModeler.generateCases(DEVICE_VOLTE4G);

      expect(caseModeler?.cases).toBeDefined();
      const keys = Object.keys(caseModeler.cases);
      expect(keys.length).toBe(2);
      expect(caseModeler.cases).toMatchObject(EXPECTED_CASE_FOR_DEVICE_VOLTE4G);
      expect(caseModeler.targets).toMatchObject(EXPECTED_TARGETS_FOR_DEVICE_VOLTE4G);
    });

    it('should generate cases with Case Name', () => {
      caseModeler = new CaseModeler({
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: true,
      });
      caseModeler.setConfiguration(CF_LIST, [], [], 'Case');
      caseModeler.generateCases(DEVICE_VOLTE4G);

      expect(caseModeler.cases).toMatchObject({
        ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G,
        'DX-CASE-1': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-1'],
          liId: 'Case 1',
        },
        'DX-CASE-2': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-2'],
          liId: 'Case 2',
        },
      });
    });

    it('should generate cases without Case Name', () => {
      caseModeler = new CaseModeler({
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: false,
      });
      caseModeler.setConfiguration(CF_LIST, [], [], 'Case');
      caseModeler.generateCases(DEVICE_VOLTE4G);

      expect(caseModeler.cases).toMatchObject({
        ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G,
        'DX-CASE-1': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-1'],
          liId: '',
        },
        'DX-CASE-2': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-2'],
          liId: '',
        },
      });
    });

    it('should extract the ending number from id if shouldAutomaticallyGenerateCaseName is false', () => {
      caseModeler = new CaseModeler({
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: false,
      });

      const cases = cloneDeep(Object.values(EXPECTED_CASE_FOR_DEVICE_4GPD));
      cases[0].id = 'DX-Case-10';
      cases[0].liId = 'Case 20';

      caseModeler.setConfiguration(CF_LIST, cases, [], 'Case');
      caseModeler.generateCases(DEVICE_VOLTE4G);

      expect(caseModeler.cases).toMatchObject({
        'DX-CASE-11': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-1'],
          id: 'DX-CASE-11',
          name: 'Case 11',
          liId: '',
        },
        'DX-CASE-12': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-2'],
          id: 'DX-CASE-12',
          name: 'Case 12',
          liId: '',
        },
      });
    });

    it('should extract the ending number from liId if shouldAutomaticallyGenerateCaseName is true', () => {
      caseModeler = new CaseModeler({
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: true,
      });

      const cases = cloneDeep(Object.values(EXPECTED_CASE_FOR_DEVICE_4GPD));
      cases[0].id = 'DX-Case-10';
      cases[0].liId = 'Case 20';

      caseModeler.setConfiguration(CF_LIST, cases, [], 'Case');
      caseModeler.generateCases(DEVICE_VOLTE4G);

      expect(caseModeler.cases).toMatchObject({
        'DX-CASE-21': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-1'],
          id: 'DX-CASE-21',
          name: 'Case 21',
          liId: 'Case 21',
        },
        'DX-CASE-22': {
          ...EXPECTED_CASE_FOR_DEVICE_VOLTE4G['DX-CASE-2'],
          id: 'DX-CASE-22',
          name: 'Case 22',
          liId: 'Case 22',
        },
      });
    });
  });

  describe('generateCases() - with cases', () => {
    let caseModeler: CaseModeler | null = null;
    const caseNamePrefix = 'Case';

    beforeEach(() => {
      caseModeler = new CaseModeler({
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: true,
      });
    });

    it('should generate cases and targets for 5G', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_5G, TARGET_DATA_5G, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_5G);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_5G);
    });

    it('should generate cases and targets for 3G', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_3G, TARGET_DATA_3G, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_3G);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_3G);
    });

    it('should generate cases and targets for Circuit Switch', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_CS, TARGET_DATA_CS, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_CS);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_CS);
    });

    it('should generate cases and targets for 4G', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_4G, TARGET_DATA_4G, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_4G);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_4G);
    });

    it('should generate cases and targets for Gizmo', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_GIZMO, TARGET_DATA_GIZMO, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_GIZMO);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_GIZMO);
    });

    it('should generate cases and targets for IoT', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_IOT, TARGET_DATA_IOT, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_IOT);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_IOT);
    });

    it('should generate cases and targets for PTT', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_PTT, TARGET_DATA_PTT, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_PTT);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_PTT);
    });

    it('should generate cases and targets for UMTS', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_UMTS, TARGET_DATA_UMTS, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_UMTS);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_UMTS);
    });

    it('should generate cases and targets for Voice over package data', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_VOICE, TARGET_DATA_VOICE, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_VOICE);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_VOICE);
    });

    it('should generate cases and targets for VoLTE 4G Voice', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_VOLTE_4G, TARGET_DATA_VOLTE_4G, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_VOLTE_4G);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_VOLTE_4G);
    });

    it('should generate cases and targets for RCS', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_RCS, TARGET_DATA_RCS, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_RCS);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_RCS);
    });

    it('should generate cases and targets for Visible', () => {
      caseModeler?.setConfiguration(CF_LIST, CASE_DATA_VISIBLE, TARGET_DATA_VISIBLE, caseNamePrefix);
      caseModeler?.generateCases(DEVICE_DATA_VISIBLE);

      expect(caseModeler?.cases).toEqual(CASE_DATA_RESULT_VISIBLE);
    });
  });

  describe('setTargetValue()', () => {
    let caseModeler: CaseModeler;

    beforeEach(() => {
      const propsToSend = {
        warrant: WARRANT,
        serviceMap,
        customerTargets: CUSTOMER_TARGETS,
        cfPool: CF_LIST,
        ontology,
        targetDefaultAFSelection: mockCustomization.targetDefaultAFSelection,
        shouldAutomaticallyGenerateCaseName: true,
      };

      const allTargets : TargetModel[] = [];
      const caseNamePrefix = 'Case';
      caseModeler = new CaseModeler(propsToSend);
      caseModeler.setConfiguration(CF_LIST, [], allTargets, caseNamePrefix);
    });

    it('when customization target uses :s substitution pattern and parent is the same target type', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }

      const device = DEVICE_DATA_SKELETON[1];
      device.types.MSISDN[0].target.primaryValue = '44114411144'; // parent
      device.services = ['MOBILE'];
      const target: TargetModel = { id: 'MSISDN_Target' };
      caseModeler.setTargetValue(target, device, 'MSISDN');
      expect(target.primaryValue).toEqual('114411144');
    });

    it('when customization target uses :s substitution pattern and parent is the different target type', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      const device = DEVICE_DATA_SKELETON[1];
      device.services = ['VOICEMAIL'];
      device.types.DN[0].target.primaryValue = '44114411144'; // parent
      const target: TargetModel = { id: 'MSISDN_Target' };
      caseModeler.setTargetValue(target, device, 'MSISDN');
      expect(target.primaryValue).toEqual('0114411144');
    });

    it('when customization target uses :s substitution pattern but parent does not exist', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }

      const device = DEVICE_DATA_SKELETON[1];
      device.services = ['MOBILE'];
      const target: TargetModel = { id: 'MSISDN_Target', primaryValue: '' };
      caseModeler.setTargetValue(target, device, 'NOT_EXIST');
      expect(target.primaryValue).toEqual('');
    });

    it('when customization target uses :s substitution pattern and parent has no value', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }
      const device = DEVICE_DATA_SKELETON[1];
      device.services = ['MOBILE'];
      device.types.MSISDN[0].target.primaryValue = ''; // parent
      const target: TargetModel = { id: 'MSISDN_Target' };
      caseModeler.setTargetValue(target, device, 'MSISDN');
      expect(target.primaryValue).toEqual('');
    });

    it('when customization target uses the {} substitution pattern', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }

      const device = DEVICE_DATA_VOICE[1];
      device.types.MDN[0].target.primaryValue = '123456789'; // parent
      const target: TargetModel = { id: 'TEL-URI_Target' };
      caseModeler.setTargetValue(target, device, 'TEL-URI');
      expect(target.primaryValue).toEqual('tel:+1123456789');
    });

    it('when customization target uses the {} substitution pattern and parent has no value', () => {
      if (!caseModeler || !caseModeler.cases) {
        expect('caseModeler initialized correctly').toBe('caseModeler not initialized');
        return;
      }

      const device = DEVICE_DATA_VOICE[1];
      device.types.MDN[0].target.primaryValue = ''; // parent
      const target: TargetModel = { id: 'TEL-URI_Target' };
      caseModeler.setTargetValue(target, device, 'TEL-URI');
      expect(target.primaryValue).toEqual('');
    });
  });

  describe('sortCases()', () => {
    it('should sort the cases and set its caseIndex', () => {
      const cases = cloneDeep(mockCases);

      expect(CaseModeler.sortCases(cases)).toEqual([{
        ...mockCases[0],
        caseIndex: 1,
      }, {
        ...mockCases[1],
        caseIndex: 2,
      }]);
    });
  });

  describe('sortByExistingCases()', () => {
    let caseA: CaseModel;
    let caseB: CaseModel;

    beforeEach(() => {
      caseA = {
        ...mockCases[0],
        id: '1',
        name: 'name 1',
        liId: 'liid 1',
      };

      caseB = {
        ...mockCases[0],
        id: '2',
        name: 'name 2',
        liId: 'liid 2',
      };
    });

    it('should return -1 if first case is not new and second case is new', () => {
      caseB.id = `${TEMPORARY_CASE_ID_PREFIX}10`;
      expect(CaseModeler.sortByExistingCases(caseA, caseB)).toEqual(-1);
    });

    it('should return 1 if first case is new and second case is not new', () => {
      caseA.id = `${TEMPORARY_CASE_ID_PREFIX}10`;
      expect(CaseModeler.sortByExistingCases(caseA, caseB)).toEqual(1);
    });

    it('should return 0 when case names are the same', () => {
      caseA.name = 'SomeName 1';
      caseB.name = 'somename 1';
      expect(CaseModeler.sortByExistingCases(caseA, caseB)).toEqual(0);
    });

    it('should return -1 if first case has a name that should be before second case name', () => {
      caseA.name = 'aaaa 9';
      caseB.name = 'aaaa 10';
      expect(CaseModeler.sortByExistingCases(caseA, caseB)).toEqual(-1);
    });

    it('should return 1 if first case has a name that should be after the second case name', () => {
      caseA.name = 'aaaa 10';
      caseB.name = 'aaaa 9';
      expect(CaseModeler.sortByExistingCases(caseA, caseB)).toEqual(1);
    });
  });

  describe('setCaseIndex()', () => {
    it('should set the caseIndex', () => {
      const cases = cloneDeep(mockCases);
      CaseModeler.setCaseIndex(cases[0], 0);
      expect(cases[0]).toEqual({
        ...mockCases[0],
        caseIndex: 1,
      });
    });
  });

  describe('flagCasesWithEmptyTargets()', () => {
    let cases: CaseModel[];

    beforeEach(() => {
      cases = [{
        ...mockCases[0],
        targets: [{
          id: '1',
          primaryValue: '12345',
        }, {
          id: '2',
          primaryValue: '',
        }, {
          id: '3',
        }],
      }];
    });

    it('should not do anything if case has at least one target empty', () => {
      const clonedCases = cloneDeep(cases);
      expect(CaseModeler.flagCasesWithEmptyTargets(cases)).toEqual([{
        ...clonedCases[0],
      }]);
    });

    it('should set the case to hideFromView and not includeInWarrant if targets are empty', () => {
      cases[0].targets[0].primaryValue = undefined;
      const clonedCases = cloneDeep(cases);
      expect(CaseModeler.flagCasesWithEmptyTargets(cases)).toEqual([{
        ...clonedCases[0],
        hideFromView: true,
        includeInWarrant: false,
      }]);
    });

    it('should set the case back to view and include in warrant if target are not empty anymore', () => {
      cases[0].hideFromView = true;
      cases[0].includeInWarrant = false;

      const clonedCases = cloneDeep(cases);
      expect(CaseModeler.flagCasesWithEmptyTargets(cases)).toEqual([{
        ...clonedCases[0],
        hideFromView: false,
        includeInWarrant: true,
      }]);
    });
  });
});
