import { Ontology } from 'data/supportedFeatures/supportedFeatures.types';
import { TargetModel } from 'data/target/target.types';
import { FormTemplateOption, Template } from 'data/template/template.types';
import React, { FC } from 'react';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import FormPanel from 'components/FormPanel';
import { DeviceModel, DeviceModelMap } from '../device.types';
import DeviceDetail from './device-detail';
import CaseModeler from './case-modeler';
import './devices-form-list.scss';

type Props = {
  devices: DeviceModelMap,
  deviceRefs: { [deviceId: string]: DeviceDetail | null },
  isReady: boolean,
  mode: string,
  warrantConfigInstance: WarrantConfiguration | null
  headers: string[]
  templateUsed: Template | null,
  availableServiceOptions: FormTemplateOption[]
  disabled?: boolean
  onDeleteDevice: (deviceId: string)=> void,
  onDeviceServiceUpdate: (deviceId: string, services: string[]) => void
  onDeviceTargetUpdate: (deviceId: string, target: TargetModel, targetType: string, targetIndex: number) => void
}

const DevicesFormList:FC<Props> = ({
  headers,
  devices,
  deviceRefs,
  isReady,
  mode,
  templateUsed,
  availableServiceOptions,
  warrantConfigInstance,
  onDeleteDevice,
  onDeviceServiceUpdate,
  onDeviceTargetUpdate,
}:Props) => {

  const { targets } = warrantConfigInstance?.configuration.config ?? {};

  const devicesEntries = (devicesMap: DeviceModelMap): DeviceModel[] => {
    return Object.values(devicesMap).sort(sortByExistingDevices);
  };

  const sortByExistingDevices = (deviceA:DeviceModel, deviceB:DeviceModel): number => {
    // Ordering to make existing devices came first them new ones
    if (!CaseModeler.isNewDevice(deviceA) && CaseModeler.isNewDevice(deviceB)) {
      return -1;
    }

    if (CaseModeler.isNewDevice(deviceA) && !CaseModeler.isNewDevice(deviceB)) {
      return 1;
    }

    return 0;
  };

  const renderHeader = (header:string) => {
    const headerLabel = targets?.[header]?.title ?? header;
    return (
      <th className="devices-form-list-header-data" key={header} data-testid={header}>{headerLabel}</th>
    );
  };

  const renderDevicesHeader = () => {
    if (mode === CONFIG_FORM_MODE.View) {
      const [_deleteHeader, ...othersHeaders] = headers;
      return othersHeaders.map(renderHeader);
    }

    return headers.map(renderHeader);
  };

  const renderDevices = () => {
    return devicesEntries(devices).map((deviceEntry) => {
      return (
        <DeviceDetail
          id={deviceEntry.device}
          key={deviceEntry.device}
          ref={(ref) => { deviceRefs[deviceEntry.device] = ref; }}
          disabled={!isReady}
          mode={mode}
          device={devices[deviceEntry.device]}
          headers={headers}
          templateUsed={templateUsed}
          availableServiceOptions={availableServiceOptions}
          warrantConfigInstance={warrantConfigInstance}
          delete={onDeleteDevice}
          onDeviceServiceUpdate={onDeviceServiceUpdate}
          onDeviceTargetUpdate={onDeviceTargetUpdate}
        />
      );
    });
  };

  return (
    <table className="devices-form-list">
      <thead className="devices-form-list-header">
        <tr>
          { renderDevicesHeader() }
        </tr>
      </thead>
      <tbody className="devices-form-list-body">
        { renderDevices() }
      </tbody>
    </table>
  );
};

export default DevicesFormList;
