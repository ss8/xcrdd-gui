import React from 'react';
import { FormTemplateOption, Template } from 'data/template/template.types';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import {
  fireEvent,
  render,
  screen,
} from '@testing-library/react';
import DeviceDetails from './device-detail';
import { DeviceModel, TargetField } from '../device.types';

describe('<DeviceDetails>', () => {
  let device: DeviceModel;
  let headers: string[];
  let availableServiceOptions: FormTemplateOption[];
  let warrantConfigInstance: WarrantConfiguration;
  let template: Template;

  beforeEach(() => {
    device = {
      device: '1',
      caseId: '',
      services: [],
      types: {},
      allTargetAfs: [],
      afMapping: {},
    };

    headers = ['Action', 'Device', 'Services'];

    availableServiceOptions = [
      { label: '5G data', value: '5G-DATA' },
      { label: '4G PD', value: 'LTE-4G-DATA' },
    ];

    warrantConfigInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: jest.fn(),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    template = {
      ...mockTemplates[1],
      template: JSON.parse(mockTemplates[1].template),
    };

    jest.spyOn(warrantConfigInstance, 'computeSelectedAfTypesForDevices').mockImplementation(() => Promise.resolve());
  });

  it('should render the device entry', () => {
    render(
      <table>
        <tbody>
          <DeviceDetails
            id="1"
            key="1"
            device={device}
            headers={headers}
            availableServiceOptions={availableServiceOptions}
            warrantConfigInstance={warrantConfigInstance}
            delete={jest.fn()}
            mode="create"
            templateUsed={template}
            onDeviceServiceUpdate={jest.fn()}
            onDeviceTargetUpdate={jest.fn()}
          />
        </tbody>
      </table>,
    );

    expect(screen.getByTitle('Delete')).toBeVisible();
    expect(screen.getByText('1')).toBeVisible();
    expect(screen.getByPlaceholderText('Search...')).toBeVisible();
  });

  it('should render the targets', () => {
    headers = ['Action', 'Device', 'Services', 'MDN', 'IMSI', 'IMEI'];

    const field: TargetField = {
      title: 'TARGET',
      subFields: {
        primaryValue: {
          label: 'Value',
          options: undefined,
          type: 'autoGrowText',
          validation: {},
        },
      },
    };

    const type = {
      display: true,
      ids: [],
      target: {
        id: '1',
      },
    };

    jest.spyOn(warrantConfigInstance, 'getTargetFields').mockImplementation((_target) => {
      return {
        MDN: field,
        IMSI: field,
        IMEI: field,
      };
    });

    device.types = {
      MDN: [type],
      IMSI: [type],
      IMEI: [type],
    };

    render(
      <table>
        <tbody>
          <DeviceDetails
            id="1"
            key="1"
            device={device}
            headers={headers}
            availableServiceOptions={availableServiceOptions}
            warrantConfigInstance={warrantConfigInstance}
            delete={jest.fn()}
            mode="create"
            templateUsed={template}
            onDeviceServiceUpdate={jest.fn()}
            onDeviceTargetUpdate={jest.fn()}
          />
        </tbody>
      </table>,
    );

    expect(screen.getByTestId('targetDetails-MDN')).toBeVisible();
    expect(screen.getByTestId('targetDetails-IMSI')).toBeVisible();
    expect(screen.getByTestId('targetDetails-IMEI')).toBeVisible();
  });

  it('should call delete when clicking in the delete icon', () => {
    const onDelete = jest.fn();

    render(
      <table>
        <tbody>
          <DeviceDetails
            id="1"
            key="1"
            device={device}
            headers={headers}
            availableServiceOptions={availableServiceOptions}
            warrantConfigInstance={warrantConfigInstance}
            delete={onDelete}
            mode="create"
            templateUsed={template}
            onDeviceServiceUpdate={jest.fn()}
            onDeviceTargetUpdate={jest.fn()}
          />
        </tbody>
      </table>,
    );

    fireEvent.click(screen.getByTitle('Delete'));

    expect(onDelete).toBeCalledWith('1');
  });

  it('should disable the delete icon and the service field if marked as disabled', () => {
    render(
      <table>
        <tbody>
          <DeviceDetails
            id="1"
            key="1"
            device={device}
            headers={headers}
            availableServiceOptions={availableServiceOptions}
            warrantConfigInstance={warrantConfigInstance}
            disabled
            delete={jest.fn()}
            mode="create"
            templateUsed={template}
            onDeviceServiceUpdate={jest.fn()}
            onDeviceTargetUpdate={jest.fn()}
          />
        </tbody>
      </table>,
    );

    expect(screen.getByTitle('Delete')).toBeDisabled();
    expect(screen.getByPlaceholderText('Search...')).toBeDisabled();
  });
});
