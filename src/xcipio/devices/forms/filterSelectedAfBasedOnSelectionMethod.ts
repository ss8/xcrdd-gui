import { TargetDefaultAFSelection, TargetDefaultAFSelectionMethod } from 'data/customization/customization.types';
import { CaseModel } from 'data/case/case.types';
import { TargetModel } from 'data/target/target.types';

export function filterSelectedAfBasedOnSelectionMethod(
  target: TargetModel,
  caseDef:CaseModel,
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
):void {
  if (!targetDefaultAFSelection || !target.afs || !target.primaryType) {
    return;
  }
  const selectionMethods = getAFSelectionMethods(targetDefaultAFSelection, caseDef, target.primaryType);
  if (!selectionMethods) {
    return;
  }

  selectionMethods.forEach((method) => {
    if (!target.afs) return;
    if (method.method === 'selectNone') {
      target.afs.afIds = [];
      target.afs.afGroups = [];
      target.afs.afAutowireTypes = [];
      target.afs.afAutowireGroups = [];
    }
  });
}

export function getAFSelectionMethods(
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  warrantCase: CaseModel,
  targetTag: string | undefined, // target identifier as defined in the ontology file
): Set<TargetDefaultAFSelectionMethod> | undefined {
  if (!targetDefaultAFSelection || !targetTag) {
    return;
  }

  // if case has multiple services and each service has the same target with different selectionMethod,
  // the specified selectionMethod is ignored.
  const afSelectionMethods:Set<TargetDefaultAFSelectionMethod> = new Set();
  let hasNoneSelectionMethod = false;
  warrantCase.services.forEach((service) => {
    if (targetDefaultAFSelection[service] && targetDefaultAFSelection[service][targetTag]) {
      const { selectionMethods } = targetDefaultAFSelection[service][targetTag];
      selectionMethods.forEach((selectionMethod) => {
        hasNoneSelectionMethod = hasNoneSelectionMethod || (selectionMethod.method === 'selectNone');
        afSelectionMethods.add(selectionMethod);
      });
    }
  });

  if (hasNoneSelectionMethod && afSelectionMethods.size > 1) {
    console.warn(`Ignore default AF selection configuration for services ${warrantCase.services.join(',')} and target ${targetTag} because there is conflicting selection methods.`);
    return;
  }

  return afSelectionMethods.size === 0 ? undefined : afSelectionMethods;
}
