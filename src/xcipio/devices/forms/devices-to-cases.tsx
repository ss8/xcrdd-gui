import cloneDeep from 'lodash.clonedeep';
import { CaseModel, CaseModelMap, INTERCEPT_TYPE_CD } from 'data/case/case.types';
import { Ontology } from 'data/supportedFeatures/supportedFeatures.types';
import { Template } from 'data/template/template.types';
import { Warrant } from 'data/warrant/warrant.types';
import {
  CollectionFunction, HI2Interface, HI3Interface, Option,
} from 'data/collectionFunction/collectionFunction.types';
import { CustomizationDerivedTargetMap, TargetDefaultAFSelection } from 'data/customization/customization.types';
import { TargetModel, TargetModelMap } from 'data/target/target.types';
import { fetchCollectionFunctionHI2InterfaceList, fetchCollectionFunctionHI3InterfaceList } from 'data/collectionFunction/collectionFunction.actions';
import CaseModeler from './case-modeler';
import { AccessFunctionsAndCollectionFunctionsByServiceMap, DeviceModelMap } from '../device.types';

export default async function getAllCasesFromDevicesModel(
  warrant: Warrant | null,
  serviceMapping: AccessFunctionsAndCollectionFunctionsByServiceMap,
  customTargetPattern: CustomizationDerivedTargetMap,
  targetDefaultAFSelection: TargetDefaultAFSelection | undefined,
  devices: DeviceModelMap,
  cfPool: CollectionFunction[],
  templateUsed: Template | null,
  selectedDF: string,
  bSelectAllHI3Interfaces: boolean,
  warrantCases: CaseModel[],
  targetsById: TargetModelMap,
  ontology : Ontology,
  shouldAutomaticallyGenerateCaseName: boolean,
): Promise<CaseModel[]> {
  if (!warrant) {
    return [];
  }

  const caseNamePrefix = getCaseNameFromWarrantTemplate(templateUsed);

  const allTargets : TargetModel[] = [];
  warrantCases.forEach(({ targets }) => {
    targets.forEach((target) => {
      if (target.caseId != null) {
        allTargets.push(target);
      } else {
        allTargets.push(targetsById[target.id as string]);
      }
    });
  });

  const caseModeler = new CaseModeler({
    warrant,
    serviceMap: serviceMapping,
    customerTargets: customTargetPattern,
    targetDefaultAFSelection,
    cfPool,
    ontology,
    shouldAutomaticallyGenerateCaseName,
  });
  caseModeler.setConfiguration(cfPool, warrantCases || [], allTargets, caseNamePrefix);
  caseModeler.generateCases(devices);

  const cases = cloneDeep(caseModeler.cases);
  const targets = cloneDeep(caseModeler.targets);
  const targetArray = cloneDeep(Object.keys(targets).map((key: string) => {
    return targets[key];
  }));

  const caseResult = await getAllCaseDetails(
    cases ?? {},
    targetArray,
    selectedDF,
    warrant,
    bSelectAllHI3Interfaces,
  );

  // Flag the cases with empty target to hide them from view and to prevent it to be
  // included in the warrant
  const flaggedCaseResult = CaseModeler.flagCasesWithEmptyTargets(caseResult);

  // Sort the cases and set the caseIndex value to be used in the cases list
  return CaseModeler.sortCases(flaggedCaseResult);
}

/**
 * This function is used by new cases to get default HI3 interfaces
 */
export function getDefaultHI3Interfaces(
  caseType: string | undefined,
  hi2Data: HI2Interface[],
  hi3Data: HI3Interface[],
  casePHIR: boolean,
  bSelectAllHI3Interfaces: boolean,
): HI3Interface[] {
  let hi3Interfaces: HI3Interface[] = [];

  if ((caseType === 'CC' || caseType === 'ALL') && hi3Data && hi3Data.length > 0) {
    // Default select the HI3 interfaces that has Content Processing (aka filter) equals to ALL.
    // If HI3 interfaces do not have Content Processing (aka filter), select the first one
    const hi3HasContentProcessingAttribute = hi3Data[0].options?.some(
      (item: Option) => (item.key === 'filter')
    );
    if (hi3HasContentProcessingAttribute) {
      hi3Data.forEach((hi3Template : HI3Interface) => {
        const hi3IsAllContentProcessing = hi3Template.options?.some(
          (option: Option) => (option.key === 'filter' && option.value === 'ALL')
        );
        if (hi3IsAllContentProcessing) {
          hi3Interfaces.push({ ...hi3Template, operation: 'CREATE' });
        }
      });
    } else {
      hi3Interfaces.push({ ...hi3Data[0], operation: 'CREATE' });
    }
    if (bSelectAllHI3Interfaces === true) {
      hi3Interfaces = hi3Data.map((hi3: HI3Interface) => {
        return {
          ...hi3,
          operation: 'CREATE',
        };
      });
    }
  } else if (caseType === INTERCEPT_TYPE_CD && casePHIR === true && hi2Data && hi2Data.length > 0) {
    hi3Interfaces.push({
      ...hi2Data[0],
      operation: 'CREATE',
    });
  }

  return hi3Interfaces;
}

function getAllCaseDetails(
  cases: CaseModelMap,
  targetArray: TargetModel[],
  selectedDF: string,
  warrant: Warrant,
  bSelectAllHI3Interfaces: boolean,
) {
  return Promise.all(Object.values(cases).map(async (item) => {
    const hi2Data = await updateHI2Interfaces(item.cfId.id, selectedDF);
    const hi3Data = await updateHI3Interfaces(item.cfId.id, selectedDF);

    item.hi2Data = hi2Data;
    item.hi3Data = hi3Data;

    const caseTargetIds = item.targets.map((target) => target.id);
    const caseTargets = targetArray.filter((target) => caseTargetIds.includes(target.id));

    item.targets = caseTargets;

    if (CaseModeler.isNewCase(item)) {
      item.type = warrant.caseType;
      item.templateUsed = warrant.caseOptTemplateUsed;
      item.options = cloneDeep(warrant.caseOptions);

      if (item.options) {
        item.options.configureOptions = true;
      }

      const selectedCf = item.cfs?.find((cf) => { return cf.id === item.cfId.id; });
      const cfType = selectedCf?.tag;
      const casePHIR = item.options?.generatePacketDataHeaderReport === true && (!cfType || cfType === 'CF-3GPP-33108');

      item.hi3Interfaces = getDefaultHI3Interfaces(
        warrant.caseType,
        hi2Data ?? [],
        hi3Data ?? [],
        casePHIR,
        bSelectAllHI3Interfaces,
      );
    }

    return item;
  }));
}

export async function updateHI2Interfaces(cfId: any, selectedDF: string): Promise<HI2Interface[] | undefined> {
  if (cfId && cfId !== '') {
    const values = await fetchCollectionFunctionHI2InterfaceList(selectedDF, cfId).payload;
    const hi2Data = (values !== undefined
          && values.data !== undefined && values.data.data !== undefined)
      ? values.data.data : [];
    return (hi2Data as HI2Interface[]) ?? [];
  }
}

export async function updateHI3Interfaces(cfId: any, selectedDF: string): Promise<HI3Interface[] | undefined> {
  if (cfId && cfId !== '') {
    const values = await fetchCollectionFunctionHI3InterfaceList(selectedDF, cfId).payload;
    const hi3Data = (values !== undefined
          && values.data !== undefined && values.data.data !== undefined)
      ? values.data.data : [];
    return (hi3Data as HI3Interface[]) ?? [];
  }
}

function getCaseNameFromWarrantTemplate(templateUsed: any) {
  return templateUsed.template.case.general.fields.liId.initial ?? '';
}
