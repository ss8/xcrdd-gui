import React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { mockCustomization } from '__test__/mockCustomization';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { Warrant } from 'data/warrant/warrant.types';
import { CaseModel } from 'data/case/case.types';
import { FormTemplateOption, Template } from 'data/template/template.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import { DevicePanel } from '.';
import { DeviceModelMap } from './device.types';

describe('<DevicePanel>', () => {
  let warrantConfigInstance: WarrantConfiguration;
  let warrant: Warrant;
  let cases: CaseModel[];
  let template: Template;

  beforeEach(() => {
    warrant = {
      id: '1',
      templateUsed: '',
      dxOwner: '',
      dxAccess: '',
      name: 'Warrant name',
      city: '',
      comments: '',
      contact: null,
      judge: '',
      receivedDateTime: '',
      region: '',
      startDateTime: '',
      stopDateTime: '2020-11-21T03:00:00.000Z',
      timeZone: 'America/New_York',
      cases: [],
      attachments: [],
      province: '',
      department: '',
      lea: 'Any',
      caseOptTemplateUsed: '5',
    };

    cases = [{
      id: 'DX-CASE-1',
      dxOwner: '',
      dxAccess: null,
      device: '1',
      name: 'Case1',
      warrantId: {
        name: 'aaa',
      },
      liId: 'Case1',
      services: [
        '5G-DATA',
      ],
      cfId: {
        id: 'Q0EtRE9KQk5FLUVUU0k',
        name: 'CA-DOJBNE-ETSI',
      },
      entryDateTime: '2020-11-19T14:19:09.987Z',
      startDateTime: '',
      stopDateTime: '2020-11-21T03:00:00.000Z',
      timeZone: 'America/New_York',
      targets: [{
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'MSISDN',
        primaryValue: '222222222222222',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'DX-TARGET-1',
        isRequired: false,
        caseId: {
          id: 'DX-CASE-1',
          name: 'Case1',
        },
      }, {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMSI',
        primaryValue: '111111111111111',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'DX-TARGET-2',
        isRequired: true,
        caseId: {
          id: 'DX-CASE-1',
          name: 'Case1',
        },
      }, {
        dxOwner: '',
        dxAccess: '',
        name: '',
        afs: {
          afIds: [],
          afGroups: [],
          afAutowireTypes: [
            {
              id: 'ERK_SMF',
              name: 'ERK_SMF',
            },
          ],
          afAutowireGroups: [],
          afccc: [],
        },
        primaryType: 'IMEI',
        primaryValue: '',
        secondaryType: '',
        secondaryValue: '',
        options: [],
        id: 'DX-TARGET-3',
        isRequired: false,
        caseId: {
          id: 'DX-CASE-1',
          name: 'Case1',
        },
      }],
    }];

    warrantConfigInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: jest.fn(),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    template = {
      ...mockTemplates[1],
      template: JSON.parse(mockTemplates[1].template),
    };
  });

  it('should display an empty and disabled device when warrant is not ready', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={null}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.getByTitle('Delete')).toBeDisabled();
    expect(screen.getByPlaceholderText('Search...')).toBeDisabled();
    expect(screen.getByText('Add Device').closest('button')).toBeDisabled();
  });

  it('should display the expected headers when there is no device configured', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={null}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.getByText('Device')).toBeVisible();
    expect(screen.getByText('Services')).toBeVisible();
  });

  it('should display the expected headers when there is a device', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={cases}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.getByText('Device')).toBeVisible();
    expect(screen.getByText('Services')).toBeVisible();
    expect(screen.getByText('MDN')).toBeVisible();
  });

  it('should display the expected data for the device', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={cases}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.getByDisplayValue('111111111111111')).toBeVisible();
    expect(screen.getByDisplayValue('22222222222222')).toBeVisible();
  });

  it('should add a new device when clicking in Add Device', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.queryAllByTitle('Delete')).toHaveLength(1);
    fireEvent.click(screen.getByText('Add Device'));
    expect(screen.queryAllByTitle('Delete')).toHaveLength(2);
  });

  it('should remove the device when clicking in Delete icon', () => {
    render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    fireEvent.click(screen.getByTitle('Delete'));
    expect(screen.queryAllByTitle('Delete')).toHaveLength(0);
  });

  it('should set the service in the service field if it is the only option and also load the targets', async () => {
    warrantConfigInstance = new WarrantConfiguration({
      global: { selectedDF: '1' },
      warrants: { config: mockSupportedFeatures },
      getAllAFsByFilter: (): Promise<{ value: { data: { data: AccessFunction[] }}}> => {
        const accessFunctions: AccessFunction[] = [{
          id: 'ZXJrc21mX3gyTGlzbnIx',
          name: 'Ericsson SMF',
          tag: 'ERK_SMF',
          timeZone: 'America/Tijuana',
          type: 'ERK_SMF',
        }];

        return Promise.resolve({
          value: { data: { data: accessFunctions } },
        });
      },
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    warrantConfigInstance.getServiceDropDownOptions = (): FormTemplateOption[] => ([{
      label: '5G data',
      value: '5G-DATA',
    }]);

    const { rerender } = render(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={null}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    rerender(
      <DevicePanel
        bSelectAllHI3Interfaces={false}
        cases={[]}
        cfGroups={[]}
        cfs={mockCollectionFunctions}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    await waitFor(() => expect(screen.getByText('5G data')).toBeVisible());
    expect(screen.getByTestId('targetDetails-MDN')).toBeVisible();
    expect(screen.getByTestId('targetDetails-IMEI')).toBeVisible();
  });

  it('should not set an error message if the targets are all optional and there is at least one defined', () => {
    const devicePanelRef = React.createRef<DevicePanel>();

    cases.forEach(({ targets }) => {
      targets.forEach((target) => {
        target.isRequired = false;
      });
    });

    render(
      <DevicePanel
        ref={devicePanelRef}
        bSelectAllHI3Interfaces={false}
        cases={cases}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    expect(screen.getByText('NOTE: when all targets are optional, for proper service configuration enter at least 1 target value.')).toBeVisible();

    const errors: string[] = [];
    devicePanelRef.current?.isValid([]);

    expect(errors).toEqual([]);
  });

  it('should show an error message if the targets are all optional and none was defined', () => {
    const devicePanelRef = React.createRef<DevicePanel>();

    cases.forEach(({ targets }) => {
      targets.forEach((target) => {
        target.primaryValue = '';
        target.isRequired = false;
      });
    });

    render(
      <DevicePanel
        ref={devicePanelRef}
        bSelectAllHI3Interfaces={false}
        cases={cases}
        cfGroups={[]}
        cfs={[]}
        derivedTargets={mockCustomization.derivedTargets}
        targetDefaultAFSelection={mockCustomization.targetDefaultAFSelection}
        selectedDF="1"
        templateUsed={template}
        updateCasesState={jest.fn()}
        warrant={warrant}
        warrantConfigInstance={warrantConfigInstance}
        mode={CONFIG_FORM_MODE.Create}
        changeTracker={null}
        targetsById={{}}
        shouldAutomaticallyGenerateCaseName
      />,
    );

    const errors: string[] = [];
    devicePanelRef.current?.isValid(errors);

    expect(errors).toEqual([
      'Missing information. Enter at least 1 target value for device 1.',
    ]);
  });

  describe('hasAtLeastOneTargetDefinedForEachDevice()', () => {
    let devices: DeviceModelMap;
    let errors: string[];

    beforeEach(() => {
      errors = [];

      devices = {
        1: {
          allTargetAfs: [],
          device: '1',
          services: ['5G-DATA'],
          types: {
            IMSI: [{
              display: true,
              ids: [],
              target: {
                id: 'IMSI_1',
                primaryValue: '',
              },
            }],
            MDN: [{
              display: true,
              ids: [],
              target: {
                id: 'MDN_1',
                primaryValue: '',
              },
            }, {
              display: true,
              ids: [],
              target: {
                id: 'MDN_2',
                primaryValue: '12341234',
              },
            }],
          },
        },
        2: {
          allTargetAfs: [],
          device: '2',
          services: ['5G-DATA'],
          types: {
            MDN: [{
              display: true,
              ids: [],
              target: {
                id: 'MDN_1',
                primaryValue: '',
              },
            }],
          },
        },
      };
    });

    it('should return true if there is at least one target defined for single device', () => {
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice({ 1: devices['1'] }, errors)).toBe(true);
      expect(errors).toEqual([]);
    });

    it('should return true if there is at least one target defined in each device', () => {
      devices['2'].types.MDN[0].target.primaryValue = '12345134165';
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice(devices, errors)).toBe(true);
      expect(errors).toEqual([]);
    });

    it('should return true if device list is empty', () => {
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice({}, errors)).toBe(true);
      expect(errors).toEqual([]);
    });

    it('should return true if the services for the device is empty', () => {
      devices['2'].services = [];
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice(devices, errors)).toBe(true);
      expect(errors).toEqual([]);
    });

    it('should return false if none of the targets are defined', () => {
      devices['1'].types.MDN[1].target.primaryValue = '';
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice(devices, errors)).toBe(false);
      expect(errors).toEqual([
        'Missing information. Enter at least 1 target value for device 1.',
      ]);
    });

    it('should return false if there is one device with no target defined', () => {
      expect(DevicePanel.hasAtLeastOneTargetDefinedForEachDevice(devices, errors)).toBe(false);
      expect(errors).toEqual([
        'Missing information. Enter at least 1 target value for device 2.',
      ]);
    });
  });
});
