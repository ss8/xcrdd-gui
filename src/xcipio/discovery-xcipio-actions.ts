import { CombinedState, Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import Http from '../shared/http';
import { getDiscoverySettingLanguage } from './discovery-xcipio-selectors';
import { setLanguage } from '../utils';

const http = Http.getHttp();

// action export constants
export const GET_DX_APP_SETTINGS = 'GET_DX_SETTINGS';
export const GET_DX_APP_SETTINGS_FULFILLED = 'GET_DX_SETTINGS_FULFILLED';
export const PUT_DX_APP_SETTINGS = 'PUT_DX_SETTINGS';

type TypeDispatch = ThunkDispatch<CombinedState<any>, unknown, Action>

function setLanguageSetting(state: any) {
  const environment = getDiscoverySettingLanguage(state);
  setLanguage(environment);
}

export const getDXSettingsAndReturn = () => {
  return async (dispatch: TypeDispatch) => {
    const response = await dispatch({
      type: GET_DX_APP_SETTINGS,
      payload: http.get('/config/app-settings/discoveryXcipio'),
    }) as any;

    const newDiscoveryXcipioSettings: any = {};

    if (response.value.data.data[0].appSettings) {
      response.value.data.data[0].appSettings.forEach((dxSetting: any) => {
        if (newDiscoveryXcipioSettings[dxSetting.name] === undefined) {
          if (dxSetting.type === 'boolean') {
            newDiscoveryXcipioSettings[dxSetting.name] =
              dxSetting.setting === 'true';
          } else {
            // default is text
            newDiscoveryXcipioSettings[dxSetting.name] = dxSetting.setting;
          }
        }
      });
    }

    return newDiscoveryXcipioSettings;
  };
};

export const getDXSettings = () => {
  return async (dispatch: TypeDispatch, getState: () => CombinedState<any>) => {
    await dispatch({
      type: GET_DX_APP_SETTINGS,
      payload: http.get('/config/app-settings/discoveryXcipio'),
    });

    setLanguageSetting(getState());
  };
};

// this function is not called any where right now but it works.
export const putDXSettings = () => {
  const bSelectAllHI3Interfaces = {
    appName: 'discoveryXcipio',
    name: 'warrant_anotherSettings',
    type: 'boolean',
    setting: 'false',
  };
  const requestBody = [bSelectAllHI3Interfaces];
  return ({ type: PUT_DX_APP_SETTINGS, payload: http.put('/config/app-settings/discoveryXcipio', requestBody) });
};
