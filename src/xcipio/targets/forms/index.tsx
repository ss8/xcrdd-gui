import React, { Fragment } from 'react';
import { Label } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import TargetDetails from './target-details';
import { ChangedField } from '../../../shared/form';

export default class TargetsSummary extends React.Component<any, any> {
  targetRefs:any[] = [];

  targetFieldUpdateHandler = (name:any, value: string, key: string, targets: any[]) => {
    const newTargets = targets.slice();
    const newTarget = newTargets.find((target:any) => { return target.primaryType === key; });

    // Track target change
    if (this.props.changeTracker && newTarget) {
      const oldValue = this.getTargetValue(newTarget, name);
      if (value.trim().length === 0) {
        this.props.changeTracker.addDeleted(`case.${this.props.warrantCase.id}.target.${newTarget.id}`, newTarget.primaryType);
        this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}`);
      } else if ((!oldValue) || (oldValue.trim() === '' && value.trim().length > 0)) {
        this.props.changeTracker.addAdded(`case.${this.props.warrantCase.id}.target.${newTarget.id}`);
        this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}`);
      } else {
        this.props.changeTracker.addUpdated(`case.${this.props.warrantCase.id}.target.${newTarget.id}`);
      }
    }

    this.customModifyTargetOutputByType(newTarget, name, value);
    this.props.caseDetailsFieldUpdateHandler({ name: 'targets', value: newTargets });
  }

  getTargetValue = (target: any, name: string) => {
    switch (target.primaryType) {
      case 'MDNwService':
      case 'MDN': {
        if (name === 'options') {
          if (target[name].length > 0) {
            return target[name][0].value;
          }
          return '';
        }
        return target[name];
      }
      default:
        return target[name];
    }
  }

  customModifyTargetOutputByType = (target: any, name: string, value: any) => {
    switch (target.primaryType) {
      case 'MDN': {
        if (name === 'options') {
          target[name] = [{
            key: 'InterceptType',
            value,
          }];
        } else {
          target[name] = value;
        }
        break;
      }

      case 'MDNwService':
        if (name === 'options') {
          target[name] = [{
            key: 'SERVICETYPE',
            value,
          }];
        } else {
          target[name] = value;
        }
        break;

      default: target[name] = value;
    }
  }

  areTargetsValid = (forceTouched = true) => {
    let areTargetValid = true;
    this.targetRefs.forEach((targetRef: any) => {
      if (targetRef) {
        const isTargetValid = targetRef.isTargetValid(forceTouched);
        areTargetValid = areTargetValid && isTargetValid;
      }
    });
    return areTargetValid;
  }

  shouldComponentUpdate = (nextProps: any) => {
    // Target Summary Component, including its complete content, should be updated only
    // when Service or CF is changed. When Service or CF is changed, the target list
    // and target detail should change.
    //
    // The Target Detail has its own target model copy.  When user changes the Target
    // Detail entry field value, the Target Detail's target model is updated via the
    // TargetDetail.targetUpdateHandler callback.  The Target
    // Detail's target model value always matches the Target Detail field value.
    return nextProps.shouldTargetsUpdate;
  }

  render() {
    this.targetRefs = [];
    return (
      <Fragment>
        <Label className="formLabel">
          Targets:
          {this.props.targetFields === undefined ?
            <div />
            : (
              <div>
                {
                  Object.keys(this.props.targetFields).map((key: any, index: number) => {
                    const target = this.props.targets.slice().find((t:any) => {
                      return t.primaryType === key;
                    });
                    if (!target) return (<Fragment />);
                    return (
                      <TargetDetails
                        key={index}
                        ref={(ref:any) => { this.targetRefs.push(ref); }}
                        formId={`${target.primaryType}_${this.props.warrantCase.id}${this.props.warrantCase.cfId ? this.props.warrantCase.cfId : ''}${target.id}`}
                        target={cloneDeep(target)}
                        targetField={cloneDeep(this.props.targetFields[key])}
                        changeTracker={this.props.changeTracker}
                        fieldUpdateHandler={(changedField: ChangedField) => {
                          const {
                            name, value,
                          } = changedField;
                          this.targetFieldUpdateHandler(name, value, key,
                            this.props.targets.slice());
                        }}
                      />
                    );
                  })
                }
              </div>
            )}
        </Label>
      </Fragment>
    );
  }
}
