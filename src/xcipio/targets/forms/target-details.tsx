import React from 'react';
import { IResizeEntry, ResizeSensor } from '@blueprintjs/core';
import './targets.scss';
import DynamicForm, { ChangedField } from '../../../shared/form';

export default class TargetDetails extends React.Component<any, any> {
  targetRef : any

  constructor(props:any) {
    super(props);
    this.state = {
      targetsRowContainerSize: {},
    };
  }

  customModifyTargetInputByType = (target: any) => {
    if (!target) return;
    switch (target.primaryType) {
      case 'MDNwService':
      case 'MDN': {
        const { options } = target;
        if (Array.isArray(options) && options?.length > 0) {
          target.options = options[0].value;
        }
        break;
      }
      default: break;
    }
    return target;
  }

  isTargetValid = (forceTouched = true) => {
    if (this.targetRef) {
      return this.targetRef.isValid(true, forceTouched);
    }
    return true;
  }

  targetUpdateHandler = (changedField: ChangedField) => {
    const { value } = changedField;
    this.setState({ target: { ...this.state.target, primaryValue: value } });
    this.props.fieldUpdateHandler(changedField);
  }

  /**
   * target field needs to know about its parent's size so the entry field can take up the entire
   * width of its parent if the entry could be long
   */
  handleResize = (entries: IResizeEntry[]) => {
    if (entries && entries.length > 0) {
      const targetsRowContainerSize = {
        width: entries[0].contentRect.width,
        height: entries[0].contentRect.height,
      };
      this.setState({ targetsRowContainerSize });
    }
  }

  render(): JSX.Element {
    return (
      <div style={{ display: 'flex' }}>
        <ResizeSensor onResize={this.handleResize}>
          <span className="targetsRowContainer" data-testid="TargetDetails">
            {this.props.targetField.title !== '' ? `${this.props.targetField.title}: ` : ''}
            <DynamicForm
              name={this.props.formId}
              ref={(ref) => { this.targetRef = ref; }}
              fields={this.props.targetField.subFields}
              layout={[Object.keys(this.props.targetField.subFields)]}
              defaults={this.customModifyTargetInputByType(this.props.target)}
              fieldUpdateHandler={this.targetUpdateHandler}
              parentSize={this.state.targetsRowContainerSize}
            />
          </span>
        </ResizeSensor>
      </div>

    );
  }
}
