import { TargetModel, TargetState } from 'data/target/target.types';
import {
  GET_ALL_TARGETS_FULFILLED,
  GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED,
  GET_TARGET_BY_ID_FULFILLED,
  CLEAR_TARGET_STATE,
} from './targets-actions';

const defaultState: TargetState = {
  targets: [],
  target: null,
  byId: {},
};

const moduleReducer = (state = defaultState, action: any): TargetState => {
  let targets: TargetModel[];

  // todo: handle error for _REJECTED
  switch (action.type) {
    case CLEAR_TARGET_STATE:
      return {
        ...state,
        ...defaultState,
      };

    case GET_ALL_TARGETS_FULFILLED:
    case GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED:
      targets = action.payload.data.data;

      return {
        ...state,
        targets: action.payload.data.data,
        byId: {
          ...state.byId,
          ...targets.reduce((previousValue, target) => ({
            ...previousValue,
            [target.id as string]: target,
          }), {}),
        },
      };

    case GET_TARGET_BY_ID_FULFILLED:
      const target: TargetModel = action.payload.data.data;
      return {
        ...state,
        target,
        byId: {
          ...state.byId,
          [target.id as string]: target,
        },
      };

    default:
      return state;
  }
};
export default moduleReducer;
