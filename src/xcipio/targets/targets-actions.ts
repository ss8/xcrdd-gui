import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_TARGETS = 'GET_ALL_TARGETS';
export const GET_TARGET_BY_ID = 'GET_TARGET_BY_ID';
export const UPDATE_TARGET = 'UPDATE_TARGET';
export const PATCH_TARGET = 'PATCH_TARGET';
export const DELETE_TARGET = 'DELETE_TARGET';
export const CREATE_TARGET = 'CREATE_TARGET';
export const GET_ALL_TARGETS_FULFILLED = 'GET_ALL_TARGETS_FULFILLED';
export const GET_TARGET_BY_ID_FULFILLED = 'GET_TARGET_BY_ID_FULFILLED';
export const UPDATE_TARGET_FULFILLED = 'UPDATE_TARGET_FULFILLED';
export const PATCH_TARGET_FULFILLED = 'PATCH_TARGET_FULFILLED';
export const DELETE_TARGET_FULFILLED = 'DELETE_TARGET_FULFILLED';
export const CREATE_TARGET_FULFILLED = 'CREATE_TARGET_FULFILLED';
export const GET_ALL_TARGETS_BY_TARGETIDS = 'GET_ALL_TARGETS_BY_TARGETIDS';
export const GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED = 'GET_ALL_TARGETS_BY_TARGETIDS_FULFILLED';
export const CLEAR_TARGET_STATE = 'CLEAR_TARGET_STATE';

// action creators
export const getAllTargets = () => ({ type: GET_ALL_TARGETS, payload: http.get('/api/surveillance') });
export const getTargetById = (id: number) => ({ type: GET_TARGET_BY_ID, payload: http.get(`/targets/${id}`) });
export const createTarget = (body: any) => ({ type: CREATE_TARGET, payload: http.post('/targets', body) });
export const updateTarget = (id: number, body: any) => ({ type: UPDATE_TARGET, payload: http.put(`/targets/${id}`, body) });
export const patchTarget = (id: number, body: any) => ({ type: PATCH_TARGET, payload: http.patch(`/targets/${id}`, body) });
export const deleteTarget = (id: number) => ({ type: DELETE_TARGET, payload: http.delete(`/targets/${id}`) });
export const getAllTargetsByTargetIds = (dfId: string, wId: string, cId:string, ids: string) =>
  ({ type: GET_ALL_TARGETS_BY_TARGETIDS, payload: http.get(`/warrants/${dfId}/${wId}/cases/${cId}/targets?ids=${ids}`) });

export function clearTargetState() {
  return {
    type: CLEAR_TARGET_STATE,
    payload: null,
  };
}
