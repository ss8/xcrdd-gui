import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllTargets,
  getTargetById,
  createTarget,
  updateTarget,
  patchTarget,
  deleteTarget,
} from './targets-actions';
import ModuleRoutes from './targets-routes';

const targets = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  targets: state.targets,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllTargets,
  getTargetById,
  createTarget,
  updateTarget,
  patchTarget,
  deleteTarget,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(targets));
