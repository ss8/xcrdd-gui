import cloneDeep from 'lodash.clonedeep';
import { HI3Interface, Option } from 'data/collectionFunction/collectionFunction.types';
import {
  interfaceEquals,
  convertToObject,
  interfaceEqualsRowData,
  getHI3RowData,
  getCommonFieldNames,
  interfaceToRowData,
} from './hi2-hi3-interface.utils';

describe('hi2-hi3-inteface.utils', () => {
  let hi3Templates: HI3Interface[];

  beforeEach(() => {
    hi3Templates = [
      {
        id: 'hi3Id1',
        name: 'IP:34.45.56.67 PORT:4545',
        transportType: 'TCP',
        options: [
          { key: 'option1', value: 'v1' },
          { key: 'option2', value: 'v2' },
          { key: 'ifid', value: 'v3' },
        ],
      },
      {
        id: 'hi3Id2',
        name: 'IP:56.34.45.45 PORT:8978',
        transportType: 'FTP',
        options: [
          { key: 'filter', value: 'ALL' },
          { key: 'option1', value: 'v4' },
          { key: 'option2', value: 'v5' },
          { key: 'ifid', value: 'v6' },
        ],
      },
      {
        id: 'hi3Id3',
        name: 'IP:56.34.45.45 PORT:8978',
        transportType: 'FTP',
        options: [
          { key: 'filter', value: 'VO' },
          { key: 'option1', value: 'v4' },
          { key: 'option2', value: 'v5' },
          { key: 'ifid', value: 'v6' },
        ],
      },
      {
        id: 'hi3Id4',
        name: 'IP:56.34.45.45 PORT:8978',
        transportType: 'FTP',
        options: [
          { key: 'option1', value: 'v4' },
          { key: 'option2', value: 'v5' },
          { key: 'ifid', value: 'v6' },
        ],
      },
    ];
  });

  describe('getCommonFieldNames', () => {
    it('should return proper field key names when there is a real Hi3 interface', () => {
      const selectedInterfaces = [
        cloneDeep(hi3Templates[0]),
        cloneDeep(hi3Templates[1]),
      ];
      selectedInterfaces[1].isRealHI3 = true;
      expect(getCommonFieldNames(selectedInterfaces, hi3Templates)).toEqual([
        'option1',
        'option2',
        'ifid',
      ]);
    });
  });

  describe('interfaceToRowData', () => {
    it('should return proper row value if fieldNames is not empty', () => {
      const fieldNames = [
        'option1',
        'option2',
        'ifid',
      ];

      expect(interfaceToRowData(hi3Templates[1], fieldNames)).toEqual({
        id: 'hi3Id2',
        name: 'IP:56.34.45.45 PORT:8978',
        option1: 'v4',
        option2: 'v5',
        ifid: 'v6',
        hiInterface: hi3Templates[1],
      });
    });

    it('should return proper row value if fieldNames is empty', () => {
      expect(interfaceToRowData(hi3Templates[1], [])).toEqual({
        id: 'hi3Id2',
        name: 'IP:56.34.45.45 PORT:8978',
        filter: 'ALL',
        option1: 'v4',
        option2: 'v5',
        ifid: 'v6',
        hiInterface: hi3Templates[1],
      });
    });

  });
  describe('interfaceEquals', () => {
    let fieldNames:string[];
    beforeEach(() => {
      fieldNames = [
        'option1',
        'option2',
        'ifid',
      ];
    });

    it('should return true if interfaces have same name and options', () => {
      expect(interfaceEquals(hi3Templates[0], hi3Templates[0], fieldNames)).toBeTruthy();
    });

    it('should return false if interfaces have different name', () => {
      const interfaceA = cloneDeep(hi3Templates[0]);
      interfaceA.name = 'ABC';
      expect(interfaceEquals(hi3Templates[0], interfaceA, fieldNames)).toBeFalsy();
    });

    it('should return false if interfaces have different options', () => {
      const diffInterface = cloneDeep(hi3Templates[0]) as HI3Interface;
      diffInterface.options = [
        { key: 'option1', value: '4545' },
        { key: 'option2', value: '34.45.56.67' },
        { key: 'ifid', value: 'Y' }, // different
      ];
      expect(interfaceEquals(hi3Templates[0], diffInterface, fieldNames)).toBeFalsy();
    });

    it('should return true even if interfaces have different number of options as long as the value for the supplied fields are the same', () => {
      const diffInterface = cloneDeep(hi3Templates[0]) as HI3Interface;
      diffInterface.options = [
        { key: 'option1', value: 'v1' },
        { key: 'option2', value: 'v2' },
        { key: 'ifid', value: 'v3' },
        { key: 'option4', value: 'v4' },
      ];
      expect(interfaceEquals(hi3Templates[0], diffInterface, fieldNames)).toBeTruthy();
    });

    it('should return false if one interface has no options and the other one has options', () => {
      const diffInterface = cloneDeep(hi3Templates[0]) as HI3Interface;
      diffInterface.options = undefined;
      expect(interfaceEquals(hi3Templates[0], diffInterface, fieldNames)).toBeFalsy();
    });

    it('should return false if one interface has more options than the other interface and there is no real HI3 selected', () => {
      expect(interfaceEquals(hi3Templates[0], hi3Templates[1], [])).toBeFalsy();
    });

    it('should return true if both interfaces have the same number of options and there is no real HI3 selected', () => {
      expect(interfaceEquals(hi3Templates[0], hi3Templates[0], [])).toBeTruthy();
    });

    it('should return true if both interface have the same field values but different ifid', () => {
      const anotherTemplate = cloneDeep(hi3Templates[1]) as HI3Interface;
      anotherTemplate.options = [
        { key: 'filter', value: 'ALL' },
        { key: 'option1', value: 'v4' },
        { key: 'option2', value: 'v5' },
        { key: 'ifid', value: 'diff' },
      ];
      expect(interfaceEquals(hi3Templates[1], anotherTemplate, [])).toBeTruthy();
    });
  });

  describe('interfaceEqualsRowData', () => {
    let rowData:{[key:string]:string | boolean} = {};
    beforeEach(() => {
      rowData = convertToObject(hi3Templates[0].options as Option[]);
      rowData.name = hi3Templates[0].name;
      rowData.id = hi3Templates[0].id;
      rowData.selected = false;
    });

    it('should return true if interface and rowData have same name and options', () => {
      expect(interfaceEqualsRowData(hi3Templates[0], rowData)).toBeTruthy();
    });

    it('should return false if interfaces have different name', () => {
      rowData.name = 'ABC';
      expect(interfaceEqualsRowData(hi3Templates[0], rowData)).toBeFalsy();
    });

    it('should return false if interfaces have different options', () => {
      rowData.option1 = '12334';
      expect(interfaceEqualsRowData(hi3Templates[0], rowData)).toBeFalsy();
    });

    it('should return false if row has more field than options', () => {
      rowData.hello = '122323';
      expect(interfaceEqualsRowData(hi3Templates[0], rowData)).toBeFalsy();
    });

    it('should return false if interfaces options is undefined and rowData have options data', () => {
      hi3Templates[0].options = undefined;
      expect(interfaceEqualsRowData(hi3Templates[0], rowData)).toBeFalsy();
    });

  });

  describe('getHI3RowData', () => {
    it('should return proper row data when there are no templates and no selected interfaces', () => {
      expect(getHI3RowData([], [], 'cfId1', false)).toEqual(expect.arrayContaining([]));
    });
    it('should return proper row data when there are no templates and selected interfaces', () => {
      const selectedInterfaces = hi3Templates;

      const rows = getHI3RowData([], selectedInterfaces, 'cfId1', false);
      expect(rows.length).toEqual(4);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'true', hiInterface: selectedInterfaces[0],
        },
        {
          id: 'hi3Id2', name: 'IP:56.34.45.45 PORT:8978', filter: 'ALL', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
        {
          id: 'hi3Id3', name: 'IP:56.34.45.45 PORT:8978', filter: 'VO', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[2],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[3],
        },
      ]));
    });
    it('should return proper row data when there are only templates and no selected HI3 interfaces', () => {
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, [], 'cfId1', false);
      expect(rows.length).toEqual(4);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'false', hiInterface: interfaceTemplates[0],
        },
        {
          id: 'hi3Id2', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'false', hiInterface: interfaceTemplates[1],
        },
        {
          id: 'hi3Id3', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'false', hiInterface: interfaceTemplates[2],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'false', hiInterface: interfaceTemplates[3],
        },
      ]));
    });
    it('should return proper row data when some templates are selected', () => {
      const selectedInterfaces = [
        hi3Templates[1],
        hi3Templates[3],
      ];
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, selectedInterfaces, 'cfId1', false);
      expect(rows.length).toEqual(3);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'false', hiInterface: interfaceTemplates[0],
        },
        {
          id: 'hi3Id2', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[0],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
      ]));
    });
    it('should return proper row data and column when there are templates and real HI3 interfaces are selected', () => {
      const selectedInterfaces = [
        { ...hi3Templates[0], cfId: 'cfId1' },
        { ...hi3Templates[3], cfId: 'cfId1' },
      ];
      selectedInterfaces[0].isRealHI3 = true;
      selectedInterfaces[1].isRealHI3 = true;
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, selectedInterfaces, 'cfId1', false);
      expect(rows.length).toEqual(2);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'true', hiInterface: selectedInterfaces[0],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
      ]));

    });

    it('should return proper row data when some templates are selected and real HI3 interfaces are selected', () => {
      const selectedInterfaces = [
        { ...hi3Templates[0], cfId: 'cfId1' },
        hi3Templates[3],
      ];
      selectedInterfaces[0].isRealHI3 = true;
      selectedInterfaces[1].isRealHI3 = false;
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, selectedInterfaces, 'cfId1', false);
      expect(rows.length).toEqual(2);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'true', hiInterface: selectedInterfaces[0],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
      ]));
    });

    it('should return proper row data when some selected HI3 interfaces has operation equals to DELETE', () => {
      const selectedInterfaces = [
        { ...hi3Templates[0], cfId: 'cfId1' },
        hi3Templates[3],
      ];
      selectedInterfaces[0].isRealHI3 = true;
      selectedInterfaces[0].operation = 'DELETE';
      selectedInterfaces[1].isRealHI3 = false;
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, selectedInterfaces, 'cfId1', false);
      expect(rows.length).toEqual(2);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id1', name: 'IP:34.45.56.67 PORT:4545', option1: 'v1', option2: 'v2', ifid: 'v3', selected: 'false', hiInterface: selectedInterfaces[0],
        },
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
      ]));
    });

    it('should return proper row data when some selected HI3 interfaces are belonging to different CFs', () => {
      const selectedInterfaces = [
        { ...hi3Templates[0], cfId: 'cfId1' },
        hi3Templates[3],
      ];
      selectedInterfaces[0].isRealHI3 = true;
      selectedInterfaces[0].operation = 'DELETE';
      selectedInterfaces[1].isRealHI3 = false;
      const interfaceTemplates = hi3Templates;
      const rows = getHI3RowData(interfaceTemplates, selectedInterfaces, 'cfId2', false);
      expect(rows.length).toEqual(1);
      expect(rows).toEqual(expect.arrayContaining([
        {
          id: 'hi3Id4', name: 'IP:56.34.45.45 PORT:8978', option1: 'v4', option2: 'v5', ifid: 'v6', selected: 'true', hiInterface: selectedInterfaces[1],
        },
      ]));
    });
  });

});
