import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { AgGridReact } from '@ag-grid-community/react';
import { Label } from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { CaseInterfacesRefs } from 'data/case/case.types';
import { RootState } from 'data/root.reducers';
import { HI2Interface, HI3Interface } from 'data/collectionFunction/collectionFunction.types';
import { WARRANT_PRIVILEGE } from '../../warrants/warrant-enums';
import ConfigFormPopup, { IConfigFormPopupProperties, CONFIG_FORM_MODE } from '../../../shared/popup/config-form-popup';
import { ROWSELECTION } from '../../../shared/commonGrid/grid-enums';
import {
  getHI3RowData,
  getCommonFieldNames,
} from './hi2-hi3-interface.utils';

interface Props {
  isOpen: boolean,
  id: string,
  currentStateMode: CONFIG_FORM_MODE,
  handleClose: () => void,
  cfId: string,
  hi3Interfaces: any[],
  handeSubmit: (hi3Interfaces: any) => void,
  changeTracker?: any,
  hi2Data: HI2Interface[],
  hi3Data: HI3Interface[],
  selectedDF?: string,
  privileges?: string[],
  caseInterfacesRefs?: CaseInterfacesRefs,
}

type State = {
  fieldDirty: boolean
  currentStateMode: CONFIG_FORM_MODE
  cfId: string
  hi3Interfaces: HI3Interface[]
}

class HI2AndHI3Options extends React.Component<Props, State> {
  gridApi: any;

  gridColumnApi: any;

  allHI3Data: any;

  private colDefsEdit: any = [
    {
      headerName: 'Select', headerCheckboxSelection: false, checkboxSelection: true, suppressMenu: true,
    },
    {
      headerName: 'id', field: 'id', hide: true, suppressMenu: true,
    },
    {
      headerName: 'Name', field: 'name', hide: false, suppressMenu: true, resizable: true,
    },
  ];

  private colDefsViewOnly: any = [
    {
      headerName: 'id', field: 'id', hide: true, suppressMenu: true,
    },
    {
      headerName: 'Name', field: 'name', hide: false, suppressMenu: true, resizable: true,
    },
  ];

  constructor(props: Props) {
    super(props);
    this.state = {
      fieldDirty: false,
      currentStateMode: this.props.currentStateMode,
      cfId: this.props.cfId,
      hi3Interfaces: cloneDeep(this.props.hi3Interfaces),
    };
  }

  // TODO: Use a different method
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: Props) {
    if (nextProps.isOpen) {
      this.setState({
        currentStateMode: nextProps.currentStateMode,
        cfId: nextProps.cfId,
        hi3Interfaces: cloneDeep(nextProps.hi3Interfaces),
      });
    }
  }

  shouldComponentUpdate(nextProps: Props) {
    return nextProps.isOpen || this.props.isOpen !== nextProps.isOpen;
  }

  handleSubmit = () => {
    this.props.handeSubmit(this.state.hi3Interfaces);
  }

  hi2GridReady = (event: any) => {
    const { hi2Data } = this.props;
    if (hi2Data.length !== 0) {
      const hi2Columns = this.getColumnsFromData(hi2Data[0], [], this.colDefsViewOnly);
      const hi2RowData = this.getHI2RowData(hi2Data);

      event.api.setColumnDefs(hi2Columns);
      event.api.setRowData(hi2RowData);
    }
  }

  getColumnsFromData = (
    interfaceWithOptions: HI3Interface | HI2Interface,
    selectedInterfaces: HI3Interface[],
    defColumnDefs: any,
  ) => {
    let columnDefs = Object.assign([], defColumnDefs); // eslint-disable-line prefer-const

    // XREST returns different options for the real HI3 and the HI3 template (XC-34473). So if there is a real HI3,
    // use the real HI3 to determine the table column.  Otherwise use the HI3 template.
    let anInterface = interfaceWithOptions;
    const realHI3Interface = selectedInterfaces.find((hi3Interface) => hi3Interface.isRealHI3 === true);
    if (realHI3Interface) {
      anInterface = realHI3Interface;
    }
    const fieldNames = getCommonFieldNames(selectedInterfaces, [interfaceWithOptions]);

    fieldNames.forEach((fieldName: string) => {
      const index = columnDefs.findIndex((e: any) => e.field === fieldName);
      if (index === -1) {
        columnDefs.push(
          {
            headerName: fieldName, field: fieldName, suppressMenu: true, resizable: true,
          },
        );
      }
    });
    return columnDefs;
  }

  getHI2RowData = (interfaceWithOptions: any) => {
    let rowData: any = []; // eslint-disable-line prefer-const
    interfaceWithOptions.forEach((item: any) => {
      const newRow: any = [];
      newRow.id = item.id;
      newRow.name = item.name;

      const { options } = item;
      options.forEach((keyPair: any) => {
        newRow[keyPair.key] = keyPair.value;
      });

      rowData.push(newRow);
    });
    return rowData;
  }

  drawHI3Options = (mode: any) => {
    const showPHIR = this.props.caseInterfacesRefs?.dataOnly && this.props.caseInterfacesRefs?.casePHIR;
    const hi3Data = showPHIR === true ? this.props.hi2Data : this.props.hi3Data;
    if (hi3Data !== undefined && hi3Data.length !== 0) {
      const { hi3Interfaces } = this.state; // this is the selected HI3 in the case.
      const hi3Columns = this.getColumnsFromData(hi3Data[0], hi3Interfaces,
        (mode === CONFIG_FORM_MODE.Edit || mode === CONFIG_FORM_MODE.Create)
          ? this.colDefsEdit : this.colDefsViewOnly);
      const hi3RowData = getHI3RowData(hi3Data, hi3Interfaces, this.state.cfId, mode === CONFIG_FORM_MODE.View);
      this.gridApi.setColumnDefs(hi3Columns);
      this.gridApi.setRowData(hi3RowData);
      this.gridColumnApi.autoSizeAllColumns();
      // TODO : Determine if both redraw and refresh are needed?
      this.gridApi.redrawRows();
      if (mode === CONFIG_FORM_MODE.Edit || mode === CONFIG_FORM_MODE.Create) {
        this.gridApi.forEachNode((node: any) => {
          if (node.data.selected === 'true') {
            node.setSelected(true);
          }
        });
      }
      this.gridApi.refreshCells({ force: true });
    }
  }

  hi3GridReady = (params: any) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.drawHI3Options(this.state.currentStateMode);
  }

  handleStateModeChanged = (stateMode: CONFIG_FORM_MODE) => {
    this.setState({ currentStateMode: stateMode });
    if (stateMode === CONFIG_FORM_MODE.Edit || stateMode === CONFIG_FORM_MODE.Create) {
      this.drawHI3Options(stateMode);
    }
  }

  getTitle = (currentStateMode: CONFIG_FORM_MODE) => {
    return (
      currentStateMode === CONFIG_FORM_MODE.Edit || currentStateMode === CONFIG_FORM_MODE.Create)
      ? 'Edit Case Interfaces' : 'View Case Interfaces';
  }

  rowSelected = (e: any) => {
    const rowData:{[key:string]:string | HI2Interface | HI3Interface } = e.node.data;
    const rowHIInterface = rowData.hiInterface as HI3Interface;
    if (e.node.isSelected()) {
      const { hi3Interfaces } = this.state;
      let addedHI3Interface = false;
      if (!hi3Interfaces.find((item: any) => item.id === rowHIInterface.id)) {
        const newObj: HI3Interface = rowData.hiInterface as HI3Interface;
        newObj.operation = 'CREATE';
        hi3Interfaces.push(newObj);
        addedHI3Interface = true;
      } else {
        hi3Interfaces.forEach((item: any) => {
          if (item.id === rowHIInterface.id && item.operation === 'DELETE') {
            item.operation = 'NO_OPERATION';
          }
        });
      }
      this.setState({ hi3Interfaces });
      const value = `IP: ${rowData.destIPAddr} PORT: ${rowData.destPort} IFID: ${rowData.ifid}`;
      if (this.props.changeTracker && addedHI3Interface) {
        this.props.changeTracker.addAdded(`case.${this.props.id}.HI3Interfaces.${rowData.ifid}`, value, 'Communication Content Interfaces');
      }
    } else {
      const { hi3Interfaces } = this.state;
      const aInterfaceIndex = hi3Interfaces?.findIndex((item: any) => item.id === rowHIInterface.id);
      if (aInterfaceIndex >= 0 && hi3Interfaces[aInterfaceIndex].isRealHI3) {
        hi3Interfaces[aInterfaceIndex].operation = 'DELETE';
      } else {
        hi3Interfaces.splice(aInterfaceIndex, 1);
      }

      this.setState({ hi3Interfaces });
      const value = `IP: ${rowData.destIPAddr} PORT: ${rowData.destPort} IFID: ${rowData.ifid}`;
      if (this.props.changeTracker) {
        this.props.changeTracker.addDeleted(`case.${this.props.id}.HI3Interfaces.${rowData.ifid}`, value);
      }
    }
    if (this.state.currentStateMode === CONFIG_FORM_MODE.Edit
      || this.state.currentStateMode === CONFIG_FORM_MODE.Create) {
      this.setState({ fieldDirty: true });
    }
  }

  render() {
    const { privileges } = this.props;
    const hasPrivilegesToEdit =
      privileges !== undefined && privileges.includes(WARRANT_PRIVILEGE.Edit);
    const configFormPopupProperties: IConfigFormPopupProperties = {
      currentStateMode: this.state.currentStateMode,
      hasPrivilegesToEdit,
      isFirstChildLeftOfEditButton: false,
      fieldDirty: this.state.fieldDirty,
      onClose: this.props.handleClose,
      onSubmit: this.handleSubmit,
      onStateModeChanged: this.handleStateModeChanged,
      getTitle: this.getTitle,
      isOpen: this.props.isOpen,
      width: '80%',
      height: 'auto',
      doNotDisplayEdit: true,
    };
    const showPHIR = this.props.caseInterfacesRefs?.dataOnly && this.props.caseInterfacesRefs?.casePHIR;
    const HI3InterfaceLabel: string = showPHIR ? 'Packet Header' : 'Communication Content';
    return (
      <Fragment>
        <ConfigFormPopup
          {...configFormPopupProperties}
        >
          <Label>Intercept Related Information Interfaces</Label>
          <div
            className="ag-theme-balham"
            style={{
              width: '100%',
              overflowY: 'auto',
            }}
          >
            <AgGridReact
              onGridReady={this.hi2GridReady}
              enableSorting={false}
              enableFilter={false}
              columnDefs={this.colDefsViewOnly}
              domLayout="autoHeight"
              rowData={[]}
              suppressCellSelection
            />
          </div>
          { this.props.caseInterfacesRefs?.dataOnly === false || showPHIR === true ? (
            <Fragment>
              <Label style={{ paddingTop: '30px' }}>{ HI3InterfaceLabel } Interfaces</Label>
              <div
                className="ag-theme-balham"
                style={{
                  width: '100%',
                  overflowY: 'auto',
                }}
              >
                <AgGridReact
                  onGridReady={this.hi3GridReady}
                  columnDefs={this.colDefsViewOnly}
                  domLayout="autoHeight"
                  rowData={[]}
                  suppressCellSelection
                  context={this}
                  onRowSelected={this.rowSelected}
                  rowSelection={ROWSELECTION.MULTIPLE}
                  deltaRowDataMode={false}
                  skipHeaderOnAutoSize
                  suppressColumnVirtualisation
                />
              </div>
            </Fragment>
          ) : <Fragment> </Fragment> }
        </ConfigFormPopup>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  global: { selectedDF },
  authentication: { privileges },
  warrants: { changeTracker },
}: RootState) => ({
  selectedDF,
  privileges,
  changeTracker,
});

export default connect(mapStateToProps, null)(HI2AndHI3Options);
