import { HI2Interface, HI3Interface, Option } from 'data/collectionFunction/collectionFunction.types';

export function interfaceToRowData(
  anInterface: HI2Interface | HI3Interface,
  fieldNames: string[],
):{[key:string]:string | HI2Interface | HI3Interface} {
  const newRow: {[key:string]:string | HI2Interface | HI3Interface} = {};
  newRow.id = anInterface.id;
  newRow.name = anInterface.name;

  const anInterfaceOptions = convertToObject(anInterface.options);
  if (fieldNames.length > 0) {
    fieldNames.forEach((fieldName) => {
      if (anInterfaceOptions[fieldName]) {
        newRow[fieldName] = anInterfaceOptions[fieldName];
      } else {
        newRow[fieldName] = '';
      }
    });
  } else {
    Object.entries(anInterfaceOptions).forEach(([key, value]) => {
      newRow[key] = value;
    });
  }

  // keeping a copy of the HI interface in the row data is because we do not
  // need to construct or search for the interface instance when it is time to get the interface that
  // the row represents.
  newRow.hiInterface = anInterface;
  return newRow;
}

/**
 * Compare only the fields in the row with the interface.
 * There could be more fields in the interface than there are fields in the row
 * because of (XC-34473)
 * @param aInterface
 * @param rowData
 * @returns
 */
export function interfaceEqualsRowData(
  aInterface: HI2Interface | HI3Interface,
  rowData: {[key:string]:string | boolean | HI3Interface | HI2Interface},
): boolean {

  if ((!aInterface && rowData) || (aInterface && !rowData)) {
    return false;
  }

  if (aInterface && rowData) {
    if (aInterface.name !== rowData.name) {
      return false;
    }

    const aInterfaceOptions = convertToObject(aInterface.options);

    const hasDifferentOptions = Object.entries(rowData).some(([key, value]) => {
      if (key !== 'selected' && key !== 'id' && key !== 'name' && key !== 'ifid' && key !== 'state' && key !== 'isRealHI3') {
        if (aInterfaceOptions[key] !== value) {
          return true;
        }
      }
      return false;
    });

    return !hasDifferentOptions ?? false;
  }

  // both are undefined or null
  return true;
}

export function convertToObject(options: Option[] | undefined): {[key:string]:string} {
  if (!options) {
    return {};
  }
  const aObject: {[key:string]:string} = {};
  options.forEach(({ key, value }) => { aObject[key] = value; });
  return aObject;
}

/**
 * Compare name and options in the two interfaces.  If realHI3OptionsKeys is not an empty
 * string, compare the options that match the key names in realHI3OptionsKeys.
 * If realHI3OptionsKeys is an empty string, compare all option fields.
 */
export function interfaceEquals(
  interfaceA: HI2Interface | HI3Interface,
  interfaceB: HI2Interface | HI3Interface,
  commonOptionFieldNames: string[],
): boolean {
  if ((!interfaceA && interfaceB) || (interfaceA && !interfaceB)) {
    return false;
  }
  if (interfaceA && interfaceB) {
    if (interfaceA.name !== interfaceB.name) {
      return false;
    }

    if ((!interfaceA.options && interfaceB.options) ||
      (interfaceA.options && !interfaceB.options)) {
      return false;
    }

    // If there is no real HI3 interface selected, the templates are expected to have the same options length
    if (commonOptionFieldNames.length === 0 && interfaceA.options?.length !== interfaceB.options?.length) {
      return false;
    }

    if (interfaceA.options && interfaceB.options) {
      const interfaceBOptions = convertToObject(interfaceB.options ?? []);
      const interfaceAOptions = convertToObject(interfaceA.options ?? []);

      const hasDifferentOptions = commonOptionFieldNames.some((fieldName) => {
        // do not compare ifid and state because the real HI3 interface will
        // have different ifid and state than the template
        if (fieldName === 'ifid' || fieldName === 'state') {
          return false;
        }
        return (interfaceBOptions[fieldName] !== interfaceAOptions[fieldName]);
      });

      return !hasDifferentOptions ?? false;
    }

    // both are undefined or null
    return true;
  }
  return true;
}

/**
 * @param hi3Interfaces
 * @param findInterface
 * @param commonFieldNames only compare field that have these names
 * @returns true if findInterface is in hi3Interfaces
 */
function interfaceIn(
  hi3Interfaces: HI3Interface[],
  findInterface: HI2Interface | HI3Interface,
  commonFieldNames: string[],
): boolean {
  const found = hi3Interfaces.find((aInterface) => {
    return interfaceEquals(aInterface, findInterface, commonFieldNames);
  });

  return !!found;
}

/**
 * @return if there is real HI3 and HI3 template, return the common field names. If only real HI3 exists,
 * return the field names of the real HI3.  If only HI3 template exists, return the field names of the template.
 */
export function getCommonFieldNames(hi3Interfaces: HI3Interface[], hi3Templates:HI3Interface[]):string[] {
  const realHI3Interface = hi3Interfaces.find((hi3Interface) => hi3Interface.isRealHI3 === true);
  const hi3template = hi3Templates && hi3Templates.length > 0 ? hi3Templates[0] : { options: [] };

  const realHI3InterfaceFieldNames: string[] = [];
  realHI3Interface?.options?.forEach(({ key, value }) => realHI3InterfaceFieldNames.push(key));

  const hi3TemplateFieldNames: string[] = [];
  hi3template?.options?.forEach(({ key, value }) => hi3TemplateFieldNames.push(key));

  let commonFieldNames:string[];
  if (!realHI3Interface) {
    commonFieldNames = hi3TemplateFieldNames;
  } else if (!hi3template) {
    commonFieldNames = realHI3InterfaceFieldNames;
  } else {
    commonFieldNames = [];
    hi3TemplateFieldNames.forEach((fieldName) => {
      if (realHI3InterfaceFieldNames.includes(fieldName)) {
        commonFieldNames.push(fieldName);
      }
    });
  }

  // make filter the first item so it appears on the first column in the HI3 table.
  const filterFieldIndex = commonFieldNames.findIndex((fieldName) => fieldName === 'filter');
  if (filterFieldIndex > 0) {
    commonFieldNames.splice(filterFieldIndex, 1);
    commonFieldNames.unshift('filter');
  }

  return commonFieldNames;
}

/**
   *
   * Display the records of HI3 and HI3-template
   * Note: From customer perspective, the unique IP-Port is one destination,
   * --the table won't display the duplicated IP-Port record
   * Side Note:
   * Case API returns real HI3
   * CF API return HI3 templates
   * @param interfaceWithOptions
   * @param showSelected
   */
export function getHI3RowData(
  interfaceTemplates: HI2Interface[] | HI3Interface[],
  selectedHI3Interfaces: HI3Interface[], // this is an array of selected hi3Interfaces. This is the actual Hi3
  cfId: string, // currently selected CF
  viewMode: boolean,
): {[key:string]:string | HI3Interface | HI2Interface}[] {
  const rowData: {[key:string]:string | HI3Interface | HI2Interface}[] = [];

  // XREST does not return the same number of fields between the real HI3 and
  // the HI3 templates (XC-34473).  So we cannot accurately determine if a real HI3
  // matches any HI3 templates.  The workaround is to only show and compare the option fields
  // available in real HI3.
  const commonFieldNames = getCommonFieldNames(selectedHI3Interfaces, interfaceTemplates);
  // First create rows using the selected HI3 interface
  selectedHI3Interfaces.forEach((selectedInterface) => {
    // Do not show interface if it is not associated to the selected CF
    // This happens when user changes the CF of an existing Case that has HI3 interfaces.
    if (selectedInterface.cfId && selectedInterface.cfId !== cfId) {
      return;
    }
    // Do not show interface if this is in view mode and interface operation is DELETE
    if (viewMode === true && selectedInterface.operation === 'DELETE') {
      return;
    }

    const newRow: {[key:string]:string | HI3Interface | HI2Interface} =
      interfaceToRowData(selectedInterface, commonFieldNames);
    if (selectedInterface.operation !== 'DELETE') {
      newRow.selected = 'true';
    } else {
      newRow.selected = 'false';
    }
    rowData.push(newRow);
  });

  // Now add in the templates that do not match the items in the selected HI3 interface array.
  interfaceTemplates.forEach((interfaceTemplate) => {
    if (interfaceIn(selectedHI3Interfaces, interfaceTemplate, commonFieldNames) === false) {
      const newRow: {[key:string]:string | HI3Interface | HI2Interface} =
        interfaceToRowData(interfaceTemplate, commonFieldNames);
      newRow.selected = 'false';
      // show only the selected row in view mode
      if (viewMode !== true) {
        rowData.push(newRow);
      }
    }
  });
  return rowData;
}
