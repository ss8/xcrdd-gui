import cloneDeep from 'lodash.clonedeep';
import {
  SupportedFeatures,
  SupportedFeaturesAccessFunctions,
  SupportedFeaturesTarget,
  SupportedFeaturesTargetItem,
  SupportedFeaturesTargets,
  SupportedFeaturesX3TargetAssociations,
  AFServiceSpecs,
} from 'data/supportedFeatures/supportedFeatures.types';
import {
  getDeploymentServices,
  getAFSupportedServices,
  getAFOptionalTargets,
  getAFRequiredTargets,
} from 'data/supportedFeatures/supportedFeatures.utils';
import {
  AccessFunctionsAndCollectionFunctionsByServiceMap,
  DeviceModelType,
  TargetField,
  TargetFieldMap,
} from 'xcipio/devices/device.types';
import { TargetModel, TargetModelMap } from 'data/target/target.types';
import { AccessFunction, AccessFunctionMap } from 'data/accessFunction/accessFunction.types';
import { FormTemplateField, FormTemplateOption } from 'data/template/template.types';
import { CustomizationDerivedTargetMap, CustomizationDerivedTargetService } from 'data/customization/customization.types';
import targetModel from '../targets/target-model.json';

const DR_VIRTUAL = 'dr';
export const UNSPECIFIED_SERVICE = {
  label: 'Unspecified',
  value: 'unspecified',
};

export const defaultOption = {
  label: 'Select',
  value: '',
};

const newTargetModel: TargetModel = { ...targetModel };

export type WarrantConfigurationProps = {
  warrants: {
    config: SupportedFeatures
  }
  fetchCollectionFunctionListByFilter: (deliveryFunctionId: string, filter: string) => Promise<any>
  getAllAFsByFilter: (deliveryFunctionId: string, filter: string) => Promise<any>
  global: {
    selectedDF: string
  }
  discoveryXcipio: {
    discoveryXcipioSettings: {
       deployment: string
    }
  }
}

export default class WarrantConfiguration {
  configuration: SupportedFeatures

  deploymentServices: string[] = [];

  featureTagList: string[] = []

  temporaryEnabledCfTypeList: string[] = []

  enabledAfTypeList: string[] = []

  enabledCfTypeList: string[] = []

  enabledServicesList: string[] = []

  allowedCfTypes: string[] = []

  allowedServices: string[] = []

  selectedAfTypesList: string[] = []

  selectedAfs: string[] = []

  selectedServices: string[] = []

  possibleTargets: TargetModel[] = []

  afsByTypeMap: AccessFunctionMap = {}

  afcfByServiceMap: AccessFunctionsAndCollectionFunctionsByServiceMap = {}

  props: WarrantConfigurationProps

  drVirtual = true

  targetsWithAllAfs: TargetModel[] = [];

  mode = '';

  genccc = false;

  constructor(props: WarrantConfigurationProps) {
    this.props = props;
    this.configuration = props.warrants.config;
    this.checkDrVirtual();
    this.computeFeatureList();
    this.computeEnabledCfTypeList();
    this.computeEnabledAfTypeList();
    this.computeEnabledServicesList();
  }

  getConfiguration(): SupportedFeatures {
    return this.configuration;
  }

  getFeatureList(): string[] {
    return this.featureTagList;
  }

  getTargetWithAllAfs(): TargetModel[] {
    return this.targetsWithAllAfs;
  }

  getDrVirtual(): boolean {
    return this.drVirtual;
  }

  checkDrVirtual(): void {
    const { nodes } = this.configuration?.features ?? {};
    this.drVirtual = nodes?.some((node) => {
      return node.features.some((feature: string) => {
        return feature.toLowerCase() === DR_VIRTUAL;
      });
    });
  }

  computeFeatureList(): void {
    const featureTagList:Set<string> = new Set();
    const { nodes } = this.configuration?.features ?? {};
    nodes?.forEach((node) => {
      node.features.forEach((feature: string) => {
        featureTagList.add(feature);
      });
    });
    this.featureTagList = Array.from(featureTagList);
  }

  getTemporaryEnabledCfTypeList(): string[] {
    return this.temporaryEnabledCfTypeList;
  }

  getEnabledCfTypeList(): string[] {
    return this.enabledCfTypeList;
  }

  getEnabledAfTypeList(): string[] {
    return this.enabledAfTypeList;
  }

  computeEnabledAfTypeList(): void {
    const enabledAfTypeList:Set<string> = new Set();
    const afs = this.configuration.config.accessFunctions;
    Object.keys(afs).forEach((key) => {
      const isFeatureIncluded = afs[key].features.some((feature: string) => {
        return this.featureTagList.includes(feature);
      });
      if (isFeatureIncluded) {
        enabledAfTypeList.add(key);
      }
    });
    this.enabledAfTypeList = Array.from(enabledAfTypeList);
  }

  computeEnabledCfTypeList(): void {
    const enabledCfTypeList:Set<string> = new Set();
    const cfs = this.configuration.config.collectionFunctions;
    Object.keys(cfs).forEach((key) => {
      const isFeatureIncluded = cfs[key].features.some((feature: string) => {
        return this.featureTagList.includes(feature);
      });
      if (isFeatureIncluded) {
        enabledCfTypeList.add(key);
      }
    });
    this.enabledCfTypeList = Array.from(enabledCfTypeList);
  }

  getAllowedServiceList(): string[] {
    return this.allowedServices;
  }

  computeGencccEnabledForTargetAfs(targets: TargetModel[]): void {
    const afs = this.configuration.config.accessFunctions;
    this.genccc = targets.some((target) => {
      return target.afs?.afIds.some((af) => {
        return (af.tag && afs[af.tag] && afs[af.tag].genccc);
      });
    });
  }

  static getAccessFunctionsWithTargets(
    accessFunctionMap?: SupportedFeaturesAccessFunctions,
  ): SupportedFeaturesAccessFunctions {
    if (accessFunctionMap == null) {
      return {};
    }

    const filteredAccessFunctions = Object.entries(accessFunctionMap)
      .filter(([_accessFunctionName, accessFunction]) => {
        const optionalTargets = getAFOptionalTargets(accessFunction, '*');
        const requiredTargets = getAFRequiredTargets(accessFunction, '*');
        return (requiredTargets && requiredTargets.length > 0) || (optionalTargets && optionalTargets.length > 0);
      });

    return filteredAccessFunctions.reduce((previousValue, [accessFunctionName, accessFunction]) => ({
      ...previousValue,
      [accessFunctionName]: accessFunction,
    }), {});
  }

  computeEnabledServicesList(): void {
    const enabledServicesList: Set<string> = new Set();

    const {
      accessFunctions,
      collectionFunctions,
    } = this.configuration.config;

    const { deployment } = this.props.discoveryXcipio.discoveryXcipioSettings;
    this.deploymentServices = getDeploymentServices(this.configuration.config, deployment);

    this.afcfByServiceMap = {};

    // Get access function that have a target to avoid creating unnecessary cases
    const filteredAccessFunctions = WarrantConfiguration.getAccessFunctionsWithTargets(accessFunctions);

    Object.keys(filteredAccessFunctions).forEach((afkey) => {
      if (this.enabledAfTypeList.includes(afkey)) {
        // when AF ontology json definition has collectionFunctions at its first level.
        if (filteredAccessFunctions[afkey].collectionFunctions) {
          filteredAccessFunctions[afkey].collectionFunctions?.forEach((cfkey) => {
            if (this.enabledCfTypeList.includes(cfkey)) {
              collectionFunctions[cfkey].services.forEach((cfService: string) => {
                const afSupportedServices = getAFSupportedServices(filteredAccessFunctions[afkey],
                  this.deploymentServices);
                if (afSupportedServices.includes(cfService)) {
                  enabledServicesList.add(cfService);
                  this.computeAfCfMappingByService(afkey, cfkey, cfService);
                }
              });
            }
          });
        } else {
          // when AF ontology json definition has collectionFunctions in the services section
          const afSupportedServices = getAFSupportedServices(filteredAccessFunctions[afkey],
            this.deploymentServices);
          afSupportedServices.forEach((afService) => {
            const afServiceSpecs = (filteredAccessFunctions[afkey].services as AFServiceSpecs);
            if (afServiceSpecs[afService].collectionFunctions) {
              afServiceSpecs[afService].collectionFunctions?.forEach((cfkey) => {
                if (this.enabledCfTypeList.includes(cfkey)) {
                  if (collectionFunctions[cfkey].services.includes(afService)) {
                    enabledServicesList.add(afService);
                    this.computeAfCfMappingByService(afkey, cfkey, afService);
                  }
                }
              });
            } else {
              console.warn(`[warrant-config.computeEnabledServicesList] AF ${afkey} does not have CF defined in ontology file.`);
            }
          });
        }

      }
    });

    this.enabledServicesList = Array.from(enabledServicesList);
    this.allowedServices = this.enabledServicesList.slice();
  }

  async computeEnabledServicesForDevicePanel(cfTags: string[]): Promise<void> {
    // these are all the services supporting the cfTags
    const servicesByCfTags : string[] = [];

    // these are all the AF Tag supporting that support the same services as the cfTags
    const afsForServicesByCfTags:Set<string> = new Set();

    Object.keys(this.afcfByServiceMap).forEach((key) => {
      const isCfTagIncluded =
        Object.keys(this.afcfByServiceMap[key].collectionFunctions).some((cfTag:string) => {
          return cfTags.includes(cfTag);
        });
      if (isCfTagIncluded) {
        servicesByCfTags.push(key);
        this.afcfByServiceMap[key].accessFunctions.forEach((af:string) =>
          afsForServicesByCfTags.add(af));
      }
    });
    // Keep only the services in servicesByCfTags that are supported by AFs in the system.
    await this.filterServicesBasedUponAvailableAfs(servicesByCfTags,
      Array.from(afsForServicesByCfTags));
    this.allowedServices = servicesByCfTags;
  }

  /*
  Filter services that dont have afs on the server
  services: List of services
  afs: Accumulated afs for all the above services
  */
  async filterServicesBasedUponAvailableAfs(services: string[], afs: string[]): Promise<void> {
    const promise = this.props.getAllAFsByFilter(this.props.global.selectedDF,
      WarrantConfiguration.convertToSQLquery('tag', afs));
    const res = await promise;
    this.createAfsByTypeMap(res.value.data.data);
    for (let i = services.length - 1; i >= 0; i -= 1) {
      const isValidService = this.afcfByServiceMap[services[i]].accessFunctions
        .some((af:string) => {
          // eslint-disable-next-line no-unneeded-ternary
          return this.afsByTypeMap[af] ? true : false;
        });
      if (!isValidService) {
        services.splice(i, 1);
      }
    }
  }

  computeAfCfMappingByService(af: string, cf: string, service: string): void {
    if (this.afcfByServiceMap[service]) {
      if (this.afcfByServiceMap[service].collectionFunctions[cf]) {
        const afs = this.afcfByServiceMap[service].collectionFunctions[cf].accessFunctions;
        if (!afs.includes(af)) {
          afs.push(af);
        }
      } else {
        this.afcfByServiceMap[service].collectionFunctions[cf] = {
          accessFunctions: [af],
        };
      }
      const afs = this.afcfByServiceMap[service].accessFunctions;
      if (!afs.includes(af)) {
        afs.push(af);
      }
    } else {
      this.afcfByServiceMap[service] = {
        collectionFunctions: {
          [cf]: {
            accessFunctions: [af],
          },
        },
        accessFunctions: [af],
      };
    }
  }

  getServiceDropDownOptions(): FormTemplateOption[] {
    const servicesDropdownOptions: FormTemplateOption[] = [];
    const { services } = this.configuration.config;
    Object.keys(services).forEach((key) => {
      if (this.allowedServices.includes(key)) {
        const option = {
          label: services[key].title,
          value: key,
        };
        servicesDropdownOptions.push(option);
      }
    });
    if (WarrantConfiguration.hasUnsupportedServices(this.selectedServices)) {
      servicesDropdownOptions.unshift(UNSPECIFIED_SERVICE);
    }
    return servicesDropdownOptions;
  }

  static hasUnsupportedServices(services: string[]): boolean {
    return services.length === 1 && services[0] === UNSPECIFIED_SERVICE.value;
  }

  async getCfDropDownOptions(setCfs: (data: any) => void, cfId: string): Promise<FormTemplateOption[]> {
    let cfDropdownOptions: FormTemplateOption[] = [];
    if (this.allowedCfTypes.length === 0) {
      // If there are no allowed CFTypes and the cfId is passed from the calling method, get that CF details
      if (cfId) {
        const promise = this.props.fetchCollectionFunctionListByFilter(this.props.global.selectedDF, WarrantConfiguration.convertToSQLquery('id', [cfId]));
        const res = await promise;
        cfDropdownOptions = res.value.data.data.map((cf: any) => {
          return {
            label: cf.name,
            value: cf.id,
          };
        });
      } else {
        cfDropdownOptions.push(defaultOption);
      }
      return cfDropdownOptions;
    }
    const promise = this.props.fetchCollectionFunctionListByFilter(this.props.global.selectedDF, WarrantConfiguration.convertToSQLquery('tag', this.allowedCfTypes));
    const res = await promise;
    setCfs(res.value.data.data);
    cfDropdownOptions = res.value.data.data.map((cf: any) => {
      return {
        label: cf.name,
        value: cf.id,
      };
    });
    if (cfDropdownOptions.length === 1) { return cfDropdownOptions; }
    cfDropdownOptions.unshift(defaultOption);
    return cfDropdownOptions;
  }

  static convertToSQLquery(field: string, values: string[]): string {
    return values.map((value: string) => {
      return `${field} eq '${value}'`;
    }).join(' or ');
  }

  getAllowedCfTypes(): string[] {
    return this.allowedCfTypes;
  }

  computeAllowedCfsAndServicesOnServiceChange(selectedServices: string[]): void {
    this.selectedServices = selectedServices;
    if (selectedServices.length === 0) {
      this.allowedCfTypes = [];
      this.allowedServices = this.enabledServicesList.slice();
      return;
    }
    const allowedCfTypes:Set<string> = new Set();
    const allowedServices:Set<string> = new Set();
    this.selectedServices.forEach((service:string) => {
      if (service === 'unspecified') return;
      Object.keys(this.afcfByServiceMap[service].collectionFunctions).forEach((cfType:string) => {
        allowedCfTypes.add(cfType);
      });
    });
    Object.keys(this.afcfByServiceMap).forEach((service:string) => {
      const includedOneOfAllowedCftype = Array.from(allowedCfTypes).some((cfType:string) => {
        return Object.keys(this.afcfByServiceMap[service].collectionFunctions).includes(cfType);
      });
      if (includedOneOfAllowedCftype) {
        allowedServices.add(service);
      }
    });
    this.allowedCfTypes = Array.from(allowedCfTypes);
    this.allowedServices = Array.from(allowedServices);
  }

  async computeAfsBasedOnCfAndTargets(cfId: { tag: string }, targets: TargetModel[]): Promise<void> {
    const allTargetsWithAllPossibleAfs: (TargetModel|null|undefined)[] =
        await Promise.all(targets.map(async (target:TargetModel) => {
          const allowedAfsByCf: Set<string> = new Set();
          const afs = this.configuration.config.accessFunctions;
          const { primaryType } = target;
          if (primaryType == null || primaryType === '') return;

          Object.keys(afs).forEach((key) => {
            if (!afs[key]?.collectionFunctions) return;
            const allTargets = getAFOptionalTargets(afs[key], '*').concat(getAFRequiredTargets(afs[key], '*'));
            const isCfAndTargetIncluded = afs[key]?.collectionFunctions?.includes(cfId.tag)
              && (allTargets.includes(primaryType));
            if (isCfAndTargetIncluded) {
              allowedAfsByCf.add(key);
            }
          });
          const allowedAfsByCfList = Array.from(allowedAfsByCf);
          const promise = this.props.getAllAFsByFilter(this.props.global.selectedDF, WarrantConfiguration.convertToSQLquery('tag', allowedAfsByCfList));
          const res = await promise;
          this.createAfsByTypeMap(res.value.data.data);

          let targetWithAllPossibleAfs: TargetModel | null = null;
          const newTarget: TargetModel = cloneDeep(newTargetModel);
          newTarget.primaryType = primaryType;
          newTarget.id = target.id;
          this.genccc = false;

          allowedAfsByCfList.forEach((afType) => {
            if (!this.afsByTypeMap[afType]) return;
            const {
              genccc,
              x3TargetAssociations,
            } = afs[afType];

            if (genccc === true) {
              this.genccc = true;
            }

            targetWithAllPossibleAfs = this.computeTargetAFs(
              newTarget,
              afType,
              x3TargetAssociations as SupportedFeaturesX3TargetAssociations,
            );
          });
          return targetWithAllPossibleAfs;
        }));
    this.targetsWithAllAfs = Object.assign([], allTargetsWithAllPossibleAfs
      .filter((tar: TargetModel | null | undefined) => (tar !== null || tar !== undefined)));
  }

  computeTargetAFs(
    target: TargetModel,
    safType: string,
    x3TargetAssociations?: SupportedFeaturesX3TargetAssociations,
  ): TargetModel | null {
    const configTargets = this.configuration.config.targets;
    // bX3TargetAssociations indicates whether the target and AF combination has TargetCCC
    let bX3TargetAssociations: string | null = null;
    // afAssociation indicates whether there is an association (AFWT)
    // between the target and the AF of safType.
    let bAFAssociation = true;
    const targetPrimaryType = target.primaryType ? target.primaryType : '';
    if (targetPrimaryType === '' || !configTargets[targetPrimaryType]) return null;
    if (x3TargetAssociations && x3TargetAssociations.targets
      && x3TargetAssociations.targets.length > 0) {
      // If target type is in x3TargetAssociations, associate AF to target
      // and the association has TargetInfo to be configured
      if (x3TargetAssociations.targets.some((targetType: string) => (targetType === targetPrimaryType))) {
        bX3TargetAssociations = x3TargetAssociations.targetInfoType;
      } else {
        // If target type is NOT in x3TargetAssociations, no need to associate AF to it
        // because the target and AF association do not have AFWT
        bAFAssociation = false;
      }
    }
    // Since unspecified services means XMS warrant, AF Types are not supported, so adding all the afs to AfIds
    this.addAfIdsToTarget(target, safType, bX3TargetAssociations, bAFAssociation);
    return target;
  }

  computeAllowedCfsOnCfChange(selectedCfType: string): void {
    if (selectedCfType === undefined) {
      this.computeAllowedCfsAndServicesOnServiceChange(this.selectedServices);
      return;
    }
    this.allowedCfTypes = [selectedCfType];
  }

  async computeSelectedAfTypes(selectedCfType: string): Promise<void> {
    if (!selectedCfType) {
      this.selectedAfTypesList = [];
      return;
    }
    const selectedAfTypesList:Set<string> = new Set();
    this.selectedServices.forEach((service: string) => {
      if (service !== 'unspecified') {
        const cfMap = this.afcfByServiceMap[service].collectionFunctions[selectedCfType];
        if (cfMap) {
          cfMap.accessFunctions.forEach((af: string) => {
            selectedAfTypesList.add(af);
          });
        }
      }
    });
    this.selectedAfTypesList = Array.from(selectedAfTypesList);
    if (this.selectedAfTypesList.length === 0) return;
    const promise = this.props.getAllAFsByFilter(this.props.global.selectedDF, WarrantConfiguration.convertToSQLquery('tag', this.selectedAfTypesList));
    const res = await promise;
    this.createAfsByTypeMap(res.value.data.data);
  }

  async computeSelectedAfTypesForDevices(selectedServices: string[], selectedCfTypes: string[]): Promise<void> {
    const selectedAfTypesList:Set<string> = new Set();

    selectedServices.forEach((service: string) => {
      if (service !== 'unspecified') {
        selectedCfTypes.forEach((selectedCfType:string) => {
          const cfMap = this.afcfByServiceMap[service].collectionFunctions[selectedCfType];
          if (cfMap) {
            cfMap.accessFunctions.forEach((af: string) => {
              selectedAfTypesList.add(af);
            });
          }
        });
      }
    });

    this.selectedAfTypesList = Array.from(selectedAfTypesList);
    if (this.selectedAfTypesList.length === 0) {
      return;
    }

    const res = await this.props.getAllAFsByFilter(this.props.global.selectedDF, WarrantConfiguration.convertToSQLquery('tag', this.selectedAfTypesList));
    this.createAfsByTypeMap(res.value.data.data);
  }

  getPossibleTargets(): TargetModel[] {
    return this.possibleTargets;
  }

  getSelectedAFTypeList(): string[] {
    return this.selectedAfTypesList;
  }

  createAfsByTypeMap(afs: AccessFunction[]): void {
    const afsByTypeMap: AccessFunctionMap = {};

    afs.forEach((af) => {
      if (af.tag == null) {
        return;
      }

      const aft = afsByTypeMap[af.tag];
      if (aft) {
        aft.push(af);

      } else {
        afsByTypeMap[af.tag] = [af];
      }
    });

    this.afsByTypeMap = afsByTypeMap;
  }

  computePossibleTargets(services:string[]): void {
    const afs = this.configuration.config.accessFunctions;
    const configTargets = this.configuration.config.targets;
    const possibleTargets: TargetModel[] = [];
    const possibleTargetsWithAllAfs: TargetModel[] = [];
    this.genccc = false;

    this.selectedAfTypesList.forEach((safType) => {
      if (!this.afsByTypeMap[safType]) {
        return;
      }

      const {
        genccc,
        x3TargetAssociations,
      } = afs[safType];

      const optionalTargets = getAFOptionalTargets(afs[safType], services);
      const requiredTargets = getAFRequiredTargets(afs[safType], services);

      if (genccc === true) {
        this.genccc = true;
      }

      this.computeTargets(
        possibleTargets,
        possibleTargetsWithAllAfs,
        configTargets,
        requiredTargets,
        true,
        safType,
        x3TargetAssociations as SupportedFeaturesX3TargetAssociations,
      );

      this.computeTargets(
        possibleTargets,
        possibleTargetsWithAllAfs,
        configTargets,
        optionalTargets,
        false,
        safType,
        x3TargetAssociations as SupportedFeaturesX3TargetAssociations,
      );
    });

    this.possibleTargets = possibleTargets;
    this.targetsWithAllAfs = possibleTargetsWithAllAfs;
  }

  static getParentType(derivedTarget: CustomizationDerivedTargetService, services: string[]): string {
    let parent = '';
    Object.keys(derivedTarget).some((key) => {
      if (services.includes(key)) {
        parent = derivedTarget[key].parent;
        return true;
      }
      return false;
    });
    if (!parent) {
      if (derivedTarget['*']) {
        parent = derivedTarget['*'].parent;
      }
    }
    return parent;
  }

  static synthesizeTarget(nextTypes: DeviceModelType, type: string, isRequired: boolean): void {
    const synthesizedTarget = cloneDeep(newTargetModel);
    synthesizedTarget.primaryType = type;
    synthesizedTarget.isRequired = isRequired;
    synthesizedTarget.synthesized = true;

    nextTypes[type] = [{
      target: synthesizedTarget,
      display: true,
      synthesized: true,
      ids: [],
    }];
  }

  static hideTarget(nextTypes: DeviceModelType, type: string, possibleTargets: TargetModelMap): void {
    const typeItem = {
      target: possibleTargets[type],
      display: false,
      isFirst: true,
      ids: [],
    };
    if (!nextTypes[type]) {
      nextTypes[type] = [];
    }
    nextTypes[type].push(typeItem);
  }

  static displayTarget(
    nextTypes: DeviceModelType,
    type: string, possibleTargets: TargetModelMap,
    parents: string[],
  ): void {
    const typeItem = {
      target: possibleTargets[type],
      display: true,
      isFirst: true,
      ids: [],
    };
    if (parents.includes(type)) {
      typeItem.target.isRequired = true;
    }
    if (!nextTypes[type]) {
      nextTypes[type] = [];
    }
    nextTypes[type].push(typeItem);
  }

  /**
   * Returns the actual target types and the derived target types supported by the
   * specified services.
   */
  getDerivedTypes(services: string[], derivedTargets: CustomizationDerivedTargetMap): DeviceModelType {
    const nextTypes: DeviceModelType = {};
    // a Possible Targets map where key is the target-primary-type and value is target
    const possibleTargets: TargetModelMap = {};

    this.possibleTargets.forEach((pt) => {
      if (pt.primaryType != null) {
        possibleTargets[pt.primaryType] = pt;
      }
    });

    const parents : string[] = [];
    const synthesizedTargetTypes = new Map<string, boolean>();
    const hide: string[] = [];
    const display: string[] = [];
    const targetTypes = Object.keys(possibleTargets);
    // iterate all the possible targets, if the target matches a derived target
    // determine whether the possible target should be hidden/displayed and
    // new targets need to be synthesized
    for (let i = 0; i < targetTypes.length; i += 1) {
      const type = targetTypes[i];
      const derivedTarget = derivedTargets[type];
      if (derivedTarget) {
        const parentType = WarrantConfiguration.getParentType(derivedTarget, services);
        if (!parentType) {
          // if derived target has no parent, display the possible target that
          // matches a derived target.
          display.push(type);
        } else if (targetTypes.includes(parentType)) {
          // if parent type is a possible target type, hide the derived target type.
          hide.push(type);
          // collect parent type if not already in the parents array, so that parent
          // type could be marked required.
          if (!parents.includes(parentType)) {
            parents.push(parentType);
          }
        } else if (synthesizedTargetTypes.has(parentType)) {
          // This handles the scenario in which the parent is already in the
          // yet to be synthesized target list.
          // For example, if SIP-URI and TEL-URL are possible targets
          // the first interaction with SIP-URI would have added
          // MDN as to be synthesized target. So there is no need to add MDN again
          // incase of TEl-URI, and TEl-URI will be hidden.
          const oneDerivedTargetIsRequired = possibleTargets[type] ? !!possibleTargets[type].isRequired : false;
          const originalSynthesizedTargetIsRequired = !!synthesizedTargetTypes.get(parentType);
          synthesizedTargetTypes.set(parentType, oneDerivedTargetIsRequired || originalSynthesizedTargetIsRequired);
          hide.push(type);
        } else {
          // This handles the scenario where the parent type is neither in
          // possible targets list nor in yet to be synthesized targets list,
          // so we will need to synthesize the parent type and hide the derivedTarget
          synthesizedTargetTypes.set(parentType, possibleTargets[type] ? !!possibleTargets[type].isRequired : false);
          hide.push(type);
        }

      } else {
        // type is not a derived target type, display the type as is.
        display.push(type);
      }
    }

    synthesizedTargetTypes.forEach((isRequired: boolean, type: string) => {
      WarrantConfiguration.synthesizeTarget(nextTypes, type, isRequired);
    });

    hide.forEach((type) => {
      WarrantConfiguration.hideTarget(nextTypes, type, possibleTargets);
    });

    display.forEach((type) => {
      WarrantConfiguration.displayTarget(nextTypes, type, possibleTargets, parents);
    });

    return nextTypes;
  }

  computeTargets(
    possibleTargets: TargetModel[],
    possibleTargetsWithAllAfs: TargetModel[],
    configTargets: SupportedFeaturesTargets,
    targetTags: string[],
    isRequired: boolean,
    safType: string,
    x3TargetAssociations?: SupportedFeaturesX3TargetAssociations,
  ): void {
    if (!targetTags) {
      return; // targetTags is a list of target types.
    }

    targetTags.forEach((t) => {
      // x3TargetAssociations indicates whether the target and AF combination has TargetCCC
      let localX3TargetAssociations: string | null = null;
      // afAssociation indicates whether there is an association (AFWT)
      // between the target and the AF of safType.
      let bAFAssociation = true;
      if (!configTargets[t]) return;
      if (x3TargetAssociations && x3TargetAssociations.targets
        && x3TargetAssociations.targets.length > 0) {
        // If target type is in x3TargetAssociations, associate AF to target
        // and the association has TargetInfo to be configured
        if (x3TargetAssociations.targets.some((targetType:string) => (targetType === t))) {
          localX3TargetAssociations = x3TargetAssociations.targetInfoType ? x3TargetAssociations.targetInfoType : null;
        } else {
          // If target type is NOT in x3TargetAssociations, no need to associate AF to it
          // because the target and AF association do not have AFWT
          bAFAssociation = false;
        }
      }
      const newtargetId = `${t}_${new Date().getTime()}`;
      const alreadyFoundTarget = possibleTargets.find((target) => target.primaryType === t);

      if (!alreadyFoundTarget) {
        const target = cloneDeep(newTargetModel);
        target.id = newtargetId;

        this.computeTargetModel(
          target,
          t,
          safType,
          isRequired,
          false,
          localX3TargetAssociations,
          bAFAssociation,
        );

        possibleTargets.push(target);

      } else {
        this.computeTargetModel(
          alreadyFoundTarget,
          t,
          safType,
          isRequired,
          false,
          localX3TargetAssociations,
          bAFAssociation,
        );
      }

      const alreadyFoundTargetWithAllAfs = possibleTargetsWithAllAfs.find((target) => target.primaryType === t);
      if (!alreadyFoundTargetWithAllAfs) {
        const target = cloneDeep(newTargetModel);
        target.id = newtargetId;

        this.computeTargetModel(
          target,
          t,
          safType,
          isRequired,
          true,
          localX3TargetAssociations,
          bAFAssociation,
        );

        possibleTargetsWithAllAfs.push(target);

      } else {
        this.computeTargetModel(
          alreadyFoundTargetWithAllAfs,
          t,
          safType,
          isRequired,
          true,
          localX3TargetAssociations,
          bAFAssociation,
        );
      }
    });
  }

  addAfIdsToTarget(
    target: TargetModel,
    safType: string,
    x3TargetAssociations: string | null,
    bAFAssociation: boolean,
  ): void {
    this.afsByTypeMap[safType].forEach((af) => {
      const isAfIdAlreadyIncluded = target.afs?.afIds.some((afId) => {
        return af.id === afId.id;
      });

      if (!isAfIdAlreadyIncluded) {
        target.afs?.afIds.push({
          id: af.id,
          name: af.name,
          type: af.type,
          tag: af.tag,
          model: af.model,
          x3TargetAssociations,
          bAFAssociation,
          contentInterfaces: af.contentInterfaces,
        });
      }
    });
  }

  addAfAutowireTypesToTarget(target: TargetModel, safType: string): void {
    this.afsByTypeMap[safType].forEach((af) => {
      const isTypeAlreadyIncluded = target.afs?.afAutowireTypes
        .some((afAutoWireType) => {
          return afAutoWireType.id === af.tag;
        });
      if (!isTypeAlreadyIncluded) {
        target.afs?.afAutowireTypes.push({ id: af.tag, name: af.tag });
      }
    });
  }

  computeTargetModel(
    target: TargetModel,
    key: string,
    safType: string,
    isRequired: boolean,
    isallAfsRequired: boolean,
    bX3TargetAssociation: string | null,
    bAFAssociation: boolean,
  ): void{
    const afs = this.configuration.config.accessFunctions;
    target.primaryType = key;

    // Target should remain required once it is set to true
    target.isRequired = target.isRequired || isRequired;

    if (this.drVirtual === true) {
      if (!(afs[safType] && afs[safType].autowire)) {
        this.addAfIdsToTarget(target, safType, bX3TargetAssociation, bAFAssociation);
      } else {
        if (isallAfsRequired) {
          this.addAfIdsToTarget(target, safType, bX3TargetAssociation, bAFAssociation);
        }
        this.addAfAutowireTypesToTarget(target, safType);
      }
    } else {
      this.addAfIdsToTarget(target, safType, bX3TargetAssociation, bAFAssociation);
    }
  }

  getTargetFields(targets: TargetModel[]): TargetFieldMap {
    const targetFields: TargetFieldMap = {};
    const configTargets = this.configuration.config.targets;

    targets.forEach((target) => {
      const targetField = {
        title: '',
        subFields: {},
      };
      const type = target.primaryType;

      if (type == null) {
        return;
      }

      const {
        isRequired = false,
        synthesized,
      } = target;

      if (synthesized) {
        WarrantConfiguration.computeTargetField(targetField, type, configTargets[type], isRequired, true);

      } else {
        WarrantConfiguration.computeTargetField(targetField, type, configTargets[type], isRequired);
      }

      targetFields[type] = targetField;
    });

    return targetFields;
  }

  static computeTargetField(
    targetField: TargetField,
    type: string,
    configTarget: SupportedFeaturesTarget,
    required: boolean,
    synthesized = false,
  ): void {
    if (configTarget.type === 'array') {
      targetField.title = configTarget.title;
      const subFields = configTarget.items;

      if (subFields == null) {
        return;
      }

      if (synthesized) {
        const field = WarrantConfiguration.getFieldJSON(
          WarrantConfiguration.getCustomSynthesizedConfigTarget(type, subFields),
          required,
        );
        targetField.subFields.primaryValue = field;

      } else if (subFields != null) {
        Object.keys(subFields).forEach((key, _index: number) => {
          const field = WarrantConfiguration.getFieldJSON(subFields[key], required);
          const fieldKey = WarrantConfiguration.getFieldKey(type, key);
          targetField.subFields[fieldKey] = field;
        });
      }

    } else {
      const field = WarrantConfiguration.getFieldJSON(configTarget, required);
      targetField.subFields.primaryValue = field;
    }
  }

  static getCustomSynthesizedConfigTarget(
    type: string,
    subFields: { [itemName: string]: SupportedFeaturesTargetItem },
  ): SupportedFeaturesTargetItem {
    switch (type) {
      case 'MDN': return subFields.value;
      default: return subFields.value;
    }
  }

  static getFieldKey(type: string, key: string): string {
    switch (type) {
      case 'MDNwService':
      case 'MDN':
        return WarrantConfiguration.getCustomFieldForMDN(key);

      case 'LOGIN':
        return WarrantConfiguration.getCustomFieldForLOGIN(key);

      default:
        return key;
    }
  }

  static getFieldJSON(
    configTarget: SupportedFeaturesTarget | SupportedFeaturesTargetItem,
    required: boolean,
  ): FormTemplateField {
    let { type } = configTarget;

    const regexErrorMsg = `Please enter a valid value.${
      configTarget.example ? ` Example:${configTarget.example}` : ''}`;

    const lengthErrorMsg = `Exceeds maximum length of ${configTarget.maxLength}`;

    let options: FormTemplateOption[] = [];
    switch (type) {
      case 'enum': {
        type = 'select';
        if (configTarget.enum != null) {
          options = configTarget.enum?.map((item) => {
            return {
              label: item.title,
              value: item.value,
            };
          });

          options.unshift(defaultOption);
        }
        break;
      }

      case 'string': {
        type = 'autoGrowText';
        break;
      }

      default: break;
    }

    return {
      validation: {
        required,
        validators: [
          {
            type: 'regexp',
            regexp: configTarget.pattern,
            errorMessage: regexErrorMsg,
          },
          {
            type: 'length',
            length: configTarget.maxLength,
            errorMessage: lengthErrorMsg,
          },
        ],
      },
      label: configTarget.title,
      type,
      options,
    };
  }

  static getCustomFieldForMDN(key: string): string {
    switch (key) {
      case 'value': return 'primaryValue';
      case 'type': return 'options';
      default: return key;
    }
  }

  static getCustomFieldForLOGIN(key: string): string {
    switch (key) {
      case 'user': return 'primaryValue';
      case 'vrf': return 'secondaryValue';
      case 'type': return 'loginType';
      default: return key;
    }
  }

  static setDxOwner(dxOwner: string): void {
    newTargetModel.dxOwner = dxOwner;
  }
}
