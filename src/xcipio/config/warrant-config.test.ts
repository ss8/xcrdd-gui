import cloneDeep from 'lodash.clonedeep';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { checkProps } from '../../shared/__test__/testUtils';
import WarrantConfig from './warrant-config';
import getAllAFsByFilterMockData from './__test__/getAllAFsByFilter-test.json';

describe('WarrantConfig', () => {
  let defaultProps: any;
  let warrantConfig: WarrantConfig;
  let getAllAFsByFilterResponse: any;

  beforeEach(() => {
    getAllAFsByFilterResponse = cloneDeep(getAllAFsByFilterMockData);

    // need to mock functions in props.
    defaultProps = {
      global: { selectedDF: 0 },
      warrants: { config: mockSupportedFeatures },
      // eslint-disable-next-line no-unused-vars
      getAllAFsByFilter: jest.fn((_selectedDF, _sqlQuery) => {
        // eslint-disable-next-line no-unused-vars
        return new Promise((resolve, _reject) => {
          const res = getAllAFsByFilterResponse;
          process.nextTick(() => resolve(res));
        });
      }),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    };
  });

  it('does not throw warning with expected props', () => {
    checkProps(WarrantConfig, defaultProps);
  });

  describe('getOnlyAccessFunctionsWithTarget()', () => {
    let accessFunctionMap: SupportedFeaturesAccessFunctions;
    beforeEach(() => {
      accessFunctionMap = {
        ERK_UPF: {
          collectionFunctions: [
            'CF-3GPP-33128',
          ],
          features: [
            'AF-ERK-UPF',
          ],
          autowire: true,
          title: 'Ericsson UPF',
          models: [],
          versions: [],
          genccc: false,
          services: [
            '5G-DATA',
          ],
          requiredTargets: [],
          optionalTargets: [],
          x3deliveryOptions: null,
          xcipioAFType: 'ERK_UPF',
          description: 'Ericsson User Plane Function Point of Interception',
        },
        ERK_SMF: {
          features: [
            'AF-ERK-SMF-X1',
            'AF-ERK-SMF-X2DIST',
          ],
          autowire: true,
          title: 'Ericsson SMF',
          models: [],
          versions: [
            '1.4',
          ],
          genccc: false,
          services: {
            '5G-DATA': {
              requiredTargets: [
                'MSISDN',
              ],
              optionalTargets: [
                'IMEI',
              ],
              collectionFunctions: [
                'CF-ETSI-102232',
              ],
            },
            'LTE-4G-DATA': {
              requiredTargets: [
                'IMSI',
              ],
              collectionFunctions: [
                'CF-3GPP-33108',
              ],
            },
          },
          x3deliveryOptions: null,
          xcipioAFType: 'ERK_SMF',
          description: 'Ericsson Session Management Function Point of Interception',
        },
        ERK_SGW: {
          title: 'Ericsson SAEGW/SGW',
          description: 'Ericsson System Architecture Evolution (SAE) SGW/PGW Access Function',
          xcipioAFType: 'ERK_SGW',
          features: [
            'AF-ERK-SGW',
          ],
          services: [
            'LTE-4G-DATA',
          ],
          requiredTargets: [
            'MSISDN',
          ],
          optionalTargets: [
            'IMSI',
            'IMEI',
          ],
          collectionFunctions: [
            'CF-3GPP-33108',
          ],
          x3deliveryOptions: null,
          versions: [],
          models: [],
          autowire: true,
          genccc: false,
        },
        STARENT_EHA: {

          collectionFunctions: [
            'CF-JSTD025B',
            'CF-TIA-1072',
          ],
          features: [
            'AF-STARENT-EHA',
          ],
          autowire: false,
          title: 'Cisco/Starent EHA',
          models: [],
          versions: [],
          genccc: false,
          services: [
            'CDMA2000-3G-DATA',
          ],
          requiredTargets: [],
          optionalTargets: [
            'MDN',
            'LOGIN',
            'IP4',
          ],
          x3deliveryOptions: null,
          xcipioAFType: 'STARENT_EHA',
          description: 'Cisco/Starent Enterprise Home Agent (EHA) Access Function',
        },
        GENVOIP: {
          title: 'AcmePacket SBC with SS8 VoIP',
          description: 'Oracle Acme Packet Session Border Controller (SBC) with SS8 VoIP interface',
          xcipioAFType: 'GENVOIP',
          features: [
            'AF-ACM-SBC',
          ],
          services: [
            'VOLTE-4G-VOICE',
          ],
          requiredTargets: [
            'SIP-URI',
            'TEL-URI',
          ],
          optionalTargets: [],
          collectionFunctions: [
            'CF-TIA-1066',
            'CF-ATIS-0700005',
          ],
          x3deliveryOptions: null,
          versions: [],
          models: [],
          autowire: true,
          genccc: false,
        },
      };
    });

    it('should return an empty map when the access function is not available', () => {
      expect(WarrantConfig.getAccessFunctionsWithTargets()).toEqual({});
    });

    it('should return an empty map when the access function map is empty', () => {
      expect(WarrantConfig.getAccessFunctionsWithTargets({})).toEqual({});
    });

    it('should return only the access function that have target', () => {
      const afOntologyWithTargets = WarrantConfig.getAccessFunctionsWithTargets(accessFunctionMap);
      const afTags = Array.from(Object.keys(afOntologyWithTargets));
      const expected = ['ERK_SMF', 'ERK_SGW', 'STARENT_EHA', 'GENVOIP'];
      expect(afTags).toEqual(expect.arrayContaining(expected));
    });
  });

  describe('computeEnabledServicesList()', () => {
    it('should set the enabledServicesList with the expected services', () => {
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.enabledServicesList).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        '5G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'PTT',
        'LTE-GIZMO',
        'ETSI-GIZMO-SERVICE',
      ]);
    });

    it('should set the allowedServices with the expected services', () => {
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.allowedServices).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        '5G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'PTT',
        'LTE-GIZMO',
        'ETSI-GIZMO-SERVICE',
      ]);
    });

    it('should set the enabledServicesList with the expected services when deployment is set to empty', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = '';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.enabledServicesList).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        '5G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'VOICE',
        'OFFNET-VOICE',
        'SUPPLEMENTARY',
        'ROAMING',
        'SMS',
        'DATA',
        'VOICE-AND-IP-SMS',
        'MOBILE',
        'BROADBAND',
        'MOBILE-DATA',
        'PTT',
        'LTE-GIZMO',
        'VOICEMAIL',
        'ETSI-GIZMO-SERVICE',
      ]);
    });

    it('should set the allowedServices with the expected services when deployment is set to empty', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = '';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.allowedServices).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        '5G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'VOICE',
        'OFFNET-VOICE',
        'SUPPLEMENTARY',
        'ROAMING',
        'SMS',
        'DATA',
        'VOICE-AND-IP-SMS',
        'MOBILE',
        'BROADBAND',
        'MOBILE-DATA',
        'PTT',
        'LTE-GIZMO',
        'VOICEMAIL',
        'ETSI-GIZMO-SERVICE',
      ]);
    });

    it('should set the enabledServicesList with the expected services when deployment is set to Endeavor', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'Endeavor';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.enabledServicesList).toEqual([
        'VOICE',
        'OFFNET-VOICE',
        'SUPPLEMENTARY',
        'ROAMING',
        'SMS',
        'DATA',
        'VOICE-AND-IP-SMS',
        'MOBILE',
        'BROADBAND',
        'MOBILE-DATA',
        'VOICEMAIL',
      ]);
    });

    it('should set the allowedServices with the expected services when deployment is set to Endeavor', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'Endeavor';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.allowedServices).toEqual([
        'VOICE',
        'OFFNET-VOICE',
        'SUPPLEMENTARY',
        'ROAMING',
        'SMS',
        'DATA',
        'VOICE-AND-IP-SMS',
        'MOBILE',
        'BROADBAND',
        'MOBILE-DATA',
        'VOICEMAIL',
      ]);
    });

    it('should set the afcfByServiceMap with the expected services when deployment is set to V', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.afcfByServiceMap).toEqual({
        'UMTS-3G-VOICE': {
          accessFunctions: [
            'ALU_DSS',
          ],
          collectionFunctions: {
            'CF-JSTD025A': {
              accessFunctions: [
                'ALU_DSS',
              ],
            },
          },
        },
        'ETSI-GIZMO-SERVICE': {
          accessFunctions: [
            'ETSI_GIZMO',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'ETSI_GIZMO',
              ],
            },
          },
        },
        'GSMCDMA-2G-VOICE': {
          accessFunctions: [
            'NORTEL_MTX',
            'NORTEL_HLR',
            'SS8_DF',
            'NORTEL_MSC',
            'NORTEL_DF',
            'HP_HLR',
            'SAMSUNG_MSC',
          ],
          collectionFunctions: {
            'CF-JSTD025A': {
              accessFunctions: [
                'NORTEL_MTX',
                'NORTEL_HLR',
                'SS8_DF',
                'NORTEL_MSC',
                'NORTEL_DF',
                'HP_HLR',
                'SAMSUNG_MSC',
              ],
            },
            'CF-NORTEL': {
              accessFunctions: [
                'NORTEL_HLR',
                'NORTEL_MSC',
              ],
            },
          },
        },
        'VOIP-VOICE': {
          accessFunctions: [
            'ACME_X123',
          ],
          collectionFunctions: {
            'CF-ATIS-1000678': {
              accessFunctions: [
                'ACME_X123',
              ],
            },
          },
        },
        'VOLTE-4G-VOICE': {
          accessFunctions: [
            'LU3G_CSCF',
            'ACME_X123',
            'GENVOIP',
            'NSN_CSCF',
            'SONUS_SBC',
            'PVOIP',
          ],
          collectionFunctions: {
            'CF-ATIS-0700005': {
              accessFunctions: [
                'LU3G_CSCF',
                'GENVOIP',
                'NSN_CSCF',
                'SONUS_SBC',
              ],
            },
            'CF-ETSI-102232': {
              accessFunctions: [
                'ACME_X123',
                'NSN_CSCF',
                'PVOIP',
              ],
            },

            'CF-TIA-1066': {
              accessFunctions: [
                'LU3G_CSCF',
                'GENVOIP',
                'NSN_CSCF',
                'SONUS_SBC',
              ],
            },
          },
        },
        'LTE-4G-DATA': {
          accessFunctions: [
            'EPS',
            'ERK_SMF',
            'ERK_VMME',
            'ALU_MME',
            'PCRF',
            'CISCOVPCRF',
            'HP_AAA',
            'ALU_HLR',
            'NSN_SAEGW',
            'MV_SMSIWF',
            'STA_GGSN',
            'NSNHLR_SDM_PRGW',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'EPS',
                'ERK_SMF',
                'ERK_VMME',
                'ALU_MME',
                'PCRF',
                'CISCOVPCRF',
                'HP_AAA',
                'ALU_HLR',
                'NSN_SAEGW',
                'MV_SMSIWF',
                'STA_GGSN',
                'NSNHLR_SDM_PRGW',
              ],
            },
          },
        },
        'LTE-4G-DATA-ROAMING': {
          accessFunctions: [
            'ERK_VMME',
            'ALU_MME',
            'ALU_SGW',
            'ERK_SGW',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'ERK_VMME',
                'ALU_MME',
                'ALU_SGW',
                'ERK_SGW',
              ],
            },
          },
        },
        'LTE-GIZMO': {
          accessFunctions: [
            'GIZMO',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'GIZMO',
              ],
            },
          },
        },
        'LTE-IOT': {
          accessFunctions: [
            'ERK_SCEF',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'ERK_SCEF',
              ],
            },
          },
        },
        'VOLTE-RCS': {
          accessFunctions: [
            'MVNR_RCS_MSTORE',
            'ICS_RCS',
            'MVNR_RCS_AS',
            'NSN_CSCF',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'MVNR_RCS_MSTORE',
              ],
            },
            'CF-ETSI-102232': {
              accessFunctions: [
                'ICS_RCS',
                'MVNR_RCS_AS',
                'NSN_CSCF',
              ],
            },
          },
        },
        'VZ-VISIBLE': {
          accessFunctions: [
            'MVNR_SBC',
            'MVNR_HSS',
            'MITEL_TAS',
            'MVNR_CSCF',
            'MVNR_PGW',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'MVNR_HSS',
                'MVNR_PGW',
              ],
            },
            'CF-ATIS-0700005': {
              accessFunctions: [
                'MVNR_SBC',
                'MITEL_TAS',
                'MVNR_CSCF',
              ],
            },

          },
        },
        PTT: {
          accessFunctions: [
            'KODIAK_PTT',
          ],
          collectionFunctions: {
            'CF-TIA-1072': {
              accessFunctions: [
                'KODIAK_PTT',
              ],
            },
          },
        },
        '5G-DATA': {
          accessFunctions: [
            'ERK_SMF',
            'MVNR_5GSMSC',
            'NOK_5GAMF',
            'NOK_5GSMSF',
            'NSN_SDM',
            'NOK_5GUDM',
            'NOK_5GSMF',
            'ERK_5GAMF',
          ],
          collectionFunctions: {
            'CF-3GPP-33108': {
              accessFunctions: [
                'NSN_SDM',
              ],
            },
            'CF-ETSI-102232': {
              accessFunctions: [
                'ERK_SMF',
                'MVNR_5GSMSC',
                'NOK_5GAMF',
                'NOK_5GSMSF',
                'NOK_5GUDM',
                'NOK_5GSMF',
                'ERK_5GAMF',
              ],
            },
          },
        },
        'CDMA2000-3G-DATA': {
          accessFunctions: [
            'STA_PDSN',
            'STARENT_EHA',
            'BRW_AAA',
          ],
          collectionFunctions: {
            'CF-JSTD025B': {
              accessFunctions: [
                'STA_PDSN',
                'STARENT_EHA',
                'BRW_AAA',
              ],
            },
            'CF-TIA-1072': {
              accessFunctions: [
                'STA_PDSN',
                'STARENT_EHA',
              ],
            },
          },
        },
      });
    });

    it('should set the afcfByServiceMap with the expected services when deployment is set to Endeavor', () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'Endeavor';
      warrantConfig = new WarrantConfig(defaultProps);

      warrantConfig.computeEnabledServicesList();
      expect(warrantConfig.afcfByServiceMap).toEqual({
        BROADBAND: {
          accessFunctions: [
            'GPVRADIUS',
            'NKSR',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'GPVRADIUS',
                'NKSR',
              ],
            },
          },
        },
        DATA: {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        MOBILE: {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        'MOBILE-DATA': {
          accessFunctions: [
            'GPVRADIUS',
            'NKSR',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'GPVRADIUS',
                'NKSR',
              ],
            },
          },
        },
        'OFFNET-VOICE': {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        ROAMING: {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        SMS: {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        SUPPLEMENTARY: {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        VOICE: {
          accessFunctions: [
            'ACME_X123',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'ACME_X123',
              ],
            },
          },
        },
        'VOICE-AND-IP-SMS': {
          accessFunctions: [
            'AXSGP',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'AXSGP',
              ],
            },
          },
        },
        VOICEMAIL: {
          accessFunctions: [
            'STREAMWIDE',
          ],
          collectionFunctions: {
            'CF-ETSI-102232': {
              accessFunctions: [
                'STREAMWIDE',
              ],
            },
          },
        },

      });
    });
  });

  describe('computeEnabledServicesForDevicePanel()', () => {
    it('should set the allowedServices with all services', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = [
        'CF-ATIS-1000678',
        'CF-TIA-1066',
        'CF-3GPP-33108',
        'CF-ETSI-102232',
        'CF-ATIS-0700005',
        'CF-JSTD025A',
        'CF-TIA-1072',
      ];

      await warrantConfig.computeEnabledServicesForDevicePanel(collectionFunctionTypes);
      expect(warrantConfig.allowedServices).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        '5G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'PTT',
        'LTE-GIZMO',
        'ETSI-GIZMO-SERVICE',
      ]);
    });

    it('should set the allowedServices with all services for Endeavor', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'Endeavor';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = [
        'CF-ATIS-1000678',
        'CF-TIA-1066',
        'CF-3GPP-33108',
        'CF-ETSI-102232',
        'CF-ATIS-0700005',
        'CF-JSTD025A',
        'CF-TIA-1072',
      ];

      await warrantConfig.computeEnabledServicesForDevicePanel(collectionFunctionTypes);
      expect(warrantConfig.allowedServices).toEqual([
        'VOICE',
        'OFFNET-VOICE',
        'SUPPLEMENTARY',
        'ROAMING',
        'SMS',
        'DATA',
        'VOICE-AND-IP-SMS',
        'MOBILE',
        'BROADBAND',
        'MOBILE-DATA',
        'VOICEMAIL',
      ]);
    });

    it('should set the allowedServices with all services except 5G data', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = [
        'CF-ATIS-1000678',
        'CF-TIA-1066',
        'CF-3GPP-33108',
        'CF-ETSI-102232',
        'CF-ATIS-0700005',
        'CF-JSTD025A',
        'CF-TIA-1072',
      ];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter((accessFunction: AccessFunction) => accessFunction.type !== 'ERK_SMF');
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeEnabledServicesForDevicePanel(collectionFunctionTypes);
      expect(warrantConfig.allowedServices).toEqual([
        'VOLTE-4G-VOICE',
        'UMTS-3G-VOICE',
        'GSMCDMA-2G-VOICE',
        'VZ-VISIBLE',
        'LTE-4G-DATA',
        'VOLTE-RCS',
        'LTE-4G-DATA-ROAMING',
        'CDMA2000-3G-DATA',
        'LTE-IOT',
        'VOIP-VOICE',
        'PTT',
        'LTE-GIZMO',
        'ETSI-GIZMO-SERVICE',
      ]);
    });
  });

  describe('filterServicesBasedUponAvailableAfs()', () => {
    test('filterServicesBasedUponAvailableAfs returns the correct services', async () => {
      const services = ['GSMCDMA-2G-VOICE', 'LTE-4G-DATA', 'VZ-VISIBLE'];
      const afs = ['NORTEL_MTX', 'HP_HLR', 'SAMSUNG_MSC', 'ALU_MME', 'PCRF', 'MVNR_PGW', 'HP_AAA', 'NSNHLR_SDM_PRGW', 'ALU_SGW', 'MVNR_HSS'];

      const expectedResult = ['GSMCDMA-2G-VOICE', 'LTE-4G-DATA', 'VZ-VISIBLE'];

      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);
      await warrantConfig.filterServicesBasedUponAvailableAfs(services, afs);
      expect(services).toEqual(expectedResult);
    });
  });

  describe('computeSelectedAfTypes()', () => {
    it('should set the afsByTypeMap and selectedAfTypesList with the expected list', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter((accessFunction: AccessFunction) => accessFunction.type === 'ERK_SMF');
      getAllAFsByFilterResponse.value.data.data = data;

      warrantConfig.computeAllowedCfsAndServicesOnServiceChange(['5G-DATA']);
      await warrantConfig.computeSelectedAfTypes('CF-ETSI-102232');
      expect(warrantConfig.afsByTypeMap).toEqual({
        ERK_SMF: data,
      });
      expect(warrantConfig.selectedAfTypesList).toEqual([
        'ERK_SMF',
        'MVNR_5GSMSC',
        'NOK_5GAMF',
        'NOK_5GSMSF',
        'NOK_5GUDM',
        'NOK_5GSMF',
        'ERK_5GAMF',
      ]);
    });
  });

  describe('computeSelectedAfTypesForDevices()', () => {
    it('should set the afsByTypeMap and selectedAfTypesList with the expected list', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = [
        'CF-ATIS-1000678',
        'CF-TIA-1066',
        'CF-3GPP-33108',
        'CF-ETSI-102232',
        'CF-ATIS-0700005',
        'CF-JSTD025A',
        'CF-TIA-1072',
      ];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter((accessFunction: AccessFunction) => accessFunction.type === 'ERK_SMF');
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeSelectedAfTypesForDevices(['5G-DATA'], collectionFunctionTypes);
      expect(warrantConfig.afsByTypeMap).toEqual({
        ERK_SMF: data,
      });
      expect(warrantConfig.selectedAfTypesList).toEqual([
        'NSN_SDM',
        'ERK_SMF',
        'MVNR_5GSMSC',
        'NOK_5GAMF',
        'NOK_5GSMSF',
        'NOK_5GUDM',
        'NOK_5GSMF',
        'ERK_5GAMF',
      ]);
    });
  });

  describe('computePossibleTargets()', () => {
    it('should return correct targets when service is 4G PD', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = ['CF-3GPP-33108'];
      const services: string[] = ['LTE-4G-DATA'];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter(
        (accessFunction: AccessFunction) => (accessFunction.type === 'ERK_SMF' || accessFunction.type === 'ALU_MME'),
      );
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeSelectedAfTypesForDevices(services, collectionFunctionTypes);
      warrantConfig.computePossibleTargets(services);
      expect(warrantConfig.possibleTargets).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ primaryType: 'IMSI', isRequired: true }),
        ]),
      );
    });

    it('should return correct targets when service is 5G PD', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = ['CF-ETSI-102232'];
      const services: string[] = ['5G-DATA'];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter(
        (accessFunction: AccessFunction) => (accessFunction.type === 'ERK_SMF'),
      );
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeSelectedAfTypesForDevices(services, collectionFunctionTypes);
      warrantConfig.computePossibleTargets(services);
      expect(warrantConfig.possibleTargets).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ primaryType: 'MSISDN', isRequired: true }),
          expect.objectContaining({ primaryType: 'IMEI', isRequired: false }),
        ]),
      );
    });

    it('should return correct targets when service is 5G PD and 4G PD', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = ['CF-ETSI-102232'];
      const services: string[] = ['5G-DATA', 'LTE-4G-DATA'];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter(
        (accessFunction: AccessFunction) => (accessFunction.type === 'ERK_SMF' || accessFunction.type === 'ALU_MME'),
      );
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeSelectedAfTypesForDevices(services, collectionFunctionTypes);
      warrantConfig.computePossibleTargets(services);
      expect(warrantConfig.possibleTargets).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ primaryType: 'IMSI', isRequired: true }),
          expect.objectContaining({ primaryType: 'MSISDN', isRequired: true }),
          expect.objectContaining({ primaryType: 'IMEI', isRequired: false }),
        ]),
      );
    });

    it('should return correct targets when service is Volte 4G', async () => {
      defaultProps.discoveryXcipio.discoveryXcipioSettings.deployment = 'andromeda';
      warrantConfig = new WarrantConfig(defaultProps);

      const collectionFunctionTypes = ['CF-ATIS-0700005'];
      const services: string[] = ['VOLTE-4G-VOICE'];

      const responseData = getAllAFsByFilterResponse.value.data.data;
      const data = responseData.filter(
        (accessFunction: AccessFunction) => (accessFunction.type === 'NSN_CSCF' || accessFunction.type === 'SONUS_SBC'),
      );
      getAllAFsByFilterResponse.value.data.data = data;

      await warrantConfig.computeSelectedAfTypesForDevices(services, collectionFunctionTypes);
      warrantConfig.computePossibleTargets(services);
      expect(warrantConfig.possibleTargets).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ primaryType: 'SIP-URI', isRequired: true }),
          expect.objectContaining({ primaryType: 'TEL-URI', isRequired: true }),
        ]),
      );

    });

  });

});
