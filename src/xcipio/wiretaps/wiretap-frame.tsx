import React, { Fragment } from 'react';
import WiretapGrid from './grid';

class WiretapFrame extends React.Component<any, any> {
    private gridRef: any;

    constructor(props:any) {
      super(props);
      this.gridRef = React.createRef();
    }

    render() {
      return (
        <Fragment>
          <div className="col-md-12" style={{ height: 'inherit' }}>
            <WiretapGrid ref={this.gridRef} {...this.props} />
          </div>
        </Fragment>
      );
    }
}

export default WiretapFrame;
