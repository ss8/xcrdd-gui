import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import WiretapFrame from './wiretap-frame';

const moduleRoot = '/wiretaps';

const ModuleRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <WiretapFrame {...props} />}
    />
  </Fragment>
);

export default ModuleRoutes;
