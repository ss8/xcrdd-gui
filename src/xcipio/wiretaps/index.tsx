import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getAllWiretaps,
  getWiretapById,
  createWiretap,
  updateWiretap,
  patchWiretap,
  deleteWiretap,
  getWiretapGridSettings,
  putWiretapGridSettings,

} from './wiretap-actions';
import ModuleRoutes from './wiretap-routes';

const Wiretaps = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  wiretaps: state.wiretaps,
  global: state.global,
  authentication: state.authentication,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getAllWiretaps,
  getWiretapById,
  createWiretap,
  updateWiretap,
  patchWiretap,
  deleteWiretap,
  getWiretapGridSettings,
  putWiretapGridSettings,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Wiretaps));
