import Http from '../../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_WIRETAPS = 'GET_ALL_WIRETAPS';
export const GET_WIRETAP_BY_ID = 'GET_WIRETAP_BY_ID';
export const UPDATE_WIRETAP = 'UPDATE_WIRETAP';
export const PATCH_WIRETAP = 'PATCH_WIRETAP';
export const DELETE_WIRETAP = 'DELETE_WIRETAP';
export const CREATE_WIRETAP = 'CREATE_WIRETAP';
export const GET_WIRETAP_GRID_SETTING = 'GET_WIRETAP_GRID_SETTING';
export const PUT_WIRETAP_GRID_SETTING = 'PUT_WIRETAP_GRID_SETTING';
export const GET_ALL_WIRETAPS_FULFILLED = 'GET_ALL_WIRETAPS_FULFILLED';
export const GET_WIRETAP_BY_ID_FULFILLED = 'GET_WIRETAP_BY_ID_FULFILLED';
export const UPDATE_WIRETAP_FULFILLED = 'UPDATE_WIRETAP_FULFILLED';
export const PATCH_WIRETAP_FULFILLED = 'PATCH_WIRETAP_FULFILLED';
export const DELETE_WIRETAP_FULFILLED = 'DELETE_WIRETAP_FULFILLED';
export const CREATE_WIRETAP_FULFILLED = 'CREATE_WIRETAP_FULFILLED';

// action creators
export const getAllWiretaps = (dfId: string, body = '') => {
  return ({ type: GET_ALL_WIRETAPS, payload: http.get(`/wiretaps/${dfId}?${body}&$view=reporting`) });
};
export const getWiretapById = (id: number) => ({ type: GET_WIRETAP_BY_ID, payload: http.get(`/wiretaps/${id}`) });
export const createWiretap = (body: any) => ({ type: CREATE_WIRETAP, payload: http.post('/wiretaps', body) });
export const updateWiretap = (id: number, body: any) => ({ type: UPDATE_WIRETAP, payload: http.put(`/wiretaps/${id}`, body) });
export const patchWiretap = (id: number, body: any) => ({ type: PATCH_WIRETAP, payload: http.patch(`/wiretaps/${id}`, body) });
export const deleteWiretap = (id: number) => ({ type: DELETE_WIRETAP, payload: http.delete(`/wiretaps/${id}`) });

// wiretap table column setting
export const getWiretapGridSettings = (userId: string) => {
  return ({ type: GET_WIRETAP_GRID_SETTING, payload: http.get(`/users/${userId}/settings?name=WiretapGridSettings`) });
};
export const putWiretapGridSettings = (userId: any, wiretapGridSettings: any) => {
  // converts double quotes to escape single quote
  const settingInJson = JSON.stringify(wiretapGridSettings);
  const wiretapGridSettingsData = {
    name: 'WiretapGridSettings',
    type: 'json',
    user: userId,
    setting: settingInJson,
  };
  const requestBody = [wiretapGridSettingsData];
  return ({ type: PUT_WIRETAP_GRID_SETTING, payload: http.put(`/users/${userId}/settings`, requestBody) });
};
