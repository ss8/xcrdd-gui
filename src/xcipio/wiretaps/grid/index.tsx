import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { getSortedTimeZoneLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { FilterModel } from 'shared/commonGrid/common-grid.types';
import { adaptToServerValueFilterModel } from 'shared/commonGrid/filterModel.adapter';
import WiretapGridColumnDefs from './grid-column-defs';
import CommonGrid, { ICommonGridProperties } from '../../../shared/commonGrid';
import DateTimeCellRenderer from '../../../shared/commonGrid/date-cell-renderer';
import { ROWSELECTION } from '../../../shared/commonGrid/grid-enums';
import DateTimeWithTimezoneCellRenderer from '../../../shared/commonGrid/DateTimeWithTimezoneCellRenderer';
import { TimeZoneCellRenderer } from '../../../shared/commonGrid/TimeZoneCellRenderer';

class WiretapGrid extends React.Component<any, any> {
  columnDefs = WiretapGridColumnDefs;

  getDataPath = (data: any) => {
    data.cases.map((item: any) => item.name);
  };

  initializeColumnDef = () => {
    const {
      timezones,
    } = this.props;

    const timeZoneCol = this.columnDefs.find((columnDef) => (columnDef.field === 'reporting.timeZone'));
    if (timeZoneCol) {
      timeZoneCol.filter = 'agSetColumnFilter';
      timeZoneCol.filterParams = {
        values: getSortedTimeZoneLabels(timezones),
      };
    }
  }

  componentDidUpdate() {
    this.initializeColumnDef();
  }

  handleSelection = (e: any) => {
    this.setState({ selection: e.api.getSelectedRows() });
  };

  handleGetResultsFromServer = (body: any) => {
    return this.props.getAllWiretaps(this.props.global.selectedDF, body);
  }

  handleGetGridSettingsFromServer = () => {
    return this.props.getWiretapGridSettings(this.props.authentication.userId);
  }

  saveGridSettings = (wiretapGridSettings: { [key: string]: any }) => {
    if (wiretapGridSettings !== null) {
      this.props.putWiretapGridSettings(this.props.authentication.userId,
        wiretapGridSettings);
    }
  }

  adaptToGridFilterModel = (filterModel:FilterModel): FilterModel => {
    return adaptToServerValueFilterModel(filterModel, 'reporting.timeZone', this.props.timezones);
  }

  render() {
    const frameworkComponents = {
      datetimeCellRenderer: DateTimeCellRenderer,
      DateTimeWithTimezoneCellRenderer,
      TimeZoneCellRenderer,
    };
    const commonGridProps: ICommonGridProperties = {
      getDataPath: this.getDataPath,
      onSelectionChanged: this.handleSelection,
      rowSelection: ROWSELECTION.SINGLE,
      columnDefs: this.columnDefs,
      rowData: this.props.wiretaps.wiretaps,
      enableBrowserTooltips: true,
      context: this,
      pagination: true,
      noResults: false,
      frameworkComponents,
      getGridResultsFromServer: this.handleGetResultsFromServer,
      adaptToFilterModel: this.adaptToGridFilterModel,
      renderDataFromServer: true,
      getGridSettingsFromServer: this.handleGetGridSettingsFromServer,
      saveGridSettings: this.saveGridSettings,
      isBeforeLogout: this.props.authentication.beforeLogout,
    };
    return (
      <Fragment>
        <div className="wiretap-grid-container" style={{ height: 'inherit' }}>
          <CommonGrid {...commonGridProps} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  users: {
    timezones,
  },
}: any) => ({
  timezones,
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(WiretapGrid);
