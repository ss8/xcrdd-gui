import {
  GET_ALL_WIRETAPS_FULFILLED,
} from './wiretap-actions';

const defaultState = { wiretaps: [] };

const moduleReducer = (state = defaultState, action: any) => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_WIRETAPS_FULFILLED:
      return { ...state, wiretaps: action.payload.data.data };
    default:
      return state;
  }
};
export default moduleReducer;
