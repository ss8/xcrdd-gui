import React from 'react';
import { Provider } from 'react-redux';
import { fireEvent, render, screen } from '@testing-library/react';
import store from 'data/store';
import AuthActions, { SET_AUTHENTICATION_EXPIRED } from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import SessionExpirationNotification, { SessionExpirationNotification as SessionExpirationNotificationComponent } from './SessionExpirationNotification';

describe('<SessionExpirationNotification />', () => {
  beforeEach(() => {
    store.dispatch({
      type: AuthActions.LOGOUT_FULFILLED,
    });
  });

  it('should not render if not authenticated', () => {
    const { container } = render(
      <Provider store={store}>
        <SessionExpirationNotification />
      </Provider>,
    );

    expect(container.firstChild).toBeNull();
  });

  it('should not render if authenticate but not expired', () => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
      sessionTimeout: '1800',
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    const { container } = render(
      <Provider store={store}>
        <SessionExpirationNotification />
      </Provider>,
    );

    expect(container.firstChild).toBeNull();
  });

  it('should render if authenticate and expired', () => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: SET_AUTHENTICATION_EXPIRED,
    });

    render(
      <Provider store={store}>
        <SessionExpirationNotification />
      </Provider>,
    );

    expect(screen.getByText('Your session expired. Please login again.')).toBeVisible();
  });

  it('should render after the timeout', async () => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
      sessionTimeout: '1',
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    render(
      <Provider store={store}>
        <SessionExpirationNotification />
      </Provider>,
    );

    expect(screen.queryByText('Your session expired. Please login again.')).toBeNull();

    await new Promise((resolve) => setTimeout(resolve, 1200));

    expect(screen.getByText('Your session expired. Please login again.')).toBeVisible();
  });

  it('should not render if no authentication timeout defined', async () => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
      sessionTimeout: '0',
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    render(
      <Provider store={store}>
        <SessionExpirationNotification />
      </Provider>,
    );

    expect(screen.queryByText('Your session expired. Please login again.')).toBeNull();

    await new Promise((resolve) => setTimeout(resolve, 1200));

    expect(screen.queryByText('Your session expired. Please login again.')).toBeNull();
  });

  it('should call handleLogout when clicking in the Ok button', () => {
    const handleLogout = jest.fn();

    render(
      <SessionExpirationNotificationComponent
        authenticationTimeout="1800"
        isAuthenticated
        isAuthenticationExpired
        handleLogout={handleLogout}
        startAuthenticationTimeout={jest.fn()}
      />,
    );

    expect(screen.getByText('Your session expired. Please login again.')).toBeVisible();

    fireEvent.click(screen.getByText('Ok'));

    expect(handleLogout).toBeCalled();
  });

  it('should call startAuthenticationTimeout if isAuthenticated and authenticationTimeout defined', () => {
    const startAuthenticationTimeout = jest.fn();

    render(
      <SessionExpirationNotificationComponent
        authenticationTimeout="1800"
        isAuthenticated
        isAuthenticationExpired
        handleLogout={jest.fn()}
        startAuthenticationTimeout={startAuthenticationTimeout}
      />,
    );

    expect(startAuthenticationTimeout).toBeCalled();
  });

  it('should not call startAuthenticationTimeout if not isAuthenticated', () => {
    const startAuthenticationTimeout = jest.fn();

    render(
      <SessionExpirationNotificationComponent
        authenticationTimeout="1800"
        isAuthenticated={false}
        isAuthenticationExpired
        handleLogout={jest.fn()}
        startAuthenticationTimeout={startAuthenticationTimeout}
      />,
    );

    expect(startAuthenticationTimeout).not.toBeCalled();
  });

  it('should not call startAuthenticationTimeout if authenticationTimeout not defined', () => {
    const startAuthenticationTimeout = jest.fn();

    render(
      <SessionExpirationNotificationComponent
        authenticationTimeout=""
        isAuthenticated
        isAuthenticationExpired
        handleLogout={jest.fn()}
        startAuthenticationTimeout={startAuthenticationTimeout}
      />,
    );

    expect(startAuthenticationTimeout).not.toBeCalled();
  });
});
