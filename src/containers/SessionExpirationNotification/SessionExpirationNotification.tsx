import React, { FunctionComponent, ReactElement, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import {
  Button,
  Dialog,
  Classes,
} from '@blueprintjs/core';
import { RootState } from 'data/root.reducers';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as authenticationActions from 'data/authentication/authentication.actions';

const mapState = (state: RootState) => ({
  isAuthenticated: authenticationSelectors.isAuthenticated(state),
  isAuthenticationExpired: authenticationSelectors.isAuthenticationExpired(state),
  authenticationTimeout: authenticationSelectors.getAuthenticationTimeout(state),
});

const mapDispatch = {
  startAuthenticationTimeout: authenticationActions.startAuthenticationTimeout,
  handleLogout: authenticationActions.handleLogout,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

export type Props = PropsFromRedux;

export const SessionExpirationNotification: FunctionComponent<Props> = ({
  isAuthenticated,
  isAuthenticationExpired,
  authenticationTimeout,
  startAuthenticationTimeout,
  handleLogout,
}: Props): ReactElement | null => {
  useEffect(() => {
    if (isAuthenticated && authenticationTimeout) {
      startAuthenticationTimeout(+authenticationTimeout);
    }
  }, [isAuthenticated, authenticationTimeout, startAuthenticationTimeout]);

  if (!isAuthenticated || !isAuthenticationExpired) {
    return null;
  }

  return (
    <Dialog
      title="Session expired"
      isOpen
      isCloseButtonShown={false}
    >
      <div className={Classes.DIALOG_BODY}>
        Your session expired. Please login again.
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button intent="primary" onClick={handleLogout}>Ok</Button>
        </div>
      </div>
    </Dialog>
  );
};

export default (connector(SessionExpirationNotification));
