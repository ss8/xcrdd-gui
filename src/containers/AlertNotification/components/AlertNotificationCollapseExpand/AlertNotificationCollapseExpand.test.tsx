import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import AlertNotificationCollapseExpand from './AlertNotificationCollapseExpand';

describe('<AlertNotificationCollapseExpand />', () => {
  it('should render the icon collapsed', () => {
    const { container } = render(
      <AlertNotificationCollapseExpand collapsed onClick={jest.fn()} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="alert-notification-collapse-expand"
          role="presentation"
          title="Expand"
        >
          <span
            class="bp3-icon bp3-icon-chevron-up bp3-intent-danger"
            icon="chevron-up"
          >
            <svg
              data-icon="chevron-up"
              height="15"
              viewBox="0 0 16 16"
              width="15"
            >
              <desc>
                chevron-up
              </desc>
              <path
                d="M12.71 9.29l-4-4C8.53 5.11 8.28 5 8 5s-.53.11-.71.29l-4 4a1.003 1.003 0 001.42 1.42L8 7.41l3.29 3.29c.18.19.43.3.71.3a1.003 1.003 0 00.71-1.71z"
                fill-rule="evenodd"
              />
            </svg>
          </span>
        </span>
      </div>
    `);
  });

  it('should render the icon expanded', () => {
    const { container } = render(
      <AlertNotificationCollapseExpand collapsed={false} onClick={jest.fn()} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="alert-notification-collapse-expand"
          role="presentation"
          title="Collapse"
        >
          <span
            class="bp3-icon bp3-icon-chevron-down bp3-intent-danger"
            icon="chevron-down"
          >
            <svg
              data-icon="chevron-down"
              height="15"
              viewBox="0 0 16 16"
              width="15"
            >
              <desc>
                chevron-down
              </desc>
              <path
                d="M12 5c-.28 0-.53.11-.71.29L8 8.59l-3.29-3.3a1.003 1.003 0 00-1.42 1.42l4 4c.18.18.43.29.71.29s.53-.11.71-.29l4-4A1.003 1.003 0 0012 5z"
                fill-rule="evenodd"
              />
            </svg>
          </span>
        </span>
      </div>
    `);
  });

  it('should call onClick function on click', () => {
    const onClick = jest.fn();
    const { container } = render(
      <AlertNotificationCollapseExpand collapsed={false} onClick={onClick} />,
    );

    fireEvent.click(container.children[0]);
    expect(onClick).toBeCalled();
  });
});
