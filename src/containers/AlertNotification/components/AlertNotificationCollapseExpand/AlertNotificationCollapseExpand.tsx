import React, { FunctionComponent, ReactElement } from 'react';
import { Icon, IconName, Intent } from '@blueprintjs/core';

type Props = {
  collapsed: boolean
  onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export const AlertNotificationCollapseExpand: FunctionComponent<Props> = ({
  collapsed,
  onClick,
}: Props): ReactElement => {

  let icon: IconName = 'chevron-down';
  let title = 'Collapse';
  if (collapsed) {
    icon = 'chevron-up';
    title = 'Expand';
  }

  return (
    <span className="alert-notification-collapse-expand" onClick={onClick} role="presentation" title={title}>
      <Icon icon={icon} iconSize={15} intent={Intent.DANGER} />
    </span>
  );
};

export default AlertNotificationCollapseExpand;
