import { getAlarmTypes } from './getAlarmTypes';

describe('getAlarmTypes()', () => {
  it('should return the expected list of alarm types', () => {
    expect(getAlarmTypes(false, false, false, false)).toEqual(
      [],
    );

    expect(getAlarmTypes(true, false, false, false)).toEqual(
      ['NEWIP'],
    );

    expect(getAlarmTypes(false, true, false, false)).toEqual(
      ['CCBUFUSAGE', 'IRIBUFUSAGE'],
    );

    expect(getAlarmTypes(false, false, true, false)).toEqual(
      ['AUDITDISC'],
    );

    expect(getAlarmTypes(false, false, false, true)).toEqual(
      ['DYNIPLKPFAILURE'],
    );

    expect(getAlarmTypes(true, true, true, true)).toEqual(
      ['NEWIP', 'CCBUFUSAGE', 'IRIBUFUSAGE', 'AUDITDISC', 'DYNIPLKPFAILURE'],
    );
  });
});
