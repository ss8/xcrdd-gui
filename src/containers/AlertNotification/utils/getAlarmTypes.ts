import { AlarmType } from 'data/alertNotification/alertNotification.types';

// eslint-disable-next-line import/prefer-default-export
export function getAlarmTypes(
  isAlertNotificationNewIPEnabled: boolean,
  isAlertNotificationBufferingEnabled: boolean,
  isAlertNotificationAuditDiscrepancyEnabled: boolean,
  isAlertNotificationInterceptDynamicIPLookupEnabled: boolean,
): AlarmType[] {
  const alarmTypes: AlarmType[] = [];

  if (isAlertNotificationNewIPEnabled) {
    alarmTypes.push('NEWIP');
  }

  if (isAlertNotificationBufferingEnabled) {
    alarmTypes.push('CCBUFUSAGE');
    alarmTypes.push('IRIBUFUSAGE');
  }

  if (isAlertNotificationAuditDiscrepancyEnabled) {
    alarmTypes.push('AUDITDISC');
  }

  if (isAlertNotificationInterceptDynamicIPLookupEnabled) {
    alarmTypes.push('DYNIPLKPFAILURE');
  }

  return alarmTypes;
}
