import React from 'react';
import { Provider } from 'react-redux';
import {
  act,
  fireEvent,
  render,
  screen,
} from '@testing-library/react';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import store from 'data/store';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import { AlertNotificationModel, GET_ALERT_NOTIFICATIONS_FULFILLED, POLL_ALERT_NOTIFICATIONS } from 'data/alertNotification/alertNotification.types';
import { mockAlertNotifications } from '__test__/mockAlertNotifications';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import AuthActions from 'data/authentication/authentication.types';
import AlertNotification from './AlertNotification';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AlertNotification />', () => {
  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should not render if user not authenticated', () => {
    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([{}]),
    });

    const { container } = render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(container).toMatchInlineSnapshot('<div />');
  });

  it('should not render if disabled', () => {
    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([{}]),
    });

    const { container } = render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(container).toMatchInlineSnapshot('<div />');
  });

  it('should not render if list of alert notification is empty', () => {
    store.dispatch({
      type: GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    const { container } = render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(container).toMatchInlineSnapshot('<div />');
  });

  it('should render the list of alert notifications', () => {
    store.dispatch({
      type: GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    });

    render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(screen.getByText(`Key Alarm Notifications (${mockAlertNotifications.length})`));
    expect(screen.getByText('Alert notification 1'));
  });

  it('should collapse and expand the list when clicking in the arrow', () => {
    store.dispatch({
      type: GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    });

    render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(screen.getByText(`Key Alarm Notifications (${mockAlertNotifications.length})`));
    expect(screen.getByText('Alert notification 1'));

    fireEvent.click(screen.getByTitle('Collapse'));
    expect(screen.queryAllByText('Alert notification 1')).toHaveLength(0);

    fireEvent.click(screen.getByTitle('Expand'));
    expect(screen.getByText('Alert notification 1'));
  });

  it('should expand when receiving new notifications', () => {
    store.dispatch({
      type: GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    });

    render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(screen.getByText('Key Alarm Notifications (1)'));

    fireEvent.click(screen.getByTitle('Collapse'));
    expect(screen.queryAllByText('Alert notification 1')).toHaveLength(0);

    const newAlertNotifications: AlertNotificationModel[] = [
      ...mockAlertNotifications,
      {
        id: '2',
        message: 'Alert notification 2',
        alarmType: 'AUDITDISC',
        procName: 'proc1',
        utcDateTime: '',
      },
    ];

    act(() => {
      store.dispatch({
        type: GET_ALERT_NOTIFICATIONS_FULFILLED,
        payload: buildAxiosServerResponse(newAlertNotifications),
      });
    });

    expect(screen.queryAllByText('Alert notification', { exact: false })).toHaveLength(2);
  });

  it('should not expand when receiving the same notifications', () => {
    store.dispatch({
      type: GET_ALERT_NOTIFICATIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAlertNotifications),
    });

    render(
      <Provider store={store}>
        <AlertNotification />
      </Provider>,
    );

    expect(screen.getByText('Key Alarm Notifications (1)'));

    fireEvent.click(screen.getByTitle('Collapse'));
    expect(screen.queryAllByText('Alert notification 1')).toHaveLength(0);

    const newAlertNotifications: AlertNotificationModel[] = [
      ...mockAlertNotifications,
    ];

    act(() => {
      store.dispatch({
        type: GET_ALERT_NOTIFICATIONS_FULFILLED,
        payload: buildAxiosServerResponse(newAlertNotifications),
      });
    });

    expect(screen.queryAllByText('Alert notification', { exact: false })).toHaveLength(0);
  });

  it('should start polling for notifications', () => {
    const mockStoreCreator = configureMockStore([thunk]);
    const mockStore = mockStoreCreator(store.getState());

    render(
      <Provider store={mockStore}>
        <AlertNotification />
      </Provider>,
    );

    expect(mockStore.getActions()).toEqual([{
      type: POLL_ALERT_NOTIFICATIONS,
      payload: {
        deliveryFunctionId: 0,
        pollingTime: '30',
        alarmTypes: [
          'NEWIP',
          'CCBUFUSAGE',
          'IRIBUFUSAGE',
          'AUDITDISC',
          'DYNIPLKPFAILURE',
        ],
        top: '10',
        orderBy: 'utcDateTime desc',
      },
    }]);
  });
});
