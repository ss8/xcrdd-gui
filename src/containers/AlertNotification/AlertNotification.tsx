import React, {
  FunctionComponent,
  ReactElement,
  useEffect,
  useRef,
  useState,
} from 'react';
import isEqual from 'lodash.isequal';
import { Icon, Intent } from '@blueprintjs/core';
import { connect, ConnectedProps } from 'react-redux';
import Draggable from 'react-draggable';
import { RootState } from 'data/root.reducers';
import * as globalSelectors from 'global/global-selectors';
import * as alertNotificationSelectors from 'data/alertNotification/alertNotification.selectors';
import * as alertNotificationActions from 'data/alertNotification/alertNotification.actions';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import { AlertNotificationModel } from 'data/alertNotification/alertNotification.types';
import AlertNotificationCollapseExpand from './components/AlertNotificationCollapseExpand';
import './alert-notification.scss';
import { getAlarmTypes } from './utils/getAlarmTypes';

const mapState = (state: RootState) => ({
  isAuthenticated: authenticationSelectors.isAuthenticated(state),
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  alertNotifications: alertNotificationSelectors.getAlertNotifications(state),
  isAlertNotificationEnabled: discoverySelectors.isAlertNotificationEnabled(state),
  alertNotificationRowNumbers: discoverySelectors.getAlertNotificationRowNumbers(state),
  alertNotificationPollingTime: discoverySelectors.getAlertNotificationPollingTime(state),
  isAlertNotificationNewIPEnabled: discoverySelectors.isAlertNotificationNewIPEnabled(state),
  isAlertNotificationBufferingEnabled: discoverySelectors.isAlertNotificationBufferingEnabled(state),
  isAlertNotificationAuditDiscrepancyEnabled: discoverySelectors.isAlertNotificationAuditDiscrepancyEnabled(state),
  isAlertNotificationInterceptDynamicIPLookupEnabled:
    discoverySelectors.isAlertNotificationInterceptDynamicIPLookupEnabled(state),
});

const mapDispatch = {
  pollAlertNotifications: alertNotificationActions.pollAlertNotifications,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

export type Props = PropsFromRedux;

export const AlertNotification: FunctionComponent<Props> = ({
  isAuthenticated,
  deliveryFunctionId,
  alertNotifications,
  isAlertNotificationEnabled,
  alertNotificationRowNumbers,
  alertNotificationPollingTime,
  isAlertNotificationNewIPEnabled,
  isAlertNotificationBufferingEnabled,
  isAlertNotificationAuditDiscrepancyEnabled,
  isAlertNotificationInterceptDynamicIPLookupEnabled,
  pollAlertNotifications,
}: Props): ReactElement | null => {
  const [collapsed, setCollapsed] = useState(false);
  const previousAlertNotifications = useRef<AlertNotificationModel[]>();

  useEffect(() => {
    if (isAlertNotificationEnabled) {
      const alarmTypes = getAlarmTypes(
        isAlertNotificationNewIPEnabled,
        isAlertNotificationBufferingEnabled,
        isAlertNotificationAuditDiscrepancyEnabled,
        isAlertNotificationInterceptDynamicIPLookupEnabled,
      );
      if (alarmTypes.length > 0) {
        pollAlertNotifications(
          deliveryFunctionId,
          alertNotificationPollingTime,
          alarmTypes,
          alertNotificationRowNumbers,
          'utcDateTime desc',
        );
      }
    }
  }, [
    isAlertNotificationEnabled,
    alertNotificationRowNumbers,
    isAlertNotificationNewIPEnabled,
    isAlertNotificationBufferingEnabled,
    isAlertNotificationAuditDiscrepancyEnabled,
    isAlertNotificationInterceptDynamicIPLookupEnabled,
    deliveryFunctionId,
    alertNotificationPollingTime,
    pollAlertNotifications,
  ]);

  useEffect(() => {
    if (!isEqual(previousAlertNotifications.current, alertNotifications)) {
      previousAlertNotifications.current = alertNotifications;
      setCollapsed(false);
    }
  }, [
    alertNotifications,
  ]);

  const onCollapseExpandClick = () => setCollapsed(!collapsed);

  const onMouseDown = (event: React.MouseEvent<HTMLElement, MouseEvent>) => event.stopPropagation();

  const renderAlarms = () => {
    if (collapsed) {
      return null;
    }

    return alertNotifications.map(({ id, message }) => (
      <div key={id} className="alert-notification-item">{message}</div>
    ));
  };

  if (!isAuthenticated || alertNotifications.length === 0 || !isAlertNotificationEnabled) {
    return null;
  }

  return (
    <Draggable>
      <div className="alert-notification" data-testid="AlertNotification">
        <div className="bp3-callout bp3-intent-danger bp3-callout-icon">
          <Icon icon="notifications" iconSize={20} intent={Intent.DANGER} />
          <h4 className="bp3-heading">{`Key Alarm Notifications (${alertNotifications.length})`}</h4>
          <AlertNotificationCollapseExpand collapsed={collapsed} onClick={onCollapseExpandClick} />
          <div className="alert-notification-content" onMouseDown={onMouseDown} role="presentation">
            {renderAlarms()}
          </div>
        </div>
      </div>
    </Draggable>
  );
};

export default (connector(AlertNotification));
