import LIIntercept from 'xcrdd/dataSources/containers/liIntercept';
import Location from 'xcrdd/dataSources/containers/location';
import DashboardModule from './dashboard';
import UsersModule from './users';
import TeamsModule from './teams';
import RolesModule from './roles';
import PoliciesModule from './policies';
import AuditModule from './audit/general-audit';
import UserAuditModule from './audit/user-audit';
import DeliveryFunc from './xcipio/dfs';
import AccessFunction from './routes/AccessFunction';
import CollectionFunction from './routes/CollectionFunction';
import Distributor from './routes/Distributor';
import TlsProxy from './routes/TlsProxy';
import InterceptsModule from './xcipio/intercepts';
import WarrantsModule from './xcipio/warrants';
import CasesModule from './xcipio/cases';
import WiretapsModule from './xcipio/wiretaps';
import ContactsModule from './xcipio/contacts';
import ExternalApps from './components/external-apps';
import XCRDDHome from './xcrdd/home';
import ConnectionStatus from './xcrdd/connectionStatus';
import KPIModule from './xcrdd/KPI';
import LogsModule from './xcrdd/logs';
import XCRDDSettings from './xcrdd/settings';
import LEA from './xcrdd/LEA';

import {
  CF_ROUTE,
  AF_ROUTE,
  SYSTEM_SETTINGS_ROUTE,
  SECURITY_GATEWAY_ROUTE,
} from './constants';

// eslint-disable-next-line import/prefer-default-export
export const Projects = [
  {
    path: '/dashboard',
    component: DashboardModule,
    permissions: 'dashboard',
  },
  {
    path: '/warrants*',
    component: WarrantsModule,
    permissions: 'warrants',
  },
  {
    path: '/users*',
    component: UsersModule,
    permissions: 'users',
  },
  {
    path: '/teams*',
    component: TeamsModule,
    permissions: 'teams',
  },
  {
    path: '/roles*',
    component: RolesModule,
    permissions: 'roles',
  },
  {
    path: '/policies*',
    component: PoliciesModule,
    permissions: 'polices',
  },
  {
    path: '/audit*',
    component: AuditModule,
    permissions: 'general',
  },
  {
    path: '/userAudit*',
    component: UserAuditModule,
    permissions: 'userAudit',
  },
  {
    path: '/dfs*',
    component: DeliveryFunc,
    permissions: 'dfs',
  },
  {
    path: '/intercepts*',
    component: InterceptsModule,
    permissions: 'intercepts',
  },
  {
    path: '/cases*',
    component: CasesModule,
    permissions: 'cases',
  },
  {
    path: '/wiretaps*',
    component: WiretapsModule,
    permissions: 'wiretaps',
  },
  {
    path: '/contacts*',
    component: ContactsModule,
    permissions: 'contacts',
  },
  {
    path: `${AF_ROUTE}/xms`,
    component: ExternalApps,
    permissions: 'xms_af',
  },
  {
    path: `${AF_ROUTE}*`,
    component: AccessFunction,
    permissions: 'afs',
  },
  {
    path: `${CF_ROUTE}*`,
    component: CollectionFunction,
    permissions: 'cfs',
  },
  {
    path: '/xms/reports*',
    component: ExternalApps,
    permissions: 'xms_reports',
  },
  {
    path: '/xms/alarm*',
    component: ExternalApps,
    permissions: 'xms_alarm',
  },
  {
    path: '/xms/monitoring*',
    component: ExternalApps,
    permissions: 'xms_monitoring',
  },
  {
    path: '/xms/systemhealth*',
    component: ExternalApps,
    permissions: 'xms_system_health',
  },
  {
    path: '/xms/sysconfhome*',
    component: ExternalApps,
    permissions: 'xms_system_conf',
  },
  {
    path: `${SYSTEM_SETTINGS_ROUTE}/xms`,
    component: ExternalApps,
    permissions: 'xms_system_conf',
  },
  {
    path: `${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}*`,
    component: TlsProxy,
    permissions: 'securityGateway',
  },
  {
    path: `${SYSTEM_SETTINGS_ROUTE}*`,
    component: Distributor,
    permissions: 'ss8_distributors',
  },
  {
    path: '/external/dashboard*',
    component: ExternalApps,
    permissions: 'external-dashboard',
  },
  {
    path: '/xcrdd/internalsystems',
    component: ConnectionStatus,
    permissions: 'logs',
  },
  {
    path: '/xcrdd/externalsystems',
    component: ConnectionStatus,
    permissions: 'logs',
  },
  {
    path: '/xcrdd/logs',
    component: LogsModule,
    permissions: 'logs',
  },
  {
    path: '/xcrdd/kpi',
    component: KPIModule,
    permissions: 'kpi',
  },
  {
    path: '/xcrdd/liintercept',
    component: LIIntercept,
    permissions: 'ss8_distributors',
  },
  {
    path: '/xcrdd/location',
    component: Location,
    permissions: 'ss8_distributors',
  },
  {
    path: '/xcrdd/lea*',
    component: LEA,
    permissions: 'ss8_distributors',
  },
  {
    path: '/xcrdd/settings',
    component: XCRDDSettings,
    permissions: 'ss8_distributors',
  },
  {
    path: '/xcrdd',
    component: XCRDDHome,
    permissions: 'home',
  },
];
