import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getAllByTestId,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { FormTemplate } from 'data/template/template.types';
import { getByDataTest } from '__test__/customQueries';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockNodes } from '__test__/mockNodes';
import TlsProxyForm from './TlsProxyForm';
import { tlsProxyTemplate } from '../../templates';
import { TlsProxyFormRef } from '.';
import { adaptTemplateOptions } from '../../utils/tlsProxyTemplate.adapter';

describe('<TlsProxyForm />', () => {
  let tlsProxy: TlsProxy;
  let adaptedTemplate: FormTemplate;

  beforeEach(() => {
    tlsProxy = {
      name: '',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        ipAddress: '',
        port: '',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '128',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        ipAddress: '',
        port: '',
        requiredState: 'ACTIVE',
        keepaliveInterval: '60',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
      }],
    };

    adaptedTemplate = adaptTemplateOptions(
      tlsProxyTemplate,
      mockSecurityInfos,
      mockNodes,
      ['instanceId1', 'instanceId2'],
      {
        listeners: {
          keepaliveTimeout: 300,
          maxConcurrentConnection: 128,
          requiredState: 'ACTIVE',
          routeRule: 'SRCADDR',
          transport: 'TCP',
        },
        destinations: {
          keepaliveInterval: 60,
          keepaliveRetries: 6,
          numOfConnections: 1,
          transport: 'TCP',
        },
      },
    );
  });

  it('should render the title and Cancel/Save button if in edit mode', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByText('Form title')).toBeVisible();
    expect(screen.queryAllByText('Save')).toHaveLength(2);
    expect(screen.queryAllByText('Cancel')).toHaveLength(2);
  });

  it('should render the title and Edit/Back button if not in edit mode', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing={false}
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickEdit={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByText('Form title')).toBeVisible();
    expect(screen.queryAllByText('Edit')).toHaveLength(2);
    expect(screen.queryAllByText('Back')).toHaveLength(2);
  });

  it('should not render add/remove Listener/Destination if not in edit mode', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing={false}
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should call the button callback when clicking on them if in edit mode', () => {
    const saveHandler = jest.fn();
    const cancelHandler = jest.fn();
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={saveHandler}
        onClickCancelClose={cancelHandler}
      />,
    );

    fireEvent.click(screen.queryAllByText('Save')[0]);
    expect(saveHandler).toBeCalled();

    fireEvent.click(screen.queryAllByText('Cancel')[0]);
    expect(cancelHandler).toBeCalled();
  });

  it('should call the button callback when clicking on them if not in edit mode', () => {
    const editHandler = jest.fn();
    const closeHandler = jest.fn();
    render(
      <TlsProxyForm
        title="Form title"
        isEditing={false}
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickEdit={editHandler}
        onClickCancelClose={closeHandler}
      />,
    );

    fireEvent.click(screen.queryAllByText('Edit')[0]);
    expect(editHandler).toBeCalled();

    fireEvent.click(screen.queryAllByText('Back')[0]);
    expect(closeHandler).toBeCalled();
  });

  it('should render the name, listeners and destinations', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByText('Security Gateway Name:')).toBeVisible();
    expect(screen.getByText('Xcipio Node:')).toBeVisible();
    expect(screen.getByText('Instance ID:')).toBeVisible();

    expect(screen.getByTestId('MultiTabFormSectionListeners')).toBeVisible();
    expect(screen.getByText('Listener 1')).toBeVisible();

    expect(screen.getByTestId('MultiTabFormSectionDestinations')).toBeVisible();
    expect(screen.getByText('Destination 1')).toBeVisible();
  });

  it('should call onFieldUpdate if a field is changed', () => {
    const updateHandler = jest.fn();
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'Name 1' } });
    expect(updateHandler).toBeCalledWith({
      label: 'Security Gateway Name',
      name: 'name',
      value: 'Name 1',
      valueLabel: 'Name 1',
    });
  });

  it('should render the Listener form as expected', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Route Rule:')).toBeVisible();
  });

  it('should render the new Listener form as expected', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(listeners, 'CollapsibleFormButton')[1]);

    expect(screen.getByText('Listener 2')).toBeVisible();

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Route Rule:')).toBeVisible();
  });

  it('should render the Destination form as expected', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Number Of Connections:')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
  });

  it('should render the new Destination form as expected', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(destinations, 'CollapsibleFormButton')[1]);

    expect(screen.getByText('Destination 2')).toBeVisible();

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Number Of Connections:')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
  });

  it('should render the Number of Connections as disabled', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));
    const tab = getByRole(destinations, 'tabpanel');

    expect(getByDataTest(tab, 'input-numOfConnections')).toBeDisabled();
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return false in isValid if form has duplicated ip:port and display error message', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    const { container } = render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });
    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '1' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });

    await waitFor(() => expect(screen.getByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')).toBeVisible());
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(2);
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    const { container } = render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });
    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '2' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should return the form values in getValues for multiple listeners and destinations', async () => {
    const formRef = createRef<TlsProxyFormRef>();

    const { container } = render(
      <TlsProxyForm
        ref={formRef}
        title="Form title"
        isEditing
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getAllByTestId(listeners, 'CollapsibleFormButton')[0]);

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-requiredState'), { target: { value: 'INACTIVE' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-keepaliveTimeout'), { target: { value: '100' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-maxConcurrentConnection'), { target: { value: '110' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-secInfoId'), { target: { value: 'Tk9ORQ' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-transport'), { target: { value: 'UDP' } });

    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));
    expect(screen.getByText('Listener 2')).toBeVisible();
    fireEvent.click(getAllByTestId(listeners, 'CollapsibleFormButton')[1]);

    const listenerTab2 = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab2.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab2.querySelector('input[name="port"]') as Element, { target: { value: '11' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getAllByTestId(destinations, 'CollapsibleFormButton')[0]);

    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(destinationTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '2.2.2.2' } });
    fireEvent.change(destinationTab.querySelector('input[name="port"]') as Element, { target: { value: '2' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-requiredState'), { target: { value: 'INACTIVE' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-keepaliveInterval'), { target: { value: '20' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-transport'), { target: { value: 'ZPUSH' } });

    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));
    expect(screen.getByText('Destination 2')).toBeVisible();
    fireEvent.click(getAllByTestId(destinations, 'CollapsibleFormButton')[1]);

    const destinationTab2 = getByRole(destinations, 'tabpanel');
    fireEvent.change(destinationTab2.querySelector('input[name="ipAddress"]') as Element, { target: { value: '2.2.2.2' } });
    fireEvent.change(destinationTab2.querySelector('input[name="port"]') as Element, { target: { value: '22' } });

    expect(formRef.current?.getValues()).toEqual({
      name: 'Name 1',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: '',
        ipAddress: '1.1.1.1',
        port: '1',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '100',
        maxConcurrentConnection: '110',
        secInfoId: 'Tk9ORQ',
        transport: 'UDP',
        routeRule: 'SRCADDR',
      }, {
        id: '',
        ipAddress: '1.1.1.1',
        port: '11',
        requiredState: 'ACTIVE',
        keepaliveTimeout: 300,
        maxConcurrentConnection: 128,
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: '',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'INACTIVE',
        keepaliveInterval: '20',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'ZPUSH',
        keepaliveRetries: 6,
      }, {
        id: '',
        ipAddress: '2.2.2.2',
        port: '22',
        requiredState: 'ACTIVE',
        keepaliveInterval: 60,
        numOfConnections: 1,
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        keepaliveRetries: 6,
      }],
    });
  });

  it('should not render add and remove button in view mode', () => {
    render(
      <TlsProxyForm
        title="Form title"
        isEditing={false}
        data={tlsProxy}
        template={adaptedTemplate}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });
});
