import React, {
  forwardRef,
  MutableRefObject,
  RefObject,
  useCallback,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import { FormTemplate, FormTemplateSection, FormTemplateValidator } from 'data/template/template.types';
import * as types from 'data/tlsProxy/tlsProxy.types';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import Form, {
  CustomValidationFunction,
  FieldUpdateHandlerParam,
} from 'shared/form';
import { showError } from 'shared/toaster';
import { validateIpPortCombinations, validateCurrentIpPortCombination } from 'utils/validators/ipPortCombinationValidators';
import MultiTabFormSection from '../MultiTabFormSection';

type Props = {
  title: string,
  isEditing: boolean,
  template: FormTemplate,
  data: Partial<types.TlsProxy>
  onClickSave?: () => void
  onClickEdit?: () => void
  onClickCancelClose: () => void
  onFieldUpdate?: (param: FieldUpdateHandlerParam, formSection?:string) => void
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => types.TlsProxy
}

export const TlsProxyForm = forwardRef<Ref, Props>(({
  title,
  isEditing,
  template,
  data,
  onClickSave,
  onClickEdit,
  onClickCancelClose,
  onFieldUpdate,
}: Props, ref) => {
  const mainRef = useRef<Form>(null);
  const listenerRefs = useRef<(RefObject<Form>)[]>([]);
  const destinationRefs = useRef<(RefObject<Form>)[]>([]);
  const [invalidListenerTabs, setInvalidListenerTabs] = useState<boolean[]>([]);
  const [invalidDestinationTabs, setInvalidDestinationTabs] = useState<boolean[]>([]);

  const getValidTabs = (
    tabFormRefs: MutableRefObject<RefObject<Form>[]>,
    forceValidation = false,
    forceTouched = true,
  ) => {
    const validFormTabs = tabFormRefs.current
      .filter((formRef) => !!formRef.current)
      .map((formRef) => formRef.current?.isValid(forceValidation, forceTouched) ?? true);

    return validFormTabs;
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isMainValid = mainRef.current?.isValid(forceValidation, forceTouched) ?? true;

    const validListeners = getValidTabs(listenerRefs, forceValidation, forceTouched);
    const allListenersValid = validListeners.every((valid) => valid === true);
    setInvalidListenerTabs(validListeners.map((entry) => !entry));

    const validDestinations = getValidTabs(destinationRefs, forceValidation, forceTouched);
    const allDestinationsValid = validDestinations.every((entry) => entry === true);
    setInvalidDestinationTabs(validDestinations.map((entry) => !entry));

    const { listeners, destinations } = getValues();
    const isIpPortCombinationsValid = validateIpPortCombinations(listeners, destinations);
    if (!isIpPortCombinationsValid) {
      showError('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.');
    }

    return isMainValid && allListenersValid && allDestinationsValid && isIpPortCombinationsValid;
  };

  const getValues = () => {
    const formValues: types.TlsProxy = {
      ...mainRef.current?.getValues(),
      listeners: listenerRefs.current
        .filter((listenerRef) => !!listenerRef.current)
        .map((listenerRef) => listenerRef.current?.getValues()),
      destinations: destinationRefs.current
        .filter((destinationRef) => !!destinationRef.current)
        .map((destinationRef) => destinationRef.current?.getValues()),
    };

    return formValues;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const customValidator = useCallback<CustomValidationFunction>((
    _propFields?: unknown,
    _stateFields?: unknown,
    _value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
    form?: Form | undefined,
  ) => {
    const { listeners, destinations } = getValues();

    const valid = validateCurrentIpPortCombination(form?.getValues(), listeners, destinations);
    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  }, []);

  const createListenerTabComponent = useCallback((tabData: Partial<types.TlsProxyListener> = {}) => {
    const listenerRef: { current: Form | null } = {
      current: null,
    };

    listenerRefs.current.push(listenerRef);

    const refCallback = (node: Form | null) => {
      listenerRef.current = node;
    };

    const customValidationFunctionMap = {
      ipPortCombinationValidator: customValidator,
    };

    const handleFieldUpdate = (param: FieldUpdateHandlerParam) => {
      if (onFieldUpdate) {
        onFieldUpdate(param, 'listeners');
      }
    };

    return (
      <Form
        ref={refCallback}
        name={template.key}
        defaults={tabData}
        fields={(template.listeners as FormTemplateSection).fields}
        layout={(template.listeners as FormTemplateSection).metadata.layout?.rows}
        collapsibleLayout={(template.listeners as FormTemplateSection).metadata.layout?.collapsible}
        fieldUpdateHandler={handleFieldUpdate}
        customValidationFunctionMap={customValidationFunctionMap}
      />
    );
  }, [template, onFieldUpdate, customValidator]);

  const createDestinationTabComponent = useCallback((tabData: Partial<types.TlsProxyDestination> = {}) => {
    const destinationRef: { current: Form | null } = {
      current: null,
    };

    destinationRefs.current.push(destinationRef);

    const refCallback = (node: Form | null) => {
      destinationRef.current = node;
    };

    const customValidationFunctionMap = {
      ipPortCombinationValidator: customValidator,
    };

    return (
      <Form
        ref={refCallback}
        name={template.key}
        defaults={tabData}
        fields={(template.destinations as FormTemplateSection).fields}
        layout={(template.destinations as FormTemplateSection).metadata.layout?.rows}
        collapsibleLayout={(template.destinations as FormTemplateSection).metadata.layout?.collapsible}
        fieldUpdateHandler={onFieldUpdate}
        customValidationFunctionMap={customValidationFunctionMap}
      />
    );
  }, [template, onFieldUpdate, customValidator]);

  const renderFormPanel = (formTitle: string | null = null) => (
    <FormPanel title={formTitle}>
      <FormButtons
        isEditing={isEditing}
        onClickCancel={onClickCancelClose}
        onClickSave={onClickSave}
        onClickEdit={onClickEdit}
        onClickClose={onClickCancelClose}
      />
    </FormPanel>
  );

  return (
    <FormLayout>
      <FormLayoutContent>
        {renderFormPanel(title)}
        <Form
          ref={mainRef}
          name={template.key}
          defaults={data}
          fields={(template.main as FormTemplateSection).fields}
          layout={(template.main as FormTemplateSection).metadata.layout?.rows}
          fieldUpdateHandler={onFieldUpdate}
        />
        <MultiTabFormSection
          tabContainerId="ListenersTab"
          title="Listeners"
          tabTitle="Listener"
          data={data.listeners}
          invalidTabs={invalidListenerTabs}
          isEditing={isEditing}
          createTabPanel={createListenerTabComponent}
        />
        <MultiTabFormSection
          tabContainerId="DestinationsTab"
          title="Destinations"
          tabTitle="Destination"
          data={data.destinations}
          invalidTabs={invalidDestinationTabs}
          isEditing={isEditing}
          createTabPanel={createDestinationTabComponent}
        />
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel()}
      </FormLayoutFooter>
    </FormLayout>
  );
});

export default TlsProxyForm;
