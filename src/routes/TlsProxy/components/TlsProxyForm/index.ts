import { Ref } from './TlsProxyForm';

export { default } from './TlsProxyForm';
export type TlsProxyFormRef = Ref;
