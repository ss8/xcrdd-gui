import React from 'react';
import {
  fireEvent,
  getByTestId,
  render,
  screen,
} from '@testing-library/react';

import MultiTabFormSection from './MultiTabFormSection';

describe('<MultiTabFormSection />', () => {
  let createTabPanel: (data?: string, tabId?: number) => JSX.Element;

  beforeEach(() => {
    createTabPanel = (data?: string, _tabId?: number) => {
      return <span>{data || 'new data'}</span>;
    };
  });

  it('should have the data-testid', () => {
    render(
      <MultiTabFormSection
        title="TestId"
        tabTitle="title"
        isEditing
        data={['data 1']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.getByTestId('MultiTabFormSectionTestId')).toBeVisible();
  });

  it('should show the form section title', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.getByText('MultiTabFormSectionTitle')).toBeVisible();
  });

  it('should not show the remove icon if only one tab', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.queryByText('MultiTabRemoveIcon')).toBeNull();
  });

  it('should not show the add icon if not in editing mode', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing={false}
        data={['data 1']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.queryByText('MultiTabAddButton')).toBeNull();
  });

  it('should initialize the tabs with corresponding tab title and content', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 3')).toBeVisible();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 1'));
    expect(screen.getByText('data 1')).toBeVisible();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 2'));
    expect(screen.getByText('data 2')).toBeVisible();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 3'));
    expect(screen.getByText('data 3')).toBeVisible();
  });

  it('should add a new tab when clicking in the plus button', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    fireEvent.click(screen.getByTestId('MultiTabAddButton'));

    expect(screen.getByText('MultiTabFormSectionTabTitle 4')).toBeVisible();
    expect(screen.getByText('new data')).toBeVisible();
  });

  it('should remove the first tab when clicking in the remove button for it', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    const tab = screen.getByTestId('MultiTabMultiTabFormSectionTabTitle 1');
    fireEvent.click(getByTestId(tab, 'MultiTabRemoveIcon'));

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.queryByText('MultiTabFormSectionTabTitle 3')).toBeNull();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 1'));
    expect(screen.getByText('data 2')).toBeVisible();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 2'));
    expect(screen.getByText('data 3')).toBeVisible();
  });

  it('should remove the last tab when clicking in the remove button for it', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    const tab = screen.getByTestId('MultiTabMultiTabFormSectionTabTitle 3');
    fireEvent.click(getByTestId(tab, 'MultiTabRemoveIcon'));

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.queryByText('MultiTabFormSectionTabTitle 3')).toBeNull();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 1'));
    expect(screen.getByText('data 1')).toBeVisible();

    fireEvent.click(screen.getByText('MultiTabFormSectionTabTitle 2'));
    expect(screen.getByText('data 2')).toBeVisible();
  });

  it('should limit the number of tabs that can be created', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        maxNumberOfTabs={5}
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    fireEvent.click(screen.getByTestId('MultiTabAddButton'));
    fireEvent.click(screen.getByTestId('MultiTabAddButton'));

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 3')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 4')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 5')).toBeVisible();

    expect(screen.queryByTestId('MultiTabAddButton')).toBeNull();
  });

  it('should limit the number of tabs that can be deleted', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        minNumberOfTabs={2}
        maxNumberOfTabs={5}
        data={['data 1', 'data 2', 'data 3']}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(3);

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 3')).toBeVisible();

    fireEvent.click(screen.getAllByTestId('MultiTabRemoveIcon')[0]);

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should render the tab as invalid if tab is marked as invalid', () => {
    render(
      <MultiTabFormSection
        title="MultiTabFormSectionTitle"
        tabTitle="MultiTabFormSectionTabTitle"
        isEditing
        data={['data 1', 'data 2', 'data 3']}
        invalidTabs={[false, true]}
        createTabPanel={createTabPanel}
        tabContainerId=""
      />,
    );

    expect(screen.getByText('MultiTabFormSectionTabTitle 1')).toBeVisible();
    const tab1 = screen.getByTestId('MultiTabMultiTabFormSectionTabTitle 1');
    expect(tab1.querySelector('.multi-tab-has-error')).toBeNull();

    expect(screen.getByText('MultiTabFormSectionTabTitle 2')).toBeVisible();
    const tab3 = screen.getByTestId('MultiTabMultiTabFormSectionTabTitle 2');
    expect(tab3.querySelector('.multi-tab-has-error')).toBeTruthy();

    expect(screen.getByText('MultiTabFormSectionTabTitle 3')).toBeVisible();
    const tab2 = screen.getByTestId('MultiTabMultiTabFormSectionTabTitle 1');
    expect(tab2.querySelector('.multi-tab-has-error')).toBeNull();
  });
});
