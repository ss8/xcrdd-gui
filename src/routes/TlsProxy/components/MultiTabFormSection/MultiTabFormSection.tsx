import cloneDeep from 'lodash.clonedeep';
import React, {
  useState,
  useEffect,
  useCallback,
} from 'react';
import MultiTab, { TabProp } from 'shared/multi-tab/MultiTab';
import './multi-tab-form-section.scss';

type Props<T> = {
  tabContainerId: string
  title: string,
  tabTitle: string,
  isEditing: boolean,
  data: T[] | undefined,
  invalidTabs?: boolean[],
  minNumberOfTabs?: number,
  maxNumberOfTabs?: number,
  createTabPanel: (data?: T, tabId?: number) => JSX.Element,
};

let nextTabId = 0;

const MultiTabFormSection = <T extends unknown>({
  tabContainerId,
  title,
  tabTitle,
  minNumberOfTabs = 1,
  maxNumberOfTabs = 10,
  isEditing,
  data,
  invalidTabs,
  createTabPanel,
}: Props<T>): JSX.Element => {
  const [tabs, setTabs] = useState<TabProp[]>([]);

  const createTab = useCallback((entry: T | undefined, index: number) => {
    nextTabId += 1;
    return {
      id: nextTabId,
      title: `${tabTitle} ${index + 1}`,
      panel: createTabPanel(entry, nextTabId),
    };
  }, [tabTitle, createTabPanel]);

  useEffect(() => {
    if (data) {
      const newTabs = data.map(createTab);
      setTabs(newTabs);
    }
  }, [data, createTab]);

  useEffect(() => {
    if (data && invalidTabs && invalidTabs.length > 0) {
      setTabs((tabEntries) => {
        const clonedTabs = cloneDeep(tabEntries);

        clonedTabs.forEach((tab, index) => {
          tab.hasErrors = !!invalidTabs?.[index] ?? false;
        });

        return clonedTabs;
      });
    }
  }, [data, invalidTabs]);

  const handleRemoveTab = (tabId: number | string) => {
    let clonedTabs = cloneDeep(tabs);
    clonedTabs = clonedTabs.filter(({ id }) => id !== tabId);
    clonedTabs = clonedTabs.map((tab, index) => ({
      ...tab,
      title: `${tabTitle} ${index + 1}`,
    }));
    setTabs(clonedTabs);
  };

  const handleAddTab = () => {
    const newTab = createTab(undefined, tabs.length);
    setTabs([
      ...tabs,
      newTab,
    ]);
  };

  return (
    <div className="multi-tab-form-section" data-testid={`MultiTabFormSection${title}`}>
      <h3>{title}</h3>
      <MultiTab
        tabContainerId={tabContainerId}
        tabs={tabs}
        addTab={handleAddTab}
        removeTab={handleRemoveTab}
        minNumberOfTabs={minNumberOfTabs}
        maxNumberOfTabs={maxNumberOfTabs}
        isEditing={isEditing}
      />
    </div>
  );
};

export default MultiTabFormSection;
