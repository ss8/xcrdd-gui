import { FormTemplate } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export const tlsProxyTemplate: FormTemplate = {
  version: '1.0.0',
  name: 'TLS Proxy',
  key: 'tlsProxy',
  main: {
    metadata: {
      layout: {
        rows: [
          [
            'name',
            'nodeName',
            'instanceId',
            '$empty',
          ],
        ],
      },
    },
    fields: {
      name: {
        type: 'text',
        label: 'Security Gateway Name',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^[A-Za-z0-9\\s\\-_\\.~!@#$%^*+?/]{1,64}$',
              errorMessage: 'Must only include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter.',
            },
            {
              type: 'length',
              length: 64,
            },
          ],
        },
      },
      nodeName: {
        label: 'Xcipio Node',
        type: 'select',
        initial: '',
        options: [
          {
            label: 'Select an Xcipio node',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
      instanceId: {
        label: 'Instance ID',
        type: 'select',
        initial: '',
        options: [
          {
            label: 'Select an instance id',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
    },
  },
  listeners: {
    metadata: {
      layout: {
        rows: [
          [
            'id',
          ],
          [
            'ipAddress',
            'port',
            'requiredState',
            '$empty',
          ],
        ],
        collapsible: {
          label: 'Advanced Settings',
          rows: [
            [
              'keepaliveTimeout',
              'maxConcurrentConnection',
              'secInfoId',
              'transport',
            ],
            [
              'routeRule',
              '$empty',
              '$empty',
              '$empty',
            ],
          ],
        },
      },
    },
    fields: {
      id: {
        type: 'text',
        initial: '',
        hide: 'true',
      },
      ipAddress: {
        label: 'Listening IP Address',
        type: 'textSuggest',
        placeholder: 'Type or select IP Address...',
        options: [],
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: 'IP address and Port combination not unique. Please review.',
            },
          ],
        },
      },
      port: {
        label: 'Listening Port',
        type: 'textSuggest',
        placeholder: 'Type or select IP Port...',
        options: [],
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'Operator',
              operator: '>=',
              fieldName: '$value',
              value: 0,
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
            {
              type: 'Operator',
              operator: '<=',
              fieldName: '$value',
              value: 65535,
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
            {
              type: 'triggerValidationOnAnotherField',
              theOtherField: 'ipAddress',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: '',
            },
          ],
        },
      },
      requiredState: {
        label: 'Configured State',
        type: 'select',
        initial: 'ACTIVE',
        options: [
          {
            label: 'Active',
            value: 'ACTIVE',
          },
          {
            label: 'Inactive',
            value: 'INACTIVE',
          },
        ],
      },
      keepaliveTimeout: {
        label: 'Keepalive Timeout (Sec)',
        type: 'text',
        initial: '300',
        validation: {
          required: true,
          validators: [
            {
              type: 'Operator',
              operator: '>=',
              fieldName: '$value',
              value: 0,
              errorMessage: 'Must be between 0 - 4096. Please re-enter.',
            },
            {
              type: 'Operator',
              operator: '<=',
              fieldName: '$value',
              value: 4096,
              errorMessage: 'Must be between 0 - 4096. Please re-enter.',
            },
          ],
        },
      },
      maxConcurrentConnection: {
        label: 'Maximum Concurrent Connections',
        type: 'text',
        initial: '128',
        validation: {
          required: true,
          validators: [
            {
              type: 'Operator',
              operator: '>=',
              fieldName: '$value',
              value: 0,
              errorMessage: 'Must be between 0 - 131070. Please re-enter.',
            },
            {
              type: 'Operator',
              operator: '<=',
              fieldName: '$value',
              value: 131070,
              errorMessage: 'Must be between 0 - 131070. Please re-enter.',
            },
          ],
        },
      },
      secInfoId: {
        label: 'TLS Profile (Security Info ID)',
        type: 'select',
        initial: 'Tk9ORQ',
        options: [{
          label: 'None',
          value: 'Tk9ORQ',
        }],
      },
      transport: {
        label: 'Transport',
        type: 'select',
        initial: 'TCP',
        options: [
          {
            label: 'TCP',
            value: 'TCP',
          },
          {
            label: 'UDP',
            value: 'UDP',
          },
        ],
      },
      routeRule: {
        label: 'Route Rule',
        type: 'select',
        initial: 'SRCADDR',
        readOnly: true,
        options: [
          {
            label: 'Based on Source Address (default)',
            value: 'SRCADDR',
          },
        ],
      },
    },
  },
  destinations: {
    metadata: {
      layout: {
        rows: [
          [
            'id',
          ],
          [
            'ipAddress',
            'port',
            'requiredState',
            '$empty',
          ],
        ],
        collapsible: {
          label: 'Advanced Settings',
          rows: [
            [
              'secInfoId',
            ],
            [
              'keepaliveInterval',
              'numOfConnections',
              'transport',
              '$empty',
            ],
          ],
        },
      },
    },
    fields: {
      id: {
        type: 'text',
        initial: '',
        hide: 'true',
      },
      ipAddress: {
        label: 'Destination IP Address',
        type: 'text',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: 'IP address and Port combination not unique. Please review.',
            },
          ],
        },
      },
      port: {
        label: 'Destination Port',
        type: 'text',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'Operator',
              operator: '>=',
              fieldName: '$value',
              value: 0,
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
            {
              type: 'Operator',
              operator: '<=',
              fieldName: '$value',
              value: 65535,
              errorMessage: 'Must be between 0 - 65535. Please re-enter.',
            },
            {
              type: 'triggerValidationOnAnotherField',
              theOtherField: 'ipAddress',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: '',
            },
          ],
        },
      },
      requiredState: {
        label: 'Configured State',
        type: 'select',
        initial: 'ACTIVE',
        options: [
          {
            label: 'Active',
            value: 'ACTIVE',
          },
          {
            label: 'Inactive',
            value: 'INACTIVE',
          },
        ],
      },
      keepaliveInterval: {
        label: 'Keepalive Interval (Sec)',
        type: 'text',
        initial: '60',
        validation: {
          required: true,
          validators: [
            {
              type: 'Operator',
              operator: '>=',
              fieldName: '$value',
              value: 1,
              errorMessage: 'Must be between 1 - 240. Please re-enter.',
            },
            {
              type: 'Operator',
              operator: '<=',
              fieldName: '$value',
              value: 240,
              errorMessage: 'Must be between 1 - 240. Please re-enter.',
            },
          ],
        },
      },
      keepaliveRetries: {
        label: 'Keepalive Interval (Sec)',
        type: 'text',
        initial: '6',
        hide: 'true',
        readOnly: true,
      },
      numOfConnections: {
        label: 'Number Of Connections',
        type: 'text',
        initial: '1',
        readOnly: true,
      },
      secInfoId: {
        type: 'text',
        hide: 'true',
        initial: 'Tk9ORQ',
      },
      transport: {
        label: 'Transport',
        type: 'select',
        initial: 'TCP',
        options: [
          {
            label: 'TCP',
            value: 'TCP',
          },
          {
            label: 'ZPUSH',
            value: 'ZPUSH',
          },
        ],
      },
    },
  },
};
