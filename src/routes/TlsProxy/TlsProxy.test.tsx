import React from 'react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { render, screen, waitFor } from '@testing-library/react';
import MockAdapter from 'axios-mock-adapter';
import store from 'data/store';
import * as types from 'data/tlsProxy/tlsProxy.types';
import Http from 'shared/http';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import TlsProxy from './TlsProxy';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<TlsProxy />', () => {
  beforeEach(() => {
    store.dispatch({
      type: types.SELECT_TLS_PROXY,
      payload: [{ id: 'ID_TO_GET' }],
    });

    axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0/ID_TO_GET`).reply(200, { data: mockTlsProxies });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should render the list for the /systemSettings/securityGateway path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('TlsProxyList')).toBeVisible();
  });

  it('should render the create for the /systemSettings/securityGateway/create path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/create`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('TlsProxyCreate')).toBeVisible();
  });

  it('should render the edit for the /systemSettings/securityGateway/edit path', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/edit`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('TlsProxyEdit')).toBeVisible());
  });

  it('should render the view for the /systemSettings/securityGateway/view path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/view`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('TlsProxyView')).toBeVisible();
  });

  it('should render the copy for the /systemSettings/securityGateway/copy path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/copy`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('TlsProxyCopy')).toBeVisible();
  });
});
