import { ColGroupDef } from '@ag-grid-enterprise/all-modules';
import { COMMON_GRID_COLUMN_FIELD_NAMES } from 'shared/commonGrid/grid-enums';
import { getFieldFromFirstInterfaceNameValueGetter } from 'utils/valueGetters';
import { TlsProxy, TlsProxyListener, TlsProxyDestination } from 'data/tlsProxy/tlsProxy.types';

export enum TlsProxyMenuActionsType {
  Copy = 'copy',
  Delete = 'delete',
  Edit = 'edit',
  View = 'view'
}

export const tlsProxyColumnDefs: ColGroupDef[] = [
  {
    headerName: '',
    children: [
      {
        headerName: '',
        field: COMMON_GRID_COLUMN_FIELD_NAMES.Action,
        cellRenderer: 'actionMenuCellRenderer',
      },
      {
        headerName: 'Security Gateway Name',
        field: 'name',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'name',
        flex: 1,
        minWidth: 259,
      },
      {
        headerName: 'Xcipio Node',
        field: 'nodeName',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'nodeName',
        flex: 1,
        minWidth: 150,
      },
      {
        headerName: 'Instance ID',
        field: 'instanceId',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'instanceId',
        flex: 1,
        minWidth: 150,
      },
    ],
  },
  {
    headerName: 'Listener 1',
    children: [
      {
        headerName: 'IP Address',
        field: 'ipAddress',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'ipAddress',
        flex: 1,
        minWidth: 218,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyListener>(
          'listeners',
        ),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyListener>('listeners'),
      },
      {
        headerName: 'Port',
        field: 'port',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'port',
        flex: 1,
        minWidth: 191,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyListener>(
          'listeners',
        ),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyListener>('listeners'),
      },
    ],
  },
  {
    headerName: 'Destination 1',
    children: [
      {
        headerName: 'IP Address',
        field: 'ipAddress',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'ipAddress',
        flex: 1,
        minWidth: 218,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyDestination>(
          'destinations',
        ),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyDestination>('destinations'),
      },
      {
        headerName: 'Port',
        field: 'port',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'port',
        flex: 1,
        minWidth: 218,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyDestination>(
          'destinations',
        ),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<TlsProxy, TlsProxyDestination>('destinations'),
      },
    ],
  },
];
