import React, { FunctionComponent, Fragment } from 'react';
import { Route } from 'react-router-dom';
import TlsProxyList from './containers/TlsProxyList';
import TlsProxyCreate from './containers/TlsProxyCreate';
import TlsProxyEdit from './containers/TlsProxyEdit';
import TlsProxyView from './containers/TlsProxyView';
import TlsProxyCopy from './containers/TlsProxyCopy';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../constants';

interface Props {
}

const TlsProxy: FunctionComponent<Props> = () => {
  return (
    <Fragment>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`}>
        <TlsProxyList />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/create`}>
        <TlsProxyCreate />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/edit`}>
        <TlsProxyEdit />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/view`}>
        <TlsProxyView />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/copy`}>
        <TlsProxyCopy />
      </Route>
    </Fragment>
  );
};

export default TlsProxy;
