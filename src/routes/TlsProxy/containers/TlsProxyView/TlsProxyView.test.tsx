import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import {
  fireEvent,
  getByDisplayValue,
  getByRole,
  getByTestId,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as types from 'data/tlsProxy/tlsProxy.types';
import AuthActions from 'data/authentication/authentication.types';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { mockNodes } from '__test__/mockNodes';
import { getByDataTest } from '__test__/customQueries';
import { buildAxiosServerResponse } from '__test__/utils';
import TlsProxyView from './TlsProxyView';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../../../constants';
import TlsProxy from '../../TlsProxy';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<TlsProxyView />', () => {
  let tlsProxy: types.TlsProxy;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['tp_access'],
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.SELECT_TLS_PROXY,
      payload: [{ id: 'ID_TO_GET' }],
    });

    tlsProxy = {
      id: 'ID_TO_GET',
      name: 'Name to view',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: '1',
        ipAddress: '1.1.1.1',
        port: '1111',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '100',
        maxConcurrentConnection: '110',
        secInfoId: 'Tk9ORQ',
        transport: 'UDP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: '2',
        ipAddress: '2.2.2.2',
        port: '2222',
        requiredState: 'ACTIVE',
        keepaliveInterval: '20',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'ZPUSH',
      }],
    };

    axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0/ID_TO_GET`).reply(200, { data: [tlsProxy] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/ss8ProxyDefaults/0').reply(200, { data: mockSs8ProxyDefaults });
    axiosMock.onAny().reply(200, { data: mockSecurityInfos });
  });

  it('should render the component with data-testid', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('TlsProxyView')).toBeVisible());
  });

  it('should render the form title with Edit and Back buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyView />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('View Security Gateway')).toBeVisible();
    let items = screen.getAllByText('Edit');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Back');
    expect(items).toHaveLength(2);
  });

  it('should render the list of tls proxies when clicking in the back button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/view`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Back')[0]);
    expect(screen.getByTestId('TlsProxyList')).toBeVisible();
  });

  it('should render the edit view when clicking in the edit button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/view`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Edit')[0]);
    await waitFor(() => expect(screen.getByTestId('TlsProxyEdit')).toBeVisible());
  });

  it('should load the data retrieved from the server', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Security Gateway')).toBeVisible());

    expect((getByDataTest(container, 'input-name') as HTMLInputElement).value).toEqual('Name to view');
    expect(getByDisplayValue(container, 'node-1')).toBeVisible();
    expect(getByDisplayValue(container, 'instanceId1')).toBeVisible();

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));
    expect((listenerTab.querySelector('input[name="ipAddress"]') as HTMLInputElement).value).toEqual('1.1.1.1');
    expect((listenerTab.querySelector('input[name="port"]') as HTMLInputElement).value).toEqual('1111');
    expect(getByDisplayValue(listenerTab, 'Inactive')).toBeVisible();
    expect((getByDataTest(listenerTab, 'input-keepaliveTimeout') as HTMLInputElement).value).toEqual('100');
    expect((getByDataTest(listenerTab, 'input-maxConcurrentConnection') as HTMLInputElement).value).toEqual('110');
    expect(getByDisplayValue(listenerTab, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'UDP')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'Based on Source Address (default)')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));
    expect((getByDataTest(destinationTab, 'input-ipAddress') as HTMLInputElement).value).toEqual('2.2.2.2');
    expect((getByDataTest(destinationTab, 'input-port') as HTMLInputElement).value).toEqual('2222');
    expect(getByDisplayValue(destinationTab, 'Active')).toBeVisible();
    expect((getByDataTest(destinationTab, 'input-keepaliveInterval') as HTMLInputElement).value).toEqual('20');
    expect((getByDataTest(destinationTab, 'input-numOfConnections') as HTMLInputElement).value).toEqual('1');
    expect(getByDisplayValue(destinationTab, 'ZPUSH')).toBeVisible();
  });

  it('should display the fields as read only', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Security Gateway')).toBeVisible());

    expect(getByDataTest(container, 'input-name')).toBeDisabled();
    expect(getByDataTest(container, 'input-nodeName')).toBeDisabled();
    expect(getByDataTest(container, 'input-instanceId')).toBeDisabled();

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));
    expect((listenerTab.querySelector('input[name="ipAddress"]') as HTMLInputElement)).toBeDisabled();
    expect((listenerTab.querySelector('input[name="port"]') as HTMLInputElement)).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-requiredState')).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-keepaliveTimeout')).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-maxConcurrentConnection')).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-secInfoId')).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-transport')).toBeDisabled();
    expect(getByDataTest(listenerTab, 'input-routeRule')).toBeDisabled();

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));
    expect(getByDataTest(destinationTab, 'input-ipAddress')).toBeDisabled();
    expect(getByDataTest(destinationTab, 'input-port')).toBeDisabled();
    expect(getByDataTest(destinationTab, 'input-requiredState')).toBeDisabled();
    expect(getByDataTest(destinationTab, 'input-keepaliveInterval')).toBeDisabled();
    expect(getByDataTest(destinationTab, 'input-numOfConnections')).toBeDisabled();
    expect(getByDataTest(destinationTab, 'input-transport')).toBeDisabled();
  });
});
