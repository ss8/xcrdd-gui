import React, {
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { FormTemplate } from 'data/template/template.types';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import * as actions from 'data/tlsProxy/tlsProxy.actions';
import * as selectors from 'data/tlsProxy/tlsProxy.selectors';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as nodeActions from 'data/nodes/nodes.actions';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import * as ss8ProxyDefaultSelectors from 'data/ss8ProxyDefault/ss8ProxyDefault.selectors';
import * as ss8ProxyDefaultActions from 'data/ss8ProxyDefault/ss8ProxyDefault.actions';
import { getSelectedDeliveryFunctionId } from 'global/global-selectors';
import TlsProxyForm, { TlsProxyFormRef } from '../../components/TlsProxyForm';
import { tlsProxyTemplate } from '../../templates';
import { adaptTemplateOptions, adaptTemplateVisibilityMode } from '../../utils/tlsProxyTemplate.adapter';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: getSelectedDeliveryFunctionId(state),
  selectedTlsProxy: selectors.getSelectedTlsProxy(state),
  tlsProxy: selectors.getTlsProxy(state),
  filteredSecurityInfos: securityInfoSelectors.getSecurityInfosFilteredByTlsAndDtls(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  instanceIds: ss8ProxyDefaultSelectors.getSs8ProxyDefaultInstanceIds(state),
  ss8ProxyDefaults: ss8ProxyDefaultSelectors.getSs8ProxyDefaultsGlobalDefaultValues(state),
});

const mapDispatch = {
  fetchTlsProxy: actions.fetchTlsProxy,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getNodes: nodeActions.getNodes,
  getSs8ProxyDefaults: ss8ProxyDefaultActions.getSs8ProxyDefaults,
  goToTlsProxyEdit: actions.goToTlsProxyEdit,
  goToTlsProxyList: actions.goToTlsProxyList,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter;

export const TlsProxyView: FunctionComponent<Props> = ({
  deliveryFunctionId,
  selectedTlsProxy,
  tlsProxy,
  filteredSecurityInfos,
  history,
  xcipioNodes,
  instanceIds,
  ss8ProxyDefaults,
  fetchTlsProxy,
  getSecurityInfos,
  getNodes,
  getSs8ProxyDefaults,
  goToTlsProxyEdit,
  goToTlsProxyList,
}: Props) => {
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<TlsProxy>>({});
  const formRef = useRef<TlsProxyFormRef>(null);
  const tlsProxyId = selectedTlsProxy[0]?.id;

  useEffect(() => {
    if (tlsProxyId) {
      fetchTlsProxy(deliveryFunctionId, tlsProxyId);
    }
  }, [tlsProxyId, deliveryFunctionId, fetchTlsProxy]);

  useEffect(() => {
    getSecurityInfos(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSs8ProxyDefaults(deliveryFunctionId);
  }, [deliveryFunctionId, getSecurityInfos, getNodes, getSs8ProxyDefaults]);

  useEffect(() => {
    if (tlsProxy != null) {
      let adaptedTemplate = adaptTemplateOptions(
        tlsProxyTemplate,
        filteredSecurityInfos,
        xcipioNodes,
        instanceIds,
        ss8ProxyDefaults,
      );
      adaptedTemplate = adaptTemplateVisibilityMode(adaptedTemplate, 'view');

      const formData = cloneDeep(tlsProxy);

      setData(formData);
      setTemplate(adaptedTemplate);
    }
  }, [tlsProxy, filteredSecurityInfos, xcipioNodes, instanceIds]);

  const handleEdit = () => goToTlsProxyEdit(history);

  const handleClose = () => goToTlsProxyList(history);

  if (!template || !data) {
    return null;
  }

  return (
    <div className="app-container-content" data-testid="TlsProxyView">
      <TlsProxyForm
        ref={formRef}
        title="View Security Gateway"
        isEditing={false}
        data={data}
        template={template}
        onClickEdit={handleEdit}
        onClickCancelClose={handleClose}
      />
    </div>
  );
};

export default withRouter(connector(TlsProxyView));
