import React, {
  Fragment, FunctionComponent, useEffect, useState,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { SelectionChangedEvent } from '@ag-grid-enterprise/all-modules';
import { AppToaster } from 'shared/toaster';
import { RootState } from 'data/root.reducers';
import * as actions from 'data/tlsProxy/tlsProxy.actions';
import * as selectors from 'data/tlsProxy/tlsProxy.selectors';
import * as userActions from 'users/users-actions';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import { TLS_PROXY_USER_CATEGORY_SETTINGS, TlsProxyPrivileges } from 'data/tlsProxy/tlsProxy.types';
import { GridSettings, WithAuditDetails } from 'data/types';
import { TlsProxyMenuActionsType } from 'routes/TlsProxy/TlsProxy.types';
import ModalDialog from 'shared/modal/dialog';
import TlsProxyListGrid from './components/TlsProxyListGrid';
import { tlsProxyColumnDefs } from '../../TlsProxy.types';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  beforeLogout: state.authentication.beforeLogout,
  data: selectors.getTlsProxyList(state),
  deliveryFunctionId: state.global.selectedDF,
  privileges: state.authentication.privileges,
  selectedTlsProxy: selectors.getSelectedTlsProxy(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
});

const mapDispatch = {
  deleteTlsProxy: actions.deleteTlsProxy,
  fetchTlsProxyListWithView: actions.fetchTlsProxyListWithView,
  getGridSettings: userActions.getUserSettings,
  saveGridSettings: userActions.putUserSettings,
  goToTlsProxyCopy: actions.goToTlsProxyCopy,
  goToTlsProxyCreate: actions.goToTlsProxyCreate,
  goToTlsProxyEdit: actions.goToTlsProxyEdit,
  goToTlsProxyView: actions.goToTlsProxyView,
  selectTlsProxy: actions.selectTlsProxy,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter

export const TlsProxyList: FunctionComponent<Props> = ({
  beforeLogout,
  data,
  deliveryFunctionId,
  history,
  privileges,
  selectedTlsProxy,
  userName,
  deleteTlsProxy,
  fetchTlsProxyListWithView,
  getGridSettings,
  goToTlsProxyCopy,
  goToTlsProxyCreate,
  goToTlsProxyEdit,
  goToTlsProxyView,
  saveGridSettings,
  selectTlsProxy,
}: Props) => {

  const [lastUpdatedAt, setLastUpdatedAt] = useState<Date>();
  const [isConfirmingTlsProxy, setConfirmingTlsProxy] = useState(false);
  const [handleSubmitConfirmActionDialog, setHandleSubmitConfirmActionDialog] = useState<CallableFunction>(() => null);
  const [confirmActionDialogCustomComponent, setConfirmActionDialogCustomComponent] =
    useState<FunctionComponent>(() => null);
  const [confirmActionDialogTitle, setConfirmActionDialogTitle] = useState('');
  const [confirmActionDialogActionText, setConfirmActionDialogActionText] = useState('');

  useEffect(() => {
    AppToaster.clear();
    fetchTlsProxyListWithView(deliveryFunctionId);
    return () => {
      AppToaster.clear();
    };
  }, [deliveryFunctionId, fetchTlsProxyListWithView]);

  useEffect(() => {
    setLastUpdatedAt(new Date());
  }, [data]);

  const handleGetGridSettings = () => getGridSettings(TLS_PROXY_USER_CATEGORY_SETTINGS, userName);

  const handleCreateNew = () => goToTlsProxyCreate(history);

  const handleRefresh = () => fetchTlsProxyListWithView(deliveryFunctionId);

  const handleSelection = (event: SelectionChangedEvent) => selectTlsProxy(event.api.getSelectedRows());

  const handleSaveGridSettings = (gridSettings: GridSettings) =>
    saveGridSettings(TLS_PROXY_USER_CATEGORY_SETTINGS, userName, gridSettings);

  const handleDeleteTlsProxy = () => {
    const tlsProxy = selectedTlsProxy[0];
    if (tlsProxy == null || tlsProxy.id == null) {
      return;
    }

    const tlsProxyBody: WithAuditDetails<void> = { data: undefined };

    deleteTlsProxy(deliveryFunctionId, tlsProxy.id, tlsProxyBody);
  };

  const handleShowActionMenu = () => true;

  const handleCancelConfirmActionDialog = () => setConfirmingTlsProxy(false);

  const handleSelectItemMenuAction = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const { name } = selectedTlsProxy[0];

    if (event?.type === TlsProxyMenuActionsType.Delete) {
      setConfirmingTlsProxy(true);
      setConfirmActionDialogTitle('Delete Access Function');
      setConfirmActionDialogCustomComponent(() => () => (
        <Fragment>
          <span>Are you sure you want to delete Security Gateway </span> &#39;{name}&#39;?
        </Fragment>
      ));
      setConfirmActionDialogActionText('Yes, delete it');
      setHandleSubmitConfirmActionDialog(() => handleDeleteTlsProxy);
    } else if (event?.type === TlsProxyMenuActionsType.Edit) {
      goToTlsProxyEdit(history);
    } else if (event?.type === TlsProxyMenuActionsType.View) {
      goToTlsProxyView(history);
    } else if (event?.type === TlsProxyMenuActionsType.Copy) {
      goToTlsProxyCopy(history);
    }
  };

  // TODO remove "|| true" in integration phase because right now we don't have those information
  const hasPrivilegesToCreateAndEdit = privileges.includes(TlsProxyPrivileges.Edit) || true;
  const hasPrivilegeToDelete = privileges.includes(TlsProxyPrivileges.Delete) || true;
  const hasPrivilegeToView = privileges.includes(TlsProxyPrivileges.View) || true;

  return (
    <div data-testid="TlsProxyList" style={{ height: 'inherit' }}>
      <TlsProxyListGrid
        hasPrivilegesToCreate={hasPrivilegesToCreateAndEdit}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegeToView={hasPrivilegeToView}
        data={data}
        beforeLogout={beforeLogout}
        lastUpdatedAt={lastUpdatedAt}
        columnDefs={tlsProxyColumnDefs}
        onRefresh={handleRefresh}
        onCreateNew={handleCreateNew}
        onSelectionChanged={handleSelection}
        onSaveGridSettings={handleSaveGridSettings}
        onGetGridSettings={handleGetGridSettings}
        onSelectItemMenuAction={handleSelectItemMenuAction}
        onShowActionMenuItem={handleShowActionMenu}
      />
      <ModalDialog
        width="500px"
        isOpen={isConfirmingTlsProxy}
        title={confirmActionDialogTitle}
        customComponent={confirmActionDialogCustomComponent}
        actionText={confirmActionDialogActionText}
        onSubmit={handleSubmitConfirmActionDialog}
        onClose={handleCancelConfirmActionDialog}
      />
    </div>
  );
};

export default withRouter(connector(TlsProxyList));
