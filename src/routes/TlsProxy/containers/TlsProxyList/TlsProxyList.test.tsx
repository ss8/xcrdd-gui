import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import {
  fireEvent, render, screen, RenderResult,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as types from 'data/tlsProxy/tlsProxy.types';
import { buildServerResponse, buildAxiosServerResponse } from '__test__/utils';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { mockNodes } from '__test__/mockNodes';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import AuthActions from 'data/authentication/authentication.types';
import TlsProxy from 'routes/TlsProxy/TlsProxy';
import { UserSettings } from 'data/types';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../../../constants';
import TlsProxyList from './TlsProxyList';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);
const mockTlsProxy1: types.TlsProxy = {
  id: 'id1',
  name: 'TLS Name 1',
  nodeName: 'node-1',
  instanceId: 'instanceId1',
  listeners: [{
    id: '1',
    ipAddress: '11.11.11.11',
    port: '1111',
    requiredState: 'INACTIVE',
    keepaliveTimeout: '300',
    maxConcurrentConnection: '256',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
    routeRule: 'SRCADDR',
  }],
  destinations: [{
    id: '1',
    ipAddress: '22.22.22.22',
    port: '2222',
    requiredState: 'ACTIVE',
    keepaliveInterval: '60',
    numOfConnections: '5',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
  }],
};

const mockTlsProxy2: types.TlsProxy = {
  id: 'id2',
  name: 'TLS Name 2',
  nodeName: 'node-1',
  instanceId: 'instanceId2',
  listeners: [{
    id: '2',
    ipAddress: '22.22.22.22',
    port: '2222',
    requiredState: 'ACTIVE',
    keepaliveTimeout: '300',
    maxConcurrentConnection: '256',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
    routeRule: 'SRCADDR',
  }],
  destinations: [{
    id: '2',
    ipAddress: '22.22.22.22',
    port: '2222',
    requiredState: 'ACTIVE',
    keepaliveInterval: '60',
    numOfConnections: '5',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
  }],
};

const mockTlsProxy3: types.TlsProxy = {
  id: 'id3',
  name: 'TLS Name 3',
  nodeName: 'node-1',
  instanceId: 'instanceId1',
  listeners: [{
    id: '3',
    ipAddress: '33.33.33.33',
    port: '3333',
    requiredState: 'INACTIVE',
    keepaliveTimeout: '300',
    maxConcurrentConnection: '356',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
    routeRule: 'SRCADDR',
  }],
  destinations: [{
    id: '3',
    ipAddress: '33.33.33.33',
    port: '3333',
    requiredState: 'ACTIVE',
    keepaliveInterval: '60',
    numOfConnections: '5',
    secInfoId: 'Tk9ORQ',
    transport: 'TCP',
  }],
};

const userSettings: UserSettings = {
  dateUpdated: '2020-07-28T17:08:46.000+0000',
  id: 'id1',
  name: 'TlsProxySettings',
  setting: '[]',
  type: 'json',
  user: 'userId1',
};

describe('<TlsProxyList />', () => {

  beforeEach(() => {
    store.dispatch({
      type: types.SELECT_TLS_PROXY,
      payload: [{ id: mockTlsProxy1.id }],
    });

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['tp_access'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    axiosMock.onGet('/securityInfos/0').reply(200, buildServerResponse(mockSecurityInfos));
    axiosMock.onGet('/xcipioNodes/0').reply(200, buildServerResponse(mockNodes));
    axiosMock.onGet('/ss8ProxyDefaults/0').reply(200, buildServerResponse(mockSs8ProxyDefaults));
    axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0/${mockTlsProxy1.id}`).reply(200, buildServerResponse([mockTlsProxy1]));
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet('/distributorConfigs/0').reply(200, { data: mockDistributorsConfiguration });
  });

  describe('when server send a TLS list with and without data', () => {
    beforeEach(() => {
      const response = buildServerResponse([mockTlsProxy1, mockTlsProxy2]);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0?$view=listeners`).reply(200, response);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, response);
      axiosMock.onGet('/users/userId1/settings').reply(200, { data: [] });

      const userSettingsResponse = buildServerResponse([userSettings]);
      axiosMock.onGet(`/users/userId1/settings?name=${types.TLS_PROXY_USER_CATEGORY_SETTINGS}`).reply(200, userSettingsResponse);

    });

    it('should render the component with data-testid', async () => {
      const response = buildServerResponse([]);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, response);
      render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );

      expect(screen.getByTestId('TlsProxyList')).toBeVisible();
    });

    it('should render the empty list if no TLS Proxy', () => {
      const response = buildServerResponse([]);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0?$view=listeners`).reply(200, response);
      const wrapper = render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );
      expect(wrapper.getByText('No Rows To Show')).toBeVisible();
    });

    it('should render the list of TLS Proxies', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );

      expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(2);
    });
  });

  describe('when clicks on new refresh button', () => {
    it('should refresh the list of TLS Proxies', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );

      expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(2);

      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, buildServerResponse([mockTlsProxy1, mockTlsProxy2, mockTlsProxy3]));

      const firstMoreButton = container.querySelector('[icon="refresh"]')?.parentElement;
      fireEvent.click(firstMoreButton as Element);

      await new Promise((resolve) => setTimeout(resolve, 500));

      expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(3);
    });
  });

  describe('when click New Security Gateway action menu item from the list', () => {

    it('should be redirected to New Security Gateway page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`]}>
            <TlsProxy />
          </MemoryRouter>
        </Provider>,
      );

      await screen.findByText(mockTlsProxy1.name);

      fireEvent.click(screen.getByText('New Security Gateway'));

      expect(await screen.findByTestId('TlsProxyCreate')).toBeVisible();
    });
  });

  describe('when click Edit TLS action menu item from the list', () => {

    it('should be redirected to Edit page', async () => {

      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`]}>
            <TlsProxy />
          </MemoryRouter>
        </Provider>,
      );

      await screen.findByText(mockTlsProxy1.name);
      const moreButtons = await screen.findAllByTitle('View available actions');

      fireEvent.click(moreButtons[0] as HTMLElement);
      const editMenuItem = await screen.findByText('Edit');
      fireEvent.click(editMenuItem as HTMLElement);

      expect(await screen.findByTestId('TlsProxyEdit')).toBeVisible();
    });
  });

  describe('when click View TLS action menu item from the list', () => {

    it('should be redirected to View page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`]}>
            <TlsProxy />
          </MemoryRouter>
        </Provider>,
      );

      await screen.findByText(mockTlsProxy1.name);
      const moreButtons = await screen.findAllByTitle('View available actions');

      fireEvent.click(moreButtons[0] as HTMLElement);
      const editMenuItem = await screen.findByText('View');
      fireEvent.click(editMenuItem as HTMLElement);

      expect(await screen.findByTestId('TlsProxyView')).toBeVisible();
    });
  });

  describe('when click Copy TLS action menu item from the list', () => {

    it('should be redirected to Copy page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}`]}>
            <TlsProxy />
          </MemoryRouter>
        </Provider>,
      );

      await screen.findByText(mockTlsProxy1.name);
      const moreButtons = await screen.findAllByTitle('View available actions');

      fireEvent.click(moreButtons[0] as HTMLElement);
      const editMenuItem = await screen.findByText('Copy');
      fireEvent.click(editMenuItem as HTMLElement);

      expect(await screen.findByTestId('TlsProxyCopy')).toBeVisible();
    });
  });

  describe('Delete TLS Proxy feature - Failure operation', () => {
    let wrapper: RenderResult;
    let initialTlsList: types.TlsProxy[] = [];
    let finalTlsList: types.TlsProxy[] = [];

    beforeEach(async () => {

      initialTlsList = [mockTlsProxy1, mockTlsProxy2];
      finalTlsList = [mockTlsProxy2];

      const response = buildServerResponse(initialTlsList);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, response);

      wrapper = render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );

    });

    it('should select and tries delete a TLS Proxy from the list but fails and show an error message', async () => {
      // Test: Ensure TLS list is rendered according initial server state
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(initialTlsList.length);

      // Get first list item more button
      const firstMoreButton = (await screen.findAllByTitle('View available actions'))[0];

      // Act: if TLS grid has elements, click the first item more button
      fireEvent.click(firstMoreButton as Element);

      // Wait: Action Menu and select the Delete action menu option
      const deleteMenuButtonEl = await wrapper.findByText('Delete');

      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, buildServerResponse(finalTlsList));
      axiosMock.onDelete(`${types.API_PATH_TLS_PROXIES}/0/${mockTlsProxy1.id}`).reply(500, { data: [] });

      // Act: Click Delete option in the TLS item Action menu
      fireEvent.click(deleteMenuButtonEl as Element);

      // Act: when confirm delete dialog became rendered, click delete button
      fireEvent.click(screen.getByText('Yes, delete it'));

      // waits for the success feedback component
      const feedbackToastMsg = await wrapper.findAllByText(`Unable to delete ${mockTlsProxy1?.name}.`);

      // Success feedback component must be rendered
      expect(feedbackToastMsg?.length).toBe(1);
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(finalTlsList.length);
    });
  });

  describe('Delete TLS Proxy feature - Successful operation', () => {
    let wrapper: RenderResult;
    let initialTlsList: types.TlsProxy[] = [];
    let finalTlsList: types.TlsProxy[] = [];

    beforeEach(async () => {

      initialTlsList = [mockTlsProxy1, mockTlsProxy2];
      finalTlsList = [mockTlsProxy2];

      const response = buildServerResponse(initialTlsList);
      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, response);
      axiosMock.onDelete(`${types.API_PATH_TLS_PROXIES}/0/${mockTlsProxy1.id}`).reply(200, { data: [] });

      wrapper = render(
        <Provider store={store}>
          <MemoryRouter>
            <TlsProxyList />
          </MemoryRouter>
        </Provider>,
      );

    });

    it('should select and delete an TLS Proxy from the list', async () => {
      // Test: Ensure TLS list is rendered according initial server state
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(initialTlsList.length);

      // Get first list item more button
      const firstMoreButton = (await screen.findAllByTitle('View available actions'))[0];

      // Act: if TLS grid has elements, click the first item more button
      fireEvent.click(firstMoreButton as Element);

      // Wait: Action Menu and select the Delete action menu option
      const deleteMenuButtonEl = await wrapper.findByText('Delete');

      axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0`).reply(200, buildServerResponse(finalTlsList));

      // Act: Click Delete option in the TLS item Action menu
      fireEvent.click(deleteMenuButtonEl as Element);

      // Act: when confirm delete dialog became rendered, click delete button
      fireEvent.click(screen.getByText('Yes, delete it'));

      // waits for the success feedback component
      const feedbackToastMsg = await wrapper.findAllByText(`${mockTlsProxy1?.name} successfully deleted.`);

      // Success feedback component must be rendered
      expect(feedbackToastMsg?.length).toBe(1);
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(finalTlsList.length);
    });
  });
});
