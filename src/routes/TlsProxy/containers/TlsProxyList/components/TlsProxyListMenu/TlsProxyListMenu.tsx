import React, { FunctionComponent } from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { TlsProxyMenuActionsType } from 'routes/TlsProxy/TlsProxy.types';

interface Props {
  data: TlsProxy,
  disabled: boolean;
  context: {
    onActionMenuItemClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    props: {
      showActionMenuItem: boolean,
      hasPrivilegeToCreateNew: boolean;
      hasPrivilegeToDelete: boolean;
      hasPrivilegetoView: boolean;
    }
  };
}

const TlsProxyListMenu: FunctionComponent<Props> = ({
  data,
  disabled,
  context: {
    onActionMenuItemClick,
    props: {
      showActionMenuItem,
      hasPrivilegetoView,
      hasPrivilegeToCreateNew,
      hasPrivilegeToDelete,
    },
  },
}: Props) => {
  return (
    <Menu>
      {hasPrivilegetoView && showActionMenuItem && (
        <MenuItem
          id={TlsProxyMenuActionsType.View}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="View"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem && (
        <MenuItem
          id={TlsProxyMenuActionsType.Edit}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Edit"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem && (
        <MenuItem
          id={TlsProxyMenuActionsType.Copy}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Copy"
        />
      )}
      {hasPrivilegeToDelete && showActionMenuItem && (
        <MenuItem
          id={TlsProxyMenuActionsType.Delete}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Delete"
        />
      )}
    </Menu>
  );
};

export default TlsProxyListMenu;
