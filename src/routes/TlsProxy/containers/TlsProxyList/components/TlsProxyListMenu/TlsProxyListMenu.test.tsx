import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { MenuItem } from '@blueprintjs/core';
import { mockTlsProxies } from '__test__/mockTlsProxies';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import TlsProxyListMenu from './TlsProxyListMenu';

describe('<TlsProxyListMenu />', () => {
  const tlsProxy:TlsProxy = mockTlsProxies[0];

  describe('when user has Privilege to View, Edit, Delete', () => {
    let wrapper: ReactWrapper;
    const context = {
      onActionMenuItemClick: jest.fn(),
      props: {
        showActionMenuItem: true,
        hasPrivilegeToCreateNew: true,
        hasPrivilegeToDelete: true,
        hasPrivilegetoView: true,
      },
    };
    beforeEach(() => {
      wrapper = mount(<TlsProxyListMenu data={tlsProxy} disabled={false} context={context} />);
    });

    it('should render all TlsProxyListMenu items but', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(4);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Edit');
      expect(menuItems.at(2).text()).toBe('Copy');
      expect(menuItems.at(3).text()).toBe('Delete');
    });

    it('should click View menu item and call onActionMenuItemClick with View data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(0).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Edit menu item and call onActionMenuItemClick with Edit data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(1).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Copy menu item and call onActionMenuItemClick with Copy data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(2).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Delete menu item and call onActionMenuItemClick with Delete data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(3).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });
  });

  describe('when user has no View Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: true,
          hasPrivilegeToCreateNew: true,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: false,
        },
      };
      wrapper = mount(<TlsProxyListMenu data={tlsProxy} disabled={false} context={context} />);
    });

    it('should render all TlsProxyListMenu items but View', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(3);
      expect(menuItems.at(0).text()).toBe('Edit');
      expect(menuItems.at(1).text()).toBe('Copy');
      expect(menuItems.at(2).text()).toBe('Delete');
    });
  });

  describe('when user has no Create Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: true,
          hasPrivilegeToCreateNew: false,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<TlsProxyListMenu data={tlsProxy} disabled={false} context={context} />);
    });

    it('should render all TlsProxyListMenu items but Edit and copy', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(2);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Delete');
    });
  });

  describe('when user has no Delete Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: true,
          hasPrivilegeToCreateNew: true,
          hasPrivilegeToDelete: false,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<TlsProxyListMenu data={tlsProxy} disabled={false} context={context} />);
    });

    it('should render all TlsProxyListMenu items but Delete', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(3);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Edit');
      expect(menuItems.at(2).text()).toBe('Copy');
    });
  });

  describe('when showActionMenuItem is set as false', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: false,
          hasPrivilegeToCreateNew: false,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<TlsProxyListMenu data={tlsProxy} disabled={false} context={context} />);
    });

    it('should render any menu item', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(0);
    });
  });
});
