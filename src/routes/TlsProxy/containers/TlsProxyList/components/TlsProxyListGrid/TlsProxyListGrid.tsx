import React, { FunctionComponent } from 'react';
import { SelectionChangedEvent, ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import CommonGrid from 'shared/commonGrid';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import { GridSettings } from 'data/types';
import TlsProxyListMenu from '../TlsProxyListMenu';

interface Props {
  data: any[]
  hasPrivilegesToCreate: boolean
  hasPrivilegeToDelete: boolean
  hasPrivilegeToView: boolean
  beforeLogout: boolean,
  lastUpdatedAt: Date | undefined,
  columnDefs: (ColGroupDef | ColDef)[],
  onRefresh: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onCreateNew: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onGetGridSettings: () => any
  onSelectionChanged: (event: SelectionChangedEvent) => void
  onSaveGridSettings: (gridSettings: GridSettings) => void
  onSelectItemMenuAction: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  onShowActionMenuItem: (menuId: string, data: any[]) => void
}

const TlsProxyListGrid: FunctionComponent<Props> = ({
  hasPrivilegesToCreate,
  hasPrivilegeToDelete,
  hasPrivilegeToView,
  data = [],
  beforeLogout,
  lastUpdatedAt,
  columnDefs,
  onCreateNew,
  onRefresh,
  onSelectionChanged,
  onSaveGridSettings,
  onGetGridSettings,
  onShowActionMenuItem,
  onSelectItemMenuAction,
}: Props) => {
  const frameworkComponents = {
    actionMenuCellRenderer: ActionMenuCellRenderer,
  };

  return (
    <div className="grid-container" style={{ height: 'inherit' }}>
      <CommonGrid
        createNewText="New Security Gateway"
        pagination
        enableBrowserTooltips
        rowSelection={ROWSELECTION.SINGLE}
        hasPrivilegeToCreateNew={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegetoView={hasPrivilegeToView}
        rowData={data}
        noResults={false}
        isBeforeLogout={beforeLogout}
        columnDefs={columnDefs}
        frameworkComponents={frameworkComponents}
        actionMenuClass={TlsProxyListMenu}
        handleCreateNew={onCreateNew}
        onRefresh={onRefresh}
        onSelectionChanged={onSelectionChanged}
        showActionMenuItem={onShowActionMenuItem}
        onActionMenuItemClick={onSelectItemMenuAction}
        saveGridSettings={onSaveGridSettings}
        getGridSettingsFromServer={onGetGridSettings}
        lastUpdatedAt={lastUpdatedAt}
      />
    </div>
  );
};

export default TlsProxyListGrid;
