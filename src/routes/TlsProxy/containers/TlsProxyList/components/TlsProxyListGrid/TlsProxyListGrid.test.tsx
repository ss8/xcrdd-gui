import React from 'react';
import { Provider } from 'react-redux';
import {
  fireEvent, render, screen, RenderResult,
} from '@testing-library/react';
import Http from 'shared/http';
import MockAdapter from 'axios-mock-adapter';
import store from 'data/store';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { tlsProxyColumnDefs } from 'routes/TlsProxy/TlsProxy.types';
import TlsProxyListGrid from './TlsProxyListGrid';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const TLS_PROXY_MOCK: TlsProxy[] = [
  {
    id: 'id1',
    name: 'name 1',
    instanceId: '',
    nodeName: '',
    listeners: [
      {
        id: 'id1',
        ipAddress: '111.111.111.111',
        port: '1111',
        keepaliveTimeout: 'keepaliveTimeout1',
        maxConcurrentConnection: 'maxConcurrentConnection1',
        secInfoId: 'secInfoId1',
        transport: 'TCP',
        requiredState: 'ACTIVE',
        routeRule: 'INTERCEPT',
      },
      {
        id: 'id2',
        ipAddress: '222.222.222.222',
        port: '2222',
        keepaliveTimeout: 'keepaliveTimeout2',
        maxConcurrentConnection: 'maxConcurrentConnection2',
        secInfoId: 'secInfoId2',
        transport: 'TCP',
        requiredState: 'ACTIVE',
        routeRule: 'INTERCEPT',
      },
    ],
    destinations: [
      {
        id: 'idD1',
        ipAddress: '111.111.113',
        port: '3333',
        transport: 'TCP',
        keepaliveInterval: '',
        numOfConnections: '',
        requiredState: 'ACTIVE',
        secInfoId: '',
        keepaliveRetries: '',
      },
      {
        id: 'idD2',
        ipAddress: '111.111.114',
        port: '4444',
        transport: 'TCP',
        keepaliveInterval: '',
        numOfConnections: '',
        requiredState: 'ACTIVE',
        secInfoId: '',
        keepaliveRetries: '',
      },
    ],
  },
  {
    id: 'id2',
    name: 'name tls 2',
    instanceId: '',
    nodeName: '',
    listeners: [
      {
        id: 'id1',
        ipAddress: '111.111.111.222',
        port: '1122',
        keepaliveTimeout: 'keepaliveTimeout1',
        maxConcurrentConnection: 'maxConcurrentConnection1',
        secInfoId: 'secInfoId1',
        transport: 'TCP',
        requiredState: 'ACTIVE',
        routeRule: 'INTERCEPT',
      },
      {
        id: 'id2',
        ipAddress: '222.222.222.333',
        port: '2233',
        keepaliveTimeout: 'keepaliveTimeout2',
        maxConcurrentConnection: 'maxConcurrentConnection2',
        secInfoId: 'secInfoId2',
        transport: 'TCP',
        requiredState: 'ACTIVE',
        routeRule: 'INTERCEPT',
      },
    ],
    destinations: [
      {
        id: 'idD1',
        ipAddress: '333.333.444',
        port: '3344',
        transport: 'TCP',
        keepaliveInterval: '',
        numOfConnections: '',
        requiredState: 'ACTIVE',
        secInfoId: '',
      },
      {
        id: 'idD2',
        ipAddress: '111.111.115',
        port: '4455',
        transport: 'TCP',
        keepaliveInterval: '',
        numOfConnections: '',
        requiredState: 'ACTIVE',
        secInfoId: '',
      },
    ],
  },
];

describe('<TlsProxyListGrid />', () => {
  let userSettings: unknown;
  let tlsProxyList: TlsProxy[];

  beforeEach(() => {
    tlsProxyList = TLS_PROXY_MOCK;
    userSettings = {
      value: {
        data: {
          data: [
            {
              setting: '[]',
            },
          ],
        },
      },
    };
  });

  describe('When TLS list input has data', () => {
    let wrapper: RenderResult;

    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      wrapper = render(
        <Provider store={store}>
          <TlsProxyListGrid
            hasPrivilegesToCreate
            hasPrivilegeToDelete
            hasPrivilegeToView
            data={tlsProxyList}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={tlsProxyColumnDefs}
          />
        </Provider>,
      );

      axiosMock.onAny().reply(200);
    });

    it('should click on Create New button and dispatch a the create new function passed in the component', async () => {
      fireEvent.click(await screen.findByText('New Security Gateway'));
      expect(handleCreateNew).toHaveBeenCalled();
    });

    it('should click on Refresh button and dispatch a the refresh function passed in the component', async () => {
      const refreshButton = wrapper.container.querySelector('[icon="refresh"]');
      fireEvent.click(refreshButton as Element);
      expect(handleRefresh).toHaveBeenCalled();
    });

    it('should filter the TLS grid according the input passed', async () => {
      // ALL TLS before filtering
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(tlsProxyList.length);

      const filterInput = wrapper.container.querySelectorAll('.bp3-input')[0];

      const event = {
        target: {
          value: 'name tls 2',
        },
      } as React.ChangeEvent<HTMLInputElement>;

      fireEvent.change(filterInput, event);

      // some TLS after filtering
      expect(wrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(tlsProxyList.length - 1);
    });

    it('should render the last updated at', async () => {
      expect(await screen.findByText('Last updated at 08/27/20 07:13:39 AM')).toBeVisible();
    });
  });

  describe('When TLS list input has data and user DONT has privileges to create', () => {
    let wrapper: RenderResult;

    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      wrapper = render(
        <Provider store={store}>
          <TlsProxyListGrid
            hasPrivilegesToCreate={false}
            hasPrivilegeToDelete={false}
            hasPrivilegeToView
            data={tlsProxyList}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={tlsProxyColumnDefs}
          />
        </Provider>,
      );
    });

    it('should not render create new button and the first button in the interface should be the refresh', async () => {
      const firstInterfaceButton = wrapper.container.querySelectorAll('.grid-container .bp3-button')[0];

      fireEvent.click(firstInterfaceButton as Element);
      expect(handleCreateNew).not.toHaveBeenCalled();
      expect(handleRefresh).toHaveBeenCalled();
    });

    it('should still click on Refresh button and dispatch a the refresh function passed in the component', async () => {
      const firstMoreButton = wrapper.container.querySelector('[icon="refresh"]');
      fireEvent.click(firstMoreButton as Element);
      expect(handleRefresh).toHaveBeenCalled();
    });
  });

  describe('When TLS input has no data ', () => {
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    it('should render No Rows To Show message', async () => {
      render(
        <Provider store={store}>
          <TlsProxyListGrid
            hasPrivilegesToCreate
            hasPrivilegeToDelete
            hasPrivilegeToView
            data={[]}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={tlsProxyColumnDefs}
          />
        </Provider>,
      );

      expect(screen.getByText('No Rows To Show')).toBeVisible();
    });
  });

  describe('When perform click events on TLS list items ', () => {
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      render(
        <Provider store={store}>
          <TlsProxyListGrid
            hasPrivilegesToCreate
            hasPrivilegeToDelete
            hasPrivilegeToView
            data={TLS_PROXY_MOCK}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={tlsProxyColumnDefs}
          />
        </Provider>,
      );

      await screen.findByRole('grid');
    });

    it('should render the TLS_MOCK collection in the grid component (parameter "name")', async () => {
      expect((await screen.findByRole('grid')).querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(TLS_PROXY_MOCK.length);
      expect(await screen?.findByText(TLS_PROXY_MOCK[0].name)).toBeVisible();
      expect(await screen?.findByText(TLS_PROXY_MOCK[1].name)).toBeVisible();
    });
  });
});
