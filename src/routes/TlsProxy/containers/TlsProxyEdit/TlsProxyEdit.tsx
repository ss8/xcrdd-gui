import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { FormTemplate } from 'data/template/template.types';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import * as actions from 'data/tlsProxy/tlsProxy.actions';
import * as selectors from 'data/tlsProxy/tlsProxy.selectors';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as nodeActions from 'data/nodes/nodes.actions';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import * as ss8ProxyDefaultSelectors from 'data/ss8ProxyDefault/ss8ProxyDefault.selectors';
import * as ss8ProxyDefaultActions from 'data/ss8ProxyDefault/ss8ProxyDefault.actions';
import * as distributorConfigurationActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorConfigurationSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { getSelectedDeliveryFunctionId } from 'global/global-selectors';
import { FieldUpdateHandlerParam } from 'shared/form';
import TlsProxyForm, { TlsProxyFormRef } from '../../components/TlsProxyForm';
import { tlsProxyTemplate } from '../../templates';
import * as adapters from '../../utils/tlsProxyTemplate.adapter';
import getTlsProxyDataToSubmit from '../../utils/getTlsProxyDataToSubmit';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: getSelectedDeliveryFunctionId(state),
  selectedTlsProxy: selectors.getSelectedTlsProxy(state),
  tlsProxy: selectors.getTlsProxy(state),
  filteredSecurityInfos: securityInfoSelectors.getSecurityInfosFilteredByTlsAndDtls(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  instanceIds: ss8ProxyDefaultSelectors.getSs8ProxyDefaultInstanceIds(state),
  distributorConfigurations: distributorConfigurationSelectors.getDistributorConfigurations(state),
  ss8ProxyDefaults: ss8ProxyDefaultSelectors.getSs8ProxyDefaultsGlobalDefaultValues(state),
});

const mapDispatch = {
  fetchTlsProxy: actions.fetchTlsProxy,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getNodes: nodeActions.getNodes,
  getSs8ProxyDefaults: ss8ProxyDefaultActions.getSs8ProxyDefaults,
  updateTlsProxy: actions.updateTlsProxy,
  goToTlsProxyList: actions.goToTlsProxyList,
  getDistributorConfigurationsWithView: distributorConfigurationActions.getDistributorConfigurationsWithView,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter;

export const TlsProxyEdit: FunctionComponent<Props> = ({
  deliveryFunctionId,
  selectedTlsProxy,
  tlsProxy,
  filteredSecurityInfos,
  history,
  isAuditEnabled,
  userId,
  userName,
  xcipioNodes,
  instanceIds,
  distributorConfigurations,
  ss8ProxyDefaults,
  fetchTlsProxy,
  getSecurityInfos,
  getNodes,
  getSs8ProxyDefaults,
  updateTlsProxy,
  goToTlsProxyList,
  getDistributorConfigurationsWithView,
}: Props) => {
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<TlsProxy>>({});
  const formRef = useRef<TlsProxyFormRef>(null);
  const [
    nodeNamesIpAddressesPortsOptionsMap,
    setNodeNamesIpAddressesPortsOptionsMap,
  ] = useState<adapters.NodeNameIpAddressPortsOptionsMap>({});
  const tlsProxyId = selectedTlsProxy[0]?.id;

  useEffect(() => {
    if (tlsProxyId) {
      fetchTlsProxy(deliveryFunctionId, tlsProxyId);
    }
  }, [tlsProxyId, deliveryFunctionId, fetchTlsProxy]);

  useEffect(() => {
    getSecurityInfos(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSs8ProxyDefaults(deliveryFunctionId);
    getDistributorConfigurationsWithView(deliveryFunctionId);
  }, [deliveryFunctionId, getSecurityInfos, getNodes, getSs8ProxyDefaults, getDistributorConfigurationsWithView]);

  useEffect(() => {
    if (tlsProxy != null) {
      let adaptedTemplate = adapters.adaptTemplateOptions(
        tlsProxyTemplate,
        filteredSecurityInfos,
        xcipioNodes,
        instanceIds,
        ss8ProxyDefaults,
      );
      adaptedTemplate = adapters.adaptTemplateVisibilityMode(adaptedTemplate, 'edit');

      const formData = cloneDeep(tlsProxy);

      const newNodeNamesOptionsMap = adapters.adaptNodeNamesIpAddressesPortsOptionsMap(distributorConfigurations);

      adapters.updateListenersIpAddressOptions(adaptedTemplate, formData.nodeName, newNodeNamesOptionsMap);

      setNodeNamesIpAddressesPortsOptionsMap(newNodeNamesOptionsMap);
      setData(formData);
      setTemplate(adaptedTemplate);
    }
  }, [tlsProxy, filteredSecurityInfos, xcipioNodes, instanceIds, distributorConfigurations]);

  const handleClickSave = () => {
    if (!tlsProxyId) {
      return;
    }

    const isValid = formRef?.current?.isValid(true, true) ?? true;
    if (!isValid) {
      return;
    }

    const formData = formRef?.current?.getValues();
    if (!formData) {
      return;
    }

    const tlsProxyBody = getTlsProxyDataToSubmit(
      formData,
      tlsProxy,
      isAuditEnabled,
      userId,
      userName,
      'UPDATE',
    );

    updateTlsProxy(deliveryFunctionId, tlsProxyId, tlsProxyBody, history);
  };

  const handleCancel = () => {
    goToTlsProxyList(history);
  };

  const handleFieldUpdate = useCallback((param: FieldUpdateHandlerParam, formSection?:string) => {
    if (!template) {
      return;
    }
    const nodeName = formRef.current?.getValues().nodeName || '';
    const isEditingNodeName = adapters.isEditingNodeName(param.name);
    const isEditingListenersIpAddress = adapters.isEditingListenersIpAddress(param.name, formSection, nodeName);
    if (isEditingNodeName) {
      adapters.updateListenersIpAddressOptions(template, param.value, nodeNamesIpAddressesPortsOptionsMap);
    } else if (isEditingListenersIpAddress) {
      adapters.updateListenersPortOptions(template, param.value, nodeName, nodeNamesIpAddressesPortsOptionsMap);
    }
  }, [template, nodeNamesIpAddressesPortsOptionsMap, formRef]);

  if (!template || !data) {
    return null;
  }

  return (
    <div className="app-container-content" data-testid="TlsProxyEdit">
      <TlsProxyForm
        ref={formRef}
        title="Edit Security Gateway"
        isEditing
        data={data}
        template={template}
        onClickSave={handleClickSave}
        onClickCancelClose={handleCancel}
        onFieldUpdate={handleFieldUpdate}
      />
    </div>
  );
};

export default withRouter(connector(TlsProxyEdit));
