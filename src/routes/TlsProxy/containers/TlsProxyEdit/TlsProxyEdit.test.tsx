import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import {
  fireEvent,
  getByDisplayValue,
  getByRole,
  getByTestId,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as types from 'data/tlsProxy/tlsProxy.types';
import AuthActions from 'data/authentication/authentication.types';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { mockNodes } from '__test__/mockNodes';
import { getByDataTest } from '__test__/customQueries';
import { buildAxiosServerResponse } from '__test__/utils';
import TlsProxyEdit from './TlsProxyEdit';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../../../constants';
import TlsProxy from '../../TlsProxy';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<TlsProxyEdit />', () => {
  let tlsProxy: types.TlsProxy;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['tp_access'],
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.SELECT_TLS_PROXY,
      payload: [{ id: 'ID_TO_GET' }],
    });

    tlsProxy = {
      id: 'ID_TO_GET',
      name: 'Name to edit',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: '1',
        ipAddress: '1.1.1.1',
        port: '1111',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '100',
        maxConcurrentConnection: '110',
        secInfoId: 'Tk9ORQ',
        transport: 'UDP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: '2',
        ipAddress: '2.2.2.2',
        port: '2222',
        requiredState: 'ACTIVE',
        keepaliveInterval: '20',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'ZPUSH',
      }],
    };

    axiosMock.onGet(`${types.API_PATH_TLS_PROXIES}/0/ID_TO_GET`).reply(200, { data: [tlsProxy] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/ss8ProxyDefaults/0').reply(200, { data: mockSs8ProxyDefaults });
    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('TlsProxyEdit')).toBeVisible());
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('Edit Security Gateway')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of tls proxies when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/edit`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByTestId('TlsProxyList')).toBeVisible();
  });

  it('should load the data retrieved from the server', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Security Gateway')).toBeVisible());

    expect((getByDataTest(container, 'input-name') as HTMLInputElement).value).toEqual('Name to edit');
    expect((getByDataTest(container, 'input-nodeName') as HTMLInputElement).value).toEqual('node-1');
    expect((getByDataTest(container, 'input-instanceId') as HTMLInputElement).value).toEqual('instanceId1');

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));

    const listenerTab = getByRole(listeners, 'tabpanel');
    expect((listenerTab.querySelector('input[name="ipAddress"]') as HTMLInputElement).value).toEqual('1.1.1.1');
    expect((listenerTab.querySelector('input[name="port"]') as HTMLInputElement).value).toEqual('1111');
    expect(getByDisplayValue(listenerTab, 'Inactive')).toBeVisible();
    expect((getByDataTest(listenerTab, 'input-keepaliveTimeout') as HTMLInputElement).value).toEqual('100');
    expect((getByDataTest(listenerTab, 'input-maxConcurrentConnection') as HTMLInputElement).value).toEqual('110');
    expect(getByDisplayValue(listenerTab, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'UDP')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'Based on Source Address (default)')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));

    const destinationTab = getByRole(destinations, 'tabpanel');
    expect((getByDataTest(destinationTab, 'input-ipAddress') as HTMLInputElement).value).toEqual('2.2.2.2');
    expect((getByDataTest(destinationTab, 'input-port') as HTMLInputElement).value).toEqual('2222');
    expect(getByDisplayValue(destinationTab, 'Active')).toBeVisible();
    expect((getByDataTest(destinationTab, 'input-keepaliveInterval') as HTMLInputElement).value).toEqual('20');
    expect((getByDataTest(destinationTab, 'input-numOfConnections') as HTMLInputElement).value).toEqual('1');
    expect(getByDisplayValue(destinationTab, 'ZPUSH')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Name to edit')[0], { target: { value: '' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')[0]).toBeVisible();
  });

  it('should show an error message if duplicated ip:port', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '1111' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(screen.getByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')).toBeVisible();
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(2);
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should show success message on Save of a valid form', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Name to edit')[0], { target: { value: 'Name 1' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    await waitFor(() => expect(screen.getByText('Name 1 successfully updated.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Name to edit')[0], { target: { value: 'Name to fail' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    await waitFor(() => expect(screen.getByText('Unable to update Name to fail: Request failed with status code 500.')).toBeVisible());
  });
});
