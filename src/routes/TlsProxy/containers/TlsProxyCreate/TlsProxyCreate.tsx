import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { RootState } from 'data/root.reducers';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import * as factory from 'data/tlsProxy/tlsProxy.factory';
import * as actions from 'data/tlsProxy/tlsProxy.actions';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as nodeActions from 'data/nodes/nodes.actions';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import * as ss8ProxyDefaultSelectors from 'data/ss8ProxyDefault/ss8ProxyDefault.selectors';
import * as ss8ProxyDefaultActions from 'data/ss8ProxyDefault/ss8ProxyDefault.actions';
import * as distributorConfigurationActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorConfigurationSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { getSelectedDeliveryFunctionId } from 'global/global-selectors';
import { FieldUpdateHandlerParam } from 'shared/form';
import TlsProxyForm, { TlsProxyFormRef } from '../../components/TlsProxyForm';
import { tlsProxyTemplate } from '../../templates';
import * as adapters from '../../utils/tlsProxyTemplate.adapter';
import getTlsProxyDataToSubmit from '../../utils/getTlsProxyDataToSubmit';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: getSelectedDeliveryFunctionId(state),
  filteredSecurityInfos: securityInfoSelectors.getSecurityInfosFilteredByTlsAndDtls(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  instanceIds: ss8ProxyDefaultSelectors.getSs8ProxyDefaultInstanceIds(state),
  distributorConfigurations: distributorConfigurationSelectors.getDistributorConfigurations(state),
  ss8ProxyDefaults: ss8ProxyDefaultSelectors.getSs8ProxyDefaultsGlobalDefaultValues(state),
});

const mapDispatch = {
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getNodes: nodeActions.getNodes,
  getSs8ProxyDefaults: ss8ProxyDefaultActions.getSs8ProxyDefaults,
  createTlsProxy: actions.createTlsProxy,
  goToTlsProxyList: actions.goToTlsProxyList,
  getDistributorConfigurationsWithView: distributorConfigurationActions.getDistributorConfigurationsWithView,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter;

export const TlsProxyCreate: FunctionComponent<Props> = ({
  deliveryFunctionId,
  filteredSecurityInfos,
  history,
  isAuditEnabled,
  userId,
  userName,
  xcipioNodes,
  instanceIds,
  distributorConfigurations,
  ss8ProxyDefaults,
  getSecurityInfos,
  getNodes,
  getSs8ProxyDefaults,
  createTlsProxy,
  goToTlsProxyList,
  getDistributorConfigurationsWithView,
}: Props) => {
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<TlsProxy>>({});
  const formRef = useRef<TlsProxyFormRef>(null);
  const [
    nodeNamesIpAddressesPortsOptionsMap,
    setNodeNamesIpAddressesPortsOptionsMap,
  ] = useState<adapters.NodeNameIpAddressPortsOptionsMap>({});

  useEffect(() => {
    getSecurityInfos(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSs8ProxyDefaults(deliveryFunctionId);
    getDistributorConfigurationsWithView(deliveryFunctionId);
  }, [deliveryFunctionId, getSecurityInfos, getNodes, getSs8ProxyDefaults, getDistributorConfigurationsWithView]);

  useEffect(() => {
    const adaptedTemplate = adapters.adaptTemplateOptions(
      tlsProxyTemplate,
      filteredSecurityInfos,
      xcipioNodes,
      instanceIds,
      ss8ProxyDefaults,
    );

    const initialData = factory.createTlsProxyBasedOnTemplate(adaptedTemplate);
    setData(initialData);

    setNodeNamesIpAddressesPortsOptionsMap(
      adapters.adaptNodeNamesIpAddressesPortsOptionsMap(distributorConfigurations),
    );

    setTemplate(adaptedTemplate);
  }, [filteredSecurityInfos, xcipioNodes, instanceIds, distributorConfigurations, ss8ProxyDefaults]);

  const handleClickSave = () => {
    const isValid = formRef?.current?.isValid(true, true) ?? true;
    if (!isValid) {
      return;
    }

    const formData = formRef?.current?.getValues();
    if (!formData) {
      return;
    }

    const tlsProxyBody = getTlsProxyDataToSubmit(
      formData,
      null,
      isAuditEnabled,
      userId,
      userName,
      'CREATE',
    );

    createTlsProxy(deliveryFunctionId, tlsProxyBody, history);
  };

  const handleCancel = () => {
    goToTlsProxyList(history);
  };

  const handleFieldUpdate = useCallback((param: FieldUpdateHandlerParam, formSection?:string) => {
    if (!template) {
      return;
    }
    const nodeName = formRef.current?.getValues().nodeName || '';

    if (adapters.isEditingNodeName(param.name)) {
      adapters.updateListenersIpAddressOptions(template, param.value, nodeNamesIpAddressesPortsOptionsMap);
    } else if (adapters.isEditingListenersIpAddress(param.name, formSection, nodeName)) {
      adapters.updateListenersPortOptions(template, param.value, nodeName, nodeNamesIpAddressesPortsOptionsMap);
    }
  }, [template, nodeNamesIpAddressesPortsOptionsMap, formRef]);

  if (!template) {
    return null;
  }

  return (
    <div className="app-container-content" data-testid="TlsProxyCreate">
      <TlsProxyForm
        ref={formRef}
        title="New Security Gateway"
        isEditing
        data={data}
        template={template}
        onClickSave={handleClickSave}
        onClickCancelClose={handleCancel}
        onFieldUpdate={handleFieldUpdate}
      />
    </div>
  );
};

export default withRouter(connector(TlsProxyCreate));
