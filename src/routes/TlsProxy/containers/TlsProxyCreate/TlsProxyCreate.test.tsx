import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import {
  fireEvent,
  getByDisplayValue,
  getByRole,
  getByTestId,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockNodes } from '__test__/mockNodes';
import { mockSs8ProxyDefaults } from '__test__/mockSs8ProxyDefaults';
import { getByDataTest } from '__test__/customQueries';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import TlsProxyCreate from './TlsProxyCreate';
import { SYSTEM_SETTINGS_ROUTE, SECURITY_GATEWAY_ROUTE } from '../../../../constants';
import TlsProxy from '../../TlsProxy';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<TlsProxyCreate />', () => {
  beforeEach(() => {
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/ss8ProxyDefaults/0').reply(200, { data: mockSs8ProxyDefaults });
    axiosMock.onGet('/distributorConfigs/0').reply(200, { data: mockDistributorsConfiguration });
    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('TlsProxyCreate')).toBeVisible();
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('New Security Gateway')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of tls proxies when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${SECURITY_GATEWAY_ROUTE}/create`]}>
          <TlsProxy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByTestId('TlsProxyList')).toBeVisible();
  });

  it('should load the initial data', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Security Gateway')).toBeVisible());
    await waitFor(() => expect(screen.getByText('node-1')).toBeVisible());
    await waitFor(() => expect(screen.getByText('instanceId1')).toBeVisible());

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    fireEvent.click(getByTestId(listeners, 'CollapsibleFormButton'));

    const listenerTab = getByRole(listeners, 'tabpanel');
    expect((listenerTab.querySelector('input[name="ipAddress"]') as HTMLInputElement).value).toEqual('');
    expect((listenerTab.querySelector('input[name="port"]') as HTMLInputElement).value).toEqual('');
    expect(getByDisplayValue(listenerTab, 'Active')).toBeVisible();
    expect((getByDataTest(listenerTab, 'input-keepaliveTimeout') as HTMLInputElement).value).toEqual('300');
    expect((getByDataTest(listenerTab, 'input-maxConcurrentConnection') as HTMLInputElement).value).toEqual('128');
    expect(getByDisplayValue(listenerTab, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'TCP')).toBeVisible();
    expect(getByDisplayValue(listenerTab, 'Based on Source Address (default)')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    fireEvent.click(getByTestId(destinations, 'CollapsibleFormButton'));

    const destinationTab = getByRole(destinations, 'tabpanel');
    expect((getByDataTest(destinationTab, 'input-ipAddress') as HTMLInputElement).value).toEqual('');
    expect((getByDataTest(destinationTab, 'input-port') as HTMLInputElement).value).toEqual('');
    expect(getByDisplayValue(listenerTab, 'Active')).toBeVisible();
    expect((getByDataTest(destinationTab, 'input-keepaliveInterval') as HTMLInputElement).value).toEqual('60');
    expect((getByDataTest(destinationTab, 'input-numOfConnections') as HTMLInputElement).value).toEqual('1');
    expect(getByDisplayValue(destinationTab, 'TCP')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')).toHaveLength(7);
  });

  it('should show an error message if duplicated ip:port', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '1' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(screen.getByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')).toBeVisible();
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(2);
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Save')[0]);

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should show success message on Save of a valid form', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Name 1 successfully created.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <TlsProxyCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name to fail' } });
    fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: 'instanceId1' } });

    const listeners = screen.getByTestId('MultiTabFormSectionListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(listenerTab.querySelector('input[name="port"]') as Element, { target: { value: '1' } });

    const destinations = screen.getByTestId('MultiTabFormSectionDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByDataTest(destinationTab, 'input-ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-port'), { target: { value: '2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Unable to create Name to fail: Request failed with status code 500.')).toBeVisible());
  });
});
