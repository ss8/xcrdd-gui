import { FormTemplateSection, FormTemplate } from 'data/template/template.types';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import { mockNodes } from '__test__/mockNodes';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { tlsProxyTemplate } from '../templates';
import {
  adaptNodeNamesIpAddressesPortsOptionsMap,
  adaptTemplateOptions,
  adaptTemplateVisibilityMode,
  NodeNameIpAddressPortsOptionsMap,
  isEditingListenersIpAddress,
  isEditingNodeName,
  updateListenersPortOptions,
  updateListenersIpAddressOptions,
} from './tlsProxyTemplate.adapter';

describe('tlsProxyTemplate.adapter', () => {
  describe('adaptTemplateOptions()', () => {
    it('should set the security info', () => {
      const template = adaptTemplateOptions(
        tlsProxyTemplate,
        mockSecurityInfos,
        mockNodes,
        ['instanceId1', 'instanceId2'],
        {
          listeners: {
            keepaliveTimeout: 300,
            maxConcurrentConnection: 128,
            requiredState: 'ACTIVE',
            routeRule: 'SRCADDR',
            transport: 'TCP',
          },
          destinations: {
            keepaliveInterval: 60,
            keepaliveRetries: 6,
            numOfConnections: 1,
            transport: 'TCP',
          },
        },
      );
      const expected = [{
        label: 'None',
        value: 'Tk9ORQ',
      }, {
        label: 'HTTPS_Server_Sim',
        value: 'SFRUUFNfU2VydmVyX1NpbQ',
      }, {
        label: 'TLS12_CLIENT',
        value: 'VExTMTJfQ0xJRU5U',
      }, {
        label: 'TLS_Server_Simpl',
        value: 'VExTX0NsaWVudF9TaW1wbA',
      }, {
        label: 'TLSv12_Server',
        value: 'VExTdjEyX1NlcnZlcg',
      }];

      expect((template.listeners as FormTemplateSection).fields.secInfoId.options)
        .toEqual(expected);
    });

    it('should set the node names', () => {
      const template = adaptTemplateOptions(
        tlsProxyTemplate,
        mockSecurityInfos,
        mockNodes,
        ['instanceId1', 'instanceId2'],
        {
          listeners: {
            keepaliveTimeout: 300,
            maxConcurrentConnection: 128,
            requiredState: 'ACTIVE',
            routeRule: 'SRCADDR',
            transport: 'TCP',
          },
          destinations: {
            keepaliveInterval: 60,
            keepaliveRetries: 6,
            numOfConnections: 1,
            transport: 'TCP',
          },
        },
      );
      const expected = [{
        label: 'Select an Xcipio node',
        value: '',
      }, {
        label: 'node-1',
        value: 'node-1',
      }, {
        label: 'node-2',
        value: 'node-2',
      }, {
        label: 'CCPAGNODE',
        value: 'CCPAGNODE',
      }, {
        label: 'MDF2NODE',
        value: 'MDF2NODE',
      }, {
        label: 'MDF3NODE',
        value: 'MDF3NODE',
      }];

      expect((template.main as FormTemplateSection).fields.nodeName.options)
        .toEqual(expected);
    });

    it('should set the instance ids', () => {
      const template = adaptTemplateOptions(
        tlsProxyTemplate,
        mockSecurityInfos,
        mockNodes,
        ['instanceId1', 'instanceId2'],
        {
          listeners: {
            keepaliveTimeout: 300,
            maxConcurrentConnection: 128,
            requiredState: 'ACTIVE',
            routeRule: 'SRCADDR',
            transport: 'TCP',
          },
          destinations: {
            keepaliveInterval: 60,
            keepaliveRetries: 6,
            numOfConnections: 1,
            transport: 'TCP',
          },
        },
      );
      const expected = [{
        label: 'Select an instance id',
        value: '',
      }, {
        label: 'instanceId1',
        value: 'instanceId1',
      }, {
        label: 'instanceId2',
        value: 'instanceId2',
      }];

      expect((template.main as FormTemplateSection).fields.instanceId.options)
        .toEqual(expected);
    });
  });

  describe('adaptTemplateVisibilityMode()', () => {
    let template: FormTemplate;

    beforeEach(() => {
      template = adaptTemplateOptions(
        tlsProxyTemplate,
        mockSecurityInfos,
        mockNodes,
        ['instanceId1', 'instanceId2'],
        {
          listeners: {
            keepaliveTimeout: 300,
            maxConcurrentConnection: 128,
            requiredState: 'ACTIVE',
            routeRule: 'SRCADDR',
            transport: 'TCP',
          },
          destinations: {
            keepaliveInterval: 60,
            keepaliveRetries: 6,
            numOfConnections: 1,
            transport: 'TCP',
          },
        },
      );
    });

    it('should return template adapted to create mode', () => {
      expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
    });

    it('should return the template adapted to edit mode', () => {
      expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
        ...template,
      });
    });

    it('should return the template adapted to view mode', () => {
      expect(adaptTemplateVisibilityMode(template, 'view')).toEqual({
        ...template,
        main: {
          ...(template.main as FormTemplateSection),
          fields: {
            ...(template.main as FormTemplateSection).fields,
            name: {
              ...(template.main as FormTemplateSection).fields.name,
              readOnly: true,
            },
            nodeName: {
              ...(template.main as FormTemplateSection).fields.nodeName,
              readOnly: true,
            },
            instanceId: {
              ...(template.main as FormTemplateSection).fields.instanceId,
              readOnly: true,
            },
          },
        },
        listeners: {
          ...(template.listeners as FormTemplateSection),
          fields: {
            ...(template.listeners as FormTemplateSection).fields,
            id: {
              ...(template.listeners as FormTemplateSection).fields.id,
              readOnly: true,
            },
            ipAddress: {
              ...(template.listeners as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(template.listeners as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(template.listeners as FormTemplateSection).fields.requiredState,
              readOnly: true,
            },
            keepaliveTimeout: {
              ...(template.listeners as FormTemplateSection).fields.keepaliveTimeout,
              readOnly: true,
            },
            maxConcurrentConnection: {
              ...(template.listeners as FormTemplateSection).fields.maxConcurrentConnection,
              readOnly: true,
            },
            secInfoId: {
              ...(template.listeners as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(template.listeners as FormTemplateSection).fields.transport,
              readOnly: true,
            },
            routeRule: {
              ...(template.listeners as FormTemplateSection).fields.routeRule,
              readOnly: true,
            },
          },
        },
        destinations: {
          ...(template.destinations as FormTemplateSection),
          fields: {
            ...(template.destinations as FormTemplateSection).fields,
            id: {
              ...(template.destinations as FormTemplateSection).fields.id,
              readOnly: true,
            },
            ipAddress: {
              ...(template.destinations as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(template.destinations as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(template.destinations as FormTemplateSection).fields.requiredState,
              readOnly: true,
            },
            keepaliveInterval: {
              ...(template.destinations as FormTemplateSection).fields.keepaliveInterval,
              readOnly: true,
            },
            numOfConnections: {
              ...(template.destinations as FormTemplateSection).fields.numOfConnections,
              readOnly: true,
            },
            secInfoId: {
              ...(template.destinations as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(template.destinations as FormTemplateSection).fields.transport,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should return the template adapted to copy mode', () => {
      expect(adaptTemplateVisibilityMode(template, 'copy')).toEqual({
        ...template,
      });
    });
  });

  describe('adaptNodeNamesIpAddressesPortsOptionsMap()', () => {
    let nodeNamesOptionsMap: NodeNameIpAddressPortsOptionsMap = {};

    const EXPECTED_GROUP: NodeNameIpAddressPortsOptionsMap = {
      'Node-Dummy-1': {
        ipAddressOptions: [{
          label: '1.1.1.1',
          value: '1.1.1.1',
        }, {
          label: '1.1.1.2',
          value: '1.1.1.2',
        }, {
          label: '2.2.2.2',
          value: '2.2.2.2',
        }, {
          label: '66.66.66.66',
          value: '66.66.66.66',
        },
        ],
        ipAddressPortOptionsMap: {
          '1.1.1.1': [
            {
              label: '1111',
              value: '1111',
            }, {
              label: '2222',
              value: '2222',
            },
          ],
          '1.1.1.2': [
            {
              label: '33333',
              value: '33333',
            },
          ],
          '2.2.2.2': [
            {
              label: '1112',
              value: '1112',
            },
          ],
          '66.66.66.66': [
            {
              label: '45064',
              value: '45064',
            },
          ],
        },
      },
      'Node-Dummy-2': {
        ipAddressOptions: [
          {
            label: '44.44.44.44',
            value: '44.44.44.44',
          },
        ],
        ipAddressPortOptionsMap: {
          '44.44.44.44': [
            {
              label: '45060',
              value: '45060',
            },
          ],
        },
      },
      CCPAGNODE: {
        ipAddressOptions: [
          {
            label: '12.2.222.1',
            value: '12.2.222.1',
          },
        ],
        ipAddressPortOptionsMap: {
          '12.2.222.1': [
            {
              label: '45160',
              value: '45160',
            },
          ],
        },
      },
    };

    beforeEach(() => {
      nodeNamesOptionsMap = adaptNodeNamesIpAddressesPortsOptionsMap(mockDistributorsConfiguration);
    });

    it('should return the ipAddressesOptions to nodeName = Node-Dummy-1', () => {
      expect(nodeNamesOptionsMap['Node-Dummy-1']?.ipAddressOptions).toEqual(EXPECTED_GROUP['Node-Dummy-1'].ipAddressOptions);
    });

    it('should return the ipAddresses Options to nodeName = Node-Dummy-2', () => {
      expect(nodeNamesOptionsMap['Node-Dummy-2']?.ipAddressOptions).toEqual(EXPECTED_GROUP['Node-Dummy-2'].ipAddressOptions);
    });

    it('should return the port Options to IP 44.44.44.44 to nodeName = Node-Dummy-2', () => {
      expect(nodeNamesOptionsMap['Node-Dummy-2']?.ipAddressPortOptionsMap['44.44.44.44']).toEqual(EXPECTED_GROUP['Node-Dummy-2'].ipAddressPortOptionsMap['44.44.44.44']);
    });

    it('should return the full NodeNames Options Map according the DistributorsConfiguration informed', () => {
      expect(nodeNamesOptionsMap).toEqual(EXPECTED_GROUP);
    });
  });

  describe('isEditingNodeName()', () => {
    it('should return truthy isEditingNodeName', () => {
      expect(isEditingNodeName('nodeName')).toEqual(true);
    });

    it('should return falsy isEditingNodeName because params is not nodeName', () => {
      expect(isEditingNodeName('port')).toEqual(false);
    });

    describe('isEditingListenersIpAddress()', () => {
      it('should return truthy isEditingListenersIpAddress', () => {
        expect(isEditingListenersIpAddress('ipAddress', 'listeners', 'nodeName')).toEqual(true);
      });

      it('should return falsy isEditingListenersIpAddress because params is not ipAddress', () => {
        expect(isEditingListenersIpAddress('port', 'listeners', 'nodeName')).toEqual(false);
      });

      it('should return falsy isEditingListenersIpAddress because form section is not listeners', () => {
        expect(isEditingListenersIpAddress('ipAddress', 'destinations', 'nodeName')).toEqual(false);
      });
    });

  });

  describe('updateListenersPortOptions()', () => {
    let nodeNamesOptionsMap: NodeNameIpAddressPortsOptionsMap = {};
    const template:FormTemplate = {
      version: '1',
      key: '1',
      name: 'template',
      listeners: {
        fields: {
          ipAddress: {
            type: 'textSuggest',
            options: [],
          },
          port: {
            type: 'textSuggest',
            options: [],
          },
        },
        metadata: {},
      },
    };

    beforeEach(() => {
      nodeNamesOptionsMap = adaptNodeNamesIpAddressesPortsOptionsMap(mockDistributorsConfiguration);
    });

    it('should update template Listeners and ports Options', () => {
      updateListenersIpAddressOptions(template, 'Node-Dummy-2', nodeNamesOptionsMap);

      expect((template.listeners as FormTemplateSection).fields.ipAddress.options).toEqual([{ label: '44.44.44.44', value: '44.44.44.44' }]);
      expect((template.listeners as FormTemplateSection).fields.port.options).toEqual([]);
    });

    it('should update template Port Options', () => {
      updateListenersPortOptions(template, '44.44.44.44', 'Node-Dummy-2', nodeNamesOptionsMap);

      expect((template.listeners as FormTemplateSection).fields.port.options).toEqual([{ label: '45060', value: '45060' }]);
    });
  });
});
