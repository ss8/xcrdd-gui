import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import getTlsProxyDataToSubmit from './getTlsProxyDataToSubmit';

describe('getTlsProxyDataToSubmit()', () => {
  let tlsProxy: TlsProxy;
  let originalTlsProxy: TlsProxy;
  let expectedTlsProxy: TlsProxy;
  const userId = 'userId1';
  const userName = 'userName1';

  beforeEach(() => {
    tlsProxy = {
      id: 'id1',
      name: 'new name',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: 'listenerId1',
        ipAddress: '1.1.1.2',
        port: '1',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '256',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: 'destinationId1',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'ACTIVE',
        keepaliveInterval: '60',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
      }],
    };

    originalTlsProxy = {
      id: 'id1',
      name: 'name1',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: 'listenerId1',
        ipAddress: '1.1.1.1',
        port: '1',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '256',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: 'destinationId1',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'ACTIVE',
        keepaliveInterval: '60',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
      }],
    };

    expectedTlsProxy = {
      id: 'id1',
      name: 'new name',
      nodeName: 'node-1',
      instanceId: 'instanceId1',
      listeners: [{
        id: 'listenerId1',
        ipAddress: '1.1.1.2',
        port: '1',
        requiredState: 'INACTIVE',
        keepaliveTimeout: '300',
        maxConcurrentConnection: '256',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
        routeRule: 'SRCADDR',
      }],
      destinations: [{
        id: 'destinationId1',
        ipAddress: '2.2.2.2',
        port: '2',
        requiredState: 'ACTIVE',
        keepaliveInterval: '60',
        numOfConnections: '1',
        secInfoId: 'Tk9ORQ',
        transport: 'TCP',
      }],
    };
  });

  describe('when operation is CREATE', () => {
    beforeEach(() => {
      expectedTlsProxy.listeners[0].operation = 'CREATE';
      expectedTlsProxy.destinations[0].operation = 'CREATE';
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTlsProxyDataToSubmit(
          tlsProxy,
          null,
          false,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedTlsProxy,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getTlsProxyDataToSubmit(
          tlsProxy,
          null,
          true,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedTlsProxy,
        auditDetails: {
          actionType: 'Security Gateway Add',
          auditEnabled: true,
          fieldDetails: '[{"field":"id","value":"id1"},{"field":"name","value":"new name"},{"field":"nodeName","value":"node-1"},{"field":"instanceId","value":"instanceId1"},{"field":"listeners","value":[{"id":"listenerId1","ipAddress":"1.1.1.2","port":"1","requiredState":"INACTIVE","keepaliveTimeout":"300","maxConcurrentConnection":"256","secInfoId":"Tk9ORQ","transport":"TCP","routeRule":"SRCADDR"}]},{"field":"destinations","value":[{"id":"destinationId1","ipAddress":"2.2.2.2","port":"2","requiredState":"ACTIVE","keepaliveInterval":"60","numOfConnections":"1","secInfoId":"Tk9ORQ","transport":"TCP"}]}]',
          recordName: 'new name',
          service: 'SecurityGateway',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });

  describe('when operation is UPDATE', () => {
    beforeEach(() => {
      expectedTlsProxy.operation = 'UPDATE';
      expectedTlsProxy.listeners[0].operation = 'UPDATE';
      expectedTlsProxy.destinations[0].operation = 'NO_OPERATION';
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getTlsProxyDataToSubmit(
          tlsProxy,
          originalTlsProxy,
          false,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedTlsProxy,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getTlsProxyDataToSubmit(
          tlsProxy,
          originalTlsProxy,
          true,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedTlsProxy,
        auditDetails: {
          actionType: 'Security Gateway Edit',
          auditEnabled: true,
          fieldDetails: '[{"field":"name","before":"name1","after":"new name"},{"field":"listeners","before":[{"id":"listenerId1","ipAddress":"1.1.1.1","port":"1","requiredState":"INACTIVE","keepaliveTimeout":"300","maxConcurrentConnection":"256","secInfoId":"Tk9ORQ","transport":"TCP","routeRule":"SRCADDR"}],"after":[{"id":"listenerId1","ipAddress":"1.1.1.2","port":"1","requiredState":"INACTIVE","keepaliveTimeout":"300","maxConcurrentConnection":"256","secInfoId":"Tk9ORQ","transport":"TCP","routeRule":"SRCADDR"}]}]',
          recordName: 'new name',
          service: 'SecurityGateway',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });
});
