import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { Operation, WithAuditDetails, AuditDetails } from 'data/types';
import { setTlsProxyOperationValues } from 'data/tlsProxy/tlsProxy.adapter';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';

export default function getTlsProxyDataToSubmit(
  tlsProxy: TlsProxy,
  originalTlsProxy: TlsProxy | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
  operation: Operation,
): WithAuditDetails<TlsProxy> {
  const dataToSubmit = setTlsProxyOperationValues(tlsProxy, originalTlsProxy, operation);

  const tlsProxyBody: WithAuditDetails<TlsProxy> = {
    data: dataToSubmit,
  };

  let actionType = AuditActionType.SECURITY_GATEWAY_ADD;
  let isEdit = false;
  if (operation === 'UPDATE') {
    actionType = AuditActionType.SECURITY_GATEWAY_EDIT;
    isEdit = true;
  }

  if (isAuditEnabled) {
    const auditDetails = getAuditDetails(
      isAuditEnabled,
      userId,
      userName,
      getAuditFieldInfo(tlsProxy, null, isEdit, originalTlsProxy),
      tlsProxy.name,
      AuditService.SECURITY_GATEWAY,
      actionType,
    );
    tlsProxyBody.auditDetails = auditDetails as AuditDetails;
  }

  return tlsProxyBody;
}
