import cloneDeep from 'lodash.clonedeep';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FormTemplate, FormTemplateOption, FormTemplateSection } from 'data/template/template.types';
import { NodeData } from 'data/nodes/nodes.types';
import { DistributorConfiguration, DistributorConfigurationDestination } from 'data/distributorConfiguration/distributorConfiguration.types';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import { GlobalDefaultValues, Ss8ProxyDefaultsTemplate } from 'data/ss8ProxyDefault/ss8ProxyDefault.types';

export type IpAddressPortsOptionsMap = {
  ipAddressOptions: TextSuggestType[],
  ipAddressPortOptionsMap: IpAddressPortOptionsMap,
}

export type NodeNameIpAddressPortsOptionsMap = {
  [nodeName:string]: IpAddressPortsOptionsMap
}

type NodeNameIpAddressMap = {
  [nodeName:string]: IpAddressPortOptionsMap
};

type IpAddressPortOptionsMap = {
  [ipAddress:string]: TextSuggestType[]
};

type RelatedNodeNameIpAddressPort = {
  nodeName: string,
  ipAddressOptions: TextSuggestType[],
  ipAddressPortOptionsMap:IpAddressPortOptionsMap,
};

export function adaptTemplateOptions(
  template: FormTemplate,
  securityInfos: SecurityInfo[],
  xcipioNodes: NodeData[],
  instanceIds: string[],
  ss8ProxyDefaults: Ss8ProxyDefaultsTemplate,
): FormTemplate {
  const adaptedTemplate = cloneDeep(template);

  const securityInfoOptions = getSecurityInfoOptions(securityInfos);
  const nodeNamesOptions = getNodeNameOptions(xcipioNodes);
  const instanceIdOptions = getInstanceIdOptions(instanceIds);

  setSecurityInfoId(adaptedTemplate, securityInfoOptions);
  setNodeName(adaptedTemplate, nodeNamesOptions);
  setInstanceIdOptions(adaptedTemplate, instanceIdOptions);
  setSs8DefaultValues(adaptedTemplate, ss8ProxyDefaults);

  return adaptedTemplate;
}

export function adaptTemplateVisibilityMode(
  template: FormTemplate,
  mode: 'create' | 'edit' | 'view' | 'copy',
): FormTemplate {
  const clonedTemplate = cloneDeep(template);

  if (mode === 'view') {
    setAllFieldsAsReadOnly(clonedTemplate);
  }

  return clonedTemplate;
}

export function isEditingListenersIpAddress(
  paramName:string, formSection?: string, nodeName?: string,
): boolean {
  return paramName === 'ipAddress' && formSection === 'listeners' && !!nodeName;
}

export function isEditingNodeName(paramName:string): boolean {
  return paramName === 'nodeName';
}

export function updateListenersPortOptions(
  template:FormTemplate, ipAddress:string, nodeName:string, nodeNamesOptionsMap: NodeNameIpAddressPortsOptionsMap,
): void {

  const options = nodeNamesOptionsMap?.[nodeName]?.ipAddressPortOptionsMap?.[ipAddress] || [];
  (template.listeners as FormTemplateSection).fields.port.options = options;
}

export function updateListenersIpAddressOptions(
  template:FormTemplate, nodeName:string, nodeNamesOptionsMap: NodeNameIpAddressPortsOptionsMap,
): void {
  const options = nodeNamesOptionsMap?.[nodeName]?.ipAddressOptions || [];
  (template.listeners as FormTemplateSection).fields.ipAddress.options = options;
  (template.listeners as FormTemplateSection).fields.port.options = [];
}

// creates a Map of NodeNames, each map value contains a list of ipAddress do be used in suggest and select components
// ad also a map of ipAddress where each ipAddress has a list of ports related to the to the ipAddress
export function adaptNodeNamesIpAddressesPortsOptionsMap(
  distributorConfigurations: DistributorConfiguration[],
):NodeNameIpAddressPortsOptionsMap {

  const nodeNamesIpAddressPortsMap = distributorConfigurations.reduce(toNodeNameWithRelatedIpAddressPorts, {});

  return adaptNodeNamesIpAddressPortMapToOptions(nodeNamesIpAddressPortsMap);
}

// creates an object relating the distributor nodeNames to the destinations ipAddress and ports
// each nodeName contains a map having the ipAddress as key and a list of related port as data
function toNodeNameWithRelatedIpAddressPorts(
  nodeNamesIpAddressPortMap: NodeNameIpAddressMap,
  distributorConfiguration:DistributorConfiguration,
): NodeNameIpAddressMap {

  const { nodeName } = distributorConfiguration;

  if (!nodeName) {
    return { ...nodeNamesIpAddressPortMap };
  }

  const currentNodeIpAddressPortMap = nodeNamesIpAddressPortMap[nodeName] || {};

  // maps destinations ipAddress and ports into objects with ipAddress as keys and a list of related ports as value
  const relatedIpAddressPortMap = groupRelatedIpAddressPort(
    distributorConfiguration.destinations,
    currentNodeIpAddressPortMap,
  );

  return { ...nodeNamesIpAddressPortMap, [nodeName]: relatedIpAddressPortMap };
}

// it loops inside the destinations, creating key values objects to the ipAddress and ports.
// Each ipAddress represents a key, and its values are the related ports
function groupRelatedIpAddressPort(
  destinations: DistributorConfigurationDestination[],
  nodeIpAddressMap: IpAddressPortOptionsMap,
):IpAddressPortOptionsMap {

  return destinations
    .reduce((ipAddressPortMap:IpAddressPortOptionsMap, destination) => {
      const { ipAddress } = destination;

      const totalIpAddressPorts = nodeIpAddressMap[ipAddress] || [];
      const ipAddressPorts = ipAddressPortMap[ipAddress] || [];

      const ports = [
        ...totalIpAddressPorts,
        ...ipAddressPorts,
        { value: destination.port, label: destination.port },
      ];

      return {
        ...nodeIpAddressMap,
        ...ipAddressPortMap,
        [ipAddress]: ports,
      };
    }, {});
}

function adaptNodeNamesIpAddressPortMapToOptions(
  nodeNamesIpAddressPortsMap:NodeNameIpAddressMap,
):NodeNameIpAddressPortsOptionsMap {
  return Object.keys(nodeNamesIpAddressPortsMap)
    .map(toRelatedNodeNamesIpAddressAndPortsOptions(nodeNamesIpAddressPortsMap))
    .reduce(adaptNodeNamesIpAddressPortsToOptionsMap, {});
}

function toRelatedNodeNamesIpAddressAndPortsOptions(
  nodeNamesIpAddressPortsMap: NodeNameIpAddressMap,
) {
  return (nodeName: string):RelatedNodeNameIpAddressPort => {
    const ipAddressPortOptionsMap = nodeNamesIpAddressPortsMap[nodeName];
    return {
      nodeName,
      ipAddressOptions: adaptIpAddressOptions(ipAddressPortOptionsMap),
      ipAddressPortOptionsMap,
    };
  };
}

function adaptIpAddressOptions(ipAddressesPortsOptionsMap:IpAddressPortOptionsMap): TextSuggestType[] {
  return Object.keys(ipAddressesPortsOptionsMap).map(
    (ipAddress) => ({ label: ipAddress, value: ipAddress }),
  );
}

function adaptNodeNamesIpAddressPortsToOptionsMap(
  nodeNameOptionsMap: NodeNameIpAddressPortsOptionsMap,
  actual: RelatedNodeNameIpAddressPort,
):NodeNameIpAddressPortsOptionsMap {
  return ({
    ...nodeNameOptionsMap,
    [actual.nodeName]: {
      ipAddressOptions: actual.ipAddressOptions,
      ipAddressPortOptionsMap: actual.ipAddressPortOptionsMap,
    },
  });
}

function getSecurityInfoOptions(securityInfos: SecurityInfo[]) {
  return securityInfos.map(({ name, id }) => ({
    label: name,
    value: id,
  }));
}

function setSecurityInfoId(template: FormTemplate, options: FormTemplateOption[]): void {
  // Listener
  const listenerTemplateSection = (template.listeners as FormTemplateSection);
  const listenerFirstOptions = listenerTemplateSection.fields.secInfoId.options?.[0];
  if (listenerFirstOptions) {
    listenerTemplateSection.fields.secInfoId.options = [listenerFirstOptions];
  }
  listenerTemplateSection.fields.secInfoId.options?.push(...options);
}

function getNodeNameOptions(nodeNames: NodeData[]): FormTemplateOption[] {
  return nodeNames.map(({ nodeName }) => ({
    label: nodeName,
    value: nodeName,
  }));
}

function setNodeName(template: FormTemplate, options: FormTemplateOption[]): void {
  const templateSection = (template.main as FormTemplateSection);
  templateSection.fields.nodeName.options?.push(...options);
}

function getInstanceIdOptions(instanceIds: string[]): FormTemplateOption[] {
  return instanceIds.map((instance) => ({
    label: instance,
    value: instance,
  }));
}

function setInstanceIdOptions(template: FormTemplate, options: FormTemplateOption[]): void {
  const templateSection = (template.main as FormTemplateSection);
  templateSection.fields.instanceId.options?.push(...options);
}

function setAllFieldsAsReadOnly(template: FormTemplate): void {
  Object.entries(template).forEach(([_templateKey, templateEntry]) => {
    if (typeof templateEntry === 'string') {
      return;
    }

    Object.entries(templateEntry.fields).forEach(([_fieldKey, fieldEntry]) => {
      fieldEntry.readOnly = true;
    });
  });
}

function setSs8DefaultValues(template: FormTemplate, ss8ProxyDefaults: Ss8ProxyDefaultsTemplate): void {
  if (!template || !ss8ProxyDefaults) {
    return;
  }

  Object.keys(ss8ProxyDefaults).forEach((key) => {

    const ss8ProxyDefault = ss8ProxyDefaults[key];

    Object.keys(ss8ProxyDefault).forEach((defaultKey) => {

      (template[key] as FormTemplateSection).fields[defaultKey].initial = ss8ProxyDefault[defaultKey];

    });
  });

}
