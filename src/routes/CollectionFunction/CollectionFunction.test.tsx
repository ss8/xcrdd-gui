import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { mockTemplates } from '__test__/mockTemplates';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { SELECT_COLLECTION_FUNCTION } from 'data/collectionFunction/collectionFunction.types';
import { API_PATH_FTP_INFOS } from 'data/ftpInfo/ftpInfo.types';
import { CF_ROUTE } from '../../constants';
import CollectionFunction from './CollectionFunction';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunction />', () => {

  beforeEach(() => {

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: SELECT_COLLECTION_FUNCTION,
      payload: [mockCollectionFunctions[0]],
    });

    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onDelete(`/cfs/0/${mockCollectionFunctions[0].id}`).reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onGet('/users/userId1/settings?name=CollectionFunctionSettings').reply(200, { data: [] });
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet(`/cfs/0/${mockCollectionFunctions[0].id}?$view=options`).reply(200, { data: [mockCollectionFunctions[0]] });
    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should render the list for the /cfs path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[CF_ROUTE]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('CollectionFunctionList')).toBeVisible();
  });

  it('should render the create for the /cfs/create path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/create`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByTestId('CollectionFunctionCreate')).toBeVisible();
  });

  it('should render the edit for the /cfs/edit path', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/edit`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());
  });

  it('should render the view for the /cfs/view path', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/view`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());
  });

  it('should render the copy for the /cfs/copy path', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/copy`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible();
  });
});
