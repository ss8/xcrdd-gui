import React from 'react';
import { Callout } from '@blueprintjs/core';
import FormButtons from 'components/FormButtons';
import { FormLayout, FormLayoutContent } from 'components/FormLayout';
import FormPanel from 'components/FormPanel';
import './collection-function-form-missing-templates.scss';

type Props = {
  title: string
  onClickCancelClose: () => void
}

const CollectionFunctionFormMissingTemplates = ({
  title,
  onClickCancelClose,
}: Props): JSX.Element => {
  return (
    <div className="collection-function-form collection-function-form-missing-template" data-testid="CollectionFunctionFormMissingTemplates">
      <FormLayout>
        <FormLayoutContent>
          <FormPanel title={title}>
            <FormButtons
              isEditing
              onClickCancel={onClickCancelClose}
              onClickClose={onClickCancelClose}
            />
          </FormPanel>
          <div className="callout">
            <Callout
              title="Unable to load Collection Function templates"
              icon="error"
              intent="danger"
            >
              Please contact system administrator to check Discovery installation
              environment to ensure that all the CF Handover Protocol templates are there.
            </Callout>
          </div>
        </FormLayoutContent>
      </FormLayout>
    </div>
  );
};

export default CollectionFunctionFormMissingTemplates;
