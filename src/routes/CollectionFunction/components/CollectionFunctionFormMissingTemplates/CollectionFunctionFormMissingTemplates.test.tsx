import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import CollectionFunctionFormMissingTemplates from './CollectionFunctionFormMissingTemplates';

describe('<CollectionFunctionFormMissingTemplates />', () => {
  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <CollectionFunctionFormMissingTemplates
        title="Collection Function form title"
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByText('Collection Function form title')).toBeVisible();
    expect(screen.getByText('Cancel')).toBeVisible();
    expect(screen.getByText('Cancel')).toBeEnabled();
    expect(screen.getByText('Save')).toBeVisible();
    expect(screen.getByText('Save').parentElement).toBeDisabled();
  });

  it('should render the callout message', () => {
    render(
      <CollectionFunctionFormMissingTemplates
        title="Collection Function form title"
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByText('Unable to load Collection Function templates')).toBeVisible();
    expect(screen.getByText('Please contact system administrator to check Discovery installation environment to ensure that all the CF Handover Protocol templates are there.')).toBeVisible();
  });

  it('should call the onClickCancelClose callback when clicking in Cancel', () => {
    const onClickCancelClose = jest.fn();
    render(
      <CollectionFunctionFormMissingTemplates
        title="Collection Function form title"
        onClickCancelClose={onClickCancelClose}
      />,
    );
    fireEvent.click(screen.getByText('Cancel'));
    expect(onClickCancelClose).toBeCalled();
  });
});
