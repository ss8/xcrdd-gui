import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
} from 'react';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import * as types from 'data/collectionFunction/collectionFunction.types';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import Form, {
  CustomFormFieldComponent,
  FieldUpdateHandlerParam,
} from 'shared/form';
import './collection-function-form.scss';
import CollectionFunctionInterfaceForm, { CollectionFunctionInterfaceFormRef } from '../CollectionFunctionInterfaceForm';

type Props = {
  title: string,
  isEditing: boolean,
  template: FormTemplate,
  data: Partial<types.CollectionFunctionFormData>
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent }
  onClickSave?: () => void
  onClickEdit?: () => void
  onClickCancelClose: () => void
  onClickCopy?: () => void,
  onClickDelete?: () => void,
  onFieldUpdate?: (param: FieldUpdateHandlerParam, formSection?: 'main' | 'facilityInformation' | 'hi2Interfaces' | 'hi3Interfaces', tabId?: string) => void
  onInterfaceAdd?: (formSection: 'hi2Interfaces' | 'hi3Interfaces') => void
  onInterfaceRemove?: (id: string | number, formSection: 'hi2Interfaces' | 'hi3Interfaces') => void
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => types.CollectionFunctionFormData
}

export const CollectionFunctionForm = forwardRef<Ref, Props>(({
  title,
  isEditing,
  template,
  data,
  customFieldComponentMap,
  onClickSave,
  onClickEdit,
  onClickCancelClose,
  onClickCopy,
  onClickDelete,
  onFieldUpdate = () => null,
  onInterfaceAdd = () => null,
  onInterfaceRemove = () => null,
}: Props, ref) => {
  const mainRef = useRef<Form>(null);
  const facilityInformationRef = useRef<Form>(null);
  const hi2InterfaceRef = useRef<CollectionFunctionInterfaceFormRef>(null);
  const hi3InterfaceRef = useRef<CollectionFunctionInterfaceFormRef>(null);

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isMainValid = mainRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isFacilityInformationValid = facilityInformationRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const allHi2InterfacesValid = hi2InterfaceRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const allHi3InterfacesValid = hi3InterfaceRef.current?.isValid(forceValidation, forceTouched) ?? true;

    return isMainValid && isFacilityInformationValid && allHi2InterfacesValid && allHi3InterfacesValid;
  };

  const getValues = () => {
    const formValues: types.CollectionFunctionFormData = {
      ...mainRef.current?.getValues(),
      ...facilityInformationRef.current?.getValues(),
      hi2Interfaces: hi2InterfaceRef.current?.getValues(),
      hi3Interfaces: hi3InterfaceRef.current?.getValues(),
      ...hi2InterfaceRef.current?.getMainFormValues(),
      ...hi3InterfaceRef.current?.getMainFormValues(),
    };

    return formValues;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const onMainFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, 'main');
  const onFacilityInformationFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, 'facilityInformation');

  const renderFormPanel = (formTitle: string | null = null) => (
    <FormPanel title={formTitle}>
      <FormButtons
        isEditing={isEditing}
        onClickCancel={onClickCancelClose}
        onClickSave={onClickSave}
        onClickEdit={onClickEdit}
        onClickClose={onClickCancelClose}
        onClickCopy={onClickCopy}
        onClickDelete={onClickDelete}
      />
    </FormPanel>
  );

  return (
    <div className="collection-function-form" data-testid="CollectionFunctionForm">
      <FormLayout>
        <FormLayoutContent>
          {renderFormPanel(title)}
          <div data-testid="CollectionFunctionFormMain">
            <Form
              ref={mainRef}
              name={template.key}
              defaults={data}
              fields={(template.main as FormTemplateSection).fields}
              layout={(template.main as FormTemplateSection).metadata.layout?.rows}
              fieldUpdateHandler={onMainFieldUpdate}
            />
          </div>
          <div data-testid="CollectionFunctionFormFacilityInformation">
            <h3>Facility Information</h3>
            <Form
              ref={facilityInformationRef}
              name={template.key}
              defaults={data}
              fields={(template.facilityInformation as FormTemplateSection).fields}
              layout={(template.facilityInformation as FormTemplateSection).metadata.layout?.rows}
              fieldUpdateHandler={onFacilityInformationFieldUpdate}
            />
          </div>
          <CollectionFunctionInterfaceForm
            ref={hi2InterfaceRef}
            id="IRI"
            title="Intercept Related Information (IRI)"
            tabTitle="Interface"
            formSection="hi2Interfaces"
            isEditing={isEditing}
            template={template}
            data={data}
            customFieldComponentMap={customFieldComponentMap}
            onFieldUpdate={onFieldUpdate}
            onInterfaceAdd={onInterfaceAdd}
            onInterfaceRemove={onInterfaceRemove}
          />
          <CollectionFunctionInterfaceForm
            ref={hi3InterfaceRef}
            id="CC"
            title="Communication Content (CC)"
            tabTitle="Interface"
            formSection="hi3Interfaces"
            isEditing={isEditing}
            template={template}
            data={data}
            customFieldComponentMap={customFieldComponentMap}
            onFieldUpdate={onFieldUpdate}
            onInterfaceAdd={onInterfaceAdd}
            onInterfaceRemove={onInterfaceRemove}
          />
        </FormLayoutContent>
        <FormLayoutFooter>
          {renderFormPanel()}
        </FormLayoutFooter>
      </FormLayout>
    </div>
  );
});

export default CollectionFunctionForm;
