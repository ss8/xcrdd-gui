import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getAllByTestId,
  getByLabelText,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';
import { CollectionFunctionFormData, CollectionFunctionInterfaceFormData } from 'data/collectionFunction/collectionFunction.types';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { getByDataTest } from '__test__/customQueries';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { mockNodes } from '__test__/mockNodes';
import CollectionFunctionForm from './CollectionFunctionForm';
import { CollectionFunctionFormRef } from '.';
import { adaptTemplateOptions } from '../../utils/collectionFunctionTemplate.adapter';
import { createCollectionFunctionBasedOnTemplate } from '../../utils/collectionFunctionTemplate.factory';

const CustomStateComponent = ({ value }: {value:string}) => <span data-testid="MyFieldCustom">{value}&nbsp;and info from component!!!</span>;

describe('<CollectionFunctionForm />', () => {
  let collectionFunctionTemplates: { [key: string]: FormTemplate };
  let collectionFunction: Partial<CollectionFunctionFormData>;
  let adaptedTemplates: { [key: string]: FormTemplate };
  let templateKey: string;

  beforeEach(() => {
    collectionFunctionTemplates = {
      'CF-3GPP-33108': getMockTemplateByKey('CF-3GPP-33108'),
      'CF-ETSI-102232': getMockTemplateByKey('CF-ETSI-102232'),
      'CF-JSTD025A': getMockTemplateByKey('CF-JSTD025A'),
      'CF-JSTD025B': getMockTemplateByKey('CF-JSTD025B'),
    };

    templateKey = 'CF-3GPP-33108';

    adaptedTemplates = adaptTemplateOptions(
      collectionFunctionTemplates,
      mockTimeZones,
      mockContacts,
      mockCollectionFunctionGroups,
      mockFtpInfos,
      mockNetworks,
      mockSecurityInfos,
      mockFqdnGroups,
      mockSupportedFeatures.config.collectionFunctions,
      mockNodes,
    );

    collectionFunction = createCollectionFunctionBasedOnTemplate(adaptedTemplates[templateKey], true);
  });

  it('should render the title and Cancel/Save button if in edit mode', () => {

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByText('Form title')).toBeVisible();
    expect(screen.queryAllByText('Save')).toHaveLength(2);
    expect(screen.queryAllByText('Cancel')).toHaveLength(2);
  });

  it('should render the title and Edit/Back button if not in edit mode', () => {
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing={false}
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickEdit={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByText('Form title')).toBeVisible();
    expect(screen.queryAllByText('Edit')).toHaveLength(2);
    expect(screen.queryAllByText('Back')).toHaveLength(2);
  });

  it('should call the button callback when clicking on them if in edit mode', () => {
    const saveHandler = jest.fn();
    const cancelHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={saveHandler}
        onClickCancelClose={cancelHandler}
      />,
    );

    fireEvent.click(screen.queryAllByText('Save')[0]);
    expect(saveHandler).toBeCalled();

    fireEvent.click(screen.queryAllByText('Cancel')[0]);
    expect(cancelHandler).toBeCalled();
  });

  it('should call the button callback when clicking on them if not in edit mode', () => {
    const editHandler = jest.fn();
    const closeHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing={false}
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickEdit={editHandler}
        onClickCancelClose={closeHandler}
      />,
    );

    fireEvent.click(screen.queryAllByText('Edit')[0]);
    expect(editHandler).toBeCalled();

    fireEvent.click(screen.queryAllByText('Back')[0]);
    expect(closeHandler).toBeCalled();
  });

  it('should render the main, facility information, IRI and CC', () => {
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByTestId('CollectionFunctionFormMain')).toBeVisible();
    expect(screen.getByText('LEA:')).toBeVisible();
    expect(screen.getByText('Handover Protocol:')).toBeVisible();
    expect(screen.getByText('Name:')).toBeVisible();

    expect(screen.getByTestId('CollectionFunctionFormFacilityInformation')).toBeVisible();
    expect(screen.getByText('Delivery Type:')).toBeVisible();
    expect(screen.getByText('Delivery Type:')).toBeVisible();
    expect(screen.getByText('Time Zone:')).toBeVisible();
    expect(screen.getByText('Contact:')).toBeVisible();
    expect(screen.getByText('Grace Timer:')).toBeVisible();
    expect(screen.getByText('Xcipio is behind NAT')).toBeVisible();

    expect(screen.getByTestId('CollectionFunctionInterfaceFormIRI')).toBeVisible();
    expect(screen.getByText('Intercept Related Information (IRI)')).toBeVisible();

    expect(screen.getByTestId('CollectionFunctionInterfaceFormCC')).toBeVisible();
    expect(screen.getByText('Communication Content (CC)')).toBeVisible();
  });

  it('should call onFieldUpdate if a field is changed in main section', () => {
    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'Name 1' } });
    expect(updateHandler).toBeCalledWith({
      label: 'Name',
      name: 'name',
      value: 'Name 1',
      valueLabel: 'Name 1',
    },
    'main');
  });

  it('should call onFieldUpdate if a field is changed in facility information section', () => {
    const updateHandler = jest.fn();
    const { container } = render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-deliveryType'), { target: { value: 'MAPPED' } });
    expect(updateHandler).toBeCalledWith({
      label: 'Delivery Type',
      name: 'deliveryType',
      value: 'MAPPED',
      valueLabel: 'MAPPED',
    },
    'facilityInformation');
  });

  it('should call onFieldUpdate if a field is changed in hi2Interfaces section', () => {
    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
    const iriTab = getByRole(iriInterfaces, 'tabpanel');
    fireEvent.change(getByDataTest(iriTab, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    expect(updateHandler).toBeCalledWith({
      label: 'IP Address',
      name: 'destIPAddr',
      value: '1.1.1.1',
      valueLabel: '1.1.1.1',
    },
    'hi2Interfaces',
    expect.stringContaining('.collectionFunctionInterface'));
  });

  it('should call onFieldUpdate if a field is changed in hi3Interfaces section', () => {
    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
    const ccTab = getByRole(ccInterfaces, 'tabpanel');
    fireEvent.change(getByDataTest(ccTab, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    expect(updateHandler).toBeCalledWith({
      label: 'IP Address',
      name: 'destIPAddr',
      value: '2.2.2.2',
      valueLabel: '2.2.2.2',
    },
    'hi3Interfaces',
    expect.stringContaining('.collectionFunctionInterface'));
  });

  it('should render the Intercept Related Information (IRI) form as expected', () => {
    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(iriContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Version:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'XCP IP Address:')).toBeVisible();
    expect(getByText(tab, 'XCP Port (TCP):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
    expect(getByText(tab, 'Connection Type:')).toBeVisible();
    expect(getByText(tab, 'TPKT')).toBeVisible();
    expect(getByText(tab, 'Comments:')).toBeVisible();
  });

  it('should render the Intercept Related Information (IRI) form as expected when Current State is visible', () => {
    const template = cloneDeep(adaptedTemplates[templateKey]);
    (template.hi2Interfaces_TCP as FormTemplateSection).fields.state.hide = 'false';

    const data = cloneDeep(collectionFunction);
    (data.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].id = 'EXISTING_INTERFACE_ID';

    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={data}
        template={template}
        customFieldComponentMap={{
          collectionFunctionCurrentState: CustomStateComponent,
        }}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(iriContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Version:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Current State:')).toBeVisible();
    expect(getByText(tab, 'XCP IP Address:')).toBeVisible();
    expect(getByText(tab, 'XCP Port (TCP):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
    expect(getByText(tab, 'Connection Type:')).toBeVisible();
    expect(getByText(tab, 'TPKT')).toBeVisible();
    expect(getByText(tab, 'Comments:')).toBeVisible();
  });

  it('should render the Intercept Related Information (IRI) form as expected if transport type is FTP', async () => {
    const updateHandler = jest.fn();

    const data = cloneDeep(collectionFunction);
    (data.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={data}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(iriContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Username:')).toBeVisible();
    expect(getByText(tab, 'Password:')).toBeVisible();
    expect(getByText(tab, 'Destination Directory:')).toBeVisible();
  });

  it('should render the Communication Content (CC) form as expected', () => {
    const updateHandler = jest.fn();
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(ccContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Version:')).toBeVisible();
    expect(getByText(tab, 'XDP IP Address:')).toBeVisible();
    expect(getByText(tab, 'XDP Port:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
    expect(getByText(tab, 'Connection Type:')).toBeVisible();
    expect(getByText(tab, 'Use XDP')).toBeVisible();
    expect(getByText(tab, 'TPKT')).toBeVisible();
    expect(getByText(tab, 'Timeout After (secs):')).toBeVisible();
    expect(getByText(tab, 'Template Display Label:')).toBeVisible();
    expect(getByText(tab, 'Comments:')).toBeVisible();
  });

  it('should render the Communication Content (CC) form as expected if transport type is FTP', async () => {
    const updateHandler = jest.fn();

    const data = cloneDeep(collectionFunction);
    (data.hi3Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={data}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={updateHandler}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(ccContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'FTP Info ID:')).toBeVisible();
    expect(getByText(tab, 'Template Display Label:')).toBeVisible();
  });

  it('should call onInterfaceAdd if clicking to add a tab', () => {
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
        onInterfaceAdd={mockCallback}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(iriInterfaces, 'MultiTabAddButton'));

    expect(mockCallback).toBeCalledWith('hi2Interfaces');

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(ccInterfaces, 'MultiTabAddButton'));

    expect(mockCallback).toBeCalledWith('hi3Interfaces');
  });

  it('should call onInterfaceRemove if clicking to remove a tab', () => {
    const mockCallback = jest.fn();

    const data = cloneDeep(collectionFunction);
    data.hi2Interfaces?.push({
      ...data.hi2Interfaces[0],
      id: '1234.collectionFunctionInterface',
    });

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={data}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
        onInterfaceRemove={mockCallback}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getAllByTestId(iriInterfaces, 'MultiTabRemoveIcon')[0]);

    expect(mockCallback).toBeCalledWith(
      expect.stringContaining('.collectionFunctionInterface'),
      'hi2Interfaces',
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(ccInterfaces, 'MultiTabRemoveIcon'));

    expect(mockCallback).toBeCalledWith(
      expect.stringContaining('.collectionFunctionInterface'),
      'hi3Interfaces',
    );
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    const { container } = render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.change(getByDataTest(ccContainer, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(ccContainer, 'input-destPort'), { target: { value: '2' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    await waitFor(() => expect(ccContainer.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should return the form values in getValues for required and default values', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    const { container } = render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.change(getByDataTest(ccContainer, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(ccContainer, 'input-destPort'), { target: { value: '2' } });

    expect(formRef.current?.getValues()).toEqual({
      id: '',
      name: 'Name 1',
      cfGroup: 'id2',
      deliveryThru: 'TCP_OR_FTP',
      tag: 'CF-3GPP-33108',
      type: 'GPRS',
      bufferCC: false,
      cellTowerLocation: false,
      contact: '',
      deliveryType: 'DSR',
      graceTimer: '0',
      insideNat: false,
      timeZone: 'America/New_York',
      transportType: 'TCP',
      hi2Interfaces: [{
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: '',
        connectionType: 'PRIMARY',
        destIPAddr: '1.1.1.1',
        destPort: '1',
        dscp: '0',
        keepAliveFreq: '60',
        keepAliveRetries: '6',
        ownIPAddr: '',
        ownIPPort: '',
        reqState: 'ACTIVE',
        state: '',
        tpkt: true,
        version: '15.6',
      }],
      hi3Interfaces: [{
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: '',
        connectionType: 'PRIMARY',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        displayLabel: '',
        dscp: '0',
        keepAliveFreq: '60',
        keepAliveRetries: '6',
        inactivityTimer: '0',
        ownIPAddr: '',
        ownIPPort: '',
        tpkt: true,
        version: '15.6',
        useIpdu: true,
      }],
    });
  });

  it('should return the form values in getValues for multiple interfaces', async () => {
    const formRef = createRef<CollectionFunctionFormRef>();

    const data = cloneDeep(collectionFunction);
    data.hi2Interfaces?.push({
      ...data.hi2Interfaces[0],
      id: '1234.collectionFunctionInterface',
    });
    data.hi3Interfaces?.push({
      ...data.hi3Interfaces[0],
      id: '5678.collectionFunctionInterface',
    });

    const { container } = render(
      <CollectionFunctionForm
        ref={formRef}
        title="Form title"
        isEditing
        data={data}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });

    fireEvent.change(getByDataTest(container, 'input-deliveryType'), { target: { value: 'MAPPED' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });
    fireEvent.change(getByDataTest(container, 'input-contact'), { target: { value: 'Q29udGFjdCAx' } });
    fireEvent.change(getByDataTest(container, 'input-graceTimer'), { target: { value: '10' } });

    fireEvent.click(getByLabelText(container, 'Xcipio is behind NAT'));

    const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
    const iriTab = getByRole(iriInterfaces, 'tabpanel');

    fireEvent.change(getByDataTest(iriTab, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriTab, 'input-destPort'), { target: { value: '1' } });
    fireEvent.change(getByDataTest(iriTab, 'input-version'), { target: { value: '15.5' } });
    fireEvent.change(getByDataTest(iriTab, 'input-reqState'), { target: { value: 'INACTIVE' } });

    fireEvent.click(getAllByTestId(iriInterfaces, 'CollapsibleFormButton')[0]);

    fireEvent.change(getByDataTest(iriTab, 'input-ownIPAddr'), { target: { value: '12.12.12.12' } });
    fireEvent.change(getByDataTest(iriTab, 'input-ownIPPort'), { target: { value: '12' } });
    fireEvent.change(getByDataTest(iriTab, 'input-keepAliveFreq'), { target: { value: '120' } });
    fireEvent.change(getByDataTest(iriTab, 'input-keepAliveRetries'), { target: { value: '12' } });

    fireEvent.change(getByDataTest(iriTab, 'input-dscp'), { target: { value: '10' } });
    fireEvent.change(getByDataTest(iriTab, 'input-connectionType'), { target: { value: 'FAILOVER' } });
    fireEvent.click(getByLabelText(iriTab, 'TPKT'));
    fireEvent.change(getByDataTest(iriTab, 'input-comments'), { target: { value: 'some comment' } });

    fireEvent.click(getByText(iriInterfaces, 'Interface 2'));
    fireEvent.click(getAllByTestId(iriInterfaces, 'CollapsibleFormButton')[1]);

    const iriTab2 = getByRole(iriInterfaces, 'tabpanel');

    fireEvent.change(getByDataTest(iriTab2, 'input-destIPAddr'), { target: { value: '13.13.13.13' } });
    fireEvent.change(getByDataTest(iriTab2, 'input-destPort'), { target: { value: '13' } });

    const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
    const ccTab = getByRole(ccInterfaces, 'tabpanel');

    fireEvent.change(getByDataTest(ccTab, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(ccTab, 'input-destPort'), { target: { value: '2' } });
    fireEvent.change(getByDataTest(ccTab, 'input-version'), { target: { value: '15.5' } });

    fireEvent.click(getAllByTestId(ccInterfaces, 'CollapsibleFormButton')[0]);

    fireEvent.change(getByDataTest(ccTab, 'input-ownIPAddr'), { target: { value: '22.22.22.22' } });
    fireEvent.change(getByDataTest(ccTab, 'input-ownIPPort'), { target: { value: '22' } });
    fireEvent.change(getByDataTest(ccTab, 'input-keepAliveFreq'), { target: { value: '220' } });
    fireEvent.change(getByDataTest(ccTab, 'input-keepAliveRetries'), { target: { value: '22' } });

    fireEvent.change(getByDataTest(ccTab, 'input-dscp'), { target: { value: '20' } });
    fireEvent.change(getByDataTest(ccTab, 'input-connectionType'), { target: { value: 'FAILOVER' } });
    fireEvent.click(getByLabelText(ccTab, 'Use XDP'));
    fireEvent.click(getByLabelText(ccTab, 'TPKT'));

    fireEvent.change(getByDataTest(ccTab, 'input-inactivityTimer'), { target: { value: '20' } });
    fireEvent.change(getByDataTest(ccTab, 'input-displayLabel'), { target: { value: 'some label' } });
    fireEvent.change(getByDataTest(ccTab, 'input-comments'), { target: { value: 'some comment 2' } });

    fireEvent.click(getByText(ccInterfaces, 'Interface 2'));
    fireEvent.click(getAllByTestId(ccInterfaces, 'CollapsibleFormButton')[1]);

    const ccTab2 = getByRole(ccInterfaces, 'tabpanel');

    fireEvent.change(getByDataTest(ccTab2, 'input-destIPAddr'), { target: { value: '23.23.23.23' } });
    fireEvent.change(getByDataTest(ccTab2, 'input-destPort'), { target: { value: '23' } });

    expect(formRef.current?.getValues()).toEqual({
      id: '',
      name: 'Name 1',
      cfGroup: 'id2',
      deliveryThru: 'TCP_OR_FTP',
      tag: 'CF-3GPP-33108',
      type: 'GPRS',
      bufferCC: false,
      cellTowerLocation: false,
      contact: 'Q29udGFjdCAx',
      deliveryType: 'MAPPED',
      graceTimer: '10',
      insideNat: true,
      timeZone: 'America/New_York',
      transportType: 'TCP',
      hi2Interfaces: [{
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: 'some comment',
        connectionType: 'FAILOVER',
        destIPAddr: '1.1.1.1',
        destPort: '1',
        dscp: '10',
        keepAliveFreq: '120',
        keepAliveRetries: '12',
        ownIPAddr: '12.12.12.12',
        ownIPPort: '12',
        reqState: 'INACTIVE',
        state: '',
        tpkt: false,
        version: '15.5',
      }, {
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: '',
        connectionType: 'PRIMARY',
        destIPAddr: '13.13.13.13',
        destPort: '13',
        dscp: '0',
        keepAliveFreq: '60',
        keepAliveRetries: '6',
        ownIPAddr: '',
        ownIPPort: '',
        reqState: 'ACTIVE',
        state: '',
        tpkt: true,
        version: '15.6',
      }],
      hi3Interfaces: [{
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: 'some comment 2',
        connectionType: 'FAILOVER',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        displayLabel: 'some label',
        dscp: '20',
        keepAliveFreq: '220',
        keepAliveRetries: '22',
        inactivityTimer: '20',
        ownIPAddr: '22.22.22.22',
        ownIPPort: '22',
        tpkt: false,
        version: '15.5',
        useIpdu: false,
      }, {
        id: expect.stringContaining('.collectionFunctionInterface'),
        name: '',
        transportType: 'TCP',
        comments: '',
        connectionType: 'PRIMARY',
        destIPAddr: '23.23.23.23',
        destPort: '23',
        displayLabel: '',
        dscp: '0',
        keepAliveFreq: '60',
        keepAliveRetries: '6',
        inactivityTimer: '0',
        ownIPAddr: '',
        ownIPPort: '',
        tpkt: true,
        version: '15.6',
        useIpdu: true,
      }],
    });
  });

  it('should not render add and remove button in view mode', () => {
    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing={false}
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should show an Add button when the data is an empty array', () => {
    collectionFunction.hi3Interfaces = [];

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
      />,
    );

    expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();
  });

  it('should call onInterfaceAdd when clicking in the add button when there is no data', () => {
    collectionFunction.hi3Interfaces = [];
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionForm
        title="Form title"
        isEditing
        data={collectionFunction}
        template={adaptedTemplates[templateKey]}
        onFieldUpdate={() => undefined}
        onClickSave={() => undefined}
        onClickCancelClose={() => undefined}
        onInterfaceAdd={mockCallback}
      />,
    );

    expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();

    fireEvent.click(screen.getByTestId('CollectionFunctionInterfaceFormAdd'));

    expect(mockCallback).toBeCalledWith('hi3Interfaces');
  });

  describe('when template is CF-JSTD025A', () => {

    beforeEach(() => {

      templateKey = 'CF-JSTD025A';
      collectionFunction = createCollectionFunctionBasedOnTemplate(collectionFunctionTemplates[templateKey]);

      adaptedTemplates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
    });

    it('should render the main, facility information, IRI and not render CC', () => {
      render(
        <CollectionFunctionForm
          title="Form title"
          isEditing
          data={collectionFunction}
          template={adaptedTemplates[templateKey]}
          onFieldUpdate={() => undefined}
          onClickSave={() => undefined}
          onClickCancelClose={() => undefined}
        />,
      );

      expect(screen.getByTestId('CollectionFunctionFormMain')).toBeVisible();
      expect(screen.getByText('Name:')).toBeVisible();
      expect(screen.getByText('Handover Protocol:')).toBeVisible();
      expect(screen.getByText('LEA:')).toBeVisible();

      expect(screen.getByTestId('CollectionFunctionFormFacilityInformation')).toBeVisible();
      expect(screen.getByText('Delivery Type:')).toBeVisible();
      expect(screen.getByText('Time Zone:')).toBeVisible();
      expect(screen.getByText('Contact:')).toBeVisible();
      expect(screen.getByText('Grace Timer:')).toBeVisible();
      expect(screen.getByText('Xcipio is behind NAT')).toBeVisible();

      expect(screen.getByText('Intercept Related Information (IRI)')).toBeVisible();

      expect(document.querySelector('[data-testid="CollectionFunctionFormCC"')).toBeFalsy();
    });

    it('should render the Intercept Related Information (IRI) form as expected', () => {
      const updateHandler = jest.fn();
      render(
        <CollectionFunctionForm
          title="Form title"
          isEditing
          data={collectionFunction}
          template={adaptedTemplates[templateKey]}
          onFieldUpdate={updateHandler}
          onClickSave={() => undefined}
          onClickCancelClose={() => undefined}
        />,
      );

      const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
      fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

      const tab = getByRole(interfaces, 'tabpanel');

      expect(getByText(tab, 'IP Address:')).toBeVisible();
      expect(getByText(tab, 'Port:')).toBeVisible();
      expect(getByText(tab, 'Configured State:')).toBeVisible();
      expect(getByText(tab, 'XCP IP Address:')).toBeVisible();
      expect(getByText(tab, 'XCP Port (TCP):')).toBeVisible();
      expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
      expect(getByText(tab, 'Trace Level:')).toBeVisible();
      expect(getByText(tab, 'Comments:')).toBeVisible();
    });
  });

  describe('when template is CF-JSTD025B', () => {

    beforeEach(() => {

      templateKey = 'CF-JSTD025B';
      collectionFunction = createCollectionFunctionBasedOnTemplate(collectionFunctionTemplates[templateKey]);

      adaptedTemplates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
    });

    it('should render the main, facility information, IRI and not render CC', () => {
      render(
        <CollectionFunctionForm
          title="Form title"
          isEditing
          data={collectionFunction}
          template={adaptedTemplates[templateKey]}
          onFieldUpdate={() => undefined}
          onClickSave={() => undefined}
          onClickCancelClose={() => undefined}
        />,
      );

      expect(screen.getByTestId('CollectionFunctionFormMain')).toBeVisible();
      expect(screen.getByText('Name:')).toBeVisible();
      expect(screen.getByText('Handover Protocol:')).toBeVisible();
      expect(screen.getByText('LEA:')).toBeVisible();

      expect(screen.getByTestId('CollectionFunctionFormFacilityInformation')).toBeVisible();
      expect(screen.getByText('Delivery Type:')).toBeVisible();
      expect(screen.getByText('Time Zone:')).toBeVisible();
      expect(screen.getByText('Contact:')).toBeVisible();
      expect(screen.getByText('Grace Timer:')).toBeVisible();
      expect(screen.getByText('Xcipio is behind NAT')).toBeVisible();

      expect(screen.getByText('Intercept Related Information (IRI)')).toBeVisible();

      expect(document.querySelector('[data-testid="CollectionFunctionFormCC"')).toBeFalsy();
    });

    it('should render the Intercept Related Information (IRI) form as expected', () => {
      const updateHandler = jest.fn();
      render(
        <CollectionFunctionForm
          title="Form title"
          isEditing
          data={collectionFunction}
          template={adaptedTemplates[templateKey]}
          onFieldUpdate={updateHandler}
          onClickSave={() => undefined}
          onClickCancelClose={() => undefined}
        />,
      );

      const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
      fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

      const tab = getByRole(interfaces, 'tabpanel');

      expect(getByText(tab, 'IP Address:')).toBeVisible();
      expect(getByText(tab, 'Port:')).toBeVisible();
      expect(getByText(tab, 'Configured State:')).toBeVisible();
      expect(getByText(tab, 'XCP IP Address:')).toBeVisible();
      expect(getByText(tab, 'XCP Port (TCP):')).toBeVisible();
      expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
      expect(getByText(tab, 'Trace Level:')).toBeVisible();
      expect(getByText(tab, 'Comments:')).toBeVisible();
    });

    it('should render the Communication Content (CC) form as expected', () => {
      const updateHandler = jest.fn();
      render(
        <CollectionFunctionForm
          title="Form title"
          isEditing
          data={collectionFunction}
          template={adaptedTemplates[templateKey]}
          onFieldUpdate={updateHandler}
          onClickSave={() => undefined}
          onClickCancelClose={() => undefined}
        />,
      );

      expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();
    });
  });
});
