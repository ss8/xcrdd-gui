import { Ref } from './CollectionFunctionForm';

export { default } from './CollectionFunctionForm';
export type CollectionFunctionFormRef = Ref;
