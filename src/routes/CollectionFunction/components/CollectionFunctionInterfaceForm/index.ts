import { Ref } from './CollectionFunctionInterfaceForm';

export { default } from './CollectionFunctionInterfaceForm';
export type CollectionFunctionInterfaceFormRef = Ref;
