import React, {
  forwardRef,
  MutableRefObject,
  RefObject,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import { Button } from '@blueprintjs/core';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import * as types from 'data/collectionFunction/collectionFunction.types';
import MultiTab from 'shared/multi-tab';
import { TabProp } from 'shared/multi-tab/MultiTab';
import Form, {
  CustomFormFieldComponent,
  FieldUpdateHandlerParam,
} from 'shared/form';
import { getInterfaceFormTemplateSection, adaptInterfaceTemplateState } from '../../utils/collectionFunctionTemplate.adapter';
import hideInterfaceSection from '../../utils/hideInterfaceSection';
import showInterfaceProtocolForm from '../../utils/showInterfaceProtocolForm';

type Props = {
  id: string
  title: string
  tabTitle: string
  formSection: 'hi2Interfaces' | 'hi3Interfaces'
  isEditing: boolean
  template: FormTemplate
  data: Partial<types.CollectionFunctionFormData>
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent }
  onFieldUpdate?: (param: FieldUpdateHandlerParam, formSection?: 'hi2Interfaces' | 'hi3Interfaces', tabId?: string) => void
  onInterfaceAdd?: (formSection: 'hi2Interfaces' | 'hi3Interfaces') => void
  onInterfaceRemove?: (id: string | number, formSection: 'hi2Interfaces' | 'hi3Interfaces') => void
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => types.CollectionFunctionInterfaceFormData[]
  getMainFormValues: () => types.CollectionFunctionInterfaceFormData[]
}

export const CollectionFunctionInterfaceForm = forwardRef<Ref, Props>(({
  id,
  title,
  tabTitle,
  formSection,
  isEditing,
  template,
  data,
  customFieldComponentMap,
  onFieldUpdate = () => null,
  onInterfaceAdd = () => null,
  onInterfaceRemove = () => null,
}: Props, ref) => {
  const interfaceRefs = useRef<(RefObject<Form>)[]>([]);
  const [invalidInterfacesTabs, setInvalidInterfacesTabs] = useState<{ [id: string]: boolean }>({});
  const mainFormRef = useRef<Form>(null);

  const formTemplateSection = getInterfaceFormTemplateSection(formSection, data, template);

  const getValidTabs = (
    tabFormRefs: MutableRefObject<RefObject<Form>[]>,
    forceValidation = false,
    forceTouched = true,
  ) => {
    const validFormTabs = tabFormRefs.current
      .filter((formRef) => !!formRef.current)
      .map((formRef) => formRef.current?.isValid(forceValidation, forceTouched) ?? true);

    return validFormTabs;
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const validInterfaces = getValidTabs(interfaceRefs, forceValidation, forceTouched);
    const allInterfacesValid = validInterfaces.every((valid) => valid === true);

    const invalidInterfaces = validInterfaces.reduce((previousValue, entry, index) => {
      const interfaceData = data[formSection];

      if (interfaceData == null) {
        return previousValue;
      }

      return {
        ...previousValue,
        [interfaceData[index].id]: !entry,
      };
    }, {});

    setInvalidInterfacesTabs(invalidInterfaces);

    const mainFormValid = mainFormRef.current?.isValid(forceValidation, forceTouched) ?? true;

    return allInterfacesValid && mainFormValid;
  };

  const getValues = () => {
    const formValues: types.CollectionFunctionInterfaceFormData[] = [
      ...interfaceRefs.current
        .filter((interfaceRef) => !!interfaceRef.current)
        .map((interfaceRef) => interfaceRef.current?.getValues()),
    ];

    return formValues;
  };

  const getMainFormValues = () => {
    return mainFormRef.current?.getValues();
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
    getMainFormValues,
  }));

  const createInterfaceTab = (
    tabData: Partial<types.CollectionFunctionInterfaceFormData> = {},
    index: number,
  ): TabProp | undefined => {
    if (!formTemplateSection) {
      return;
    }
    if (tabData.id == null) {
      return;
    }

    const interfaceRef: { current: Form | null } = {
      current: null,
    };

    interfaceRefs.current.push(interfaceRef);

    const refCallback = (node: Form | null) => {
      interfaceRef.current = node;
    };

    const handleFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, formSection, tabData.id);

    const panel = (
      <Form
        ref={refCallback}
        name={template.key}
        defaults={tabData}
        fields={adaptInterfaceTemplateState((formTemplateSection as FormTemplateSection).fields, tabData, isEditing)}
        layout={(formTemplateSection as FormTemplateSection).metadata.layout?.rows}
        collapsibleLayout={(formTemplateSection as FormTemplateSection).metadata.layout?.collapsible}
        fieldUpdateHandler={handleFieldUpdate}
        customFieldComponentMap={customFieldComponentMap}
      />
    );

    return {
      id: tabData.id,
      title: `${tabTitle} ${index + 1}`,
      panel,
      hasErrors: !!invalidInterfacesTabs?.[tabData.id] ?? false,
    };
  };

  const onTabAdd = () => onInterfaceAdd(formSection);
  const onTabRemove = (tabId: string | number) => onInterfaceRemove(tabId, formSection);
  const onTabFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, formSection);

  const tabs = data[formSection]?.map(createInterfaceTab).filter((entry): entry is TabProp => entry != null) ?? [];

  if (hideInterfaceSection(formTemplateSection, isEditing, tabs)) {
    return null;
  }

  if (tabs.length === 0) {
    return (
      <div data-testid={`CollectionFunctionInterfaceForm${id}`}>
        <h3>{title}</h3>
        <Button data-testid="CollectionFunctionInterfaceFormAdd" icon="plus" onClick={onTabAdd} className="add-button" />
      </div>
    );
  }

  return (
    <div data-testid={`CollectionFunctionInterfaceForm${id}`}>
      <h3>{title}</h3>
      {showInterfaceProtocolForm(template[formSection] as FormTemplateSection) && (
        <Form
          name={template.key}
          ref={mainFormRef}
          defaults={data[formSection]?.[0]}
          fields={(template[formSection] as FormTemplateSection).fields}
          layout={(template[formSection] as FormTemplateSection).metadata.layout?.rows}
          fieldUpdateHandler={onTabFieldUpdate}
        />
      )}
      <div className="multi-tab-form-section" data-testid={`MultiTabFormSection${id}`}>
        <MultiTab
          tabContainerId={id}
          tabs={tabs}
          addTab={onTabAdd}
          removeTab={onTabRemove}
          minNumberOfTabs={formTemplateSection?.metadata.min}
          maxNumberOfTabs={formTemplateSection?.metadata.max}
          isEditing={isEditing}
        />
      </div>
    </div>
  );
});

export default CollectionFunctionInterfaceForm;
