import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getByRole,
  getByTestId,
  getByText,
  queryByLabelText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { CollectionFunctionFormData, CollectionFunctionInterfaceFormData } from 'data/collectionFunction/collectionFunction.types';
import { FormTemplate } from 'data/template/template.types';
import { getByDataTest } from '__test__/customQueries';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { mockNodes } from '__test__/mockNodes';
import CollectionFunctionInterfaceForm from './CollectionFunctionInterfaceForm';
import { adaptTemplateOptions } from '../../utils/collectionFunctionTemplate.adapter';
import { createCollectionFunctionBasedOnTemplate } from '../../utils/collectionFunctionTemplate.factory';
import { CollectionFunctionInterfaceFormRef } from '.';

describe('<CollectionFunctionInterfaceForm />', () => {
  let collectionFunctionTemplates: { [key: string]: FormTemplate };
  let collectionFunction: Partial<CollectionFunctionFormData>;
  let adaptedTemplates: { [key: string]: FormTemplate };
  let templateKey: string;
  let template: FormTemplate;

  beforeEach(() => {
    collectionFunctionTemplates = {
      'CF-3GPP-33108': getMockTemplateByKey('CF-3GPP-33108'),
      'CF-ETSI-102232': getMockTemplateByKey('CF-ETSI-102232'),
      'CF-TIA-1072': getMockTemplateByKey('CF-TIA-1072'),
      'CF-JSTD025A': getMockTemplateByKey('CF-JSTD025A'),
    };

    templateKey = 'CF-3GPP-33108';

    adaptedTemplates = adaptTemplateOptions(
      collectionFunctionTemplates,
      mockTimeZones,
      mockContacts,
      mockCollectionFunctionGroups,
      mockFtpInfos,
      mockNetworks,
      mockSecurityInfos,
      mockFqdnGroups,
      mockSupportedFeatures.config.collectionFunctions,
      mockNodes,
    );

    collectionFunction = createCollectionFunctionBasedOnTemplate(adaptedTemplates[templateKey], true);
    template = adaptedTemplates[templateKey];
  });

  it('should render the Intercept Related Information (IRI) form as expected', () => {
    render(
      <CollectionFunctionInterfaceForm
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(iriContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Version:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'XCP IP Address:')).toBeVisible();
    expect(getByText(tab, 'XCP Port (TCP):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
    expect(getByText(tab, 'Connection Type:')).toBeVisible();
    expect(getByText(tab, 'TPKT')).toBeVisible();
    expect(getByText(tab, 'Comments:')).toBeVisible();
  });

  it('should render the Communication Content (CC) form as expected', () => {
    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    expect(getByText(ccContainer, 'Interface 1')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'CollapsibleFormButton'));

    const tab = getByRole(interfaces, 'tabpanel');

    expect(getByText(tab, 'IP Address:')).toBeVisible();
    expect(getByText(tab, 'Port:')).toBeVisible();
    expect(getByText(tab, 'Version:')).toBeVisible();
    expect(getByText(tab, 'XDP IP Address:')).toBeVisible();
    expect(getByText(tab, 'XDP Port:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Interval:')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
    expect(getByText(tab, 'Connection Type:')).toBeVisible();
    expect(getByText(tab, 'Use XDP')).toBeVisible();
    expect(getByText(tab, 'TPKT')).toBeVisible();
    expect(getByText(tab, 'Timeout After (secs):')).toBeVisible();
    expect(getByText(tab, 'Template Display Label:')).toBeVisible();
    expect(getByText(tab, 'Comments:')).toBeVisible();
  });

  it('should not render the Communication Content (CC) form as expected when Server returns J25-A data with HI3', () => {
    templateKey = 'CF-JSTD025A';
    collectionFunction =
    {
      id: 'VG9ueV9QYWNrZXREYXRh',
      dxOwner: 'dxOwner',
      dxAccess: 'dxAccess',
      name: 'j25a-cf',
      bufferCC: false,
      cellTowerLocation: true,
      deliveryType: 'DSR',
      graceTimer: '0',
      insideNat: false,
      type: 'CALEA',
      tag: 'CF-JSTD025A',
      timeZone: 'US/Central',
      hi2Interfaces: [
        {
          id: 'VG9ueV9QYWNrZXREYXRhIzE',
          name: 'IP:172.16.162.46 PORT:10072',
          transportType: 'TCP',
        },
      ],
      hi3Interfaces: [
        {
          id: 'VG9ueV9QYWNrZXREYXRhIzE',
          name: 'IP:172.16.162.46 PORT:10073',
          transportType: 'TCP',
        },
      ],
    };
    template = adaptedTemplates[templateKey];
    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    expect(screen.queryByTestId('CollectionFunctionInterfaceFormCC')).toBeNull();
  });

  xit('should set the Transport Type as expected', () => {
    (collectionFunction.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';

    render(
      <CollectionFunctionInterfaceForm
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');

    expect(queryByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('Over FTP');
  });

  it('should hide the Transport Type form if its field is hidden', () => {
    template = adaptedTemplates['CF-TIA-1072'];

    render(
      <CollectionFunctionInterfaceForm
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(queryByLabelText(iriContainer, 'Transport Protocol:')).toBeNull();
  });

  it('should call onFieldUpdate if any field change', () => {
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={mockCallback}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    fireEvent.change(getByDataTest(ccContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });

    expect(mockCallback).toBeCalledWith(
      {
        label: 'IP Address', name: 'destIPAddr', value: '1.1.1.1', valueLabel: '1.1.1.1',
      },
      'hi3Interfaces',
      expect.stringContaining('.collectionFunctionInterface'),
    );
  });

  it('should call onInterfaceAdd if clicking to add a tab', () => {
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={mockCallback}
        onInterfaceRemove={() => null}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));

    expect(mockCallback).toBeCalledWith('hi3Interfaces');
  });

  it('should call onInterfaceRemove if clicking to remove a tab', () => {
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={mockCallback}
      />,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'MultiTabRemoveIcon'));

    expect(mockCallback).toBeCalledWith(
      expect.stringContaining('.collectionFunctionInterface'),
      'hi3Interfaces',
    );
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<CollectionFunctionInterfaceFormRef>();

    render(
      <CollectionFunctionInterfaceForm
        ref={formRef}
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<CollectionFunctionInterfaceFormRef>();

    render(
      <CollectionFunctionInterfaceForm
        ref={formRef}
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<CollectionFunctionInterfaceFormRef>();

    render(
      <CollectionFunctionInterfaceForm
        ref={formRef}
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<CollectionFunctionInterfaceFormRef>();

    render(
      <CollectionFunctionInterfaceForm
        ref={formRef}
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should return the form values in getValues for required and default values', async () => {
    const formRef = createRef<CollectionFunctionInterfaceFormRef>();

    render(
      <CollectionFunctionInterfaceForm
        ref={formRef}
        id="IRI"
        title="Intercept Related Information (IRI)"
        tabTitle="Interface"
        formSection="hi2Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    expect(formRef.current?.getValues()).toEqual([{
      id: expect.stringContaining('.collectionFunctionInterface'),
      name: '',
      transportType: 'TCP',
      comments: '',
      connectionType: 'PRIMARY',
      destIPAddr: '1.1.1.1',
      destPort: '1',
      dscp: '0',
      keepAliveFreq: '60',
      keepAliveRetries: '6',
      ownIPAddr: '',
      ownIPPort: '',
      reqState: 'ACTIVE',
      state: '',
      tpkt: true,
      version: '15.6',
    }]);
  });

  it('should not render add and remove button in view mode', () => {
    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing={false}
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should show an Add button when the data is an empty array', () => {
    collectionFunction.hi3Interfaces = [];

    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={() => null}
        onInterfaceRemove={() => null}
      />,
    );

    expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();
  });

  it('should call onInterfaceAdd when clicking in the add button when there is no data', () => {
    collectionFunction.hi3Interfaces = [];
    const mockCallback = jest.fn();

    render(
      <CollectionFunctionInterfaceForm
        id="CC"
        title="Communication Content (CC)"
        tabTitle="Interface"
        formSection="hi3Interfaces"
        isEditing
        template={template}
        data={collectionFunction}
        onFieldUpdate={() => null}
        onInterfaceAdd={mockCallback}
        onInterfaceRemove={() => null}
      />,
    );

    expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();

    fireEvent.click(screen.getByTestId('CollectionFunctionInterfaceFormAdd'));

    expect(mockCallback).toBeCalledWith('hi3Interfaces');
  });
});
