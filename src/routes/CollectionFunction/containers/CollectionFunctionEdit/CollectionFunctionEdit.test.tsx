import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import {
  fireEvent,
  getAllByTestId,
  getByDisplayValue,
  getByLabelText,
  getByRole,
  getByTestId,
  getByText,
  queryAllByLabelText,
  queryByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import Http from 'shared/http';
import store from 'data/store';
import { MemoryRouter } from 'react-router-dom';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as collectionFunctionTypes from 'data/collectionFunction/collectionFunction.types';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as ftpInfoTypes from 'data/ftpInfo/ftpInfo.types';
import * as networkTypes from 'data/network/network.types';
import * as securityInfoTypes from 'data/securityInfo/securityInfo.types';
import * as fqdnGroupTypes from 'data/fqdnGroup/fqdnGroup.types';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { getByDataTest } from '__test__/customQueries';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { CF_ROUTE } from '../../../../constants';
import CollectionFunctionEdit from './CollectionFunctionEdit';
import CollectionFunction from '../../CollectionFunction';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunctionEdit />', () => {
  let collectionFunction: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSI: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSIFTP: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSIWithCountryDisabled: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSIWithCountryIE: collectionFunctionTypes.CollectionFunction;
  let collectionFunction1072: collectionFunctionTypes.CollectionFunction;
  let collectionFunction1066: collectionFunctionTypes.CollectionFunction;
  let collectionFunction1066UDP: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionWithUnsupportedTimezone: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionATIS1000678: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionATIS1000678UDP: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionJSTD025A: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionJSTD025B: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionATIS0700005: collectionFunctionTypes.CollectionFunction;

  beforeEach(() => {
    collectionFunction = {
      id: 'ID_TO_GET',
      dxOwner: 'dxOwner',
      dxAccess: 'dxAccess',
      name: 'CA-DOJBNE-33108',
      bufferCC: false,
      cellTowerLocation: false,
      contact: {
        id: 'Q29udGFjdCAx',
        name: 'Contact 1',
      },
      deliveryType: 'DSR',
      insideNat: false,
      type: 'GPRS',
      tag: 'CF-3GPP-33108',
      timeZone: 'America/Los_Angeles',
      cfGroup: {
        id: 'id1',
        name: 'CA-DOJBNE',
      },
      graceTimer: '10',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '15.6' },
          { key: 'traceLevel', value: '0' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'comments', value: 'some comments' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'dscp', value: '0' },
          { key: 'destPort', value: '1' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'tpkt', value: 'Y' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'ownIPAddr', value: '12.12.12.12' },
          { key: 'cfid', value: 'CA-DOJBNE-33108' },
          { key: 'ownIPPort', value: '12' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'label 2' },
          { key: 'dscp', value: '10' },
          { key: 'keepAliveFreq', value: '120' },
          { key: 'keepAliveRetries', value: '20' },
          { key: 'inactivityTimer', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
          { key: 'useIpdu', value: 'N' },
        ],
      }],
    };

    collectionFunctionWithUnsupportedTimezone = cloneDeep(collectionFunction);
    collectionFunctionWithUnsupportedTimezone.timeZone = 'America/Tijuana';

    collectionFunction1072 = {
      ...collectionFunction,
      id: 'ID_TO_GET_3',
      name: 'CA-DOJBNE-1072',
      tag: 'CF-TIA-1072',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'ownIPAddr', value: '11.11.11.11' },
          { key: 'ownIPPort', value: '11' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: '' },
          { key: 'version', value: 'TIA1072' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'some label' },
          { key: 'dscp', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'version', value: 'TIA1072' },
        ],
      }],
    };

    collectionFunction1066 = {
      ...collectionFunction,
      id: 'ID_TO_GET_4',
      name: 'CA-DOJBNE-1066',
      tag: 'CF-TIA-1066',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'ownIPAddr', value: '11.11.11.11' },
          { key: 'ownIPPort', value: '11' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'version', value: '4' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'some label' },
          { key: 'dscp', value: '20' },
          { key: 'filter', value: 'VO' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'version', value: '4' },
          { key: 'transport', value: 'TCP' },
        ],
      }],
    };

    collectionFunction1066UDP = {
      ...collectionFunction1066,
      id: 'ID_TO_GET_5',
      hi3Interfaces: [{
        ...collectionFunction1066.hi3Interfaces?.[0],
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'UDP',
        options: [
          ...collectionFunction1066.hi3Interfaces?.[0].options ?? [],
          { key: 'transport', value: 'UDP' },
        ],
      }],
    };

    collectionFunctionATIS1000678 = {
      ...collectionFunction,
      id: 'ID_TO_GET_6',
      name: 'CF-ATIS-1000678',
      tag: 'CF-ATIS-1000678',
      type: 'T1.678',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:11.22.11.22 PORT:1212',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '2' },
          { key: 'traceLevel', value: '0' },
          { key: 'comments', value: 'a comment in atis iri' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'dscp', value: '10' },
          { key: 'destPort', value: '1212' },
          { key: 'destIPAddr', value: '11.22.11.22' },
          { key: 'ownIPAddr', value: '11.11.22.22' },
          { key: 'ownIPPort', value: '2121' },
          { key: 'reqState', value: 'ACTIVE' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:33.22.33.11 PORT:2121',
        transportType: 'TCP',
        options: [
          { key: 'displayLabel', value: 'Temp D Lab' },
          { key: 'filter', value: 'VO' },
          { key: 'uri', value: '' },
          { key: 'transport', value: 'TCP' },
          { key: 'traceLevel', value: '1' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'secinfoid', value: 'NONE' },
          { key: 'role', value: 'CLIENT' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'tpkt', value: 'N' },
          { key: 'destPort', value: '2121' },
          { key: 'destIPAddr', value: '33.22.33.11' },
          { key: 'ownnetid', value: '' },
          { key: 'appProtocol', value: '' },
          { key: 'countryCode', value: 'NONE' },
          { key: 'ifid', value: '1' },
          { key: 'version', value: '2' },
          { key: 'dscp', value: '25' },
          { key: 'encryptKey', value: '*****' },
          { key: 'sshInfoId', value: '' },
          { key: 'encryptMethod', value: 'NONE' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
        ],
      }],
    };

    collectionFunctionATIS1000678UDP = {
      ...collectionFunctionATIS1000678,
      id: 'ID_TO_GET_7',
      hi3Interfaces: [{
        ...collectionFunction1066.hi3Interfaces?.[0],
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'UDP',
        options: [
          ...collectionFunctionATIS1000678.hi3Interfaces?.[0].options ?? [],
          { key: 'transport', value: 'UDP' },
        ],
      }],
    };

    collectionFunctionETSI = {
      ...collectionFunction,
      id: 'ID_TO_GET_8',
      type: 'ETSIIP',
      name: 'CF-ETSI-102232',
      tag: 'CF-ETSI-102232',
      insideNat: true,
      bufferCC: true,
      hi2Interfaces: [{
        id: 'id1',
        name: '',
        transportType: 'TCP',
        options: [
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'countryCode', value: 'US' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '61' },
          { key: 'hashCert', value: 'a123' },
          { key: 'hashNum', value: '11' },
          { key: 'hashSec', value: '61' },
          { key: 'icheck', value: 'SIGNEDHASH' },
          { key: 'ownnetid', value: 'WENQdjY' },
          { key: 'pldEncKey', value: '' },
          { key: 'pldEncType', value: 'NONE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'secinfoid', value: '' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'useHi3', value: 'Y' },
          { key: 'version', value: '3.17.1' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: '',
        transportType: 'TCP',
        options: [
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'countryCode', value: 'US' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'destType', value: 'ipAddress' },
          { key: 'displayLabel', value: 'label 1' },
          { key: 'fsid', value: '' },
          { key: 'dscp', value: '62' },
          { key: 'hashCert', value: '' },
          { key: 'hashNum', value: '10' },
          { key: 'hashSec', value: '60' },
          { key: 'icheck', value: 'OFF' },
          { key: 'ownnetid', value: 'WFRQMTI4djRfV0dX' },
          { key: 'pldEncKey', value: '' },
          { key: 'pldEncType', value: 'NONE' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'secinfoid', value: '' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'traceLevel', value: '3' },
          { key: 'version', value: '3.17.1' },
        ],
      }],
    };

    collectionFunctionETSIFTP = {
      ...collectionFunctionETSI,
      id: 'ID_TO_GET_9',
      hi2Interfaces: [{
        id: 'id1',
        name: '',
        transportType: 'FTP',
        options: [
          { key: 'countryCode', value: 'TR' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPath', value: '/tmp' },
          { key: 'destPort', value: '1' },
          { key: 'fileMode', value: 'ALLINONE' },
          { key: 'ftpFileSec', value: '5' },
          { key: 'ftpFileSize', value: '64' },
          { key: 'ftpType', value: 'FTP' },
          { key: 'hashCert', value: '' },
          { key: 'hashNum', value: '10' },
          { key: 'hashSec', value: '60' },
          { key: 'icheck', value: 'OFF' },
          { key: 'operatorId', value: '1' },
          { key: 'pldEncKey', value: '123' },
          { key: 'pldEncType', value: 'AES-192-CBC' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
          { key: 'userName', value: 'user' },
          { key: 'userPassword', value: '*****' },
          { key: 'version', value: '3.15.1' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: '',
        transportType: 'FTP',
        options: [
          { key: 'countryCode', value: 'NONE' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPath', value: '/test/dir1' },
          { key: 'destPort', value: '11' },
          { key: 'displayLabel', value: 'label 1' },
          { key: 'fileMode', value: 'CASE' },
          { key: 'ftpFileSec', value: '60' },
          { key: 'ftpFileSize', value: '2048' },
          { key: 'ftpInfoId', value: 'ZnRwMQ' },
          { key: 'ftpType', value: 'SFTP' },
          { key: 'hashCert', value: '' },
          { key: 'hashNum', value: '10' },
          { key: 'hashSec', value: '60' },
          { key: 'icheck', value: 'OFF' },
          { key: 'operatorId', value: '1' },
          { key: 'pldEncKey', value: '' },
          { key: 'pldEncType', value: 'NONE' },
          { key: 'userName', value: 'test-1' },
          { key: 'userPassword', value: '*****' },
          { key: 'version', value: '3.21.1' },
        ],
      }],
    };

    collectionFunctionETSIWithCountryDisabled = {
      ...collectionFunctionETSI,
      id: 'ID_TO_GET_10',
      hi2Interfaces: [{
        ...collectionFunctionETSI?.hi2Interfaces?.[0],
        id: 'id1',
        name: '',
        options: [
          ...collectionFunctionETSI?.hi2Interfaces?.[0].options ?? [],
          { key: 'countryCode', value: 'NONE' },
          { key: 'version', value: '1.4.1' },
        ],
      }],
    };

    collectionFunctionETSIWithCountryIE = {
      ...collectionFunctionETSI,
      id: 'ID_TO_GET_11',
      hi2Interfaces: [{
        ...collectionFunctionETSI?.hi2Interfaces?.[0],
        id: 'id1',
        name: '',
        options: [
          ...collectionFunctionETSI?.hi2Interfaces?.[0].options ?? [],
          { key: 'countryCode', value: 'IE' },
          { key: 'version', value: '2.2.1' },
        ],
      }],
      hi3Interfaces: [{
        ...collectionFunctionETSI?.hi3Interfaces?.[0],
        id: 'id1',
        name: '',
        options: [
          ...collectionFunctionETSI?.hi3Interfaces?.[0].options ?? [],
          { key: 'destType', value: 'domainGroup' },
          { key: 'fsid', value: 'ZG9tYWluMg' },
        ],
      }],
    };

    collectionFunctionJSTD025A = {
      id: 'ID_TO_GET_12',
      name: 'CF-JSTD025A',
      tag: 'CF-JSTD025A',
      dxOwner: 'dxOwner',
      dxAccess: 'dxAccess',
      bufferCC: false,
      cellTowerLocation: false,
      contact: {
        id: 'Q29udGFjdCAx',
        name: 'Contact 1',
      },
      deliveryType: 'DSR',
      insideNat: false,
      type: 'GPRS',
      timeZone: 'America/Los_Angeles',
      cfGroup: {
        id: 'id1',
        name: 'CA-DOJBNE',
      },
      graceTimer: '10',

      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'ownIPAddr', value: '11.11.11.11' },
          { key: 'ownIPPort', value: '11' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'version', value: 'J25-A' },
          { key: 'traceLevel', value: '0' },
        ],
      }],
    };

    collectionFunctionJSTD025B = {
      ...collectionFunction,
      id: 'ID_TO_GET_13',
      name: 'CF-JSTD025B',
      tag: 'CF-JSTD025B',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'ownIPAddr', value: '11.11.11.11' },
          { key: 'ownIPPort', value: '11' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'version', value: 'J25-B' },
          { key: 'traceLevel', value: '0' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'TCP',
        options: [
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'some label' },
          { key: 'dscp', value: '20' },
          { key: 'filter', value: 'a filter name' },
          { key: 'ownnetid', value: 'WENQdjY' },
        ],
      }],
    };

    collectionFunctionATIS0700005 = {
      ...collectionFunction,
      id: 'ID_TO_GET_14',
      name: 'CF-ATIS-0700005',
      tag: 'CF-ATIS-0700005',
      type: 'T1.678',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:11.22.11.22 PORT:1212',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '0700005V2' },
          { key: 'traceLevel', value: '0' },
          { key: 'comments', value: 'a comment in atis iri' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'dscp', value: '10' },
          { key: 'destPort', value: '1212' },
          { key: 'destIPAddr', value: '11.22.11.22' },
          { key: 'ownIPAddr', value: '11.11.22.22' },
          { key: 'ownIPPort', value: '2121' },
          { key: 'reqState', value: 'ACTIVE' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:33.22.33.11 PORT:2121',
        transportType: 'TCP',
        options: [
          { key: 'displayLabel', value: 'Temp D Lab' },
          { key: 'filter', value: 'VO' },
          { key: 'uri', value: '' },
          { key: 'transport', value: 'TCP' },
          { key: 'traceLevel', value: '1' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'secinfoid', value: 'NONE' },
          { key: 'role', value: 'CLIENT' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'tpkt', value: 'N' },
          { key: 'destPort', value: '2121' },
          { key: 'destIPAddr', value: '33.22.33.11' },
          { key: 'ownnetid', value: '' },
          { key: 'appProtocol', value: '' },
          { key: 'countryCode', value: 'NONE' },
          { key: 'ifid', value: '1' },
          { key: 'version', value: '2' },
          { key: 'dscp', value: '25' },
          { key: 'encryptKey', value: '*****' },
          { key: 'sshInfoId', value: '' },
          { key: 'encryptMethod', value: 'NONE' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
        ],
      }],
    };

    store.dispatch({
      type: collectionFunctionTypes.GET_COLLECTION_FUNCTION_PENDING,
      payload: null,
    });

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['cf_view', 'cf_write', 'cf_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: collectionFunctionTypes.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: contactActions.GET_ALL_CONTACTS_FULFILLED,
      payload: buildAxiosServerResponse(mockContacts),
    });

    store.dispatch({
      type: ftpInfoTypes.GET_FTP_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockFtpInfos),
    });

    store.dispatch({
      type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: networkTypes.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(mockNetworks),
    });

    store.dispatch({
      type: securityInfoTypes.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSecurityInfos),
    });

    store.dispatch({
      type: fqdnGroupTypes.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    });

    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${ftpInfoTypes.API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onGet('/cfs/0/ID_TO_GET?$view=options').reply(200, { data: [collectionFunction] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_2?$view=options').reply(200, { data: [collectionFunctionWithUnsupportedTimezone] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_3?$view=options').reply(200, { data: [collectionFunction1072] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_4?$view=options').reply(200, { data: [collectionFunction1066] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_5?$view=options').reply(200, { data: [collectionFunction1066UDP] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_6?$view=options').reply(200, { data: [collectionFunctionATIS1000678] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_7?$view=options').reply(200, { data: [collectionFunctionATIS1000678UDP] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_8?$view=options').reply(200, { data: [collectionFunctionETSI] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_9?$view=options').reply(200, { data: [collectionFunctionETSIFTP] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_10?$view=options').reply(200, { data: [collectionFunctionETSIWithCountryDisabled] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_11?$view=options').reply(200, { data: [collectionFunctionETSIWithCountryIE] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_12?$view=options').reply(200, { data: [collectionFunctionJSTD025A] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_13?$view=options').reply(200, { data: [collectionFunctionJSTD025B] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_14?$view=options').reply(200, { data: [collectionFunctionATIS0700005] });
    axiosMock.onGet('/networks/0').reply(200, { data: mockNetworks });
    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('123.123.123.123') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());
    expect(screen.getByTestId('CollectionFunctionForm')).toBeVisible();
  });

  it('should render the form title with Cancel and Save buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of collection functions when clicking in the cancel button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/edit`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByTestId('CollectionFunctionList')).toBeVisible();
  });

  describe('CF-3GPP-33108', () => {
    it('should load the initial data for CF-3GPP-33108', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByDisplayValue('CA-DOJBNE')).toBeVisible();
      expect(screen.getByDisplayValue('CA-DOJBNE')).toBeEnabled();
      expect(screen.getByDisplayValue('US 3GPP LI (TS 33.108) over TCP or FTP')).toBeVisible();
      expect(screen.getByDisplayValue('US 3GPP LI (TS 33.108) over TCP or FTP')).toBeDisabled();
      expect(getByDataTest(container, 'input-name').getAttribute('value')).toEqual(collectionFunction.name);
      expect(getByDataTest(container, 'input-name')).toBeDisabled();

      expect(screen.getByDisplayValue('DSR')).toBeVisible();
      expect(screen.getByDisplayValue('US/Pacific', { exact: false })).toBeVisible();
      expect(screen.getByDisplayValue(collectionFunction.contact?.name as string)).toBeVisible();

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByDisplayValue(iriContainer, 'Over TCP')).toBeVisible();

      expect(getByDataTest(iriTab, 'input-destIPAddr').getAttribute('value')).toEqual('1.1.1.1');
      expect(getByDataTest(iriTab, 'input-destPort').getAttribute('value')).toEqual('1');
      expect(getByDisplayValue(iriTab, '15.6')).toBeVisible();
      expect(getByDisplayValue(iriTab, 'Active')).toBeVisible();
      expect(getByTestId(iriTab, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByDataTest(iriTab, 'input-ownIPAddr').getAttribute('value')).toEqual('12.12.12.12');
      expect(getByDataTest(iriTab, 'input-ownIPPort').getAttribute('value')).toEqual('12');
      expect(getByDataTest(iriTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('60');
      expect(getByDataTest(iriTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('6');

      expect(getByDataTest(iriTab, 'input-dscp').getAttribute('value')).toEqual('0');
      expect(getByDisplayValue(iriTab, 'Primary')).toBeVisible();
      expect(getByLabelText(iriTab, 'TPKT')).toBeChecked();
      expect(getByDataTest(iriTab, 'input-comments').getAttribute('value')).toEqual('some comments');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByDisplayValue(ccContainer, 'Over TCP')).toBeVisible();

      expect(getByDataTest(ccTab, 'input-destIPAddr').getAttribute('value')).toEqual('2.2.2.2');
      expect(getByDataTest(ccTab, 'input-destPort').getAttribute('value')).toEqual('2');
      expect(getByDisplayValue(ccTab, '15.5')).toBeVisible();

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByDataTest(ccTab, 'input-ownIPAddr').getAttribute('value')).toEqual('22.22.22.22');
      expect(getByDataTest(ccTab, 'input-ownIPPort').getAttribute('value')).toEqual('22');
      expect(getByDataTest(ccTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('120');
      expect(getByDataTest(ccTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('20');

      expect(getByDataTest(ccTab, 'input-dscp').getAttribute('value')).toEqual('10');
      expect(getByDisplayValue(ccTab, 'Failover')).toBeVisible();
      expect(getByLabelText(ccTab, 'Use XDP')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'TPKT')).not.toBeChecked();

      expect(getByDataTest(ccTab, 'input-inactivityTimer').getAttribute('value')).toEqual('20');
      expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('label 2');
      expect(getByDataTest(ccTab, 'input-comments').getAttribute('value')).toEqual('some comment 2');
    });

    xit('should load the initial data for CF-3GPP-33108 when FTP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');

      fireEvent.change(getByDisplayValue(iriContainer, 'Over TCP'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(iriContainer, 'Over FTP')).toBeVisible();

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByDataTest(iriTab, 'input-destIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-destPort').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-userName').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-userPassword').getAttribute('value')).toEqual('');

      expect(getByDataTest(iriTab, 'input-destPath').getAttribute('value')).toEqual('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

      fireEvent.change(getByDisplayValue(ccContainer, 'Over TCP'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(ccContainer, 'Over FTP')).toBeVisible();

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByDisplayValue(ccTab, 'Select a FTP info ID')).toBeVisible();
      expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('');
    });

    xit('should load the FTP Info data from the select one for CF-3GPP-33108 CC interface', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(ccContainer, 'Over FTP')).toBeVisible();

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toBeVisible();
      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveValue('');

      fireEvent.change(getByLabelText(ccTab, 'FTP Info ID:*'), { target: { value: 'ZnRwMQ' } });

      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue(mockFtpInfos[0].ipAddress);
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue(mockFtpInfos[0].port);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');

      fireEvent.click(getByTestId(ccTab, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue(mockFtpInfos[0].username);
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue(mockFtpInfos[0].userPassword);
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue(mockFtpInfos[0].ftpFileTransferTimeout);
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue(mockFtpInfos[0].ftpFileSize);
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue(mockFtpInfos[0].operatorId);
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveValue(mockFtpInfos[0].fileMode);
      expect(getByLabelText(ccTab, 'Path:')).toHaveValue(mockFtpInfos[0].destPath);
    });

    xit('should clean up the FTP Info data for CF-3GPP-33108 CC interface if unselecting it', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(ccContainer, 'Over FTP')).toBeVisible();

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toBeVisible();
      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveValue('');

      fireEvent.change(getByLabelText(ccTab, 'FTP Info ID:*'), { target: { value: 'ZnRwMQ' } });

      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue(mockFtpInfos[0].ipAddress);
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue(mockFtpInfos[0].port);

      fireEvent.change(getByLabelText(ccTab, 'FTP Info ID:*'), { target: { value: '' } });

      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');

      fireEvent.click(getByTestId(ccTab, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue('');
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveValue('ALLINONE');
      expect(getByLabelText(ccTab, 'Path:')).toHaveValue('');
    });
  });

  describe('CF-ETSI-102232', () => {
    it('should load the initial data for CF-ETSI-102232', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_8' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CA-DOJBNE');
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ETSI-102232');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('US/Pacific');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 1');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.17.1');
      expect(getByLabelText(iriTab, 'XCP Network ID:*')).toHaveDisplayValue('XCPv6');

      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('61');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('SIGNEDHASH');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('61');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('a123');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeEnabled();

      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeChecked();
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(iriTab, 'Security Info ID:')).toHaveDisplayValue('Select a Security Info ID');
      expect(getByLabelText(iriTab, 'Security Info ID:')).toBeDisabled();

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('Over TCP');

      // expect(getByText(ccTab, 'Destination Address Type:')).toBeVisible();
      // expect(getByLabelText(ccTab, 'IP Address')).toBeChecked();
      // expect(getByLabelText(ccTab, 'Domain Name')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.17.1');

      expect(getByLabelText(ccTab, 'Configured State:')).toHaveDisplayValue('Inactive');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP Network ID:')).toHaveDisplayValue('XTP128v4_WGW');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('62');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Trace Level:')).toHaveDisplayValue('3');

      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('US');
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Security Info ID:')).toHaveDisplayValue('Select a Security Info ID');
      expect(getByLabelText(ccTab, 'Security Info ID:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('label 1');
    });

    xit('should load the initial data for CF-ETSI-102232 when FTP', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_9' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CA-DOJBNE');
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ETSI-102232');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('US/Pacific');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 1');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP']);

      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Username:*')).toHaveValue('user');
      expect(getByLabelText(iriTab, 'Password:*')).toHaveValue('*****');

      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.15.1');
      expect(getByLabelText(iriTab, 'Operator ID:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('TR');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('123');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'File Mode:')).toHaveDisplayValue('IRI for multiple targets per file');
      expect(getByLabelText(iriTab, 'File Transfer Timeout (secs):*')).toHaveValue('5');
      expect(getByLabelText(iriTab, 'File Transfer Maximum Kbytes:*')).toHaveValue('64');

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Destination Directory:')).toHaveValue('/tmp');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP/SFTP']);

      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveDisplayValue('ftp1');
      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue('1.1.1.1');
      expect(getByLabelText(ccTab, 'IP Address:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue('11');
      expect(getByLabelText(ccTab, 'Port:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.21.1');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue('test-1');
      expect(getByLabelText(ccTab, 'Username:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue('*****');
      expect(getByLabelText(ccTab, 'Password:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue('1');
      expect(getByLabelText(ccTab, 'Operator ID:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Destination Directory:')).toHaveValue('/test/dir1');
      expect(getByLabelText(ccTab, 'Destination Directory:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'FTP Type:')).toHaveDisplayValue('FTP');
      expect(getByLabelText(ccTab, 'FTP Type:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveDisplayValue('CC for one target per file');
      expect(getByLabelText(ccTab, 'File Mode:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue('2048');
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('label 1');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Encryption Key:')).toBeDisabled();
    });

    it('should disable the Integrity Check related fields when changing to Off', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_8' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('SIGNEDHASH');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeEnabled();

      fireEvent.change(getByLabelText(iriContainer, 'Integrity Check:'), { target: { value: 'OFF' } });

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();
    });

    it('should disable the Variant / Country Code and Deliver IRI with CC for some versions in TCP', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_8' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '1.4.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.2.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.5.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.3.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeEnabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.21.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeEnabled();
    });

    it('should load the Variant / Country Code and Deliver IRI with CC as disabled for some versions', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_10' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();
    });

    it('should load the Variant / Country Code with the expected list', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_10' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(1);
    });

    it('should load the Variant / Country Code as expected when IE', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_11' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('IE');
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(2);

      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();
    });

    xit('should show/hide IP Address and show/hide Domain Group when changing Destination Address Type', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_11' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'IP Address')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Name')).toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Group:*')).toBeVisible();
      expect(getByLabelText(ccTab, 'Domain Group:*')).toHaveDisplayValue('domain2');
      expect(queryAllByLabelText(ccTab, 'IP Address:*')).toHaveLength(0);

      fireEvent.click(getByLabelText(ccTab, 'IP Address'));

      expect(getByLabelText(ccTab, 'IP Address')).toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Name')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'IP Address:*')).toBeVisible();
      expect(queryAllByLabelText(ccTab, 'Domain Group:*')).toHaveLength(0);
    });
  });

  describe('CF-TIA-1072', () => {
    it('should load the initial data for CF-TIA-1072', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_3' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 PoC (TIA-1072)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-1072');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.11.11');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('some comment');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('20');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('some label');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('some comment 2');
    });
  });

  describe('CF-TIA-1066', () => {
    it('should load the initial data for CF-TIA-1066', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_4' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 VoIP (TIA-1066)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-1066');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.b.2010');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Inactive']);
      expect(getByTestId(iriTab, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.11.11');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('some comment');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('20');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('some label');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('some comment 2');
    });

    it('should load the initial data for CF-TIA-1066 when UDP', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_5' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 VoIP (TIA-1066)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-1066');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.b.2010');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Inactive']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.11.11');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('some comment');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['UDP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('20');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('some label');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('some comment 2');
    });
  });

  describe('CF-ATIS-1000678', () => {
    it('should load the initial data for CF-ATIS-1000678', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_6' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ATIS-1000678');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('11.22.11.22');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1212');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Active']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.22.22');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('2121');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('a comment in atis iri');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('33.22.33.11');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2121');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('25');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('Temp D Lab');
    });

    it('should load the initial data for CF-ATIS-1000678 when UDP', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_7' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ATIS-1000678');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('11.22.11.22');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1212');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Active']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.22.22');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('2121');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('a comment in atis iri');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['UDP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('33.22.33.11');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2121');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('25');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('Temp D Lab');
    });
  });

  describe('CF-ATIS-0700005', () => {
    it('should load the initial data for CF-ATIS-0700005', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_14' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ATIS-0700005');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('11.22.11.22');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1212');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS 0700005.a.2010');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Active']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.22.22');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('2121');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('a comment in atis iri');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('33.22.33.11');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2121');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('25');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('Temp D Lab');
    });

    it('should load the initial data for CF-ATIS-1000678 when UDP', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_7' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-ATIS-1000678');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('11.22.11.22');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1212');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Active']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.22.22');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('2121');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('a comment in atis iri');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['UDP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('33.22.33.11');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('2121');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('22.22.22.22');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('22');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('25');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('Temp D Lab');
    });
  });

  describe('CF-JSTD025A', () => {
    it('should load the initial data forCF-JSTD025A', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_12' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireless/Wireline (J-STD-025A)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-JSTD025A');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(queryByText(iriTab, 'Version:')).toBeNull();
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Inactive']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.11.11');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('some comment');

      expect(queryByText(iriContainer, 'Communication Content (CC)')).toBeNull();
    });
  });

  describe('CF-JSTD025B', () => {
    it('should load the initial data forCF-JSTD025B', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_13' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue(['CA-DOJBNE']);
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 Wireless (J-STD-025B)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('CF-JSTD025B');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue([/US\/Pacific \(GMT-0[78]\)/]);
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue(['Contact 1']);
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('1');
      expect(queryByText(iriTab, 'Version:')).toBeNull();
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue(['Inactive']);

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('11.11.11.11');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('some comment');

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue('2');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Network ID:')).toHaveValue('WENQdjY');
      expect(getByLabelText(ccTab, 'Network ID:')).toHaveDisplayValue('XCPv6 - [abcd:abcd:abcd:abcd::abcd]:0');
      expect(getByLabelText(ccTab, 'Filter Name:')).toHaveValue('a filter name');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('20');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('some label');
    });

  });

  it('should render the new tab in Intercept Related Information (IRI) form as expected', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();
  });

  it('should render the new tab in Communication Content (CC) form as expected', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();
  });

  it('should remove the tab in Intercept Related Information (IRI) form as expected', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();

    fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[0]);

    expect(screen.queryAllByText('Interface 2')).toHaveLength(0);
  });

  it('should remove the new tab in Communication Content (CC) and show the add button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    expect(getByText(ccContainer, 'Interface 1')).toBeVisible();

    fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[0]);

    expect(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd')).toBeVisible();
  });

  it('should load the timezone even if it is not supported', async () => {
    store.dispatch({
      type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
      payload: [{ id: 'ID_TO_GET_2' }],
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    expect(screen.getByLabelText('Time Zone:*')).toHaveValue('America/Tijuana');
  });

  it('should validate fields on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')).toHaveLength(2);
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
    await waitFor(() => expect(getByText(iriContainer, 'Interface 2')).toBeVisible());

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'MultiTabAddButton'));
    await waitFor(() => expect(getByText(ccContainer, 'Interface 2')).toBeVisible());

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());
    await waitFor(() => expect(ccContainer.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean up error indication on tabs if tab is removed', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
    await waitFor(() => expect(getByText(iriContainer, 'Interface 2')).toBeVisible());
    fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
    await waitFor(() => expect(getByText(iriContainer, 'Interface 3')).toBeVisible());

    expect(getByText(iriContainer, 'Interface 1')).toBeVisible();
    expect(getByText(iriContainer, 'Interface 2')).toBeVisible();
    expect(getByText(iriContainer, 'Interface 3')).toBeVisible();

    fireEvent.click(getByText(iriContainer, 'Interface 3'));

    const iriTab2 = getByRole(iriContainer, 'tabpanel');
    fireEvent.change(getByDataTest(iriTab2, 'input-destIPAddr'), { target: { value: '3.3.3.3' } });
    fireEvent.change(getByDataTest(iriTab2, 'input-destPort'), { target: { value: '3' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

    fireEvent.click(getAllByTestId(iriContainer, 'MultiTabRemoveIcon')[1]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should show success message on Save of a valid form', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('CA-DOJBNE-33108 successfully updated.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '123.123.123.123' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Unable to update CA-DOJBNE-33108: Request failed with status code 500.')).toBeVisible());
  });

  it('should render State for existing Interface, but do not render for new interfaces', async () => {
    store.dispatch({
      type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
      payload: [{ id: 'ID_TO_GET_8' }],
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Collection Function')).toBeVisible());

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    // checks Interface 1 render Current State
    let iriTab = getByRole(iriContainer, 'tabpanel');
    expect(iriTab.querySelectorAll('[data-testid="FieldCurrentState"]')).toHaveLength(1);

    // Add Interface 2 and check it doesn't render Current State
    fireEvent.click(getByTestId(screen.getByTestId('MultiTabFormSectionIRI'), 'MultiTabAddButton'));
    fireEvent.click(screen.getByText('Interface 2'));
    iriTab = getByRole(iriContainer, 'tabpanel');
    expect(iriTab.querySelectorAll('[data-testid="FieldCurrentState"]')).toHaveLength(0);

    // Add Interface 3 and check it doesn't render Current State
    fireEvent.click(getByTestId(screen.getByTestId('MultiTabFormSectionIRI'), 'MultiTabAddButton'));
    fireEvent.click(screen.getByText('Interface 3'));
    iriTab = getByRole(iriContainer, 'tabpanel');
    expect(iriTab.querySelectorAll('[data-testid="FieldCurrentState"]')).toHaveLength(0);

    fireEvent.click(screen.getAllByText('Interface 1')[0]);

    iriTab = getByRole(iriContainer, 'tabpanel');
    expect(iriTab.querySelectorAll('[data-testid="FieldCurrentState"]')).toHaveLength(1);
  });

});
