import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import {
  fireEvent,
  getAllByTestId,
  getByDisplayValue,
  getByLabelText,
  getByRole,
  getByTestId,
  getByText,
  queryAllByLabelText,
  queryByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import Http from 'shared/http';
import store from 'data/store';
import { MemoryRouter } from 'react-router-dom';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as collectionFunctionTypes from 'data/collectionFunction/collectionFunction.types';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as ftpInfoTypes from 'data/ftpInfo/ftpInfo.types';
import * as networkTypes from 'data/network/network.types';
import * as securityInfoTypes from 'data/securityInfo/securityInfo.types';
import * as fqdnGroupTypes from 'data/fqdnGroup/fqdnGroup.types';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { getByDataTest } from '__test__/customQueries';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { mockCustomization } from '__test__/mockCustomization';
import { CF_ROUTE } from '../../../../constants';
import CollectionFunctionCreate from './CollectionFunctionCreate';
import CollectionFunction from '../../CollectionFunction';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunctionCreate />', () => {
  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['cf_view', 'cf_write', 'cf_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: collectionFunctionTypes.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: contactActions.GET_ALL_CONTACTS_FULFILLED,
      payload: buildAxiosServerResponse(mockContacts),
    });

    store.dispatch({
      type: ftpInfoTypes.GET_FTP_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockFtpInfos),
    });

    store.dispatch({
      type: networkTypes.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(mockNetworks),
    });

    store.dispatch({
      type: securityInfoTypes.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSecurityInfos),
    });

    store.dispatch({
      type: fqdnGroupTypes.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    });

    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${ftpInfoTypes.API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByTestId('CollectionFunctionCreate')).toBeVisible();
    expect(screen.getByTestId('CollectionFunctionForm')).toBeVisible();
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByText('New Collection Function')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of collection functions when clicking in the cancel button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/create`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByTestId('CollectionFunctionList')).toBeVisible();
  });

  it('should show the warning when there is no template', () => {
    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByText('New Collection Function')).toBeVisible();
    expect(screen.getByText('Unable to load Collection Function templates')).toBeVisible();
  });

  it('should render the list of collection function when clicking in the cancel button and templates are missing', () => {
    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/create`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New Collection Function')).toBeVisible();
  });

  describe('CF-3GPP-33108', () => {
    it('should load the initial data for CF-3GPP-33108', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByDisplayValue('Select a LEA')).toBeVisible();
      expect(screen.getByDisplayValue('US 3GPP LI (TS 33.108) over TCP or FTP')).toBeVisible();
      expect(getByDataTest(container, 'input-name').getAttribute('value')).toEqual('');

      expect(screen.getByDisplayValue('DSR')).toBeVisible();
      expect(screen.getByDisplayValue('Select a time zone')).toBeVisible();
      expect(screen.getByDisplayValue('Select a contact')).toBeVisible();

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByDisplayValue(iriContainer, 'Over TCP')).toBeVisible();

      expect(getByDataTest(iriTab, 'input-destIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-destPort').getAttribute('value')).toEqual('');
      expect(getByDisplayValue(iriTab, '15.6')).toBeVisible();
      expect(getByDisplayValue(iriTab, 'Active')).toBeVisible();

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByDataTest(iriTab, 'input-ownIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-ownIPPort').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('60');
      expect(getByDataTest(iriTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('6');

      expect(getByDataTest(iriTab, 'input-dscp').getAttribute('value')).toEqual('0');
      expect(getByDisplayValue(iriTab, 'Primary')).toBeVisible();
      expect(getByLabelText(iriTab, 'TPKT')).toBeChecked();
      expect(getByDataTest(iriTab, 'input-comments').getAttribute('value')).toEqual('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByDisplayValue(ccContainer, 'Over TCP')).toBeVisible();

      expect(getByDataTest(ccTab, 'input-destIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(ccTab, 'input-destPort').getAttribute('value')).toEqual('');
      expect(getByDisplayValue(ccTab, '15.6')).toBeVisible();

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByDataTest(ccTab, 'input-ownIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(ccTab, 'input-ownIPPort').getAttribute('value')).toEqual('');
      expect(getByDataTest(ccTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('60');
      expect(getByDataTest(ccTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('6');

      expect(getByDataTest(ccTab, 'input-dscp').getAttribute('value')).toEqual('0');
      expect(getByDisplayValue(ccTab, 'Primary')).toBeVisible();
      expect(getByLabelText(ccTab, 'Use XDP')).toBeChecked();
      expect(getByLabelText(ccTab, 'TPKT')).toBeChecked();

      expect(getByDataTest(ccTab, 'input-inactivityTimer').getAttribute('value')).toEqual('0');
      expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('');
      expect(getByDataTest(ccTab, 'input-comments').getAttribute('value')).toEqual('');
    });

    xit('should load the initial data for CF-3GPP-33108 when changing to FTP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');

      fireEvent.change(getByDisplayValue(iriContainer, 'Over TCP'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(iriContainer, 'Over FTP')).toBeVisible();

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByDataTest(iriTab, 'input-destIPAddr').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-destPort').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-userName').getAttribute('value')).toEqual('');
      expect(getByDataTest(iriTab, 'input-userPassword').getAttribute('value')).toEqual('');

      expect(getByDataTest(iriTab, 'input-destPath').getAttribute('value')).toEqual('');

      expect(screen.getByTestId('CollectionFunctionInterfaceFormAdd')).toBeVisible();

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      fireEvent.change(getByDisplayValue(ccContainer, 'Over TCP'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(ccContainer, 'Over FTP')).toBeVisible();

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByDisplayValue(ccTab, 'Select a FTP info ID')).toBeVisible();
      expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('');
    });

    xit('should load the FTP Info data from the select one for CF-3GPP-33108 CC interface', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByDisplayValue(ccContainer, 'Over FTP')).toBeVisible();

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toBeVisible();
      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveValue('');

      fireEvent.change(getByLabelText(ccTab, 'FTP Info ID:*'), { target: { value: 'ZnRwMQ' } });

      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue(mockFtpInfos[0].ipAddress);
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue(mockFtpInfos[0].port);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');

      fireEvent.click(getByTestId(ccTab, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue(mockFtpInfos[0].username);
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue(mockFtpInfos[0].userPassword);
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue(mockFtpInfos[0].ftpFileTransferTimeout);
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue(mockFtpInfos[0].ftpFileSize);
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue(mockFtpInfos[0].operatorId);
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveValue(mockFtpInfos[0].fileMode);
      expect(getByLabelText(ccTab, 'Path:')).toHaveValue(mockFtpInfos[0].destPath);
    });
  });

  describe('CF-ETSI-102232', () => {
    it('should load the initial data for CF-ETSI-102232', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.21.1');
      expect(getByLabelText(iriTab, 'XCP Network ID:*')).toHaveDisplayValue('Select a XCP Network ID');

      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(iriTab, 'Security Info ID:')).toHaveDisplayValue('Select a Security Info ID');
      expect(getByLabelText(iriTab, 'Security Info ID:')).toBeDisabled();

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('Over TCP');

      // expect(getByText(ccTab, 'Destination Address Type:')).toBeVisible();
      // expect(getByLabelText(ccTab, 'IP Address')).toBeChecked();
      // expect(getByLabelText(ccTab, 'Domain Name')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.21.1');

      expect(getByLabelText(ccTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP Network ID:')).toHaveDisplayValue('Select a XDP Network ID');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Trace Level:')).toHaveDisplayValue('None');

      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Security Info ID:')).toHaveDisplayValue('Select a Security Info ID');
      expect(getByLabelText(ccTab, 'Security Info ID:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
    });

    xit('should load the initial data for CF-ETSI-102232 when changing to FTP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(iriContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP']);

      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Username:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Password:*')).toHaveValue('');

      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.21.1');
      expect(getByLabelText(iriTab, 'Operator ID:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'File Mode:')).toHaveDisplayValue('IRI for multiple targets per file');
      expect(getByLabelText(iriTab, 'File Transfer Timeout (secs):*')).toHaveValue('5');
      expect(getByLabelText(iriTab, 'File Transfer Maximum Kbytes:*')).toHaveValue('64');

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Destination Directory:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP/SFTP']);

      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveDisplayValue('Select a FTP info ID');
      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'IP Address:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.21.1');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Username:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Password:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Operator ID:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Destination Directory:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Destination Directory:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'FTP Type:')).toHaveDisplayValue('FTP');
      expect(getByLabelText(ccTab, 'FTP Type:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveDisplayValue('CC for multiple targets per file');
      expect(getByLabelText(ccTab, 'File Mode:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue('');
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Encryption Key:')).toBeDisabled();
    });

    xit('should load the initial data for CF-ETSI-102232 when changing to SFTP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(iriContainer, 'Transport Protocol:'), { target: { value: 'SFTP' } });
      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over SFTP']);

      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Username:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Password:*')).toHaveValue('');

      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.21.1');
      expect(getByLabelText(iriTab, 'Operator ID:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'File Mode:')).toHaveDisplayValue('IRI for multiple targets per file');
      expect(getByLabelText(iriTab, 'File Transfer Timeout (secs):*')).toHaveValue('5');
      expect(getByLabelText(iriTab, 'File Transfer Maximum Kbytes:*')).toHaveValue('64');

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Destination Directory:')).toHaveValue('');
    });

    xit('should load the initial data for CF-ETSI-102232 when changing the FTP Info ID', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP/SFTP']);

      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveDisplayValue('Select a FTP info ID');
      fireEvent.change(getByLabelText(ccTab, 'FTP Info ID:*'), { target: { value: 'ZnRwMQ' } });
      expect(getByLabelText(ccTab, 'FTP Info ID:*')).toHaveDisplayValue('ftp1');

      expect(getByLabelText(ccTab, 'IP Address:')).toHaveValue(mockFtpInfos[0].ipAddress);
      expect(getByLabelText(ccTab, 'IP Address:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Port:')).toHaveValue(mockFtpInfos[0].port);
      expect(getByLabelText(ccTab, 'Port:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.21.1');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Username:')).toHaveValue(mockFtpInfos[0].username);
      expect(getByLabelText(ccTab, 'Username:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Password:')).toHaveValue(mockFtpInfos[0].userPassword);
      expect(getByLabelText(ccTab, 'Password:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Operator ID:')).toHaveValue(mockFtpInfos[0].operatorId);
      expect(getByLabelText(ccTab, 'Operator ID:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Destination Directory:')).toHaveValue(mockFtpInfos[0].destPath);
      expect(getByLabelText(ccTab, 'Destination Directory:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'FTP Type:')).toHaveValue(mockFtpInfos[0].ftpType);
      expect(getByLabelText(ccTab, 'FTP Type:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Mode:')).toHaveValue(mockFtpInfos[0].fileMode);
      expect(getByLabelText(ccTab, 'File Mode:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toHaveValue(mockFtpInfos[0].ftpFileSize);
      expect(getByLabelText(ccTab, 'File Transfer Maximum Kbytes:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toHaveValue(mockFtpInfos[0].ftpFileTransferTimeout);
      expect(getByLabelText(ccTab, 'File Transfer Timeout (secs):')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
    });

    xit('should enable the Integrity Check related fields when changing from Off to SIGNEDHASH', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP/SFTP']);

      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      fireEvent.change(getByLabelText(ccContainer, 'Integrity Check:'), { target: { value: 'SIGNEDHASH' } });

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('SIGNEDHASH');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeEnabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeEnabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeEnabled();
    });

    it('should disable the Variant / Country Code and Deliver IRI with CC for some versions in TCP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '1.4.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.2.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.5.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.3.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeEnabled();

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.21.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeEnabled();
    });

    xit('should disable the Variant / Country Code for some versions in FTP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'FTP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['Over FTP/SFTP']);

      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '1.4.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '2.2.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeEnabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '2.5.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeEnabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '3.3.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeEnabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '3.15.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeEnabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '3.17.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();

      fireEvent.change(getByLabelText(ccTab, 'Version:'), { target: { value: '3.21.1' } });
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toBeDisabled();
    });

    it('should set the Variant / Country Code options depend on the selected version', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('3.21.1');
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(8);

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '1.4.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(1);

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.2.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(2);

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '2.5.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(2);

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.3.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(8);

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.21.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(8);
    });

    it('should set the Variant / Country Code to None when changing the version', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'US' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '1.4.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.21.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'US' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');

      fireEvent.change(getByLabelText(iriTab, 'Version:'), { target: { value: '3.17.1' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('None');
    });

    it('should enable Encryption Key and Security Info ID for some countries', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Security Info ID:')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'US' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Security Info ID:')).toBeDisabled();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'TR' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('TR');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Security Info ID:*')).toBeEnabled();
    });

    it('should set Encryption Key to checked for some countries', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'US' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'NL' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('NL');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeChecked();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'TR' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('TR');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeChecked();

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'GB' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('GB');

      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
    });

    it('should clean up the Encryption Key when it become disabled', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'NL' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('NL');

      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      fireEvent.change(getByLabelText(iriTab, 'Encryption Key:'), { target: { value: '123' } });
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('123');

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'TR' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('TR');

      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('123');

      fireEvent.change(getByLabelText(iriTab, 'Variant / Country Code:'), { target: { value: 'GB' } });
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('GB');

      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
    });

    xit('should show/hide IP Address and show/hide Domain Group when changing Destination Address Type', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'IP Address')).toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Name')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'IP Address:*')).toBeVisible();
      expect(queryAllByLabelText(ccTab, 'Domain Group:*')).toHaveLength(0);

      fireEvent.click(getByLabelText(ccTab, 'Domain Name'));

      expect(getByLabelText(ccTab, 'IP Address')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Name')).toBeChecked();
      expect(getByLabelText(ccTab, 'Domain Group:*')).toBeVisible();
      expect(getByLabelText(ccTab, 'Domain Group:*')).toHaveDisplayValue('Select a Domain Group');
      expect(queryAllByLabelText(ccTab, 'IP Address:*')).toHaveLength(0);
    });
  });

  describe('CF-TIA-1072', () => {
    it('should load the initial data for CF-TIA-1072', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-TIA-1072' } });

      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 PoC (TIA-1072)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('');
    });
  });

  describe('CF-TIA-1066', () => {
    it('should load the initial data for CF-TIA-1066', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-TIA-1066' } });

      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 VoIP (TIA-1066)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');

      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Content Processing:')).toHaveDisplayValue(['All Content']);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('');
    });

    it('should load the initial data for CF-TIA-1066 when changing to UDP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-TIA-1066' } });

      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US cdma2000 VoIP (TIA-1066)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('');
      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');

      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'UDP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['UDP']);
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Content Processing:')).toHaveDisplayValue(['All Content']);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('');
    });
  });

  describe('CF-ATIS-1000678', () => {
    it('should load the initial data for CF-ATIS-1000678', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ATIS-1000678' } });

      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');

      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Content Processing:')).toHaveDisplayValue(['All Content']);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('');
    });

    it('should load the initial data for CF-ATIS-1000678 when changing to UDP', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ATIS-1000678' } });

      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['US Wireline VoIP (T1.678)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('0');

      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      expect(screen.getByLabelText('Cell Tower Location')).not.toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(queryByText(iriContainer, 'Transport Protocol:')).toBeNull();

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');
      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'XCP IP Address:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'XCP Port (TCP):')).toHaveValue('');
      expect(getByLabelText(iriTab, 'DSCP (0-63):')).toHaveValue('0');
      expect(getByLabelText(iriTab, 'Connection Type:')).toHaveDisplayValue('Primary');

      expect(getByLabelText(iriTab, 'Comments:')).toHaveValue('');

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['TCP']);
      fireEvent.change(getByLabelText(ccContainer, 'Transport Protocol:'), { target: { value: 'UDP' } });
      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue(['UDP']);
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('ATIS-1000678.2006');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP IP Address:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'XDP Port:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'DSCP (0-63):')).toHaveValue('0');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Content Processing:')).toHaveDisplayValue(['All Content']);
      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Comments:')).toHaveValue('');
    });
  });

  it('should render the new tab in Intercept Related Information (IRI) form as expected', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();
  });

  it('should render the new tab in Communication Content (CC) form as expected', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();
  });

  it('should remove the tab in Intercept Related Information (IRI) form as expected', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
    fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));
    fireEvent.click(getAllByTestId(interfaces, 'CollapsibleFormButton')[1]);
    expect(screen.getByText('Interface 2')).toBeVisible();

    fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[0]);

    expect(screen.queryAllByText('Interface 2')).toHaveLength(0);
  });

  it('should remove the new tab in Communication Content (CC) and show the add button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

    expect(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd')).toBeVisible();
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

    expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

    const interfaces = screen.getByTestId('MultiTabFormSectionCC');
    expect(getByText(ccContainer, 'Interface 1')).toBeVisible();

    fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[0]);

    expect(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')).toHaveLength(7);
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());
    await waitFor(() => expect(ccContainer.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean up error indication on tabs if tab is removed', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
    fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));

    fireEvent.click(getByText(iriContainer, 'Interface 3'));

    const iriTab2 = getByRole(iriContainer, 'tabpanel');
    fireEvent.change(getByDataTest(iriTab2, 'input-destIPAddr'), { target: { value: '3.3.3.3' } });
    fireEvent.change(getByDataTest(iriTab2, 'input-destPort'), { target: { value: '3' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

    fireEvent.click(getAllByTestId(iriContainer, 'MultiTabRemoveIcon')[1]);

    await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should show success message on Save of a valid form', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));
    fireEvent.change(getByDataTest(ccContainer, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(ccContainer, 'input-destPort'), { target: { value: '2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Name 1 successfully created.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name to fail' } });
    fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

    const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
    fireEvent.change(getByDataTest(iriContainer, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(iriContainer, 'input-destPort'), { target: { value: '1' } });

    const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
    fireEvent.click(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd'));
    fireEvent.change(getByDataTest(ccContainer, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(ccContainer, 'input-destPort'), { target: { value: '2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Unable to create Name to fail: Request failed with status code 500.')).toBeVisible());
  });

  describe('collection function name customization', () => {
    beforeEach(() => {
      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([mockCustomization]),
      });
    });

    afterEach(() => {
      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([]),
      });
    });

    it('should set the collection function name when changing the LEA if customization is defined', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('Select a LEA');
      fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'id1' } });
      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CA-DOJBNE');

      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-33108');
    });

    it('should set the collection function name when changing the Handover Protocol if LEA and customization are defined', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');

      fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'id1' } });
      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-33108');

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-ETSI');
    });
  });
});
