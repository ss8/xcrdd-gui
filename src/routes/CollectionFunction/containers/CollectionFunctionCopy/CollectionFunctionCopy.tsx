import React, {
  FunctionComponent, useEffect, useRef, useState,
} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import * as globalSelectors from 'global/global-selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as collectionFunctionSelectors from 'data/collectionFunction/collectionFunction.selectors';
import * as collectionFunctionActions from 'data/collectionFunction/collectionFunction.actions';
import * as userSelectors from 'users/users-selectors';
import * as contactSelectors from 'xcipio/contacts/contacts-selectors';
import * as templateSelectors from 'data/template/template.selectors';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as ftpInfoSelectors from 'data/ftpInfo/ftpInfo.selectors';
import * as networkSelectors from 'data/network/network.selectors';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as fqdnGroupSelectors from 'data/fqdnGroup/fqdnGroup.selectors';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { FormTemplate } from 'data/template/template.types';
import { adaptToForm, adaptToCopy } from 'data/collectionFunction/collectionFunction.adapter';
import { CollectionFunctionFormData, CollectionFunctionInterfaceFormData } from 'data/collectionFunction/collectionFunction.types';
import { FieldUpdateHandlerParam } from 'shared/form';
import CollectionFunctionForm, { CollectionFunctionFormRef } from '../../components/CollectionFunctionForm';
import {
  adaptTemplateForInterfacesFieldUpdate,
  adaptTemplateInterfaceFields,
  adaptTemplateInterfaceTransportType,
  adaptTemplateName,
  adaptTemplateOptions,
  adaptTemplateTimezoneOptions,
  adaptTemplateVisibilityMode,
  adaptTemplate5GFields,
} from '../../utils/collectionFunctionTemplate.adapter';
import getCollectionFunctionDataToSubmit from '../../utils/getCollectionFunctionDataToSubmit';
import { createCollectionFunctionBasedOnTemplate } from '../../utils/collectionFunctionTemplate.factory';
import { handleInterfacesFieldUpdate } from '../../utils/handleInterfacesFieldUpdate';
import { handleCollectionFunctionNameUpdate } from '../../utils/handleCollectionFunctionNameUpdate';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  selectedCollectionFunction: collectionFunctionSelectors.getSelectedCollectionFunction(state),
  collectionFunction: collectionFunctionSelectors.getCollectionFunction(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  timezones: userSelectors.getTimezones(state),
  contacts: contactSelectors.getContacts(state),
  ftpInfos: ftpInfoSelectors.getFtpInfos(state),
  collectionFunctionGroupList: collectionFunctionSelectors.getCollectionFunctionGroupList(state),
  collectionFunctionTemplates: templateSelectors.getEnabledCollectionFunctionFormTemplatesByKey(state),
  enabledCollectionFunctions: supportedFeaturesSelectors.getEnabledCollectionFunctions(state),
  networks: networkSelectors.getNetworks(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  fqdnGroups: fqdnGroupSelectors.getFqdnGroups(state),
  collectionFunctionNameCustomization: warrantSelectors.getCustomizationCollectionFunctionName(state),
  is5GCVcpEnabled: supportedFeaturesSelectors.is5gcVcpEnabled(state),
  xcipioNodes: nodeSelectors.getNodes(state),
});

const mapDispatch = {
  fetchCollectionFunctionWithViewOptions: collectionFunctionActions.fetchCollectionFunctionWithViewOptions,
  createCollectionFunction: collectionFunctionActions.createCollectionFunction,
  goToCollectionFunctionList: collectionFunctionActions.goToCollectionFunctionList,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
type Props = PropsFromRedux & PropsFromRouter;

export const CollectionFunctionCopy: FunctionComponent<Props> = ({
  deliveryFunctionId,
  selectedCollectionFunction,
  collectionFunction,
  history,
  isAuditEnabled,
  userId,
  userName,
  timezones,
  contacts,
  ftpInfos,
  collectionFunctionGroupList,
  collectionFunctionTemplates,
  enabledCollectionFunctions,
  networks,
  securityInfos,
  fqdnGroups,
  collectionFunctionNameCustomization,
  is5GCVcpEnabled,
  xcipioNodes,
  fetchCollectionFunctionWithViewOptions,
  createCollectionFunction,
  goToCollectionFunctionList,
}: Props) => {
  const [templates, setTemplates] = useState<{ [key: string]: FormTemplate }>({});
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<CollectionFunctionFormData>>({});
  const formRef = useRef<CollectionFunctionFormRef>(null);

  const collectionFunctionId: string | undefined = selectedCollectionFunction[0]?.id;

  useEffect(() => {
    if (collectionFunctionId) {
      fetchCollectionFunctionWithViewOptions(deliveryFunctionId, collectionFunctionId);
    }
  }, [collectionFunctionId, deliveryFunctionId, fetchCollectionFunctionWithViewOptions]);

  useEffect(() => {
    const availableTemplates = adaptTemplateOptions(
      collectionFunctionTemplates,
      timezones,
      contacts,
      collectionFunctionGroupList,
      ftpInfos,
      networks,
      securityInfos,
      fqdnGroups,
      enabledCollectionFunctions,
      xcipioNodes,
    );
    setTemplates(availableTemplates);
  }, [
    collectionFunctionTemplates,
    timezones,
    contacts,
    ftpInfos,
    collectionFunctionGroupList,
    enabledCollectionFunctions,
    networks,
    securityInfos,
    fqdnGroups,
    xcipioNodes,
  ]);

  useEffect(() => {
    if (collectionFunction != null && templates != null) {
      const formData = adaptToCopy(adaptToForm(collectionFunction));
      let templateData = adaptTemplateVisibilityMode(templates[formData.tag], 'copy');
      templateData = adaptTemplateTimezoneOptions(templateData, formData, timezones);
      templateData = adaptTemplate5GFields(templateData, is5GCVcpEnabled);
      templateData = adaptTemplateInterfaceFields(templateData, formData);

      setData(formData);
      setTemplate(templateData);
    }
  }, [collectionFunction, templates, timezones, is5GCVcpEnabled]);

  const handleClickSave = () => {

    const isValid = formRef?.current?.isValid(true, true) ?? true;
    if (!isValid) {
      return;
    }

    const formData = formRef?.current?.getValues();
    if (!formData) {
      return;
    }

    const collectionFunctionBody = getCollectionFunctionDataToSubmit(
      formData,
      null,
      isAuditEnabled,
      userId,
      userName,
      'CREATE',
    );

    createCollectionFunction(deliveryFunctionId, collectionFunctionBody, history);
  };

  const handleCancel = () => {
    goToCollectionFunctionList(history);
  };

  const handleInterfaceAdd = (formSection: 'hi2Interfaces' | 'hi3Interfaces') => {
    const clonedData = cloneDeep(data);

    const newData = createCollectionFunctionBasedOnTemplate(template, true);
    const newInterfaceData = (newData[formSection] as CollectionFunctionInterfaceFormData[])[0];
    (clonedData[formSection] as CollectionFunctionInterfaceFormData[]).push(newInterfaceData);

    setData(clonedData);
  };

  const handleInterfaceRemove = (tabId: string | number, formSection: 'hi2Interfaces' | 'hi3Interfaces') => {
    const clonedData = cloneDeep(data);

    clonedData[formSection] = clonedData[formSection]?.filter(({ id }) => id !== tabId);

    setData(clonedData);
  };

  const handleFieldUpdate = (
    { name: fieldName, value }: FieldUpdateHandlerParam,
    formSection?: 'main' | 'facilityInformation' | 'hi2Interfaces' | 'hi3Interfaces',
    interfaceId?: string,
  ) => {
    const clonedData = cloneDeep(data);

    if (formSection === 'main' || formSection === 'facilityInformation') {
      if (fieldName === 'cfGroup') {
        const name = handleCollectionFunctionNameUpdate(
          collectionFunctionNameCustomization,
          collectionFunctionGroupList,
          clonedData,
          fieldName,
          value,
        );

        const adaptedTemplate = adaptTemplateName(template, name);

        setData({
          ...clonedData,
          name,
          [fieldName]: value,
        });
        setTemplate(adaptedTemplate);

      } else if (fieldName === 'tag') {
        let adaptedTemplate = adaptTemplateVisibilityMode(templates[value], 'create');

        const initialData = createCollectionFunctionBasedOnTemplate(adaptedTemplate);

        const name = handleCollectionFunctionNameUpdate(
          collectionFunctionNameCustomization,
          collectionFunctionGroupList,
          clonedData,
          fieldName,
          value,
        );
        adaptedTemplate = adaptTemplateName(template, name);

        const { cfGroup } = clonedData;

        // When changing the type, only the name and cfGroup are preserved
        setData({
          ...initialData,
          name,
          cfGroup,
        });
        setTemplate(adaptedTemplate);

      } else {
        setData({
          ...clonedData,
          [fieldName]: value,
        });
      }

    } else if (formSection === 'hi2Interfaces' || formSection === 'hi3Interfaces') {
      if (fieldName === 'transportType' && clonedData[formSection]?.[0].transportType !== value) {
        // Changing the collection function interface transport type should reset the interfaces
        const adaptedTemplate = adaptTemplateInterfaceTransportType(template, formSection, value);
        const initialData = createCollectionFunctionBasedOnTemplate(adaptedTemplate, true);

        clonedData[formSection] = initialData[formSection] as CollectionFunctionInterfaceFormData[];

        setData(clonedData);
        setTemplate(adaptedTemplate);

      } else {
        clonedData[formSection] = handleInterfacesFieldUpdate(
          clonedData[formSection],
          interfaceId,
          fieldName,
          value,
          ftpInfos,
        );

        const adaptedTemplate = adaptTemplateForInterfacesFieldUpdate(
          formSection,
          clonedData,
          template,
          fieldName,
          value,
        );

        setData(clonedData);
        setTemplate(adaptedTemplate);
      }
    }
  };

  if (!template || !data) {
    return null;
  }

  return (
    <div className="app-container-content" data-testid="CollectionFunctionCopy">
      <CollectionFunctionForm
        ref={formRef}
        title="New Collection Function"
        isEditing
        data={data}
        template={template}
        onClickSave={handleClickSave}
        onClickCancelClose={handleCancel}
        onFieldUpdate={handleFieldUpdate}
        onInterfaceAdd={handleInterfaceAdd}
        onInterfaceRemove={handleInterfaceRemove}
      />
    </div>
  );
};

export default withRouter(connector(CollectionFunctionCopy));
