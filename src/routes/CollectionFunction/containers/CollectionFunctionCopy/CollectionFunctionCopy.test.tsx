import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import {
  fireEvent,
  getAllByDisplayValue,
  getAllByLabelText,
  getAllByRole,
  getAllByTestId,
  getByDisplayValue,
  getByLabelText,
  getByRole,
  getByTestId,
  getByText,
  render,
  RenderResult,
  screen,
  waitFor,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import cloneDeep from 'lodash.clonedeep';
import Http from 'shared/http';
import store from 'data/store';
import { MemoryRouter } from 'react-router-dom';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as collectionFunctionTypes from 'data/collectionFunction/collectionFunction.types';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as ftpInfoTypes from 'data/ftpInfo/ftpInfo.types';
import * as networkTypes from 'data/network/network.types';
import * as securityInfoTypes from 'data/securityInfo/securityInfo.types';
import * as fqdnGroupTypes from 'data/fqdnGroup/fqdnGroup.types';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { getAllByDataTest, getByDataTest } from '__test__/customQueries';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { mockCustomization } from '__test__/mockCustomization';
import { CF_ROUTE } from '../../../../constants';
import CollectionFunction from '../../CollectionFunction';
import CollectionFunctionCopy from './CollectionFunctionCopy';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunctionCopy />', () => {
  let collectionFunction: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionWithUnsupportedTimezone: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSI: collectionFunctionTypes.CollectionFunction;
  let collectionFunctionETSIWithCountryTR: collectionFunctionTypes.CollectionFunction;

  beforeEach(() => {
    collectionFunction = {
      id: 'ID_TO_GET',
      dxOwner: 'dxOwner',
      dxAccess: 'dxAccess',
      name: 'CA-DOJBNE-33108',
      bufferCC: false,
      cellTowerLocation: false,
      contact: {
        id: 'Q29udGFjdCAx',
        name: 'Contact 1',
      },
      deliveryType: 'DSR',
      insideNat: false,
      type: 'GPRS',
      tag: 'CF-3GPP-33108',
      timeZone: 'America/Los_Angeles',
      cfGroup: {
        id: 'id1',
        name: '',
      },
      graceTimer: '10',
      hi2Interfaces: [{
        id: 'id1',
        name: 'IP:1.1.1.1 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '15.6' },
          { key: 'traceLevel', value: '0' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'comments', value: 'some comments' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'dscp', value: '0' },
          { key: 'destPort', value: '1' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'tpkt', value: 'Y' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'ownIPAddr', value: '12.12.12.12' },
          { key: 'cfid', value: 'CA-DOJBNE-33108' },
          { key: 'ownIPPort', value: '12' },
        ],
      }, {
        id: 'id2',
        name: 'IP:111.111.222.222 PORT:1',
        transportType: 'TCP',
        options: [
          { key: 'version', value: '15.6' },
          { key: 'traceLevel', value: '0' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'comments', value: 'some comments' },
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'ifid', value: '1' },
          { key: 'dscp', value: '0' },
          { key: 'destPort', value: '1' },
          { key: 'keepAliveFreq', value: '60' },
          { key: 'keepAliveRetries', value: '6' },
          { key: 'tpkt', value: 'Y' },
          { key: 'destIPAddr', value: '111.111.222.222' },
          { key: 'ownIPAddr', value: '131.131.232.232' },
          { key: 'cfid', value: 'CA-DOJBNE-33108' },
          { key: 'ownIPPort', value: '12' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: 'IP:22.22.22.22 PORT:22',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'label 2' },
          { key: 'dscp', value: '10' },
          { key: 'keepAliveFreq', value: '120' },
          { key: 'keepAliveRetries', value: '20' },
          { key: 'inactivityTimer', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
          { key: 'useIpdu', value: 'N' },
        ],
      }],
    };

    collectionFunctionWithUnsupportedTimezone = cloneDeep(collectionFunction);
    collectionFunctionWithUnsupportedTimezone.timeZone = 'America/Tijuana';

    collectionFunctionETSI = {
      ...collectionFunction,
      id: 'ID_TO_GET_3',
      type: 'ETSIIP',
      name: 'CF-ETSI-102232',
      tag: 'CF-ETSI-102232',
      insideNat: true,
      bufferCC: true,
      hi2Interfaces: [{
        id: 'id1',
        name: '',
        transportType: 'TCP',
        options: [
          { key: 'countryCode', value: 'US' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '61' },
          { key: 'hashCert', value: 'a123' },
          { key: 'hashNum', value: '11' },
          { key: 'hashSec', value: '61' },
          { key: 'icheck', value: 'SIGNEDHASH' },
          { key: 'ownnetid', value: 'WENQdjY' },
          { key: 'pldEncKey', value: '' },
          { key: 'pldEncType', value: 'NONE' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'useHi3', value: 'Y' },
          { key: 'version', value: '3.17.1' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id2',
        name: '',
        transportType: 'TCP',
        options: [
          { key: 'connectionType', value: 'PRIMARY' },
          { key: 'countryCode', value: 'US' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'destType', value: 'ipAddress' },
          { key: 'displayLabel', value: 'label 1' },
          { key: 'fsid', value: '' },
          { key: 'dscp', value: '62' },
          { key: 'hashCert', value: '' },
          { key: 'hashNum', value: '10' },
          { key: 'hashSec', value: '60' },
          { key: 'icheck', value: 'OFF' },
          { key: 'ownnetid', value: 'WFRQMTI4djRfV0dX' },
          { key: 'pldEncKey', value: '' },
          { key: 'pldEncType', value: 'NONE' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'secinfoid', value: '' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'traceLevel', value: '3' },
          { key: 'version', value: '3.17.1' },
        ],
      }],
    };

    collectionFunctionETSIWithCountryTR = {
      ...collectionFunctionETSI,
      id: 'ID_TO_GET_4',
      hi2Interfaces: [{
        ...collectionFunctionETSI?.hi2Interfaces?.[0],
        id: 'id1',
        name: '',
        options: [
          ...collectionFunctionETSI?.hi2Interfaces?.[0].options ?? [],
          { key: 'countryCode', value: 'TR' },
          { key: 'version', value: '2.5.1' },
        ],
      }],
    };

    store.dispatch({
      type: collectionFunctionTypes.GET_COLLECTION_FUNCTION_PENDING,
      payload: null,
    });

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['cf_view', 'cf_write', 'cf_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: collectionFunctionTypes.GET_COLLECTION_FUNCTION_GROUP_LIST_FULFILLED,
      payload: buildAxiosServerResponse(mockCollectionFunctionGroups),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: contactActions.GET_ALL_CONTACTS_FULFILLED,
      payload: buildAxiosServerResponse(mockContacts),
    });

    store.dispatch({
      type: ftpInfoTypes.GET_FTP_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockFtpInfos),
    });

    store.dispatch({
      type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: networkTypes.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(mockNetworks),
    });

    store.dispatch({
      type: securityInfoTypes.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSecurityInfos),
    });

    store.dispatch({
      type: fqdnGroupTypes.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    });

    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${ftpInfoTypes.API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onGet('/cfs/0/ID_TO_GET?$view=options').reply(200, { data: [collectionFunction] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_2?$view=options').reply(200, { data: [collectionFunctionWithUnsupportedTimezone] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_3?$view=options').reply(200, { data: [collectionFunctionETSI] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_4?$view=options').reply(200, { data: [collectionFunctionETSIWithCountryTR] });
    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('123.123.123.123') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());
    expect(screen.getByTestId('CollectionFunctionForm')).toBeVisible();
  });

  it('should render the list of collection functions when clicking in the cancel button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${CF_ROUTE}/copy`]}>
          <CollectionFunction />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByTestId('CollectionFunctionList')).toBeVisible();
  });

  it('should render the form title with Cancel and Save buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <CollectionFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  describe('When loads CF copy mode page', () => {
    let component: RenderResult;
    let container: HTMLElement;

    beforeEach(async () => {

      component = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      container = component.container;
      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());
    });

    it('should validate fields on Save', async () => {

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
      fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));

      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(screen.getAllByText('Please enter a value.')).toHaveLength(5);
    });

    it('should clean up error indication on tabs if tab is removed', async () => {

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
      await waitFor(() => expect(getByText(iriContainer, 'Interface 3')).toBeVisible());
      fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
      await waitFor(() => expect(getByText(iriContainer, 'Interface 4')).toBeVisible());

      expect(getByText(iriContainer, 'Interface 1')).toBeVisible();
      expect(getByText(iriContainer, 'Interface 2')).toBeVisible();
      expect(getByText(iriContainer, 'Interface 3')).toBeVisible();
      expect(getByText(iriContainer, 'Interface 4')).toBeVisible();

      fireEvent.click(getByText(iriContainer, 'Interface 1'));
      const iriTab1 = getByRole(iriContainer, 'tabpanel');
      fireEvent.change(getByDataTest(iriTab1, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
      fireEvent.change(getByDataTest(iriTab1, 'input-destPort'), { target: { value: '1' } });

      fireEvent.click(getByText(iriContainer, 'Interface 2'));
      const iriTab2 = getByRole(iriContainer, 'tabpanel');
      fireEvent.change(getByDataTest(iriTab2, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
      fireEvent.change(getByDataTest(iriTab2, 'input-destPort'), { target: { value: '2' } });

      fireEvent.click(getByText(iriContainer, 'Interface 4'));
      const iriTab4 = getByRole(iriContainer, 'tabpanel');
      fireEvent.change(getByDataTest(iriTab4, 'input-destIPAddr'), { target: { value: '4.4.4.4' } });
      fireEvent.change(getByDataTest(iriTab4, 'input-destPort'), { target: { value: '4' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());

      fireEvent.click(getAllByTestId(iriContainer, 'MultiTabRemoveIcon')[2]);

      await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeNull());
    });

    it('should indicate tab has errors on Save', async () => {

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');

      fireEvent.click(getByText(iriContainer, 'Interface 1'));
      const iriTab1 = getByRole(iriContainer, 'tabpanel');
      fireEvent.change(getByDataTest(iriTab1, 'input-destIPAddr'), { target: { value: '1.1.1.1' } });
      fireEvent.change(getByDataTest(iriTab1, 'input-destPort'), { target: { value: '1' } });

      fireEvent.click(getByText(iriContainer, 'Interface 2'));
      const iriTab2 = getByRole(iriContainer, 'tabpanel');
      fireEvent.change(getByDataTest(iriTab2, 'input-destIPAddr'), { target: { value: '2.2.2.2' } });
      fireEvent.change(getByDataTest(iriTab2, 'input-destPort'), { target: { value: '2' } });

      fireEvent.click(getByTestId(iriContainer, 'MultiTabAddButton'));
      await waitFor(() => expect(getByText(iriContainer, 'Interface 3')).toBeVisible());

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.click(getByText(ccContainer, 'Interface 1'));
      const ccTab1 = getByRole(ccContainer, 'tabpanel');
      fireEvent.change(getByDataTest(ccTab1, 'input-destIPAddr'), { target: { value: '3.3.3.3' } });
      fireEvent.change(getByDataTest(ccTab1, 'input-destPort'), { target: { value: '3' } });

      fireEvent.click(getByTestId(ccContainer, 'MultiTabAddButton'));
      await waitFor(() => expect(getByText(ccContainer, 'Interface 2')).toBeVisible());

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(iriContainer.querySelector('.multi-tab-has-error')).toBeVisible());
      await waitFor(() => expect(ccContainer.querySelector('.multi-tab-has-error')).toBeVisible());
    });

    it('should show error message on Save if server returns an error', async () => {

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      fireEvent.change(getAllByDataTest(iriContainer, 'input-destIPAddr')[0], { target: { value: '123.123.123.123' } });

      fireEvent.change(getAllByDataTest(iriContainer, 'input-destPort')[0], { target: { value: '1111' } });
      fireEvent.change(getAllByDataTest(iriContainer, 'input-destPort')[1], { target: { value: '2211' } });

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.change(getAllByDataTest(ccContainer, 'input-destPort')[0], { target: { value: '2221' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText(`Unable to create Copy of ${collectionFunction.name}: Request failed with status code 500.`)).toBeVisible());
    });

    it('should show success message on Save of a valid form', async () => {
      fireEvent.change(getByDataTest(container, 'input-cfGroup'), { target: { value: 'id2' } });
      fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      fireEvent.change(getAllByDataTest(iriContainer, 'input-destPort')[0], { target: { value: '1111' } });
      fireEvent.change(getAllByDataTest(iriContainer, 'input-destPort')[1], { target: { value: '2211' } });

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      fireEvent.change(getAllByDataTest(ccContainer, 'input-destPort')[0], { target: { value: '2221' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText(`Copy of ${collectionFunction.name} successfully created.`)).toBeVisible());
    });

    describe('Main information', () => {

      it('should render the component with data-testid', () => {
        expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible();
        expect(screen.getByText('New Collection Function')).toBeVisible();
      });

      it('should render main information in copy only mode', () => {
        expect(screen.getByText('LEA:')).toBeVisible();
        expect(screen.getByText('Handover Protocol:')).toBeVisible();
        expect(screen.getByText('Name:')).toBeVisible();

        expect(getByDataTest(container, 'input-cfGroup')).toBeEnabled();
        expect(getByDataTest(container, 'input-tag')).toBeEnabled();
        expect(getByDataTest(container, 'input-name')).toBeEnabled();

        expect(screen.getByDisplayValue('US 3GPP LI (TS 33.108) over TCP or FTP')).toBeVisible();
        expect(screen.getByDisplayValue(`Copy of ${collectionFunction.name}`)).toBeVisible();
      });
    });

    describe('Facility Information', () => {
      it('should render Facility Information in copy only mode', async () => {
        await waitFor(() => expect(getByDataTest(container, 'input-timeZone')).toBeEnabled());

        expect(screen.getByText('Delivery Type:')).toBeVisible();
        expect(screen.getByText('Time Zone:')).toBeVisible();
        expect(screen.getByText('Contact:')).toBeVisible();
        expect(screen.getByText('Contact:')).toBeVisible();
        expect(screen.getByText('Grace Timer:')).toBeVisible();

        expect(getByDataTest(container, 'input-deliveryType')).toBeEnabled();
        expect(getByDataTest(container, 'input-timeZone')).toBeEnabled();
        expect(getByDataTest(container, 'input-contact')).toBeEnabled();
        expect(getByDataTest(container, 'input-graceTimer')).toBeEnabled();
        expect(getByDataTest(container, 'input-insideNat')).toBeEnabled();

        expect(screen.getByDisplayValue('DSR')).toBeVisible();
        expect(screen.getByText('Select a contact')).toBeVisible();
        expect(screen.getByText('Select a time zone')).toBeVisible();
        expect(screen.getByText('Xcipio is behind NAT')).toBeVisible();
        expect(getByDataTest(container, 'input-insideNat')).not.toBeChecked();

      });
    });

    describe('Intercept Related Information (IRI)', () => {
      it('should render IRI values in copy only mode', () => {
        const iriTab = screen.getByTestId('MultiTabFormSectionIRI');

        fireEvent.click(getAllByTestId(iriTab, 'CollapsibleFormButton')[0]);

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-destPort')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-version')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-reqState')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[0]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-dscp')[0]).toBeEnabled();
        expect(getAllByDisplayValue(iriTab, 'Primary')[0]).toBeEnabled();
        expect(getAllByLabelText(iriTab, 'TPKT')[0]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[0]).toBeEnabled();

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[0].getAttribute('value')).toBe('1.1.1.1');
        expect(getAllByDataTest(iriTab, 'input-destPort')[0].getAttribute('value')).toBe('');
        expect(getAllByDisplayValue(iriTab, '15.6')[0]).toBeVisible();
        expect(getAllByDisplayValue(iriTab, 'Active')[0]).toBeVisible();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[0].getAttribute('value')).toEqual('12.12.12.12');
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[0].getAttribute('value')).toEqual('12');
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[0].getAttribute('value')).toEqual('60');
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[0].getAttribute('value')).toEqual('6');
        expect(getAllByDataTest(iriTab, 'input-dscp')[0].getAttribute('value')).toEqual('0');
        expect(getAllByDisplayValue(iriTab, 'Primary')[0]).toBeVisible();
        expect(getAllByLabelText(iriTab, 'TPKT')[0]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[0].getAttribute('value')).toEqual('some comments');

        // Tab 2 data

        fireEvent.click(getAllByTestId(iriTab, 'CollapsibleFormButton')[1]);

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-destPort')[1]).toBeEnabled();
        expect(getAllByDisplayValue(iriTab, '15.6')[1]).toBeEnabled();
        expect(getAllByDisplayValue(iriTab, 'Active')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[1]).toBeEnabled();
        expect(getAllByDataTest(iriTab, 'input-dscp')[1]).toBeEnabled();
        expect(getAllByDisplayValue(iriTab, 'Primary')[1]).toBeEnabled();
        expect(getAllByLabelText(iriTab, 'TPKT')[1]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[1]).toBeEnabled();

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[1].getAttribute('value')).toBe('111.111.222.222');
        expect(getAllByDataTest(iriTab, 'input-destPort')[1].getAttribute('value')).toBe('');
        expect(getAllByDisplayValue(iriTab, '15.6')[1]).toBeVisible();
        expect(getAllByDisplayValue(iriTab, 'Active')[1]).toBeVisible();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[1].getAttribute('value')).toEqual('131.131.232.232');
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[1].getAttribute('value')).toEqual('12');
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[1].getAttribute('value')).toEqual('60');
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[1].getAttribute('value')).toEqual('6');
        expect(getAllByDataTest(iriTab, 'input-dscp')[1].getAttribute('value')).toEqual('0');
        expect(getAllByDisplayValue(iriTab, 'Primary')[1]).toBeVisible();
        expect(getAllByLabelText(iriTab, 'TPKT')[1]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[1].getAttribute('value')).toEqual('some comments');
      });

      it('should add and remove tabs in Intercept Related Information (IRI) form as expected', async () => {
        await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

        const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
        expect(getByText(iriContainer, 'Transport Protocol:')).toBeVisible();

        const interfaces = screen.getByTestId('MultiTabFormSectionIRI');
        expect(getAllByRole(interfaces, 'tab')).toHaveLength(2);
        expect(getByText(interfaces, 'Interface 1')).toBeVisible();
        expect(getByText(interfaces, 'Interface 2')).toBeVisible();

        fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));

        expect(getAllByRole(interfaces, 'tab')).toHaveLength(3);
        expect(getByText(interfaces, 'Interface 3')).toBeVisible();

        fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[2]);
        expect(getAllByRole(interfaces, 'tab')).toHaveLength(2);

        fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[1]);
        expect(getByText(interfaces, 'Interface 1')).toBeVisible();
        expect(getAllByRole(interfaces, 'tab')).toHaveLength(1);

      });

    });

    describe('Communication Content (CC)', () => {
      it('should render CC from TAB 1 in copy only mode', () => {
        const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

        const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
        const ccTab = getByRole(ccInterfaces, 'tabpanel');

        expect(getByDisplayValue(ccContainer, 'Over TCP')).toBeVisible();

        expect(getByDataTest(ccTab, 'input-destIPAddr').getAttribute('value')).toEqual('2.2.2.2');
        expect(getByDataTest(ccTab, 'input-destPort').getAttribute('value')).toEqual('');
        expect(getByDisplayValue(ccTab, '15.5')).toBeVisible();

        fireEvent.click(getAllByTestId(ccInterfaces, 'CollapsibleFormButton')[0]);

        expect(getByDataTest(ccTab, 'input-ownIPAddr').getAttribute('value')).toEqual('22.22.22.22');
        expect(getByDataTest(ccTab, 'input-ownIPPort').getAttribute('value')).toEqual('22');
        expect(getByDataTest(ccTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('120');
        expect(getByDataTest(ccTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('20');

        expect(getByDataTest(ccTab, 'input-dscp').getAttribute('value')).toEqual('10');
        expect(getByLabelText(ccTab, 'Use XDP')).not.toBeChecked();
        expect(getByLabelText(ccTab, 'TPKT')).not.toBeChecked();

        expect(getByDataTest(ccTab, 'input-inactivityTimer').getAttribute('value')).toEqual('20');
        expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('label 2');
        expect(getByDataTest(ccTab, 'input-comments').getAttribute('value')).toEqual('some comment 2');
      });

      it('should add and remove tabs in Communication Content (CC) form as expected', async () => {
        await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

        const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
        expect(getByText(ccContainer, 'Transport Protocol:')).toBeVisible();

        const interfaces = screen.getByTestId('MultiTabFormSectionCC');

        expect(getAllByRole(interfaces, 'tab')).toHaveLength(1);
        expect(getByText(interfaces, 'Interface 1')).toBeVisible();

        fireEvent.click(getByTestId(interfaces, 'MultiTabAddButton'));

        expect(getAllByRole(interfaces, 'tab')).toHaveLength(2);
        expect(getByText(interfaces, 'Interface 2')).toBeVisible();

        fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[1]);
        expect(getAllByRole(interfaces, 'tab')).toHaveLength(1);
        expect(getByText(interfaces, 'Interface 1')).toBeVisible();

        fireEvent.click(getAllByTestId(interfaces, 'MultiTabRemoveIcon')[0]);
        expect(getByTestId(ccContainer, 'CollectionFunctionInterfaceFormAdd')).toBeVisible();
      });
    });

  });

  describe('When test unsupported timezone', () => {
    it('should load the timezone even if it is not supported', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_2' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Time Zone:*')).toHaveValue('America/Tijuana');
    });
  });

  describe('CF-ETSI-102232', () => {
    it('should load the initial data for CF-ETSI-102232', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_3' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CA-DOJBNE');
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);
      expect(screen.getByLabelText('Name:*')).toHaveValue('Copy of CF-ETSI-102232');

      expect(screen.getByLabelText('Delivery Type:*')).toHaveDisplayValue('DSR');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('US/Pacific');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 1');
      expect(screen.getByLabelText('Grace Timer:*')).toHaveValue('10');

      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();
      expect(screen.getByLabelText('Buffering CC')).toBeChecked();

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      expect(getByLabelText(iriContainer, 'Transport Protocol:')).toHaveDisplayValue('TCP');

      expect(getByLabelText(iriTab, 'IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(iriTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Version:')).toHaveValue('3.17.1');
      expect(getByLabelText(iriTab, 'XCP Network ID:*')).toHaveDisplayValue('XCPv6');

      expect(getByLabelText(iriTab, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'DSCP (0-63):*')).toHaveValue('61');
      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('US');
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(iriTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(iriTab, 'Encryption Key:')).toBeDisabled();

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('SIGNEDHASH');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toHaveValue('11');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toHaveValue('61');
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toHaveValue('a123');
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeEnabled();

      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeChecked();

      const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');
      const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
      const ccTab = getByRole(ccInterfaces, 'tabpanel');

      expect(getByLabelText(ccContainer, 'Transport Protocol:')).toHaveDisplayValue('Over TCP');

      // expect(getByText(ccTab, 'Destination Address Type:')).toBeVisible();
      // expect(getByLabelText(ccTab, 'IP Address')).toBeChecked();
      // expect(getByLabelText(ccTab, 'Domain Name')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(ccTab, 'Port:*')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Version:')).toHaveDisplayValue('3.17.1');

      expect(getByLabelText(ccTab, 'Configured State:')).toHaveDisplayValue('Inactive');

      fireEvent.click(getByTestId(ccInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(ccTab, 'XDP Network ID:')).toHaveDisplayValue('XTP128v4_WGW');
      expect(getByLabelText(ccTab, 'DSCP (0-63):*')).toHaveValue('62');
      expect(getByLabelText(ccTab, 'Connection Type:')).toHaveDisplayValue('Primary');
      expect(getByLabelText(ccTab, 'Trace Level:')).toHaveDisplayValue('3');

      expect(getByLabelText(ccTab, 'Variant / Country Code:')).toHaveDisplayValue('US');
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).not.toBeChecked();
      expect(getByLabelText(ccTab, 'Use AES-192-CBC')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Encryption Key:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Encryption Key:')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Security Info ID:')).toHaveDisplayValue('Select a Security Info ID');
      expect(getByLabelText(ccTab, 'Security Info ID:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toHaveValue('10');
      expect(getByLabelText(ccTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toHaveValue('60');
      expect(getByLabelText(ccTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toHaveValue('');
      expect(getByLabelText(ccTab, 'Hash Certificate:')).toBeDisabled();

      expect(getByLabelText(ccTab, 'Template Display Label:')).toHaveValue('label 1');
    });

    it('should disable the Integrity Check related fields when changing to Off', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_3' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      const iriContainer = screen.getByTestId('CollectionFunctionInterfaceFormIRI');
      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('SIGNEDHASH');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeEnabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeEnabled();

      fireEvent.change(getByLabelText(iriContainer, 'Integrity Check:'), { target: { value: 'OFF' } });

      expect(getByLabelText(iriTab, 'Integrity Check:')).toHaveDisplayValue('Off');
      expect(getByLabelText(iriTab, 'Max Payloads PDUs:*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash PDUs Interval (secs):*')).toBeDisabled();
      expect(getByLabelText(iriTab, 'Hash Certificate:')).toBeDisabled();
    });

    it('should load the Variant / Country Code as expected when TR', async () => {
      store.dispatch({
        type: collectionFunctionTypes.SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_4' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      const iriInterfaces = screen.getByTestId('MultiTabFormSectionIRI');
      const iriTab = getByRole(iriInterfaces, 'tabpanel');

      fireEvent.click(getByTestId(iriInterfaces, 'CollapsibleFormButton'));

      expect(getByLabelText(iriTab, 'Variant / Country Code:')).toHaveDisplayValue('TR');
      expect(getByLabelText(iriTab, 'Variant / Country Code:').querySelectorAll('option')).toHaveLength(2);

      expect(getByLabelText(iriTab, 'Deliver IRI with CC')).toBeDisabled();
    });
  });

  describe('collection function name customization', () => {
    beforeEach(() => {
      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([mockCustomization]),
      });
    });

    afterEach(() => {
      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([]),
      });
    });

    it('should set the collection function name when changing the LEA if customization is defined', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('Copy of CA-DOJBNE-33108');

      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CA-DOJBNE');
      fireEvent.change(screen.getByLabelText('LEA:*'), { target: { value: 'id2' } });
      expect(screen.getByLabelText('LEA:*')).toHaveDisplayValue('CookCounty');

      expect(screen.getByLabelText('Name:*')).toHaveValue('CookCounty-33108');
    });

    it('should set the collection function name when changing the Handover Protocol if LEA and customization are defined', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('Copy of CA-DOJBNE-33108');

      fireEvent.change(screen.getByLabelText('Handover Protocol:*'), { target: { value: 'CF-ETSI-102232' } });
      expect(screen.getByLabelText('Handover Protocol:*')).toHaveDisplayValue(['ETSI IP (TS 102 232)']);

      expect(screen.getByLabelText('Name:*')).toHaveValue('CA-DOJBNE-ETSI');
    });
  });
});
