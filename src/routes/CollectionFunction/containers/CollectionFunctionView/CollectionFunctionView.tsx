import React, {
  FunctionComponent, useEffect, useRef, useState,
} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { RootState } from 'data/root.reducers';
import * as globalSelectors from 'global/global-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as collectionFunctionSelectors from 'data/collectionFunction/collectionFunction.selectors';
import * as collectionFunctionActions from 'data/collectionFunction/collectionFunction.actions';
import * as userSelectors from 'users/users-selectors';
import * as contactSelectors from 'xcipio/contacts/contacts-selectors';
import * as templateSelectors from 'data/template/template.selectors';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as ftpInfoSelectors from 'data/ftpInfo/ftpInfo.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as networkSelectors from 'data/network/network.selectors';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as fqdnGroupSelectors from 'data/fqdnGroup/fqdnGroup.selectors';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import { FormTemplate } from 'data/template/template.types';
import { CollectionFunctionFormData } from 'data/collectionFunction/collectionFunction.types';
import { adaptToForm } from 'data/collectionFunction/collectionFunction.adapter';
import ModalDialog from 'shared/modal/dialog';
import { AuditDetails, WithAuditDetails } from 'data/types';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';
import CollectionFunctionForm, { CollectionFunctionFormRef } from '../../components/CollectionFunctionForm';
import {
  adaptTemplateOptions,
  adaptTemplateVisibilityMode,
  adaptTemplateTimezoneOptions,
  adaptTemplateInterfaceFields,
  adaptTemplate5GFields,
} from '../../utils/collectionFunctionTemplate.adapter';
import FieldCurrentState from '../../../../components/FieldCurrentState';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  timezones: userSelectors.getTimezones(state),
  contacts: contactSelectors.getContacts(state),
  ftpInfos: ftpInfoSelectors.getFtpInfos(state),
  collectionFunctionGroupList: collectionFunctionSelectors.getCollectionFunctionGroupList(state),
  collectionFunctionTemplates: templateSelectors.getEnabledCollectionFunctionFormTemplatesByKey(state),
  enabledCollectionFunctions: supportedFeaturesSelectors.getEnabledCollectionFunctions(state),
  selectedCollectionFunction: collectionFunctionSelectors.getSelectedCollectionFunction(state),
  collectionFunction: collectionFunctionSelectors.getCollectionFunction(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  networks: networkSelectors.getNetworks(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  fqdnGroups: fqdnGroupSelectors.getFqdnGroups(state),
  is5GCVcpEnabled: supportedFeaturesSelectors.is5gcVcpEnabled(state),
  xcipioNodes: nodeSelectors.getNodes(state),
});

const mapDispatch = {
  goToCollectionFunctionList: collectionFunctionActions.goToCollectionFunctionList,
  goToCollectionFunctionCopy: collectionFunctionActions.goToCollectionFunctionCopy,
  goToCollectionFunctionEdit: collectionFunctionActions.goToCollectionFunctionEdit,
  fetchCollectionFunction: collectionFunctionActions.fetchCollectionFunctionWithViewOptions,
  deleteCollectionFunction: collectionFunctionActions.deleteCollectionFunction,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter;

export const CollectionFunctionView: FunctionComponent<Props> = ({
  deliveryFunctionId,
  history,
  isAuditEnabled,
  userId,
  userName,
  timezones,
  contacts,
  ftpInfos,
  collectionFunctionGroupList,
  collectionFunctionTemplates,
  enabledCollectionFunctions,
  selectedCollectionFunction,
  collectionFunction,
  networks,
  securityInfos,
  fqdnGroups,
  is5GCVcpEnabled,
  xcipioNodes,
  goToCollectionFunctionList,
  goToCollectionFunctionCopy,
  fetchCollectionFunction,
  deleteCollectionFunction,
  goToCollectionFunctionEdit,
}: Props) => {
  const [templates, setTemplates] = useState<{ [key: string]: FormTemplate }>({});
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<CollectionFunctionFormData>>({});
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const formRef = useRef<CollectionFunctionFormRef>(null);

  const collectionFunctionId = selectedCollectionFunction[0]?.id;

  const customFieldComponentMap = {
    collectionFunctionCurrentState: FieldCurrentState,
  };

  useEffect(() => {
    if (collectionFunctionId) {
      fetchCollectionFunction(deliveryFunctionId, collectionFunctionId);
    }
  }, [collectionFunctionId, deliveryFunctionId, fetchCollectionFunction]);

  useEffect(() => {
    const availableTemplates = adaptTemplateOptions(
      collectionFunctionTemplates,
      timezones,
      contacts,
      collectionFunctionGroupList,
      ftpInfos,
      networks,
      securityInfos,
      fqdnGroups,
      enabledCollectionFunctions,
      xcipioNodes,
    );

    setTemplates(availableTemplates);
  }, [
    collectionFunctionTemplates,
    timezones,
    contacts,
    ftpInfos,
    collectionFunctionGroupList,
    enabledCollectionFunctions,
    networks,
    securityInfos,
    fqdnGroups,
    xcipioNodes,
  ]);

  useEffect(() => {
    if (collectionFunction != null && templates != null) {
      const formData = adaptToForm(collectionFunction);
      let templateData = adaptTemplateVisibilityMode(templates[formData.tag], 'view');
      templateData = adaptTemplateTimezoneOptions(templateData, formData, timezones);
      templateData = adaptTemplateInterfaceFields(templateData, formData);
      templateData = adaptTemplate5GFields(templateData, is5GCVcpEnabled);

      setData(formData);
      setTemplate(templateData);
    }
  }, [collectionFunction, templates, timezones, is5GCVcpEnabled]);

  const handleCancel = () => {
    goToCollectionFunctionList(history);
  };

  const handleClickCopy = () => {
    goToCollectionFunctionCopy(history);
  };

  const handleClickCEdit = () => {
    goToCollectionFunctionEdit(history);
  };

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };

  const handleConfirmDeleteCollectionFunction = () => {

    if (collectionFunction?.id == null) {
      return;
    }

    const collectionFunctionBody: WithAuditDetails<void> = { data: undefined };

    setIsDeleteDialogOpen(false);

    if (isAuditEnabled) {
      const auditDetails = getAuditDetails(
        isAuditEnabled,
        userId,
        userName,
        getAuditFieldInfo(null),
        collectionFunction.name,
        AuditService.CF,
        AuditActionType.ACCESS_FUNCTION_DELETE,
      );
      collectionFunctionBody.auditDetails = auditDetails as AuditDetails;
    }

    deleteCollectionFunction(deliveryFunctionId, collectionFunction.id, collectionFunctionBody, history);

  };

  if (!template || !data) {
    return null;
  }

  return (
    <div className="app-container-content" data-testid="CollectionFunctionView">
      <CollectionFunctionForm
        ref={formRef}
        title="View Collection Function"
        isEditing={false}
        data={data}
        template={template}
        customFieldComponentMap={customFieldComponentMap}
        onClickCancelClose={handleCancel}
        onClickDelete={handleOpenDeleteDialog}
        onClickCopy={handleClickCopy}
        onClickEdit={handleClickCEdit}
      />
      <ModalDialog
        width="500px"
        isOpen={isDeleteDialogOpen}
        title="Delete Collection Function"
        displayMessage={`Are you sure you want to delete '${collectionFunction?.name}'?`}
        actionText="Yes, delete it"
        onSubmit={handleConfirmDeleteCollectionFunction}
        onClose={handleCloseDeleteDialog}
      />
    </div>
  );
};

export default withRouter(connector(CollectionFunctionView));
