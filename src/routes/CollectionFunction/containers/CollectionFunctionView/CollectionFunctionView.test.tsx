import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import {
  render,
  screen,
  waitFor,
  RenderResult,
  fireEvent,
  getByDisplayValue,
  getByLabelText,
  getAllByDisplayValue,
  getAllByLabelText,
  getAllByTestId,
  getByRole,
  getAllByText,
} from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { mockTemplates } from '__test__/mockTemplates';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { getAllByDataTest, getByDataTest } from '__test__/customQueries';
import { CollectionFunction, GET_COLLECTION_FUNCTION_PENDING, SELECT_COLLECTION_FUNCTION } from 'data/collectionFunction/collectionFunction.types';
import * as userActions from 'users/users-actions';
import CollectionFunctionRoute from 'routes/CollectionFunction/CollectionFunction';
import { API_PATH_FTP_INFOS } from 'data/ftpInfo/ftpInfo.types';
import * as securityInfoTypes from 'data/securityInfo/securityInfo.types';
import * as fqdnGroupTypes from 'data/fqdnGroup/fqdnGroup.types';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { CF_ROUTE } from '../../../../constants';
import CollectionFunctionView from './CollectionFunctionView';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);
describe('<CollectionFunctionView />', () => {
  let collectionFunctionWithUnsupportedTimezone: CollectionFunction;

  beforeEach(async () => {
    collectionFunctionWithUnsupportedTimezone = cloneDeep(mockCollectionFunctions[0]);
    collectionFunctionWithUnsupportedTimezone.timeZone = 'America/Tijuana';

    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onDelete(`/cfs/0/${mockCollectionFunctions[0].id}`).reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onGet('/users/userId1/settings?name=CollectionFunctionSettings').reply(200, { data: [] });
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet(`/cfs/0/${mockCollectionFunctions[0].id}?$view=options`).reply(200, { data: [mockCollectionFunctions[0]] });
    axiosMock.onGet('/cfs/0/ID_TO_GET_2?$view=options').reply(200, { data: [collectionFunctionWithUnsupportedTimezone] });
    axiosMock.onGet('/networks/0').reply(200, { data: mockNetworks });
    axiosMock.onAny().reply(200, { data: [] });

    store.dispatch({
      type: GET_COLLECTION_FUNCTION_PENDING,
      payload: null,
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: SELECT_COLLECTION_FUNCTION,
      payload: [mockCollectionFunctions[0]],
    });

    store.dispatch({
      type: securityInfoTypes.GET_SECURITY_INFOS_FULFILLED,
      payload: buildAxiosServerResponse(mockSecurityInfos),
    });

    store.dispatch({
      type: fqdnGroupTypes.GET_FQDN_GROUPS_FULFILLED,
      payload: buildAxiosServerResponse(mockFqdnGroups),
    });
  });

  describe('When loads CF view mode page', () => {
    let component: RenderResult;
    let container: HTMLElement;

    beforeEach(async () => {

      component = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionView />
          </MemoryRouter>
        </Provider>,
      );

      container = component.container;
      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());
    });

    describe('Main information', () => {

      it('should render the component with data-testid', () => {
        expect(screen.getByTestId('CollectionFunctionView')).toBeVisible();
        expect(screen.getByText('View Collection Function')).toBeVisible();
      });

      it('should render main information in read only mode', () => {
        expect(screen.getByText('LEA:')).toBeVisible();
        expect(screen.getByText('Handover Protocol:')).toBeVisible();
        expect(screen.getByText('Name:')).toBeVisible();

        expect(getByDataTest(container, 'input-cfGroup')).toBeDisabled();
        expect(getByDataTest(container, 'input-tag')).toBeDisabled();
        expect(getByDataTest(container, 'input-name')).toBeDisabled();

        expect(screen.getByDisplayValue('US 3GPP LI (TS 33.108) over TCP or FTP')).toBeVisible();
        expect(screen.getByDisplayValue('Select a LEA')).toBeVisible();
      });
    });

    describe('Facility Information', () => {
      it('should render Facility Information in read only mode', async () => {
        await waitFor(() => expect(getByDataTest(container, 'input-timeZone')).toBeDisabled());

        expect(screen.getByText('Delivery Type:')).toBeVisible();
        expect(screen.getByText('Time Zone:')).toBeVisible();
        expect(screen.getByText('Contact:')).toBeVisible();
        expect(screen.getByText('Contact:')).toBeVisible();
        expect(screen.getByText('Grace Timer:')).toBeVisible();

        expect(getByDataTest(container, 'input-deliveryType')).toBeDisabled();
        expect(getByDataTest(container, 'input-timeZone')).toBeDisabled();
        expect(getByDataTest(container, 'input-contact')).toBeDisabled();
        expect(getByDataTest(container, 'input-graceTimer')).toBeDisabled();
        expect(getByDataTest(container, 'input-insideNat')).toBeDisabled();

        expect(screen.getByDisplayValue('DSR')).toBeVisible();
        expect(screen.getByText('Select a contact')).toBeVisible();
        expect(screen.getByText('Select a time zone')).toBeVisible();
        expect(screen.getByDisplayValue('0')).toBeVisible();
        expect(screen.getByText('Xcipio is behind NAT')).toBeVisible();
        expect(getByDataTest(container, 'input-insideNat')).not.toBeChecked();

      });
    });

    describe('Intercept Related Information (IRI)', () => {
      it('should render IRI values in read only mode', () => {
        const iriTab = screen.getByTestId('MultiTabFormSectionIRI');

        fireEvent.click(getAllByTestId(iriTab, 'CollapsibleFormButton')[0]);

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-destPort')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-version')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-reqState')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[0]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-dscp')[0]).toBeDisabled();
        expect(getAllByDisplayValue(iriTab, 'Primary')[0]).toBeDisabled();
        expect(getAllByLabelText(iriTab, 'TPKT')[0]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[0]).toBeDisabled();

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[0].getAttribute('value')).toBe('202.202.202.202');
        expect(getAllByDataTest(iriTab, 'input-destPort')[0].getAttribute('value')).toBe('1111');
        expect(getAllByDisplayValue(iriTab, '15.5')[0]).toBeVisible();
        expect(getAllByDisplayValue(iriTab, 'Active')[0]).toBeVisible();
        expect(getAllByText(iriTab, 'Current State', { exact: false })[0]).toBeVisible();
        expect(getAllByTestId(iriTab, 'FieldCurrentState')[0].textContent).toBe('Inactive');
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[0].getAttribute('value')).toEqual('0.0.0.0');
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[0].getAttribute('value')).toEqual('0');
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[0].getAttribute('value')).toEqual('60');
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[0].getAttribute('value')).toEqual('6');
        expect(getAllByDataTest(iriTab, 'input-dscp')[0].getAttribute('value')).toEqual('0');
        expect(getAllByDisplayValue(iriTab, 'Primary')[0]).toBeVisible();
        expect(getAllByLabelText(iriTab, 'TPKT')[0]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[0].getAttribute('value')).toEqual('');

        // Tab 2 data

        fireEvent.click(getAllByTestId(iriTab, 'CollapsibleFormButton')[1]);

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-destPort')[1]).toBeDisabled();
        expect(getAllByDisplayValue(iriTab, '15.5')[1]).toBeDisabled();
        expect(getAllByDisplayValue(iriTab, 'Active')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[1]).toBeDisabled();
        expect(getAllByDataTest(iriTab, 'input-dscp')[1]).toBeDisabled();
        expect(getAllByDisplayValue(iriTab, 'Primary')[1]).toBeDisabled();
        expect(getAllByLabelText(iriTab, 'TPKT')[1]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[1]).toBeDisabled();

        expect(getAllByDataTest(iriTab, 'input-destIPAddr')[1].getAttribute('value')).toBe('100.100.100.100');
        expect(getAllByDataTest(iriTab, 'input-destPort')[1].getAttribute('value')).toBe('2222');
        expect(getAllByDisplayValue(iriTab, '15.5')[1]).toBeVisible();
        expect(getAllByDisplayValue(iriTab, 'Active')[1]).toBeVisible();
        expect(getAllByDataTest(iriTab, 'input-ownIPAddr')[1].getAttribute('value')).toEqual('0.0.0.0');
        expect(getAllByDataTest(iriTab, 'input-ownIPPort')[1].getAttribute('value')).toEqual('0');
        expect(getAllByDataTest(iriTab, 'input-keepAliveFreq')[1].getAttribute('value')).toEqual('60');
        expect(getAllByDataTest(iriTab, 'input-keepAliveRetries')[1].getAttribute('value')).toEqual('6');
        expect(getAllByDataTest(iriTab, 'input-dscp')[1].getAttribute('value')).toEqual('0');
        expect(getAllByDisplayValue(iriTab, 'Primary')[1]).toBeVisible();
        expect(getAllByLabelText(iriTab, 'TPKT')[1]).toBeChecked();
        expect(getAllByDataTest(iriTab, 'input-comments')[1].getAttribute('value')).toEqual('');
      });
    });

    describe('Communication Content (CC)', () => {
      it('should render CC from TAB 1 in read only mode', () => {
        const ccContainer = screen.getByTestId('CollectionFunctionInterfaceFormCC');

        const ccInterfaces = screen.getByTestId('MultiTabFormSectionCC');
        const ccTab = getByRole(ccInterfaces, 'tabpanel');

        expect(getByDisplayValue(ccContainer, 'Over TCP')).toBeVisible();

        expect(getByDataTest(ccTab, 'input-destIPAddr').getAttribute('value')).toEqual('34.45.56.67');
        expect(getByDataTest(ccTab, 'input-destPort').getAttribute('value')).toEqual('4545');
        expect(getByDisplayValue(ccTab, '15.5')).toBeVisible();

        fireEvent.click(getAllByTestId(ccInterfaces, 'CollapsibleFormButton')[0]);

        expect(getByDataTest(ccTab, 'input-ownIPAddr').getAttribute('value')).toEqual('');
        expect(getByDataTest(ccTab, 'input-ownIPPort').getAttribute('value')).toEqual('');
        expect(getByDataTest(ccTab, 'input-keepAliveFreq').getAttribute('value')).toEqual('60');
        expect(getByDataTest(ccTab, 'input-keepAliveRetries').getAttribute('value')).toEqual('6');

        expect(getByDataTest(ccTab, 'input-dscp').getAttribute('value')).toEqual('0');
        expect(getByDisplayValue(ccTab, 'Primary')).toBeVisible();
        expect(getByLabelText(ccTab, 'Use XDP')).not.toBeChecked();
        expect(getByLabelText(ccTab, 'TPKT')).not.toBeChecked();

        expect(getByDataTest(ccTab, 'input-inactivityTimer').getAttribute('value')).toEqual('0');
        expect(getByDataTest(ccTab, 'input-displayLabel').getAttribute('value')).toEqual('');
        expect(getByDataTest(ccTab, 'input-comments').getAttribute('value')).toEqual('');
      });
    });
  });

  describe('When test unsupported timezone', () => {
    it('should load the timezone even if it is not supported', async () => {
      store.dispatch({
        type: SELECT_COLLECTION_FUNCTION,
        payload: [{ id: 'ID_TO_GET_2' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionView />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('View Collection Function')).toBeVisible());

      expect(screen.getByLabelText('Time Zone:*')).toHaveValue('America/Tijuana');
    });
  });

  describe('When test View mode Actions', () => {
    it('should go to Edit Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/view`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );
      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());

      fireEvent.click((screen.getAllByTestId('FormButtonsEdit')[0]) as Element);

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());
    });

    it('should go to Copy Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/view`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );
      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());

      fireEvent.click((screen.getAllByTestId('FormButtonsCopy')[0]) as Element);

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());

    });

    it('should select a Collection Function to delete, but cancel the operation', async () => {

      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/view`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const [_deletedCf, ...mockOthersCollectionFunctions] = mockCollectionFunctions;

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());

      fireEvent.click((screen.getAllByTestId('FormButtonsDelete')[0]) as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();

      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Cancel') as Element);

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());
    });

    it('should select a Collection Function and Delete it', async () => {

      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/view`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const [_deletedCf, ...mockOthersCollectionFunctions] = mockCollectionFunctions;

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());

      fireEvent.click((screen.getAllByTestId('FormButtonsDelete')[0]) as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();

      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Yes, delete it') as Element);

      axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockOthersCollectionFunctions });

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      // rows after delete CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockOthersCollectionFunctions.length);
      expect(screen.getByText(`${mockCollectionFunctions[0].name} successfully deleted.`)).toBeVisible();

    });
  });
});
