import React from 'react';
import {
  render, screen, waitFor, fireEvent, RenderResult, waitForElementToBeRemoved, act,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import MockAdapter from 'axios-mock-adapter';
import store from 'data/store';
import AuthActions from 'data/authentication/authentication.types';
import Http from 'shared/http';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as networkTypes from 'data/network/network.types';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import { buildAxiosServerResponse } from '__test__/utils';
import { CollectionFunction, SELECT_COLLECTION_FUNCTION } from 'data/collectionFunction/collectionFunction.types';
import CollectionFunctionRoute from 'routes/CollectionFunction/CollectionFunction';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey, mockTemplates } from '__test__/mockTemplates';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { API_PATH_FTP_INFOS } from 'data/ftpInfo/ftpInfo.types';
import { CF_ROUTE } from '../../../../constants';
import CollectionFunctionList from './CollectionFunctionList';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunctionList />', () => {
  const enabledCollectionFunction = mockSupportedFeatures.config.collectionFunctions;

  beforeEach(() => {

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: networkTypes.GET_NETWORKS_FULFILLED,
      payload: buildAxiosServerResponse(mockNetworks),
    });

    axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockCollectionFunctions });
    axiosMock.onDelete(`/cfs/0/${mockCollectionFunctions[0].id}`).reply(200, { data: [] });
    axiosMock.onGet('/cf-groups/0?$orderBy=NAME asc').reply(200, { data: mockCollectionFunctionGroups });
    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet(`${API_PATH_FTP_INFOS}/0`).reply(200, { data: mockFtpInfos });
    axiosMock.onGet('/users/userId1/settings?name=CollectionFunctionSettings').reply(200, { data: [] });
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet(`/cfs/0/${mockCollectionFunctions[0].id}?$view=options`).reply(200, { data: [mockCollectionFunctions[0]] });
    axiosMock.onGet('/networks/0').reply(200, { data: mockNetworks });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onGet('/fqdn-groups/0').reply(200, { data: mockFqdnGroups });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  describe('when retrieve collection function data from server', () => {
    let row0: Element;
    let row1: Element;
    let row2: Element;
    let row3: Element;
    let row4: Element;
    let row5: Element;

    beforeEach(async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      [row0, row1, row2, row3, row4, row5] = Array.from(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row'));

    });

    it('should render CF grid', async () => {
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockCollectionFunctions.length);
    });

    it('should render Name column', async () => {
      await waitFor(() => expect(screen.getByText('Name')).toBeVisible());

      expect(row0.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[0].name);
      expect(row1.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[1].name);
      expect(row2.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[2].name);
      expect(row3.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[3].name);
      expect(row4.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[4].name);
      expect(row5.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[5].name);
    });

    it('should render Handover Protocol column', async () => {
      await waitFor(() => expect(screen.getByText('Handover Protocol')).toBeVisible());

      expect(row0.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[0].tag].title);
      expect(row1.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[1].tag].title);
      expect(row2.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[2].tag].title);
      expect(row3.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[3].tag].title);
      expect(row4.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[4].tag].title);
      expect(row5.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[5].tag].title);
    });

    it('should render LEA column', async () => {
      // just to remember, the LEA is based on CF group related to the CF
      await waitFor(() => expect(expect(screen.getByText('LEA')).toBeVisible()));

      expect(row0.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE');
      expect(row1.querySelector('[col-id="cfGroup"')?.textContent).toBe('CookCounty');
      expect(row2.querySelector('[col-id="cfGroup"')?.textContent).toBe('CookCounty');
      expect(row3.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE-TIA1072');
      expect(row4.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE-5GCF');
      expect(row5.querySelector('[col-id="cfGroup"')?.textContent).toBe('Flavia');
    });

    it('should render Xcipio behind NAT column', async () => {
      await waitFor(() => expect(expect(screen.getByText('Xcipio behind NAT')).toBeVisible()));

      expect(row0.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row1.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row2.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
      expect(row3.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
      expect(row4.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row5.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
    });

    describe('when render Intercept Related Information - Interface 1 columns', () => {

      it('should render Intercept Related Information Transport Protocol column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('Transport Protocol')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row1.querySelector('[col-id="transportType"')?.textContent).toBe('FTP');
        expect(row2.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row3.querySelector('[col-id="transportType"')?.textContent).toBe('FTP');
        expect(row4.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row5.querySelector('[col-id="transportType"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information IP Address column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('IP Address')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row1.querySelector('[col-id="destIPAddr"')?.textContent).toBe('100.100.100.100');
        expect(row2.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row3.querySelector('[col-id="destIPAddr"')?.textContent).toBe('100.100.100.100');
        expect(row4.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row5.querySelector('[col-id="destIPAddr"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information  Port column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('Port')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row1.querySelector('[col-id="destPort"')?.textContent).toBe('2222');
        expect(row2.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row3.querySelector('[col-id="destPort"')?.textContent).toBe('2222');
        expect(row4.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row5.querySelector('[col-id="destPort"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information Current State column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Current State')).toBeVisible()));

        expect(row0.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row1.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row2.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row3.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row4.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row5.querySelector('[col-id="state"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information Configured State column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Configured State')).toBeVisible()));

        expect(row0.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row1.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row2.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row3.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row4.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row5.querySelector('[col-id="reqState"')?.textContent).toBe('-');
      });
    });

    describe('when render Communication Content - Interface 1 columns', () => {

      it('should render Communication Content Transport Protocol column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('Transport Protocol')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row1.querySelector('[col-id="transportType_1"')?.textContent).toBe('FTP');
        expect(row2.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row3.querySelector('[col-id="transportType_1"')?.textContent).toBe('FTP');
        expect(row4.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row5.querySelector('[col-id="transportType_1"')?.textContent).toBe('-');
      });

      it('should render Communication Content IP Address column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('IP Address')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row1.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('56.34.45.45');
        expect(row2.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row3.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('56.34.45.45');
        expect(row4.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row5.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('-');
      });

      it('should render Communication Content Port column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('Port')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row1.querySelector('[col-id="destPort_1"')?.textContent).toBe('8978');
        expect(row2.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row3.querySelector('[col-id="destPort_1"')?.textContent).toBe('8978');
        expect(row4.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row5.querySelector('[col-id="destPort_1"')?.textContent).toBe('-');
      });
    });
  });

  describe('when retrieve empty collection function data from server', () => {

    beforeEach(async () => {
      axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: [] });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    });

    it('should not have any CF rows and render an informative message', async () => {
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(0);
      expect(screen.getByText('No Rows To Show')).toBeVisible();
    });

  });

  describe('When filter a CF by Name', () => {

    it('should filter the TLS grid according the input passed', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      // Show ALL CFs before filtering
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(6);

      const filterInput = container.querySelector('[placeholder="Search"]');

      if (filterInput) {
        act(() => {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'CA-DOJBNE-',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        });
      }

      // show 3 matching search CFs
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(3);

      if (filterInput) {
        act(() => {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'CA-DOJBNE-5GCF',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        });
      }

      // show 1 matching search CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(1);
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row')[0].querySelector('[col-id="name"')?.textContent).toBe('CA-DOJBNE-5GCF');

      if (filterInput) {
        act(() => {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'Do not match any name',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        });
      }

      // show 0 matching search CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(0);
    });
  });

  describe('When press "Refresh" button', () => {
    let mockUpdatedCollectionFunctions: CollectionFunction[];
    let updatedCollectionFunction: CollectionFunction;
    let newCollectionFunction: CollectionFunction;

    let component: RenderResult;
    let row0: Element;
    let row1: Element;
    let row2: Element;
    let row3: Element;
    let row4: Element;
    let row5: Element;
    let row6: Element;

    beforeEach(async () => {
      // when render the page, the CFs request will retrieve the previous CF list version mocked
      component = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      // Let's simulate that another user made some updates in CF list, updating a CF of the list and adding another one
      updatedCollectionFunction = { ...mockCollectionFunctions[0], name: 'Updated CF' };
      newCollectionFunction = { ...mockCollectionFunctions[0], id: 'new_ID', name: 'New CF' };

      mockUpdatedCollectionFunctions = [...mockCollectionFunctions];
      mockUpdatedCollectionFunctions.splice(0, 1, updatedCollectionFunction);
      mockUpdatedCollectionFunctions.push(newCollectionFunction);

      // Now the next CFs request will retrieve the updated cf version
      axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockUpdatedCollectionFunctions });
    });

    it('should render the CFs retrieved by the server and them render an updated version of the grid after click refresh', async () => {
      const refreshButton = component.container.querySelector('[icon="refresh"]')?.parentElement;

      [row0, row1, row2, row3, row4, row5, row6] = Array.from(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row'));

      // current state of the CFs
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(6);
      expect(row0.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[0].name);
      expect(row1.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[1].name);
      expect(row2.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[2].name);
      expect(row3.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[3].name);
      expect(row4.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[4].name);
      expect(row5.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[5].name);
      expect(row6).toBe(undefined);

      act(() => {
        fireEvent.click(refreshButton as Element);
      });

      await waitFor(() => expect(screen.getByText(mockUpdatedCollectionFunctions[6].name)).toBeVisible());

      [row0, row1, row2, row3, row4, row5, row6] = Array.from(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row'));

      // updated state of the CFs
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(7);
      expect(row0.querySelector('[col-id="name"')?.textContent).toBe(mockUpdatedCollectionFunctions[0].name);
      expect(row1.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[1].name);
      expect(row2.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[2].name);
      expect(row3.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[3].name);
      expect(row4.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[4].name);
      expect(row5.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[5].name);
      expect(row6.querySelector('[col-id="name"')?.textContent).toBe(mockUpdatedCollectionFunctions[6].name);

    });
  });

  describe('Collection Function Actions', () => {

    it('should redirect to create Collection Function Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText('New Collection Function'));
      });

      expect(screen.getByTestId('CollectionFunctionCreate')).toBeVisible();
      expect(screen.getByText('New Collection Function')).toBeVisible();
    });

    it('should go to View Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      act(() => {
        fireEvent.doubleClick(screen.getByText(mockCollectionFunctions[0].name) as Element);
      });

      store.dispatch({
        type: SELECT_COLLECTION_FUNCTION,
        payload: [mockCollectionFunctions[0]],
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionView')).toBeVisible());
    });

    it('should go to Edit Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText(mockCollectionFunctions[0].name) as Element);
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionListMenu')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText('edit') as Element);
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionEdit')).toBeVisible());
    });

    it('should go to Copy Page', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${CF_ROUTE}/`]}>
            <CollectionFunctionRoute />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText(mockCollectionFunctions[0].name) as Element);
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionListMenu')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText('copy') as Element);
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionCopy')).toBeVisible());
    });

    it('should select a Collection Function to delete, but cancel the operation', async () => {

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const [_deletedCf, ...mockOthersCollectionFunctions] = mockCollectionFunctions;

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      // rows before delete CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockCollectionFunctions.length);
      act(() => {
        fireEvent.click(screen.getByText(mockCollectionFunctions[0].name) as Element);
      });
      await waitFor(() => expect(screen.getByTestId('CollectionFunctionListMenu')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText('delete') as Element);
      });

      expect(screen.getByText('Delete Collection Function')).toBeVisible();

      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      act(() => {
        fireEvent.click(screen.getByText('Cancel') as Element);
      });

      axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockOthersCollectionFunctions });

      // rows after delete CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockCollectionFunctions.length);
    });

    it('should select a Collection Function and Delete it', async () => {

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionList suppressColumnVirtualisation />
          </MemoryRouter>
        </Provider>,
      );

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const [_deletedCf, ...mockOthersCollectionFunctions] = mockCollectionFunctions;

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      // rows before delete CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockCollectionFunctions.length);

      act(() => {
        fireEvent.click(screen.getByText(mockCollectionFunctions[0].name) as Element);
      });

      await waitFor(() => expect(screen.getByTestId('CollectionFunctionListMenu')).toBeVisible());

      act(() => {
        fireEvent.click(screen.getByText('delete') as Element);
      });

      expect(screen.getByText('Delete Collection Function')).toBeVisible();

      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      act(() => {
        fireEvent.click(screen.getByText('Yes, delete it') as Element);
      });

      axiosMock.onGet('/cfs/0?$view=options').reply(200, { data: mockOthersCollectionFunctions });

      await waitForElementToBeRemoved(screen.getByText(mockCollectionFunctions[0].name));

      // rows after delete CF
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockOthersCollectionFunctions.length);
      expect(screen.getByText(`${mockCollectionFunctions[0].name} successfully deleted.`)).toBeVisible();

    });

  });

});
