import React from 'react';
import {
  render, screen, waitFor, fireEvent, RenderResult,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import Http from 'shared/http';
import MockAdapter from 'axios-mock-adapter';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { collectionFunctionsColumnDefs } from 'routes/CollectionFunction/CollectionFunction.types';
import adaptCollectionFunctionsToGrid from 'routes/CollectionFunction/utils/collectionFunctionGrid.adapter';
import { FormTemplateMapByKey } from 'data/template/template.types';
import CollectionFunctionGrid from './CollectionFunctionGrid';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<CollectionFunctionGrid />', () => {
  const lastUpdatedAt = new Date('Thu Aug 27 2020 11:13:39 GMT-0300');
  let userSettings: unknown;
  const enabledCollectionFunction = mockSupportedFeatures.config.collectionFunctions;
  let collectionFunctionTemplates: FormTemplateMapByKey;

  beforeEach(() => {
    userSettings = {
      value: {
        data: {
          data: [
            {
              setting: '[]',
            },
          ],
        },
      },
    };

    collectionFunctionTemplates = {
      'CF-3GPP-33108': getMockTemplateByKey('CF-3GPP-33108'),
      'CF-ETSI-102232': getMockTemplateByKey('CF-ETSI-102232'),
      'CF-JSTD025A': getMockTemplateByKey('CF-JSTD025A'),
      'CF-JSTD025B': getMockTemplateByKey('CF-JSTD025B'),
    };
  });

  describe('when Collection Function grid has data', () => {

    let component: RenderResult;
    let row0: Element;
    let row1: Element;
    let row2: Element;
    let row3: Element;
    let row4: Element;
    let row5: Element;

    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelectionChanged = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleView = jest.fn();
    const handleEdit = jest.fn();
    const onGetGridSettings: () => any = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      const data = adaptCollectionFunctionsToGrid(
        mockCollectionFunctions,
        enabledCollectionFunction,
      );

      component = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionGrid
              suppressColumnVirtualisation
              data={data}
              columnDefs={collectionFunctionsColumnDefs}
              noResults={false}
              beforeLogout={false}
              lastUpdatedAt={lastUpdatedAt}
              hasPrivilegeToCopy
              hasPrivilegesToCreate
              hasPrivilegeToDelete
              hasPrivilegeToView
              enabledCollectionFunctions={enabledCollectionFunction}
              collectionFunctionTemplates={collectionFunctionTemplates}
              onRefresh={handleRefresh}
              onCreateNew={handleCreateNew}
              onSelectionChanged={handleSelectionChanged}
              onSaveGridSettings={handleSaveGridSettings}
              onCopy={handleCopy}
              onDelete={handleDelete}
              onEdit={handleEdit}
              onView={handleView}
              onGetGridSettings={onGetGridSettings}
            />
          </MemoryRouter>
        </Provider>,
      );

      axiosMock.onAny().reply(200);

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      [row0, row1, row2, row3, row4, row5] = Array.from(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row'));

    });

    it('should render CF grid', async () => {
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(mockCollectionFunctions.length);
      expect(screen.getByText('Last updated at 08/27/20 07:13:39 AM')).toBeVisible();
    });

    it('should render CF Name column', async () => {

      expect(row0.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[0].name);
      expect(row1.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[1].name);
      expect(row2.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[2].name);
      expect(row3.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[3].name);
      expect(row4.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[4].name);
      expect(row5.querySelector('[col-id="name"')?.textContent).toBe(mockCollectionFunctions[5].name);
    });

    it('should render Handover Protocol column', async () => {
      await waitFor(() => expect(screen.getByText('Handover Protocol')).toBeVisible());

      expect(row0.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[0].tag].title);
      expect(row1.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[1].tag].title);
      expect(row2.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[2].tag].title);
      expect(row3.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[3].tag].title);
      expect(row4.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[4].tag].title);
      expect(row5.querySelector('[col-id="tagName"')?.textContent).toBe(enabledCollectionFunction[mockCollectionFunctions[5].tag].title);
    });

    it('should render LEA column', async () => {
      // just to remember, the LEA is based on CF group related to the CF
      await waitFor(() => expect(expect(screen.getByText('LEA')).toBeVisible()));

      expect(row0.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE');
      expect(row1.querySelector('[col-id="cfGroup"')?.textContent).toBe('CookCounty');
      expect(row2.querySelector('[col-id="cfGroup"')?.textContent).toBe('CookCounty');
      expect(row3.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE-TIA1072');
      expect(row4.querySelector('[col-id="cfGroup"')?.textContent).toBe('CA-DOJBNE-5GCF');
      expect(row5.querySelector('[col-id="cfGroup"')?.textContent).toBe('Flavia');
    });

    it('should render Xcipio behind NAT column', async () => {
      await waitFor(() => expect(expect(screen.getByText('Xcipio behind NAT')).toBeVisible()));

      expect(row0.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row1.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row2.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
      expect(row3.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
      expect(row4.querySelector('[col-id="insideNat"')?.textContent).toBe('no');
      expect(row5.querySelector('[col-id="insideNat"')?.textContent).toBe('yes');
    });

    describe('when render Intercept Related Information - Interface 1 columns', () => {

      it('should render Intercept Related Information Transport Protocol column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('Transport Protocol')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row1.querySelector('[col-id="transportType"')?.textContent).toBe('FTP');
        expect(row2.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row3.querySelector('[col-id="transportType"')?.textContent).toBe('FTP');
        expect(row4.querySelector('[col-id="transportType"')?.textContent).toBe('TCP');
        expect(row5.querySelector('[col-id="transportType"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information IP Address column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('IP Address')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row1.querySelector('[col-id="destIPAddr"')?.textContent).toBe('100.100.100.100');
        expect(row2.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row3.querySelector('[col-id="destIPAddr"')?.textContent).toBe('100.100.100.100');
        expect(row4.querySelector('[col-id="destIPAddr"')?.textContent).toBe('202.202.202.202');
        expect(row5.querySelector('[col-id="destIPAddr"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information  Port column', async () => {
        await waitFor(() => expect(expect(screen.getAllByText('Port')[0]).toBeVisible()));

        expect(row0.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row1.querySelector('[col-id="destPort"')?.textContent).toBe('2222');
        expect(row2.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row3.querySelector('[col-id="destPort"')?.textContent).toBe('2222');
        expect(row4.querySelector('[col-id="destPort"')?.textContent).toBe('1111');
        expect(row5.querySelector('[col-id="destPort"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information Current State column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Current State')).toBeVisible()));

        expect(row0.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row1.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row2.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row3.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row4.querySelector('[col-id="state"')?.textContent).toBe('Inactive');
        expect(row5.querySelector('[col-id="state"')?.textContent).toBe('-');
      });

      it('should render Intercept Related Information Configured State column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Configured State')).toBeVisible()));

        expect(row0.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row1.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row2.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row3.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row4.querySelector('[col-id="reqState"')?.textContent).toBe('Active');
        expect(row5.querySelector('[col-id="reqState"')?.textContent).toBe('-');
      });
    });

    describe('when render Communication Content - Interface 1 columns', () => {

      it('should render Communication Content Transport Protocol column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('Transport Protocol')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row1.querySelector('[col-id="transportType_1"')?.textContent).toBe('FTP');
        expect(row2.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row3.querySelector('[col-id="transportType_1"')?.textContent).toBe('FTP');
        expect(row4.querySelector('[col-id="transportType_1"')?.textContent).toBe('TCP');
        expect(row5.querySelector('[col-id="transportType_1"')?.textContent).toBe('-');
      });

      it('should render Communication Content IP Address column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('IP Address')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row1.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('56.34.45.45');
        expect(row2.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row3.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('56.34.45.45');
        expect(row4.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('34.45.56.67');
        expect(row5.querySelector('[col-id="destIPAddr_1"')?.textContent).toBe('-');
      });

      it('should render Communication Content Port column', async () => {
        await waitFor(() => expect(expect(screen.getByText('Communication Content - Interface 1')).toBeVisible()));
        await waitFor(() => expect(expect(screen.getAllByText('Port')[1]).toBeVisible()));

        expect(row0.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row1.querySelector('[col-id="destPort_1"')?.textContent).toBe('8978');
        expect(row2.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row3.querySelector('[col-id="destPort_1"')?.textContent).toBe('8978');
        expect(row4.querySelector('[col-id="destPort_1"')?.textContent).toBe('4545');
        expect(row5.querySelector('[col-id="destPort_1"')?.textContent).toBe('-');
      });

      it('should filter the TLS grid according the input passed', async () => {

        await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

        // Show ALL CFs before filtering
        expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(6);

        const filterInput = component.container.querySelector('[placeholder="Search"]');

        if (filterInput) {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'CA-DOJBNE-',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        }

        // show 3 matching search CFs
        expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(3);

        if (filterInput) {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'CA-DOJBNE-5GCF',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        }

        // show 1 matching search CF
        expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(1);
        expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row')[0].querySelector('[col-id="name"')?.textContent).toBe('CA-DOJBNE-5GCF');

        if (filterInput) {
          fireEvent.change(filterInput as Element, {
            target: {
              value: 'Do not match any CF name',
            },
          } as React.ChangeEvent<HTMLInputElement>);
        }

        // show 0 matching search CF
        expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(0);
      });
    });
  });

  describe('when Collection Function grid has not data', () => {
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelectionChanged = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleView = jest.fn();
    const handleEdit = jest.fn();
    const onGetGridSettings: () => any = () => Promise.resolve(userSettings);

    beforeEach(async () => {

      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionGrid
              suppressColumnVirtualisation
              data={[]}
              columnDefs={collectionFunctionsColumnDefs}
              noResults={false}
              beforeLogout={false}
              lastUpdatedAt={lastUpdatedAt}
              hasPrivilegeToCopy
              hasPrivilegesToCreate
              hasPrivilegeToDelete
              hasPrivilegeToView
              enabledCollectionFunctions={enabledCollectionFunction}
              collectionFunctionTemplates={collectionFunctionTemplates}
              onRefresh={handleRefresh}
              onCreateNew={handleCreateNew}
              onSelectionChanged={handleSelectionChanged}
              onSaveGridSettings={handleSaveGridSettings}
              onCopy={handleCopy}
              onDelete={handleDelete}
              onEdit={handleEdit}
              onView={handleView}
              onGetGridSettings={onGetGridSettings}
            />
          </MemoryRouter>
        </Provider>,
      );

      axiosMock.onAny().reply(200);

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    });

    it('should not have any CF rows and render an informative message', async () => {
      expect(screen.getByRole('grid').querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(0);
      expect(screen.getByText('No Rows To Show')).toBeVisible();
    });

    it('should not render action menu button if no Collection is selected', () => {
      expect(document.querySelector('[data-testid="CollectionFunctionListMenu"]')).toBeNull();
    });

  });

  describe('when dispatch action on grid', () => {
    let component: RenderResult;
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelectionChanged = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleView = jest.fn();
    const handleEdit = jest.fn();
    const onGetGridSettings: () => any = () => Promise.resolve(userSettings);
    const data = adaptCollectionFunctionsToGrid(mockCollectionFunctions, enabledCollectionFunction);

    beforeEach(async () => {

      component = render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionGrid
              suppressColumnVirtualisation
              data={data}
              selectedCollectionFunction={data[0]}
              columnDefs={collectionFunctionsColumnDefs}
              noResults={false}
              beforeLogout={false}
              lastUpdatedAt={lastUpdatedAt}
              hasPrivilegeToCopy
              hasPrivilegesToCreate
              hasPrivilegeToDelete
              hasPrivilegeToView
              enabledCollectionFunctions={enabledCollectionFunction}
              collectionFunctionTemplates={collectionFunctionTemplates}
              onRefresh={handleRefresh}
              onCreateNew={handleCreateNew}
              onSelectionChanged={handleSelectionChanged}
              onSaveGridSettings={handleSaveGridSettings}
              onCopy={handleCopy}
              onDelete={handleDelete}
              onEdit={handleEdit}
              onView={handleView}
              onGetGridSettings={onGetGridSettings}
            />
          </MemoryRouter>
        </Provider>,
      );

      axiosMock.onAny().reply(200);

      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    });

    it('should click refresh button and call onRefresh action', async () => {
      fireEvent.click(component.container.querySelector('[icon="refresh"]')?.parentElement as Element);

      expect(handleRefresh).toBeCalled();
    });

    it('should click refresh button and call onCreateNew action', async () => {
      fireEvent.click(screen.getByText('New Collection Function') as Element);

      expect(handleCreateNew).toBeCalled();
    });

    it('should select a Collection Function and call onEdit callback', async () => {

      fireEvent.click(screen.getByText(data[0].name) as Element);

      fireEvent.click(screen.getByText('edit') as Element);

      expect(handleEdit).toBeCalled();
    });

    it('should select a Collection Function and call onCopy callback', async () => {

      fireEvent.click(screen.getByText(data[0].name) as Element);

      fireEvent.click(screen.getByText('copy') as Element);

      expect(handleCopy).toBeCalled();
    });

    it('should select a Collection Function to delete, but cancel the operation', async () => {

      fireEvent.click(screen.getByText(data[0].name) as Element);

      fireEvent.click(screen.getByText('delete') as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();
      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Cancel') as Element);

      expect(handleDelete).not.toBeCalled();
    });

    it('should select a Collection Function and call onDelete callback', async () => {

      fireEvent.click(screen.getByText(data[0].name) as Element);

      fireEvent.click(screen.getByText('delete') as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();
      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Yes, delete it') as Element);

      expect(handleDelete).toBeCalled();
    });
  });

  describe('when template is not available', () => {
    const onGetGridSettings: () => any = () => Promise.resolve(userSettings);
    const data = adaptCollectionFunctionsToGrid(mockCollectionFunctions, enabledCollectionFunction);

    beforeEach(() => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <CollectionFunctionGrid
              suppressColumnVirtualisation
              data={data}
              selectedCollectionFunction={data[0]}
              columnDefs={collectionFunctionsColumnDefs}
              noResults={false}
              beforeLogout={false}
              lastUpdatedAt={lastUpdatedAt}
              hasPrivilegeToCopy
              hasPrivilegesToCreate
              hasPrivilegeToDelete
              hasPrivilegeToView
              enabledCollectionFunctions={enabledCollectionFunction}
              collectionFunctionTemplates={{}}
              onRefresh={jest.fn()}
              onCreateNew={jest.fn()}
              onSelectionChanged={jest.fn()}
              onSaveGridSettings={jest.fn()}
              onCopy={jest.fn()}
              onDelete={jest.fn()}
              onEdit={jest.fn()}
              onView={jest.fn()}
              onGetGridSettings={onGetGridSettings}
            />
          </MemoryRouter>
        </Provider>,
      );

      axiosMock.onAny().reply(200);
    });

    it('should show a popup on edit', async () => {
      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      fireEvent.click(screen.getByText(data[0].name));
      fireEvent.click(screen.getByText('edit'));

      await waitFor(() => expect(screen.getByText('Edit Collection Function - Missing Template')).toBeVisible());
      fireEvent.click(screen.getByText('Close'));
      await waitFor(() => expect(screen.queryAllByText('Edit Collection Function - Missing Template')).toHaveLength(0));
    });

    it('should show a popup on copy', async () => {
      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      fireEvent.click(screen.getByText(data[0].name));
      fireEvent.click(screen.getByText('copy'));

      await waitFor(() => expect(screen.getByText('Copy Collection Function - Missing Template')).toBeVisible());
      fireEvent.click(screen.getByText('Close'));
      await waitFor(() => expect(screen.queryAllByText('Copy Collection Function - Missing Template')).toHaveLength(0));
    });

    it('should show a popup on view', async () => {
      await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

      fireEvent.doubleClick(screen.getByText(data[0].name));

      await waitFor(() => expect(screen.getByText('View Collection Function - Missing Template')).toBeVisible());
      fireEvent.click(screen.getByText('Close'));
      await waitFor(() => expect(screen.queryAllByText('View Collection Function - Missing Template')).toHaveLength(0));
    });
  });
});
