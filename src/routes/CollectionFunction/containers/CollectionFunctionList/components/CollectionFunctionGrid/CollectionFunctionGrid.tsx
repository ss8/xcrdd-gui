import React, { FunctionComponent } from 'react';
import { AxiosPromise } from 'axios';
import CommonGrid from 'shared/commonGrid';
import { SelectionChangedEvent, ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import { CollectionFunction, CollectionFunctionListData } from 'data/collectionFunction/collectionFunction.types';
import { ServerResponse, UserSettings, GridSettings } from 'data/types';
import { SupportedFeaturesCollectionFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { FormTemplateMapByKey } from 'data/template/template.types';
import ModalDialog from 'shared/modal/dialog';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import useConfirmActionDialog from 'utils/hooks/useConfirmActionDialog';
import CollectionFunctionListMenu from '../CollectionFunctionListMenu';
import CollectionFunctionMissingTemplateDialogBody from '../CollectionFunctionMissingTemplateDialogBody/CollectionFunctionMissingTemplateDialogBody';

type Props = {
  selectedCollectionFunction?: CollectionFunction;
  data: CollectionFunctionListData[];
  hasPrivilegeToCopy: boolean;
  hasPrivilegesToCreate: boolean
  hasPrivilegeToDelete: boolean
  hasPrivilegeToView: boolean
  noResults: boolean,
  beforeLogout: boolean,
  lastUpdatedAt: Date | undefined,
  columnDefs: (ColGroupDef | ColDef)[],
  suppressColumnVirtualisation?: boolean,
  enabledCollectionFunctions: SupportedFeaturesCollectionFunctions;
  collectionFunctionTemplates: FormTemplateMapByKey;
  onRefresh: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onCreateNew: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onSelectionChanged: (event: SelectionChangedEvent) => void;
  onSaveGridSettings: (gridSettings: GridSettings) => void;
  onGetGridSettings: () => { type: string, payload: AxiosPromise<ServerResponse<UserSettings>> };
  onView: () => void;
  onEdit: () => void;
  onCopy: () => void;
  onDelete: () => void;
}

const CollectionFunctionGrid: FunctionComponent<Props> = ({
  hasPrivilegesToCreate,
  hasPrivilegeToDelete,
  hasPrivilegeToView,
  data = [],
  noResults,
  beforeLogout,
  lastUpdatedAt,
  columnDefs,
  suppressColumnVirtualisation,
  selectedCollectionFunction,
  enabledCollectionFunctions,
  collectionFunctionTemplates,
  onRefresh,
  onCreateNew,
  onSelectionChanged,
  onSaveGridSettings,
  onGetGridSettings,
  onView,
  onEdit,
  onCopy,
  onDelete,
}: Props) => {
  const [confirmActionDialog, setConfirmActionDialog] = useConfirmActionDialog();

  const frameworkComponents = {
    actionMenuCellRenderer: ActionMenuCellRenderer,
  };

  const showPopupForUnavailableTemplate = (title: string, collectionFunctionTag: string) => {
    const collectionFunctionTitle = enabledCollectionFunctions[collectionFunctionTag].title;

    setConfirmActionDialog(
      `${title} Collection Function - Missing Template`,
      <CollectionFunctionMissingTemplateDialogBody name={collectionFunctionTitle} />,
      'Close',
    );
  };

  const isTemplateAvailable = ({ tag }: CollectionFunction) => {
    return collectionFunctionTemplates[tag] != null;
  };

  const handleEdit = () => {
    if (selectedCollectionFunction == null) {
      return;
    }

    if (isTemplateAvailable(selectedCollectionFunction)) {
      onEdit();
    } else {
      showPopupForUnavailableTemplate('Edit', selectedCollectionFunction.tag);
    }
  };

  const handleCopy = () => {
    if (selectedCollectionFunction == null) {
      return;
    }

    if (isTemplateAvailable(selectedCollectionFunction)) {
      onCopy();
    } else {
      showPopupForUnavailableTemplate('Copy', selectedCollectionFunction.tag);
    }
  };

  const handleView = () => {
    if (selectedCollectionFunction == null) {
      return;
    }

    if (isTemplateAvailable(selectedCollectionFunction)) {
      onView();
    } else {
      showPopupForUnavailableTemplate('View', selectedCollectionFunction.tag);
    }
  };

  return (
    <div className="grid-container" style={{ height: 'inherit' }}>
      <CommonGrid
        createNewText="New Collection Function"
        pagination
        enableBrowserTooltips
        rowSelection={ROWSELECTION.SINGLE}
        hasPrivilegeToCreateNew={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegetoView={hasPrivilegeToView}
        rowData={data}
        noResults={noResults}
        isBeforeLogout={beforeLogout}
        columnDefs={columnDefs}
        frameworkComponents={frameworkComponents}
        suppressColumnVirtualisation={suppressColumnVirtualisation}
        lastUpdatedAt={lastUpdatedAt}
        handleCreateNew={onCreateNew}
        onRefresh={onRefresh}
        onSelectionChanged={onSelectionChanged}
        saveGridSettings={onSaveGridSettings}
        getGridSettingsFromServer={onGetGridSettings}
        onActionMenuItemClick={handleView}
      >
        <CollectionFunctionListMenu
          selectedCollectionFunction={selectedCollectionFunction}
          hasPrivilegeToCreateNew={hasPrivilegesToCreate}
          hasPrivilegeToDelete={hasPrivilegeToDelete}
          onEdit={handleEdit}
          onCopy={handleCopy}
          onDelete={onDelete}
        />
      </CommonGrid>
      <ModalDialog
        width="500px"
        isOpen={confirmActionDialog.isOpen}
        title={confirmActionDialog.title}
        customComponent={confirmActionDialog.customComponent}
        actionText={confirmActionDialog.actionText}
        onSubmit={confirmActionDialog.onSubmit}
        onClose={confirmActionDialog.onClose}
        doNotDisplayCancelButton={confirmActionDialog.hideCancel}
      />
    </div>
  );
};

export default CollectionFunctionGrid;
