import React, { FunctionComponent, useState } from 'react';
import { Button } from '@blueprintjs/core';
import { CollectionFunctionMenuActionsType } from 'routes/CollectionFunction/CollectionFunction.types';
import ModalDialog from 'shared/modal/dialog';
import { CollectionFunction } from 'data/collectionFunction/collectionFunction.types';
import './collection-function-list-menu.scss';

type Props = {
  selectedCollectionFunction?: CollectionFunction;
  hasPrivilegeToCreateNew: boolean;
  hasPrivilegeToDelete: boolean;
  onEdit: ()=> void;
  onCopy: ()=> void;
  onDelete: ()=> void;
}

const CollectionFunctionListMenu: FunctionComponent<Props> = ({
  selectedCollectionFunction,
  hasPrivilegeToCreateNew,
  hasPrivilegeToDelete,
  onEdit,
  onCopy,
  onDelete,
}: Props) => {

  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };

  const handleDeleteDelete = () => {
    onDelete();
    setIsDeleteDialogOpen(false);
  };

  if (selectedCollectionFunction == null) {
    return null;
  }

  return (
    <div data-testid="CollectionFunctionListMenu" className="collection-function-list-menu">
      {hasPrivilegeToCreateNew && (
      <Button
        className="collection-function-list-menu-button"
        data-testid={`${CollectionFunctionMenuActionsType.Edit}-button`}
        text={CollectionFunctionMenuActionsType.Edit}
        onClick={onEdit}
      />
      )}
      {hasPrivilegeToCreateNew && (
      <Button
        className="collection-function-list-menu-button"
        data-testid={`${CollectionFunctionMenuActionsType.Copy}-button`}
        text={CollectionFunctionMenuActionsType.Copy}
        onClick={onCopy}
      />
      )}
      {hasPrivilegeToDelete && (
      <Button
        className="collection-function-list-menu-button"
        data-testid={`${CollectionFunctionMenuActionsType.Delete}-button`}
        text={CollectionFunctionMenuActionsType.Delete}
        onClick={handleOpenDeleteDialog}
      />
      )}
      <ModalDialog
        width="500px"
        isOpen={isDeleteDialogOpen}
        title="Delete Collection Function"
        displayMessage={`Are you sure you want to delete '${selectedCollectionFunction.name}'?`}
        actionText="Yes, delete it"
        onSubmit={handleDeleteDelete}
        onClose={handleCloseDeleteDialog}
      />
    </div>

  );
};

export default CollectionFunctionListMenu;
