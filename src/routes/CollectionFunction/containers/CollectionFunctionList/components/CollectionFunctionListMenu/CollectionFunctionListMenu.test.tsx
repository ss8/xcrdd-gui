import React from 'react';
import {
  render, screen, fireEvent,
} from '@testing-library/react';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import CollectionFunctionListMenu from './CollectionFunctionListMenu';

describe('<CollectionFunctionListMenu />', () => {

  describe('call menu actions when user has all privileges ', () => {
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleEdit = jest.fn();

    beforeEach(() => {
      render(<CollectionFunctionListMenu
        selectedCollectionFunction={mockCollectionFunctions[0]}
        hasPrivilegeToCreateNew
        hasPrivilegeToDelete
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);
    });

    it('should call Edit callback', () => {
      fireEvent.click(screen.getByText('edit') as Element);

      expect(handleEdit).toBeCalled();
    });

    it('should call Copy callback', () => {
      fireEvent.click(screen.getByText('copy') as Element);

      expect(handleCopy).toBeCalled();
    });

    it('should cancel Delete operation and not call Delete callback', () => {
      fireEvent.click(screen.getByText('delete') as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();
      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Cancel') as Element);

      expect(handleDelete).not.toBeCalled();
    });

    it('should call Delete callback', () => {
      fireEvent.click(screen.getByText('delete') as Element);

      expect(screen.getByText('Delete Collection Function')).toBeVisible();
      expect(screen.getByText(`Are you sure you want to delete '${mockCollectionFunctions[0].name}'?`)).toBeVisible();

      fireEvent.click(screen.getByText('Yes, delete it') as Element);

      expect(handleDelete).toBeCalled();
    });

  });

  describe('when there is no Collection Function selected ', () => {
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleEdit = jest.fn();

    beforeEach(() => {
      render(<CollectionFunctionListMenu
        hasPrivilegeToCreateNew
        hasPrivilegeToDelete
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);
    });

    it('should not render Menu component call Edit callback', () => {
      expect(document.querySelector('[data-testid="CollectionFunctionListMenu"')).toBeNull();
    });
  });

  describe('when user does not have all privileges', () => {
    const handleCopy = jest.fn();
    const handleDelete = jest.fn();
    const handleEdit = jest.fn();

    it('should render all buttons to all privileges ', () => {
      const { container } = render(<CollectionFunctionListMenu
        selectedCollectionFunction={mockCollectionFunctions[0]}
        hasPrivilegeToCreateNew
        hasPrivilegeToDelete
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);

      const actionsButtons = Array.from(container.querySelectorAll('button')).map((buttonEl) => buttonEl.textContent);

      expect(actionsButtons).toEqual(['edit', 'copy', 'delete']);
    });

    it('should not render Edit and Copy render when hasPrivilegeToCreateNew=false ', () => {
      const { container } = render(<CollectionFunctionListMenu
        selectedCollectionFunction={mockCollectionFunctions[0]}
        hasPrivilegeToCreateNew={false}
        hasPrivilegeToDelete
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);

      const actionsButtons = Array.from(container.querySelectorAll('button')).map((buttonEl) => buttonEl.textContent);

      expect(actionsButtons).toEqual(['delete']);
    });

    it('should not render Delete and copy render when hasPrivilegeToDelete=false ', () => {
      const { container } = render(<CollectionFunctionListMenu
        selectedCollectionFunction={mockCollectionFunctions[0]}
        hasPrivilegeToCreateNew
        hasPrivilegeToDelete={false}
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);

      const actionsButtons = Array.from(container.querySelectorAll('button')).map((buttonEl) => buttonEl.textContent);

      expect(actionsButtons).toEqual(['edit', 'copy']);
    });

    it('should not render any button hasPrivilegeToDelete=false and hasPrivilegeToCreateNew=false ', () => {
      const { container } = render(<CollectionFunctionListMenu
        selectedCollectionFunction={mockCollectionFunctions[0]}
        hasPrivilegeToCreateNew={false}
        hasPrivilegeToDelete={false}
        onEdit={handleEdit}
        onCopy={handleCopy}
        onDelete={handleDelete}
      />);

      const actionsButtons = Array.from(container.querySelectorAll('button')).map((buttonEl) => buttonEl.textContent);

      expect(actionsButtons).toEqual([]);
    });

  });

});
