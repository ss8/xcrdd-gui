import React, { Fragment } from 'react';

const CollectionFunctionMissingTemplateDialogBody = ({ name }: { name: string }): JSX.Element => (
  <Fragment>
    Unable to find Collection Function template.
    <br />
    Please contact system administrator to check Discovery installation environment
    to ensure that the template for CF Handover Protocol &#39;{name}&#39; is there.
  </Fragment>
);

export default CollectionFunctionMissingTemplateDialogBody;
