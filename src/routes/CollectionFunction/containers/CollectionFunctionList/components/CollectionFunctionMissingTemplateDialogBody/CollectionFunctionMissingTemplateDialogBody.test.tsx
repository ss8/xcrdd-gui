import React from 'react';
import { render } from '@testing-library/react';
import CollectionFunctionMissingTemplateDialogBody from './CollectionFunctionMissingTemplateDialogBody';

describe('<CollectionFunctionMissingTemplateDialogBody />', () => {
  it('should render the dialog body', () => {
    const { container } = render(<CollectionFunctionMissingTemplateDialogBody name="NAME" />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        Unable to find Collection Function template.
        <br />
        Please contact system administrator to check Discovery installation environment to ensure that the template for CF Handover Protocol '
        NAME
        ' is there.
      </div>
    `);
  });
});
