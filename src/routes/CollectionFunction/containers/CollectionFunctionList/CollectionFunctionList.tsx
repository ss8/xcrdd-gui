import React, { FunctionComponent, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { SelectionChangedEvent } from '@ag-grid-enterprise/all-modules';
import { RootState } from 'data/root.reducers';
import { CollectionFunctionListData, CollectionFunctionPrivileges } from 'data/collectionFunction/collectionFunction.types';
import { GridSettings, WithAuditDetails } from 'data/types';
import * as selectors from 'data/collectionFunction/collectionFunction.selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as globalSelectors from 'global/global-selectors';
import * as actions from 'data/collectionFunction/collectionFunction.actions';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as ftpInfoActions from 'data/ftpInfo/ftpInfo.actions';
import * as networkActions from 'data/network/network.actions';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as fqdnGroupActions from 'data/fqdnGroup/fqdnGroup.actions';
import * as nodeActions from 'data/nodes/nodes.actions';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as templateSelectors from 'data/template/template.selectors';
import { AppToaster } from 'shared/toaster';
import adaptCollectionFunctionsToGrid from 'routes/CollectionFunction/utils/collectionFunctionGrid.adapter';
import CollectionFunctionGrid from './components/CollectionFunctionGrid';
import { collectionFunctionsColumnDefs } from '../../CollectionFunction.types';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  userName: authenticationSelectors.getAuthenticationUserName(state),
  privileges: authenticationSelectors.getAuthenticationPrivileges(state),
  beforeLogout: authenticationSelectors.getAuthenticationBeforeLogout(state),
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  data: selectors.getCollectionFunctionList(state),
  enabledCollectionFunctions: supportedFeaturesSelectors.getEnabledCollectionFunctions(state),
  selectedCollectionFunction: selectors.getSelectedCollectionFunction(state),
  collectionFunctionTemplates: templateSelectors.getEnabledCollectionFunctionFormTemplatesByKey(state),
});

const mapDispatch = {
  getTimezones: userActions.getTimezones,
  getContacts: contactActions.getAllContacts,
  getSupportedFeature: warrantActions.getWarrantFormConfig,
  getTemplates: warrantActions.getAllTemplates,
  getFtpInfos: ftpInfoActions.getFtpInfos,
  fetchCollectionFunctions: actions.fetchCollectionFunctionListViewOptions,
  fetchCollectionFunctionGroupList: actions.fetchCollectionFunctionGroupList,
  goToCollectionFunctionCreate: actions.goToCollectionFunctionCreate,
  selectCollectionFunction: actions.selectCollectionFunction,
  goToCollectionFunctionEdit: actions.goToCollectionFunctionEdit,
  goToCollectionFunctionView: actions.goToCollectionFunctionView,
  goToCollectionFunctionCopy: actions.goToCollectionFunctionCopy,
  deleteCollectionFunction: actions.deleteCollectionFunction,
  getGridSettings: userActions.getUserSettings,
  saveGridSettings: userActions.putUserSettings,
  getNetworks: networkActions.getNetworks,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getFqdnGroups: fqdnGroupActions.getFqdnGroups,
  getCustomization: warrantActions.getWarrantFormCustomizationConfig,
  getXcipioNodes: nodeActions.getNodes,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>
type ComponentProps = { suppressColumnVirtualisation?: boolean } // this prop is only used for testing purposes.

export type Props = PropsFromRedux & PropsFromRouter & ComponentProps;

const USER_CATEGORY_SETTINGS = 'CollectionFunctionSettings';

export const CollectionFunctionList: FunctionComponent<Props> = ({
  suppressColumnVirtualisation,
  userName,
  privileges,
  deliveryFunctionId,
  data = [],
  beforeLogout,
  history,
  selectedCollectionFunction,
  enabledCollectionFunctions,
  collectionFunctionTemplates,
  getTimezones,
  getContacts,
  getSupportedFeature,
  getTemplates,
  getFtpInfos,
  fetchCollectionFunctions,
  fetchCollectionFunctionGroupList,
  getGridSettings,
  saveGridSettings,
  goToCollectionFunctionCreate,
  selectCollectionFunction,
  goToCollectionFunctionEdit,
  goToCollectionFunctionView,
  goToCollectionFunctionCopy,
  deleteCollectionFunction,
  getNetworks,
  getSecurityInfos,
  getFqdnGroups,
  getCustomization,
  getXcipioNodes,
}: Props) => {

  const [lastUpdatedAt, setLastUpdatedAt] = useState<Date>();
  const [collectionFunctionGridData, setCollectionFunctionGridData] = useState<CollectionFunctionListData[]>([]);

  useEffect(() => {
    getTimezones();
    getContacts(deliveryFunctionId);
    getSupportedFeature(deliveryFunctionId);
    getTemplates();
    getFtpInfos(deliveryFunctionId);
    fetchCollectionFunctions(deliveryFunctionId);
    fetchCollectionFunctionGroupList(deliveryFunctionId);
    getNetworks(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
    getFqdnGroups(deliveryFunctionId);
    getCustomization(deliveryFunctionId);
    getXcipioNodes(deliveryFunctionId);

    return () => {
      AppToaster.clear();
    };
  }, [
    deliveryFunctionId,
    getTimezones,
    getContacts,
    getSupportedFeature,
    getTemplates,
    getFtpInfos,
    fetchCollectionFunctions,
    fetchCollectionFunctionGroupList,
    getNetworks,
    getSecurityInfos,
    getFqdnGroups,
    getCustomization,
    getXcipioNodes,
  ]);

  useEffect(() => {
    setLastUpdatedAt(new Date());

    setCollectionFunctionGridData(
      adaptCollectionFunctionsToGrid(data, enabledCollectionFunctions),
    );
  }, [data, enabledCollectionFunctions]);

  const handleRefresh = () => fetchCollectionFunctions(deliveryFunctionId);

  const handleCreateNew = () => goToCollectionFunctionCreate(history);

  const handleSelectionChanged = (event: SelectionChangedEvent) =>
    selectCollectionFunction(event.api.getSelectedRows());

  const handleGetGridSettings = () => getGridSettings(USER_CATEGORY_SETTINGS, userName);

  const handleSaveGridSettings = (gridSettings: GridSettings) =>
    saveGridSettings(USER_CATEGORY_SETTINGS, userName, gridSettings);

  const handleCollectionFunctionView = () => goToCollectionFunctionView(history);

  const handleCollectionFunctionEdit = () => goToCollectionFunctionEdit(history);

  const handleCollectionFunctionCopy = () => goToCollectionFunctionCopy(history);

  const handleCollectionFunctionDelete = () => {

    const collectionFunction = selectedCollectionFunction[0];

    if (collectionFunction?.id == null) {
      return;
    }

    const collectionFunctionBody: WithAuditDetails<void> = { data: undefined };

    deleteCollectionFunction(deliveryFunctionId, collectionFunction.id, collectionFunctionBody);
  };

  const hasPrivilegesToCreate = privileges.includes(CollectionFunctionPrivileges.Edit);
  const hasPrivilegeToDelete = privileges.includes(CollectionFunctionPrivileges.Delete);
  const hasPrivilegeToView = privileges.includes(CollectionFunctionPrivileges.View);
  const hasPrivilegeToCopy = privileges.includes(CollectionFunctionPrivileges.Edit);
  return (
    <div data-testid="CollectionFunctionList" style={{ height: 'inherit' }}>
      <CollectionFunctionGrid
        suppressColumnVirtualisation={suppressColumnVirtualisation}
        selectedCollectionFunction={selectedCollectionFunction?.[0]}
        data={collectionFunctionGridData}
        columnDefs={collectionFunctionsColumnDefs}
        noResults={false}
        beforeLogout={beforeLogout}
        lastUpdatedAt={lastUpdatedAt}
        hasPrivilegeToCopy={hasPrivilegeToCopy}
        hasPrivilegesToCreate={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegeToView={hasPrivilegeToView}
        enabledCollectionFunctions={enabledCollectionFunctions}
        collectionFunctionTemplates={collectionFunctionTemplates}
        onRefresh={handleRefresh}
        onCreateNew={handleCreateNew}
        onGetGridSettings={handleGetGridSettings}
        onSaveGridSettings={handleSaveGridSettings}
        onSelectionChanged={handleSelectionChanged}
        onView={handleCollectionFunctionView}
        onEdit={handleCollectionFunctionEdit}
        onCopy={handleCollectionFunctionCopy}
        onDelete={handleCollectionFunctionDelete}
      />
    </div>
  );
};

export default withRouter(connector(CollectionFunctionList));
