/* eslint-disable import/prefer-default-export */
import { CollectionFunction, CollectionFunctionListData } from 'data/collectionFunction/collectionFunction.types';
import { SupportedFeaturesCollectionFunctions } from 'data/supportedFeatures/supportedFeatures.types';

// sets cfGroup names to each collection function based on collectionFunctionsGroups
export default function adaptCollectionFunctionsToGrid(
  collectionFunctions: CollectionFunction[],
  enabledCollectionFunctions: SupportedFeaturesCollectionFunctions,
): CollectionFunctionListData[] {

  return collectionFunctions.map((currentCollectionFunction) => {
    if (currentCollectionFunction.tag === 'CF-ATIS-0700005') {
      currentCollectionFunction.tag = 'CF-ATIS-1000678';
    }

    return {
      ...currentCollectionFunction,
      tagName: enabledCollectionFunctions[currentCollectionFunction.tag]?.title || '',
    };
  });
}
