import lodashTemplate from 'lodash.template';
import { CollectionFunctionFormData, CollectionFunctionGroup } from 'data/collectionFunction/collectionFunction.types';
import { CustomizationCollectionFunctionName } from 'data/customization/customization.types';

const TEMPLATE_SETTINGS = {
  interpolate: /\{\{(.+?)\}\}/g,
};

// eslint-disable-next-line import/prefer-default-export
export function handleCollectionFunctionNameUpdate(
  collectionFunctionNameCustomization: CustomizationCollectionFunctionName | undefined,
  collectionFunctionGroupList: CollectionFunctionGroup[],
  data: Partial<CollectionFunctionFormData>,
  fieldName: keyof CollectionFunctionFormData,
  fieldValue: string,
): string {
  const {
    name = '',
    tag = '',
    cfGroup = '',
  } = data;

  const {
    template = '',
  } = collectionFunctionNameCustomization || {};

  const cfGroupName = getCollectionGroupName(collectionFunctionGroupList, cfGroup, fieldName, fieldValue);
  const tagName = getCollectionFunctionTagName(tag, fieldName, fieldValue);

  if (cfGroupName == null || collectionFunctionNameCustomization == null || template === '') {
    return name ?? '';
  }

  const compiledNameTemplate = lodashTemplate(template, TEMPLATE_SETTINGS);
  const nameTemplateData = getTemplateData(
    collectionFunctionNameCustomization,
    cfGroupName,
    tagName,
  );

  return compiledNameTemplate(nameTemplateData);
}

function getCollectionGroupName(
  collectionFunctionGroupList: CollectionFunctionGroup[],
  cfGroup: string,
  fieldName: keyof CollectionFunctionFormData,
  fieldValue: string,
): string | undefined {
  let cfGroupId = cfGroup;

  if (fieldName === 'cfGroup') {
    cfGroupId = fieldValue;
  }

  const collectionFunctionGroup = collectionFunctionGroupList.find(({ id }) => id === cfGroupId);
  return collectionFunctionGroup?.name;
}

function getCollectionFunctionTagName(
  tag: string,
  fieldName: keyof CollectionFunctionFormData,
  fieldValue: string,
): string {
  if (fieldName === 'tag') {
    return fieldValue;
  }

  return tag;
}

function getTemplateData(
  collectionFunctionNameCustomization: CustomizationCollectionFunctionName,
  cfGroup: string,
  tag: string,
): { [key: string]: string } {
  const templateData: { [key: string]: string } = {};

  templateData.cfGroup = cfGroup;
  templateData.tag = tag;
  templateData.tagMap = collectionFunctionNameCustomization.tagMap?.[tag] ?? tag;

  return templateData;
}
