import cloneDeep from 'lodash.clonedeep';
import { CollectionFunctionInterfaceFormData, CollectionFunctionCountryCode } from 'data/collectionFunction/collectionFunction.types';
import { FtpInfo } from 'data/ftpInfo/ftpInfo.types';

// eslint-disable-next-line import/prefer-default-export
export function handleInterfacesFieldUpdate(
  interfaceFormDataList: CollectionFunctionInterfaceFormData[] | undefined,
  interfaceId: string | undefined,
  fieldName: keyof CollectionFunctionInterfaceFormData,
  value: unknown,
  ftpInfos: FtpInfo[],
): CollectionFunctionInterfaceFormData[] | undefined {
  const clonedInterfaceFormDataList = cloneDeep(interfaceFormDataList);

  return clonedInterfaceFormDataList?.map((interfaceFormData) => {
    if (interfaceFormData.id === interfaceId) {
      return handleInterfaceFieldUpdate(interfaceFormData, fieldName, value, ftpInfos);
    }

    return interfaceFormData;
  });
}

function handleInterfaceFieldUpdate(
  interfaceFormData: CollectionFunctionInterfaceFormData,
  fieldName: keyof CollectionFunctionInterfaceFormData,
  fieldValue: unknown,
  ftpInfos: FtpInfo[],
) {
  setInterfaceFieldValue(interfaceFormData, fieldName, fieldValue);

  if (fieldName === 'ftpInfoId') {
    interfaceFormData = setInterfaceValuesForSelectedFtpInfoId(
      interfaceFormData,
      fieldValue as string,
      ftpInfos,
    );
  }

  if (fieldName === 'version') {
    interfaceFormData = setInterfaceValuesForSelectedVersion(
      interfaceFormData,
    );
  }

  if (fieldName === 'countryCode') {
    interfaceFormData = setInterfaceValuesForSelectedCountry(
      interfaceFormData,
      fieldValue as CollectionFunctionCountryCode,
    );
  }

  return interfaceFormData;
}

function setInterfaceFieldValue(
  interfaceFormData: any,
  fieldName: string,
  value: unknown,
): CollectionFunctionInterfaceFormData {
  interfaceFormData[fieldName] = value;

  return interfaceFormData;
}

function setInterfaceValuesForSelectedFtpInfoId(
  interfaceFormData: CollectionFunctionInterfaceFormData,
  ftpInfoId: string,
  ftpInfos: FtpInfo[],
): CollectionFunctionInterfaceFormData {
  const selectedFtpInfo = ftpInfos.find(({ id }) => id === ftpInfoId);

  interfaceFormData.destIPAddr = selectedFtpInfo?.ipAddress ?? '';
  interfaceFormData.destPort = selectedFtpInfo?.port ?? '';
  interfaceFormData.userName = selectedFtpInfo?.username ?? '';
  interfaceFormData.userPassword = selectedFtpInfo?.userPassword ?? '';
  interfaceFormData.ftpFileSec = selectedFtpInfo?.ftpFileTransferTimeout ?? '';
  interfaceFormData.ftpFileSize = selectedFtpInfo?.ftpFileSize ?? '';
  interfaceFormData.operatorId = selectedFtpInfo?.operatorId ?? '';
  interfaceFormData.fileMode = selectedFtpInfo?.fileMode ?? 'ALLINONE';
  interfaceFormData.destPath = selectedFtpInfo?.destPath ?? '';
  interfaceFormData.ftpType = selectedFtpInfo?.ftpType ?? 'FTP';

  return interfaceFormData;
}

function setInterfaceValuesForSelectedVersion(
  interfaceFormData: CollectionFunctionInterfaceFormData,
): CollectionFunctionInterfaceFormData {

  if (interfaceFormData.countryCode != null) {
    interfaceFormData.countryCode = 'NONE';

    setInterfaceValuesForSelectedCountry(interfaceFormData, 'NONE');
  }

  return interfaceFormData;
}

function setInterfaceValuesForSelectedCountry(
  interfaceFormData: CollectionFunctionInterfaceFormData,
  fieldValue: CollectionFunctionCountryCode,
): CollectionFunctionInterfaceFormData {

  if (interfaceFormData.pldEncType == null || interfaceFormData.pldEncKey == null) {
    return interfaceFormData;
  }

  const interfaceVersion = interfaceFormData.version;

  if ((fieldValue === 'TR' && interfaceVersion !== '2.5.1') || fieldValue === 'NL') {
    interfaceFormData.pldEncType = true;

  } else {
    interfaceFormData.pldEncType = false;
    interfaceFormData.pldEncKey = '';

    if (interfaceFormData.secinfoid != null) {
      interfaceFormData.secinfoid = '';
    }
  }

  return interfaceFormData;
}
