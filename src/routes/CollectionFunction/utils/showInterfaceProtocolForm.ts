import { FormTemplateSection } from 'data/template/template.types';

export default function showInterfaceProtocolForm(templateFormSection:FormTemplateSection): boolean {

  if (templateFormSection == null) {
    return false;
  }

  return Object.values(templateFormSection.fields).some((field) => field.hide !== 'true');
}
