import { CollectionFunction, CollectionFunctionFormData } from 'data/collectionFunction/collectionFunction.types';
import { Operation, WithAuditDetails, AuditDetails } from 'data/types';
import { adaptToSubmit, setCollectionFunctionOperationValues } from 'data/collectionFunction/collectionFunction.adapter';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';

export default function getCollectionFunctionDataToSubmit(
  collectionFunctionFormData: CollectionFunctionFormData,
  originalCollectionFunction: CollectionFunction | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
  operation: Operation,
): WithAuditDetails<CollectionFunction> {
  const adaptedFormData = adaptToSubmit(collectionFunctionFormData);
  const dataToSubmit = setCollectionFunctionOperationValues(adaptedFormData, originalCollectionFunction, operation);

  const accessFunctionBody: WithAuditDetails<CollectionFunction> = {
    data: dataToSubmit,
  };

  let actionType = AuditActionType.COLLECTION_FUNCTION_ADD;
  let isEdit = false;
  if (operation === 'UPDATE') {
    actionType = AuditActionType.COLLECTION_FUNCTION_EDIT;
    isEdit = true;
  }

  if (isAuditEnabled) {
    const auditDetails = getAuditDetails(
      isAuditEnabled,
      userId,
      userName,
      getAuditFieldInfo(adaptedFormData, null, isEdit, originalCollectionFunction),
      adaptedFormData.name,
      AuditService.CF,
      actionType,
    );
    accessFunctionBody.auditDetails = auditDetails as AuditDetails;
  }

  return accessFunctionBody;
}
