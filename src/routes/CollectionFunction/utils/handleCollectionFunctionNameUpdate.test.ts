import { CollectionFunctionFormData } from 'data/collectionFunction/collectionFunction.types';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockCustomization } from '__test__/mockCustomization';
import { handleCollectionFunctionNameUpdate } from './handleCollectionFunctionNameUpdate';

describe('handleCollectionFunctionNameUpdate()', () => {
  let collectionFunction: Partial<CollectionFunctionFormData>;

  beforeEach(() => {
    collectionFunction = {
      name: 'name1',
      cfGroup: 'id1',
      tag: 'CF-3GPP-33108',
    };
  });

  it('should return the name as is if customization is not defined', () => {
    expect(handleCollectionFunctionNameUpdate(
      undefined,
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      '',
    )).toEqual('name1');
  });

  it('should return the name as is if template is not defined', () => {
    expect(handleCollectionFunctionNameUpdate(
      {},
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      'id2',
    )).toEqual('name1');
  });

  it('should return the name as is if template is empty', () => {
    expect(handleCollectionFunctionNameUpdate(
      {
        template: '',
      },
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      'id2',
    )).toEqual('name1');
  });

  it('should return the name as empty string if customization is not defined and name is undefined', () => {
    collectionFunction.name = undefined;
    expect(handleCollectionFunctionNameUpdate(
      undefined,
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      '',
    )).toEqual('');
  });

  it('should return the name as is if cfGroup is not defined', () => {
    expect(handleCollectionFunctionNameUpdate(
      mockCustomization.collectionFunctionName,
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      '',
    )).toEqual('name1');
  });

  it('should return the name based in the cfGroup and mapped tag', () => {
    collectionFunction.cfGroup = mockCollectionFunctionGroups[0].id;
    expect(handleCollectionFunctionNameUpdate(
      mockCustomization.collectionFunctionName,
      mockCollectionFunctionGroups,
      collectionFunction,
      'type',
      '',
    )).toEqual('CA-DOJBNE-33108');
  });

  it('should return the name based in the new cfGroup passed as fieldName and mapped tag', () => {
    collectionFunction.cfGroup = mockCollectionFunctionGroups[0].id;
    expect(handleCollectionFunctionNameUpdate(
      mockCustomization.collectionFunctionName,
      mockCollectionFunctionGroups,
      collectionFunction,
      'cfGroup',
      mockCollectionFunctionGroups[1].id,
    )).toEqual('CookCounty-33108');
  });

  it('should return the name based in the cfGroup and the new mapped tag passed as fieldName', () => {
    collectionFunction.cfGroup = mockCollectionFunctionGroups[0].id;
    expect(handleCollectionFunctionNameUpdate(
      mockCustomization.collectionFunctionName,
      mockCollectionFunctionGroups,
      collectionFunction,
      'tag',
      'CF-ETSI-102232',
    )).toEqual('CA-DOJBNE-ETSI');
  });

  it('should return the name based in cfGroup and tag if not mapped in the customization', () => {
    collectionFunction.cfGroup = mockCollectionFunctionGroups[0].id;
    expect(handleCollectionFunctionNameUpdate(
      mockCustomization.collectionFunctionName,
      mockCollectionFunctionGroups,
      collectionFunction,
      'tag',
      'CF-ATIS-0700005',
    )).toEqual('CA-DOJBNE-CF-ATIS-0700005');
  });
});
