import cloneDeep from 'lodash.clonedeep';
import {
  FormTemplate,
  FormTemplateFieldMap,
  FormTemplateOption,
  FormTemplateSection,
} from 'data/template/template.types';
import {
  Contact, ValueLabelPair,
} from 'data/types';
import { getSortedTimeZoneValueLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { SupportedFeaturesCollectionFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import {
  CollectionFunctionFormData,
  CollectionFunctionGroup,
  CollectionFunctionInterfaceFormData,
  CollectionFunctionCountryCode,
  CollectionFunctionDestinationType,
  CollectionFunctionIntegrityCheck,
  COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID,
} from 'data/collectionFunction/collectionFunction.types';
import { FtpInfo } from 'data/ftpInfo/ftpInfo.types';
import { Network } from 'data/network/network.types';
import ipRegex from 'ip-regex';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FqdnGroup } from 'data/fqdnGroup/fqdnGroup.types';
import { NodeData } from 'data/nodes/nodes.types';

export function adaptTemplateOptions(
  templates: {[key: string]: FormTemplate},
  timezones: ValueLabelPair[],
  contacts: Contact[],
  collectionFunctionGroupList: CollectionFunctionGroup[],
  ftpInfos: FtpInfo[],
  networks: Network[],
  securityInfos: SecurityInfo[],
  fqdnGroups: FqdnGroup[],
  enabledCollectionFunctions: SupportedFeaturesCollectionFunctions,
  xcipioNodes: NodeData[],
): { [key: string]: FormTemplate } {
  const adaptedTemplates: { [key: string]: FormTemplate } = {};

  const tagOptions = getTagOptions(templates, enabledCollectionFunctions);
  const timezoneOptions = getTimeZoneFieldOptions(timezones);
  const contactOptions = getFieldOptions(contacts);
  const collectionFunctionGroupOptions = getFieldOptions(collectionFunctionGroupList);
  const ftpInfoOptions = getFtpInfoOptions(ftpInfos);
  const networkOptions = getNetworkOptions(networks);
  const securityInfoOptions = getFieldOptions(securityInfos);
  const fqdnGroupsOptions = getFqdnGroupOptions(fqdnGroups);
  const mdf2Options = getMdfOptions(xcipioNodes, 'MDF2');
  const mdf3Options = getMdfOptions(xcipioNodes, 'MDF3');

  Object.keys(templates).forEach((templateName) => {
    const template = cloneDeep(templates[templateName]);

    if (enabledCollectionFunctions[template.key] == null) {
      return;
    }

    setFieldOptions(template.main as FormTemplateSection, 'tag', tagOptions);
    setFieldOptions(template.facilityInformation as FormTemplateSection, 'contact', contactOptions);
    setFieldOptions(template.main as FormTemplateSection, 'cfGroup', collectionFunctionGroupOptions);
    setTimezoneOptions(template, timezoneOptions);

    if (template.key === 'CF-3GPP-33108') {
      setFieldOptions(template.hi3Interfaces_FTP as FormTemplateSection, 'ftpInfoId', ftpInfoOptions);
    }

    if (template.key === 'CF-JSTD025B') {
      setFieldOptions(template.hi3Interfaces_TCP as FormTemplateSection, 'ownnetid', networkOptions);
    }

    if (template.key === 'CF-ETSI-102232') {
      setFieldOptions(template.hi2Interfaces_TCP as FormTemplateSection, 'ownnetid', networkOptions);
      setFieldOptions(template.hi2Interfaces_TCP as FormTemplateSection, 'secinfoid', securityInfoOptions);

      setFieldOptions(template.hi3Interfaces_TCP as FormTemplateSection, 'ownnetid', networkOptions);
      setFieldOptions(template.hi3Interfaces_TCP as FormTemplateSection, 'secinfoid', securityInfoOptions);
      setFieldOptions(template.hi3Interfaces_TCP as FormTemplateSection, 'fsid', fqdnGroupsOptions);

      setFieldOptions(template.hi3Interfaces_FTP as FormTemplateSection, 'ftpInfoId', ftpInfoOptions);

      setFieldOptions(template.hi2Interfaces as FormTemplateSection, 'mdf2NodeName', mdf2Options);
      setFieldOptions(template.hi3Interfaces as FormTemplateSection, 'mdf3NodeName', mdf3Options);
    }

    adaptedTemplates[template.key] = template;
  });

  return adaptedTemplates;
}

export function adaptTemplateVisibilityMode(
  template: FormTemplate | undefined,
  mode: 'create' | 'edit' | 'view' | 'copy',
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  let clonedTemplate = cloneDeep(template);

  if (mode === 'view') {
    clonedTemplate = adaptTemplateForViewMode(clonedTemplate);

  } else if (mode === 'edit') {
    clonedTemplate = adaptTemplateForEditMode(clonedTemplate);
  }

  return clonedTemplate;
}

export function adaptTemplateName(
  template: FormTemplate | undefined,
  name: string,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.key = Date.now().toString();

  const templateSection = (clonedTemplate.main as FormTemplateSection);

  templateSection.fields.name.initial = name;

  return clonedTemplate;
}

export function adaptTemplateTimezoneOptions(
  template: FormTemplate | undefined,
  formData: CollectionFunctionFormData,
  timezones: ValueLabelPair[],
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const templateTimezoneOptions = (template.facilityInformation as FormTemplateSection).fields.timeZone.options;

  const isTimezoneAvailable = templateTimezoneOptions?.some(({ value }) => value === formData.timeZone) ?? false;
  if (isTimezoneAvailable) {
    return template;
  }

  timezones.push({
    label: formData.timeZone,
    value: formData.timeZone,
  });
  const timezoneOptions = getTimeZoneFieldOptions(timezones);

  const clonedTemplate = cloneDeep(template);

  const templateSection = (clonedTemplate.facilityInformation as FormTemplateSection);

  const firstOption = templateSection.fields.timeZone.options?.[0];
  if (firstOption) {
    templateSection.fields.timeZone.options = [firstOption];
  }
  templateSection.fields.timeZone.options?.push(...timezoneOptions);

  return clonedTemplate;
}

export function adaptTemplateInterfaceFields(
  template: FormTemplate | undefined,
  formData: CollectionFunctionFormData,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);

  formData.hi2Interfaces?.forEach((interfaceFormData) => {
    if (formData.tag === 'CF-ETSI-102232') {
      if (interfaceFormData.icheck != null) {
        adaptTemplateInterfaceForIntegrityCheck(
          'hi2Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.icheck,
        );
      }

      if (interfaceFormData.version != null) {
        adaptTemplateInterfaceForVersion(
          'hi2Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.version,
        );
      }

      if (interfaceFormData.countryCode != null) {
        adaptTemplateInterfaceForCountryCode(
          'hi2Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.countryCode,
        );
      }
    }
  });

  formData.hi3Interfaces?.forEach((interfaceFormData) => {
    if (formData.tag === 'CF-ETSI-102232') {
      if (interfaceFormData.icheck != null) {
        adaptTemplateInterfaceForIntegrityCheck(
          'hi3Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.icheck,
        );
      }

      if (interfaceFormData.version != null) {
        adaptTemplateInterfaceForVersion(
          'hi3Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.version,
        );
      }

      if (interfaceFormData.countryCode != null) {
        adaptTemplateInterfaceForCountryCode(
          'hi3Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.countryCode,
        );
      }

      if (interfaceFormData.destType != null) {
        adaptTemplateInterfaceForDestinationType(
          'hi3Interfaces',
          formData,
          clonedTemplate,
          interfaceFormData.destType,
        );
      }
    }
  });

  return clonedTemplate;
}

export function adaptTemplateInterfaceTransportType(
  template: FormTemplate | undefined,
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  transportType: 'TCP' | 'FTP' | 'SFTP',
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.key = Date.now().toString();

  (clonedTemplate[interfaceName] as FormTemplateSection).fields.transportType.initial = transportType;

  return clonedTemplate;
}

export function adaptTemplateForInterfacesFieldUpdate(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate | undefined,
  fieldName: keyof CollectionFunctionInterfaceFormData,
  fieldValue: unknown,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  let clonedTemplate = cloneDeep(template);

  if (data.tag === 'CF-3GPP-33108' || data.tag === 'CF-ETSI-102232') {
    if (fieldName === 'ftpInfoId') {
      clonedTemplate.key = Date.now().toString();
    }
  }

  if (data.tag === 'CF-ETSI-102232') {
    if (fieldName === 'icheck') {
      clonedTemplate.key = Date.now().toString();
      clonedTemplate = adaptTemplateInterfaceForIntegrityCheck(
        interfaceName,
        data,
        clonedTemplate,
        fieldValue as CollectionFunctionIntegrityCheck,
      );
    }

    if (fieldName === 'version') {
      clonedTemplate.key = Date.now().toString();
      clonedTemplate = adaptTemplateInterfaceForVersion(
        interfaceName,
        data,
        clonedTemplate,
        fieldValue as string,
      );

      clonedTemplate = adaptTemplateInterfaceForCountryCode(
        interfaceName,
        data,
        clonedTemplate,
        fieldValue as CollectionFunctionCountryCode,
      );
    }

    if (fieldName === 'countryCode') {
      clonedTemplate.key = Date.now().toString();
      clonedTemplate = adaptTemplateInterfaceForCountryCode(
        interfaceName,
        data,
        clonedTemplate,
        fieldValue as CollectionFunctionCountryCode,
      );
    }

    if (fieldName === 'destType') {
      clonedTemplate.key = Date.now().toString();
      clonedTemplate = adaptTemplateInterfaceForDestinationType(
        interfaceName,
        data,
        clonedTemplate,
        fieldValue as CollectionFunctionDestinationType,
      );
    }
  }

  return clonedTemplate;
}

export function getInterfaceFormTemplateSection(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
): FormTemplateSection | undefined {
  const interfaceTransportType = getInterfaceTransportType(interfaceName, data, template);

  if (interfaceTransportType == null) {
    return;
  }

  const interfaceFormTemplateSectionName = getInterfaceFormTemplateSectionName(interfaceName, interfaceTransportType);
  return template[interfaceFormTemplateSectionName] as FormTemplateSection;
}

export function getInterfaceFormTemplateSectionName(interfaceName: 'hi2Interfaces' | 'hi3Interfaces', interfaceTransportType: string): string {
  return `${interfaceName}_${interfaceTransportType}`;
}

export function adaptInterfaceTemplateState(
  interfaceFields: FormTemplateFieldMap,
  interfaceData:Partial<CollectionFunctionInterfaceFormData>,
  isEditing: boolean,
):FormTemplateFieldMap {

  const clonedInterfaceFields = cloneDeep(interfaceFields);

  if (
    isEditing &&
    clonedInterfaceFields.state != null &&
    interfaceData.id?.includes(COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID)
  ) {
    clonedInterfaceFields.state.hide = 'true';
  }

  return clonedInterfaceFields;
}

function getFieldOptions(
  fieldList: { name: string, id: string }[],
): FormTemplateOption[] {
  return fieldList.map((field) => ({
    label: field.name,
    value: field.id,
  }));
}

function getFtpInfoOptions(
  fieldList: FtpInfo[],
): FormTemplateOption[] {
  return fieldList.map((field) => ({
    label: field.ftpId,
    value: field.id,
  }));
}

function getNetworkOptions(fieldList: Network[]): FormTemplateOption[] {
  return fieldList.map((field) => {
    let label = `${field.networkId} - ${field.ownIPAddress}:${field.startPortNumber}`;
    const value = field.id;

    if (ipRegex.v6({ exact: true }).test(field.ownIPAddress)) {
      label = `${field.networkId} - [${field.ownIPAddress}]:${field.startPortNumber}`;
    }

    return {
      label,
      value,
    };
  });
}

function getFqdnGroupOptions(
  fieldList: FqdnGroup[],
): FormTemplateOption[] {
  return fieldList.map((field) => ({
    label: field.groupName,
    value: field.id,
  }));
}

function getMdfOptions(
  fieldList: NodeData[],
  nodeType: string,
): FormTemplateOption[] {
  return fieldList?.filter((field) => field.nodeType === nodeType).map((field) => ({
    label: field.nodeName,
    value: field.id,
  }));
}

function setFieldOptions(templateSection: FormTemplateSection, field: string, options: FormTemplateOption[]): void {
  templateSection.fields[field]?.options?.push(...options);
}

function getTagOptions(
  templates: {[key: string]: FormTemplate},
  enabledCollectionFunctions: SupportedFeaturesCollectionFunctions,
): FormTemplateOption[] {
  return Object.keys(templates).map((key) => ({
    label: enabledCollectionFunctions[key]?.title ?? templates[key].name,
    value: templates[key].key,
  }));
}

function getTimeZoneFieldOptions(timezones: ValueLabelPair[]): FormTemplateOption[] {
  const date = new Date();
  return getSortedTimeZoneValueLabels(date, timezones);
}

function setTimezoneOptions(template: FormTemplate, options: FormTemplateOption[]): void {
  if (options.length > 0) {
    (template.facilityInformation as FormTemplateSection).fields.timeZone.type = 'select';
    (template.facilityInformation as FormTemplateSection).fields.timeZone.options?.push(...options);

    if (options.length === 1) {
      (template.facilityInformation as FormTemplateSection).fields.timeZone.initial = options[0].value;
    }
  }
}

function adaptTemplateForViewMode(template: FormTemplate) {
  Object.entries(template).forEach(([key, templateSection]) => {
    const { fields } = (templateSection as FormTemplateSection);
    if (fields != null) {
      Object.values(fields).forEach((field) => {
        field.readOnly = true;
      });
    }
  });

  (template.hi2Interfaces_TCP as FormTemplateSection).fields.state.hide = 'false';

  if (template.key === 'CF-ETSI-102232') {
    (template.hi2Interfaces_FTP as FormTemplateSection).fields.state.hide = 'false';
    (template.hi2Interfaces_SFTP as FormTemplateSection).fields.state.hide = 'false';
  }

  return template;
}

function adaptTemplateForEditMode(template: FormTemplate) {
  (template.main as FormTemplateSection).fields.tag.readOnly = true;
  (template.main as FormTemplateSection).fields.name.readOnly = true;

  (template.hi2Interfaces_TCP as FormTemplateSection).fields.state.hide = 'false';

  if (template.key === 'CF-ETSI-102232') {
    (template.hi2Interfaces_FTP as FormTemplateSection).fields.state.hide = 'false';
    (template.hi2Interfaces_SFTP as FormTemplateSection).fields.state.hide = 'false';
  }

  return template;
}

function adaptTemplateInterfaceForIntegrityCheck(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
  fieldValue: CollectionFunctionIntegrityCheck,
): FormTemplate {
  const interfaceFormTemplateSection = getInterfaceFormTemplateSection(interfaceName, data, template);

  if (interfaceFormTemplateSection == null) {
    return template;
  }

  const {
    hashNum,
    hashSec,
    hashCert,
  } = interfaceFormTemplateSection.fields;

  if (hashNum == null || hashSec == null || hashCert == null) {
    return template;
  }

  if (fieldValue === 'OFF') {
    hashNum.disabled = true;
    hashSec.disabled = true;
    hashCert.disabled = true;

  } else if (fieldValue === 'HASH1WAY' || fieldValue === 'SIGNEDHASH' || fieldValue === 'HASH2WAY') {
    hashNum.disabled = false;
    hashSec.disabled = false;
    hashCert.disabled = false;
  }

  return template;
}

function adaptTemplateInterfaceForVersion(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
  fieldValue: string,
): FormTemplate {
  const interfaceFormTemplateSection = getInterfaceFormTemplateSection(interfaceName, data, template);

  if (interfaceFormTemplateSection == null) {
    return template;
  }

  const {
    countryCode,
    useHi3,
  } = interfaceFormTemplateSection.fields;

  if (countryCode == null) {
    return template;
  }

  if (fieldValue === '1.4.1') {
    countryCode.disabled = true;
    countryCode.options = countryCode.allOptions?.filter(({ value }) => value === 'NONE') ?? [];

  } else if (fieldValue === '2.2.1') {
    countryCode.disabled = false;
    countryCode.options = countryCode.allOptions?.filter(({ value }) => value === 'NONE' || value === 'IE') ?? [];

  } else if (fieldValue === '2.5.1') {
    countryCode.disabled = false;
    countryCode.options = countryCode.allOptions?.filter(({ value }) => value === 'NONE' || value === 'TR') ?? [];

  } else if (((fieldValue === '3.17.1') || (fieldValue === '3.21.1')) && !isTransportTypeTCP(interfaceName, data, template)) {
    countryCode.disabled = true;
    countryCode.options = countryCode.allOptions?.filter(({ value }) => value === 'NONE') ?? [];

  } else {
    countryCode.disabled = false;
    countryCode.options = countryCode.allOptions?.filter(({ value }) => value !== 'IE') ?? [];
  }

  if (useHi3 == null) {
    return template;
  }

  if (fieldValue === '1.4.1' || fieldValue === '2.2.1' || fieldValue === '2.5.1') {
    useHi3.disabled = true;

  } else {
    useHi3.disabled = false;
  }

  return template;
}

function adaptTemplateInterfaceForCountryCode(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
  fieldValue: CollectionFunctionCountryCode,
): FormTemplate {
  const interfaceFormTemplateSection = getInterfaceFormTemplateSection(interfaceName, data, template);

  if (interfaceFormTemplateSection == null) {
    return template;
  }

  const {
    pldEncKey,
    secinfoid,
  } = interfaceFormTemplateSection.fields;

  if (pldEncKey == null) {
    return template;
  }

  const interfaceVersion = getInterfaceVersion(interfaceName, data, template);

  if ((fieldValue === 'TR' && interfaceVersion !== '2.5.1') || fieldValue === 'NL') {
    pldEncKey.disabled = false;

    if (secinfoid != null) {
      secinfoid.disabled = false;

      if (secinfoid.validation != null) {
        secinfoid.validation.required = true;
      }
    }

  } else {
    pldEncKey.disabled = true;

    if (secinfoid != null) {
      secinfoid.disabled = true;

      if (secinfoid.validation) {
        secinfoid.validation.required = false;
      }
    }
  }

  return template;
}

function adaptTemplateInterfaceForDestinationType(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
  fieldValue: CollectionFunctionDestinationType,
): FormTemplate {

  const interfaceFormTemplateSection = getInterfaceFormTemplateSection(interfaceName, data, template);

  if (interfaceFormTemplateSection == null) {
    return template;
  }

  const {
    destIPAddr,
    fsid,
  } = interfaceFormTemplateSection.fields;

  if (destIPAddr == null || fsid == null) {
    return template;
  }

  if (fieldValue === 'domainGroup') {
    destIPAddr.hide = 'true';
    fsid.hide = 'false';

  } else {
    destIPAddr.hide = 'false';
    fsid.hide = 'true';
  }

  return template;
}

function getInterfaceTransportType(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
): string | undefined {
  const interfaceTemplateSection = (template[interfaceName] as FormTemplateSection);

  if (interfaceTemplateSection == null) {
    return;
  }

  const defaultValue = interfaceTemplateSection.fields.transportType.initial as string;
  return data[interfaceName]?.[0]?.transportType ?? defaultValue;
}

function isTransportTypeTCP(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
): boolean {
  return getInterfaceTransportType(interfaceName, data, template) === 'TCP';
}

function getInterfaceVersion(
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  data: Partial<CollectionFunctionFormData>,
  template: FormTemplate,
): string | undefined {
  const interfaceTransportType = getInterfaceTransportType(interfaceName, data, template);

  if (interfaceTransportType == null) {
    return;
  }

  const interfaceFormTemplateSectionName = getInterfaceFormTemplateSectionName(interfaceName, interfaceTransportType);
  const interfaceTemplateSection = (template[interfaceFormTemplateSectionName] as FormTemplateSection);

  if (interfaceTemplateSection == null) {
    return;
  }

  if (interfaceName === 'hi2Interfaces') {
    const defaultValue = interfaceTemplateSection.fields.version.initial as string;
    return data[interfaceName]?.[0]?.version ?? defaultValue;
  }

  const defaultValue = interfaceTemplateSection.fields.version.initial as string;
  return data[interfaceName]?.[0]?.version ?? defaultValue;
}

export function adaptTemplate5GFields(
  template: FormTemplate | undefined,
  is5GCVcpEnabled: boolean,
): FormTemplate | undefined {
  if (is5GCVcpEnabled || template == null) return template;

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  const hi2InterfaceTemplateSection = (clonedTemplate.hi2Interfaces as FormTemplateSection);
  const hi3InterfaceTemplateSection = (clonedTemplate.hi3Interfaces as FormTemplateSection);

  const mdf2NodeNameField = hi2InterfaceTemplateSection?.fields?.mdf2NodeName;

  if (mdf2NodeNameField != null) {
    mdf2NodeNameField.hide = 'true';

    if (hi2InterfaceTemplateSection.metadata.layout !== undefined) {
      const updatedRows = hi2InterfaceTemplateSection.metadata.layout?.rows.map((row) => {
        const index = row.findIndex((field) => field === 'mdf2NodeName');
        if (index !== -1) {
          row.splice(index, 1);
          row.push('$empty');
        }
        return row;
      });
      hi2InterfaceTemplateSection.metadata.layout.rows = updatedRows;
    }
  }

  const mdf3NodeNameField = hi3InterfaceTemplateSection?.fields?.mdf3NodeName;
  if (mdf3NodeNameField != null) {
    mdf3NodeNameField.hide = 'true';
    if (hi3InterfaceTemplateSection.metadata.layout !== undefined) {
      const updatedRows = hi3InterfaceTemplateSection.metadata.layout?.rows.map((row) => {
        const index = row.findIndex((field) => field === 'mdf3NodeName');
        if (index !== -1) {
          row.splice(index, 1);
          row.push('$empty');
        }
        return row;
      });
      hi3InterfaceTemplateSection.metadata.layout.rows = updatedRows;
    }
  }

  return clonedTemplate;
}
