import {
  FormTemplateSection,
  FormTemplate,
  FormTemplateOption,
  FormTemplateFieldMap,
} from 'data/template/template.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockContacts } from '__test__/mockContacts';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { CollectionFunctionFormData, CollectionFunctionInterfaceFormData, COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID } from 'data/collectionFunction/collectionFunction.types';
import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { adaptToForm } from 'data/collectionFunction/collectionFunction.adapter';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockFqdnGroups } from '__test__/mockFqdnGroups';
import { mockNodes } from '__test__/mockNodes';
import {
  adaptInterfaceTemplateState,
  adaptTemplateForInterfacesFieldUpdate,
  adaptTemplateInterfaceTransportType,
  adaptTemplateName,
  adaptTemplateOptions,
  adaptTemplateTimezoneOptions,
  adaptTemplateVisibilityMode,
  getInterfaceFormTemplateSection,
  getInterfaceFormTemplateSectionName,
} from './collectionFunctionTemplate.adapter';
import { createCollectionFunctionBasedOnTemplate } from './collectionFunctionTemplate.factory';

describe('adaptTemplateOptions()', () => {
  let collectionFunctionTemplates: {[key: string]: FormTemplate};

  beforeEach(() => {
    collectionFunctionTemplates = {
      'CF-3GPP-33108': getMockTemplateByKey('CF-3GPP-33108'),
      'CF-ETSI-102232': getMockTemplateByKey('CF-ETSI-102232'),
      'CF-TIA-1072': getMockTemplateByKey('CF-TIA-1072'),
      'CF-TIA-1066': getMockTemplateByKey('CF-TIA-1066'),
      'CF-JSTD025A': getMockTemplateByKey('CF-JSTD025A'),
      'CF-JSTD025B': getMockTemplateByKey('CF-JSTD025B'),
      'CF-ATIS-1000678': getMockTemplateByKey('CF-ATIS-1000678'),
    };
  });

  describe('adaptTemplateOptions()', () => {
    it('should set the tag based in the templates', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'US 3GPP LI (TS 33.108) over TCP or FTP',
        value: 'CF-3GPP-33108',
      }, {
        label: 'ETSI IP (TS 102 232)',
        value: 'CF-ETSI-102232',
      }, {
        label: 'US cdma2000 PoC (TIA-1072)',
        value: 'CF-TIA-1072',
      }, {
        label: 'US cdma2000 VoIP (TIA-1066)',
        value: 'CF-TIA-1066',
      }, {
        label: 'US Wireless/Wireline (J-STD-025A)',
        value: 'CF-JSTD025A',
      },
      {
        label: 'US cdma2000 Wireless (J-STD-025B)',
        value: 'CF-JSTD025B',
      },
      {
        label: 'US Wireline VoIP (T1.678)',
        value: 'CF-ATIS-1000678',
      }];

      expect((templates['CF-3GPP-33108'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-ETSI-102232'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-TIA-1072'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-TIA-1066'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-JSTD025A'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-JSTD025B'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates['CF-ATIS-1000678'].main as FormTemplateSection).fields.tag.options).toEqual(expected);
    });

    it('should set the timezones', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = expect.arrayContaining([{
        label: 'Select a time zone',
        value: '',
      }, {
        label: expect.stringContaining('US/Alaska'),
        value: 'America/Anchorage',
      }, {
        label: expect.stringContaining('US/Arizona'),
        value: 'America/Phoenix',
      }, {
        label: expect.stringContaining('US/Central'),
        value: 'America/Chicago',
      }, {
        label: expect.stringContaining('US/Eastern'),
        value: 'America/New_York',
      }, {
        label: expect.stringContaining('US/Hawaii '),
        value: 'Pacific/Honolulu',
      }, {
        label: expect.stringContaining('US/Mountain'),
        value: 'America/Denver',
      }, {
        label: expect.stringContaining('US/Pacific'),
        value: 'America/Los_Angeles',
      }]);

      expect((templates['CF-3GPP-33108'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-ETSI-102232'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-TIA-1072'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-TIA-1066'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-JSTD025A'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-JSTD025B'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-ATIS-1000678'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
    });

    it('should set the timezone if one option only', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        [mockTimeZones[0]],
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = expect.arrayContaining([{
        label: 'Select a time zone',
        value: '',
      }, {
        label: expect.stringContaining('US/Pacific'),
        value: 'America/Los_Angeles',
      }]);

      expect((templates['CF-3GPP-33108'].facilityInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates['CF-3GPP-33108'].facilityInformation as FormTemplateSection).fields.timeZone.initial).toEqual(mockTimeZones[0].value);
    });

    it('should set the contacts', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a contact',
        value: '',
      }, {
        label: 'Contact 1',
        value: 'Q29udGFjdCAx',
      }, {
        label: 'Contact 2',
        value: 'Q29udGFjdCAy',
      }];

      expect((templates['CF-3GPP-33108'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-ETSI-102232'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-TIA-1072'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-TIA-1066'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-JSTD025A'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-JSTD025B'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates['CF-ATIS-1000678'].facilityInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
    });

    it('should set the lea', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [
        { label: 'Select a LEA', value: '' },
        { label: 'CA-DOJBNE', value: 'id1' },
        { label: 'CookCounty', value: 'id2' },
        { label: 'CA-DOJBNE-TIA1072', value: 'id3' },
        { label: 'CA-DOJBNE-5GCF', value: 'id4' },
        { label: 'Flavia', value: 'id5' },
      ];

      expect((templates['CF-3GPP-33108'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-ETSI-102232'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-TIA-1072'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-TIA-1066'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-JSTD025A'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-JSTD025A'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
      expect((templates['CF-ATIS-1000678'].main as FormTemplateSection).fields.cfGroup.options)
        .toEqual(expected);
    });

    it('should set the ftp info', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a FTP info ID',
        value: '',
      }, {
        label: 'ftp1',
        value: 'ZnRwMQ',
      }, {
        label: 'ftp2',
        value: 'ZnRwMg',
      }];

      expect((templates['CF-3GPP-33108'].hi3Interfaces_FTP as FormTemplateSection).fields.ftpInfoId.options).toEqual(expected);
      expect((templates['CF-ETSI-102232'].hi3Interfaces_FTP as FormTemplateSection).fields.ftpInfoId.options).toEqual(expected);
    });

    it('should set the network id', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [
        { label: 'Select a Network ID', value: '' },
        { label: 'XCPrcs - 11.11.11.11:0', value: 'WENQcmNz' },
        { label: 'XCPv6 - [abcd:abcd:abcd:abcd::abcd]:0', value: 'WENQdjY' },
        { label: 'XTP128v4_WGW - 101.101.101.101:15581', value: 'WFRQMTI4djRfV0dX' },
        { label: 'XTP128v6_WGW - [bcde:bcde:bcde:bcde::bcde]:15581', value: 'WFRQMTI4djZfV0dX' },
        { label: 'XCP-container - 0.0.0.0:0', value: 'WENQLWNvbnRhaW5lcg' },
      ];

      const expectedXCPNetwork = [
        { label: 'Select a XCP Network ID', value: '' },
        ...([...expected].slice(1)),
      ];

      const expectedXDPNetwork = [
        { label: 'Select a XDP Network ID', value: '' },
        ...([...expected].slice(1)),
      ];

      expect((templates['CF-JSTD025B'].hi3Interfaces_TCP as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expected);
      expect((templates['CF-ETSI-102232'].hi3Interfaces_TCP as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedXDPNetwork);
      expect((templates['CF-ETSI-102232'].hi2Interfaces_TCP as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedXCPNetwork);
    });

    it('should set the security info', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [
        { label: 'Select a Security Info ID', value: '' },
        { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
        { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
        { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
        { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
      ];

      expect((templates['CF-ETSI-102232'].hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expected);
      expect((templates['CF-ETSI-102232'].hi3Interfaces_TCP as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expected);
    });

    it('should set the domain groups', () => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      const expected = [
        { label: 'Select a Domain Group', value: '' },
        { label: 'domain1', value: 'ZG9tYWluMQ' },
        { label: 'domain2', value: 'ZG9tYWluMg' },
      ];

      expect((templates['CF-ETSI-102232'].hi3Interfaces_TCP as FormTemplateSection).fields.fsid.options)
        .toEqual(expected);
    });
  });

  describe('adaptTemplateVisibilityMode()', () => {
    describe('CF-3GPP-33108', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-3GPP-33108'];
      });

      it('should return undefined if template is not defined', () => {
        expect(adaptTemplateVisibilityMode(undefined, 'create')).toEqual(undefined);
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-ETSI-102232', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-ETSI-102232'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          hi2Interfaces_FTP: {
            ...(template.hi2Interfaces_FTP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_FTP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_FTP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          hi2Interfaces_SFTP: {
            ...(template.hi2Interfaces_SFTP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_SFTP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_SFTP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-TIA-1072', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-TIA-1072'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-TIA-1066', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-TIA-1066'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-JSTD025A', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-JSTD025A'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-JSTD025B', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-JSTD025B'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });

    describe('CF-ATIS-1000678', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          collectionFunctionTemplates,
          mockTimeZones,
          mockContacts,
          mockCollectionFunctionGroups,
          mockFtpInfos,
          mockNetworks,
          mockSecurityInfos,
          mockFqdnGroups,
          mockSupportedFeatures.config.collectionFunctions,
          mockNodes,
        );
        template = templates['CF-ATIS-1000678'];
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          main: {
            ...(template.main as FormTemplateSection),
            fields: {
              ...(template.main as FormTemplateSection).fields,
              tag: {
                ...(template.main as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.main as FormTemplateSection).fields.name,
                readOnly: true,
              },
            },
          },
          hi2Interfaces_TCP: {
            ...(template.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(template.hi2Interfaces_TCP as FormTemplateSection).fields,
              state: {
                ...(template.hi2Interfaces_TCP as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });
    });
  });

  describe('adaptTemplateName()', () => {
    let template: FormTemplate;

    beforeEach(() => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      template = templates['CF-3GPP-33108'];
    });

    it('should set the name and key', () => {
      expect(adaptTemplateName(template, 'name1')).toEqual({
        ...template,
        key: expect.not.stringContaining(template.key),
        main: {
          ...(template.main as FormTemplateSection),
          fields: {
            ...(template.main as FormTemplateSection).fields,
            name: {
              ...(template.main as FormTemplateSection).fields.name,
              initial: 'name1',
            },
          },
        },
      });
    });
  });

  describe('adaptTemplateTimezoneOptions()', () => {
    let collectionFunctionFormData: CollectionFunctionFormData;
    let template: FormTemplate;

    beforeEach(() => {
      collectionFunctionFormData = adaptToForm(mockCollectionFunctions[0]);
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      template = templates['CF-ETSI-102232'];
    });

    it('should return undefined if template is not defined', () => {
      expect(adaptTemplateTimezoneOptions(undefined, collectionFunctionFormData, mockTimeZones)).toEqual(undefined);
    });

    it('should return the template without any change if timezone is supported', () => {
      expect(adaptTemplateTimezoneOptions(template, collectionFunctionFormData, mockTimeZones)).toEqual(template);
    });

    it('should add the timezone from data if it was not included before', () => {
      collectionFunctionFormData.timeZone = 'America/Tijuana';
      expect(adaptTemplateTimezoneOptions(template, collectionFunctionFormData, mockTimeZones)).toEqual({
        ...template,
        facilityInformation: {
          ...(template.facilityInformation as FormTemplateSection),
          fields: {
            ...(template.facilityInformation as FormTemplateSection).fields,
            timeZone: {
              ...(template.facilityInformation as FormTemplateSection).fields.timeZone,
              options: [{
                label: 'Select a time zone',
                value: '',
              }, {
                label: expect.stringContaining('America/Tijuana'),
                value: 'America/Tijuana',
              }, {
                label: expect.stringContaining('US/Alaska'),
                value: 'America/Anchorage',
              }, {
                label: expect.stringContaining('US/Arizona'),
                value: 'America/Phoenix',
              }, {
                label: expect.stringContaining('US/Central'),
                value: 'America/Chicago',
              }, {
                label: expect.stringContaining('US/Eastern'),
                value: 'America/New_York',
              }, {
                label: expect.stringContaining('US/Hawaii '),
                value: 'Pacific/Honolulu',
              }, {
                label: expect.stringContaining('US/Mountain'),
                value: 'America/Denver',
              }, {
                label: expect.stringContaining('US/Pacific'),
                value: 'America/Los_Angeles',
              }],
            },
          },
        },
      });
    });
  });

  describe('adaptTemplateInterfaceTransportType()', () => {
    let template: FormTemplate;

    beforeEach(() => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      template = templates['CF-3GPP-33108'];
    });

    it('should set the initial field as specified', () => {
      expect(adaptTemplateInterfaceTransportType(template, 'hi2Interfaces', 'FTP')).toEqual({
        ...template,
        key: expect.any(String),
        hi2Interfaces: {
          ...(template.hi2Interfaces as FormTemplateSection),
          fields: {
            ...(template.hi2Interfaces as FormTemplateSection).fields,
            transportType: {
              ...(template.hi2Interfaces as FormTemplateSection).fields.transportType,
              initial: 'FTP',
            },
          },
        },
      });
    });
  });

  describe('adaptTemplateForInterfacesFieldUpdate()', () => {
    let template: FormTemplate;
    let templateETSI: FormTemplate;
    let collectionFunction: Partial<CollectionFunctionFormData>;
    let collectionFunctionETSI: Partial<CollectionFunctionFormData>;

    beforeEach(() => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      template = templates['CF-3GPP-33108'];
      templateETSI = templates['CF-ETSI-102232'];

      collectionFunction = createCollectionFunctionBasedOnTemplate(template, true);
      collectionFunctionETSI = createCollectionFunctionBasedOnTemplate(templateETSI, true);
    });

    it('should not change anything if not one of the affected fields', () => {
      expect(adaptTemplateForInterfacesFieldUpdate('hi2Interfaces', collectionFunction, template, 'id', '1')).toEqual({
        ...template,
      });
    });

    describe('FTP Info ID (ftpInfoId)', () => {
      it('should change the template key if ftpInfoId was changed', () => {
        expect(adaptTemplateForInterfacesFieldUpdate('hi2Interfaces', collectionFunction, template, 'ftpInfoId', '1')).toEqual({
          ...template,
          key: expect.not.stringContaining(template.key),
        });
      });
    });

    describe('Integrity check (icheck)', () => {
      it('should set hashNum, hashSec, hashCert as disabled if icheck is OFF', () => {
        const actual = adaptTemplateForInterfacesFieldUpdate(
          'hi2Interfaces',
          collectionFunctionETSI,
          templateETSI,
          'icheck',
          'OFF',
        );

        expect(actual).toEqual({
          ...templateETSI,
          key: expect.not.stringContaining(templateETSI.key),
          hi2Interfaces_TCP: {
            ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
              hashNum: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashNum,
                disabled: true,
              },
              hashSec: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashSec,
                disabled: true,
              },
              hashCert: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashCert,
                disabled: true,
              },
            },
          },
        });
      });

      it('should set hashNum, hashSec, hashCert as enabled if icheck is HASH1WAY', () => {
        const actual = adaptTemplateForInterfacesFieldUpdate(
          'hi2Interfaces',
          collectionFunctionETSI,
          templateETSI,
          'icheck',
          'SIGNEDHASH',
        );

        expect(actual).toEqual({
          ...templateETSI,
          key: expect.not.stringContaining(templateETSI.key),
          hi2Interfaces_TCP: {
            ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
              hashNum: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashNum,
                disabled: false,
              },
              hashSec: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashSec,
                disabled: false,
              },
              hashCert: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashCert,
                disabled: false,
              },
            },
          },
        });
      });

      it('should set hashNum, hashSec, hashCert as enabled if icheck is SIGNEDHASH', () => {
        const actual = adaptTemplateForInterfacesFieldUpdate(
          'hi2Interfaces',
          collectionFunctionETSI,
          templateETSI,
          'icheck',
          'SIGNEDHASH',
        );

        expect(actual).toEqual({
          ...templateETSI,
          key: expect.not.stringContaining(templateETSI.key),
          hi2Interfaces_TCP: {
            ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
              hashNum: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashNum,
                disabled: false,
              },
              hashSec: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashSec,
                disabled: false,
              },
              hashCert: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashCert,
                disabled: false,
              },
            },
          },
        });
      });

      it('should set hashNum, hashSec, hashCert as enabled if icheck is HASH2WAY', () => {
        const actual = adaptTemplateForInterfacesFieldUpdate(
          'hi2Interfaces',
          collectionFunctionETSI,
          templateETSI,
          'icheck',
          'HASH2WAY',
        );

        expect(actual).toEqual({
          ...templateETSI,
          key: expect.not.stringContaining(templateETSI.key),
          hi2Interfaces_TCP: {
            ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
            fields: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
              hashNum: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashNum,
                disabled: false,
              },
              hashSec: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashSec,
                disabled: false,
              },
              hashCert: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.hashCert,
                disabled: false,
              },
            },
          },
        });
      });

      it('should do nothing otherwise', () => {
        const actual = adaptTemplateForInterfacesFieldUpdate(
          'hi2Interfaces',
          collectionFunctionETSI,
          templateETSI,
          'icheck',
          'something',
        );

        expect(actual).toEqual({
          ...templateETSI,
          key: expect.not.stringContaining(templateETSI.key),
        });
      });
    });

    describe('Version (version)', () => {
      let options: FormTemplateOption[] = [];
      let filteredOptions: FormTemplateOption[] = [];

      beforeEach(() => {
        options = [
          { label: 'None', value: 'NONE' },
          { label: 'GB (Britain)', value: 'GB' },
          { label: 'JP (Japan)', value: 'JP' },
          { label: 'NL (Netherlands)', value: 'NL' },
          { label: 'VN (Vietnam)', value: 'VN' },
          { label: 'OM (Oman)', value: 'OM' },
          { label: 'TR (Turkey)', value: 'TR' },
          { label: 'US (USA)', value: 'US' },
          { label: 'IE (Ireland)', value: 'IE' },
        ];

        filteredOptions = options.filter(({ value }) => value !== 'IE');
      });

      describe('when TCP', () => {
        it('should set useHi3 as disabled, countryCode as disabled and limit its options if version is 1.4.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '1.4.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: true,
                  options: [options[0]],
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: true,
                },
              },
            },
          });
        });

        it('should set useHi3 as disabled, the countryCode as enabled and limit its options if version is 2.2.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '2.2.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: [options[0], options[8]],
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: true,
                },
              },
            },
          });
        });

        it('should set useHi3 as disabled, the countryCode as enabled and limit its options if version is 2.5.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '2.5.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: [options[0], options[6]],
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: true,
                },
              },
            },
          });
        });

        it('should set useHi3 as enabled, the countryCode as enabled and limit its options if version is 3.1.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.1.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
              },
            },
          });
        });

        it('should set useHi3 as enabled, countryCode as enabled and limit its options if version is 3.10.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.10.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
              },
            },
          });
        });

        it('should set useHi3 as enabled, countryCode as enabled and limit its options if version is 3.15.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.15.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
              },
            },
          });
        });

        it('should set useHi3 as enabled, countryCode as enabled and limit its options if version is 3.17.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.17.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
              },
            },
          });
        });

        it('should set useHi3 as enabled, countryCode as enabled and limit its options if version is 3.21.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.21.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
              },
            },
          });
        });

        it('should also set the countryCode related fields when changing the version', () => {
          const adaptedTemplateETSI = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NL',
          ) as FormTemplate;

          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            adaptedTemplateETSI,
            'version',
            '3.21.1',
          );

          expect(actual).toEqual({
            ...adaptedTemplateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
                useHi3: {
                  ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.useHi3,
                  disabled: false,
                },
                pldEncKey: {
                  ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                },
                secinfoid: {
                  ...(adaptedTemplateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should not change useHi3 if it does not exists and set the countryCode for hi3 too', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.21.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi3Interfaces_TCP: {
              ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
              },
            },
          });
        });
      });

      xdescribe('when FTP', () => {
        beforeEach(() => {
          (collectionFunctionETSI.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
          (collectionFunctionETSI.hi3Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
        });

        it('should set countryCode as disabled and limit its options if version is 1.4.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '1.4.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: true,
                  options: [options[0]],
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 2.2.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '2.2.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: [options[0], options[8]],
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 2.5.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '2.5.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: [options[0], options[6]],
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 3.1.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.1.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 3.10.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.10.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 3.15.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.15.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: false,
                  options: filteredOptions,
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 3.17.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.17.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: true,
                  options: [options[0]],
                },
              },
            },
          });
        });

        it('should set countryCode as enabled and limit its options if version is 3.21.1', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.21.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: true,
                  options: [options[0]],
                },
              },
            },
          });
        });

        it('should set the countryCode for hi3 too', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'version',
            '3.21.1',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi3Interfaces_FTP: {
              ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection).fields,
                countryCode: {
                  ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection).fields.countryCode,
                  disabled: true,
                  options: [options[0]],
                },
              },
            },
          });
        });
      });
    });

    describe('Variant / Country Code (countryCode)', () => {
      describe('when TCP', () => {
        it('should set pldEncKey and secinfoid as disabled if country is NONE', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NONE',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey and secinfoid as disabled if country is US', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'US',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey and secinfoid as enabled if country is TR', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'TR',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: false,
                  validation: { required: true },
                },
              },
            },
          });
        });

        it('should set pldEncKey and secinfoid as disabled if country is TR and version 2.5.1', () => {
          (collectionFunctionETSI.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].version = '2.5.1';

          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'TR',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey and secinfoid as enabled if country is NL', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NL',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: false,
                  validation: { required: true },
                },
              },
            },
          });
        });

        it('should set pldEncKey and secinfoid as enabled if country is NL for hi3 too', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NL',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_TCP: {
              ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                },
                secinfoid: {
                  ...(templateETSI.hi2Interfaces_TCP as FormTemplateSection).fields.secinfoid,
                  disabled: false,
                  validation: { required: true },
                },
              },
            },
          });
        });
      });

      xdescribe('when FTP', () => {
        beforeEach(() => {
          (collectionFunctionETSI.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
          (collectionFunctionETSI.hi3Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
        });

        it('should set pldEncKey as disabled if country is NONE', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NONE',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey as disabled if country is US', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'US',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey as enabled if country is TR', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'TR',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey as disabled if country is TR and version 2.5.1', () => {
          (collectionFunctionETSI.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].version = '2.5.1';

          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'TR',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });

        it('should set pldEncKey as enabled if country is NL', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NL',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                  validation: { required: true },
                },
              },
            },
          });
        });

        it('should set pldEncKey as enabled if country is NL for hi3 too', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi2Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'NL',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi2Interfaces_FTP: {
              ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi2Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: false,
                  validation: { required: true },
                },
              },
            },
          });
        });

        it('should set pldEncKey as disabled if country is TR and version 2.5.1', () => {
          (collectionFunctionETSI.hi3Interfaces as CollectionFunctionInterfaceFormData[])[0].version = '2.5.1';

          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'countryCode',
            'TR',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi3Interfaces_FTP: {
              ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection).fields,
                pldEncKey: {
                  ...(templateETSI.hi3Interfaces_FTP as FormTemplateSection).fields.pldEncKey,
                  disabled: true,
                  validation: { required: false },
                },
              },
            },
          });
        });
      });
    });

    describe('Destination Address Type (destType)', () => {
      describe('When TCP', () => {
        it('should set destIPAddr to visible and fsid to hidden if destType is ipAddress', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'destType',
            'ipAddress',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi3Interfaces_TCP: {
              ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields,
                destIPAddr: {
                  ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields.destIPAddr,
                  hide: 'false',
                },
                fsid: {
                  ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields.fsid,
                  hide: 'true',
                },
              },
            },
          });
        });

        it('should set destIPAddr to hidden and fsid to visible if destType is domainGroup', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'destType',
            'domainGroup',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
            hi3Interfaces_TCP: {
              ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection),
              fields: {
                ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields,
                destIPAddr: {
                  ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields.destIPAddr,
                  hide: 'true',
                },
                fsid: {
                  ...(templateETSI.hi3Interfaces_TCP as FormTemplateSection).fields.fsid,
                  hide: 'false',
                },
              },
            },
          });
        });
      });

      xdescribe('when FTP', () => {
        beforeEach(() => {
          (collectionFunctionETSI.hi2Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
          (collectionFunctionETSI.hi3Interfaces as CollectionFunctionInterfaceFormData[])[0].transportType = 'FTP';
        });

        it('should do nothing for FTP', () => {
          const actual = adaptTemplateForInterfacesFieldUpdate(
            'hi3Interfaces',
            collectionFunctionETSI,
            templateETSI,
            'destType',
            'domainGroup',
          );

          expect(actual).toEqual({
            ...templateETSI,
            key: expect.not.stringContaining(templateETSI.key),
          });
        });
      });
    });
  });

  describe('getInterfaceFormTemplateSection()', () => {
    let template: FormTemplate;

    beforeEach(() => {
      const templates = adaptTemplateOptions(
        collectionFunctionTemplates,
        mockTimeZones,
        mockContacts,
        mockCollectionFunctionGroups,
        mockFtpInfos,
        mockNetworks,
        mockSecurityInfos,
        mockFqdnGroups,
        mockSupportedFeatures.config.collectionFunctions,
        mockNodes,
      );
      template = templates['CF-3GPP-33108'];
    });

    it('should get the interface form section', () => {
      const data: Partial<CollectionFunctionFormData> = {
        hi2Interfaces: [{
          id: '',
          name: '',
          transportType: 'FTP',
        }],
      };

      expect(getInterfaceFormTemplateSection('hi2Interfaces', data, template)).toEqual(
        template.hi2Interfaces_FTP,
      );
    });
  });

  describe('getInterfaceFormTemplateSectionName()', () => {
    it('should get the template form section name', () => {
      expect(getInterfaceFormTemplateSectionName('hi2Interfaces', 'FTP')).toEqual(
        'hi2Interfaces_FTP',
      );
    });
  });

  describe('adaptInterfaceTemplateState()', () => {
    it('should set "true" for interface state.hide', () => {
      const interfaceFields: FormTemplateFieldMap = { state: { hide: 'false', type: 'custom' } };
      const interfaceData: CollectionFunctionInterfaceFormData = { id: `1.${COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`, name: 'cf name' };

      expect(interfaceFields.state.hide).toEqual('false');

      const adaptedInterfaceFields = adaptInterfaceTemplateState(interfaceFields, interfaceData, true);

      expect(adaptedInterfaceFields.state.hide).toEqual('true');
    });

    it('should not set "true" for interface state.hide when is not editing', () => {
      const interfaceFields: FormTemplateFieldMap = { state: { hide: 'false', type: 'custom' } };
      const interfaceData: CollectionFunctionInterfaceFormData = { id: `1.${COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`, name: 'cf name' };

      expect(interfaceFields.state.hide).toEqual('false');

      const adaptedInterfaceFields = adaptInterfaceTemplateState(interfaceFields, interfaceData, false);

      expect(adaptedInterfaceFields.state.hide).toEqual('false');
    });

    it('should not set "true" for interface state.hide when interface id is not temporary', () => {
      const interfaceFields: FormTemplateFieldMap = { state: { hide: 'false', type: 'custom' } };
      const interfaceData: CollectionFunctionInterfaceFormData = { id: 'INTERFACE_ID', name: 'cf name' };

      expect(interfaceFields.state.hide).toEqual('false');

      const adaptedInterfaceFields = adaptInterfaceTemplateState(interfaceFields, interfaceData, true);

      expect(adaptedInterfaceFields.state.hide).toEqual('false');
    });

    it('should not set "true" for interface state.hide when interface state is not defined', () => {
      const interfaceFields: FormTemplateFieldMap = { };
      const interfaceData: CollectionFunctionInterfaceFormData = { id: `1.${COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`, name: 'cf name' };

      const adaptedInterfaceFields = adaptInterfaceTemplateState(interfaceFields, interfaceData, true);

      expect(adaptedInterfaceFields.state).toBeFalsy();
    });

  });
});
