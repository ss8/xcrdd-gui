import { mockCollectionFunctions } from '__test__/mockCollectionFunctions';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import adaptCollectionFunctionsToGrid from './collectionFunctionGrid.adapter';

describe('collectionFunctionGrid.adapter', () => {
  describe('adaptCollectionFunctionsToGrid()', () => {
    it('should set tagName property for each CF according the collection template', () => {
      const { collectionFunctions } = mockSupportedFeatures.config;
      const adaptedCollectionFunctions = adaptCollectionFunctionsToGrid(
        mockCollectionFunctions,
        collectionFunctions,
      );

      expect(adaptedCollectionFunctions[0].tagName).toBe(collectionFunctions[mockCollectionFunctions[0].tag].title);
      expect(adaptedCollectionFunctions[1].tagName).toBe(collectionFunctions[mockCollectionFunctions[1].tag].title);
      expect(adaptedCollectionFunctions[2].tagName).toBe(collectionFunctions[mockCollectionFunctions[2].tag].title);
      expect(adaptedCollectionFunctions[3].tagName).toBe(collectionFunctions[mockCollectionFunctions[3].tag].title);
      expect(adaptedCollectionFunctions[4].tagName).toBe(collectionFunctions[mockCollectionFunctions[4].tag].title);
      expect(adaptedCollectionFunctions[5].tagName).toBe(collectionFunctions[mockCollectionFunctions[5].tag].title);
    });
  });
});
