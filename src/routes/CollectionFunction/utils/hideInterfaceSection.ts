import { FormTemplateSection } from 'data/template/template.types';
import { TabProp } from 'shared/multi-tab/MultiTab';

export default function hideInterfaceSection(
  formTemplateSection:FormTemplateSection | undefined,
  isEditing:boolean,
  tabs:TabProp[],
): boolean {
  const hasNoInterfacesInViewMode = isEditing === false && tabs?.length === 0;

  return hasNoInterfacesInViewMode || formTemplateSection == null;
}
