import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';
import {
  CollectionFunctionFormData,
  HI2InterfaceFormData,
  HI3InterfaceFormData,
  COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID,
} from 'data/collectionFunction/collectionFunction.types';
import { getInterfaceFormTemplateSectionName } from './collectionFunctionTemplate.adapter';

let nextId = 1;

// eslint-disable-next-line import/prefer-default-export
export function createCollectionFunctionBasedOnTemplate(
  template?: FormTemplate,
  forceInterfaceCreation = false,
): Partial<CollectionFunctionFormData> {
  if (template == null) {
    return {};
  }

  const collectionFunctionForm: Partial<CollectionFunctionFormData> = {
    ...getFieldValuesFromSection(template.main as FormTemplateSection),
    ...getFieldValuesFromSection(template.facilityInformation as FormTemplateSection),
    hi2Interfaces: getInterfaceFieldValuesFromSection(
      template.hi2Interfaces as FormTemplateSection,
      template,
      'hi2Interfaces',
      forceInterfaceCreation,
    ),
    hi3Interfaces: getInterfaceFieldValuesFromSection(
      template.hi3Interfaces as FormTemplateSection,
      template,
      'hi3Interfaces',
      forceInterfaceCreation,
    ),
  };

  return omitBy(collectionFunctionForm, isNil) as unknown as CollectionFunctionFormData;
}

function getFieldValuesFromSection(
  templateSection: FormTemplateSection,
): Partial<CollectionFunctionFormData> {
  return Object.entries(templateSection?.fields)
    .filter(([_fieldName, field]) => field.initial != null)
    .reduce((previousValue, [fieldName, field]) => ({
      ...previousValue,
      [fieldName]: field.initial,
    }), {});
}

function getInterfaceFieldValuesFromSection(
  templateSection: FormTemplateSection,
  template: FormTemplate,
  interfaceName: 'hi2Interfaces' | 'hi3Interfaces',
  forceInterfaceCreation = false,
): (HI2InterfaceFormData | HI3InterfaceFormData)[] | undefined {

  if (templateSection == null) {
    return;
  }

  let fields: Partial<HI2InterfaceFormData | HI3InterfaceFormData> = {
    ...getFieldValuesFromSection(templateSection),
  };

  const templateSectionName = getInterfaceFormTemplateSectionName(interfaceName, fields.transportType as string);

  if (!forceInterfaceCreation && (template[templateSectionName] as FormTemplateSection).metadata.min === 0) {
    return [];
  }

  fields = {
    ...fields,
    ...getFieldValuesFromSection(template[templateSectionName] as FormTemplateSection),
    id: `${nextId}.${COLLECTION_FUNCTION_INTERFACE_TEMPORARY_ID}`,
  };

  nextId += 1;

  return [fields as HI2InterfaceFormData | HI3InterfaceFormData];
}
