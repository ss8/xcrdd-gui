import { xcipioBehindNatValueGetter } from './collectionFunctionGridValueGetters';

describe('collectionFunctionGridValueGetters.adapter', () => {

  describe('xcipioBehindNatValueGetter()', () => {
    it('should return "no" to insideNat=false', () => {
      expect(xcipioBehindNatValueGetter({ data: { insideNat: false } })).toBe('no');
    });

    it('should return "yes" to insideNat=true', () => {
      expect(xcipioBehindNatValueGetter({ data: { insideNat: true } })).toBe('yes');
    });
  });

});
