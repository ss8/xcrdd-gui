import {
  FormTemplate,
} from 'data/template/template.types';
import { CollectionFunctionFormData } from 'data/collectionFunction/collectionFunction.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { createCollectionFunctionBasedOnTemplate } from './collectionFunctionTemplate.factory';
import { adaptTemplateInterfaceTransportType } from './collectionFunctionTemplate.adapter';

describe('collectionFunctionTemplate.factory', () => {
  describe('createCollectionFunctionBasedOnTemplate', () => {
    let collectionFunctionTemplates: {[key: string]: FormTemplate};

    beforeEach(() => {
      collectionFunctionTemplates = {
        'CF-3GPP-33108': getMockTemplateByKey('CF-3GPP-33108'),
        'CF-ETSI-102232': getMockTemplateByKey('CF-ETSI-102232'),
        'CF-TIA-1072': getMockTemplateByKey('CF-TIA-1072'),
        'CF-TIA-1066': getMockTemplateByKey('CF-TIA-1066'),
        'CF-JSTD025A': getMockTemplateByKey('CF-JSTD025A'),
        'CF-JSTD025B': getMockTemplateByKey('CF-JSTD025B'),
        'CF-ATIS-1000678': getMockTemplateByKey('CF-ATIS-1000678'),
      };
    });

    describe('CF-3GPP-33108', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'GPRS',
          name: '',
          tag: 'CF-3GPP-33108',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              keepAliveFreq: '60',
              keepAliveRetries: '6',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              tpkt: true,
              transportType: 'TCP',
              version: '15.6',
            },
          ],
          hi3Interfaces: [],
        };

        const template = collectionFunctionTemplates['CF-3GPP-33108'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing create all interfaces', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'GPRS',
          name: '',
          tag: 'CF-3GPP-33108',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              keepAliveFreq: '60',
              keepAliveRetries: '6',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              tpkt: true,
              transportType: 'TCP',
              version: '15.6',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              displayLabel: '',
              dscp: '0',
              keepAliveFreq: '60',
              keepAliveRetries: '6',
              inactivityTimer: '0',
              ownIPAddr: '',
              ownIPPort: '',
              tpkt: true,
              version: '15.6',
              useIpdu: true,
              transportType: 'TCP',
            },
          ],
        };

        const template = collectionFunctionTemplates['CF-3GPP-33108'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template for transport type as FTP', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'GPRS',
          name: '',
          tag: 'CF-3GPP-33108',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              destIPAddr: '',
              destPort: '',
              destPath: '',
              transportType: 'FTP',
              userName: '',
              userPassword: '',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              destIPAddr: '',
              destPort: '',
              displayLabel: '',
              transportType: 'FTP',
              destPath: '',
              userName: '',
              userPassword: '',
              operatorId: '',
              fileMode: 'ALLINONE',
              ftpFileSize: '',
              ftpFileSec: '',
              ftpInfoId: '',
            },
          ],
        };

        let template = collectionFunctionTemplates['CF-3GPP-33108'];
        template = adaptTemplateInterfaceTransportType(template, 'hi2Interfaces', 'FTP') as FormTemplate;
        template = adaptTemplateInterfaceTransportType(template, 'hi3Interfaces', 'FTP') as FormTemplate;
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('CF-ETSI-102232', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'ETSIIP',
          name: '',
          tag: 'CF-ETSI-102232',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf2NodeName: '',
              name: '',
              transportType: 'TCP',
              connectionType: 'PRIMARY',
              countryCode: 'NONE',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              ownnetid: '',
              pldEncKey: '',
              pldEncType: false,
              reqState: 'ACTIVE',
              secinfoid: '',
              state: '',
              useHi3: false,
              version: '3.21.1',
            },
          ],
          hi3Interfaces: [],
        };

        const template = collectionFunctionTemplates['CF-ETSI-102232'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing all interfaces to be created', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'ETSIIP',
          name: '',
          tag: 'CF-ETSI-102232',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf2NodeName: '',
              name: '',
              transportType: 'TCP',
              connectionType: 'PRIMARY',
              countryCode: 'NONE',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              ownnetid: '',
              pldEncKey: '',
              pldEncType: false,
              reqState: 'ACTIVE',
              secinfoid: '',
              state: '',
              useHi3: false,
              version: '3.21.1',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf3NodeName: '',
              name: '',
              transportType: 'TCP',
              connectionType: 'PRIMARY',
              countryCode: 'NONE',
              destIPAddr: '',
              destPort: '',
              destType: 'ipAddress',
              displayLabel: '',
              fsid: '',
              dscp: '0',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              ownnetid: '',
              pldEncKey: '',
              pldEncType: false,
              reqState: 'ACTIVE',
              secinfoid: '',
              traceLevel: '0',
              version: '3.21.1',
            },
          ],
        };

        const template = collectionFunctionTemplates['CF-ETSI-102232'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template for transport type as FTP', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'ETSIIP',
          name: '',
          tag: 'CF-ETSI-102232',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf2NodeName: '',
              name: '',
              transportType: 'FTP',
              countryCode: 'NONE',
              destIPAddr: '',
              destPath: '',
              destPort: '',
              fileMode: 'ALLINONE',
              ftpFileSec: '5',
              ftpFileSize: '64',
              ftpType: 'FTP',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              operatorId: '',
              pldEncKey: '',
              pldEncType: false,
              reqState: 'ACTIVE',
              state: '',
              userName: '',
              userPassword: '',
              version: '3.21.1',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf3NodeName: '',
              name: '',
              transportType: 'FTP',
              countryCode: 'NONE',
              destIPAddr: '',
              destPath: '',
              destPort: '',
              displayLabel: '',
              fileMode: 'ALLINONE',
              ftpFileSec: '',
              ftpFileSize: '',
              ftpInfoId: '',
              ftpType: 'FTP',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              operatorId: '',
              pldEncKey: '',
              pldEncType: false,
              userName: '',
              userPassword: '',
              version: '3.21.1',
            },
          ],
        };

        let template = collectionFunctionTemplates['CF-ETSI-102232'];
        template = adaptTemplateInterfaceTransportType(template, 'hi2Interfaces', 'FTP') as FormTemplate;
        template = adaptTemplateInterfaceTransportType(template, 'hi3Interfaces', 'FTP') as FormTemplate;
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template for transport type as SFTP', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'ETSIIP',
          name: '',
          tag: 'CF-ETSI-102232',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf2NodeName: '',
              name: '',
              transportType: 'SFTP',
              countryCode: 'NONE',
              destIPAddr: '',
              destPath: '',
              destPort: '',
              fileMode: 'ALLINONE',
              ftpFileSec: '5',
              ftpFileSize: '64',
              ftpType: 'SFTP',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              operatorId: '',
              pldEncKey: '',
              pldEncType: false,
              reqState: 'ACTIVE',
              state: '',
              userName: '',
              userPassword: '',
              version: '3.21.1',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              mdf3NodeName: '',
              name: '',
              transportType: 'FTP',
              countryCode: 'NONE',
              destIPAddr: '',
              destPath: '',
              destPort: '',
              displayLabel: '',
              fileMode: 'ALLINONE',
              ftpFileSec: '',
              ftpFileSize: '',
              ftpInfoId: '',
              ftpType: 'FTP',
              hashCert: '',
              hashNum: '10',
              hashSec: '60',
              icheck: 'OFF',
              operatorId: '',
              pldEncKey: '',
              pldEncType: false,
              userName: '',
              userPassword: '',
              version: '3.21.1',
            },
          ],
        };

        let template = collectionFunctionTemplates['CF-ETSI-102232'];
        template = adaptTemplateInterfaceTransportType(template, 'hi2Interfaces', 'SFTP') as FormTemplate;
        template = adaptTemplateInterfaceTransportType(template, 'hi3Interfaces', 'FTP') as FormTemplate;
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('CF-TIA-1072', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'TIA1072',
          name: '',
          tag: 'CF-TIA-1072',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: 'TIA1072',
            },
          ],
          hi3Interfaces: [],
        };

        const template = collectionFunctionTemplates['CF-TIA-1072'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing create all interfaces', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'TIA1072',
          name: '',
          tag: 'CF-TIA-1072',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: 'TIA1072',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              destIPAddr: '',
              destPort: '',
              displayLabel: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              version: 'TIA1072',
              transport: 'TCP',
              transportType: 'TCP',
            },
          ],
        };

        const template = collectionFunctionTemplates['CF-TIA-1072'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('CF-TIA-1066', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'TIA1066',
          name: '',
          tag: 'CF-TIA-1066',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: '2',
            },
          ],
          hi3Interfaces: [],
        };

        const template = collectionFunctionTemplates['CF-TIA-1066'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing create all interfaces', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'TIA1066',
          name: '',
          tag: 'CF-TIA-1066',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: '2',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              version: '2',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              displayLabel: '',
              dscp: '0',
              filter: 'ALL',
              ownIPAddr: '',
              ownIPPort: '',
              transport: 'TCP',
              transportType: 'TCP',
            },
          ],
        };

        const template = collectionFunctionTemplates['CF-TIA-1066'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('CF-JSTD025A', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          bufferCC: false,
          cellTowerLocation: false,
          cfGroup: '',
          contact: '',
          deliveryThru: 'TCP_OR_FTP',
          deliveryType: 'DSR',
          graceTimer: '0',
          hi2Interfaces: [{
            comments: '',
            connectionType: 'PRIMARY',
            destIPAddr: '',
            destPort: '',
            dscp: '0',
            id: expect.stringContaining('.collectionFunctionInterface'),
            name: '',
            ownIPAddr: '',
            ownIPPort: '',
            reqState: 'ACTIVE',
            state: '',
            traceLevel: '0',
            transportType: 'TCP',
            version: 'J25-A',
          }],
          id: '',
          insideNat: false,
          name: '',
          tag: 'CF-JSTD025A',
          timeZone: '',
          type: 'CALEA',
        };
        const template = collectionFunctionTemplates['CF-JSTD025A'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('CF-JSTD025B', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          bufferCC: false,
          cellTowerLocation: false,
          cfGroup: '',
          contact: '',
          deliveryThru: 'TCP_OR_FTP',
          deliveryType: 'DSR',
          graceTimer: '0',
          hi2Interfaces: [{
            comments: '',
            connectionType: 'PRIMARY',
            destIPAddr: '',
            destPort: '',
            dscp: '0',
            id: expect.stringContaining('.collectionFunctionInterface'),
            name: '',
            ownIPAddr: '',
            ownIPPort: '',
            reqState: 'ACTIVE',
            state: '',
            traceLevel: '0',
            transportType: 'TCP',
            version: 'J25-B',
          }],
          hi3Interfaces: [],
          id: '',
          insideNat: false,
          name: '',
          tag: 'CF-JSTD025B',
          timeZone: '',
          type: 'CALEA',
        };
        const template = collectionFunctionTemplates['CF-JSTD025B'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing create all interfaces', () => {
        const expected: CollectionFunctionFormData = {
          bufferCC: false,
          cellTowerLocation: false,
          cfGroup: '',
          contact: '',
          deliveryThru: 'TCP_OR_FTP',
          deliveryType: 'DSR',
          graceTimer: '0',
          hi2Interfaces: [{
            comments: '',
            connectionType: 'PRIMARY',
            destIPAddr: '',
            destPort: '',
            dscp: '0',
            id: expect.stringContaining('.collectionFunctionInterface'),
            name: '',
            ownIPAddr: '',
            ownIPPort: '',
            reqState: 'ACTIVE',
            state: '',
            traceLevel: '0',
            transportType: 'TCP',
            version: 'J25-B',
          }],
          hi3Interfaces: [{
            destIPAddr: '',
            destPort: '15001',
            displayLabel: '',
            dscp: '0',
            filter: '',
            id: expect.stringContaining('.collectionFunctionInterface'),
            name: '',
            ownnetid: '',
            transportType: 'TCP',
          }],
          id: '',
          insideNat: false,
          name: '',
          tag: 'CF-JSTD025B',
          timeZone: '',
          type: 'CALEA',
        };
        const template = collectionFunctionTemplates['CF-JSTD025B'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('CF-ATIS-1000678', () => {
      it('should return a CollectionFunctionFormData instance based in the template', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'T1.678',
          name: '',
          tag: 'CF-ATIS-1000678',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: '2',
            },
          ],
          hi3Interfaces: [],
        };

        const template = collectionFunctionTemplates['CF-ATIS-1000678'];
        expect(createCollectionFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a CollectionFunctionFormData instance based in the template when forcing create all interfaces', () => {
        const expected: CollectionFunctionFormData = {
          id: '',
          deliveryThru: 'TCP_OR_FTP',
          type: 'T1.678',
          name: '',
          tag: 'CF-ATIS-1000678',
          cfGroup: '',
          deliveryType: 'DSR',
          timeZone: '',
          contact: '',
          graceTimer: '0',
          insideNat: false,
          bufferCC: false,
          cellTowerLocation: false,
          hi2Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              dscp: '0',
              ownIPAddr: '',
              ownIPPort: '',
              reqState: 'ACTIVE',
              state: '',
              transportType: 'TCP',
              version: '2',
            },
          ],
          hi3Interfaces: [
            {
              id: expect.stringContaining('.collectionFunctionInterface'),
              name: '',
              version: '2',
              comments: '',
              connectionType: 'PRIMARY',
              destIPAddr: '',
              destPort: '',
              displayLabel: '',
              dscp: '0',
              filter: 'ALL',
              ownIPAddr: '',
              ownIPPort: '',
              transport: 'TCP',
              transportType: 'TCP',
            },
          ],
        };

        const template = collectionFunctionTemplates['CF-ATIS-1000678'];
        expect(createCollectionFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

  });
});
