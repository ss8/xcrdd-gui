import { FormTemplateSection } from 'data/template/template.types';
import { TabProp } from 'shared/multi-tab/MultiTab';
import hideInterfaceSection from './hideInterfaceSection';

describe('hideInterfaceSection()', () => {
  let formTemplateSection:FormTemplateSection | undefined;
  let tabs:TabProp[];
  let isEditing:boolean;

  beforeEach(() => {
    formTemplateSection = {
      metadata: { },
      fields: {
        id: { type: 'text', hide: 'true' },
        name: { type: 'text', hide: 'false' },
      },
    } as FormTemplateSection;

    tabs = [{
      id: ' number | string',
      title: 'title',
      panel: {} as JSX.Element,
    }];
  });

  it('should return false when is not Editing CF but there are tabs', () => {
    isEditing = false;
    expect(hideInterfaceSection(formTemplateSection, isEditing, tabs)).toBeFalsy();
  });

  it('should return false when there are no tabs but is Editing CF', () => {
    isEditing = true;

    expect(hideInterfaceSection(formTemplateSection, isEditing, tabs)).toBeFalsy();
  });

  it('should return true when there are no tabs and is not Editing CF', () => {
    isEditing = false;

    expect(hideInterfaceSection(formTemplateSection, isEditing, [])).toBeTruthy();
  });

  it('should return true when form section is undefined', () => {
    isEditing = true;

    expect(hideInterfaceSection(undefined, isEditing, tabs)).toBeTruthy();
  });

});
