import { CollectionFunction, CollectionFunctionFormData, CollectionFunctionInterface } from 'data/collectionFunction/collectionFunction.types';
import getCollectionFunctionDataToSubmit from './getCollectionFunctionDataToSubmit';

describe('getCollectionFunctionDataToSubmit()', () => {
  let collectionFunction: CollectionFunctionFormData;
  let originalCollectionFunction: CollectionFunction;
  let expectedCollectionFunction: CollectionFunction;
  const userId = 'userId1';
  const userName = 'userName1';

  beforeEach(() => {
    collectionFunction = {
      id: 'id1',
      name: 'new name',
      cfGroup: 'id2',
      deliveryThru: 'TCP_OR_FTP',
      tag: 'CF-3GPP-33108',
      type: 'GPRS',
      bufferCC: false,
      cellTowerLocation: false,
      contact: 'Q29udGFjdCAx',
      deliveryType: 'MAPPED',
      graceTimer: '10',
      insideNat: true,
      timeZone: 'America/New_York',
      hi2Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        comments: 'some comment',
        connectionType: 'FAILOVER',
        destIPAddr: '1.1.1.1',
        destPort: '1',
        dscp: '10',
        keepAliveFreq: '120',
        keepAliveRetries: '12',
        ownIPAddr: '12.12.12.12',
        ownIPPort: '12',
        reqState: 'INACTIVE',
        state: '',
        tpkt: false,
        version: '15.5',
      }],
      hi3Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        comments: 'some comment 2',
        connectionType: 'FAILOVER',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        displayLabel: 'some label',
        dscp: '20',
        keepAliveFreq: '220',
        keepAliveRetries: '22',
        inactivityTimer: '20',
        ownIPAddr: '22.22.22.22',
        ownIPPort: '22',
        tpkt: false,
        version: '15.5',
        useIpdu: false,
      }],
    };

    originalCollectionFunction = {
      id: 'id1',
      name: 'Name 1',
      cfGroup: { id: 'id2', name: '' },
      tag: 'CF-3GPP-33108',
      type: 'GPRS',
      bufferCC: false,
      cellTowerLocation: false,
      contact: { id: 'Q29udGFjdCAx', name: 'Contact 1' },
      deliveryType: 'MAPPED',
      graceTimer: '10',
      insideNat: true,
      timeZone: 'America/New_York',
      hi2Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'another comment' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'keepAliveFreq', value: '120' },
          { key: 'keepAliveRetries', value: '12' },
          { key: 'ownIPAddr', value: '12.12.12.12' },
          { key: 'ownIPPort', value: '12' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'state', value: '' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'some label' },
          { key: 'dscp', value: '20' },
          { key: 'keepAliveFreq', value: '220' },
          { key: 'keepAliveRetries', value: '22' },
          { key: 'inactivityTimer', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
          { key: 'useIpdu', value: 'N' },
        ],
      }],
    };

    expectedCollectionFunction = {
      id: 'id1',
      name: 'new name',
      cfGroup: { id: 'id2', name: '' },
      tag: 'CF-3GPP-33108',
      type: 'GPRS',
      bufferCC: false,
      cellTowerLocation: false,
      contact: { id: 'Q29udGFjdCAx', name: 'Contact 1' },
      deliveryType: 'MAPPED',
      graceTimer: '10',
      insideNat: true,
      timeZone: 'America/New_York',
      hi2Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'dscp', value: '10' },
          { key: 'keepAliveFreq', value: '120' },
          { key: 'keepAliveRetries', value: '12' },
          { key: 'ownIPAddr', value: '12.12.12.12' },
          { key: 'ownIPPort', value: '12' },
          { key: 'reqState', value: 'INACTIVE' },
          { key: 'state', value: '' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
        ],
      }],
      hi3Interfaces: [{
        id: 'id1',
        name: 'name1',
        transportType: 'TCP',
        options: [
          { key: 'comments', value: 'some comment 2' },
          { key: 'connectionType', value: 'FAILOVER' },
          { key: 'destIPAddr', value: '2.2.2.2' },
          { key: 'destPort', value: '2' },
          { key: 'displayLabel', value: 'some label' },
          { key: 'dscp', value: '20' },
          { key: 'keepAliveFreq', value: '220' },
          { key: 'keepAliveRetries', value: '22' },
          { key: 'inactivityTimer', value: '20' },
          { key: 'ownIPAddr', value: '22.22.22.22' },
          { key: 'ownIPPort', value: '22' },
          { key: 'tpkt', value: 'N' },
          { key: 'version', value: '15.5' },
          { key: 'useIpdu', value: 'N' },
        ],
      }],
    };
  });

  describe('when operation is CREATE', () => {
    beforeEach(() => {
      (expectedCollectionFunction.hi2Interfaces as CollectionFunctionInterface[])[0].operation = 'CREATE';
      (expectedCollectionFunction.hi3Interfaces as CollectionFunctionInterface[])[0].operation = 'CREATE';
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getCollectionFunctionDataToSubmit(
          collectionFunction,
          null,
          false,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedCollectionFunction,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getCollectionFunctionDataToSubmit(
          collectionFunction,
          null,
          true,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedCollectionFunction,
        auditDetails: {
          actionType: 'Collection Function Add',
          auditEnabled: true,
          fieldDetails: '[{"field":"id","value":"id1"},{"field":"name","value":"new name"},{"field":"cfGroup","value":{"id":"id2","name":""}},{"field":"tag","value":"CF-3GPP-33108"},{"field":"type","value":"GPRS"},{"field":"bufferCC","value":false},{"field":"cellTowerLocation","value":false},{"field":"contact","value":{"id":"Q29udGFjdCAx","name":"Contact 1"}},{"field":"deliveryType","value":"MAPPED"},{"field":"graceTimer","value":"10"},{"field":"insideNat","value":true},{"field":"timeZone","value":"America/New_York"},{"field":"hi2Interfaces","value":[{"name":"name1","id":"id1","transportType":"TCP","options":[{"key":"comments","value":"some comment"},{"key":"connectionType","value":"FAILOVER"},{"key":"destIPAddr","value":"1.1.1.1"},{"key":"destPort","value":"1"},{"key":"dscp","value":"10"},{"key":"keepAliveFreq","value":"120"},{"key":"keepAliveRetries","value":"12"},{"key":"ownIPAddr","value":"12.12.12.12"},{"key":"ownIPPort","value":"12"},{"key":"reqState","value":"INACTIVE"},{"key":"state","value":""},{"key":"tpkt","value":"N"},{"key":"version","value":"15.5"}]}]},{"field":"hi3Interfaces","value":[{"name":"name1","id":"id1","transportType":"TCP","options":[{"key":"comments","value":"some comment 2"},{"key":"connectionType","value":"FAILOVER"},{"key":"destIPAddr","value":"2.2.2.2"},{"key":"destPort","value":"2"},{"key":"displayLabel","value":"some label"},{"key":"dscp","value":"20"},{"key":"keepAliveFreq","value":"220"},{"key":"keepAliveRetries","value":"22"},{"key":"inactivityTimer","value":"20"},{"key":"ownIPAddr","value":"22.22.22.22"},{"key":"ownIPPort","value":"22"},{"key":"tpkt","value":"N"},{"key":"version","value":"15.5"},{"key":"useIpdu","value":"N"}]}]}]',
          recordName: 'new name',
          service: 'CFs',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });

  describe('when operation is UPDATE', () => {
    beforeEach(() => {
      expectedCollectionFunction.operation = 'UPDATE';
      (expectedCollectionFunction.hi2Interfaces as CollectionFunctionInterface[])[0].operation = 'UPDATE';
      (expectedCollectionFunction.hi3Interfaces as CollectionFunctionInterface[])[0].operation = 'NO_OPERATION';
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getCollectionFunctionDataToSubmit(
          collectionFunction,
          originalCollectionFunction,
          false,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedCollectionFunction,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getCollectionFunctionDataToSubmit(
          collectionFunction,
          originalCollectionFunction,
          true,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedCollectionFunction,
        auditDetails: {
          actionType: 'Collection Function Edit',
          auditEnabled: true,
          fieldDetails: '[{"field":"name","before":"Name 1","after":"new name"},{"field":"hi2Interfaces","before":[{"id":"id1","name":"name1","transportType":"TCP","options":[{"key":"comments","value":"another comment"},{"key":"connectionType","value":"FAILOVER"},{"key":"destIPAddr","value":"1.1.1.1"},{"key":"destPort","value":"1"},{"key":"dscp","value":"10"},{"key":"keepAliveFreq","value":"120"},{"key":"keepAliveRetries","value":"12"},{"key":"ownIPAddr","value":"12.12.12.12"},{"key":"ownIPPort","value":"12"},{"key":"reqState","value":"INACTIVE"},{"key":"state","value":""},{"key":"tpkt","value":"N"},{"key":"version","value":"15.5"}]}],"after":[{"name":"name1","id":"id1","transportType":"TCP","options":[{"key":"comments","value":"some comment"},{"key":"connectionType","value":"FAILOVER"},{"key":"destIPAddr","value":"1.1.1.1"},{"key":"destPort","value":"1"},{"key":"dscp","value":"10"},{"key":"keepAliveFreq","value":"120"},{"key":"keepAliveRetries","value":"12"},{"key":"ownIPAddr","value":"12.12.12.12"},{"key":"ownIPPort","value":"12"},{"key":"reqState","value":"INACTIVE"},{"key":"state","value":""},{"key":"tpkt","value":"N"},{"key":"version","value":"15.5"}]}]}]',
          recordName: 'new name',
          service: 'CFs',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });
});
