import { ValueGetterParams, ITooltipParams } from '@ag-grid-enterprise/all-modules';
import { CollectionFunction } from 'data/collectionFunction/collectionFunction.types';

// eslint-disable-next-line import/prefer-default-export
export function xcipioBehindNatValueGetter(
  { data }: Partial<ValueGetterParams> | Partial<ITooltipParams>,
): string {
  const { insideNat } = data as CollectionFunction;

  return insideNat ? 'yes' : 'no';
}
