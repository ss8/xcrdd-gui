import { FormTemplateSection } from 'data/template/template.types';
import showInterfaceProtocolForm from './showInterfaceProtocolForm';

describe('showInterfaceProtocolForm()', () => {
  it('should return true when at least one field has "hide"="false" ', () => {

    const templateFormSection:FormTemplateSection = {
      metadata: { },
      fields: {
        id: { type: 'text', hide: 'true' },
        name: { type: 'text', hide: 'false' },
      },
    };

    expect(showInterfaceProtocolForm(templateFormSection as FormTemplateSection)).toBeTruthy();
  });

  it('should return true when any fields do not have "hide"="true" ', () => {
    const templateFormSection:FormTemplateSection = {
      metadata: { },
      fields: {
        id: { type: 'text' },
        name: { type: 'text' },
      },
    };

    expect(showInterfaceProtocolForm(templateFormSection as FormTemplateSection)).toBeTruthy();

  });

  it('should return when all field have "hide"="true" ', () => {
    const templateFormSection:FormTemplateSection = {
      metadata: { },
      fields: {
        id: { type: 'text', hide: 'true' },
        name: { type: 'text', hide: 'true' },
      },
    };
    expect(showInterfaceProtocolForm(templateFormSection as FormTemplateSection)).toBeFalsy();
  });
});
