import { CollectionFunctionInterfaceFormData } from 'data/collectionFunction/collectionFunction.types';
import { mockFtpInfos } from '__test__/mockFtpInfos';
import { handleInterfacesFieldUpdate } from './handleInterfacesFieldUpdate';

describe('handleInterfacesFieldUpdate()', () => {
  let interfaceFormDataList: CollectionFunctionInterfaceFormData[];

  beforeEach(() => {
    interfaceFormDataList = [{
      id: 'id1',
      name: 'name1',
      transportType: 'TCP',
      comments: 'some comment',
      connectionType: 'FAILOVER',
      destIPAddr: '1.1.1.1',
      destPort: '1',
      dscp: '10',
      keepAliveFreq: '120',
      keepAliveRetries: '12',
      ownIPAddr: '12.12.12.12',
      ownIPPort: '12',
      reqState: 'INACTIVE',
      state: '',
      tpkt: false,
      version: '3.15.1',
      countryCode: 'US',
      pldEncType: false,
      pldEncKey: '',
    }, {
      id: 'id2',
      name: 'name2',
      transportType: 'TCP',
      comments: 'some comment 2',
      connectionType: 'FAILOVER',
      destIPAddr: '2.2.2.2',
      destPort: '2',
      displayLabel: 'some label',
      dscp: '20',
      keepAliveFreq: '220',
      keepAliveRetries: '22',
      inactivityTimer: '20',
      ownIPAddr: '22.22.22.22',
      ownIPPort: '22',
      tpkt: false,
      version: '3.15.1',
      useIpdu: false,
      countryCode: 'TR',
    }, {
      id: 'id3',
      name: 'name3',
      transportType: 'FTP',
      ftpInfoId: 'ZnRwMQ',
      destIPAddr: '1.1.1.1',
      destPort: '1',
      displayLabel: 'some label',
      userName: 'test-1',
      userPassword: '*****',
      ftpFileSec: '60',
      ftpFileSize: '2048',
      operatorId: '1',
      fileMode: 'CASE',
      destPath: '/test/dir1',
    }];
  });

  it('should return undefined if no interfaces', () => {
    expect(handleInterfacesFieldUpdate(undefined, 'id1', 'destIPAddr', '', mockFtpInfos)).toEqual(undefined);
  });

  it('should return empty array if interface list is empty', () => {
    expect(handleInterfacesFieldUpdate([], 'id1', 'destIPAddr', '', mockFtpInfos)).toEqual([]);
  });

  it('should return the interface list unchanged if no matching interface id', () => {
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'idx', 'destIPAddr', '', mockFtpInfos)).toEqual(interfaceFormDataList);
  });

  it('should set the interface field with expected value', () => {
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'destIPAddr', '11.11.11.11', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        destIPAddr: '11.11.11.11',
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the ftp info data if changing the ftp info id', () => {
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id3', 'ftpInfoId', 'ZnRwMg', mockFtpInfos)).toEqual([
      interfaceFormDataList[0],
      interfaceFormDataList[1],
      {
        ...interfaceFormDataList[2],
        destIPAddr: mockFtpInfos[1].ipAddress,
        destPath: mockFtpInfos[1].destPath,
        destPort: mockFtpInfos[1].port,
        fileMode: mockFtpInfos[1].fileMode,
        ftpInfoId: mockFtpInfos[1].id,
        ftpType: mockFtpInfos[1].ftpType,
        operatorId: mockFtpInfos[1].operatorId,
        userName: mockFtpInfos[1].username,
        ftpFileSize: mockFtpInfos[1].ftpFileSize,
        ftpFileSec: mockFtpInfos[1].ftpFileTransferTimeout,
        userPassword: mockFtpInfos[1].userPassword,
      },
    ]);
  });

  it('should reset the ftp info data if changing the ftp info id to an invalid id', () => {
    interfaceFormDataList[2] = {
      ...interfaceFormDataList[2],
      destIPAddr: mockFtpInfos[1].ipAddress,
      destPath: mockFtpInfos[1].destPath,
      destPort: mockFtpInfos[1].port,
      fileMode: mockFtpInfos[1].fileMode,
      ftpInfoId: mockFtpInfos[1].id,
      operatorId: mockFtpInfos[1].operatorId,
      userName: mockFtpInfos[1].username,
      ftpFileSize: mockFtpInfos[1].ftpFileSize,
      ftpFileSec: mockFtpInfos[1].ftpFileTransferTimeout,
      userPassword: mockFtpInfos[1].userPassword,
    };

    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id3', 'ftpInfoId', '', mockFtpInfos)).toEqual([
      interfaceFormDataList[0],
      interfaceFormDataList[1],
      {
        ...interfaceFormDataList[2],
        destIPAddr: '',
        destPath: '',
        destPort: '',
        fileMode: 'ALLINONE',
        ftpInfoId: '',
        ftpType: 'FTP',
        operatorId: '',
        userName: '',
        ftpFileSize: '',
        ftpFileSec: '',
        userPassword: '',
      },
    ]);
  });

  it('should set the country to NONE when changing the version', () => {
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'version', '3.15.1', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'NONE',
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the fields related to country when changing the version', () => {
    interfaceFormDataList[0].pldEncKey = '123';
    interfaceFormDataList[0].secinfoid = 'abc';
    interfaceFormDataList[0].countryCode = 'NL';
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'version', '3.15.1', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'NONE',
        pldEncKey: '',
        secinfoid: '',
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the country to NONE when changing the version', () => {
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id2', 'version', '3.15.1', mockFtpInfos)).toEqual([
      interfaceFormDataList[0],
      {
        ...interfaceFormDataList[1],
        countryCode: 'NONE',
      },
      interfaceFormDataList[2],
    ]);
  });

  it('should set the pldEncType to true if version is not 2.5.1 when changing country to TR', () => {
    interfaceFormDataList[0].pldEncKey = '123';
    interfaceFormDataList[0].secinfoid = 'abc';
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'countryCode', 'TR', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'TR',
        pldEncType: true,
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the pldEncType to true when changing country to NL', () => {
    interfaceFormDataList[0].pldEncKey = '123';
    interfaceFormDataList[0].secinfoid = 'abc';
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'countryCode', 'NL', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'NL',
        pldEncType: true,
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the pldEncType to false and pldEncKey to empty if version is 2.5.1 when changing country to TR', () => {
    interfaceFormDataList[0].pldEncKey = '123';
    interfaceFormDataList[0].secinfoid = 'abc';
    interfaceFormDataList[0].version = '2.5.1';
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'countryCode', 'TR', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'TR',
        pldEncType: false,
        pldEncKey: '',
        secinfoid: '',
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });

  it('should set the pldEncType to false and pldEncKey to empty when changing country to US', () => {
    interfaceFormDataList[0].pldEncKey = '123';
    interfaceFormDataList[0].secinfoid = 'abc';
    expect(handleInterfacesFieldUpdate(interfaceFormDataList, 'id1', 'countryCode', 'US', mockFtpInfos)).toEqual([
      {
        ...interfaceFormDataList[0],
        countryCode: 'US',
        pldEncType: false,
        pldEncKey: '',
        secinfoid: '',
      },
      interfaceFormDataList[1],
      interfaceFormDataList[2],
    ]);
  });
});
