import { ColGroupDef } from '@ag-grid-community/core';
import { CollectionFunction, HI2Interface, HI3Interface } from 'data/collectionFunction/collectionFunction.types';
import { stateValueFormatter } from 'utils/valueFormatters';
import { getFieldFromFirstInterfaceNameValueGetter } from 'utils/valueGetters';
import getFieldFromPropertyPathValueGetter from 'utils/valueGetters/getFieldFromPropertyPathValueGetter';
import getFieldOptionFromFirstInterfaceNameValueGetter from 'utils/valueGetters/getFieldOptionFromFirstInterfaceNameValueGetter';
import getNumberOfItemsFromFieldNameValueGetter from 'utils/valueGetters/getNumberOfItemsFromFieldNameValueGetter';
import { xcipioBehindNatValueGetter } from './utils/collectionFunctionGridValueGetters';

export enum CollectionFunctionMenuActionsType {
  Copy = 'copy',
  Delete = 'delete',
  Edit = 'edit',
  View = 'view'
}

export const collectionFunctionsColumnDefs: ColGroupDef[] = [
  {
    headerName: '',
    children: [
      {
        headerName: 'Name',
        field: 'name',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'name',
        width: 198,
        lockVisible: true,
      },
      {
        headerName: 'Handover Protocol',
        field: 'tagName',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'tagName',
        minWidth: 186,
      },
      {
        headerName: 'LEA',
        field: 'cfGroup',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 186,
        tooltipValueGetter: getFieldFromPropertyPathValueGetter(['name']),
        valueGetter: getFieldFromPropertyPathValueGetter(['name']),
      },
      {
        headerName: 'Xcipio behind NAT',
        field: 'insideNat',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 143,
        tooltipValueGetter: xcipioBehindNatValueGetter,
        valueGetter: xcipioBehindNatValueGetter,
      },
      {
        headerName: 'CF Id',
        hide: true,
        field: 'id',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'id',
      },
      {
        headerName: '# of IRI',
        hide: true,
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        valueGetter: getNumberOfItemsFromFieldNameValueGetter<CollectionFunction, HI3Interface>('hi2Interfaces'),
        tooltipValueGetter: getNumberOfItemsFromFieldNameValueGetter<CollectionFunction, HI3Interface>('hi2Interfaces'),
      },
      {
        headerName: '# of CC',
        hide: true,
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipValueGetter: getNumberOfItemsFromFieldNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
        valueGetter: getNumberOfItemsFromFieldNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
      },
    ],
  },
  {
    headerName: 'Intercept Related Information - Interface 1',
    children: [
      {
        headerName: 'Transport Protocol',
        field: 'transportType',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 147,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
      },
      {
        headerName: 'IP Address',
        field: 'destIPAddr',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 147,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
      },
      {
        headerName: 'Port',
        field: 'destPort',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 96,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
      },
      {
        headerName: 'Current State',
        field: 'state',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 143,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueFormatter: stateValueFormatter,

      },
      {
        headerName: 'Configured State',
        field: 'reqState',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 151,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI2Interface>('hi2Interfaces'),
        valueFormatter: stateValueFormatter,
      },
    ],
  },
  {
    headerName: 'Communication Content - Interface 1',
    children: [
      {
        headerName: 'Transport Protocol',
        field: 'transportType',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 147,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
      },
      {
        headerName: 'IP Address',
        field: 'destIPAddr',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 147,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
      },
      {
        headerName: 'Port',
        field: 'destPort',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        minWidth: 96,
        tooltipValueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
        valueGetter: getFieldOptionFromFirstInterfaceNameValueGetter<CollectionFunction, HI3Interface>('hi3Interfaces'),
      },
    ],
  },
];
