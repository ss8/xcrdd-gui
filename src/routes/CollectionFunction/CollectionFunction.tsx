import React, { FunctionComponent, Fragment } from 'react';
import { Route } from 'react-router-dom';
import { CF_ROUTE } from '../../constants';
import CollectionFunctionList from './containers/CollectionFunctionList';
import CollectionFunctionCreate from './containers/CollectionFunctionCreate';
import CollectionFunctionEdit from './containers/CollectionFunctionEdit';
import CollectionFunctionView from './containers/CollectionFunctionView';
import CollectionFunctionCopy from './containers/CollectionFunctionCopy';

interface Props {}

const CollectionFunction: FunctionComponent<Props> = () => {
  return (
    <Fragment>
      <Route exact path={CF_ROUTE}>
        <CollectionFunctionList />
      </Route>
      <Route exact path={`${CF_ROUTE}/create`}>
        <CollectionFunctionCreate />
      </Route>
      <Route exact path={`${CF_ROUTE}/edit`}>
        <CollectionFunctionEdit />
      </Route>
      <Route exact path={`${CF_ROUTE}/view`}>
        <CollectionFunctionView />
      </Route>
      <Route exact path={`${CF_ROUTE}/copy`}>
        <CollectionFunctionCopy />
      </Route>
    </Fragment>
  );
};

export default CollectionFunction;
