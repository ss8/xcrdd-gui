import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import store from 'data/store';
import Distributor from './Distributor';
import DistributorList from './containers/DistributorList';
import { SYSTEM_SETTINGS_ROUTE } from '../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<Distributor />', () => {
  let wrapper: ReactWrapper;

  beforeEach(() => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}/distributors`]}>
          <Distributor />
        </MemoryRouter>
      </Provider>
    ));

    axiosMock.onAny().reply(200, { data: [] });
  });

  it('should render the list for the /afs path', () => {
    expect(wrapper.find(DistributorList)).toHaveLength(1);
  });
});
