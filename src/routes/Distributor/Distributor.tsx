import React, { FunctionComponent, Fragment } from 'react';
import { Route } from 'react-router-dom';
import DistributorList from './containers/DistributorList';
import DistributorConfigCreate from './containers/DistributorConfigCreate';
import DistributorConfigEdit from './containers/DistributorConfigEdit';
import DistributorConfigView from './containers/DistributorConfigView';
import DistributorConfigCopy from './containers/DistributorConfigCopy';
import { SYSTEM_SETTINGS_ROUTE, DISTRIBUTORS_ROUTE } from '../../constants';

interface Props {
}

const Distributor: FunctionComponent<Props> = () => {
  return (
    <Fragment>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}`}>
        <DistributorList />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/create`}>
        <DistributorConfigCreate />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/edit`}>
        <DistributorConfigEdit />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/view`}>
        <DistributorConfigView />
      </Route>
      <Route exact path={`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/copy`}>
        <DistributorConfigCopy />
      </Route>
    </Fragment>
  );
};

export default Distributor;
