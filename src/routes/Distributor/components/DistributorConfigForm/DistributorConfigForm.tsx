import React, {
  useState, useImperativeHandle, useEffect, useRef, forwardRef,
} from 'react';
import { FormTemplate, FormTemplateSection, FormTemplateValidator } from 'data/template/template.types';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { DistributorData } from 'data/distributor/distributor.types';
import { NodeData } from 'data/nodes/nodes.types';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { showError } from 'shared/toaster';
import cloneDeep from 'lodash.clonedeep';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import Form, { ChangedField } from 'shared/form';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import { validateCurrentIpPortCombination, validateIpPortCombinations } from 'utils/validators/ipPortCombinationValidators';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import MainForm from './Main';
import { Ref as MainRef } from './Main/MainForm';
import { ListenersRef } from './Listeners/Listeners';
import { DestinationsRef } from './Destinations/Destinations';
import Listeners from './Listeners';
import Destinations from './Destinations';
import * as adapters from '../../utils/distributorConfigTemplate.adapter';
import { handleInterfacesFieldUpdate } from '../../utils/distributorConfigInterfaceTabFieldUpdate';
import './distributor-config-form.scss';
import newDistributorConfiguration from '../../newDistributorConfiguration';

export type DistributorConfigFormRef = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => DistributorConfiguration
}

type Props = {
  template: FormTemplate,
  distributorConfig: DistributorConfiguration,
  distributors: DistributorData,
  nodes: NodeData[],
  securityInfos:SecurityInfo[],
  isEditing: boolean,
  tlsProxies: TlsProxy[],
  availableAccessFunction: SupportedFeaturesAccessFunctions,
  enabledAccessFunction: SupportedFeaturesAccessFunctions,
}

const DistributorConfigForm = forwardRef<DistributorConfigFormRef, Props>(({
  isEditing,
  template,
  distributorConfig,
  distributors,
  nodes,
  securityInfos,
  tlsProxies,
  availableAccessFunction,
  enabledAccessFunction,
}: Props, ref) => {
  const [formData, setFormData] = useState(distributorConfig);
  const [nodeNameIpAddressOptions, setNodeNameIpAddressOptions] = useState<TextSuggestType[]>([]);
  const [destinationsTemplate, setDestinationsTemplate] = useState<FormTemplateSection>(
    template.destinations as FormTemplateSection,
  );
  const mainFormRef = useRef<MainRef>(null);
  const listenerRefs = useRef<ListenersRef>(null);
  const destinationRefs = useRef<DestinationsRef>(null);

  useEffect(() => {
    setFormData({ ...distributorConfig });
  }, [distributorConfig]);

  useEffect(() => {
    setNodeNameIpAddressOptions(adapters.adaptNodeNameIpAddressOptions(distributorConfig.nodeName || '', nodes));
  }, [distributorConfig, nodes]);

  useEffect(() => {
    const newDestinationsTemplate = adapters.adaptDestinationsIpAddressPortTemplate(
      distributorConfig.moduleName,
      template.destinations as FormTemplateSection,
    );

    setDestinationsTemplate(newDestinationsTemplate);
  }, [template, distributorConfig]);

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isMainFormValid = mainFormRef.current?.isValid(forceValidation, forceTouched) ?? true;

    const listeners = listenerRefs.current?.getValues();
    const destinations = destinationRefs.current?.getValues();

    const isListenersValid = listenerRefs.current?.isValid(forceValidation, forceTouched) ?? true;
    const isDestinationsValid = destinationRefs.current?.isValid(forceValidation, forceTouched) ?? true;

    const isIpPortCombinationsValid = validateIpPortCombinations(listeners, destinations);
    if (!isIpPortCombinationsValid) {
      showError('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.');
    }

    return isMainFormValid && isListenersValid && isDestinationsValid && isIpPortCombinationsValid;
  };

  const getValues = (): DistributorConfiguration => {
    const values = mainFormRef.current?.getValues();
    if (values != null) {
      (values as DistributorConfiguration).listeners = listenerRefs.current?.getValues() ?? [];
      (values as DistributorConfiguration).destinations = destinationRefs.current?.getValues() ?? [];
    }
    return values as DistributorConfiguration;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const customValidator = (
    _propFields?: unknown,
    _stateFields?: unknown,
    _value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
    form?: Form | undefined,
  ) => {
    const { listeners, destinations } = getValues();

    const valid = validateCurrentIpPortCombination(form?.getValues(), listeners, destinations);
    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  };

  const customValidationFunctionMap = {
    ipPortCombinationValidator: customValidator,
  };

  const mainFormFieldUpdateHandler = ({ name, value }: ChangedField) => {
    const newFormData = cloneDeep(getValues());
    newFormData[name as keyof DistributorConfiguration] = value;

    if (name === 'moduleName') {
      newFormData.afType = '';
      const newDestinationsTemplate = adapters.adaptDestinationsIpAddressPortTemplate(
        newFormData.moduleName,
        template.destinations as FormTemplateSection,
      );
      setDestinationsTemplate(newDestinationsTemplate);

      if (newFormData.destinations.length === 0) {
        newFormData.destinations = cloneDeep(newDistributorConfiguration.destinations);
      }

    } else if (name === 'instanceId') {
      newFormData.afType = '';

    } else if (name === 'nodeName') {
      setNodeNameIpAddressOptions(adapters.adaptNodeNameIpAddressOptions(name, nodes));
    }

    handleInterfacesFieldUpdate(newFormData, name);

    setFormData(newFormData);
  };

  let showDestinations = true;
  if (formData.moduleName === 'RADAFIM') {
    showDestinations = false;
  }

  return (
    <div>
      <MainForm
        ref={mainFormRef}
        formData={formData}
        templateSection={template.main as FormTemplateSection}
        distributors={distributors}
        nodes={nodes}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
        fieldUpdateHandler={mainFormFieldUpdateHandler}
      />
      <Listeners
        ref={listenerRefs}
        distributorConfiguration={formData}
        type={formData.afType}
        moduleName={formData.moduleName}
        listeners={formData.listeners}
        templateSection={template.listeners as FormTemplateSection}
        distributors={distributors}
        securityInfos={securityInfos}
        isEditing={isEditing}
        customValidationFunctionMap={customValidationFunctionMap}
        ipAddressOptions={nodeNameIpAddressOptions}
      />
      {showDestinations && (
        <Destinations
          ref={destinationRefs}
          templateSection={destinationsTemplate as FormTemplateSection}
          destinations={formData.destinations}
          securityInfos={securityInfos}
          isEditing={isEditing}
          type={formData.afType}
          moduleName={formData.moduleName}
          distributors={distributors}
          tlsProxies={tlsProxies}
          customValidationFunctionMap={customValidationFunctionMap}
        />
      )}
    </div>
  );
});

export default DistributorConfigForm;
