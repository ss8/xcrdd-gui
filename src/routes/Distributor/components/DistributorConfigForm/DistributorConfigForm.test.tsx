import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getAllByTestId,
  getByDisplayValue,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { getByDataTest, getByName } from '__test__/customQueries';
import { mockDistributors } from '__test__/mockDistributors';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import DistributorConfigForm, { DistributorConfigFormRef } from './DistributorConfigForm';
import { distributorTemplate } from '../../templates/distributorTemplate';

describe('<DistributorConfigForm />', () => {
  let distributorConfig: DistributorConfiguration;
  const availableAccessFunction = mockSupportedFeatures.config.accessFunctions;
  const enabledAccessFunction = mockSupportedFeatures.config.accessFunctions;

  beforeEach(() => {
    distributorConfig = {
      id: 'ID_TO_GET',
      name: 'DC Name to edit',
      moduleName: 'ERKUPFX3IM',
      instanceId: '1',
      afType: 'ERK_UPF',
      nodeName: 'node-1',
      listeners: [
        {
          id: '',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '',
          port: '',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        },
      ],
      destinations: [
        {
          id: '',
          ipAddress: '',
          port: '',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
        },
      ],
    };
  });

  it('should render the Main form fields as expected when there is no nodes', () => {
    distributorConfig.moduleName = '';
    distributorConfig.instanceId = '';
    distributorConfig.afType = '';
    distributorConfig.nodeName = '';

    const { container } = render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    expect(getByText(container, 'Configuration Name:')).toBeVisible();
    expect(getByDataTest(container, 'input-name')).toBeEnabled();
    expect(getByText(container, 'Module Name:')).toBeVisible();
    expect(getByDataTest(container, 'input-moduleName')).toBeEnabled();
    expect(getByText(container, 'Instance ID:')).toBeVisible();
    expect(getByDataTest(container, 'input-instanceId')).toBeDisabled();
    expect(getByText(container, 'AF Type:')).toBeVisible();
    expect(getByDataTest(container, 'input-afType')).toBeDisabled();
    expect(getByText(container, 'Xcipio Node:')).toBeVisible();
    expect(getByDataTest(container, 'input-nodeName')).toBeDisabled();
  });

  it('should enable the Instance ID field after selecting a Module Name', () => {
    distributorConfig.moduleName = '';
    distributorConfig.instanceId = '';
    distributorConfig.afType = '';
    distributorConfig.nodeName = '';

    const { container } = render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-moduleName'), { target: { value: 'ERKUPFX3IM' } });

    expect(getByText(container, 'Instance ID:')).toBeVisible();
    expect(getByDataTest(container, 'input-instanceId')).toBeEnabled();
    expect(getByText(container, 'AF Type:')).toBeVisible();
    expect(getByDataTest(container, 'input-afType')).toBeDisabled();
  });

  it('should enable the AF Type field after selecting an Instance ID', () => {
    distributorConfig.moduleName = '';
    distributorConfig.instanceId = '';
    distributorConfig.afType = '';
    distributorConfig.nodeName = '';

    const { container } = render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-moduleName'), { target: { value: 'ERKUPFX3IM' } });
    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: '1' } });

    expect(getByText(container, 'AF Type:')).toBeVisible();
    expect(getByDataTest(container, 'input-afType')).toBeEnabled();
  });

  it('should reset the Instance ID and AF Type when changing the Module Name', () => {
    distributorConfig.instanceId = '2';

    const { container } = render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-moduleName'), { target: { value: 'ERKSMFX2IM' } });

    expect(getByText(container, 'Instance ID:')).toBeVisible();
    expect(getByDisplayValue(container, 'Select an instance ID')).toBeEnabled();
    expect(getByText(container, 'AF Type:')).toBeVisible();
    expect(getByDisplayValue(container, 'Select an AF type')).toBeDisabled();
  });

  it('should reset the AF Type when changing the Instance ID', () => {
    const { container } = render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: '1' } });

    expect(getByText(container, 'AF Type:')).toBeVisible();
    expect(getByDisplayValue(container, 'Select an AF type')).toBeEnabled();
  });

  it('should not render add/remove Listener/Destination if not in edit mode', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing={false}
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should render the name, listeners and destinations', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    expect(screen.getByText('Configuration Name:')).toBeVisible();
    expect(screen.getByText('Module Name:')).toBeVisible();
    expect(screen.getByText('Instance ID:')).toBeVisible();
    expect(screen.getByText('AF Type:')).toBeVisible();

    expect(screen.getByTestId('DistributorConfigListeners')).toBeVisible();
    expect(screen.getByText('Listener 1')).toBeVisible();

    expect(screen.getByTestId('DistributorConfigListeners')).toBeVisible();
    expect(screen.getByText('Destination 1')).toBeVisible();
  });

  it('should render the Listener form as expected', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listener Type:')).toBeVisible();
    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Sync Header:')).toBeVisible();
    expect(getByText(tab, 'Number Of Parsers:')).toBeVisible();
  });

  it('should render the new Listener form as expected', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));

    expect(screen.getByText('Listener 2')).toBeVisible();

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listener Type:')).toBeVisible();
    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Sync Header:')).toBeVisible();
    expect(getByText(tab, 'Number Of Parsers:')).toBeVisible();
  });

  it('should render the Destination form as expected', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'Number of Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Destination Protocol:')).toBeVisible();
  });

  it('should render the new Destination form as expected', () => {
    render(
      <DistributorConfigForm
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));

    expect(screen.getByText('Destination 2')).toBeVisible();

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'Number of Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Destination Protocol:')).toBeVisible();
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return false in isValid if form has duplicated ip:port and display error message', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    const { container } = render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    const listeners = screen.getByTestId('DistributorConfigListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '1' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });

    await waitFor(() => expect(screen.getByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')).toBeVisible());
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(2);
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    const { container } = render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });
    const listeners = screen.getByTestId('DistributorConfigListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '2' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should clean up tab error indication after removing a tab', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(listeners.querySelectorAll('.multi-tab-has-error')).toHaveLength(2));

    fireEvent.click(getAllByTestId(listeners, 'MultiTabRemoveIcon')[0]);

    await waitFor(() => expect(listeners.querySelectorAll('.multi-tab-has-error')).toHaveLength(0));
  });

  it('should return the form values in getValues for multiple listeners and destinations', async () => {
    const formRef = createRef<DistributorConfigFormRef>();

    const { container } = render(
      <DistributorConfigForm
        ref={formRef}
        template={distributorTemplate}
        distributorConfig={distributorConfig}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        isEditing
        nodes={[]}
        tlsProxies={[]}
        availableAccessFunction={availableAccessFunction}
        enabledAccessFunction={enabledAccessFunction}
      />,
    );

    fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: 'Name 1' } });

    const listeners = screen.getByTestId('DistributorConfigListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    fireEvent.click(getByText(listenerTab, 'Advanced Settings'));
    fireEvent.change(getByDataTest(listenerTab, 'input-keepaliveTimeout'), { target: { value: '10' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-maxConcurrentConnection'), { target: { value: '11' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-numberOfParsers'), { target: { value: '12' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-secInfoId'), { target: { value: 'Tk9ORQ' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-transport'), { target: { value: 'UDP' } });

    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));
    expect(screen.getByText('Listener 2')).toBeVisible();

    const listenerTab2 = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab2.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab2, 'input-port'), { target: { value: '11' } });

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '2' } });

    fireEvent.click(getByText(destinationTab, 'Advanced Settings'));
    fireEvent.change(getByDataTest(destinationTab, 'input-keepaliveInterval'), { target: { value: '20' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-keepaliveRetries'), { target: { value: '21' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-numOfConnections'), { target: { value: '22' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-secInfoId'), { target: { value: 'Tk9ORQ' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-transport'), { target: { value: 'UDP' } });
    fireEvent.change(getByDataTest(destinationTab, 'input-protocol'), { target: { value: 'PASSTHRU_W_SX3HDR' } });

    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));
    expect(screen.getByText('Destination 2')).toBeVisible();

    const destinationTab2 = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab2, 'ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByName(destinationTab2, 'port'), { target: { value: '22' } });

    expect(formRef.current?.getValues()).toEqual({
      id: 'ID_TO_GET',
      name: 'Name 1',
      moduleName: 'ERKUPFX3IM',
      instanceId: '1',
      afType: 'ERK_UPF',
      nodeName: 'node-1',
      listeners: [
        {
          id: '',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '1.1.1.1',
          port: '1',
          keepaliveTimeout: '10',
          maxConcurrentConnection: '11',
          secInfoId: 'Tk9ORQ',
          transport: 'UDP',
          requiredState: 'ACTIVE',
          numberOfParsers: '12',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        }, {
          id: '',
          afProtocol: '',
          ipAddress: '1.1.1.1',
          port: '11',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          numberOfParsers: '2',
          synchronizationHeader: 'NONE',
          afWhitelist: [],
          operation: 'CREATE',
        },
      ],
      destinations: [
        {
          id: '',
          ipAddress: '2.2.2.2',
          port: '2',
          keepaliveInterval: '20',
          keepaliveRetries: '21',
          numOfConnections: '22',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
        }, {
          id: '',
          ipAddress: '2.2.2.2',
          port: '22',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: 'Tk9ORQ',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
          operation: 'CREATE',
        },
      ],
    });
  });
});
