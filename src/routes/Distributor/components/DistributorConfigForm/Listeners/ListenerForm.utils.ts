import cloneDeep from 'lodash.clonedeep';
import { DistributorConfigurationListener, DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { toDistributorConfigurationListenerWhiteList } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { DistributorData } from 'data/distributor/distributor.types';

export const updateWhiteListIps = (
  distributorConfigListener: DistributorConfigurationListener,
  newAFIpsList: DistributorConfigurationListenerAfWhiteList[],
): DistributorConfigurationListener => {
  const clonedDistributorConfigListener = cloneDeep(distributorConfigListener);
  return {
    ...clonedDistributorConfigListener,
    afWhitelist: newAFIpsList.map(toDistributorConfigurationListenerWhiteList),
  };
};

export function getAfTagsToProtocolMapKey(type:string, moduleName:string, distributors:DistributorData): string {
  let key = `${type}#${moduleName}`;
  if (!distributors?.afTagsToProtocolMap[key]) {
    key = type;
  }
  return key;
}

export default updateWhiteListIps;
