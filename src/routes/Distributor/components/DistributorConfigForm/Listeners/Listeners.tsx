import React, {
  FunctionComponent, useState, useEffect, forwardRef, createRef, useImperativeHandle, useRef, Fragment,
} from 'react';
import MultiTab from 'shared/multi-tab/MultiTab';
import ModalDialog from 'shared/modal/dialog';
import { CustomValidationFunction } from 'shared/form';
import { DistributorConfigurationListener, DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import cloneDeep from 'lodash.clonedeep';
import newDistributorConfiguration from 'routes/Distributor/newDistributorConfiguration';
import { DistributorData } from 'data/distributor/distributor.types';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FormTemplateSection } from 'data/template/template.types';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import ListenerForm, { Ref as ListenerRef } from './ListenerForm';
import { getAfTagsToProtocolMapKey } from './ListenerForm.utils';

const newListener: DistributorConfigurationListener = cloneDeep(newDistributorConfiguration.listeners[0]);

type Props = {
  isEditing: boolean,
  type: string,
  moduleName: string,
  templateSection: FormTemplateSection,
  listeners: DistributorConfigurationListener[],
  distributors: DistributorData,
  securityInfos: SecurityInfo[]
  customValidationFunctionMap: { [key: string]: CustomValidationFunction },
  ipAddressOptions: TextSuggestType[],
  distributorConfiguration: DistributorConfiguration,
};

export type ListenersRef = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean,
  getValues: () => DistributorConfigurationListener[]
}

const Listeners = forwardRef<ListenersRef, Props>(({
  listeners,
  templateSection,
  distributors,
  type,
  moduleName,
  securityInfos,
  isEditing,
  customValidationFunctionMap,
  ipAddressOptions,
  distributorConfiguration,
}: Props, ref) => {
  const [showRemoveListenerConfirmation, setShowRemoveListenerConfirmation] = useState<boolean>();
  const [confirmActionDialogCustomComponent, setConfirmActionDialogCustomComponent] =
    useState<FunctionComponent>(() => null);
  const [selectedListenerToRemove, setSelectedListenerToRemove] = useState<number>();
  const [listenerList, setListenerList] = useState<DistributorConfigurationListener[]>(listeners);
  const [listenerTypes, setListenerTypes] = useState<string[]>([]);
  const [invalidTabs, setInvalidTabs] = useState<boolean[]>([]);
  const deletedListeners = useRef<DistributorConfigurationListener[]>([]);
  const listenerRefs = useRef<React.RefObject<ListenerRef>[]>([]);

  useEffect(() => {
    setListenerList(listeners.slice());
  }, [listeners]);

  useEffect(() => {
    const key = getAfTagsToProtocolMapKey(type, moduleName, distributors);
    setListenerTypes(distributors?.afTagsToProtocolMap[key]?.afProtocols ?? []);
  }, [type, moduleName]);

  const isValid = (forceValidation = false, forceTouched = true) => {
    const validListeners = listenerRefs.current
      .filter((formRef) => !!formRef.current)
      .map((formRef) => formRef.current?.isValid(forceValidation, forceTouched) ?? true);

    const allListenersValid = validListeners.every((valid) => valid === true);
    setInvalidTabs(validListeners.map((entry) => !entry));
    return allListenersValid;
  };

  const getValues = () => {
    const formListeners: DistributorConfigurationListener[] = getCurrentListeners();
    const combinedListeners = formListeners.concat(deletedListeners.current);
    return combinedListeners;
  };

  const getCurrentListeners = () => {
    return (listenerRefs.current.map((listenerRef: React.RefObject<ListenerRef>) => {
      return listenerRef.current?.getValues();
    })).filter((listenersRefValues) => !!listenersRefValues) as DistributorConfigurationListener[];
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const getTabs = () => {
    listenerRefs.current = [];
    return (listenerList?.map((listener: DistributorConfigurationListener, index: number) => {
      const listenerRef = createRef<ListenerRef>();
      listenerRefs.current.push(listenerRef);
      return {
        id: index,
        title: `Listener ${index + 1}`,
        hasErrors: invalidTabs[index] ?? false,
        panel: <ListenerForm
          ref={listenerRef}
          templateSection={templateSection}
          listener={listener}
          listenerTypes={listenerTypes}
          securityInfos={securityInfos}
          distributors={distributors}
          customValidationFunctionMap={customValidationFunctionMap}
          ipAddressOptions={ipAddressOptions}
          distributorConfiguration={distributorConfiguration}
          isEditing={isEditing}
          afType={type}
          moduleName={moduleName}
        />,
      };
    }));
  };

  const removeTab = (id: number | string) => {
    const newlistenerList = getCurrentListeners();
    if (newlistenerList.length < 2) return;
    if (newlistenerList[id as number].afWhitelist?.length > 0) {
      setSelectedListenerToRemove(id as number);
      setShowRemoveListenerConfirmation(true);
      setConfirmActionDialogCustomComponent(() => () => (
        <Fragment>
          <span>This listener</span>&#39;
          <span>s Allowed IP Address list is not empty.
            Deleting the listener may cause AFs not to be able to connect.
          </span>
          <br />
          Are you sure you want to delete this listener?
        </Fragment>
      ));
    } else {
      removeListenerAt(id as number);
    }
  };

  const removeListenerAt = (id: number | undefined) => {
    const newlistenerList = getCurrentListeners();
    if (newlistenerList.length < 2 || id === undefined) return;
    const deletedListener = newlistenerList.splice(id, 1)[0];
    addToDeletedListenerList(deletedListener);
    setListenerList(newlistenerList);
    setInvalidTabs([]);
    setSelectedListenerToRemove(undefined);
  };

  const addToDeletedListenerList = (deletedListener: DistributorConfigurationListener) => {
    if (deletedListener && deletedListener.id !== '') {
      deletedListener.operation = 'DELETE';
      deletedListeners.current.push(deletedListener);
    }
  };

  const addTab = () => {
    const newlistenerList = getCurrentListeners();
    newlistenerList.push(cloneDeep(newListener));
    setListenerList(newlistenerList);
  };

  const handleConfirmRemoveListener = () => {
    removeListenerAt(selectedListenerToRemove);
  };

  const handleCancelRemoveListener = () => {
    setSelectedListenerToRemove(undefined);
    setShowRemoveListenerConfirmation(false);
  };

  return (
    <div data-testid="DistributorConfigListeners">
      <h3>Listeners</h3>
      <div className="multi-tab-form-section">
        <MultiTab
          tabContainerId="DistributorConfigListenersTab"
          tabs={getTabs()}
          removeTab={removeTab}
          addTab={addTab}
          maxNumberOfTabs={10}
          isEditing={isEditing}
        />
      </div>
      <ModalDialog
        isOpen={!!showRemoveListenerConfirmation}
        title="Delete SS8 Distributor Listener"
        customComponent={confirmActionDialogCustomComponent}
        actionText="Yes, delete it"
        onSubmit={handleConfirmRemoveListener}
        onClose={handleCancelRemoveListener}
      />
    </div>
  );
});

export default Listeners;
