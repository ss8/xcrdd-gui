import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getAllByTestId,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { FormTemplateSection } from 'data/template/template.types';
import { getByDataTest } from '__test__/customQueries';
import { mockDistributors } from '__test__/mockDistributors';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import Listeners, { ListenersRef } from './Listeners';
import { distributorTemplate } from '../../../templates/distributorTemplate';

describe('<Listeners />', () => {
  let distributorConfig: DistributorConfiguration;
  let ipAddressOptions: TextSuggestType[];

  beforeEach(() => {
    distributorConfig = {
      id: 'ID_TO_GET',
      name: 'DC Name to edit',
      moduleName: 'ERKUPFX3IM',
      instanceId: '1',
      afType: 'ERK_UPF',
      listeners: [
        {
          id: '',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '',
          port: '',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [
            {
              afAddress: '10.0.0.1',
              afId: 'eEFGXzEyMTU5NDIxODk',
              afName: 'ERK_UPF_1',
            },
            {
              afAddress: '10.0.0.1',
              afId: 'eEFGXzEyMTU5NDIxODk',
              afName: 'ERK_UPF_2',
            },
            {
              afAddress: '10.0.0.1',
              afId: 'eEFGXzEyMTU5NDIxODk',
              afName: 'ERK_UPF_3',
            },
          ],
        },
      ],
      destinations: [
        {
          id: '',
          ipAddress: '2.2.2.2',
          port: '2',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
        },
      ],
    };

    ipAddressOptions = [
      { label: '1.1.1.1', value: '1.1.1.1' },
      { label: '11.11.11.11', value: '11.11.11.11' },
      { label: '111.111.111.111', value: '111.111.111.111' },
    ];
  });

  it('should not render add/remove Listener/Destination if not in edit mode', () => {
    render(
      <Listeners
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing={false}
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should render the listeners', () => {
    render(
      <Listeners
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    expect(screen.getByTestId('DistributorConfigListeners')).toBeVisible();
    expect(screen.getByText('Listener 1')).toBeVisible();
  });

  it('should render the Listener form as expected', () => {
    render(
      <Listeners
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listener Type:')).toBeVisible();
    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Sync Header:')).toBeVisible();
    expect(getByText(tab, 'Number Of Parsers:')).toBeVisible();
  });

  it('should render the new Listener form as expected', () => {
    render(
      <Listeners
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));

    expect(screen.getByText('Listener 2')).toBeVisible();

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'Listener Type:')).toBeVisible();
    expect(getByText(tab, 'Listening IP Address:')).toBeVisible();
    expect(getByText(tab, 'Listening Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Timeout (Sec):')).toBeVisible();
    expect(getByText(tab, 'Maximum Concurrent Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Sync Header:')).toBeVisible();
    expect(getByText(tab, 'Number Of Parsers:')).toBeVisible();
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return false in isValid if form has duplicated ip:port and display error message', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: false, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '2.2.2.2' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '2' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });

    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(1);
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const listenerTab = getByRole(listeners, 'tabpanel');
    fireEvent.change(listenerTab.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab, 'input-port'), { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should clean up tab error indication after removing a tab', async () => {
    const formRef = createRef<ListenersRef>();

    render(
      <Listeners
        ref={formRef}
        distributorConfiguration={distributorConfig}
        listeners={distributorConfig.listeners}
        templateSection={distributorTemplate.listeners as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        ipAddressOptions={ipAddressOptions}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const listeners = screen.getByTestId('DistributorConfigListeners');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(listeners.querySelectorAll('.multi-tab-has-error')).toHaveLength(2));

    fireEvent.click(getAllByTestId(listeners, 'MultiTabRemoveIcon')[1]);

    await waitFor(() => expect(listeners.querySelectorAll('.multi-tab-has-error')).toHaveLength(0));
  });
});
