import { mockDistributors } from '__test__/mockDistributors';
import { getAfTagsToProtocolMapKey } from './ListenerForm.utils';

describe('getAfTagsToProtocolMapKey', () => {

  it('returns type when type#moduleName cannot be found in afTagsToProtocolMap', () => {
    expect(getAfTagsToProtocolMapKey('ERK_SMF', 'ERKSMFX2IM', mockDistributors[0])).toEqual('ERK_SMF');
  });

  it('returns type#moduleName when type#moduleName cannot be found in afTagsToProtocolMap', () => {
    expect(getAfTagsToProtocolMapKey('ERK_MCPTT', 'E103221X2IM', mockDistributors[0])).toEqual('ERK_MCPTT#E103221X2IM');
  });

  it('returns type when type#moduleName and type are not in afTagsToProtocolMap', () => {
    expect(getAfTagsToProtocolMapKey('NotExistType', 'NotExistModule', mockDistributors[0])).toEqual('NotExistType');
  });

});
