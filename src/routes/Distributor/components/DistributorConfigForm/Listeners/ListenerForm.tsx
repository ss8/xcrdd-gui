import React, {
  useState, useRef, forwardRef, useImperativeHandle, useEffect,
} from 'react';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm, { ChangedField, CustomValidationFunction } from 'shared/form';
import { DistributorConfigurationListener, DistributorConfiguration, DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FormTemplateSection } from 'data/template/template.types';
import { setFieldOptions } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { DistributorData, Value } from 'data/distributor/distributor.types';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import {
  distributorsIPsWhiteListEditableTableColumnDefs,
  distributorsIPsWhiteListReadOnlyTableColumnDefs,
} from 'routes/AccessFunction/AccessFunction.types';
import EditAllowedIPsDialog from '../../../../../components/EditAllowedIPsDialog';
import * as adapters from '../../../utils/distributorConfigTemplate.adapter';
import { getAfTagsToProtocolMapKey, updateWhiteListIps } from './ListenerForm.utils';

type Props = {
  listener: DistributorConfigurationListener,
  templateSection: FormTemplateSection,
  listenerTypes: string[],
  securityInfos: SecurityInfo[],
  distributors: DistributorData
  customValidationFunctionMap: { [key: string]: CustomValidationFunction },
  ipAddressOptions: TextSuggestType[],
  distributorConfiguration: DistributorConfiguration,
  isEditing: boolean,
  afType:string
  moduleName: string
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => DistributorConfigurationListener
}

const ListenerForm = forwardRef<Ref, Props>(({
  listener,
  templateSection,
  listenerTypes,
  securityInfos,
  distributors,
  customValidationFunctionMap,
  ipAddressOptions,
  distributorConfiguration,
  isEditing,
  afType,
  moduleName,
}: Props, ref) => {
  const [showAllowedIPAddresses, setShowAllowedIPAddresses] = useState<boolean>(false);
  const [formData, setFormData] = useState<DistributorConfigurationListener>(listener);
  const [formName, setFormName] = useState<string>(`listener_${Date.now()}`);
  const [template, setTemplate] = useState(templateSection);

  const formRef = useRef<DynamicForm>(null);

  useEffect(() => {
    setFormData(listener);
    setFormName(`listener_${Date.now()}`);
  }, [listener]);

  useEffect(() => {
    setDefaultsOnTypeChange();
    setFormName(`listener_${Date.now()}`);
  }, [formData?.afProtocol]);

  const setDefaultsOnTypeChange = () => {
    if (formData.operation !== 'CREATE') return;
    const newFormData: any = cloneDeep(formData);
    const key = getAfTagsToProtocolMapKey(afType, moduleName, distributors);
    const defaultValues: Value[] = distributors.afTagsToProtocolMap[key]?.listenerDefaultValues ?? [];
    defaultValues.forEach((df: Value) => {
      let value = df.defaultValue;
      if (typeof value === 'number') {
        value = value.toString();
      }
      newFormData[df.field] = value;
    });
    setFormData(newFormData);
  };

  const setListenerTypeOptions = (newTemplate: FormTemplateSection, listenerTypesParam: string[]) => {
    const options = listenerTypesParam.map((type: string) => {
      return {
        label: type,
        value: type,
      };
    });
    setFieldOptions(newTemplate, options, 'afProtocol');
  };

  const setSecurityInfoOptions = (newTemplate: FormTemplateSection, securityInfosParam: SecurityInfo[]) => {
    const options = securityInfosParam.map((secInfo: SecurityInfo) => {
      return {
        label: secInfo.name,
        value: secInfo.id,
      };
    });
    setFieldOptions(newTemplate, options, 'secInfoId');
  };

  const setListenerIpAddressOptions = (formTemplate:FormTemplateSection, options: TextSuggestType[]) => {
    formTemplate.fields.ipAddress.options = options;
  };

  const setAllowedIPAddressesFieldAttributes = (
    newTemplate: FormTemplateSection,
    afWhitelist: DistributorConfigurationListenerAfWhiteList[],
    distributorConfigurationId: string,
  ) => {
    if (distributorConfigurationId) {
      newTemplate.fields.allowedIPAddresses.handleClick = openAllowedIPsAddresses;
      newTemplate.fields.allowedIPAddresses.label = adapters.adaptAllowedIPAddressesLabel(afWhitelist);
    } else {
      newTemplate.fields.allowedIPAddresses.hide = 'true';
    }
  };

  const openAllowedIPsAddresses = () => {
    setShowAllowedIPAddresses(true);
  };

  const handleCloseAllowedIPsDialog = () => {
    setShowAllowedIPAddresses(false);
  };

  const fieldUpdateHandler = (field: ChangedField) => {
    const newListenerState: any = cloneDeep(formData);
    newListenerState[field.name] = field.value;
    setFormData(newListenerState);
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isFormValid = formRef.current?.isValid(forceValidation, forceTouched) ?? true;
    return isFormValid;
  };

  const getValues = () => {
    return formData;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const handleUpdateIpList = (newAFIpsList: DistributorConfigurationListenerAfWhiteList[]) => {
    const updatedFormData: DistributorConfigurationListener = updateWhiteListIps(formData, newAFIpsList);
    setFormData(updatedFormData);

    const newTemplate = cloneDeep(template);
    setAllowedIPAddressesFieldAttributes(newTemplate, newAFIpsList, distributorConfiguration?.id);
    setTemplate(newTemplate);
  };

  // set options to ListenerType, SecurityInfo, IpAddress and AllowedIP button when those source data change
  useEffect(() => {
    const newTemplate = cloneDeep(templateSection);
    setListenerTypeOptions(newTemplate, listenerTypes);
    setSecurityInfoOptions(newTemplate, securityInfos);
    setAllowedIPAddressesFieldAttributes(newTemplate, listener.afWhitelist, distributorConfiguration?.id);
    setListenerIpAddressOptions(newTemplate, ipAddressOptions);
    setTemplate(newTemplate);
    setFormName(`listener_${Date.now()}`);
  }, [templateSection, listenerTypes, securityInfos, listener.afWhitelist, ipAddressOptions]);

  let ipTableColumnDefs = distributorsIPsWhiteListReadOnlyTableColumnDefs;
  if (isEditing) {
    ipTableColumnDefs = distributorsIPsWhiteListEditableTableColumnDefs;
  }

  return (
    <div>
      <DynamicForm
        ref={formRef}
        data-test="ListenerDetail-form"
        name={formName}
        fields={template.fields}
        layout={template.metadata?.layout?.rows}
        collapsibleLayout={template.metadata.layout?.collapsible}
        defaults={formData}
        fieldUpdateHandler={fieldUpdateHandler}
        customValidationFunctionMap={customValidationFunctionMap}
      />
      <EditAllowedIPsDialog
        isOpen={showAllowedIPAddresses}
        ipsWhiteList={formData?.afWhitelist || []}
        isEditing={isEditing}
        ipTableColumnDefs={ipTableColumnDefs}
        distributorName={distributorConfiguration?.name}
        listeningIpAddress={formData?.ipAddress}
        listeningPort={formData?.port}
        onSubmit={handleUpdateIpList}
        onCancel={handleCloseAllowedIPsDialog}
      />
    </div>
  );
});

export default ListenerForm;
