import React, { createRef } from 'react';
import {
  act,
  fireEvent,
  getAllByTestId,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { FormTemplateSection } from 'data/template/template.types';
import { getByName } from '__test__/customQueries';
import { mockDistributors } from '__test__/mockDistributors';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import Destinations, { DestinationsRef } from './Destinations';
import { distributorTemplate } from '../../../templates/distributorTemplate';

describe('<Destinations />', () => {
  let distributorConfig: DistributorConfiguration;

  beforeEach(() => {
    distributorConfig = {
      id: 'ID_TO_GET',
      name: 'DC Name to edit',
      moduleName: 'ERKUPFX3IM',
      instanceId: '1',
      afType: 'ERK_UPF',
      listeners: [
        {
          id: '',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '2.2.2.2',
          port: '2',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        },
      ],
      destinations: [
        {
          id: '',
          ipAddress: '',
          port: '',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
        },
      ],
    };
  });

  it('should not render add/remove Destination/Destination if not in edit mode', () => {
    render(
      <Destinations
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing={false}
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should render the destinations', () => {
    render(
      <Destinations
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    expect(screen.getByTestId('DistributorConfigDestinations')).toBeVisible();
    expect(screen.getByText('Destination 1')).toBeVisible();
  });

  it('should render the Destination form as expected', () => {
    render(
      <Destinations
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'Number of Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Destination Protocol:')).toBeVisible();
  });

  it('should render the new Destination form as expected', () => {
    render(
      <Destinations
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));

    expect(screen.getByText('Destination 2')).toBeVisible();

    const tab = getByRole(destinations, 'tabpanel');

    expect(getByText(tab, 'Destination IP Address:')).toBeVisible();
    expect(getByText(tab, 'Destination Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();

    expect(getByText(tab, 'Advanced Settings')).toBeVisible();
    fireEvent.click(getByText(tab, 'Advanced Settings'));

    expect(getByText(tab, 'Keepalive Interval (Sec):')).toBeVisible();
    expect(getByText(tab, 'Keepalive Retries:')).toBeVisible();
    expect(getByText(tab, 'Number of Connections:')).toBeVisible();
    expect(getByText(tab, 'TLS Profile (Security Info ID):')).toBeVisible();
    expect(getByText(tab, 'Transport:')).toBeVisible();
    expect(getByText(tab, 'Destination Protocol:')).toBeVisible();
  });

  it('should return false in isValid if form is not valid', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });
  });

  it('should return false in isValid if form has duplicated ip:port  and display error message', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: false, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '2.2.2.2' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '2' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });

    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(1);
  });

  it('should return true in isValid if form is valid', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '1' } });

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(true);
    });
  });

  it('should indicate the tab has invalid values when calling isValid', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  it('should clean error indication after error is fixed and calling isValid', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    act(() => {
      formRef.current?.isValid(true, true);
    });

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinationTab = getByRole(destinations, 'tabpanel');
    fireEvent.change(getByName(destinationTab, 'ipAddress'), { target: { value: '1.1.1.1' } });
    fireEvent.change(getByName(destinationTab, 'port'), { target: { value: '1' } });

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeNull());
  });

  it('should clean up tab error indication after removing a tab', async () => {
    const formRef = createRef<DestinationsRef>();

    render(
      <Destinations
        ref={formRef}
        destinations={distributorConfig.destinations}
        templateSection={distributorTemplate.destinations as FormTemplateSection}
        distributors={mockDistributors[0]}
        securityInfos={mockSecurityInfos}
        type="ERK_SMF"
        isEditing
        tlsProxies={[]}
        customValidationFunctionMap={{
          ipPortCombinationValidator: () => ({ isValid: true, errorMessage: '' }),
        }}
        moduleName=""
      />,
    );

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    fireEvent.click(getByTestId(destinations, 'MultiTabAddButton'));

    act(() => {
      formRef.current?.isValid(true, true);
    });

    await waitFor(() => expect(destinations.querySelectorAll('.multi-tab-has-error')).toHaveLength(2));

    fireEvent.click(getAllByTestId(destinations, 'MultiTabRemoveIcon')[0]);

    await waitFor(() => expect(destinations.querySelectorAll('.multi-tab-has-error')).toHaveLength(0));
  });
});
