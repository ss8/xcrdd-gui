import React, {
  useState, useEffect, forwardRef, createRef, useImperativeHandle, useRef,
} from 'react';
import MultiTab from 'shared/multi-tab/MultiTab';
import { CustomValidationFunction } from 'shared/form';
import { DistributorConfigurationDestination } from 'data/distributorConfiguration/distributorConfiguration.types';
import cloneDeep from 'lodash.clonedeep';
import newDistributorConfiguration from 'routes/Distributor/newDistributorConfiguration';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FormTemplateSection } from 'data/template/template.types';
import { DistributorData, Value } from 'data/distributor/distributor.types';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { validateCurrentIpPortCombination } from 'utils/validators/ipPortCombinationValidators';
import DestinationForm, { Ref as DestinationRef } from './DestinationForm';
import { getAfTagsToProtocolMapKey } from '../Listeners/ListenerForm.utils';

const newDestination: DistributorConfigurationDestination = cloneDeep(newDistributorConfiguration.destinations[0]);

type Props = {
  isEditing: boolean
  destinations: DistributorConfigurationDestination[],
  templateSection: FormTemplateSection,
  securityInfos: SecurityInfo[],
  distributors: DistributorData,
  type: string,
  moduleName : string,
  tlsProxies: TlsProxy[]
  customValidationFunctionMap: { [key: string]: CustomValidationFunction },
};

export type DestinationsRef = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean,
  getValues: () => DistributorConfigurationDestination[]
}

const Destinations = forwardRef<DestinationsRef, Props>(({
  isEditing,
  destinations,
  templateSection,
  securityInfos,
  distributors,
  type,
  moduleName,
  tlsProxies,
  customValidationFunctionMap,
}: Props, ref) => {
  const [destinationList, setDestinationList] = useState<DistributorConfigurationDestination[]>(destinations);
  const [deletedDestinations] = useState<DistributorConfigurationDestination[]>([]);
  const [invalidTabs, setInvalidTabs] = useState<boolean[]>([]);
  const destinationRefs = useRef<React.RefObject<DestinationRef>[]>([]);

  useEffect(() => {
    setDefaultsOnTypeChange();
  }, [destinations, type]);

  const setDefaultsOnTypeChange = () => {
    const key = getAfTagsToProtocolMapKey(type, moduleName, distributors);
    const defaultValues: Value[] = distributors.afTagsToProtocolMap[key]?.destinationDefaultValues ?? [];
    const newDestinationList: DistributorConfigurationDestination[] = cloneDeep(destinations);
    newDestinationList.forEach((newDest) => {
      setDefaultsOnDestination(newDest, defaultValues);
    });
    setDefaultsOnDestination(newDestination, defaultValues);
    setDestinationList(newDestinationList);
  };

  const setDefaultsOnDestination = (destination: any, defaultValues: Value[]) => {
    if (destination.operation !== 'CREATE') return;
    defaultValues.forEach((df: Value) => {
      let value = df.defaultValue;
      if (typeof value === 'number') {
        value = value.toString();
      }
      destination[df.field] = value;
    });
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const validDestinations = destinationRefs.current
      .filter((formRef) => !!formRef.current)
      .map((formRef) => formRef.current?.isValid(forceValidation, forceTouched) ?? true);

    const allListenersValid = validDestinations.every((valid) => valid === true);
    setInvalidTabs(validDestinations.map((entry) => !entry));
    return allListenersValid;
  };

  const getValues = () => {
    const formDestinations = getCurrentDestinations();
    const combinedDestinations = formDestinations.concat(deletedDestinations);
    return combinedDestinations;
  };

  const getCurrentDestinations = () => {
    return (destinationRefs.current.map((destinationRef: React.RefObject<DestinationRef>) => {
      return destinationRef.current?.getValues();
    })).filter((destinationRefValues) => !!destinationRefValues) as DistributorConfigurationDestination[];
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const getTabs = () => {
    destinationRefs.current = [];
    return (destinationList?.map((destination: DistributorConfigurationDestination, index: number) => {
      const destinationRef = createRef<DestinationRef>();
      destinationRefs.current.push(destinationRef);

      return {
        id: index,
        title: `Destination ${index + 1}`,
        hasErrors: invalidTabs[index] ?? false,
        panel: <DestinationForm
          ref={destinationRef}
          destination={destination}
          templateSection={templateSection}
          securityInfos={securityInfos}
          tlsProxies={tlsProxies}
          customValidationFunctionMap={customValidationFunctionMap}
        />,
      };
    }));
  };
  const addToDeletedDestinationList = (deletedDestination: DistributorConfigurationDestination) => {
    if (deletedDestination && deletedDestination.id !== '') {
      deletedDestination.operation = 'DELETE';
      deletedDestinations.push(deletedDestination);
    }
  };

  const removeTab = (id: string | number) => {
    const newDestinationList = getCurrentDestinations();
    if (newDestinationList.length < 2) return;
    const deletedDestination = newDestinationList.splice(id as number, 1)[0];
    addToDeletedDestinationList(deletedDestination);
    setDestinationList(newDestinationList);
    setInvalidTabs([]);
  };

  const addTab = () => {
    const newDestinationList = getCurrentDestinations();
    newDestinationList.push(cloneDeep(newDestination));
    setDestinationList(newDestinationList);
  };

  return (
    <div data-testid="DistributorConfigDestinations">
      <h3>Destinations</h3>
      <div className="multi-tab-form-section">
        <MultiTab
          tabContainerId="DistributorConfigDestinationsTab"
          tabs={getTabs()}
          removeTab={removeTab}
          addTab={addTab}
          maxNumberOfTabs={64}
          isEditing={isEditing}
        />
      </div>
    </div>
  );
});

export default Destinations;
