import React, {
  useState, useRef, forwardRef, useImperativeHandle, useEffect,
} from 'react';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm, { ChangedField, CustomValidationFunction } from 'shared/form';
import { DistributorConfigurationDestination } from 'data/distributorConfiguration/distributorConfiguration.types';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { FormTemplateSection } from 'data/template/template.types';
import { setFieldOptions } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import * as adapters from '../../../utils/distributorConfigTemplate.adapter';

type Props = {
  destination: DistributorConfigurationDestination,
  templateSection: FormTemplateSection,
  securityInfos: SecurityInfo[],
  tlsProxies: TlsProxy[],
  customValidationFunctionMap: { [key: string]: CustomValidationFunction }
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => DistributorConfigurationDestination
}

const DestinationForm = forwardRef<Ref, Props>(({
  destination,
  templateSection,
  securityInfos,
  tlsProxies,
  customValidationFunctionMap,
}: Props, ref) => {
  const [formData, setFormData] = useState<DistributorConfigurationDestination>(cloneDeep(destination));
  const [formName, setFormName] = useState<string>(`destination_${Date.now()}`);
  const [template, setTemplate] = useState(templateSection);
  const [listenersIpAddressPorts, setListenersIpAddressPorts] = useState<adapters.ListenersIpAddressPortsType>({});

  const formRef = useRef<DynamicForm>(null);

  useEffect(() => {
    const newTemplate = cloneDeep(template);
    setSecurityInfoOptions(newTemplate);
    setTemplate(newTemplate);
    setFormName(`destination_${Date.now()}`);
  }, [securityInfos]);

  useEffect(() => {
    setFormData(destination);
    setFormName(`destination_${Date.now()}`);
  }, [destination]);

  useEffect(() => {
    setListenersIpAddressPorts(
      adapters.adaptListenersIpAddressPorts(tlsProxies),
    );
  }, [tlsProxies]);

  useEffect(() => {
    template.fields.ipAddress.options = adapters.adaptDestinationsIpAddressesOptions(listenersIpAddressPorts);

    template.fields.port.options = adapters
      .adaptDestinationsPortsOptions(destination.ipAddress, listenersIpAddressPorts);
  }, [templateSection, template, listenersIpAddressPorts, destination, destination.ipAddress]);

  useEffect(() => {
    template.fields.ipAddress.type = templateSection.fields.ipAddress.type;
    template.fields.port.type = templateSection.fields.port.type;
  }, [template, templateSection]);

  const setSecurityInfoOptions = (newTemplate: FormTemplateSection) => {
    const options = securityInfos.map((secInfo: SecurityInfo) => {
      return {
        label: secInfo.name,
        value: secInfo.id,
      };
    });
    setFieldOptions(newTemplate, options, 'secInfoId');
  };

  const updateDestinationsPortsOptions = (param: ChangedField) => {
    if (param.name !== 'ipAddress') {
      return;
    }

    template.fields.port.options = adapters.adaptDestinationsPortsOptions(param.value, listenersIpAddressPorts);
  };

  const fieldUpdateHandler = (field: ChangedField) => {
    const newDestinationState: any = cloneDeep(formData);
    newDestinationState[field.name] = field.value;
    setFormData(newDestinationState);
    updateDestinationsPortsOptions(field);
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isFormValid = formRef.current?.isValid(forceValidation, forceTouched) ?? true;
    return isFormValid;
  };

  const getValues = () => {
    return formData;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  return (
    <div>
      <DynamicForm
        ref={formRef}
        data-test="DestinationDetail-form"
        name={formName}
        fields={template.fields}
        layout={template.metadata.layout?.rows}
        collapsibleLayout={template.metadata.layout?.collapsible}
        defaults={formData}
        fieldUpdateHandler={fieldUpdateHandler}
        customValidationFunctionMap={customValidationFunctionMap}
      />
    </div>
  );
});

export default DestinationForm;
