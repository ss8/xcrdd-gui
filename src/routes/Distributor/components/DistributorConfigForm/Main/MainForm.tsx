import React, {
  useState, useEffect, forwardRef, useImperativeHandle, useRef,
} from 'react';
import cloneDeep from 'lodash.clonedeep';
import DynamicForm, { ChangedField } from 'shared/form';
import { MainFormData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { Distributor, DistributorData } from 'data/distributor/distributor.types';
import { NodeData } from 'data/nodes/nodes.types';
import { FormTemplateOption, FormTemplateSection } from 'data/template/template.types';
import { setFieldOptions } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';

export type Props = {
  formData: MainFormData,
  templateSection: FormTemplateSection,
  distributors: DistributorData,
  nodes: NodeData[],
  availableAccessFunction: SupportedFeaturesAccessFunctions
  enabledAccessFunction: SupportedFeaturesAccessFunctions
  fieldUpdateHandler: (field: ChangedField) => void
}
export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => MainFormData
}

const MainForm = forwardRef<Ref, Props>(({
  formData,
  templateSection,
  distributors,
  nodes,
  availableAccessFunction,
  enabledAccessFunction,
  fieldUpdateHandler,
}: Props, ref) => {
  const formRef = useRef<DynamicForm>(null);

  const setTemplateOptions = (template: FormTemplateSection) => {
    const newTemplate = cloneDeep(template);
    setModuleOptions(newTemplate);
    setInstanceIdOptions(newTemplate);
    setAfTypeOptions(newTemplate);
    return newTemplate;
  };

  const setModuleOptions = (newTemplate: FormTemplateSection) => {
    const moduleNameSet: Set<string> = new Set<string>();

    distributors?.ss8Distributors.forEach(({ moduleName, afTags }) => {
      const shouldModuleNameBeAvailable = afTags.some((afTag) => !!enabledAccessFunction[afTag]);
      if (shouldModuleNameBeAvailable) {
        moduleNameSet.add(moduleName);
      }
    });

    const options = Array.from(moduleNameSet).map((moduleName) => {
      return {
        label: moduleName,
        value: moduleName,
      };
    }).sort((a, b) => a.label.toLocaleLowerCase().localeCompare(b.label.toLocaleLowerCase()));

    setFieldOptions(newTemplate, options, 'moduleName');
  };

  const setInstanceIdOptions = (newTemplate: FormTemplateSection) => {
    const options: FormTemplateOption[] = [];
    distributors?.ss8Distributors.forEach((distributor:Distributor) => {
      if (formData?.moduleName === distributor.moduleName) {
        const { instanceId } = distributor;
        options.push({
          label: `${instanceId}`,
          value: `${instanceId}`,
        });
      }
    });
    setFieldOptions(newTemplate, options, 'instanceId');
  };

  const setAfTypeOptions = (newTemplate: FormTemplateSection) => {
    const dist = distributors?.ss8Distributors.find((distributor:Distributor) => {
      if (formData?.moduleName === distributor.moduleName &&
        formData?.instanceId === distributor.instanceId.toString()) {
        return true;
      }
    });
    const options = dist?.afTags.map((afTag) => {
      return ({
        label: availableAccessFunction[afTag]?.title,
        value: afTag,
      });
    }) ?? [];
    setFieldOptions(newTemplate, options, 'afType');
  };

  const setNodeNameOptions = (newTemplate: FormTemplateSection) => {
    if (!formData?.moduleName) {
      setFieldOptions(newTemplate, [], 'nodeName');
      return;
    }

    const options: {label:string, value:string}[] = [];
    nodes?.forEach((node: NodeData) => {
      options.push({
        label: `${node.nodeName} (${node.nodeType})`,
        value: node.nodeName,
      });
    });

    setFieldOptions(newTemplate, options, 'nodeName');
  };

  const [template, setTemplate] = useState(setTemplateOptions(templateSection));

  useEffect(() => {
    const newTemplate = cloneDeep(template);
    setInstanceIdOptions(newTemplate);
    setAfTypeOptions(newTemplate);
    setNodeNameOptions(newTemplate);
    setTemplate(newTemplate);
  }, [formData?.moduleName, formData?.instanceId]);

  const isValid = (forceValidation = false, forceTouched = true) => {
    return formRef.current?.isValid(forceValidation, forceTouched) ?? true;
  };

  const getValues = (): MainFormData => formRef.current?.getValues();

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  return (
    <div>
      <DynamicForm
        ref={formRef}
        data-test="ListenerDetail-form"
        name={`listener_${new Date().getTime()}`}
        fields={template.fields}
        layout={template.metadata?.layout?.rows}
        defaults={formData}
        fieldUpdateHandler={fieldUpdateHandler}
      />
    </div>
  );
});

export default MainForm;
