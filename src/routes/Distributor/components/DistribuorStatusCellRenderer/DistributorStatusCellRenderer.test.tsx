import React from 'react';
import { render, screen } from '@testing-library/react';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import DistributorStatusCellRenderer from './DistributorStatusCellRenderer';

describe('<DistributorStatusCellRenderer />', () => {
  let data: DistributorConfiguration;

  beforeEach(() => {
    data = {
      id: 'id1',
      name: 'DC-NAME-1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      listeners: [{
        id: 'listeners1',
        afProtocol: '',
        ipAddress: '',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '10',
        port: '1134',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [],
      }],
      destinations: [{
        id: 'destinations1',
        ipAddress: '',
        port: '1134',
        requiredState: 'ACTIVE',
        status: 'ACTIVE',
        keepaliveInterval: '10',
        keepaliveRetries: '10',
        numOfConnections: '10',
        protocol: '',
        secInfoId: '1234',
        transport: 'TCP',
      }],
    };
  });

  it('should render as "-" if no value', () => {
    render(
      <DistributorStatusCellRenderer />,
    );

    expect(screen.getByText('-')).toBeVisible();
  });

  it('should render the icon and the status for ACTIVE', () => {
    render(
      <DistributorStatusCellRenderer
        data={data}
      />,
    );

    expect(screen.getByText('Active')).toBeVisible();
    expect(screen.getByTitle('Active')).toBeVisible();
  });

  it('should render the icon and status for INACTIVE', () => {
    data.listeners[0].status = 'INACTIVE';
    data.destinations[0].status = 'INACTIVE';

    render(
      <DistributorStatusCellRenderer
        data={data}
      />,
    );

    expect(screen.getByText('Inactive')).toBeVisible();
    expect(screen.getByTitle('Inactive')).toBeVisible();
  });

  it('should render the icon and the status for PARTIALLY_ACTIVE', () => {
    data.destinations[0].status = 'INACTIVE';
    render(
      <DistributorStatusCellRenderer
        data={data}
      />,
    );

    expect(screen.getByText('Partially Active')).toBeVisible();
    expect(screen.getByTitle('Partially Active')).toBeVisible();
  });
});
