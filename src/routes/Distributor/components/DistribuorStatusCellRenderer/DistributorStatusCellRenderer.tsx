import React, { Fragment } from 'react';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';
import { stateValueFormatter } from 'utils/valueFormatters';
import { IconCycle } from 'utils/icons';
import './distributor-status-cell-renderer.scss';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';

type Props = Partial<ICellRendererParams>;

export default function DistributorStatusCellRenderer({
  data = {},
  ...rest
}: Props): JSX.Element {
  const {
    listeners = [],
    destinations = [],
  } = data as DistributorConfiguration;

  if (listeners.length === 0 && destinations.length === 0) {
    return (
      <Fragment>-</Fragment>
    );
  }

  const areAllListenersActive = listeners.every(({ status }) => status === 'ACTIVE');
  const areAllDestinationsActive = destinations.every(({ status }) => status === 'ACTIVE');

  const areAllListenersInactive = listeners.every(({ status }) => status === 'INACTIVE');
  const areAllDestinationsInactive = destinations.every(({ status }) => status === 'INACTIVE');

  let value = 'PARTIALLY_ACTIVE';

  if (areAllListenersActive && areAllDestinationsActive) {
    value = 'ACTIVE';
  } else if (areAllListenersInactive && areAllDestinationsInactive) {
    value = 'INACTIVE';
  }

  let className = 'status ';

  if (value === 'ACTIVE') {
    className += 'active';
  } else if (value === 'INACTIVE') {
    className += 'inactive';
  } else if (value === 'PARTIALLY_ACTIVE') {
    className += 'partially-active';
  }

  value = stateValueFormatter({ ...(rest as ICellRendererParams), value, data });

  return (
    <span className="distributor-status-cell-renderer">
      <span className={className} title={value}><IconCycle /></span>
      {value}
    </span>
  );
}
