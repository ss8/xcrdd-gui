import { FormTemplate } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export const distributorTemplate: FormTemplate = {
  version: '1.0.0',
  name: 'Distributor Configuration',
  key: 'distributorConfig',
  main: {
    metadata: {
      layout: {
        rows: [
          ['id'],
          ['name', 'moduleName', 'instanceId', 'afType'],
          ['nodeName', '$empty', '$empty', '$empty'],
        ],
      },
    },
    fields: {
      id: {
        type: 'text',
        initial: '',
        hide: 'true',
      },
      name: {
        type: 'text',
        label: 'Configuration Name',
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
          ],
        },
      },
      moduleName: {
        label: 'Module Name',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Select a module',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
      instanceId: {
        label: 'Instance ID',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Select an instance ID',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
      afType: {
        label: 'AF Type',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Select an AF type',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
      nodeName: {
        label: 'Xcipio Node',
        type: 'select',
        options: [
          {
            label: 'Select a Node',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
    },
  },
  listeners: {
    metadata: {
      layout: {
        rows: [
          [
            'afProtocol',
            'ipAddress',
            'port',
            'requiredState',
          ],
          [
            'allowedIPAddresses',
          ],
        ],
        collapsible: {
          label: 'Advanced Settings',
          rows: [
            [
              'keepaliveTimeout',
              'maxConcurrentConnection',
              'secInfoId',
              'transport',
            ],
            [
              'synchronizationHeader',
              'numberOfParsers',
              '$empty',
              '$empty',
            ],
          ],
        },
      },
    },
    fields: {
      afProtocol: {
        label: 'Listener Type',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Select a listener type',
            value: '',
          },
        ],
        validation: {
          required: true,
        },
      },
      ipAddress: {
        label: 'Listening IP Address',
        type: 'textSuggest',
        options: [],
        initial: '',
        placeholder: 'Type or select IP Address...',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: 'IP address and Port combination not unique. Please review.',
            },
          ],
        },
      },
      port: {
        label: 'Listening Port',
        type: 'text',
        initial: '1',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$',
              errorMessage: 'Must be a number between 1 - 65535. Please re-enter.',
            },
            {
              type: 'triggerValidationOnAnotherField',
              theOtherField: 'ipAddress',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: '',
            },
          ],
        },
      },
      keepaliveTimeout: {
        label: 'Keepalive Timeout (Sec)',
        type: 'text',
        initial: '300',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-3][0-9]{3}|40[0-8][0-9]|409[0-6])$',
              errorMessage: 'Must be a number between 0 - 4096. Please re-enter.',
            },
          ],
        },
      },
      maxConcurrentConnection: {
        label: 'Maximum Concurrent Connections',
        type: 'text',
        initial: '128',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99[0-8][0-9]{2}|999[0-8][0-9]|9999[0-9]|1[0-2][0-9]{4}|130[0-9]{3}|1310[0-6][0-9]|131070)$',
              errorMessage: 'Must be a number between 1 - 131070. Please re-enter.',
            },
          ],
        },
      },
      secInfoId: {
        label: 'TLS Profile (Security Info ID)',
        type: 'select',
        initial: 'Tk9ORQ',
        readOnlyIfOneOption: true,
        options: [{
          label: 'None',
          value: 'Tk9ORQ',
        },
        ],
      },
      transport: {
        label: 'Transport',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'TCP',
            value: 'TCP',
          },
          {
            label: 'UDP',
            value: 'UDP',
          },
          {
            label: 'ZPULL',
            value: 'ZPULL',
          },
        ],
      },
      requiredState: {
        label: 'Configured State',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Active',
            value: 'ACTIVE',
          },
          {
            label: 'Inactive',
            value: 'INACTIVE',
          },
        ],
      },
      synchronizationHeader: {
        label: 'Sync Header',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'None',
            value: 'NONE',
          },
          {
            label: 'TPKT',
            value: 'TPKT',
          },
        ],
      },
      numberOfParsers: {
        label: 'Number Of Parsers',
        type: 'text',
        initial: '2',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|1[0-6])$',
              errorMessage: 'Must be a number between 1 - 16. Please re-enter.',
            },
          ],
        },
      },
      allowedIPAddresses: {
        type: 'buttonWithMessage',
        initial: 'Allowed IP Addresses',
        className: 'allowed-ip-addresses',
      },
    },
  },
  destinations: {
    metadata: {
      layout: {
        rows: [
          [
            'ipAddress',
            'port',
            'requiredState',
            '$empty',
          ],
        ],
        collapsible: {
          label: 'Advanced Settings',
          rows: [
            [
              'keepaliveInterval',
              'keepaliveRetries',
              'numOfConnections',
              'secInfoId',
            ],
            [
              'transport',
              'protocol',
              '$empty',
              '$empty',
            ],
          ],
        },
      },
    },
    fields: {
      ipAddress: {
        label: 'Destination IP Address',
        type: 'text',
        placeholder: 'Type or select IP Address...',
        options: [],
        initial: '',
        validation: {
          required: true,
          validators: [
            {
              type: 'length',
              length: 50,
            },
            {
              type: 'IPAddress',
              errorMessage: 'Must be a valid IP Address. Please re-enter.',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: 'IP address and Port combination not unique. Please review.',
            },
          ],
        },
      },
      port: {
        label: 'Destination Port',
        type: 'text',
        placeholder: 'Type or select IP Port...',
        options: [],
        initial: '1',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$',
              errorMessage: 'Must be a number between 1 - 65535. Please re-enter.',
            },
            {
              type: 'triggerValidationOnAnotherField',
              theOtherField: 'ipAddress',
            },
            {
              type: 'customFunction',
              customValidationFunction: 'ipPortCombinationValidator',
              errorMessage: '',
            },
          ],
        },
      },
      keepaliveInterval: {
        label: 'Keepalive Interval (Sec)',
        type: 'text',
        initial: '60',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-3][0-9]|240)$',
              errorMessage: 'Must be a number between 1 - 240. Please re-enter.',
            },
          ],
        },
      },
      keepaliveRetries: {
        label: 'Keepalive Retries',
        type: 'text',
        initial: '6',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]|10)$',
              errorMessage: 'Must be a number between 1 - 10. Please re-enter.',
            },
          ],
        },
      },
      numOfConnections: {
        label: 'Number of Connections',
        type: 'text',
        initial: '2',
        validation: {
          required: true,
          validators: [
            {
              type: 'regexp',
              regexp: '^([1-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$',
              errorMessage: 'Must be a number between 1 - 65535. Please re-enter.',
            },
          ],
        },
      },
      requiredState: {
        label: 'Configured State',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Active',
            value: 'ACTIVE',
          },
          {
            label: 'Inactive',
            value: 'INACTIVE',
          },
        ],
      },
      secInfoId: {
        label: 'TLS Profile (Security Info ID)',
        type: 'select',
        initial: 'Tk9ORQ',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'None',
            value: 'Tk9ORQ',
          },
        ],
      },
      transport: {
        label: 'Transport',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'TCP',
            value: 'TCP',
          },
          {
            label: 'ZPUSH',
            value: 'ZPUSH',
          },
        ],
      },
      protocol: {
        label: 'Destination Protocol',
        type: 'select',
        readOnlyIfOneOption: true,
        options: [
          {
            label: 'Passthrough',
            value: 'PASSTHRU',
          },
          {
            label: 'Passthrough with SX3 header',
            value: 'PASSTHRU_W_SX3HDR',
          },
        ],
      },
    },
  },
};
