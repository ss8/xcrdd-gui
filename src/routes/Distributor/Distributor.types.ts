import { COMMON_GRID_COLUMN_FIELD_NAMES } from 'shared/commonGrid/grid-enums';
import { ColGroupDef } from '@ag-grid-enterprise/all-modules';
import {
  DistributorConfiguration,
  DistributorConfigurationDestination,
  DistributorConfigurationListener,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import { stateValueFormatter } from 'utils/valueFormatters';
import { getFieldFromFirstInterfaceNameValueGetter } from 'utils/valueGetters';
import getNumberOfItemsFromFieldNameValueGetter from 'utils/valueGetters/getNumberOfItemsFromFieldNameValueGetter';
import {
  moduleNameAndInstanceIdValueGetter,
} from './utils/distributorValueGetters';
import DistributorStatusCellRenderer from './components/DistribuorStatusCellRenderer';

export enum DistributorMenuActionsType {
  Copy = 'copy',
  Delete = 'delete',
  Edit = 'edit',
  View = 'view'
}

export const distributorColumnDefs: ColGroupDef[] = [
  {
    headerName: '',
    children: [
      {
        headerName: '',
        field: COMMON_GRID_COLUMN_FIELD_NAMES.Action,
        cellRenderer: 'actionMenuCellRenderer',
      },
      {
        headerName: 'Configuration Name',
        field: 'name',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'name',
        flex: 1,
        minWidth: 198,
      },
      {
        field: 'moduleName',
        headerName: 'Module/Instance ID',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 130,
        tooltipValueGetter: moduleNameAndInstanceIdValueGetter,
        valueGetter: moduleNameAndInstanceIdValueGetter,
      },
      {
        headerName: 'AF Type',
        field: 'afType',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'afType',
        flex: 1,
        minWidth: 150,
      },
      {
        headerName: 'Current Status',
        field: 'currentStatus',
        sortable: true,
        filter: 'agSetColumnFilter',
        resizable: true,
        tooltipField: 'currentStatus',
        flex: 1,
        minWidth: 140,
        cellRendererFramework: DistributorStatusCellRenderer,
      },
      {
        headerName: '# of Listeners',
        field: 'listeners',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 110,
        tooltipValueGetter: getNumberOfItemsFromFieldNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
        valueGetter: getNumberOfItemsFromFieldNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
      },
      {
        headerName: 'Node Name',
        field: 'nodeName',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        tooltipField: 'nodeName',
        flex: 1,
        minWidth: 150,
        hide: true,
      },
    ],
  },
  {
    headerName: 'Listener 1',
    children: [
      {
        headerName: 'Type',
        field: 'afProtocol',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 134,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
      },
      {
        headerName: 'IP Addresses',
        field: 'ipAddress',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 158,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
      },
      {
        headerName: 'Port',
        field: 'port',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 100,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener >('listeners'),
      },
      {
        headerName: 'Configured State',
        field: 'requiredState',
        sortable: true,
        filter: 'agSetColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 130,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener>('listeners'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationListener >('listeners'),
        valueFormatter: stateValueFormatter,
      },
    ],
  },
  {
    headerName: 'Destination 1',
    children: [
      {
        headerName: 'IP Addresses',
        field: 'ipAddress',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 158,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination >('destinations'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination >('destinations'),
      },
      {
        headerName: 'Port',
        field: 'port',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 100,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination>('destinations'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination>('destinations'),
      },
      {
        headerName: 'Configured State',
        field: 'requiredState',
        sortable: true,
        filter: 'agSetColumnFilter',
        resizable: true,
        flex: 1,
        minWidth: 130,
        tooltipValueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination>('destinations'),
        valueGetter: getFieldFromFirstInterfaceNameValueGetter<DistributorConfiguration, DistributorConfigurationDestination >('destinations'),
        valueFormatter: stateValueFormatter,
      },
    ],
  },
];
