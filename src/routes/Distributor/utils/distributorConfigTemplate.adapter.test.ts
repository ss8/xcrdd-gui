import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { FormTemplateSection } from 'data/template/template.types';
import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';
import { mockNodes } from '__test__/mockNodes';
import { distributorTemplate } from '../templates';
import {
  adaptAllowedIPAddressesLabel,
  adaptDestinationsIpAddressPortTemplate,
  adaptDestinationsPortsOptions,
  adaptListenersIpAddressPorts,
  adaptNodeNameIpAddressOptions,
  adaptTemplateVisibilityMode,
} from './distributorConfigTemplate.adapter';

const TLS_PROXIES_MOCK:TlsProxy[] = [
  {
    id: 'MQ',
    name: 'smf-1',
    nodeName: 'node-1',
    instanceId: '1',
    listeners: [{
      id: 'MSMx',
      ipAddress: '27.0.0.1',
      port: '12115',
      keepaliveTimeout: '330',
      maxConcurrentConnection: '128',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      requiredState: 'ACTIVE',
      routeRule: 'SRCADDR',
    },
    ],
    destinations: [],
  },
  {
    id: 'Mg',
    name: 'upf-1',
    nodeName: 'node-2',
    instanceId: '1',
    listeners: [{
      id: 'MiMx',
      ipAddress: '27.0.0.3',
      port: '45160',
      keepaliveTimeout: '300',
      maxConcurrentConnection: '128',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      routeRule: 'SRCADDR',
      requiredState: 'ACTIVE',
    }, {
      id: 'MiMy',
      ipAddress: '27.0.0.3',
      port: '45161',
      keepaliveTimeout: '300',
      maxConcurrentConnection: '128',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      routeRule: 'SRCADDR',
      requiredState: 'ACTIVE',
    }],
    destinations: [],
  },
];

const TLS_PROXIES_ADDRESS_PORT_MOCK = { '27.0.0.1': ['12115'], '27.0.0.3': ['45160', '45161'] };

describe('distributorConfigTemplate.adapter', () => {
  describe('adaptTemplateVisibilityMode()', () => {
    it('should return the template adapted to create mode', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'create')).toEqual({
        ...distributorTemplate,
      });
    });

    it('should return the template adapted to edit mode', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'edit')).toEqual({
        ...distributorTemplate,
        main: {
          ...distributorTemplate.main as FormTemplateSection,
          fields: {
            ...(distributorTemplate.main as FormTemplateSection).fields,
            moduleName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.moduleName,
              readOnly: true,
            },
            instanceId: {
              ...(distributorTemplate.main as FormTemplateSection).fields.instanceId,
              readOnly: true,
            },
            afType: {
              ...(distributorTemplate.main as FormTemplateSection).fields.afType,
              readOnly: true,
            },
            nodeName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.nodeName,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should return the template adapted to copy mode', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'copy')).toEqual({
        ...distributorTemplate,
        main: {
          ...distributorTemplate.main as FormTemplateSection,
          fields: {
            ...(distributorTemplate.main as FormTemplateSection).fields,
            moduleName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.moduleName,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should return the template adapted to view mode', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'view')).toEqual({
        ...distributorTemplate,
        main: {
          ...distributorTemplate.main as FormTemplateSection,
          fields: {
            ...(distributorTemplate.main as FormTemplateSection).fields,
            id: {
              ...(distributorTemplate.main as FormTemplateSection).fields.id,
              readOnly: true,
            },
            name: {
              ...(distributorTemplate.main as FormTemplateSection).fields.name,
              readOnly: true,
            },
            moduleName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.moduleName,
              readOnly: true,
            },
            instanceId: {
              ...(distributorTemplate.main as FormTemplateSection).fields.instanceId,
              readOnly: true,
            },
            afType: {
              ...(distributorTemplate.main as FormTemplateSection).fields.afType,
              readOnly: true,
            },
            nodeName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.nodeName,
              readOnly: true,
            },
          },
        },
        listeners: {
          ...distributorTemplate.listeners as FormTemplateSection,
          fields: {
            ...(distributorTemplate.listeners as FormTemplateSection).fields,
            afProtocol: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.afProtocol,
              readOnly: true,
            },
            ipAddress: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.requiredState,
              readOnly: true,
            },
            allowedIPAddresses: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.allowedIPAddresses,
              readOnly: true,
            },
            keepaliveTimeout: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.keepaliveTimeout,
              readOnly: true,
            },
            maxConcurrentConnection: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.maxConcurrentConnection,
              readOnly: true,
            },
            secInfoId: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.transport,
              readOnly: true,
            },
            synchronizationHeader: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.synchronizationHeader,
              readOnly: true,
            },
            numberOfParsers: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.numberOfParsers,
              readOnly: true,
            },
          },
        },
        destinations: {
          ...distributorTemplate.destinations as FormTemplateSection,
          fields: {
            ...(distributorTemplate.destinations as FormTemplateSection).fields,
            ipAddress: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.requiredState,
              readOnly: true,
            },
            keepaliveInterval: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.keepaliveInterval,
              readOnly: true,
            },
            keepaliveRetries: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.keepaliveRetries,
              readOnly: true,
            },
            numOfConnections: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.numOfConnections,
              readOnly: true,
            },
            secInfoId: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.transport,
              readOnly: true,
            },
            protocol: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.protocol,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should return the template adapted to edit mode', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'edit', 'ou1')).toEqual({
        ...distributorTemplate,
        main: {
          ...distributorTemplate.main as FormTemplateSection,
          fields: {
            ...(distributorTemplate.main as FormTemplateSection).fields,
            moduleName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.moduleName,
              readOnly: true,
            },
            instanceId: {
              ...(distributorTemplate.main as FormTemplateSection).fields.instanceId,
              readOnly: true,
            },
            afType: {
              ...(distributorTemplate.main as FormTemplateSection).fields.afType,
              readOnly: true,
            },
            nodeName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.nodeName,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should return the template adapted to edit mode for Helm Charts scenario', () => {
      expect(adaptTemplateVisibilityMode(distributorTemplate, 'edit', 'lis')).toEqual({
        ...distributorTemplate,
        main: {
          ...distributorTemplate.main as FormTemplateSection,
          fields: {
            ...(distributorTemplate.main as FormTemplateSection).fields,
            id: {
              ...(distributorTemplate.main as FormTemplateSection).fields.id,
              readOnly: true,
            },
            name: {
              ...(distributorTemplate.main as FormTemplateSection).fields.name,
              readOnly: true,
            },
            moduleName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.moduleName,
              readOnly: true,
            },
            instanceId: {
              ...(distributorTemplate.main as FormTemplateSection).fields.instanceId,
              readOnly: true,
            },
            afType: {
              ...(distributorTemplate.main as FormTemplateSection).fields.afType,
              readOnly: true,
            },
            nodeName: {
              ...(distributorTemplate.main as FormTemplateSection).fields.nodeName,
              readOnly: true,
            },
          },
        },
        listeners: {
          ...distributorTemplate.listeners as FormTemplateSection,
          fields: {
            ...(distributorTemplate.listeners as FormTemplateSection).fields,
            afProtocol: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.afProtocol,
              readOnly: true,
            },
            ipAddress: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.requiredState,
            },
            allowedIPAddresses: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.allowedIPAddresses,
              readOnly: true,
            },
            keepaliveTimeout: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.keepaliveTimeout,
              readOnly: true,
            },
            maxConcurrentConnection: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.maxConcurrentConnection,
              readOnly: true,
            },
            secInfoId: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.transport,
              readOnly: true,
            },
            synchronizationHeader: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.synchronizationHeader,
              readOnly: true,
            },
            numberOfParsers: {
              ...(distributorTemplate.listeners as FormTemplateSection).fields.numberOfParsers,
              readOnly: true,
            },
          },
        },
        destinations: {
          ...distributorTemplate.destinations as FormTemplateSection,
          fields: {
            ...(distributorTemplate.destinations as FormTemplateSection).fields,
            ipAddress: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.ipAddress,
              readOnly: true,
            },
            port: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.port,
              readOnly: true,
            },
            requiredState: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.requiredState,
            },
            keepaliveInterval: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.keepaliveInterval,
              readOnly: true,
            },
            keepaliveRetries: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.keepaliveRetries,
              readOnly: true,
            },
            numOfConnections: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.numOfConnections,
              readOnly: true,
            },
            secInfoId: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.secInfoId,
              readOnly: true,
            },
            transport: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.transport,
              readOnly: true,
            },
            protocol: {
              ...(distributorTemplate.destinations as FormTemplateSection).fields.protocol,
              readOnly: true,
            },
          },
        },
      });
    });
  });

  describe('adaptListenersIpAddressPorts()', () => {
    it('should return a empty object when input is empty ', () => {
      expect(adaptListenersIpAddressPorts([])).toEqual({});
    });

    it('should return the IpAddressPorts according TlsProxy listeners ipDresses and ports ', () => {
      expect(adaptListenersIpAddressPorts(TLS_PROXIES_MOCK)).toEqual(TLS_PROXIES_ADDRESS_PORT_MOCK);
    });
  });

  describe('adaptDestinationsPortsOptions()', () => {
    it('should return a empty array of ports when input is empty ', () => {
      expect(adaptDestinationsPortsOptions('', {})).toEqual([]);
    });

    it('should return the Ports Options according received listenersIpAddressPorts ipAddress "27.0.0.3"', () => {
      expect(adaptDestinationsPortsOptions('27.0.0.3', TLS_PROXIES_ADDRESS_PORT_MOCK)).toEqual([{ label: '45160', value: '45160' }, { label: '45161', value: '45161' }]);
    });

    it('should return the Ports Options according received listenersIpAddressPorts ipAddress "27.0.0.1"', () => {
      expect(adaptDestinationsPortsOptions('27.0.0.1', TLS_PROXIES_ADDRESS_PORT_MOCK)).toEqual([{ label: '12115', value: '12115' }]);
    });
  });

  describe('adaptNodeNameIpAddressOptions', () => {
    it('should return an empty list if nodes list does not contain the informed nodeName', () => {
      const input = adaptNodeNameIpAddressOptions('node-invalid', mockNodes);
      expect(input).toEqual([]);
    });

    it('should return the ipAddress list based in the informed nodeName = node-1', () => {
      const input = adaptNodeNameIpAddressOptions('node-1', mockNodes);
      const output = [{ label: '10.0.1.11', value: '10.0.1.11' }];
      expect(input).toEqual(output);
    });

    it('should return the ipAddress list based in the informed nodeName = node-2', () => {
      const input = adaptNodeNameIpAddressOptions('node-2', mockNodes);
      const output = [{ label: '10.0.2.22', value: '10.0.2.22' }];
      expect(input).toEqual(output);
    });
  });

  describe('adaptDestinationsIpAddressPortTemplate', () => {

    it('should set destinations ipAddress and port type as test if  module name is not XCF3', () => {
      const destinationsTemplate:FormTemplateSection = {
        fields: {
          ipAddress: {
            type: 'textSuggest',
          },
          port: {
            type: 'textSuggest',
          },
        },
        metadata: {
          layout: { rows: [] },

        },
      };

      const input = adaptDestinationsIpAddressPortTemplate('moduleName', destinationsTemplate);

      const output:FormTemplateSection = {
        fields: {
          ipAddress: {
            type: 'text',
          },
          port: {
            type: 'text',
          },
        },
        metadata: {
          layout: { rows: [] },

        },
      };
      expect(input).toEqual(output);
    });

    it('should set destinations ipAddress and port type as testSuggest if module name is XCF3 ', () => {

      const destinationsTemplate:FormTemplateSection = {
        fields: {
          ipAddress: {
            type: 'text',
          },
          port: {
            type: 'text',
          },
        },
        metadata: {
          layout: { rows: [] },

        },
      };

      const input = adaptDestinationsIpAddressPortTemplate('XCF3', destinationsTemplate);

      const output:FormTemplateSection = {
        fields: {
          ipAddress: {
            type: 'textSuggest',
          },
          port: {
            type: 'textSuggest',
          },
        },
        metadata: {
          layout: { rows: [] },

        },
      };
      expect(input).toEqual(output);
    });
  });

  describe('adaptAllowedIPAddressesLabel()', () => {
    it('should return "All IP addresses allowed" if whitelist is empty', () => {
      const afWhiteList: DistributorConfigurationListenerAfWhiteList[] = [];
      expect(adaptAllowedIPAddressesLabel(afWhiteList)).toBe('All IP addresses allowed');
    });

    it('should return  1 IP address allowed if whitelist length is 1', () => {
      const afWhiteList: DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'Name_1',
        },
      ];
      expect(adaptAllowedIPAddressesLabel(afWhiteList)).toBe('1 IP address allowed');
    });

    it('should return  total IP address allowed if whitelist length is greater than 1', () => {
      const afWhiteList: DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'Name_1',
        },
        {
          afAddress: '1.1.1.2',
          afId: 'id_2',
          afName: 'Name_2',
        },
      ];
      expect(adaptAllowedIPAddressesLabel(afWhiteList)).toBe('2 IP addresses allowed');
    });
  });
});
