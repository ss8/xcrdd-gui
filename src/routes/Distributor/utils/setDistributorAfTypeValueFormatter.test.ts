import cloneDeep from 'lodash.clonedeep';
import { ColDef } from '@ag-grid-enterprise/all-modules';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import {
  setDistributorAfTypeValueFormatter,
} from './setDistributorAfTypeValueFormatter';
import { distributorColumnDefs } from '../Distributor.types';

describe('setDistributorAfTypeValueFormatter()', () => {
  let accessFunctions: SupportedFeaturesAccessFunctions;

  beforeEach(() => {
    accessFunctions = mockSupportedFeatures.config.accessFunctions;
  });

  it('should return the colum def as is if access functions not defined', () => {
    expect(setDistributorAfTypeValueFormatter({}, distributorColumnDefs)).toEqual(distributorColumnDefs);
  });

  it('should set the value formatter for the type column', () => {
    const expected = cloneDeep(distributorColumnDefs);
    (expected[0].children[3] as ColDef).valueFormatter = expect.any(Function);
    expect(setDistributorAfTypeValueFormatter(accessFunctions, distributorColumnDefs)).toEqual(expected);
  });
});
