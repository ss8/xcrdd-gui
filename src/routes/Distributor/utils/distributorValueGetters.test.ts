import { ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import * as types from 'data/distributorConfiguration/distributorConfiguration.types';
import getNumberOfItemsFromFieldNameValueGetter from 'utils/valueGetters/getNumberOfItemsFromFieldNameValueGetter';
import {
  moduleNameAndInstanceIdValueGetter,
} from './distributorValueGetters';

describe('DistributorValueGetters', () => {
  let distributorConfiguration: types.DistributorConfiguration;
  let params: Partial<ValueGetterParams>;

  beforeEach(() => {
    distributorConfiguration = {
      id: 'id1',
      name: 'name1',
      instanceId: '1',
      moduleName: 'moduleName1',
      afType: 'type1',
      listeners: [{
        id: '2',
        afProtocol: 'afProtocol',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      }, {
        id: '3',
        afProtocol: 'afProtocol',
        ipAddress: '22.22.22.22',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '2234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'INACTIVE',
        synchronizationHeader: 'NONE',
        numberOfParsers: '2',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId2',
          afAddress: '22.22.22.22',
          afName: 'afName',
        }],
      }],
      destinations: [],
    };

    params = {
      colDef: {
        field: 'ipAddress',
      },
      data: distributorConfiguration,
    };
  });

  describe('get Number Of Interfaces', () => {
    it('should return a value getter function', () => {
      expect(getNumberOfItemsFromFieldNameValueGetter<types.DistributorConfiguration, types.DistributorConfigurationListener>('listeners')).toEqual(expect.any(Function));
    });

    it('should return the length of interfaces as 0 if no interfaces', () => {
      const valueGetter = getNumberOfItemsFromFieldNameValueGetter<types.DistributorConfiguration, types.DistributorConfiguration>('destinations');
      expect(valueGetter(params)).toEqual('0');
    });

    it('should return the length of interfaces when list is not empty', () => {
      const valueGetter = getNumberOfItemsFromFieldNameValueGetter<types.DistributorConfiguration, types.DistributorConfiguration>('listeners');
      expect(valueGetter(params)).toEqual('2');
    });
  });

  describe('moduleNameAndInstanceIdValueGetter', () => {
    it('should return the moduleName na instanceId', () => {
      expect(moduleNameAndInstanceIdValueGetter(params)).toEqual('moduleName1/1');
    });
  });
});
