import { ColGroupDef, ColDef } from '@ag-grid-enterprise/all-modules';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import getAccessFunctionTypeValueFormatter from 'utils/valueFormatters/getAccessFunctionTypeValueFormatter';
import cloneDeep from 'lodash.clonedeep';

// eslint-disable-next-line import/prefer-default-export
export function setDistributorAfTypeValueFormatter(
  accessFunctions: SupportedFeaturesAccessFunctions,
  distributorColumnDefs: ColGroupDef[],
): ColGroupDef[] {
  const clonedDistributorColumnDefs = cloneDeep(distributorColumnDefs);

  if (Object.keys(accessFunctions).length > 0) {
    const typeColumDef = clonedDistributorColumnDefs[0].children.find((entry) => (entry as ColDef).field === 'afType');
    if (typeColumDef) {
      (typeColumDef as ColDef).valueFormatter = getAccessFunctionTypeValueFormatter(accessFunctions);
    }
  }

  return clonedDistributorColumnDefs;
}
