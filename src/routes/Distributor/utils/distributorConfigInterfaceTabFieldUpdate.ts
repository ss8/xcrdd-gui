import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';

// eslint-disable-next-line import/prefer-default-export
export function handleInterfacesFieldUpdate(
  distributorConfiguration: DistributorConfiguration,
  fieldName: string,
): void {
  if (fieldName === 'moduleName' || fieldName === 'instanceId' || fieldName === 'afType') {
    resetListeners(distributorConfiguration);
  }
}

function resetListeners(distributorConfiguration: DistributorConfiguration) {
  distributorConfiguration.listeners.forEach((listener) => {
    listener.afProtocol = '';
  });
}
