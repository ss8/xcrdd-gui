/* eslint-disable import/prefer-default-export */
import cloneDeep from 'lodash.clonedeep';
import {
  FormTemplate,
  FormTemplateSection,
} from 'data/template/template.types';
import { TlsProxy, TlsProxyListener } from 'data/tlsProxy/tlsProxy.types';
import { NodeData } from 'data/nodes/nodes.types';
import { TextSuggestType } from 'shared/form/custom/TextSuggest/TextSuggest';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';

export type ListenersIpAddressPortsType = {
  [key:string]: string[],
}

export function adaptTemplateVisibilityMode(
  template: FormTemplate | undefined,
  mode: 'create' | 'edit' | 'view' | 'copy',
  owner?: string,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);

  if (mode === 'edit') {
    if (owner === 'lis') {
      setAllFieldsAsReadOnlyExcludingFields(clonedTemplate, ['requiredState']);
    } else {
      (clonedTemplate.main as FormTemplateSection).fields.moduleName.readOnly = true;
      (clonedTemplate.main as FormTemplateSection).fields.instanceId.readOnly = true;
      (clonedTemplate.main as FormTemplateSection).fields.afType.readOnly = true;
      (clonedTemplate.main as FormTemplateSection).fields.nodeName.readOnly = true;
    }
  } else if (mode === 'view') {
    setAllFieldsAsReadOnlyExcludingFields(clonedTemplate, []);
  } else if (mode === 'copy') {
    (clonedTemplate.main as FormTemplateSection).fields.moduleName.readOnly = true;
  }

  return clonedTemplate;
}

export function adaptListenersIpAddressPorts(tlsProxies: TlsProxy[]): ListenersIpAddressPortsType {
  return (tlsProxies || [])
    .map(selectListeners)
    .reduce(flatListeners, [])
    .reduce(groupPortsByIpAddress, {});
}

export function adaptDestinationsIpAddressesOptions(
  listenersIpAddressPorts: ListenersIpAddressPortsType,
): TextSuggestType[] {
  return Object.keys(listenersIpAddressPorts).map((ipAddress) => ({
    label: ipAddress,
    value: ipAddress,
  }));
}

export function adaptDestinationsPortsOptions(
  ipAddress: string,
  listenersIpAddressPorts: ListenersIpAddressPortsType,
): TextSuggestType[] {
  const ipAddressPorts = (listenersIpAddressPorts || {})[ipAddress] || [];
  return ipAddressPorts.map((port: string) => ({ label: port, value: port }));
}

export const adaptNodeNameIpAddressOptions = (selectedNodeId: string, nodeNames: NodeData[]): TextSuggestType[] => {
  const selectedNode = nodeNames.find((node) => node.nodeName === selectedNodeId);
  if (!selectedNode) {
    return [];
  }

  return [
    {
      label: selectedNode.ipAddress,
      value: selectedNode.ipAddress,
    },
  ];
};

export const adaptDestinationsIpAddressPortTemplate = (
  moduleName: string,
  destinationsTemplate: FormTemplateSection,
): FormTemplateSection => {
  const clonedDestinationsTemplate = cloneDeep(destinationsTemplate);

  if (moduleName !== 'XCF3') {
    (clonedDestinationsTemplate as FormTemplateSection).fields.ipAddress.type = 'text';
    (clonedDestinationsTemplate as FormTemplateSection).fields.port.type = 'text';
  } else {
    (clonedDestinationsTemplate as FormTemplateSection).fields.ipAddress.type = 'textSuggest';
    (clonedDestinationsTemplate as FormTemplateSection).fields.port.type = 'textSuggest';
  }

  return clonedDestinationsTemplate;
};

export const adaptAllowedIPAddressesLabel = (
  afWhitelist: DistributorConfigurationListenerAfWhiteList[],
): string => {

  if (!afWhitelist || !afWhitelist?.length) {
    return 'All IP addresses allowed';
  }

  if (afWhitelist?.length === 1) {
    return `${afWhitelist?.length} IP address allowed`;
  }

  return `${afWhitelist?.length} IP addresses allowed`;

};

function selectListeners(item:TlsProxy) {
  return item.listeners;
}

function flatListeners(previous:TlsProxyListener[], current:TlsProxyListener[]):TlsProxyListener[] {
  return [...previous, ...current];
}

function groupPortsByIpAddress(
  groupObj: ListenersIpAddressPortsType,
  item: TlsProxyListener,
): ListenersIpAddressPortsType {

  const { ipAddress, port } = item;
  const currentIpAddressPorts = groupObj[ipAddress] || [];
  const updatedIpAddressPorts = [...currentIpAddressPorts, port];

  return { ...groupObj, [ipAddress]: updatedIpAddressPorts };

}

function setAllFieldsAsReadOnlyExcludingFields(template: FormTemplate, excludingFields: string[]) {
  Object.entries(template).forEach(([_templateKey, templateEntry]) => {
    if (typeof templateEntry === 'string') {
      return;
    }
    Object.entries(templateEntry.fields).forEach(([fieldKey, fieldEntry]) => {
      if (!excludingFields.includes(fieldKey)) {
        fieldEntry.readOnly = true;
      }
    });
  });
}
