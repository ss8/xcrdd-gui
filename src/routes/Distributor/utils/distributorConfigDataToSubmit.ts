import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';
import { Operation, AuditDetails } from 'data/types';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';

export function getAuditDetailsToSubmit(
  distributorConfigFormData: DistributorConfiguration,
  OriginaldistributorConfigFormData: DistributorConfiguration | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
  operation: Operation,
): AuditDetails {

  let actionType = AuditActionType.DISTRIBUTOR_CONFIGURATION_ADD;
  let isEdit = false;
  if (operation === 'UPDATE') {
    actionType = AuditActionType.DISTRIBUTOR_CONFIGURATION_EDIT;
    isEdit = true;
  }

  const auditDetails = getAuditDetails(
    isAuditEnabled,
    userId,
    userName,
    getAuditFieldInfo(distributorConfigFormData, null, isEdit, OriginaldistributorConfigFormData),
    distributorConfigFormData.name,
    AuditService.DISTRIBUTOR_CONFIGS,
    actionType,
  );
  return auditDetails as AuditDetails;
}
