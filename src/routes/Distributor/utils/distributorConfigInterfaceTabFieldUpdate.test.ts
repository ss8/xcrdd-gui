import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import cloneDeep from 'lodash.clonedeep';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import { handleInterfacesFieldUpdate } from './distributorConfigInterfaceTabFieldUpdate';

describe('distributorConfigInterfaceTabFieldUpdate', () => {
  describe('handleInterfacesFieldUpdate()', () => {
    let originalDistributorConfiguration: DistributorConfiguration;
    let distributorConfiguration: DistributorConfiguration;

    beforeEach(() => {
      [originalDistributorConfiguration] = mockDistributorsConfiguration;
      distributorConfiguration = cloneDeep(originalDistributorConfiguration);
    });

    it('should reset the afProtocol for all listeners if moduleName is changed', () => {
      handleInterfacesFieldUpdate(distributorConfiguration, 'moduleName');
      expect(distributorConfiguration).toEqual({
        ...originalDistributorConfiguration,
        listeners: [{
          ...originalDistributorConfiguration.listeners[0],
          afProtocol: '',
        }],
      });
    });

    it('should reset the afProtocol for all listeners if instanceId is changed', () => {
      handleInterfacesFieldUpdate(distributorConfiguration, 'instanceId');
      expect(distributorConfiguration).toEqual({
        ...originalDistributorConfiguration,
        listeners: [{
          ...originalDistributorConfiguration.listeners[0],
          afProtocol: '',
        }],
      });
    });

    it('should reset the afProtocol for all listeners if afType is changed', () => {
      handleInterfacesFieldUpdate(distributorConfiguration, 'afType');
      expect(distributorConfiguration).toEqual({
        ...originalDistributorConfiguration,
        listeners: [{
          ...originalDistributorConfiguration.listeners[0],
          afProtocol: '',
        }],
      });
    });

    it('should do nothing otherwise', () => {
      handleInterfacesFieldUpdate(distributorConfiguration, 'name');
      expect(distributorConfiguration).toEqual({
        ...originalDistributorConfiguration,
      });
    });
  });
});
