import React, { useEffect, useState, useRef } from 'react';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import DistributorConfigForm from 'routes/Distributor/components/DistributorConfigForm';
import { DistributorConfigFormRef } from 'routes/Distributor/components/DistributorConfigForm/DistributorConfigForm';
import * as distributorConfigSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { RootState } from 'data/root.reducers';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorSelectors from 'data/distributor/distributor.selectors';
import * as nodesSelectors from 'data/nodes/nodes.selectors';
import { ConnectedProps, connect } from 'react-redux';
import * as distributorActions from 'data/distributor/distributor.actions';
import * as nodesActions from 'data/nodes/nodes.actions';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import { adaptToUpdate } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as tlsProxySelectors from 'data/tlsProxy/tlsProxy.selectors';
import * as tlsProxyActions from 'data/tlsProxy/tlsProxy.actions';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import { distributorTemplate } from '../../templates';
import { adaptTemplateVisibilityMode } from '../../utils/distributorConfigTemplate.adapter';
import { getAuditDetailsToSubmit } from '../../utils/distributorConfigDataToSubmit';

type RouterParams = {
  history: string
}
const mapState = (state: RootState) => ({
  deliveryFunctionId: state.global.selectedDF,
  selectedDistributorConfig: distributorConfigSelectors.getSelectedDistributorConfiguration(state)[0],
  distributorConfig: distributorConfigSelectors.getDistributorConfiguration(state),
  distributors: distributorSelectors.getDistributors(state),
  nodes: nodesSelectors.getNodes(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  originalDistributorConfig: distributorConfigSelectors.getSelectedDistributorConfiguration(state)[0],
  tlsProxies: tlsProxySelectors.getTlsProxyList(state),
  availableAccessFunction: supportedFeaturesSelectors.getAvailableAccessFunction(state),
  enabledAccessFunction: supportedFeaturesSelectors.getEnabledAccessFunction(state),
});
const mapDispatch = {
  getDistributors: distributorActions.getDistributors,
  getNodes: nodesActions.getNodes,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  goToDistributorConfigurationList: distributorConfigActions.goToDistributorConfigList,
  updateDistributorConfiguration: distributorConfigActions.updateDistributorConfiguration,
  getDistributorConfiguration: distributorConfigActions.getDistributorConfiguration,
  updateAndCopyDistributorConfiguration: distributorConfigActions.updateAndCopyDistributorConfiguration,
  fetchTlsProxyListWithView: tlsProxyActions.fetchTlsProxyListWithView,
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

export const DistributorConfigEdit = ({
  history,
  selectedDistributorConfig,
  distributorConfig,
  distributors,
  nodes,
  deliveryFunctionId,
  getDistributors,
  getNodes,
  securityInfos,
  getSecurityInfos,
  goToDistributorConfigurationList,
  updateDistributorConfiguration,
  userId,
  userName,
  isAuditEnabled,
  originalDistributorConfig,
  getDistributorConfiguration,
  updateAndCopyDistributorConfiguration,
  tlsProxies,
  fetchTlsProxyListWithView,
  availableAccessFunction,
  enabledAccessFunction,
}: Props): JSX.Element => {
  const formRef = useRef<DistributorConfigFormRef>(null);
  const [formData, setFormData] = useState<DistributorConfiguration>();
  const id = selectedDistributorConfig?.id;
  const owner = selectedDistributorConfig?.owner ?? '';
  const template = adaptTemplateVisibilityMode(distributorTemplate, 'edit', owner);

  useEffect(() => {
    if (id) {
      getDistributorConfiguration(deliveryFunctionId, id);
      fetchTlsProxyListWithView(deliveryFunctionId);
    }
  }, [deliveryFunctionId, id, getDistributorConfiguration, fetchTlsProxyListWithView]);

  useEffect(() => {
    if (distributorConfig && distributorConfig.id === id) {
      setFormData({ ...distributorConfig });
    }
  }, [distributorConfig]);

  useEffect(() => {
    getDistributors(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
  }, [deliveryFunctionId, getDistributors, getNodes, getSecurityInfos]);

  const getCopyDistributorActionPayload = () => {
    if (formRef.current === undefined || formRef.current === null) return null;

    if (!formRef.current.isValid(true, true)) {
      return null;
    }

    const distributorConfigFormData = formRef.current.getValues();

    return {
      data: adaptToUpdate(distributorConfigFormData, distributorConfig),
      auditDetails: getAuditDetailsToSubmit(
        distributorConfigFormData, originalDistributorConfig, isAuditEnabled, userId, userName, 'UPDATE',
      ),
    };
  };

  const handleClickSave = () => {
    const payload = getCopyDistributorActionPayload();
    if (payload) {
      updateDistributorConfiguration(history, deliveryFunctionId, id, payload);
    }
  };

  const handleClickSaveAndCopy = () => {
    const payload = getCopyDistributorActionPayload();
    if (payload) {
      updateAndCopyDistributorConfiguration(history, deliveryFunctionId, id, payload);
    }
  };

  const handleCancel = () => goToDistributorConfigurationList(history);
  const renderForm = () => {
    if (formData && template && distributors?.ss8Distributors && securityInfos) {
      return (
        <DistributorConfigForm
          ref={formRef}
          distributorConfig={formData}
          template={template}
          distributors={distributors}
          nodes={nodes}
          securityInfos={securityInfos}
          isEditing={owner !== 'lis'}
          tlsProxies={tlsProxies}
          availableAccessFunction={availableAccessFunction}
          enabledAccessFunction={enabledAccessFunction}
        />
      );
    }
  };
  const renderFormPanel = (formTitle:string) => {
    return (
      <FormPanel title={formTitle}>
        <FormButtons
          isEditing
          onClickCancel={handleCancel}
          onClickSave={handleClickSave}
          onClickClose={handleCancel}
          onClickSaveAndCopy={handleClickSaveAndCopy}
        />
      </FormPanel>
    );
  };

  return (
    <FormLayout>
      <FormLayoutContent>
        {renderFormPanel('Edit Distributor Configuration')}
        {renderForm()}
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel('')}
      </FormLayoutFooter>
    </FormLayout>
  );
};

export default withRouter(connector(DistributorConfigEdit));
