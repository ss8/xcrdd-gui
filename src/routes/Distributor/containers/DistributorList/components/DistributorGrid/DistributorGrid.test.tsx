import React from 'react';
import { Provider } from 'react-redux';
import { mount, ReactWrapper } from 'enzyme';
import Http from 'shared/http';
import MockAdapter from 'axios-mock-adapter';
import store from 'data/store';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { ensureGridApiHasBeenSet } from '__test__/utils';
import { distributorColumnDefs } from 'routes/Distributor/Distributor.types';
import DistributorGrid from './DistributorGrid';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<DistributorGrid />', () => {
  let wrapper: ReactWrapper;
  let data: DistributorConfiguration[];

  beforeEach(() => {
    data = [{
      id: 'id1',
      name: 'DC-NAME-1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      listeners: [{
        id: 'listeners1',
        afProtocol: 'afProtocol1',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '10',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'UDP',
        afWhitelist: [],
      }],
      destinations: [],
    }, {
      id: 'id2',
      name: 'DC-NAME-2',
      instanceId: 'instanceId2',
      moduleName: 'moduleName2',
      afType: 'afType1',
      listeners: [{
        id: 'listeners2',
        afProtocol: '',
        ipAddress: '',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '10',
        port: '1234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [],
      }],
      destinations: [],
    }];

    axiosMock.onAny().reply(200);
  });

  describe('when user has privileges to view, edit, create', () => {
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();

    beforeEach(async () => {
      wrapper = mount(
        <Provider store={store}>
          <DistributorGrid
            hasPrivilegesToCreate
            hasPrivilegeToDelete
            hasPrivilegeToView
            data={data}
            noResults={!data.length}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={() => false}
            onSaveGridSettings={() => false}
            onGetGridSettings={() => Promise.resolve({ value: { data: { data: [] } } })}
            onSelectItemMenuAction={() => false}
            onShowActionMenuItem={() => false}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={distributorColumnDefs}
          />
        </Provider>,
      );

      await ensureGridApiHasBeenSet(wrapper);
    });

    it('should call handleCreateNew when clicking in the create new button', () => {
      wrapper.find({ text: 'New SS8 Distributor Configuration' }).simulate('click');
      expect(handleCreateNew).toHaveBeenCalled();
    });

    it('should call handleRefresh when clicking in the refresh button', () => {
      wrapper.find({ icon: 'refresh' }).first().simulate('click');
      expect(handleRefresh).toHaveBeenCalled();
    });

    it('should render the data in the grid component', async () => {
      const gridRows = wrapper.render().find('.ag-center-cols-container .ag-cell[col-id="name"]');
      const names = data.map((item) => item.name);
      const gridNames = gridRows.toArray().map((item) => item.children[0].data);
      expect(gridNames).toEqual(names);
    });

    it('should filter the grid according to the input passed', async () => {
      expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(2);

      const onChange = wrapper.find({ placeholder: 'Search' }).first().prop('onChange');
      if (onChange) onChange({ currentTarget: { value: 'DC-NAME-2' } } as React.ChangeEvent<HTMLInputElement>);

      expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(1);
    });

    it('should render the last updated at', async () => {
      expect(wrapper.render().find('.last-updated-at').text()).toEqual('Last updated at 08/27/20 07:13:39 AM');
    });
  });

  describe('when user has privileges to view only', () => {
    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();

    beforeEach(async () => {
      wrapper = mount(
        <Provider store={store}>
          <DistributorGrid
            hasPrivilegesToCreate={false}
            hasPrivilegeToDelete={false}
            hasPrivilegeToView
            data={data}
            noResults={!data.length}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={() => false}
            onSaveGridSettings={() => false}
            onGetGridSettings={() => Promise.resolve({ value: { data: { data: [] } } })}
            onSelectItemMenuAction={() => false}
            onShowActionMenuItem={() => false}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={distributorColumnDefs}
          />
        </Provider>,
      );

      await ensureGridApiHasBeenSet(wrapper);
    });

    it('should not render create new button', async () => {
      expect(wrapper.find({ text: 'New SS8 Distributor Configuration' })).toHaveLength(0);
    });
  });
});
