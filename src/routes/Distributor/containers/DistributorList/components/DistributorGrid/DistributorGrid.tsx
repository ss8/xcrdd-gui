import React, { FunctionComponent } from 'react';
import { SelectionChangedEvent, ColGroupDef, ColDef } from '@ag-grid-enterprise/all-modules';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import CommonGrid from 'shared/commonGrid';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import { GridSettings } from 'data/types';
import DistributorGridActionMenu from '../DistributorGridActionMenu';

interface Props {
  data: DistributorConfiguration[]
  hasPrivilegeToView: boolean
  hasPrivilegesToCreate: boolean
  hasPrivilegeToDelete: boolean
  noResults: boolean,
  beforeLogout: boolean,
  lastUpdatedAt: Date | undefined,
  columnDefs: (ColGroupDef | ColDef)[],
  onRefresh: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onCreateNew: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onGetGridSettings: () => any
  onSelectionChanged: (event: SelectionChangedEvent) => void
  onSaveGridSettings: (gridSettings: GridSettings) => void
  onSelectItemMenuAction: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  onShowActionMenuItem: (menuId: string, data: DistributorConfiguration[]) => void
}

const DistributorGrid: FunctionComponent<Props> = ({
  hasPrivilegesToCreate,
  hasPrivilegeToDelete,
  hasPrivilegeToView,
  data = [],
  noResults,
  beforeLogout,
  lastUpdatedAt,
  columnDefs,
  onCreateNew,
  onRefresh,
  onSelectionChanged,
  onSaveGridSettings,
  onGetGridSettings,
  onShowActionMenuItem,
  onSelectItemMenuAction,
}: Props) => {
  const frameworkComponents = {
    actionMenuCellRenderer: ActionMenuCellRenderer,
  };

  return (
    <div className="grid-container" style={{ height: 'inherit' }}>
      <CommonGrid
        createNewText="New SS8 Distributor Configuration"
        pagination
        enableBrowserTooltips
        rowSelection={ROWSELECTION.SINGLE}
        hasPrivilegeToCreateNew={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegetoView={hasPrivilegeToView}
        rowData={data}
        noResults={noResults}
        isBeforeLogout={beforeLogout}
        columnDefs={columnDefs}
        frameworkComponents={frameworkComponents}
        actionMenuClass={DistributorGridActionMenu}
        handleCreateNew={onCreateNew}
        onRefresh={onRefresh}
        onSelectionChanged={onSelectionChanged}
        showActionMenuItem={onShowActionMenuItem}
        onActionMenuItemClick={onSelectItemMenuAction}
        saveGridSettings={onSaveGridSettings}
        getGridSettingsFromServer={onGetGridSettings}
        lastUpdatedAt={lastUpdatedAt}
      />
    </div>
  );
};

export default DistributorGrid;
