import React, { FunctionComponent } from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import { DistributorMenuActionsType } from 'routes/Distributor/Distributor.types';

interface Props {
  disabled: boolean;
  context: {
    onActionMenuItemClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    props: {
      showActionMenuItem: boolean,
      hasPrivilegeToCreateNew: boolean;
      hasPrivilegeToDelete: boolean;
      hasPrivilegetoView: boolean;
    }
  };
}

const DistributorGridActionMenu: FunctionComponent<Props> = ({
  disabled,
  context: {
    onActionMenuItemClick,
    props: {
      showActionMenuItem,
      hasPrivilegetoView,
      hasPrivilegeToCreateNew,
      hasPrivilegeToDelete,
    },
  },
}: Props) => {
  return (
    <Menu>
      {hasPrivilegetoView && showActionMenuItem && (
        <MenuItem
          id={DistributorMenuActionsType.View}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="View"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem && (
        <MenuItem
          id={DistributorMenuActionsType.Edit}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Edit"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem && (
        <MenuItem
          id={DistributorMenuActionsType.Copy}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Copy"
        />
      )}
      {hasPrivilegeToDelete && showActionMenuItem && (
        <MenuItem
          id={DistributorMenuActionsType.Delete}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Delete"
        />
      )}
    </Menu>
  );
};

export default DistributorGridActionMenu;
