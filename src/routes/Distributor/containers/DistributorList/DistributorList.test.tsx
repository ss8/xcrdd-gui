import React from 'react';
import { Provider } from 'react-redux';
import { mount, ReactWrapper } from 'enzyme';
import MockAdapter from 'axios-mock-adapter';
import { render, waitFor, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import {
  DistributorConfiguration,
  API_PATH_DISTRIBUTOR_CONFIGURATIONS,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import {
  buildServerResponse,
  ensureGridApiHasBeenSet,
  buildAxiosServerResponse,
} from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import store from 'data/store';
import Http from 'shared/http';
import { UserSettings } from 'data/types';
import AuthActions from 'data/authentication/authentication.types';
import Distributor from 'routes/Distributor/Distributor';
import { DISTRIBUTORS_ROUTE } from '../../../../constants';
import DistributorList from './DistributorList';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<DistributorList />', () => {
  let distributorConfiguration1: DistributorConfiguration;
  let distributorConfiguration2: DistributorConfiguration;
  let distributorConfiguration3: DistributorConfiguration;
  let userSettings: UserSettings;
  let wrapper: ReactWrapper;

  beforeEach(() => {
    distributorConfiguration1 = {
      id: 'id1',
      name: 'name1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      listeners: [{
        id: 'listeners1',
        afProtocol: 'afProtocol',
        ipAddress: '11.11.11.11',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '1134',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '11.11.11.11',
          afName: 'afName',
        }],
      }],
      destinations: [],
    };

    distributorConfiguration2 = {
      id: 'id2',
      name: 'name2',
      instanceId: 'instanceId2',
      moduleName: 'moduleName2',
      afType: 'afType2',
      listeners: [{
        id: 'listeners2',
        afProtocol: 'afProtocol',
        ipAddress: '22.22.22.22',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '2234',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId2',
          afAddress: '22.22.22.22',
          afName: 'afName',
        }],
      }],
      destinations: [],
    };

    distributorConfiguration3 = {
      id: 'id3',
      name: 'name3',
      instanceId: 'instanceId3',
      moduleName: 'moduleName3',
      afType: 'afType3',
      listeners: [{
        id: 'listeners3',
        afProtocol: 'afProtocol',
        ipAddress: '33.33.33.33',
        keepaliveTimeout: '20',
        maxConcurrentConnection: '30',
        port: '3334',
        requiredState: 'ACTIVE',
        secInfoId: '',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId3',
          afAddress: '33.33.33.33',
          afName: 'afName',
        }],
      }],
      destinations: [],
    };

    userSettings = {
      dateUpdated: '2020-07-28T17:08:46.000+0000',
      id: 'id1',
      name: 'DistributorConfigurationSettings',
      setting: '[]',
      type: 'json',
      user: 'userId1',
    };

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['dc_access'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    const response = buildServerResponse([distributorConfiguration1, distributorConfiguration2]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);

    const userSettingsResponse = buildServerResponse([userSettings]);
    axiosMock.onGet('/users/userId1/settings?name=DistributorConfigurationSettings').reply(200, userSettingsResponse);

    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
  });

  it('should render the empty list if no distributor configuration', () => {
    const response = buildServerResponse([]);
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);
    wrapper = mount(<Provider store={store}><MemoryRouter><DistributorList /></MemoryRouter></Provider>);
    expect(wrapper.render().find('.ag-overlay-no-rows-center').text()).toEqual('No Rows To Show');
  });

  it('should render the list of distributor configuration', async () => {
    wrapper = mount(<Provider store={store}><MemoryRouter><DistributorList /></MemoryRouter></Provider>);
    await ensureGridApiHasBeenSet(wrapper);
    expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(2);
  });

  it('should refresh the list of distributor configuration', async () => {
    wrapper = mount(<Provider store={store}><MemoryRouter><DistributorList /></MemoryRouter></Provider>);
    await ensureGridApiHasBeenSet(wrapper);

    expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(2);

    const response = buildServerResponse(
      [distributorConfiguration1, distributorConfiguration2, distributorConfiguration3],
    );
    axiosMock.onGet(`${API_PATH_DISTRIBUTOR_CONFIGURATIONS}/0`).reply(200, response);

    const onClick = wrapper.find('[icon="refresh"]').at(0).prop('onClick');
    if (onClick) onClick({} as React.MouseEvent);

    await new Promise((resolve) => setTimeout(resolve, 500));

    expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(3);
  });

  it('should render the last updated at', async () => {
    wrapper = mount(<Provider store={store}><MemoryRouter><DistributorList /></MemoryRouter></Provider>);
    await ensureGridApiHasBeenSet(wrapper);

    expect(wrapper.render().find('.last-updated-at').text()).toMatch(/Last updated at/);
  });
});
