import React, {
  FunctionComponent, useEffect, useState, Fragment,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { SelectionChangedEvent, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { RootState } from 'data/root.reducers';
import { GridSettings, WithAuditDetails, AuditDetails } from 'data/types';
import {
  DistributorConfigurationPrivileges,
  DISTRIBUTOR_CONFIGURATION_USER_CATEGORY_SETTINGS,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import * as actions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as selectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import * as userActions from 'users/users-actions';
import ModalDialog from 'shared/modal/dialog';
import {
  getAuditDetails, getAuditFieldInfo, AuditService, AuditActionType,
} from 'shared/userAudit/audit-constants';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import { AppToaster } from 'shared/toaster';
import DistributorGrid from './components/DistributorGrid';
import { DistributorMenuActionsType, distributorColumnDefs } from '../../Distributor.types';
import { setDistributorAfTypeValueFormatter } from '../../utils/setDistributorAfTypeValueFormatter';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  userId: state.authentication.userId,
  beforeLogout: state.authentication.beforeLogout,
  privileges: state.authentication.privileges,
  deliveryFunctionId: state.global.selectedDF,
  data: selectors.getDistributorConfigurations(state),
  selectedDistributorConfiguration: selectors.getSelectedDistributorConfiguration(state),
  userDBId: state.authentication.userDBId,
  isAuditEnabled: state.discoveryXcipio?.discoveryXcipioSettings?.isAuditEnabled,
  enabledAccessFunctions: supportedFeaturesSelectors.getEnabledAccessFunction(state),
});

const mapDispatch = {
  getDistributorConfigurationsWithView: actions.getDistributorConfigurationsWithView,
  getGridSettings: userActions.getUserSettings,
  saveGridSettings: userActions.putUserSettings,
  goToDistributorConfigCreate: actions.goToDistributorConfigCreate,
  goToDistributorConfigEdit: actions.goToDistributorConfigEdit,
  goToDistributorConfigCopy: actions.goToDistributorConfigCopy,
  goToDistributorConfigView: actions.goToDistributorConfigView,
  selectDistributorConfiguration: actions.selectDistributorConfiguration,
  deleteDistributorConfiguration: actions.deleteDistributorConfiguration,
  getSupportedFeature: warrantActions.getWarrantFormConfig,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter

export const DistributorList: FunctionComponent<Props> = ({
  history,
  userId,
  userDBId,
  beforeLogout,
  privileges,
  deliveryFunctionId,
  data,
  selectedDistributorConfiguration,
  enabledAccessFunctions,
  getDistributorConfigurationsWithView,
  selectDistributorConfiguration,
  deleteDistributorConfiguration,
  getGridSettings,
  saveGridSettings,
  goToDistributorConfigCreate,
  goToDistributorConfigEdit,
  goToDistributorConfigCopy,
  goToDistributorConfigView,
  isAuditEnabled,
  getSupportedFeature,
}: Props) => {
  const [lastUpdatedAt, setLastUpdatedAt] = useState<Date>();
  const [columnDefs, setColumnDefs] = useState<ColGroupDef[]>(distributorColumnDefs);
  const [isDeletingDistributorConfiguration, setIsDeletingDistributorConfiguration] = useState(false);
  const [deleteDialogCustomComponent, setDeleteDialogCustomComponent] =
    useState<FunctionComponent>(() => null);
  const [deleteDialogTitle, setDeleteDialogTitle] = useState('');
  const [deleteDialogActionText, setDeleteDialogActionText] = useState('');

  useEffect(() => {
    getSupportedFeature(deliveryFunctionId);
    getDistributorConfigurationsWithView(deliveryFunctionId);
    return () => {
      AppToaster.clear();
    };
  }, [deliveryFunctionId, getDistributorConfigurationsWithView, getSupportedFeature]);

  useEffect(() => {
    setLastUpdatedAt(new Date());
  }, [data]);

  useEffect(() => {
    const updatedColumnDefs = setDistributorAfTypeValueFormatter(enabledAccessFunctions, columnDefs);
    setColumnDefs(updatedColumnDefs);
  }, [enabledAccessFunctions]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleRefresh = () => getDistributorConfigurationsWithView(deliveryFunctionId);

  const handleCreateNew = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    goToDistributorConfigCreate(history);
  };

  const handleSelectionChanged = (event: SelectionChangedEvent) => {
    selectDistributorConfiguration(event.api.getSelectedRows());
  };

  const handleSaveGridSettings = (gridSettings: GridSettings) =>
    saveGridSettings(DISTRIBUTOR_CONFIGURATION_USER_CATEGORY_SETTINGS, userId, gridSettings);

  const handleGetGridSettings = () => getGridSettings(DISTRIBUTOR_CONFIGURATION_USER_CATEGORY_SETTINGS, userId);

  const handleShowActionMenuItem = () => true;

  const handleSelectItemMenuAction = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const { id, name } = selectedDistributorConfiguration[0];
    if (event?.type === DistributorMenuActionsType.Delete) {
      setIsDeletingDistributorConfiguration(true);
      setDeleteDialogTitle('Delete SS8 Distributor Configuration');
      setDeleteDialogCustomComponent(() => () => (
        <Fragment>
          <span>Are you sure you want to delete Distributor Configuration
          </span> &#39;{name}&#39;?
          <br />
          All the associated Allowed AF IP Address list may also be deleted.
        </Fragment>
      ));
      setDeleteDialogActionText('Yes, delete it');
    } else if (event?.type === DistributorMenuActionsType.Edit) {
      goToDistributorConfigEdit(history, id);
    } else if (event?.type === DistributorMenuActionsType.View) {
      goToDistributorConfigView(history, id);
    } else if (event?.type === DistributorMenuActionsType.Copy) {
      goToDistributorConfigCopy(history, id);
    }
  };

  const handleCancelIsDeletingDistributorConfig = () => setIsDeletingDistributorConfiguration(false);

  const handleDeleteDistributorConfiguration = () => {
    const distributorConfigrationId = selectedDistributorConfiguration[0]?.id;
    if (distributorConfigrationId == null) {
      return;
    }

    const body: WithAuditDetails<void> = { data: undefined };

    if (isAuditEnabled) {
      const auditDetails = getAuditDetails(
        isAuditEnabled,
        userDBId,
        userId,
        getAuditFieldInfo(null),
        selectedDistributorConfiguration[0]?.name,
        AuditService.DISTRIBUTOR_CONFIGS,
        AuditActionType.DISTRIBUTOR_CONFIGURATION_DELETE,
      );
      body.auditDetails = auditDetails as AuditDetails;
    }

    deleteDistributorConfiguration(deliveryFunctionId, distributorConfigrationId, body);
  };

  const hasPrivilegeToView = privileges.includes(DistributorConfigurationPrivileges.View);
  const hasPrivilegesToCreate = privileges.includes(DistributorConfigurationPrivileges.Edit);
  const hasPrivilegeToDelete = privileges.includes(DistributorConfigurationPrivileges.Delete);

  return (
    <Fragment>
      <DistributorGrid
        data={data}
        hasPrivilegeToView={hasPrivilegeToView}
        hasPrivilegesToCreate={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        noResults={false}
        beforeLogout={beforeLogout}
        columnDefs={columnDefs}
        onRefresh={handleRefresh}
        onCreateNew={handleCreateNew}
        onGetGridSettings={handleGetGridSettings}
        onSelectionChanged={handleSelectionChanged}
        onSaveGridSettings={handleSaveGridSettings}
        onSelectItemMenuAction={handleSelectItemMenuAction}
        onShowActionMenuItem={handleShowActionMenuItem}
        lastUpdatedAt={lastUpdatedAt}
      />
      <ModalDialog
        isOpen={isDeletingDistributorConfiguration}
        customComponent={deleteDialogCustomComponent}
        title={deleteDialogTitle}
        width="500px"
        actionText={deleteDialogActionText}
        onSubmit={handleDeleteDistributorConfiguration}
        onClose={handleCancelIsDeletingDistributorConfig}
      />
    </Fragment>
  );
};

export default withRouter(connector(DistributorList));
