import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getByDisplayValue,
  getByLabelText,
} from '@testing-library/react';
import AuthActions from 'data/authentication/authentication.types';
import { DistributorConfiguration, SELECT_DISTRIBUTOR_CONFIGURATION } from 'data/distributorConfiguration/distributorConfiguration.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDistributors } from '__test__/mockDistributors';
import { mockNodes } from '__test__/mockNodes';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import DistributorConfigView from './DistributorConfigView';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<DistributorConfigView />', () => {
  let distributorConfig: DistributorConfiguration;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['dc_access'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    distributorConfig = {
      id: 'ID_TO_GET',
      name: 'DC Name to view',
      moduleName: 'XCF3',
      instanceId: '1',
      afType: 'STA_PGW',
      nodeName: 'node-1',
      listeners: [
        {
          id: '1',
          afProtocol: 'CISCO_PGW',
          ipAddress: '1.1.1.1',
          port: '45160',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        },
      ],
      destinations: [
        {
          id: '1',
          ipAddress: '2.2.2.2',
          port: '45160',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
          status: 'INACTIVE',
        },
      ],
    };

    store.dispatch({
      type: SELECT_DISTRIBUTOR_CONFIGURATION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    axiosMock.onGet('/distributorConfigs/0/ID_TO_GET').reply(200, { data: [distributorConfig] });
    axiosMock.onGet('/distributors/0').reply(200, { data: mockDistributors });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });

    axiosMock.onAny().reply((config) => {
      return [200, { data: [] }];
    });
  });

  it('should render the form title with Edit and Back buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigView />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('View Distributor Configuration')).toBeVisible();
    let items = screen.getAllByText('Edit');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Back');
    expect(items).toHaveLength(2);
  });

  it('should load the data retrieved from the server', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Distributor Configuration')).toBeVisible());
    expect(screen.getByLabelText('Configuration Name:*')).toHaveValue('DC Name to view');
    expect(screen.getByLabelText('Module Name:*')).toHaveDisplayValue('XCF3');
    expect(screen.getByLabelText('Instance ID:*')).toHaveDisplayValue('1');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Cisco/Starent PGW');
    expect(screen.getByLabelText('Xcipio Node:*')).toHaveDisplayValue('node-1 (XCF3)');

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listenerTab1 = screen.getAllByRole('tabpanel')[0];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[0]);

    expect(getByLabelText(listenerTab1, 'Listener Type:*')).toHaveDisplayValue('CISCO_PGW');
    expect(getByDisplayValue(listenerTab1, '1.1.1.1')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '300')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '128')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TCP')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'Active')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TPKT')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '2')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[1]);

    expect(getByDisplayValue(destinationTab1, '2.2.2.2')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '60')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '6')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '2')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'ZPUSH')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Passthrough with SX3 header')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Active')).toBeVisible();
  });

});
