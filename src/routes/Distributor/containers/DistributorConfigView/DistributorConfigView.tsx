import React, {
  FunctionComponent,
  useState,
  useEffect,
  useRef,
} from 'react';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import DistributorConfigForm from 'routes/Distributor/components/DistributorConfigForm';
import { DistributorConfigFormRef } from 'routes/Distributor/components/DistributorConfigForm/DistributorConfigForm';
import * as distributorConfigSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { RootState } from 'data/root.reducers';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorSelectors from 'data/distributor/distributor.selectors';
import * as nodesSelectors from 'data/nodes/nodes.selectors';
import { ConnectedProps, connect } from 'react-redux';
import * as distributorActions from 'data/distributor/distributor.actions';
import * as nodesActions from 'data/nodes/nodes.actions';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import { distributorTemplate } from '../../templates';
import { adaptTemplateVisibilityMode } from '../../utils/distributorConfigTemplate.adapter';

type RouterParams = {
  history: string
}
const mapState = (state: RootState) => ({
  deliveryFunctionId: state.global.selectedDF,
  selectedDistributorConfig: distributorConfigSelectors.getSelectedDistributorConfiguration(state)[0],
  distributorConfig: distributorConfigSelectors.getDistributorConfiguration(state),
  distributors: distributorSelectors.getDistributors(state),
  nodes: nodesSelectors.getNodes(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  template: adaptTemplateVisibilityMode(distributorTemplate, 'view'),
  availableAccessFunction: supportedFeaturesSelectors.getAvailableAccessFunction(state),
  enabledAccessFunction: supportedFeaturesSelectors.getEnabledAccessFunction(state),
});
const mapDispatch = {
  getDistributors: distributorActions.getDistributors,
  getNodes: nodesActions.getNodes,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  goToDistributorConfigList: distributorConfigActions.goToDistributorConfigList,
  goToDistributorConfigEdit: distributorConfigActions.goToDistributorConfigEdit,
  getDistributorConfiguration: distributorConfigActions.getDistributorConfiguration,
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

export const DistributorConfigView : FunctionComponent<Props> = ({
  history,
  selectedDistributorConfig,
  distributorConfig,
  template,
  distributors,
  nodes,
  deliveryFunctionId,
  securityInfos,
  getDistributors,
  getNodes,
  getSecurityInfos,
  goToDistributorConfigList,
  goToDistributorConfigEdit,
  getDistributorConfiguration,
  availableAccessFunction,
  enabledAccessFunction,
}: Props) => {
  const title = 'View Distributor Configuration';
  const formRef = useRef<DistributorConfigFormRef>(null);
  const [formData, setFormData] = useState<DistributorConfiguration>();
  const id = selectedDistributorConfig?.id;
  useEffect(() => {
    getDistributors(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
  }, [deliveryFunctionId, getDistributors, getNodes, getSecurityInfos]);

  useEffect(() => {
    getDistributorConfiguration(deliveryFunctionId, id);
  }, [deliveryFunctionId, id, getDistributorConfiguration]);

  useEffect(() => {
    if (distributorConfig && distributorConfig.id === id) {
      setFormData({ ...distributorConfig });
    }
  }, [distributorConfig]);

  const handleClickEdit = () => goToDistributorConfigEdit(history, id);

  const handleCancel = () => goToDistributorConfigList(history);

  const renderForm = () => {
    if (formData && template && distributors?.ss8Distributors && securityInfos) {
      return (
        <DistributorConfigForm
          ref={formRef}
          distributorConfig={formData}
          template={template}
          distributors={distributors}
          nodes={nodes}
          securityInfos={securityInfos}
          isEditing={false}
          tlsProxies={[]}
          availableAccessFunction={availableAccessFunction}
          enabledAccessFunction={enabledAccessFunction}
        />
      );
    }
  };
  const renderFormPanel = (formTitle:string) => {
    return (
      <FormPanel title={formTitle}>
        <FormButtons
          isEditing={false}
          onClickCancel={handleCancel}
          onClickEdit={handleClickEdit}
          onClickClose={handleCancel}
        />
      </FormPanel>
    );
  };

  return (
    <FormLayout>
      <FormLayoutContent>
        {renderFormPanel(title)}
        {renderForm()}
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel('')}
      </FormLayoutFooter>
    </FormLayout>
  );
};

export default withRouter(connector(DistributorConfigView));
