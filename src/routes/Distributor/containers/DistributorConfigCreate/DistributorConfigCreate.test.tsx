import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getAllByDisplayValue,
} from '@testing-library/react';
import Distributor from 'routes/Distributor/Distributor';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDistributors } from '__test__/mockDistributors';
import { mockNodes } from '__test__/mockNodes';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { getByDataTest } from '__test__/customQueries';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import DistributorConfigCreate from './DistributorConfigCreate';
import { DISTRIBUTORS_ROUTE, SYSTEM_SETTINGS_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<DistributorConfigCreate />', () => {
  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['dc_access'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    axiosMock.onGet('/distributors/0').reply(200, { data: mockDistributors });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });

    axiosMock.onAny().reply((config) => {
      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('New Distributor Configuration')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of distributor configuration when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/create`]}>
          <Distributor />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New SS8 Distributor Configuration')).toBeVisible();
  });

  it('should show Node Name if Module Name is XCF3', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Distributor Configuration')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'DC Name 1' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a module')[0], { target: { value: 'XCF3' } });
    fireEvent.change(screen.getAllByDisplayValue('Select an instance ID')[0], { target: { value: '1' } });
    fireEvent.change(screen.getAllByDisplayValue('Select an AF type')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a Node')[0], { target: { value: 'node-1' } });

    expect(screen.getByDisplayValue('node-1 (XCF3)')).toBeVisible();
  });

  it('should filter the list of Module Names', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Distributor Configuration')).toBeVisible());

    const moduleNameSelect = screen.getByLabelText('Module Name:*');
    expect(moduleNameSelect.querySelectorAll('option')).toHaveLength(20);

    const options: string[] = [];
    moduleNameSelect.querySelectorAll('option').forEach((option) => options.push(option.label));
    expect(options).toEqual([
      'Select a module',
      '5GXCF3',
      'CSCOPGWX3IM',
      'E103221X2IM',
      'E103221X3IM',
      'ERK5GAMFX2IM',
      'ERKSMFX2IM',
      'ERKUPFX3IM',
      'ETSIGIZMOX2IM',
      'ETSIGIZMOX3IM',
      'MV5GSMSCX2IM',
      'NOK5GAMFX2IM',
      'NOK5GSMFX2IM',
      'NOK5GSMSFX2IM',
      'NOK5GUDMX2IM',
      'NOKIASRX3IM',
      'NSNSDMX2IM',
      'RADAFIM',
      'RADX2IM',
      'XCF3',
    ]);
  });

  it('should show an error message if duplicated ip:port', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Distributor Configuration')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'DC Name 1' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a module')[0], { target: { value: 'ERKUPFX3IM' } });
    fireEvent.change(screen.getAllByDisplayValue('Select an instance ID')[0], { target: { value: '1' } });
    fireEvent.change(screen.getAllByDisplayValue('Select an AF type')[0], { target: { value: 'ERK_UPF' } });

    const listenerTab1 = screen.getAllByRole('tabpanel')[0];

    expect(screen.getByText('Listener 1')).toBeVisible();
    fireEvent.change(getAllByDisplayValue(listenerTab1, 'Select a listener type')[0], { target: { value: 'ERKUPF_X3' } });
    fireEvent.change(getAllByDisplayValue(listenerTab1, '')[0], { target: { value: '1.1.1.1' } });

    const listenerTabList = screen.getAllByRole('tablist')[0];
    const addListenerButton = listenerTabList.querySelector('button') as HTMLButtonElement;
    fireEvent.click(addListenerButton);
    expect(screen.getByText('Listener 2')).toBeVisible();

    const listenerTab2 = screen.getAllByRole('tabpanel')[0];
    fireEvent.change(getAllByDisplayValue(listenerTab2, 'Select a listener type')[0], { target: { value: 'ERKUPF_X3' } });
    fireEvent.change(getAllByDisplayValue(listenerTab2, '')[0], { target: { value: '1.1.1.1' } });

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.change(getAllByDisplayValue(destinationTab1, '')[0], { target: { value: '1.1.1.1' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')[0]).toBeVisible();
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(3);
  });

  it('should show an error message if duplicated ip:port for XCF3 module', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Distributor Configuration')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Configuration Name:*'), { target: { value: 'DC Name 1' } });
    fireEvent.change(screen.getByLabelText('Module Name:*'), { target: { value: 'XCF3' } });
    fireEvent.change(screen.getByLabelText('Instance ID:*'), { target: { value: '1' } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'STA_PGW' } });
    fireEvent.change(screen.getByLabelText('Xcipio Node:*'), { target: { value: 'node-1' } });

    const listenerTab1 = screen.getAllByRole('tabpanel')[0];

    expect(screen.getByText('Listener 1')).toBeVisible();
    fireEvent.change(getAllByDisplayValue(listenerTab1, 'Select a listener type')[0], { target: { value: 'CISCO_PGW' } });
    fireEvent.change(getAllByDisplayValue(listenerTab1, '')[0], { target: { value: '1.1.1.1' } });

    const listenerTabList = screen.getAllByRole('tablist')[0];
    const addListenerButton = listenerTabList.querySelector('button') as HTMLButtonElement;
    fireEvent.click(addListenerButton);
    expect(screen.getByText('Listener 2')).toBeVisible();

    const listenerTab2 = screen.getAllByRole('tabpanel')[0];
    fireEvent.change(getAllByDisplayValue(listenerTab2, 'Select a listener type')[0], { target: { value: 'CISCO_PGW' } });
    fireEvent.change(getAllByDisplayValue(listenerTab2, '')[0], { target: { value: '1.1.1.1' } });

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.change(getAllByDisplayValue(destinationTab1, '')[0], { target: { value: '1.1.1.1' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')[0]).toBeVisible();
  });

  it('should show success message on Save of a valid form for XCF3', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Distributor Configuration')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Configuration Name:*'), { target: { value: 'DC Name 1' } });
    fireEvent.change(screen.getByLabelText('Module Name:*'), { target: { value: 'XCF3' } });
    fireEvent.change(screen.getByLabelText('Instance ID:*'), { target: { value: '1' } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'STA_PGW' } });
    fireEvent.change(screen.getByLabelText('Xcipio Node:*'), { target: { value: 'node-1' } });

    const listenerTab1 = screen.getAllByRole('tabpanel')[0];

    expect(screen.getByText('Listener 1')).toBeVisible();
    fireEvent.change(getAllByDisplayValue(listenerTab1, 'Select a listener type')[0], { target: { value: 'CISCO_PGW' } });
    fireEvent.change(getAllByDisplayValue(listenerTab1, '')[0], { target: { value: '1.1.1.1' } });

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.change(getAllByDisplayValue(destinationTab1, '')[0], { target: { value: '1.1.1.2' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Distributor configuration successfully created.')).toBeVisible());
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Save')[0]);

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  describe('when create and copy a Distributor', () => {
    it('should create a new Distributor and be redirected to copy path', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/create`]}>
            <Distributor />
          </MemoryRouter>
        </Provider>,
      );

      expect(await screen.findByText('New Distributor Configuration')).toBeVisible();

      const distributorConfigurationChecker = {
        newName: 'New DC',
      };

      fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: distributorConfigurationChecker.newName } });
      fireEvent.change(getByDataTest(container, 'input-moduleName'), { target: { value: 'ERKUPFX3IM' } });
      fireEvent.change(getByDataTest(container, 'input-instanceId'), { target: { value: '1' } });
      fireEvent.change(getByDataTest(container, 'input-afType'), { target: { value: 'ERK_UPF' } });
      fireEvent.change(getByDataTest(container, 'input-nodeName'), { target: { value: 'node-1' } });

      const listenerTab1 = screen.getAllByRole('tabpanel')[0];
      fireEvent.change(getByDataTest(listenerTab1, 'input-afProtocol'), { target: { value: 'ERKUPF_X3' } });
      fireEvent.change(listenerTab1.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });

      const destinationTab1 = screen.getAllByRole('tabpanel')[1];
      fireEvent.change(destinationTab1.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.2' } });

      fireEvent.click(screen.getAllByTestId('FormButtonsSaveAndCopy')[0]);

      expect(await screen.findByText('Copy Distributor Configuration')).toBeVisible();
    });
  });

});
