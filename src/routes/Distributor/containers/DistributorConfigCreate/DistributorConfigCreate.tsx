import React, { useEffect, useState, useRef } from 'react';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import DistributorConfigForm from 'routes/Distributor/components/DistributorConfigForm';
import { DistributorConfigFormRef } from 'routes/Distributor/components/DistributorConfigForm/DistributorConfigForm';
import { RootState } from 'data/root.reducers';
import * as distributorSelectors from 'data/distributor/distributor.selectors';
import * as nodesSelectors from 'data/nodes/nodes.selectors';
import { ConnectedProps, connect } from 'react-redux';
import * as distributorActions from 'data/distributor/distributor.actions';
import * as nodesActions from 'data/nodes/nodes.actions';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import cloneDeep from 'lodash.clonedeep';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import newDistributorConfiguration from 'routes/Distributor/newDistributorConfiguration';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import { adaptToCreate } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as tlsProxySelectors from 'data/tlsProxy/tlsProxy.selectors';
import * as tlsProxyActions from 'data/tlsProxy/tlsProxy.actions';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import { distributorTemplate } from '../../templates';
import { adaptTemplateVisibilityMode } from '../../utils/distributorConfigTemplate.adapter';
import { getAuditDetailsToSubmit } from '../../utils/distributorConfigDataToSubmit';

type RouterParams = {
  history: string
}
const mapState = (state: RootState) => ({
  deliveryFunctionId: state.global.selectedDF,
  userId: state.authentication.userId,
  distributors: distributorSelectors.getDistributors(state),
  nodes: nodesSelectors.getNodes(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  template: adaptTemplateVisibilityMode(distributorTemplate, 'create'),
  userDBId: authenticationSelectors.getAuthenticationUserId(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  tlsProxies: tlsProxySelectors.getTlsProxyList(state),
  availableAccessFunction: supportedFeaturesSelectors.getAvailableAccessFunction(state),
  enabledAccessFunction: supportedFeaturesSelectors.getEnabledAccessFunction(state),
});

const mapDispatch = {
  getDistributors: distributorActions.getDistributors,
  getNodes: nodesActions.getNodes,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  createDistributorConfiguration: distributorConfigActions.createDistributorConfiguration,
  goToDistributorConfigurationList: distributorConfigActions.goToDistributorConfigList,
  createAndCopyDistributorConfiguration: distributorConfigActions.createAndCopyDistributorConfiguration,
  fetchTlsProxyListWithView: tlsProxyActions.fetchTlsProxyListWithView,
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>
export type Props = PropsFromRedux & PropsFromRouter;

export const DistributorConfigCreate = ({
  history,
  userId,
  template,
  distributors,
  nodes,
  deliveryFunctionId,
  getDistributors,
  getNodes,
  securityInfos,
  getSecurityInfos,
  goToDistributorConfigurationList,
  createDistributorConfiguration,
  userDBId,
  isAuditEnabled,
  createAndCopyDistributorConfiguration,
  tlsProxies,
  fetchTlsProxyListWithView,
  availableAccessFunction,
  enabledAccessFunction,
}: Props): JSX.Element => {
  const formRef = useRef<DistributorConfigFormRef>(null);
  const [formData] = useState<DistributorConfiguration>(cloneDeep(newDistributorConfiguration));
  useEffect(() => {
    getDistributors(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
    fetchTlsProxyListWithView(deliveryFunctionId);
  }, [deliveryFunctionId, getDistributors, getNodes, getSecurityInfos, fetchTlsProxyListWithView]);

  const getCreateDistributorActionPayload = () => {
    if (formRef.current === undefined || formRef.current === null) return null;

    if (!formRef.current.isValid(true, true)) {
      return null;
    }

    const distributorConfigFormData = formRef.current.getValues();
    const payload = {
      data: adaptToCreate(distributorConfigFormData),
      auditDetails: getAuditDetailsToSubmit(distributorConfigFormData, null, isAuditEnabled, userDBId, userId, 'CREATE'),
    };

    return payload;

  };

  const handleClickSave = () => {
    const payload = getCreateDistributorActionPayload();
    if (payload) {
      createDistributorConfiguration(history, deliveryFunctionId, payload);
    }
  };

  const handleClickSaveAndCopy = () => {
    const payload = getCreateDistributorActionPayload();
    if (payload) {
      createAndCopyDistributorConfiguration(history, deliveryFunctionId, payload);
    }
  };

  const handleCancel = () => goToDistributorConfigurationList(history);

  const renderForm = () => {
    if (formData && template && distributors?.ss8Distributors && securityInfos) {
      return (
        <DistributorConfigForm
          ref={formRef}
          distributorConfig={formData}
          template={template}
          distributors={distributors}
          nodes={nodes}
          securityInfos={securityInfos}
          isEditing
          tlsProxies={tlsProxies}
          availableAccessFunction={availableAccessFunction}
          enabledAccessFunction={enabledAccessFunction}
        />
      );
    }
  };
  const renderFormPanel = (formTitle: string) => {
    return (
      <FormPanel title={formTitle}>
        <FormButtons
          isEditing
          onClickCancel={handleCancel}
          onClickSave={handleClickSave}
          onClickClose={handleCancel}
          onClickSaveAndCopy={handleClickSaveAndCopy}
        />
      </FormPanel>
    );
  };

  return (
    <FormLayout>
      <FormLayoutContent>
        {renderFormPanel('New Distributor Configuration')}
        {renderForm()}
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel('')}
      </FormLayoutFooter>
    </FormLayout>
  );
};

export default withRouter(connector(DistributorConfigCreate));
