import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getAllByDisplayValue,
  getByDisplayValue,
  getByLabelText,
} from '@testing-library/react';
import Distributor from 'routes/Distributor/Distributor';
import AuthActions from 'data/authentication/authentication.types';
import { DistributorConfiguration, SELECT_DISTRIBUTOR_CONFIGURATION } from 'data/distributorConfiguration/distributorConfiguration.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDistributors } from '__test__/mockDistributors';
import { mockNodes } from '__test__/mockNodes';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { getByDataTest } from '__test__/customQueries';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import DistributorConfigCopy from './DistributorConfigCopy';
import { DISTRIBUTORS_ROUTE, SYSTEM_SETTINGS_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<DistributorConfigCopy />', () => {
  let distributorConfig: DistributorConfiguration;
  let dcXCF3: DistributorConfiguration;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['dc_access'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    distributorConfig = {
      id: 'ID_TO_GET',
      name: 'DC Name to copy',
      moduleName: 'ERKUPFX3IM',
      instanceId: '1',
      afType: 'ERK_UPF',
      nodeName: 'node-1',
      listeners: [
        {
          id: '1',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '1.1.1.1',
          port: '45160',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        },
      ],
      destinations: [
        {
          id: '1',
          ipAddress: '2.2.2.2',
          port: '45160',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
          status: 'INACTIVE',
        },
      ],
    };

    dcXCF3 = {
      id: 'ID_TO_GET',
      name: 'DC-XCF3 Name to copy',
      moduleName: 'XCF3',
      instanceId: '1',
      afType: 'STA_PGW',
      nodeName: 'node-1',
      listeners: [
        {
          id: '1',
          afProtocol: 'CISCO_PGW',
          ipAddress: '1.1.1.123',
          port: '45160',
          keepaliveTimeout: '300',
          maxConcurrentConnection: '128',
          secInfoId: '',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          numberOfParsers: '2',
          synchronizationHeader: 'TPKT',
          afWhitelist: [],
        },
      ],
      destinations: [
        {
          id: '1',
          ipAddress: '2.2.2.123',
          port: '45160',
          keepaliveInterval: '60',
          keepaliveRetries: '6',
          numOfConnections: '2',
          secInfoId: '',
          transport: 'ZPUSH',
          protocol: 'PASSTHRU_W_SX3HDR',
          requiredState: 'ACTIVE',
          status: 'INACTIVE',
        },
      ],
    };

    store.dispatch({
      type: SELECT_DISTRIBUTOR_CONFIGURATION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    axiosMock.onGet('/distributorConfigs/0/ID_TO_GET').reply(200, { data: [distributorConfig] });
    axiosMock.onGet('/distributors/0').reply(200, { data: mockDistributors });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockNodes });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });

    axiosMock.onAny().reply((config) => {
      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('Copy Distributor Configuration')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should load the data retrieved from the server', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Copy Distributor Configuration')).toBeVisible());

    expect(screen.getByLabelText('Configuration Name:*')).toHaveValue('Copy of DC Name to copy');
    expect(screen.getByLabelText('Module Name:*')).toHaveDisplayValue('ERKUPFX3IM');
    expect(screen.getByLabelText('Module Name:*')).toBeDisabled();
    expect(screen.getByLabelText('Instance ID:*')).toHaveDisplayValue('1');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson UPF');

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listenerTab1 = screen.getAllByRole('tabpanel')[0];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[0]);

    expect(getByDisplayValue(listenerTab1, 'ERKUPF_X3')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '300')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '128')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TCP')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'Active')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TPKT')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '2')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[1]);

    expect(getByDisplayValue(destinationTab1, '60')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '6')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '2')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'ZPUSH')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Passthrough with SX3 header')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Active')).toBeVisible();
  });

  it('should reset the AF Type after changing the Instance Id', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Copy Distributor Configuration')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Instance ID:*'), { target: { value: '3' } });
    expect(screen.getByLabelText('Instance ID:*')).toHaveDisplayValue('3');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Select an AF type');
  });

  it('should load the data retrieved from the server for XCF3', async () => {
    axiosMock.onGet('/distributorConfigs/0/ID_TO_GET').reply(200, { data: [dcXCF3] });
    axiosMock.onAny().reply((config) => {
      return [200, { data: [] }];
    });
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Copy Distributor Configuration')).toBeVisible());

    expect(screen.getByLabelText('Configuration Name:*')).toHaveValue('Copy of DC-XCF3 Name to copy');
    expect(screen.getByLabelText('Module Name:*')).toHaveDisplayValue('XCF3');
    expect(screen.getByLabelText('Instance ID:*')).toHaveDisplayValue('1');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Cisco/Starent PGW');
    expect(screen.getByLabelText('Xcipio Node:*')).toHaveDisplayValue('node-1 (XCF3)');

    expect(screen.getByText('Listener 1')).toBeVisible();
    const listenerTab1 = screen.getAllByRole('tabpanel')[0];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[0]);

    expect(getByLabelText(listenerTab1, 'Listener Type:*')).toHaveDisplayValue('CISCO_PGW');
    expect((listenerTab1.querySelector('input[name="ipAddress"]') as HTMLInputElement).value).toEqual('1.1.1.123');
    expect(getByDisplayValue(listenerTab1, '300')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '128')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TCP')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'Active')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, 'TPKT')).toBeVisible();
    expect(getByDisplayValue(listenerTab1, '2')).toBeVisible();

    expect(screen.getByText('Destination 1')).toBeVisible();
    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.click(screen.getAllByTestId('CollapsibleFormButton')[1]);

    expect((destinationTab1.querySelector('input[name="ipAddress"]') as HTMLInputElement).value).toEqual('2.2.2.123');
    expect(getByDisplayValue(destinationTab1, '60')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '6')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, '2')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'None')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'ZPUSH')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Passthrough with SX3 header')).toBeVisible();
    expect(getByDisplayValue(destinationTab1, 'Active')).toBeVisible();
  });

  it('should render the list of distributor configuration when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/create`]}>
          <Distributor />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New SS8 Distributor Configuration')).toBeVisible();
  });

  it('should show an error message if duplicated ip:port', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Copy Distributor Configuration')).toBeVisible());

    const listenerTab1 = screen.getAllByRole('tabpanel')[0];

    fireEvent.change(getByDataTest(listenerTab1, 'input-port'), { target: { value: '45160' } });

    const listenerTabList = screen.getAllByRole('tablist')[0];
    const addListenerButton = listenerTabList.querySelector('button') as HTMLButtonElement;
    fireEvent.click(addListenerButton);

    const listenerTab2 = screen.getAllByRole('tabpanel')[0];
    fireEvent.change(getAllByDisplayValue(listenerTab2, 'Select a listener type')[0], { target: { value: 'ERKUPF_X3' } });
    fireEvent.change(listenerTab2.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
    fireEvent.change(getByDataTest(listenerTab2, 'input-port'), { target: { value: '45160' } });

    const destinationTab1 = screen.getAllByRole('tabpanel')[1];
    fireEvent.change(destinationTab1.querySelector('input[name="port"]') as Element, { target: { value: '45160' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(screen.getAllByText('Some IP address and Port combinations are not unique. Please review Listeners and Destinations.')[0]).toBeVisible();
    expect(screen.getAllByText('IP address and Port combination not unique. Please review.')).toHaveLength(2);
  });

  it('should indicate tab has errors on Save', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <DistributorConfigCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Save')[0]);

    const listeners = screen.getByTestId('DistributorConfigListeners');
    await waitFor(() => expect(listeners.querySelector('.multi-tab-has-error')).toBeVisible());

    const destinations = screen.getByTestId('DistributorConfigDestinations');
    await waitFor(() => expect(destinations.querySelector('.multi-tab-has-error')).toBeVisible());
  });

  describe('when create a copy by clicking Save and Copy', () => {
    it('should create a new Distributor and be redirected to copy path', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${SYSTEM_SETTINGS_ROUTE}${DISTRIBUTORS_ROUTE}/copy`]}>
            <Distributor />
          </MemoryRouter>
        </Provider>,
      );

      expect(await screen.findByText('Copy Distributor Configuration')).toBeVisible();

      const listenerTab1 = screen.getAllByRole('tabpanel')[0];
      fireEvent.change(listenerTab1.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.1.1.1' } });
      fireEvent.change(getByDataTest(listenerTab1, 'input-port'), { target: { value: '4562' } });

      const destinationTab1 = screen.getAllByRole('tabpanel')[1];
      fireEvent.change(destinationTab1.querySelector('input[name="ipAddress"]') as Element, { target: { value: '1.2.1.2' } });
      fireEvent.change(destinationTab1.querySelector('input[name="port"]') as Element, { target: { value: '4160' } });

      fireEvent.click(screen.getAllByTestId('FormButtonsSaveAndCopy')[0]);

      expect(await screen.findByText('Copy Distributor Configuration')).toBeVisible();
    });
  });
});
