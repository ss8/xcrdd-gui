import React, {
  FunctionComponent,
  useState,
  useEffect,
  useRef,
} from 'react';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import DistributorConfigForm from 'routes/Distributor/components/DistributorConfigForm';
import { DistributorConfigFormRef } from 'routes/Distributor/components/DistributorConfigForm/DistributorConfigForm';
import * as distributorConfigSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { initializeForCopy, adaptToCopy } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { RootState } from 'data/root.reducers';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorSelectors from 'data/distributor/distributor.selectors';
import * as nodesSelectors from 'data/nodes/nodes.selectors';
import { ConnectedProps, connect } from 'react-redux';
import * as distributorActions from 'data/distributor/distributor.actions';
import * as nodesActions from 'data/nodes/nodes.actions';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import {
  DistributorConfiguration,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as tlsProxySelectors from 'data/tlsProxy/tlsProxy.selectors';
import * as tlsProxyActions from 'data/tlsProxy/tlsProxy.actions';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import { distributorTemplate } from '../../templates';
import { adaptTemplateVisibilityMode } from '../../utils/distributorConfigTemplate.adapter';
import { getAuditDetailsToSubmit } from '../../utils/distributorConfigDataToSubmit';

type RouterParams = {
  history: string
}
const mapState = (state: RootState) => ({
  deliveryFunctionId: state.global.selectedDF,
  selectedDistributorConfig: distributorConfigSelectors.getSelectedDistributorConfiguration(state)[0],
  distributorConfig: distributorConfigSelectors.getDistributorConfiguration(state),
  distributors: distributorSelectors.getDistributors(state),
  nodes: nodesSelectors.getNodes(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  template: adaptTemplateVisibilityMode(distributorTemplate, 'copy'),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  tlsProxies: tlsProxySelectors.getTlsProxyList(state),
  availableAccessFunction: supportedFeaturesSelectors.getAvailableAccessFunction(state),
  enabledAccessFunction: supportedFeaturesSelectors.getEnabledAccessFunction(state),
});
const mapDispatch = {
  getDistributors: distributorActions.getDistributors,
  getNodes: nodesActions.getNodes,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  createDistributorConfiguration: distributorConfigActions.createDistributorConfiguration,
  goToDistributorConfigurationList: distributorConfigActions.goToDistributorConfigList,
  getDistributorConfiguration: distributorConfigActions.getDistributorConfiguration,
  createAndCopyDistributorConfiguration: distributorConfigActions.createAndCopyDistributorConfiguration,
  fetchTlsProxyListWithView: tlsProxyActions.fetchTlsProxyListWithView,
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

export const DistributorConfigCopy : FunctionComponent<Props> = ({
  history,
  selectedDistributorConfig,
  distributorConfig,
  template,
  distributors,
  nodes,
  deliveryFunctionId,
  securityInfos,
  getDistributors,
  getNodes,
  getSecurityInfos,
  getDistributorConfiguration,
  goToDistributorConfigurationList,
  createDistributorConfiguration,
  userId,
  userName,
  isAuditEnabled,
  createAndCopyDistributorConfiguration,
  tlsProxies,
  fetchTlsProxyListWithView,
  availableAccessFunction,
  enabledAccessFunction,
}: Props) => {
  const formRef = useRef<DistributorConfigFormRef>(null);
  const [formData, setFormData] = useState<DistributorConfiguration>();
  const id = selectedDistributorConfig?.id;

  useEffect(() => {
    getDistributors(deliveryFunctionId);
    getNodes(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
    fetchTlsProxyListWithView(deliveryFunctionId);
  }, [deliveryFunctionId, getDistributors, getNodes, getSecurityInfos, fetchTlsProxyListWithView]);

  useEffect(() => {
    if (id) {
      getDistributorConfiguration(deliveryFunctionId, id);
    }
  }, [deliveryFunctionId, id, getDistributorConfiguration]);

  useEffect(() => {
    if (distributorConfig && distributorConfig.id === id) {
      const data = initializeForCopy(distributorConfig);
      setFormData(data);
    }
  }, [distributorConfig]);

  const getCopyDistributorActionPayload = () => {
    if (formRef.current === undefined || formRef.current === null) return null;

    if (!formRef.current.isValid(true, true)) {
      return null;
    }

    const data = formRef.current.getValues();
    const distributorConfigFormData = adaptToCopy(data);
    const payload = {
      data: distributorConfigFormData,
      auditDetails: getAuditDetailsToSubmit(distributorConfigFormData, null, isAuditEnabled, userId, userName, 'CREATE'),
    };

    return payload;
  };

  const handleClickSave = () => {
    const payload = getCopyDistributorActionPayload();
    if (payload) {
      createDistributorConfiguration(history, deliveryFunctionId, payload);
    }
  };

  const handleClickSaveAndCopy = () => {
    const payload = getCopyDistributorActionPayload();
    if (payload) {
      createAndCopyDistributorConfiguration(history, deliveryFunctionId, payload);
    }
  };

  const handleCancel = () => goToDistributorConfigurationList(history);

  const renderForm = () => {
    if (formData && template && distributors?.ss8Distributors && securityInfos) {
      return (
        <DistributorConfigForm
          ref={formRef}
          distributorConfig={formData}
          template={template}
          distributors={distributors}
          nodes={nodes}
          securityInfos={securityInfos}
          isEditing
          tlsProxies={tlsProxies}
          availableAccessFunction={availableAccessFunction}
          enabledAccessFunction={enabledAccessFunction}
        />
      );
    }
  };
  const renderFormPanel = (formTitle:string) => {
    return (
      <FormPanel title={formTitle}>
        <FormButtons
          isEditing
          onClickCancel={handleCancel}
          onClickSave={handleClickSave}
          onClickClose={handleCancel}
          onClickSaveAndCopy={handleClickSaveAndCopy}
        />
      </FormPanel>
    );
  };

  return (
    <FormLayout>
      <FormLayoutContent>
        {renderFormPanel('Copy Distributor Configuration')}
        {renderForm()}
      </FormLayoutContent>
      <FormLayoutFooter>
        {renderFormPanel('')}
      </FormLayoutFooter>
    </FormLayout>
  );
};

export default withRouter(connector(DistributorConfigCopy));
