import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';

const newDistributorConfiguration: DistributorConfiguration = {
  id: '',
  name: '',
  moduleName: '',
  instanceId: '',
  afType: '',
  nodeName: '',
  operation: 'CREATE',
  listeners: [
    {
      id: '',
      afProtocol: '',
      ipAddress: '',
      port: '',
      keepaliveTimeout: '300',
      maxConcurrentConnection: '128',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      requiredState: 'ACTIVE',
      afWhitelist: [],
      synchronizationHeader: 'NONE',
      numberOfParsers: '2',
      operation: 'CREATE',
    },
  ],
  destinations: [
    {
      id: '',
      ipAddress: '',
      port: '',
      keepaliveInterval: '60',
      keepaliveRetries: '6',
      numOfConnections: '2',
      requiredState: 'ACTIVE',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      protocol: 'PASSTHRU',
      operation: 'CREATE',
    },
  ],
};
export default newDistributorConfiguration;
