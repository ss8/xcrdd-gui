import { FormTemplate, Template } from 'data/template/template.types';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockTeams } from '__test__/mockTeams';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { createWarrantBasedOnTemplate } from './warrantTemplate.factory';
import andromedaWarrantTemplate from '../../../../deployment/templates/warrant-create-template-andromeda.json';
import andromedaWizardWarrantTemplate from '../../../../deployment/templates/warrantWizard-create-template-andromeda.json';
import endeavorWarrantTemplate from '../../../../deployment/templates/warrant-create-template-endeavor.json';
import endeavorWizardWarrantTemplate from '../../../../deployment/templates/warrantWizard-create-template-endeavor.json';
import { adaptTemplate } from './warrantTemplate.adapter';

describe('warrantTemplate.factory', () => {
  describe('createWarrantBasedOnTemplate', () => {
    let template: Template;

    beforeEach(() => {
      template = {
        category: 'warrants',
        categoryDefault: false,
        dateUpdated: '2020-08-21T07:19:42.000+0000',
        id: '0',
        name: 'Default',
        owner: null,
        publicTemplate: true,
        template: {} as unknown as FormTemplate,
      };
    });

    it('should return the warrant based in andromeda template', () => {
      template.template = andromedaWarrantTemplate as unknown as FormTemplate;

      const adaptedFormTemplate = adaptTemplate(
        template,
        'create',
        null,
        mockTemplates,
        false,
        mockTimeZones,
        mockContacts,
        mockTeams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      expect(createWarrantBasedOnTemplate(adaptedFormTemplate.template)).toEqual({
        id: '',
        name: '',
        city: '',
        comments: '',
        dxOwner: '',
        judge: '',
        region: '',
        receivedDateTime: '',
        startDateTime: '',
        stopDateTime: '',
        timeZone: '',
        templateUsed: '',
        dxAccess: '',
        cases: [],
        attachments: [],
        province: '',
        department: '',
      });
    });

    it('should return the warrant based in andromedaWizard template', () => {
      template.template = andromedaWizardWarrantTemplate as unknown as FormTemplate;

      const adaptedFormTemplate = adaptTemplate(
        template,
        'create',
        null,
        mockTemplates,
        false,
        mockTimeZones,
        mockContacts,
        mockTeams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      expect(createWarrantBasedOnTemplate(adaptedFormTemplate.template)).toEqual({
        id: '',
        name: '',
        city: '',
        comments: '',
        dxOwner: '',
        judge: '',
        region: '',
        receivedDateTime: '',
        startDateTime: '',
        stopDateTime: '',
        timeZone: '',
        templateUsed: '',
        dxAccess: '',
        cases: [],
        attachments: [],
        province: '',
        department: '',
      });
    });

    it('should return the warrant based in endeavor template', () => {
      template.template = endeavorWarrantTemplate as unknown as FormTemplate;

      const adaptedFormTemplate = adaptTemplate(
        template,
        'create',
        null,
        mockTemplates,
        false,
        [{ value: 'Europe/London', label: 'London' }],
        mockContacts,
        mockTeams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      expect(createWarrantBasedOnTemplate(adaptedFormTemplate.template)).toEqual({
        id: '',
        name: '',
        city: '',
        comments: '',
        dxOwner: '',
        judge: '',
        region: '',
        receivedDateTime: '',
        startDateTime: '',
        stopDateTime: '',
        timeZone: 'Europe/London',
        templateUsed: '',
        dxAccess: '',
        cases: [],
        attachments: [],
        province: '',
        department: '',
      });
    });

    it('should return the warrant based in endeavorWizard template', () => {
      template.template = endeavorWizardWarrantTemplate as unknown as FormTemplate;

      const adaptedFormTemplate = adaptTemplate(
        template,
        'create',
        null,
        mockTemplates,
        false,
        [{ value: 'Europe/London', label: 'London' }],
        mockContacts,
        mockTeams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      expect(createWarrantBasedOnTemplate(adaptedFormTemplate.template)).toEqual({
        id: '',
        name: '',
        city: '',
        comments: '',
        dxOwner: '',
        judge: '',
        region: '',
        receivedDateTime: '',
        startDateTime: '',
        stopDateTime: '',
        timeZone: 'Europe/London',
        templateUsed: '',
        dxAccess: '',
        cases: [],
        attachments: [],
        province: '',
        department: '',
      });
    });
  });
});
