import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { Warrant } from 'data/warrant/warrant.types';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';
import { warrantModel } from 'xcipio/warrants/warrant-model';

// eslint-disable-next-line import/prefer-default-export
export function createWarrantBasedOnTemplate(
  template: FormTemplate,
): Warrant {
  let partialWarrant: Partial<Warrant> = {
    ...warrantModel,
    ...getFieldValuesFromSection(template.general as FormTemplateSection),
  };
  partialWarrant = omitBy(partialWarrant, (_value, key) => ['caseOptionDetails', 'attachmentsDetails'].includes(key));

  const formData = omitBy(partialWarrant, isNil) as unknown as Warrant;
  return formData;
}

function getFieldValuesFromSection(
  templateSection: FormTemplateSection,
): Partial<Warrant> {
  return Object.entries(templateSection?.fields)
    .filter(([_fieldName, field]) => field.initial != null)
    .reduce((previousValue, [fieldName, field]) => ({
      ...previousValue,
      [fieldName]: field.initial,
    }), {});
}
