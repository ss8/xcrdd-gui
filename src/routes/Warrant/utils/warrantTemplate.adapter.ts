import {
  FormTemplate,
  FormTemplateSection,
  Template,
  RawTemplate,
} from 'data/template/template.types';
import { Contact, ValueLabelPair } from 'data/types';
import { Warrant, WARRANT_LEA_ANY } from 'data/warrant/warrant.types';
import cloneDeep from 'lodash.clonedeep';
import { hideAttachmentFieldFromTemplate } from 'xcipio/warrants/forms';
import CaseCreateOptions from 'xcipio/cases/options/case-options.json';
import { checkTimeValidationInComputerTimezone } from 'shared/constants/checkTimeValidation';
import { Team } from 'data/team/team.types';
import { CollectionFunctionGroup } from 'data/collectionFunction/collectionFunction.types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { getSortedTimeZoneValueLabels } from 'shared/timeZoneUtils/timeZone.utils';

const defaultOption = {
  label: 'Select',
  value: '',
};

// eslint-disable-next-line import/prefer-default-export
export function adaptTemplate(
  formTemplate: Template,
  mode: string,
  warrant: Warrant | null,
  templates: RawTemplate[],
  attachmentEnabled: boolean,
  timezones: ValueLabelPair[],
  contacts: Contact[],
  teams: Team[],
  cfGroups: CollectionFunctionGroup[],
  handleManageAttachmentClick: () => void,
  showCaseOptions: () => void,
): Template {
  const clonedFormTemplate = cloneDeep(formTemplate);

  setTimezoneOptions(clonedFormTemplate.template, warrant, timezones);
  setContactOptions(clonedFormTemplate.template, contacts);
  setVisibilityOptions(clonedFormTemplate.template, teams);
  setWarrantLEAOptions(clonedFormTemplate.template, cfGroups);
  setCaseOptions(clonedFormTemplate.template, templates);

  hideAttachmentFieldFromTemplate(clonedFormTemplate.template, attachmentEnabled ?? false);

  setFormTemplateFieldReadOnly(clonedFormTemplate, warrant, mode);

  setManageAttachmentHandler(clonedFormTemplate.template, warrant, handleManageAttachmentClick);
  setCaseOptionDetailsHandler(clonedFormTemplate.template, showCaseOptions);

  return clonedFormTemplate;
}

export function setTimezoneOptions(
  formTemplate: FormTemplate,
  warrant: Warrant | null,
  timezones: ValueLabelPair[],
): void {
  if (timezones.length > 0) {
    const formTemplateField = (formTemplate.general as FormTemplateSection).fields.timeZone;

    formTemplateField.type = 'select';

    const date = new Date();
    const newTimeZones = [...timezones];

    // if the warrant.timezone is not in the list of timezones from the DB, add to the time zone list.
    const matchWarrantTimeZone = timezones.some((timezone) => (timezone.value === warrant?.timeZone));
    if (warrant && warrant.timeZone && !matchWarrantTimeZone) {
      newTimeZones.push({
        label: warrant.timeZone,
        value: warrant.timeZone,
      });
    }

    // Add offsets to label and sort the labels
    const timezoneOptions = getSortedTimeZoneValueLabels(date, newTimeZones);

    if (timezoneOptions.length === 1) {
      formTemplateField.initial = timezoneOptions[0].value;
    }

    if (formTemplateField.options == null) {
      formTemplateField.options = [];
    }

    formTemplateField.options?.push(...timezoneOptions);
  }
}

function setContactOptions(formTemplate: FormTemplate, contacts: Contact[]) {
  const contactOptions = contacts.map((contact) => {
    return {
      label: contact.name,
      value: contact.id,
    };
  });
  contactOptions.unshift(defaultOption);
  (formTemplate.general as FormTemplateSection).fields.contact.options = contactOptions;
}

function setVisibilityOptions(formTemplate: FormTemplate, teams: Team[]) {
  const visibilityOptions = teams.map((team) => {
    return {
      label: team.name,
      value: team.id,
    };
  });
  (formTemplate.general as FormTemplateSection).fields.dxOwner.options = visibilityOptions;
}

function setManageAttachmentHandler(
  formTemplate: FormTemplate,
  warrant: Warrant | null,
  handleManageAttachmentClick: () => void,
) {
  if ((formTemplate.general as FormTemplateSection).fields.attachmentsDetails) {
    let attachmentsLength = 0;
    if (warrant && warrant.attachments) {
      attachmentsLength = warrant.attachments.length;
    }
    (formTemplate.general as FormTemplateSection).fields.attachmentsDetails.handleClick = handleManageAttachmentClick;
    (formTemplate.general as FormTemplateSection).fields.attachmentsDetails.initial = `${attachmentsLength} Attachments`;
  }
}

function setWarrantLEAOptions(formTemplate: FormTemplate, cfGroups: CollectionFunctionGroup[]) {
  const warrantFormTemplate = formTemplate;
  const leaOptions = cfGroups.map((item) => {
    return {
      label: item.name,
      value: item.id,
    };
  });
  leaOptions.unshift(defaultOption);
  leaOptions.push({ label: WARRANT_LEA_ANY, value: WARRANT_LEA_ANY });
  if ((warrantFormTemplate.general as FormTemplateSection).fields.lea) {
    (warrantFormTemplate.general as FormTemplateSection).fields.lea.options = leaOptions;
  }
}

function setCaseOptions(formTemplate: FormTemplate, templates: RawTemplate[]) {
  if ((formTemplate.general as FormTemplateSection).fields.caseOptTemplateUsed) {
    (formTemplate.general as FormTemplateSection).fields.caseOptTemplateUsed.options
    = getAllCaseOptionTemplates(templates);
  }
}

function setCaseOptionDetailsHandler(formTemplate: FormTemplate, showCaseOptions: () => void): void {
  if ((formTemplate.general as FormTemplateSection).fields.caseOptionDetails) {
    (formTemplate.general as FormTemplateSection).fields.caseOptionDetails.handleClick = showCaseOptions;
  }
}

function getAllCaseOptionTemplates(templates: RawTemplate[]) {
  if (templates.length === 0) {
    templates = CaseCreateOptions as RawTemplate[];
  }
  const result = templates.filter((template) => template.category === 'CaseOptions').map((template) =>
    ({ label: template.name, value: template.id }));
  result.unshift(defaultOption);
  return result;
}

function setFormTemplateFieldReadOnly(formTemplate: Template, warrant: Warrant | null, mode: string) {
  if (warrant != null && checkTimeValidationInComputerTimezone(warrant.startDateTime, warrant.stopDateTime)) {
    (formTemplate.template.general as FormTemplateSection).fields.startDateTime.disabled = true;
    (formTemplate.template.general as FormTemplateSection).fields.timeZone.disabled = true;
  }

  if (mode === CONFIG_FORM_MODE.Edit) {
    (formTemplate.template.general as FormTemplateSection).fields.lea.disabled = true;
  }
}
