import cloneDeep from 'lodash.clonedeep';
import { Team } from 'data/team/team.types';
import { FormTemplate, FormTemplateSection, Template } from 'data/template/template.types';
import { Contact, ValueLabelPair } from 'data/types';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { mockCollectionFunctionGroups } from '__test__/mockCollectionFunctionGroups';
import { mockTemplates } from '__test__/mockTemplates';
import { mockWarrants } from '__test__/mockWarrants';
import { adaptTemplate, setTimezoneOptions } from './warrantTemplate.adapter';
import andromedaWizardWarrantTemplate from '../../../../deployment/templates/warrantWizard-create-template-andromeda.json';
import endeavorWizardWarrantTemplate from '../../../../deployment/templates/warrantWizard-create-template-endeavor.json';

describe('warrantTemplate.adapter', () => {
  let formTemplate: Template;
  let timezones: ValueLabelPair[];
  let contacts: Contact[];
  let teams: Team[];

  beforeEach(() => {
    formTemplate = {
      ...mockTemplates[1],
      template: JSON.parse(mockTemplates[1].template),
    };

    timezones = [
      { value: 'America/Los_Angeles', label: 'US/Pacific' },
      { value: 'America/New_York', label: 'US/Eastern' },
    ];

    contacts = [{
      id: 'id1',
      name: 'Name 1',
    }, {
      id: 'id2',
      name: 'Name 2',
    }];

    teams = [{
      id: 'id1',
      name: 'team1',
      description: 'desc1',
      members: [],
    }, {
      id: 'id2',
      name: 'team2',
      description: 'desc2',
      members: [],
    }];
  });

  describe('adaptTemplate()', () => {
    it('should set the timezones options', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      const expected = expect.arrayContaining([{
        label: 'Select',
        value: '',
      }, {
        label: expect.stringMatching(/(US\/Pacific \(GMT-08\)|US\/Pacific \(GMT-07\))/),
        value: 'America/Los_Angeles',
      }, {
        label: expect.stringMatching(/(US\/Eastern \(GMT-05\)|US\/Eastern \(GMT-04\))/),
        value: 'America/New_York',
      }]);

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.timeZone.options).toEqual(expected);
    });

    it('should set the contacts options', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      const expected = [{
        label: 'Select',
        value: '',
      }, {
        label: 'Name 1',
        value: 'id1',
      }, {
        label: 'Name 2',
        value: 'id2',
      }];

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.contact.options).toEqual(expected);
    });

    it('should set the visibility options', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      const expected = [{
        label: 'team1',
        value: 'id1',
      }, {
        label: 'team2',
        value: 'id2',
      }];

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.dxOwner.options).toEqual(expected);
    });

    it('should set the LEA options', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      const expected = [
        { label: 'Select', value: '' },
        { label: 'CA-DOJBNE', value: 'id1' },
        { label: 'CookCounty', value: 'id2' },
        { label: 'CA-DOJBNE-TIA1072', value: 'id3' },
        { label: 'CA-DOJBNE-5GCF', value: 'id4' },
        { label: 'Flavia', value: 'id5' },
        { label: 'Any', value: 'Any' },
      ];

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.lea.options).toEqual(expected);
    });

    it('should set the case options', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      const expected = [{
        label: 'Select',
        value: '',
      }, {
        label: 'Default',
        value: '3',
      }, {
        label: 'With Location',
        value: '4',
      }, {
        label: 'Without Location',
        value: '5',
      }];

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.caseOptTemplateUsed.options)
        .toEqual(expected);
    });

    it('should disable the fields for edit mode', () => {
      const adaptedTemplate = adaptTemplate(
        formTemplate,
        CONFIG_FORM_MODE.Edit,
        mockWarrants[0],
        mockTemplates,
        false,
        timezones,
        contacts,
        teams,
        mockCollectionFunctionGroups,
        jest.fn(),
        jest.fn(),
      );

      expect((adaptedTemplate.template.general as FormTemplateSection).fields.lea.disabled).toEqual(true);
    });
  });

  describe('setTimezoneOptions()', () => {
    it('should do nothing if list of template is empty', () => {
      const warrantFormTemplate = cloneDeep(andromedaWizardWarrantTemplate) as unknown as FormTemplate;
      setTimezoneOptions(warrantFormTemplate, null, []);
      expect(warrantFormTemplate).toEqual(andromedaWizardWarrantTemplate);
    });

    it('should set the timeZone field type and options', () => {
      const warrantFormTemplate = cloneDeep(andromedaWizardWarrantTemplate) as unknown as FormTemplate;
      setTimezoneOptions(warrantFormTemplate, null, timezones);
      expect(warrantFormTemplate).toEqual({
        ...andromedaWizardWarrantTemplate,
        general: {
          ...andromedaWizardWarrantTemplate.general,
          fields: {
            ...andromedaWizardWarrantTemplate.general.fields,
            timeZone: {
              ...andromedaWizardWarrantTemplate.general.fields.timeZone,
              type: 'select',
              options: [{
                label: 'Select',
                value: '',
              }, {
                label: expect.stringContaining('US/Eastern'),
                value: 'America/New_York',
              }, {
                label: expect.stringContaining('US/Pacific'),
                value: 'America/Los_Angeles',
              }],
            },
          },
        },
      });
    });

    it('should set the timeZone initial state if only one timezone option in template', () => {
      const warrantFormTemplate = cloneDeep(endeavorWizardWarrantTemplate) as unknown as FormTemplate;
      setTimezoneOptions(warrantFormTemplate, null, [{ value: 'Europe/London', label: 'London' }]);
      expect(warrantFormTemplate).toEqual({
        ...endeavorWizardWarrantTemplate,
        general: {
          ...endeavorWizardWarrantTemplate.general,
          fields: {
            ...endeavorWizardWarrantTemplate.general.fields,
            timeZone: {
              ...endeavorWizardWarrantTemplate.general.fields.timeZone,
              type: 'select',
              initial: 'Europe/London',
              options: [{
                label: 'Select',
                value: '',
              }, {
                label: expect.stringContaining('London'),
                value: 'Europe/London',
              }],
            },
          },
        },
      });
    });

    it('should set the timeZone options with the unknown timezone when it is configured in the warrant', () => {
      const warrantFormTemplate = cloneDeep(andromedaWizardWarrantTemplate) as unknown as FormTemplate;
      const warrant = cloneDeep(mockWarrants[0]);
      warrant.timeZone = 'Europe/London';
      setTimezoneOptions(warrantFormTemplate, warrant, timezones);
      expect(warrantFormTemplate).toEqual({
        ...andromedaWizardWarrantTemplate,
        general: {
          ...andromedaWizardWarrantTemplate.general,
          fields: {
            ...andromedaWizardWarrantTemplate.general.fields,
            timeZone: {
              ...andromedaWizardWarrantTemplate.general.fields.timeZone,
              type: 'select',
              options: [{
                label: 'Select',
                value: '',
              }, {
                label: expect.stringContaining('London'),
                value: 'Europe/London',
              }, {
                label: expect.stringContaining('US/Eastern'),
                value: 'America/New_York',
              }, {
                label: expect.stringContaining('US/Pacific'),
                value: 'America/Los_Angeles',
              }],
            },
          },
        },
      });
    });
  });
});
