import React, { Component, Fragment } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  Button, Classes, Dialog, ProgressBar, Tab, Tabs,
} from '@blueprintjs/core';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { FormLayout, FormLayoutContent } from 'components/FormLayout';
import { FormLayoutFooter } from 'components/FormLayout/FormLayout';
import FormPanel from 'components/FormPanel';
import FormButtons from 'components/FormButtons';
import * as globalSelectors from 'global/global-selectors';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as caseActions from 'xcipio/cases/case-actions';
import * as caseSelectors from 'data/case/case.selectors';
import * as targetActions from 'xcipio/targets/targets-actions';
import * as targetSelectors from 'data/target/target.selectors';
import * as collectionFunctionActions from 'data/collectionFunction/collectionFunction.actions';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import WarrantAndDevicePanel from 'xcipio/warrants/WarrantAndDevicePanel';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { trimJsonObj } from 'shared/dataUtils/json-utils';
import { CaseModel } from 'data/case/case.types';
import { TEMPORARY_TARGET_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import './warrant-edit.scss';
import { RetrievingDataProgress } from 'components';
import {
  AppToaster, showError, showErrors, showSuccessMessage,
} from 'shared/toaster';
import WarrantCasesPanel from 'routes/Warrant/components/WarrantCasesPanel';
import ModalDialog from 'shared/modal/dialog';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import { EditWarrantSubmit } from 'xcipio/warrants/forms/warrantSubmit/warrant-edit-submit';
import { Warrant } from 'data/warrant/warrant.types';
import ChangeTracker from 'xcipio/warrants/forms/warrant-change-tracker';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  selectedWarrant: warrantSelectors.getSelectedWarrant(state),
  warrant: warrantSelectors.getWarrant(state),
  cases: caseSelectors.getCases(state),
  targetsById: targetSelectors.getTargetsById(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  changeTracker: warrantSelectors.getChangeTrackerInstance(state),
  warrantUserAudit: warrantSelectors.getWarrantUserAudit(state),
  selectedCase: warrantSelectors.getSelectedWarrantCase(state),
});

const mapDispatch = {
  goToWarrantList: warrantActions.goToWarrantList,
  getWarrantById: warrantActions.getWarrantById,
  getAllCasesByCaseIds: caseActions.getAllCasesByCaseIds,
  getAllTargetsByTargetIds: targetActions.getAllTargetsByTargetIds,
  setWarrantTracker: warrantActions.setWarrantTracker,
  updateWarrantState: warrantActions.updateWarrantState,
  updateCasesState: caseActions.updateCasesState,
  clearWarrantState: warrantActions.clearWarrantState,
  clearCasesState: caseActions.clearCasesState,
  clearTargetState: targetActions.clearTargetState,
  clearFormTemplateUsed: warrantActions.clearFormTemplateUsed,
  clearWarrantTracker: warrantActions.clearWarrantTracker,
  clearCollectionFunctionState: collectionFunctionActions.clearCollectionFunctionState,
  goToCaseList: caseActions.goToCaseList,
  clearSelectedWarrantCase: warrantActions.clearSelectedWarrantCase,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type WarrantEditProps = PropsFromRedux & PropsFromRouter;

export type WarrantEditState = {
  selectedTabId: string
  isRetrievingWarrant: boolean
  isRetrievingCases: boolean
  isNewDeviceDetectedPopupOpen: boolean
  isMissingInformationPopupOpen: boolean
  missingInformationCaseIndex: number | null
  isConfirmCancelPopupOpen: boolean
  isSubmitInProgress: boolean
  progressTotalSteps: number
  progressStep: number
  progressMessage: string
  warrantToSubmit: Warrant | null
}

const WARRANT_TAB_ID = 'warrantAndDevices';
const CASES_TAB_ID = 'cases';

export class WarrantEdit extends Component<WarrantEditProps, WarrantEditState> {
  private warrantRef: React.RefObject<WarrantAndDevicePanel>;
  private casesRef: React.RefObject<WarrantCasesPanel>;
  private submitRef: React.RefObject<EditWarrantSubmit>;
  private originalWarrant: Warrant | null = null;
  private originalCases: CaseModel[] | null = null;
  private openingFromCaseList = false;

  constructor(props: WarrantEditProps) {
    super(props);

    this.warrantRef = React.createRef<WarrantAndDevicePanel>();
    this.casesRef = React.createRef<WarrantCasesPanel>();
    this.submitRef = React.createRef<EditWarrantSubmit>();

    const {
      clearCollectionFunctionState,
      clearCasesState,
      selectedCase,
    } = this.props;

    this.openingFromCaseList = !!selectedCase;

    clearCollectionFunctionState();
    clearCasesState();

    this.state = {
      selectedTabId: WARRANT_TAB_ID,
      isRetrievingWarrant: true,
      isRetrievingCases: false,
      isNewDeviceDetectedPopupOpen: false,
      isMissingInformationPopupOpen: false,
      missingInformationCaseIndex: null,
      isConfirmCancelPopupOpen: false,
      isSubmitInProgress: false,
      progressTotalSteps: 0,
      progressStep: 0,
      progressMessage: '',
      warrantToSubmit: null,
    };
  }

  componentDidMount(): void {
    this.fetchWarrant();
    this.fetchCases();
    this.setChangeTracker();
  }

  componentDidUpdate(prevProps: WarrantEditProps): void {
    const {
      warrant,
      cases,
      updateWarrantState,
      updateCasesState,
    } = this.props;

    if (warrant != null && prevProps.warrant == null) {
      this.originalWarrant = cloneDeep(warrant);

      // Convert date/time fields to computer timezone.
      // Note that they will be converted back to warrant timezone before submit.
      const adaptedWarrant = WarrantAdapter.adaptWarrantToLoad(warrant);
      updateWarrantState(adaptedWarrant);
    }

    if (cases.length > 0 && prevProps.cases.length === 0) {
      this.originalCases = cloneDeep(cases);

      // Convert date/time fields to computer timezone.
      // Note that they will be converted back to warrant timezone before submit.
      const adaptedCases = WarrantAdapter.adaptCasesToLoad(cases);
      updateCasesState(adaptedCases);

      this.fetchTargets();
    }
  }

  componentWillUnmount(): void {
    const {
      clearWarrantState,
      clearCasesState,
      clearTargetState,
      clearFormTemplateUsed,
      clearWarrantTracker,
      clearCollectionFunctionState,
      clearSelectedWarrantCase,
    } = this.props;

    clearWarrantState();
    clearCasesState();
    clearTargetState();
    clearFormTemplateUsed();
    clearWarrantTracker();
    clearCollectionFunctionState();
    clearSelectedWarrantCase();
  }

  fetchWarrant(): void {
    const {
      deliveryFunctionId,
      selectedWarrant,
      getWarrantById,
    } = this.props;

    if (selectedWarrant == null) {
      return;
    }

    getWarrantById(deliveryFunctionId, selectedWarrant.id);
  }

  fetchCases(): void {
    const {
      deliveryFunctionId,
      selectedWarrant,
      getAllCasesByCaseIds,
    } = this.props;

    if (selectedWarrant == null || selectedWarrant.cases.length === 0) {
      return;
    }

    const caseIds = selectedWarrant.cases.map(({ id }) => id).join(',');
    getAllCasesByCaseIds(deliveryFunctionId, selectedWarrant.id, caseIds);
  }

  fetchTargets(): void {
    const {
      deliveryFunctionId,
      cases,
      getAllTargetsByTargetIds,
    } = this.props;

    cases.forEach(({ targets, id: caseId, warrantId: { id: warrantId } }) => {
      if (targets.length === 0) {
        return;
      }

      const targetIds = targets.map(({ id }) => id).join(',');
      getAllTargetsByTargetIds(deliveryFunctionId, warrantId as string, caseId, targetIds);
    });
  }

  setChangeTracker(): void {
    const {
      setWarrantTracker,
      deliveryFunctionId,
      selectedWarrant,
    } = this.props;

    if (selectedWarrant == null) {
      return;
    }

    const changeTracker = new ChangeTracker({
      idDF: deliveryFunctionId,
      idWarrant: selectedWarrant.id,
    });

    setWarrantTracker(changeTracker);
  }

  handleDeleteCase = (caseId: string): void => {
    const {
      cases,
      updateCasesState,
      changeTracker,
    } = this.props;

    let clonedCases = cloneDeep(cases);
    const toBeDeletedCase = clonedCases.find((caseItem) => caseItem.id !== caseId);
    clonedCases = clonedCases.filter((caseItem) => caseItem.id !== caseId);

    updateCasesState(clonedCases);

    if (changeTracker != null) {
      changeTracker.addDeleted(`case.${caseId}`, toBeDeletedCase?.liId as string);
      changeTracker.deleteDependentActions(`case.${caseId}`);
    }
  }

  isReady(): boolean {
    const {
      warrant,
      cases,
    } = this.props;

    const {
      isRetrievingWarrant,
    } = this.state;

    if (warrant === null) {
      return false;
    }

    if (warrant.cases.length === 0) {
      return true;
    }

    if (cases.length === 0 && isRetrievingWarrant) {
      return false;
    }

    const isEveryTargetLoaded = this.isEveryTargetLoaded(cases);

    if (!isEveryTargetLoaded) {
      return false;
    }

    return true;
  }

  isEveryTargetLoaded(cases: CaseModel[]): boolean {
    const {
      targetsById,
    } = this.props;

    return cases.every((caseEntry) => {
      return caseEntry.targets.every(({ id, primaryType }) => {
        const isNewTarget = primaryType ? id?.startsWith(primaryType) : false;

        return id == null || id.startsWith(TEMPORARY_TARGET_ID_PREFIX) || targetsById[id] != null || isNewTarget;
      });
    });
  }

  handleTabChange = async (newTabId: string): Promise<void> => {
    const { selectedTabId } = this.state;

    if (newTabId === selectedTabId) {
      return;
    }

    if (newTabId === WARRANT_TAB_ID) {
      await this.handleTabChangeToWarrant();

    } else {
      await this.handleTabChangeToCases();
    }
  }

  async handleTabChangeToWarrant(): Promise<void> {
    const { clearSelectedWarrantCase } = this.props;
    clearSelectedWarrantCase();

    AppToaster.clear();
    const errors: string[] = [];

    if (!this.casesRef.current?.isValid(errors)) {
      showErrors(errors);
      return;
    }

    await this.casesRef.current?.submit();

    this.setState({
      missingInformationCaseIndex: null,
      selectedTabId: WARRANT_TAB_ID,
    });
  }

  async handleTabChangeToCases(): Promise<void> {
    AppToaster.clear();

    const errors: string[] = [];

    if (!this.warrantRef.current?.isValid(errors)) {
      showErrors(errors);
      return;
    }

    this.setState({
      isRetrievingCases: true,
    });

    await this.warrantRef.current?.submit(true);
    const { selectedCase, cases } = this.props;
    let selectedCaseIndex = 0;
    if (selectedCase) {
      cases.find((warrantCase, index) => {
        if (warrantCase.id === selectedCase.id) {
          selectedCaseIndex = index;
          return true;
        }
        return false;
      });

      this.setState({
        missingInformationCaseIndex: selectedCaseIndex,
      });
    }

    this.setState({
      selectedTabId: CASES_TAB_ID,
      isRetrievingCases: false,
    });
  }

  handleClickCancel = (): void => {
    this.setState({
      isConfirmCancelPopupOpen: true,
    });
  }

  handleClickSave = async (): Promise<void> => {
    const {
      selectedTabId,
    } = this.state;

    AppToaster.clear();

    let isValid = false;
    if (selectedTabId === WARRANT_TAB_ID) {
      isValid = await this.handleClickSaveFromWarrantAndDevices();
    } else {
      isValid = await this.handleClickSaveFromCases();
    }

    // Warrant and cases should be retrieve from props after the above
    // function, so they are up to date to be submitted.
    const {
      cases,
      warrant,
    } = this.props;

    if (!isValid || warrant == null) {
      return;
    }

    let warrantToSubmit = cloneDeep(warrant);
    warrantToSubmit.cases = cloneDeep(cases);
    warrantToSubmit = trimJsonObj(warrantToSubmit);
    warrantToSubmit = WarrantEdit.cleanUpWarrant(warrantToSubmit);
    // Must assign attachments to formData here because the above trimJsonObj() changes
    // the file object in the attachment and breaks the upload file.
    // Since edit submit does not change the attachments object, we do not need to do a deep copy.
    if (warrant.attachments) {
      const attachments = { ...warrant.attachments };
      warrantToSubmit.attachments = attachments;
    }

    this.setState({
      progressTotalSteps: 0,
      progressStep: 0,
      warrantToSubmit,
    });

    this.submitRef.current?.resetSubmit();
    await this.submitRef.current?.updateWarrant();
  }

  static cleanUpWarrant(warrant: Warrant): Warrant {
    const clonedWarrant = cloneDeep(warrant);

    warrantActions.cleanUpWarrantFormData(clonedWarrant);

    return clonedWarrant;
  }

  goToWarrantOrCaseList(): void {
    const {
      history,
      goToWarrantList,
      goToCaseList,
    } = this.props;

    if (this.openingFromCaseList) {
      goToCaseList(history);
    } else {
      goToWarrantList(history);
    }
  }

  handleSubmitNoChangeToUpdate = (): void => {
    this.goToWarrantOrCaseList();
  }

  handleSubmitProgressStart = (totalSteps: number): void => {
    if (totalSteps === 0) {
      return;
    }

    this.setState({
      isSubmitInProgress: true,
      progressTotalSteps: totalSteps + 1,
      progressStep: 1,
    });
  }

  handleSubmitProgressUpdate = (progressList: { text: string }[], failed = false): void => {
    if (failed) {
      return;
    }

    const item = progressList[progressList.length - 1];

    this.setState({
      isSubmitInProgress: true,
      progressStep: progressList.length,
      progressMessage: item.text,
    });
  }

  handleSubmitProgressEnd = (messageList: string[], failed = false): void => {
    const {
      isSubmitInProgress,
    } = this.state;

    if (!isSubmitInProgress) {
      return;
    }

    this.setState({
      isSubmitInProgress: false,
    });

    if (failed) {
      showError((
        <Fragment>
          {messageList.map((message) => <p>{message}</p>)}
        </Fragment>
      ));

    } else {
      showSuccessMessage(messageList[0], 5000);
      this.goToWarrantOrCaseList();
    }
  }

  handleConfirmCancelSubmit = (): void => {
    AppToaster.clear();
    this.goToWarrantOrCaseList();
  }

  handleConfirmCancelClose = (): void => {
    this.setState({
      isConfirmCancelPopupOpen: false,
    });
  }

  async handleClickSaveFromWarrantAndDevices(): Promise<boolean> {
    let errors: string[] = [];

    if (!this.warrantRef.current?.isValid(errors)) {
      showErrors(errors);
      return false;
    }

    if (this.warrantRef.current?.hasAnyDeviceChange()) {
      this.setState({
        isNewDeviceDetectedPopupOpen: true,
      });
    }

    await this.warrantRef.current?.submit();

    errors = [];
    const [isValid, caseIndex] = this.warrantRef.current?.isWarrantAndCasesValid(errors);
    if (!isValid) {
      this.setState({
        isNewDeviceDetectedPopupOpen: false,
        isMissingInformationPopupOpen: true,
        missingInformationCaseIndex: caseIndex,
      });

      return false;
    }

    this.setState({
      isNewDeviceDetectedPopupOpen: false,
    });

    return true;
  }

  async handleClickSaveFromCases(): Promise<boolean> {
    const errors: string[] = [];

    if (!this.casesRef.current?.isValid(errors)) {
      showErrors(errors);
      return false;
    }

    const isValid = await this.casesRef.current?.submit() ?? false;

    return isValid;
  }

  handleMissingInformationPopupClose = (): void => {
    this.setState({
      isMissingInformationPopupOpen: false,
      missingInformationCaseIndex: null,
    });
  }

  handleGoToCasesClick = (): void => {
    this.setState({
      isMissingInformationPopupOpen: false,
    });

    this.handleTabChange(CASES_TAB_ID);
  }

  handleDeviceReady = (): void => {
    const { selectedCase } = this.props;
    this.setState({
      isRetrievingWarrant: false,
    });
    if (selectedCase) {
      this.handleTabChangeToCases();
    }
  }

  renderFormPanel(formTitle: string | null = null): JSX.Element {
    return (
      <FormPanel title={formTitle}>
        <FormButtons
          isEditing
          onClickClose={this.handleClickCancel}
          onClickCancel={this.handleClickCancel}
          onClickSave={this.handleClickSave}
        />
      </FormPanel>
    );
  }

  renderWarrantAndDevices(): JSX.Element | undefined {
    const {
      warrant,
    } = this.props;

    if (warrant == null) {
      return;
    }

    return (
      <WarrantAndDevicePanel
        ref={this.warrantRef}
        mode={CONFIG_FORM_MODE.Edit}
        onDevicesReady={this.handleDeviceReady}
        warrantId={warrant.id}
      />
    );
  }

  renderCases(): JSX.Element {
    const {
      cases,
    } = this.props;

    const {
      missingInformationCaseIndex,
    } = this.state;

    return (
      <WarrantCasesPanel
        ref={this.casesRef}
        cases={cases}
        onDeleteCase={this.handleDeleteCase}
        mode={CONFIG_FORM_MODE.Edit}
        selectedCaseIndex={missingInformationCaseIndex}
      />
    );
  }

  renderMissingInformationPopupMessage(): JSX.Element | null {
    const {
      cases,
    } = this.props;

    const {
      missingInformationCaseIndex,
    } = this.state;

    let message = 'Some additional information need to be provided on Cases before saving the changes.';

    if (missingInformationCaseIndex != null && cases[missingInformationCaseIndex] != null) {
      const {
        liId,
        services,
      } = cases[missingInformationCaseIndex];

      message = `Some additional information need to be provided on ${liId} for the targets of ${services.join(', ')} before saving the changes.`;
    }

    return (
      <Fragment>
        {message}
      </Fragment>
    );
  }

  render(): JSX.Element | null {
    const {
      isAuditEnabled,
      userId,
      userName,
      changeTracker,
      history,
      warrantUserAudit,
    } = this.props;

    const {
      selectedTabId,
      isRetrievingWarrant,
      isRetrievingCases,
      isNewDeviceDetectedPopupOpen,
      isMissingInformationPopupOpen,
      isConfirmCancelPopupOpen,
      isSubmitInProgress,
      progressMessage,
      progressTotalSteps,
      progressStep,
      warrantToSubmit,
    } = this.state;

    if (!this.isReady()) {
      return null;
    }

    return (
      <div className="app-container-content warrant-edit" data-testid="WarrantEdit">
        <FormLayout>
          <FormLayoutContent>
            {this.renderFormPanel('Edit Warrant')}
            <Tabs
              id="WarrantEditTabs"
              onChange={this.handleTabChange}
              selectedTabId={selectedTabId}
              renderActiveTabPanelOnly
            >
              <Tab id={WARRANT_TAB_ID} title="Warrant & Devices" panel={this.renderWarrantAndDevices()} />
              <Tab id={CASES_TAB_ID} title="Cases" panel={this.renderCases()} />
            </Tabs>
          </FormLayoutContent>
          <FormLayoutFooter>
            {this.renderFormPanel(null)}
          </FormLayoutFooter>
        </FormLayout>
        <RetrievingDataProgress isOpen={isRetrievingWarrant} message="Retrieving warrants..." />
        <RetrievingDataProgress isOpen={isRetrievingCases} message="Retrieving cases..." />
        <Dialog
          title="Device changes detected"
          isOpen={isNewDeviceDetectedPopupOpen}
          canEscapeKeyClose={false}
          canOutsideClickClose={false}
          isCloseButtonShown={false}
        >
          <div className={Classes.DIALOG_BODY}>
            <div>Cases will be created/updated according to the changes. Please wait.</div>
            <div className="warrant-edit-creating-cases">Processing cases...</div>
            <ProgressBar className="warrant-edit-creating-cases-progress" intent="primary" value={0.5} />
          </div>
        </Dialog>
        <Dialog
          title="Missing information on Cases"
          isOpen={isMissingInformationPopupOpen}
          onClose={this.handleMissingInformationPopupClose}
        >
          <div className={Classes.DIALOG_BODY}>
            {this.renderMissingInformationPopupMessage()}
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button intent="primary" onClick={this.handleGoToCasesClick}>Go to Cases</Button>
            </div>
          </div>
        </Dialog>
        <ModalDialog
          displayMessage="Any changes will be lost. Click 'Ok' to continue."
          isOpen={isConfirmCancelPopupOpen}
          onSubmit={this.handleConfirmCancelSubmit}
          onClose={this.handleConfirmCancelClose}
          actionText="Ok"
        />
        <Dialog
          isOpen={isSubmitInProgress}
          title="Saving changes"
          isCloseButtonShown={false}
        >
          <div className={`${Classes.DIALOG_BODY} warrant-edit-submit-progress-body`}>
            <p className="warrant-edit-submit-progress-message">{progressMessage}</p>
            <ProgressBar intent="primary" value={progressStep / progressTotalSteps} />
          </div>
        </Dialog>
        <EditWarrantSubmit
          ref={this.submitRef}
          formData={warrantToSubmit}
          userAudit={warrantUserAudit}
          userId={userId}
          userName={userName}
          auditEnabled={isAuditEnabled}
          history={history}
          changeTracker={changeTracker}
          isOpen={false}
          mode={CONFIG_FORM_MODE.Edit}
          isDialog={false}
          hideProgress
          warrantBeforeFieldValues={this.originalWarrant}
          caseBeforeFieldValues={this.originalCases}
          onProgressStart={this.handleSubmitProgressStart}
          onProgressUpdate={this.handleSubmitProgressUpdate}
          onProgressEnd={this.handleSubmitProgressEnd}
          onNoChangeToUpdate={this.handleSubmitNoChangeToUpdate}
        />
      </div>
    );
  }
}

export default withRouter(connector(WarrantEdit));
