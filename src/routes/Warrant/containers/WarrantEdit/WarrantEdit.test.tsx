import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import moment from 'moment';
import cloneDeep from 'lodash.clonedeep';
import {
  fireEvent,
  getAllByDisplayValue,
  getAllByTestId,
  getAllByText,
  getByDisplayValue,
  getByLabelText,
  getByPlaceholderText,
  getByTestId,
  getByText,
  getByTitle,
  queryAllByTestId,
  queryByText,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as userActions from 'users/users-actions';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import { getAccessFunctionsByFilter } from 'data/accessFunction/accessFunction.api';
import AuthActions from 'data/authentication/authentication.types';
import { mockTemplates } from '__test__/mockTemplates';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { mockTeams } from '__test__/mockTeams';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockCustomization } from '__test__/mockCustomization';
import { getByDataTest } from '__test__/customQueries';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import WarrantEdit from './WarrantEdit';
import {
  mockAccessFunctions1,
  mockAccessFunctions2,
  mockAccessFunctions3,
  mockCases,
  mockCollectionFunctions,
  mockGizmoAccessFunctions,
  mockGizmoCases,
  mockGizmoTargets,
  mockGizmoWarrant,
  mockHi2Interfaces,
  mockHi3Interfaces,
  mockTargets,
  mockWarrants,
} from './__test__/mockWarrantEditData';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<WarrantEdit />', () => {
  let warrantConfigurationInstance: WarrantConfiguration;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['tp_access'],
    };

    warrantConfigurationInstance = new WarrantConfiguration({
      global: {
        selectedDF: '1',
      },
      warrants: {
        config: mockSupportedFeatures,
      },
      getAllAFsByFilter: (...args) => getAccessFunctionsByFilter(...args).then(({ data }) => ({
        value: { data },
      })),
      fetchCollectionFunctionListByFilter: jest.fn(),
      discoveryXcipio: {
        discoveryXcipioSettings: {
          deployment: 'andromeda',
        },
      },
    });

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: mockWarrants,
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{ zones: mockTimeZones }]),
    });

    store.dispatch({
      type: contactActions.GET_ALL_CONTACTS_FULFILLED,
      payload: buildAxiosServerResponse(mockContacts),
    });

    store.dispatch({
      type: userActions.GET_USER_TEAMS_FULFILLED,
      payload: buildAxiosServerResponse(mockTeams),
    });

    store.dispatch({
      type: warrantActions.SET_WARRANT_FORM_CONFIG_INSTANCE,
      payload: warrantConfigurationInstance,
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CUSTOMIZATION_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockCustomization]),
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });

    axiosMock.onGet('/warrants/0/MjA0ODExNDU5Mw').reply(200, { data: [mockWarrants[0]] });
    axiosMock.onGet('/warrants/0/MjA0ODExNDU5Mw/cases?ids=NUcgLSBVVA').reply(200, { data: mockCases });
    axiosMock.onGet('/warrants/0/MjA0ODExNDU5Mw/cases/NUcgLSBVVA/targets?ids=NUcgLSBVVCMx').reply(200, { data: mockTargets });
    axiosMock.onGet('/af-groups/0').reply(200, { data: [] });
    axiosMock.onGet('/cfs/0').reply(200, { data: mockCollectionFunctions });
    axiosMock.onGet('/afs/1?$filter=tag eq \'ERK_SMF\' or tag eq \'MVNR_5GSMSC\' or tag eq \'NOK_5GAMF\' or tag eq \'NOK_5GSMSF\' or tag eq \'NOK_5GUDM\' or tag eq \'NOK_5GSMF\' or tag eq \'ERK_5GAMF\'').reply(200, { data: mockAccessFunctions1 });
    axiosMock.onGet('/afs/1?$filter=tag eq \'NSN_SDM\' or tag eq \'ERK_SMF\' or tag eq \'MVNR_5GSMSC\' or tag eq \'NOK_5GAMF\' or tag eq \'NOK_5GSMSF\' or tag eq \'NOK_5GUDM\' or tag eq \'NOK_5GSMF\' or tag eq \'ERK_5GAMF\'').reply(200, { data: mockAccessFunctions1 });
    axiosMock.onGet('/afs/1?$filter=tag eq \'LU3G_CSCF\' or tag eq \'ACME_X123\' or tag eq \'GENVOIP\' or tag eq \'NSN_CSCF\' or tag eq \'SONUS_SBC\' or tag eq \'PVOIP\' or tag eq \'ALU_DSS\' or tag eq \'NORTEL_MTX\' or tag eq \'NORTEL_HLR\' or tag eq \'SS8_DF\' or tag eq \'NORTEL_MSC\' or tag eq \'NORTEL_DF\' or tag eq \'HP_HLR\' or tag eq \'SAMSUNG_MSC\' or tag eq \'MVNR_SBC\' or tag eq \'MVNR_HSS\' or tag eq \'MITEL_TAS\' or tag eq \'MVNR_CSCF\' or tag eq \'MVNR_PGW\' or tag eq \'EPS\' or tag eq \'ERK_SMF\' or tag eq \'ERK_VMME\' or tag eq \'ALU_MME\' or tag eq \'PCRF\' or tag eq \'CISCOVPCRF\' or tag eq \'HP_AAA\' or tag eq \'ALU_HLR\' or tag eq \'NSN_SAEGW\' or tag eq \'MV_SMSIWF\' or tag eq \'STA_GGSN\' or tag eq \'NSNHLR_SDM_PRGW\' or tag eq \'MVNR_5GSMSC\' or tag eq \'NOK_5GAMF\' or tag eq \'NOK_5GSMSF\' or tag eq \'NSN_SDM\' or tag eq \'NOK_5GUDM\' or tag eq \'NOK_5GSMF\' or tag eq \'ERK_5GAMF\' or tag eq \'MVNR_RCS_MSTORE\' or tag eq \'ICS_RCS\' or tag eq \'MVNR_RCS_AS\' or tag eq \'ALU_SGW\' or tag eq \'ERK_SGW\' or tag eq \'STA_PDSN\' or tag eq \'STARENT_EHA\' or tag eq \'BRW_AAA\' or tag eq \'ERK_SCEF\' or tag eq \'KODIAK_PTT\' or tag eq \'GIZMO\' or tag eq \'ETSI_GIZMO\'').reply(200, { data: mockAccessFunctions2 });
    axiosMock.onGet('/afs/1?$filter=tag eq \'NORTEL_MTX\' or tag eq \'NORTEL_HLR\' or tag eq \'SS8_DF\' or tag eq \'NORTEL_MSC\' or tag eq \'NORTEL_DF\' or tag eq \'HP_HLR\' or tag eq \'SAMSUNG_MSC\'').reply(200, { data: mockAccessFunctions3 });

    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTVHQ0Y/hi2-interfaces?$view=options').reply(200, { data: mockHi2Interfaces });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTVHQ0Y/hi3-interfaces?$view=options').reply(200, { data: mockHi3Interfaces });

    axiosMock.onGet('/warrants/0/MTQyNTU1MDkxNQ').reply(200, { data: [mockGizmoWarrant] });
    axiosMock.onGet('/warrants/0/MTQyNTU1MDkxNQ/cases?ids=Ui00').reply(200, { data: mockGizmoCases });
    axiosMock.onGet('/warrants/0/MTQyNTU1MDkxNQ/cases/Ui00/targets?ids=Ui00IzE').reply(200, { data: mockGizmoTargets });
    axiosMock.onGet('/afs/1?$filter=tag eq \'ETSI_GIZMO\'').reply(200, { data: mockGizmoAccessFunctions });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      /** *************** */
      // IMPORTANT: if WarrantEdit test fails after you have modified the mockSupportedFeatures, most likely you need
      // to update one of the axiosMock.onGet() lines above. Here is how to fix it:
      //   1. Uncomment the console.log(config) line below
      //   2. Run the failed test
      //   3. The console.log(config) will output an unhandled HTTP request.  Copy the request from the console.log
      //      output into one of the axiosMock.onGet() lines above that has the matching prefix
      // console.log(config);
      return [200, { data: [] }];
    });
  });

  it('should render the component with data-testid', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByTestId('WarrantEdit')).toBeVisible());
  });

  it('should render the form title with Cancel and Save buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());

    expect(screen.getByText('Warrant & Devices')).toBeVisible();
    expect(screen.getByText('Cases')).toBeVisible();

    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should load the Warrant Information', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    expect(screen.getByText('Warrant Name:')).toBeVisible();
    expect(screen.getByText('Start Time:')).toBeVisible();
    expect(screen.getByText('Stop Time:')).toBeVisible();
    expect(screen.getByText('Time Zone:')).toBeVisible();
    expect(screen.getByText('Contact:')).toBeVisible();
    expect(screen.getByText('Visibility:')).toBeVisible();
    expect(screen.getByText('LEA:')).toBeVisible();
    expect(screen.getByText('Intercept Type:')).toBeVisible();
    expect(screen.getByText('Case Options Template:')).toBeVisible();
    expect(screen.getByText('Case Options')).toBeVisible();
    expect(screen.getByText('Comments:')).toBeVisible();

    expect(screen.getByDisplayValue('5G - UT')).toBeVisible();
    expect(screen.getByDisplayValue('5G - UT')).toBeEnabled();
    expect(screen.getByDisplayValue('US/Pacific', { exact: false })).toBeVisible();
    expect(screen.getByDisplayValue('US/Pacific', { exact: false })).toBeEnabled();
    expect(screen.getByDisplayValue('01/20/21 12:00:00 AM')).toBeVisible();
    expect(screen.getByDisplayValue('01/20/21 12:00:00 AM')).toBeEnabled();
    expect(screen.getByDisplayValue('01/26/21 12:00:00 AM')).toBeVisible();
    expect(screen.getByDisplayValue('01/26/21 12:00:00 AM')).toBeEnabled();
    expect(screen.getByDisplayValue('Contact 2')).toBeVisible();
    expect(screen.getByDisplayValue('Contact 2')).toBeEnabled();
    expect(screen.getByDisplayValue('Unrestricted')).toBeVisible();
    expect(screen.getByDisplayValue('Unrestricted')).toBeEnabled();
    expect(screen.getByDisplayValue('Any')).toBeVisible();
    expect(screen.getByDisplayValue('Any')).toBeDisabled();
    expect(screen.getByDisplayValue('Data Only')).toBeVisible();
    expect(screen.getByDisplayValue('Data Only')).toBeEnabled();
    expect(screen.getByDisplayValue('With Location')).toBeVisible();
    expect(screen.getByDisplayValue('With Location')).toBeEnabled();
    expect(screen.getByDisplayValue('some comment to the warrant')).toBeVisible();
    expect(screen.getByDisplayValue('some comment to the warrant')).toBeEnabled();
  });

  it('should load the Case Options for the warrant', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.click(screen.getByText('Case Options'));

    await waitFor(() => expect(screen.getByText('Edit Case Options')).toBeVisible());

    const caseOptionsHeaderContainer = await screen.findByTestId('ConfigFormHeader');
    const caseOptionsBodyContainer = await screen.findByTestId('ConfigFormBody');

    expect(getByDisplayValue(caseOptionsHeaderContainer, 'With Location')).toBeVisible();

    expect(getByLabelText(caseOptionsBodyContainer, 'Enable Optional Parameters')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs')).toBeChecked();

    expect((getByDataTest(caseOptionsBodyContainer, 'input-traceLevel') as HTMLInputElement).value).toEqual('0');

    expect(getByLabelText(caseOptionsBodyContainer, 'Packet Envelope Message')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Packet Envelope Content')).not.toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Real Time Text')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Calling Party Number Display')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Dialed Digit Extraction Data')).toBeChecked();

    expect(getByLabelText(caseOptionsBodyContainer, 'Monitoring Replacement Parties Allowed')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Circuit Switched Voice Combined')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Include In-band and Out-of-band Delivery')).toBeChecked();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'REFNUM')).toBeEnabled();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'CCC Billing')).toBeEnabled();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'FLATRATE')).toBeEnabled();
    expect(getByDisplayValue(caseOptionsBodyContainer, 'NORMAL')).toBeEnabled();

    expect(getByLabelText(caseOptionsBodyContainer, 'Location')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'SMS')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Target')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'CISS Content')).toBeChecked();

    expect((getByDataTest(caseOptionsBodyContainer, 'input-packetSummaryCount') as HTMLInputElement).value).toEqual('0');
    expect((getByDataTest(caseOptionsBodyContainer, 'input-packetSummaryTimer') as HTMLInputElement).value).toEqual('0');

    expect(getByLabelText(caseOptionsBodyContainer, 'Headers from Content')).not.toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Summaries from Access Functions')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Headers from Access Functions')).toBeChecked();
  });

  it('should mark Start Time and Timezone as disabled if warrant is active', async () => {
    const startDate = moment();
    startDate.subtract(2, 'd');

    const stopDate = moment();
    stopDate.add(2, 'd');

    const warrant = mockWarrants[0];
    warrant.startDateTime = startDate.toISOString();
    warrant.stopDateTime = stopDate.toISOString();

    axiosMock.onGet('/warrants/0/MjA0ODExNDU5Mw').reply(200, { data: [mockWarrants[0]] });

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    expect(screen.getByDisplayValue('5G - UT')).toBeEnabled();
    expect(screen.getByDisplayValue('US/Pacific', { exact: false })).toBeDisabled();
    expect(container.querySelector('[name="startDateTime"]')).toBeDisabled();
    expect(container.querySelector('[name="stopDateTime"]')).toBeEnabled();
    expect(screen.getByDisplayValue('Contact 2')).toBeEnabled();
    expect(screen.getByDisplayValue('Unrestricted')).toBeEnabled();
    expect(screen.getByDisplayValue('Any')).toBeDisabled();
    expect(screen.getByDisplayValue('Data Only')).toBeEnabled();
    expect(screen.getByDisplayValue('With Location')).toBeEnabled();
    expect(screen.getByDisplayValue('some comment to the warrant')).toBeEnabled();
  });

  it('should load the Devices', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    const devicesContainer = screen.getByTestId('WarrantDevices');

    expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(1);

    const deviceRowContainer = screen.getByTestId('DeviceDetailRow');
    const deviceDetailIndexContainer = getByTestId(deviceRowContainer, 'DeviceDetailIndex');
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

    // Should load the row
    expect(getByTitle(deviceRowContainer, 'Delete')).toBeEnabled();
    expect(getByText(deviceDetailIndexContainer, '1')).toBeVisible();
    expect(getByText(deviceRowContainer, '5G data')).toBeVisible();
    expect(deviceDetailServiceContainer.querySelector('input')).toBeDisabled();
    expect(getByDisplayValue(deviceRowContainer, '8111111111')).toBeEnabled();

    expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible();
    expect(getByTestId(deviceRowContainer, 'targetDetails-IMEI')).toBeVisible();

    expect((getByTestId(deviceRowContainer, 'targetDetails-IMEI').querySelector('input') as HTMLInputElement).value).toBe('');

    // Should have the button enabled
    expect(getByText(devicesContainer, 'Add Device')).toBeEnabled();
  });

  it('should load the cases when there is just one case', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    const casesContainer = screen.getByTestId('WarrantCasesPanel');

    expect(screen.getByText('Case Name:')).toBeVisible();
    expect(screen.getByText('Entry Time:')).toBeVisible();
    expect(screen.getByText('Device:')).toBeVisible();
    expect(screen.getByText('Services:')).toBeVisible();
    expect(screen.getByText('Case Options Template:')).toBeVisible();
    expect(screen.getByText('Intercept Type:')).toBeVisible();
    expect(screen.getByText('CF:')).toBeVisible();
    expect(screen.getByText('Case Options')).toBeVisible();
    expect(screen.getByText('Case Interfaces')).toBeVisible();

    expect(getByDataTest(casesContainer, 'input-liId')).toHaveValue('Case 1');
    expect(getByDataTest(casesContainer, 'input-liId')).toBeDisabled();
    expect(getByDisplayValue(casesContainer, '01/26/21', { exact: false })).toBeDisabled();
    expect(getByDataTest(casesContainer, 'input-device')).toHaveValue('1');
    expect(getByDataTest(casesContainer, 'input-device')).toBeDisabled();
    expect(getByText(casesContainer, '5G data')).toBeVisible();
    expect(getByDisplayValue(casesContainer, 'With Location')).toBeEnabled();
    expect(getByDisplayValue(casesContainer, 'Data Only')).toBeEnabled();
    expect(getByDisplayValue(casesContainer, 'CA-DOJBNE-5GCF')).toBeEnabled();

    const targetsContainer = screen.getByTestId('TargetSummaryOfTargetPanelTargetsList');
    expect(getByText(targetsContainer, 'MSISDN:')).toBeVisible();
    expect(getByDisplayValue(targetsContainer, '18111111111')).toBeDisabled();

    const accessFunctionsContainer = screen.getByTestId('AdvancedProvisioningAccessFunctions');
    expect(getByText(accessFunctionsContainer, 'Provisioning for MSISDN')).toBeVisible();
    expect(getByLabelText(accessFunctionsContainer, 'ERK_SMF')).toBeChecked();
  });

  it('should load the Case Options for the case', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    fireEvent.click(screen.getByText('Case Options'));

    await waitFor(() => expect(screen.getByText('Edit Case Options')).toBeVisible());

    const caseOptionsHeaderContainer = await screen.findByTestId('ConfigFormHeader');
    const caseOptionsBodyContainer = await screen.findByTestId('ConfigFormBody');

    expect(getByDisplayValue(caseOptionsHeaderContainer, 'With Location')).toBeVisible();

    expect(getByLabelText(caseOptionsBodyContainer, 'Enable Optional Parameters')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs')).toBeChecked();

    expect((getByDataTest(caseOptionsBodyContainer, 'input-traceLevel') as HTMLInputElement).value).toEqual('0');

    expect(getByLabelText(caseOptionsBodyContainer, 'Packet Envelope Message')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Packet Envelope Content')).not.toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Real Time Text')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Calling Party Number Display')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Dialed Digit Extraction Data')).toBeChecked();

    expect(getByLabelText(caseOptionsBodyContainer, 'Monitoring Replacement Parties Allowed')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Circuit Switched Voice Combined')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Include In-band and Out-of-band Delivery')).toBeChecked();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'EMPTY')).toBeEnabled();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'CCC Billing')).toBeEnabled();

    expect(getByDisplayValue(caseOptionsBodyContainer, 'FLATRATE')).toBeEnabled();
    expect(getByDisplayValue(caseOptionsBodyContainer, 'NORMAL')).toBeEnabled();

    expect(getByLabelText(caseOptionsBodyContainer, 'Location')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'SMS')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Target')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'CISS Content')).toBeChecked();

    expect((getByDataTest(caseOptionsBodyContainer, 'input-packetSummaryCount') as HTMLInputElement).value).toEqual('0');
    expect((getByDataTest(caseOptionsBodyContainer, 'input-packetSummaryTimer') as HTMLInputElement).value).toEqual('0');

    expect(getByLabelText(caseOptionsBodyContainer, 'Headers from Content')).not.toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Summaries from Access Functions')).toBeChecked();
    expect(getByLabelText(caseOptionsBodyContainer, 'Headers from Access Functions')).toBeChecked();
  });

  it('should load the Case Interfaces for the case', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    fireEvent.click(screen.getByText('Case Interfaces'));

    await waitFor(() => expect(screen.getByText('Edit Case Interfaces')).toBeVisible());

    expect(screen.getByText('IP:222.222.222.222 PORT:4564')).toBeVisible();
  });

  it('should add a NEW device when clicking in the Add Device button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    const devicesContainer = screen.getByTestId('WarrantDevices');

    expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(1);

    fireEvent.click(getByText(devicesContainer, 'Add Device'));

    expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(2);

    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
    const deviceDetailIndexContainer = getByTestId(deviceRowContainer, 'DeviceDetailIndex');

    // Should load the row
    expect(getByTitle(deviceRowContainer, 'Delete')).toBeEnabled();
    expect(getByText(deviceDetailIndexContainer, '2')).toBeVisible();
    expect(getByText(deviceDetailIndexContainer, 'NEW')).toBeVisible();
    expect(getByPlaceholderText(deviceRowContainer, 'Search...')).toBeEnabled();

    // Should have the button enabled
    expect(getByText(devicesContainer, 'Add Device')).toBeEnabled();
  });

  it('should remove the device when clicking in the Delete icon', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    const devicesContainer = screen.getByTestId('WarrantDevices');

    expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(1);

    fireEvent.click(getByTitle(devicesContainer, 'Delete'));

    expect(queryAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(0);
  });

  it('should show a new cases after adding a new device', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());
    const devicesContainer = screen.getByTestId('WarrantDevices');

    fireEvent.click(getByText(devicesContainer, 'Add Device'));
    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

    fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
    fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

    expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

    await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
    const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

    fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    const caseListContainer = screen.getByTestId('WarrantStepPanelList');
    expect(getByText(caseListContainer, 'Case 1')).toBeVisible();
    expect(getByText(caseListContainer, 'Case 2')).toBeVisible();
    expect(queryByText(caseListContainer, 'Case 3')).toBeNull();
    expect(getAllByText(caseListContainer, 'new')).toHaveLength(1);
  });

  it('should allow editing the Case name for new cases', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());
    const devicesContainer = screen.getByTestId('WarrantDevices');

    fireEvent.click(getByText(devicesContainer, 'Add Device'));
    const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
    const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

    fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
    fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

    expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

    await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
    const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

    fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

    const caseListContainer = screen.getByTestId('WarrantStepPanelList');
    expect(getByText(caseListContainer, 'Case 1')).toBeVisible();
    expect(getByText(caseListContainer, 'Case 2')).toBeVisible();
    expect(queryByText(caseListContainer, 'Case 3')).toBeNull();
    expect(getAllByText(caseListContainer, 'new')).toHaveLength(1);

    expect(screen.getByLabelText('Case Name:*')).toBeDisabled();
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

    fireEvent.click(screen.getByText('Case 2'));

    expect(screen.getByLabelText('Case Name:*')).toBeEnabled();
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('');
    fireEvent.change(screen.getByLabelText('Case Name:*'), { target: { value: 'Case 2' } });
  });

  it('should remove the case when deleting the device', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    const devicesContainer = screen.getByTestId('WarrantDevices');

    expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(1);

    fireEvent.click(getByTitle(devicesContainer, 'Delete'));

    expect(queryAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(0);

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('No cases because no devices were entered')).toBeVisible());
  });

  it('should change the Intercept Type in cases then Intercept Type is changed in warrant', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    fireEvent.change(screen.getByDisplayValue('Data Only'), { target: { value: 'CC' } });
    expect(screen.getByDisplayValue('Content Only')).toBeVisible();

    // Wait for the cases to be updated before changing to the Cases tab
    await new Promise((resolve) => setTimeout(resolve, 500));

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());
    expect(screen.getByDisplayValue('Content Only')).toBeVisible();
  });

  it('should change the Case Options in cases then Case Options is changed in warrant', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
    await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());

    fireEvent.click(screen.getByText('Case Options'));

    await waitFor(() => expect(screen.getByText('Edit Case Options')).toBeVisible());

    let caseOptionsHeaderContainer = await screen.findByTestId('ConfigFormHeader');
    let caseOptionsBodyContainer = await screen.findByTestId('ConfigFormBody');
    let caseOptionsFooterContainer = await screen.findByTestId('ConfigFormFooter');

    expect(getByDisplayValue(caseOptionsHeaderContainer, 'With Location')).toBeVisible();
    fireEvent.change(getByDisplayValue(caseOptionsHeaderContainer, 'With Location'), { target: { value: '5' } });
    expect(getByDisplayValue(caseOptionsHeaderContainer, 'Without Location')).toBeVisible();

    expect(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs')).toBeChecked();

    fireEvent.click(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs'));
    expect(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs')).not.toBeChecked();

    fireEvent.click(getByText(caseOptionsFooterContainer, 'Save'));

    // Wait for the cases to be updated before changing to the Cases tab
    await new Promise((resolve) => setTimeout(resolve, 500));

    fireEvent.click(screen.getByText('Cases'));

    await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());
    expect(screen.getByDisplayValue('Without Location')).toBeVisible();

    fireEvent.click(screen.getByText('Case Options'));

    await waitFor(() => expect(screen.getByText('Edit Case Options')).toBeVisible());

    caseOptionsHeaderContainer = await screen.findByTestId('ConfigFormHeader');
    caseOptionsBodyContainer = await screen.findByTestId('ConfigFormBody');
    caseOptionsFooterContainer = await screen.findByTestId('ConfigFormFooter');

    expect(getByDisplayValue(caseOptionsHeaderContainer, 'Without Location')).toBeVisible();
    expect(getByLabelText(caseOptionsBodyContainer, 'Enable KPIs')).not.toBeChecked();
  });

  describe('when in Warrant & Devices tab', () => {
    it('should show message if no change and Save', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('No changes to update')).toBeVisible());
    });

    it('should validate the changes before going to the Cases tab', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.change(screen.getByDisplayValue('5G - UT'), { target: { value: '' } });

      fireEvent.click(screen.getByText('Cases'));

      expect(screen.getByText('Please enter a value.')).toBeVisible();
    });

    it('should validate fields on Save', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.change(screen.getByDisplayValue('5G - UT'), { target: { value: '' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      expect(screen.getByText('Please enter a value.')).toBeVisible();
    });

    it('should show success message on Save of a valid form', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.change(screen.getByDisplayValue('5G - UT'), { target: { value: '5G - UT 2' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Warrant updated successfully.')).toBeVisible());
    });

    it('should show error message on Save if server returns an error', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.change(screen.getByDisplayValue('5G - UT'), { target: { value: 'Name to fail' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Update warrant \'Name to fail\' - FAILED')).toBeVisible());
    });

    it('should show a confirm popup if clicking Cancel', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
    });

    xit('should close the Warrant Edit if Cancel and OK in confirm popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
      fireEvent.click(getByDataTest(document.body, 'modalDialog-button-save'));

      await waitFor(() => expect(screen.queryAllByText('Edit Warrant')).toHaveLength(0));
    });

    it('should remain in the warrant edit if Cancel and Cancel in confirm popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
      fireEvent.click(getByDataTest(document.body, 'modalDialog-button-cancel'));

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
    });

    it('should should the "Missing information on Cases" popup for new devices', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));
      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

      expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Missing information on Cases')).toBeVisible());
    });

    it('should show the "Missing information on Cases" popup if adding new devices that misses information on Cases', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: 'Circuit' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, 'Circuit Switch'));

      expect(getByText(deviceDetailServiceContainer, 'Circuit Switch')).toBeVisible();

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      const deviceDetailMINContainer = getByTestId(deviceRowContainer, 'targetDetails-MIN');
      fireEvent.change(deviceDetailMINContainer.querySelector('input') as HTMLInputElement, { target: { value: '8333333333' } });

      const deviceDetailCASEIDContainer = getByTestId(deviceRowContainer, 'targetDetails-CASEID');
      fireEvent.change(deviceDetailCASEIDContainer.querySelector('input') as HTMLInputElement, { target: { value: '844' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Missing information on Cases')).toBeVisible());
    });

    it('should go to the Cases when clicking in Go to Cases from "Missing information on Cases" popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: 'Circuit' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, 'Circuit Switch'));

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      const deviceDetailMINContainer = getByTestId(deviceRowContainer, 'targetDetails-MIN');
      fireEvent.change(deviceDetailMINContainer.querySelector('input') as HTMLInputElement, { target: { value: '8333333333' } });

      const deviceDetailCASEIDContainer = getByTestId(deviceRowContainer, 'targetDetails-CASEID');
      fireEvent.change(deviceDetailCASEIDContainer.querySelector('input') as HTMLInputElement, { target: { value: '844' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Missing information on Cases')).toBeVisible());

      fireEvent.click(screen.getByText('Go to Cases'));

      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      const caseListContainer = screen.getByTestId('WarrantStepPanelList');
      expect(getByText(caseListContainer, 'Case 2')).toBeVisible();
      expect(getByText(caseListContainer, 'new')).toBeVisible();

      const targetsContainer = screen.getByTestId('TargetSummaryOfTargetPanelTargetsList');
      expect(getAllByText(targetsContainer, 'Case Identity/MIN:')).toHaveLength(1);
      expect(getAllByText(targetsContainer, 'Case Identity/LIID:')).toHaveLength(1);
      expect(getAllByDisplayValue(targetsContainer, '8333333333')).toHaveLength(2);
    });
  });

  describe('when in Cases tab', () => {
    it('should show message if no change and Save', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('No changes to update')).toBeVisible());
    });

    it('should validate the changes before going to the Warrant & Devices tab', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));
      fireEvent.click(screen.getByText('Warrant & Devices'));

      await waitFor(() => expect(screen.getByText('Error:5G - UT each target needs to have at least one AF selected')).toBeVisible());
    });

    it('should validate fields on Save', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Error:5G - UT each target needs to have at least one AF selected')).toBeVisible());
    });

    it('should show success message on Save of a valid form', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));
      fireEvent.click(screen.getByLabelText('ERK_SMF'));

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Warrant updated successfully.')).toBeVisible());
    });

    it('should show error message on Save if server returns an error', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      fireEvent.change(screen.getByDisplayValue('5G - UT'), { target: { value: 'Name to fail' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Update warrant \'Name to fail\' - FAILED')).toBeVisible());
    });

    it('should show a confirm popup if clicking Cancel', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
    });

    xit('should go to the warrant list if Cancel and OK in confirm popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
      fireEvent.click(getByDataTest(document.body, 'modalDialog-button-save'));

      await waitFor(() => expect(screen.queryAllByText('Edit Warrant')).toHaveLength(0));
    });

    it('should remain in the warrant edit if Cancel and Cancel in confirm popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Cancel')[0]);

      await waitFor(() => expect(screen.getByText('Any changes will be lost. Click \'Ok\' to continue.')).toBeVisible());
      fireEvent.click(getByDataTest(document.body, 'modalDialog-button-cancel'));

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());
    });

    it('should indicate missing required info for invalid case', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));

      await waitFor(() => expect(screen.getByText('Complete all required case information before proceeding to save')).toBeVisible());
    });

    it('should hide the missing required indication once the data is completed', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));

      await waitFor(() => expect(screen.getByText('Complete all required case information before proceeding to save')).toBeVisible());

      fireEvent.click(screen.getByLabelText('ERK_SMF'));

      await waitFor(() => expect(screen.queryAllByText('Complete all required case information before proceeding to save')).toHaveLength(0));
    });

    it('should show a toast error if trying to go to another case while the current case has an error', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: 'Circuit' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, 'Circuit Switch'));

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      const deviceDetailMINContainer = getByTestId(deviceRowContainer, 'targetDetails-MIN');
      fireEvent.change(deviceDetailMINContainer.querySelector('input') as HTMLInputElement, { target: { value: '8333333333' } });

      const deviceDetailCASEIDContainer = getByTestId(deviceRowContainer, 'targetDetails-CASEID');
      fireEvent.change(deviceDetailCASEIDContainer.querySelector('input') as HTMLInputElement, { target: { value: '844' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByText('Case 2'));
      fireEvent.change(screen.getByLabelText('Case Name:*'), { target: { value: 'Case 2' } });

      fireEvent.click(screen.getByText('Case 1'));

      await waitFor(() => expect(screen.getByText('Case 2 Target MIN: Fill the required Target Info on the selected AFs')).toBeVisible());
    });

    it('should show a toast error if trying to save while another case has an error', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));
      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

      expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getAllByText('Save')[0]);

      await waitFor(() => expect(screen.getByText('Case 2 - Case Name: Please enter a value.')).toBeVisible());
    });

    it('should create the new cases with auto generated Case Name', async () => {
      const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
      const setting = discoverySettings.appSettings.find(({ name }) => name === 'shouldAutomaticallyGenerateCaseName');
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      setting!.setting = 'true';

      store.dispatch({
        type: GET_DX_APP_SETTINGS_FULFILLED,
        payload: buildAxiosServerResponse([discoverySettings]),
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Devices')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));
      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));

      expect(getByText(deviceDetailServiceContainer, '5G data')).toBeVisible();

      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');

      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));

      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      const caseListContainer = screen.getByTestId('WarrantStepPanelList');
      expect(getByText(caseListContainer, 'Case 1')).toBeVisible();
      expect(getByText(caseListContainer, 'Case 2')).toBeVisible();
      expect(queryByText(caseListContainer, 'Case 3')).toBeNull();
      expect(getAllByText(caseListContainer, 'new')).toHaveLength(1);

      expect(screen.getByLabelText('Case Name:*')).toBeDisabled();
      expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

      fireEvent.click(screen.getByText('Case 2'));

      expect(screen.getByLabelText('Case Name:*')).toBeEnabled();
      expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 2');
    });
  });

  describe('when deleting a case', () => {
    it('should show a confirm popup', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));
      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByText('Delete'));

      await waitFor(() => expect(screen.getByText('Delete Case')).toBeVisible());
    });

    it('should do nothing if clicking Cancel', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));
      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      fireEvent.click(screen.getByText('Delete'));

      await waitFor(() => expect(screen.getByText('Delete Case')).toBeVisible());

      fireEvent.click(getByDataTest(document.body, 'modalDialog-button-cancel'));

      expect(screen.getByText('Case Information')).toBeVisible();
    });

    it('should delete the case if clicking Yes, delete it', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));
      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());
      expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

      fireEvent.click(screen.getByText('Delete'));

      await waitFor(() => expect(screen.getByText('Delete Case')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, delete it'));

      expect(screen.getByText('Case Information')).toBeVisible();
      expect(screen.getByLabelText('Case Name:*')).toHaveValue('');
    });

    it('should remove the device from the list of devices if this is the only case in the service', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());
      const devicesContainer = screen.getByTestId('WarrantDevices');

      fireEvent.click(getByText(devicesContainer, 'Add Device'));

      const deviceRowContainer = screen.getAllByTestId('DeviceDetailRow')[1];
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      fireEvent.change(deviceDetailServiceContainer.querySelector('input') as HTMLInputElement, { target: { value: '5G data' } });
      fireEvent.click(getByText(document.body.querySelector('.bp3-popover-content') as HTMLElement, '5G data'));
      await waitFor(() => expect(getByTestId(deviceRowContainer, 'targetDetails-MDN')).toBeVisible());
      const deviceDetailMDNContainer = getByTestId(deviceRowContainer, 'targetDetails-MDN');
      fireEvent.change(deviceDetailMDNContainer.querySelector('input') as HTMLInputElement, { target: { value: '8222222222' } });

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());
      expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

      fireEvent.click(screen.getByText('Delete'));

      await waitFor(() => expect(screen.getByText('Delete Case')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, delete it'));

      await waitFor(() => expect(screen.getByLabelText('Case Name:*')).toHaveValue(''));
      fireEvent.change(screen.getByLabelText('Case Name:*'), { target: { value: 'Case 2' } });

      fireEvent.click(screen.getByText('Warrant & Devices'));

      await waitFor(() => expect(screen.queryAllByTestId('DeviceDetailRow')).toHaveLength(1));
    });
  });

  describe('Warrant with Gizmo Service', () => {
    it('should load warrant with Gizmo Service as expected', async () => {
      store.dispatch({
        type: warrantActions.BROADCAST_SELECTED_WARRANTS,
        selectedWarrants: [mockGizmoWarrant],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Warrant')).toBeVisible());
      await waitFor(() => expect(screen.getByText('Warrant Information')).toBeVisible());

      expect(screen.getByLabelText('Warrant Name:*')).toHaveValue('Gizmo Warrant');

      const devicesContainer = screen.getByTestId('WarrantDevices');

      expect(getAllByTestId(devicesContainer, 'DeviceDetailRow')).toHaveLength(1);

      const deviceRowContainer = screen.getByTestId('DeviceDetailRow');
      const deviceDetailIndexContainer = getByTestId(deviceRowContainer, 'DeviceDetailIndex');
      const deviceDetailServiceContainer = getByTestId(deviceRowContainer, 'DeviceDetailService');

      // Should load the row
      expect(getByTitle(deviceRowContainer, 'Delete')).toBeEnabled();
      expect(getByText(deviceDetailIndexContainer, '1')).toBeVisible();
      expect(getByText(deviceRowContainer, 'Gizmo ETSI')).toBeVisible();
      expect(deviceDetailServiceContainer.querySelector('input')).toBeDisabled();

      expect(getByTestId(devicesContainer, 'MDNwService')).toHaveTextContent('MDN');

      const mdnServiceTargetContainer = getByTestId(deviceRowContainer, 'targetDetails-MDNwService');
      expect(getByLabelText(mdnServiceTargetContainer, 'Service Type:*')).toHaveDisplayValue('Voice and Data');
      expect(getByLabelText(mdnServiceTargetContainer, 'Value:*')).toHaveDisplayValue('1212122555');

      fireEvent.click(screen.getByText('Cases'));
      await waitFor(() => expect(screen.getByText('Case Information')).toBeVisible());

      expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

      const targetContainers = screen.getAllByTestId('TargetDetails');
      expect(targetContainers).toHaveLength(1);

      expect(getByLabelText(targetContainers[0], 'Service Type:')).toHaveDisplayValue('Voice and Data');
      expect(getByLabelText(targetContainers[0], 'Value:')).toHaveDisplayValue('1212122555');
    });
  });
});
