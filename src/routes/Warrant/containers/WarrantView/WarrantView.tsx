import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Button, Tab, Tabs } from '@blueprintjs/core';
import { RootState } from 'data/root.reducers';
import { FormLayout, FormLayoutContent } from 'components/FormLayout';
import { FormLayoutFooter } from 'components/FormLayout/FormLayout';
import FormPanel from 'components/FormPanel';
import * as globalSelectors from 'global/global-selectors';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as warrantSelectors from 'data/warrant/warrant.selectors';
import * as caseActions from 'xcipio/cases/case-actions';
import * as caseSelectors from 'data/case/case.selectors';
import * as targetActions from 'xcipio/targets/targets-actions';
import * as targetSelectors from 'data/target/target.selectors';
import * as collectionFunctionActions from 'data/collectionFunction/collectionFunction.actions';
import WarrantAndDevicePanel from 'xcipio/warrants/WarrantAndDevicePanel';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { CaseModel } from 'data/case/case.types';
import { TEMPORARY_TARGET_ID_PREFIX } from 'xcipio/devices/forms/case-modeler';
import './warrant-view.scss';
import { RetrievingDataProgress } from 'components';
import { AppToaster } from 'shared/toaster';
import WarrantCasesPanel from 'routes/Warrant/components/WarrantCasesPanel';
import WarrantAdapter from 'data/warrant/warrant.adapter';
import ActionMenuBar from 'xcipio/warrants/actions-bar/action-menu';
import { Warrant } from 'data/warrant/warrant.types';
import { COMMON_GRID_ACTION } from 'shared/commonGrid/grid-enums';
import { WARRANT_PROVSTATE } from 'xcipio/warrants/warrant-enums';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: globalSelectors.getSelectedDeliveryFunctionId(state),
  selectedWarrant: warrantSelectors.getSelectedWarrant(state),
  warrant: warrantSelectors.getWarrant(state),
  cases: caseSelectors.getCases(state),
  targetsById: targetSelectors.getTargetsById(state),
  selectedCase: warrantSelectors.getSelectedWarrantCase(state),
});

const mapDispatch = {
  goToWarrantList: warrantActions.goToWarrantList,
  goToWarrantEdit: warrantActions.goToWarrantEdit,
  getWarrantById: warrantActions.getWarrantById,
  getAllCasesByCaseIds: caseActions.getAllCasesByCaseIds,
  getAllTargetsByTargetIds: targetActions.getAllTargetsByTargetIds,
  updateWarrantState: warrantActions.updateWarrantState,
  updateCasesState: caseActions.updateCasesState,
  clearWarrantState: warrantActions.clearWarrantState,
  clearCasesState: caseActions.clearCasesState,
  clearTargetState: targetActions.clearTargetState,
  clearFormTemplateUsed: warrantActions.clearFormTemplateUsed,
  clearWarrantTracker: warrantActions.clearWarrantTracker,
  clearCollectionFunctionState: collectionFunctionActions.clearCollectionFunctionState,
  broadcastSelectedWarrants: warrantActions.broadcastSelectedWarrants,
  goToCaseList: caseActions.goToCaseList,
  clearSelectedWarrantCase: warrantActions.clearSelectedWarrantCase,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type WarrantViewProps = PropsFromRedux & PropsFromRouter;

export type WarrantViewState = {
  selectedTabId: string
  isRetrievingWarrant: boolean
  isRetrievingCases: boolean
  hasCalledWarrantAction: boolean
  missingInformationCaseIndex: number | null
  warrantAction: string | null
}
const WARRANT_TAB_ID = 'warrantAndDevices';
const CASES_TAB_ID = 'cases';

export class WarrantView extends Component<WarrantViewProps, WarrantViewState> {
  private warrantRef: React.RefObject<WarrantAndDevicePanel>;
  private casesRef: React.RefObject<WarrantCasesPanel>;

  constructor(props: WarrantViewProps) {
    super(props);

    this.warrantRef = React.createRef<WarrantAndDevicePanel>();
    this.casesRef = React.createRef<WarrantCasesPanel>();

    const {
      clearCasesState,
    } = this.props;

    clearCasesState();

    this.state = {
      selectedTabId: WARRANT_TAB_ID,
      isRetrievingWarrant: true,
      isRetrievingCases: false,
      hasCalledWarrantAction: false,
      missingInformationCaseIndex: null,
      warrantAction: null,
    };
  }

  componentDidMount(): void {
    this.fetchWarrant();
    this.fetchCases();
  }

  componentWillUnmount(): void {
    this.clearData();
  }

  componentDidUpdate(prevProps: WarrantViewProps): void {
    const {
      warrant,
      cases,
      updateWarrantState,
      updateCasesState,
      broadcastSelectedWarrants,
    } = this.props;

    const {
      hasCalledWarrantAction,
      warrantAction,
    } = this.state;
    const isReprovisioned = warrantAction === COMMON_GRID_ACTION.Reprovision;
    if (warrant && hasCalledWarrantAction
      && WarrantView.shouldUpdateSelectedWarrant(prevProps.warrant, warrant, isReprovisioned)) {
      // Convert date/time fields to computer timezone.
      // Note that they will be converted back to warrant timezone before submit.
      const adaptedWarrant = WarrantAdapter.adaptWarrantToLoad(warrant);
      broadcastSelectedWarrants([warrant]);
      updateWarrantState(adaptedWarrant);
      this.setState({
        hasCalledWarrantAction: false,
        warrantAction: null,
      });
    }

    if (warrant != null && prevProps.warrant == null) {
      const adaptedWarrant = WarrantAdapter.adaptWarrantToLoad(warrant);
      updateWarrantState(adaptedWarrant);
    }

    if (cases.length > 0 && prevProps.cases.length === 0) {
      // Convert date/time fields to computer timezone.
      // Note that they will be converted back to warrant timezone before submit.
      const adaptedCases = WarrantAdapter.adaptCasesToLoad(cases);
      updateCasesState(adaptedCases);

      this.fetchTargets();
    }
  }

  static shouldUpdateSelectedWarrant(
    newWarrantState: Warrant | null,
    currentWarrantState: Warrant | null,
    isReprovisioned: boolean,
  ): boolean {
    if (!newWarrantState || !currentWarrantState) {
      return false;
    }

    const hasStatusChanged = newWarrantState?.state !== currentWarrantState?.state;
    const hasScheduleChanged =
      (newWarrantState?.startDateTime !== currentWarrantState?.startDateTime) ||
      (newWarrantState?.stopDateTime !== currentWarrantState?.stopDateTime);

    return hasStatusChanged || hasScheduleChanged || isReprovisioned;
  }

  clearData(): void {
    const {
      clearWarrantState,
      clearCasesState,
      clearTargetState,
      clearFormTemplateUsed,
      clearWarrantTracker,
      clearCollectionFunctionState,
      clearSelectedWarrantCase,
    } = this.props;

    clearWarrantState();
    clearCasesState();
    clearTargetState();
    clearFormTemplateUsed();
    clearWarrantTracker();
    clearCollectionFunctionState();
    clearSelectedWarrantCase();
  }

  fetchWarrant(): void {
    const {
      deliveryFunctionId,
      selectedWarrant,
      getWarrantById,
    } = this.props;

    if (selectedWarrant == null) {
      return;
    }

    getWarrantById(deliveryFunctionId, selectedWarrant.id);
  }

  fetchCases(): void {
    const {
      deliveryFunctionId,
      selectedWarrant,
      getAllCasesByCaseIds,
    } = this.props;

    if (selectedWarrant == null || selectedWarrant.cases.length === 0) {
      return;
    }

    const caseIds = selectedWarrant.cases.map(({ id }) => id).join(',');
    getAllCasesByCaseIds(deliveryFunctionId, selectedWarrant.id, caseIds);
  }

  fetchTargets(): void {
    const {
      deliveryFunctionId,
      cases,
      getAllTargetsByTargetIds,
    } = this.props;

    cases.forEach(({ targets, id: caseId, warrantId: { id: warrantId } }) => {
      if (targets.length === 0) {
        return;
      }

      const targetIds = targets.map(({ id }) => id).join(',');
      getAllTargetsByTargetIds(deliveryFunctionId, warrantId as string, caseId, targetIds);
    });
  }

  isReady(): boolean {
    const {
      warrant,
      cases,
    } = this.props;

    const {
      isRetrievingWarrant,
    } = this.state;

    if (warrant === null) {
      return false;
    }

    if (warrant.cases.length === 0 || warrant.state === WARRANT_PROVSTATE.Trash) {
      return true;
    }

    if (cases.length === 0 && isRetrievingWarrant) {
      return false;
    }

    const isEveryTargetLoaded = this.isEveryTargetLoaded(cases);

    if (!isEveryTargetLoaded) {
      return false;
    }

    return true;
  }

  isEveryTargetLoaded(cases: CaseModel[]): boolean {
    const {
      targetsById,
    } = this.props;

    return cases.every((caseEntry) => {
      return caseEntry.targets.every(({ id, primaryType }) => {
        const isNewTarget = primaryType ? id?.startsWith(primaryType) : false;

        return id == null || id.startsWith(TEMPORARY_TARGET_ID_PREFIX) || targetsById[id] != null || isNewTarget;
      });
    });
  }

  handleTabChange = async (newTabId: string): Promise<void> => {
    const { selectedTabId } = this.state;

    if (newTabId === selectedTabId) {
      return;
    }

    if (newTabId === WARRANT_TAB_ID) {
      await this.handleTabChangeToWarrant();
    } else {
      await this.handleTabChangeToCases();
    }
  }

  async handleTabChangeToWarrant(): Promise<void> {
    this.props.clearSelectedWarrantCase();
    this.setState({
      selectedTabId: WARRANT_TAB_ID,
      missingInformationCaseIndex: null,
    });
  }

  async handleTabChangeToCases(): Promise<void> {
    AppToaster.clear();

    this.setState({
      isRetrievingCases: true,
    });

    await this.warrantRef.current?.submit(true);

    const { selectedCase, cases } = this.props;
    let selectedCaseIndex = 0;
    if (selectedCase) {
      cases.find((warrantCase: any, index: number) => {
        if (warrantCase.id === selectedCase.id) {
          selectedCaseIndex = index;
          return true;
        }
        return false;
      });
    }

    this.setState({
      selectedTabId: CASES_TAB_ID,
      isRetrievingCases: false,
      missingInformationCaseIndex: selectedCaseIndex,
    });
  }

  handleDeviceReady = (): void => {
    const { selectedCase } = this.props;
    this.setState({
      isRetrievingWarrant: false,
    });
    if (selectedCase) {
      this.handleTabChangeToCases();
    }
  }

  handleClickBack = (): void => {
    const {
      history,
      goToWarrantList,
      goToCaseList,
      selectedCase,
    } = this.props;
    AppToaster.clear();
    if (selectedCase) {
      goToCaseList(history);
    } else {
      goToWarrantList(history);
    }
  }

  handleGoToCasesClick = (): void => {
    this.handleTabChange(CASES_TAB_ID);
  }

  handleReloadWarrant = async (): Promise<void> => {
    this.setState({
      hasCalledWarrantAction: true,
    });
    this.fetchWarrant();
    this.fetchCases();
  }

  handleWarrantAction = async (action: COMMON_GRID_ACTION): Promise<void> => {
    if (WarrantView.shouldRedirectToWarrantListAfterAction(action)) {
      this.handleClickBack();
    }
    this.setState({
      hasCalledWarrantAction: true,
      warrantAction: action,
    });
  }

  handleClickEdit = async (): Promise<void> => {
    const {
      history,
      goToWarrantEdit,
    } = this.props;

    AppToaster.clear();
    goToWarrantEdit(history);
    this.clearData();
  }

  static shouldRedirectToWarrantListAfterAction = (action: string): boolean => {
    return (
      action?.toLowerCase() === COMMON_GRID_ACTION.Trash ||
      action?.toLowerCase() === COMMON_GRID_ACTION.Untrash ||
      action?.toLowerCase() === COMMON_GRID_ACTION.Restore ||
      action?.toLowerCase() === COMMON_GRID_ACTION.Delete
    );
  }

  renderFormPanel(formTitle: string | null = null): JSX.Element {
    return (
      <FormPanel title={formTitle}>
        <div className="warrant-view-actions">
          <ActionMenuBar
            keepSelectedWarrantOnCancelReschedule
            handleEdit={this.handleClickEdit}
            onReloadWarrant={this.handleReloadWarrant}
            onWarrantAction={this.handleWarrantAction}
          />
          <Button
            className="warrant-view-actions-button"
            text="Back"
            intent="primary"
            onClick={this.handleClickBack}
          />
        </div>
      </FormPanel>
    );
  }

  renderWarrantAndDevices(): JSX.Element | undefined {
    const {
      warrant,
    } = this.props;

    if (warrant == null) {
      return;
    }

    return (
      <WarrantAndDevicePanel
        ref={this.warrantRef}
        mode={CONFIG_FORM_MODE.View}
        onDevicesReady={this.handleDeviceReady}
        warrantId={warrant.id}
      />
    );
  }

  renderCases(): JSX.Element {
    const {
      cases,
    } = this.props;

    const {
      missingInformationCaseIndex,
    } = this.state;

    return (
      <WarrantCasesPanel
        ref={this.casesRef}
        mode={CONFIG_FORM_MODE.View}
        cases={cases}
        selectedCaseIndex={missingInformationCaseIndex}
      />
    );
  }

  render(): JSX.Element | null {
    const {
      selectedTabId,
      isRetrievingWarrant,
      isRetrievingCases,
    } = this.state;

    if (!this.isReady()) {
      return null;
    }

    return (
      <div className="app-container-content warrant-edit" data-testid="WarrantView">
        <FormLayout>
          <FormLayoutContent>
            {this.renderFormPanel('View Warrant')}
            <Tabs
              id="WarrantViewTabs"
              onChange={this.handleTabChange}
              selectedTabId={selectedTabId}
            >
              <Tab id={WARRANT_TAB_ID} title="Warrant & Devices" panel={this.renderWarrantAndDevices()} />
              <Tab id={CASES_TAB_ID} title="Cases" panel={this.renderCases()} />
            </Tabs>
          </FormLayoutContent>
          <FormLayoutFooter>
            {this.renderFormPanel(null)}
          </FormLayoutFooter>
        </FormLayout>
        <RetrievingDataProgress isOpen={isRetrievingWarrant} message="Retrieving warrants..." />
        <RetrievingDataProgress isOpen={isRetrievingCases} message="Retrieving cases..." />
      </div>
    );
  }
}

export default withRouter(connector(WarrantView));
