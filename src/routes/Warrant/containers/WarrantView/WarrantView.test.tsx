import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter, Route } from 'react-router-dom';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as userActions from 'users/users-actions';
import * as caseActions from 'xcipio/cases/case-actions';
import AuthActions from 'data/authentication/authentication.types';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { buildAxiosServerResponse } from '__test__/utils';
import { getByDataTest } from '__test__/customQueries';
import {
  mockWarrant,
  mockCases,
  mockTarget1,
  mockTarget2,
  mockTarget3,
  mockCfs,
  mockAfFilter1,
  mockAfFilter2,
  mockFilter3,
  mockWarrantConfigInstance,
  mockHi2Interfaces1,
  mockHi2Interfaces2,
  mockHi2Interfaces3,
  mockHi3Interfaces1,
  mockHi3Interfaces2,
  mockHi3Interfaces3,
  mockStartDateTime,
  mockStopDateTime,
  mockEntryDateTime,
  mockWarrantSingleCase,
  mockCasesSingleCase,
  mockTargetsSingleCase,
  mockAfFilter1SingleCase,
  mockAfFilter2SingleCase,
  mockWarrantNoCases,
  mockWarrantRetryResponse,
  mockWarrantRetryResultDetails,
} from '__test__/mockWarrantsCaseTargets';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import { mockTeams } from '__test__/mockTeams';
import { getAccessFunctionsByFilter } from 'data/accessFunction/accessFunction.api';
import WarrantConfiguration from 'xcipio/config/warrant-config';
import moment from 'moment';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import cloneDeep from 'lodash.clonedeep';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import WarrantView from './WarrantView';
import { WARRANT_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<WarrantView />', () => {
  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: mockUserPrivileges,
    };

    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.BROADCAST_SELECTED_WARRANTS,
      selectedWarrants: [mockWarrant],
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
    discoverySettings.appSettings.forEach((entry) => {
      if (entry.name === 'isWarrantPauseActionEnabled' || entry.name === 'isWarrantResumeActionEnabled') {
        entry.setting = 'true';
      }

      if (entry.name === 'warrantDefaultExpireDays') {
        entry.setting = '0';
      }
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([discoverySettings]),
    });

    store.dispatch({
      type: userActions.GET_TIMEZONES_FULFILLED,
      payload: buildAxiosServerResponse([{
        id: 1,
        description: 'Uploaded on 1605893715.07',
        zones: mockTimeZones,
        dateCreated: '2020-11-20T17:35:15.000+0000',
      }]),
    });

    store.dispatch({
      type: userActions.GET_USER_TEAMS_FULFILLED,
      payload: buildAxiosServerResponse(mockTeams),
    });

    store.dispatch({
      type: warrantActions.SET_WARRANT_FORM_CONFIG_INSTANCE,
      payload: new WarrantConfiguration({
        global: {
          selectedDF: '0',
        },
        warrants: {
          config: mockWarrantConfigInstance.configuration,
        },
        getAllAFsByFilter: (...args) => getAccessFunctionsByFilter(...args).then((res) => ({
          value: { data: res.data },
        })),
        fetchCollectionFunctionListByFilter: jest.fn(),
        discoveryXcipio: {
          discoveryXcipioSettings: {
            deployment: 'andromeda',
          },
        },
      }),
    });

    axiosMock.onGet('/cfs/0').reply(200, { data: mockCfs });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [mockWarrant] });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES/cases?ids=VEVzdCBWaWV3,VEVzdCBWaWV3MDA,VEVzdCBWaWV3MDE').reply(200, { data: mockCases });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES/cases/VEVzdCBWaWV3/targets?ids=VEVzdCBWaWV3IzE,VEVzdCBWaWV3IzI,VEVzdCBWaWV3IzM').reply(200, { data: mockTarget1 });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES/cases/VEVzdCBWaWV3MDA/targets?ids=VEVzdCBWaWV3MDAjMQ,VEVzdCBWaWV3MDAjMg,VEVzdCBWaWV3MDAjMw').reply(200, { data: mockTarget2 });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES/cases/VEVzdCBWaWV3MDE/targets?ids=VEVzdCBWaWV3MDEjMQ,VEVzdCBWaWV3MDEjMg,VEVzdCBWaWV3MDEjMw,VEVzdCBWaWV3MDEjNA').reply(200, { data: mockTarget3 });
    axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES/state').reply(200, { data: [] });
    axiosMock.onPut('/warrants/0/WARRANT_ID_MANY_CASES/retry').reply(200, mockWarrantRetryResponse);
    axiosMock.onGet("/afs/0?$filter=tag eq 'EPS' or tag eq 'ERK_SMF' or tag eq 'ERK_VMME' or tag eq 'ALU_MME' or tag eq 'PCRF' or tag eq 'HP_AAA' or tag eq 'ALU_HLR' or tag eq 'ALU_SGW' or tag eq 'MV_SMSIWF' or tag eq 'ERK_SGW' or tag eq 'STA_GGSN' or tag eq 'NSNHLR_SDM_PRGW'").reply(200, { data: mockAfFilter1 });
    axiosMock.onGet("/afs/0?$filter=tag eq 'ALU_DSS' or tag eq 'NORTEL_MTX' or tag eq 'NORTEL_HLR' or tag eq 'SS8_DF' or tag eq 'NORTEL_MSC' or tag eq 'NORTEL_DF' or tag eq 'HP_HLR' or tag eq 'SAMSUNG_MSC' or tag eq 'MVNR_SBC' or tag eq 'MVNR_HSS' or tag eq 'MITEL_TAS' or tag eq 'MVNR_CSCF' or tag eq 'MVNR_PGW' or tag eq 'EPS' or tag eq 'ERK_SMF' or tag eq 'ERK_VMME' or tag eq 'ALU_MME' or tag eq 'PCRF' or tag eq 'HP_AAA' or tag eq 'ALU_HLR' or tag eq 'ALU_SGW' or tag eq 'MV_SMSIWF' or tag eq 'ERK_SGW' or tag eq 'STA_GGSN' or tag eq 'NSNHLR_SDM_PRGW' or tag eq 'MVNR_RCS_MSTORE' or tag eq 'ICS_RCS' or tag eq 'MVNR_RCS_AS' or tag eq 'NSN_CSCF' or tag eq 'STA_PDSN' or tag eq 'STARENT_EHA' or tag eq 'BRW_AAA' or tag eq 'ERK_SCEF' or tag eq 'ACME_X123' or tag eq 'GENVOIP' or tag eq 'SONUS_SBC' or tag eq 'KODIAK_PTT' or tag eq 'GIZMO'").reply(200, { data: mockAfFilter2 });
    axiosMock.onGet("/afs/0?$filter=tag eq 'NORTEL_MTX' or tag eq 'NORTEL_HLR' or tag eq 'SS8_DF' or tag eq 'NORTEL_MSC' or tag eq 'NORTEL_DF' or tag eq 'HP_HLR' or tag eq 'SAMSUNG_MSC'").reply(200, { data: mockFilter3 });

    axiosMock.onGet('/warrants/0/WARRANT_ID_ONE_CASES').reply(200, { data: [mockWarrantSingleCase] });
    axiosMock.onGet('/warrants/0/WARRANT_ID_ONE_CASES/cases?ids=bG9j').reply(200, { data: mockCasesSingleCase });
    axiosMock.onGet('/warrants/0/WARRANT_ID_ONE_CASES/cases/bG9j/targets?ids=bG9jIzE').reply(200, { data: mockTargetsSingleCase });
    axiosMock.onGet("/afs/0?$filter=tag eq 'ERK_SMF'").reply(200, { data: mockAfFilter1SingleCase });
    axiosMock.onGet("/afs/0?$filter=tag eq 'ALU_DSS' or tag eq 'NORTEL_MTX' or tag eq 'NORTEL_HLR' or tag eq 'SS8_DF' or tag eq 'NORTEL_MSC' or tag eq 'NORTEL_DF' or tag eq 'HP_HLR' or tag eq 'SAMSUNG_MSC' or tag eq 'MVNR_SBC' or tag eq 'MVNR_HSS' or tag eq 'MITEL_TAS' or tag eq 'MVNR_CSCF' or tag eq 'MVNR_PGW' or tag eq 'EPS' or tag eq 'ERK_SMF' or tag eq 'ERK_VMME' or tag eq 'ALU_MME' or tag eq 'PCRF' or tag eq 'HP_AAA' or tag eq 'ALU_HLR' or tag eq 'MV_SMSIWF' or tag eq 'STA_GGSN' or tag eq 'NSNHLR_SDM_PRGW' or tag eq 'MVNR_RCS_MSTORE' or tag eq 'ICS_RCS' or tag eq 'MVNR_RCS_AS' or tag eq 'NSN_CSCF' or tag eq 'ALU_SGW' or tag eq 'ERK_SGW' or tag eq 'STA_PDSN' or tag eq 'STARENT_EHA' or tag eq 'BRW_AAA' or tag eq 'ERK_SCEF' or tag eq 'ACME_X123' or tag eq 'GENVOIP' or tag eq 'SONUS_SBC' or tag eq 'KODIAK_PTT' or tag eq 'GIZMO'").reply(200, { data: mockAfFilter2SingleCase });

    axiosMock.onGet('/warrants/0/WARRANT_ID_NO_CASES').reply(200, { data: [mockWarrantNoCases] });

    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTVHQ0Y/hi2-interfaces?$view=options').reply(200, { data: mockHi2Interfaces1 });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTMzMTA4/hi2-interfaces?$view=options').reply(200, { data: mockHi2Interfaces2 });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLUoyNUE/hi2-interfaces?$view=options').reply(200, { data: mockHi2Interfaces3 });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTMzMTA4/hi3-interfaces?$view=options').reply(200, { data: mockHi3Interfaces1 });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLTVHQ0Y/hi3-interfaces?$view=options').reply(200, { data: mockHi3Interfaces2 });
    axiosMock.onGet('/cfs/0/Q0EtRE9KQk5FLUoyNUE/hi3-interfaces?$view=options').reply(200, { data: mockHi3Interfaces3 });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render Warrant Information in View Mode', async () => {
    const SELECTED_WARRANT = { ...mockWarrant };

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );
    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();
    await waitFor(() => expect(screen.getByDisplayValue('Any')).toBeVisible());

    expect(container.querySelector('.warrant-state')?.innerHTML).toBe(SELECTED_WARRANT.state);

    // Disabled inputs
    expect(getByDataTest(container, 'input-name')).toBeDisabled();
    expect(container.querySelector('[name=startDateTime]')).toBeDisabled();
    expect(container.querySelector('[name=stopDateTime]')).toBeDisabled();
    expect(getByDataTest(container, 'input-contact')).toBeDisabled();
    expect(getByDataTest(container, 'input-timeZone')).toBeDisabled();
    expect(getByDataTest(container, 'input-dxOwner')).toBeDisabled();
    expect(getByDataTest(container, 'input-lea')).toBeDisabled();
    expect(getByDataTest(container, 'input-caseType')).toBeDisabled();
    expect(getByDataTest(container, 'input-caseOptTemplateUsed')).toBeDisabled();
    expect(getByDataTest(container, 'input-comments')).toBeDisabled();

    // inputs values
    expect(screen.getByDisplayValue(SELECTED_WARRANT.name)).toBeVisible();
    expect(container.querySelector('[name=startDateTime]')?.getAttribute('value')).toBe(`${mockStartDateTime.format('MM/DD/YY')} 12:00:00 AM`); // StartTime
    expect(container.querySelector('[name=stopDateTime]')?.getAttribute('value')).toBe(`${mockStopDateTime.format('MM/DD/YY')} 12:00:00 AM`); // StopTime
    expect(getByDataTest(container, 'input-contact').getAttribute('value')).toBe(null);
    expect(screen.getByDisplayValue('US/Pacific', { exact: false })).toBeVisible();
    expect(getByDataTest(container, 'input-dxOwner').getAttribute('value')).toBe(null);
    expect(screen.getByDisplayValue('Any')).toBeVisible(); // LEA
    expect(screen.getByDisplayValue('All')).toBeVisible(); // Intercept Type
    expect(screen.getByDisplayValue('Unrestricted')).toBeVisible();

  });

  it('should render Warrant Case Options in View Mode', async () => {
    const SELECTED_WARRANT = { ...mockWarrant };

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    fireEvent.click(getByDataTest(container, 'input-caseOptionDetails') as Element);

    const configFormBody = await screen.findByTestId('ConfigFormBody');

    // check all inputs are disabled
    const allInputsDisabled = Array.from(configFormBody?.querySelectorAll('input') || [])
      .map((element) => element.getAttribute('disabled'))
      .every((disabled) => disabled === '');

    expect(allInputsDisabled).toBeTruthy();

    // case Options values
    expect(getByDataTest(configFormBody, 'input-kpi')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-traceLevel')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.traceLevel.toString());
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeMessage')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeContent')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-realTimeText')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callPartyNumberDisplay')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-directDigitExtractionData')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-monitoringReplacementPartiesAllowed')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-circuitSwitchedVoiceCombined')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-includeInBandOutBoundSignalling')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-location')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-target')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-sms')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callIndependentSupplementaryServicesContent')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-cccBillingNumber')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.cccBillingNumber.toString());
    expect(getByDataTest(configFormBody, 'input-summaryAF')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-headersAF')).toBeChecked();

    fireEvent.click(getByDataTest(document.body, 'configFormPopup-CancelOrOk') as Element);
  });

  it('should render Warrant Devices in View Mode', async () => {

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    // Check Devices headers
    const targetsHeaders = Array.from(container.querySelectorAll('.devices-form-list-header-data')).map((element) => element.innerHTML);
    expect(targetsHeaders).toEqual(['Device', 'Services', 'MSISDN', 'IMSI', 'IMEI', 'MIN', 'Case Identity/MIN', 'Case Identity/LIID']);

    const devicesFormListBody = container.querySelector('.devices-form-list-body');

    // check all inputs are disabled
    const allInputsDisabled = Array.from(devicesFormListBody?.querySelectorAll('input') || [])
      .map((element) => element.getAttribute('disabled'))
      .every((disabled) => disabled === '');

    expect(allInputsDisabled).toBeTruthy();

    const devices = devicesFormListBody?.querySelectorAll('[data-test="deviceDetail-device"]') || [];
    const firstDeviceTds = devices?.[0].querySelectorAll('td');
    const secondDeviceTds = devices?.[1].querySelectorAll('td');

    const firstDeviceServices = firstDeviceTds[1].querySelectorAll('.bp3-fill');
    const secondDeviceServices = secondDeviceTds[1].querySelectorAll('.bp3-fill');

    expect(devices?.length).toBe(2);

    // Devices Ids
    expect(firstDeviceTds[0].querySelector('.bp3-label')?.innerHTML).toBe('1');
    expect(secondDeviceTds[0].querySelector('.bp3-label')?.innerHTML).toBe('2');

    // Device 1 Services
    expect(firstDeviceServices?.length).toBe(2);
    expect(firstDeviceServices?.[0].innerHTML).toBe('5G data');
    expect(firstDeviceServices?.[1].innerHTML).toBe('4G PD');

    // Device 1 Targets
    expect(firstDeviceTds[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('11111111111');
    expect(firstDeviceTds[3].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('22222222222222');
    expect(firstDeviceTds[4].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('333333333333333');

    // Device 2 Services
    expect(secondDeviceServices?.length).toBe(1);
    expect(secondDeviceServices?.[0].innerHTML).toBe('Circuit Switch');

    expect(secondDeviceTds[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('14444444444');
    expect(secondDeviceTds[5].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('5555555555');
    expect(secondDeviceTds[7].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('6');

  });

  it('should render Warrant Cases in View Mode', async () => {

    // Here we are updating the global Cases state to ensure that having them
    // will break the WarrantView before it's rendering (it was an observed and fixed issue)
    store.dispatch({
      type: caseActions.GET_ALL_CASES_BY_CASEIDS_FULFILLED,
      payload: buildAxiosServerResponse([mockCases[0], mockCases[1]]),
    });

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);

    expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

    // check cases are ordered, they are originally sent as ['Case 2','Case 10', 'Case 1']
    const casesTabs = Array.from(container.querySelectorAll('.listMenuStyle span.MuiTypography-root')).map((element) => element.innerHTML);
    expect(casesTabs).toEqual(['Case 1', 'Case 2', 'Case 3']);

    fireEvent.click(screen.getByText('Case 1'));
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 1');

    fireEvent.click(screen.getByText('Case 2'));
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 2');

    fireEvent.click(screen.getByText('Case 3'));
    expect(screen.getByLabelText('Case Name:*')).toHaveValue('Case 10');
  });

  it('should render Warrant Case 1', async () => {

    const SELECTED_WARRANT = { ...mockWarrant };

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
    expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

    // check all inputs are disabled
    const allInputsDisabled = Array.from(container?.querySelectorAll('input') || [])
      .map((element) => element.getAttribute('disabled'))
      .every((disabled) => disabled === '');

    expect(allInputsDisabled).toBeTruthy();

    expect(await screen.findByDisplayValue('Case 1')).toBeVisible();

    expect(container.querySelector('[name=entryDateTime]')?.getAttribute('value')).toBe(`${mockEntryDateTime.format('MM/DD/YY')} 12:00:00 AM`); // entryDateTime

    // services
    expect(container.querySelectorAll('.wizardCaseContainer .bp3-popover-wrapper .bp3-fill')[0].textContent).toBe('5G data');
    expect(container.querySelectorAll('.wizardCaseContainer .bp3-popover-wrapper .bp3-fill')[1].textContent).toBe('4G PD');

    // test Case options
    fireEvent.click(getByDataTest(container, 'input-caseOptionDetails') as Element);

    const configFormBody = await screen.findByTestId('ConfigFormBody');

    // case Options values
    expect(getByDataTest(configFormBody, 'input-kpi')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-traceLevel')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.traceLevel.toString());
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeMessage')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeContent')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-realTimeText')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callPartyNumberDisplay')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-directDigitExtractionData')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-monitoringReplacementPartiesAllowed')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-circuitSwitchedVoiceCombined')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-includeInBandOutBoundSignalling')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-location')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-target')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-sms')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callIndependentSupplementaryServicesContent')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-cccBillingNumber')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.cccBillingNumber.toString());
    expect(getByDataTest(configFormBody, 'input-summaryAF')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-headersAF')).toBeChecked();

    fireEvent.click(getByDataTest(document.body, 'configFormPopup-CancelOrOk') as Element);

    // Targets Values
    const caseTargets = (await screen.findByTestId('TargetSummaryOfTargetPanelTargetsList')).querySelectorAll('.field');

    expect(caseTargets.length).toBe(3);

    fireEvent.click(caseTargets[1] as Element);
    // Check target input
    expect(caseTargets[1].querySelector('.bp3-label')?.textContent).toBe('IMSI:');
    expect(caseTargets[1].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('22222222222222');
    // Check target AF
    const case1AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case1AccessFunctions.length).toBe(1);
    expect(case1AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case1AccessFunctions[0].querySelector('input')).toBeChecked();

    fireEvent.click(caseTargets[2] as Element);
    expect(caseTargets[2].querySelector('.bp3-label')?.textContent).toBe('IMEI:');
    expect(caseTargets[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('333333333333333');
    // Check target AF
    const case2AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case2AccessFunctions.length).toBe(1);
    expect(case2AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case2AccessFunctions[0].querySelector('input')).toBeChecked();

    fireEvent.click(caseTargets[0] as Element);
    expect(caseTargets[0].querySelector('.bp3-label')?.textContent).toBe('MSISDN:');
    expect(caseTargets[0].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('11111111111');
    const case3AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case3AccessFunctions.length).toBe(1);
    expect(case3AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case3AccessFunctions[0].querySelector('input')).toBeChecked();

  });

  it('should render Warrant Case 2', async () => {

    const SELECTED_WARRANT = { ...mockWarrant };

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);

    expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

    // Go to Case 2
    fireEvent.click(container.querySelectorAll('.listMenuStyle span.MuiTypography-root')[1] as Element);

    // check all inputs are disabled
    const allInputsDisabled = Array.from(container?.querySelectorAll('input') || [])
      .map((element) => element.getAttribute('disabled'))
      .every((disabled) => disabled === '');

    expect(allInputsDisabled).toBeTruthy();

    expect(await screen.findByDisplayValue('Case 2')).toBeVisible();

    expect(container.querySelector('[name=entryDateTime]')?.getAttribute('value')).toBe(`${mockEntryDateTime.format('MM/DD/YY')} 12:00:00 AM`); // entryDateTime
    // services
    expect(container.querySelectorAll('.wizardCaseContainer .bp3-popover-wrapper .bp3-fill')[0].textContent).toBe('4G PD');

    // test Case options
    fireEvent.click(getByDataTest(container, 'input-caseOptionDetails') as Element);

    const configFormBody = await screen.findByTestId('ConfigFormBody');

    // case Options values
    expect(getByDataTest(configFormBody, 'input-kpi')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-traceLevel')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.traceLevel.toString());
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeMessage')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeContent')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-realTimeText')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callPartyNumberDisplay')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-directDigitExtractionData')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-monitoringReplacementPartiesAllowed')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-circuitSwitchedVoiceCombined')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-includeInBandOutBoundSignalling')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-location')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-target')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-sms')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callIndependentSupplementaryServicesContent')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-cccBillingNumber')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.cccBillingNumber.toString());
    expect(getByDataTest(configFormBody, 'input-summaryAF')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-headersAF')).toBeChecked();

    fireEvent.click(getByDataTest(document.body, 'configFormPopup-CancelOrOk') as Element);

    // Targets Values
    const caseTargets = (await screen.findByTestId('TargetSummaryOfTargetPanelTargetsList')).querySelectorAll('.field');

    expect(caseTargets.length).toBe(3);

    fireEvent.click(caseTargets[1] as Element);
    // Check target input
    expect(caseTargets[1].querySelector('.bp3-label')?.textContent).toBe('IMSI:');
    expect(caseTargets[1].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('22222222222222');
    // Check target AF
    const case1AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case1AccessFunctions.length).toBe(10);
    expect(case1AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case1AccessFunctions[0].querySelector('input')).toBeChecked();
    expect(case1AccessFunctions[1].textContent).toBe('ERK_VMME');
    expect(case1AccessFunctions[1].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[2].textContent).toBe('ALU_MME');
    expect(case1AccessFunctions[2].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[3].textContent).toBe('PCRF');
    expect(case1AccessFunctions[3].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[4].textContent).toBe('HP_AAA');
    expect(case1AccessFunctions[4].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[5].textContent).toBe('ALU_SGW');
    expect(case1AccessFunctions[5].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[6].textContent).toBe('MV_SMSIWF');
    expect(case1AccessFunctions[6].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[7].textContent).toBe('ERK_SGW');
    expect(case1AccessFunctions[7].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[8].textContent).toBe('STA_GGSN');
    expect(case1AccessFunctions[8].querySelector('input')).not.toBeChecked();
    expect(case1AccessFunctions[9].textContent).toBe('NSNHLR_SDM_PRGW');
    expect(case1AccessFunctions[9].querySelector('input')).not.toBeChecked();

    fireEvent.click(caseTargets[2] as Element);
    expect(caseTargets[2].querySelector('.bp3-label')?.textContent).toBe('IMEI:');
    expect(caseTargets[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('333333333333333');
    // Check target AF
    const case2AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case2AccessFunctions.length).toBe(4);
    expect(case2AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case2AccessFunctions[0].querySelector('input')).toBeChecked();
    expect(case2AccessFunctions[1].textContent).toBe('MV_SMSIWF');
    expect(case2AccessFunctions[1].querySelector('input')).not.toBeChecked();
    expect(case2AccessFunctions[2].textContent).toBe('ERK_SGW');
    expect(case2AccessFunctions[2].querySelector('input')).not.toBeChecked();
    expect(case2AccessFunctions[3].textContent).toBe('STA_GGSN');
    expect(case2AccessFunctions[3].querySelector('input')).not.toBeChecked();

    fireEvent.click(caseTargets[0] as Element);
    expect(caseTargets[0].querySelector('.bp3-label')?.textContent).toBe('MSISDN:');
    expect(caseTargets[0].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('11111111111');
    const case3AccessFunctions = (await screen.findByTestId('AccessFunctionList')).querySelectorAll('.access-function-item');
    expect(case3AccessFunctions.length).toBe(5);
    expect(case3AccessFunctions[0].textContent).toBe('ERK_SMF');
    expect(case3AccessFunctions[0].querySelector('input')).toBeChecked();
    expect(case3AccessFunctions[1].textContent).toBe('MV_SMSIWF');
    expect(case3AccessFunctions[1].querySelector('input')).toBeChecked();
    expect(case3AccessFunctions[2].textContent).toBe('ERK_SGW');
    expect(case3AccessFunctions[2].querySelector('input')).toBeChecked();
    expect(case3AccessFunctions[3].textContent).toBe('STA_GGSN');
    expect(case3AccessFunctions[3].querySelector('input')).toBeChecked();
    expect(case3AccessFunctions[4].textContent).toBe('NSNHLR_SDM_PRGW');
    expect(case3AccessFunctions[4].querySelector('input')).toBeChecked();

  });

  it('should render Warrant Case 3 (named as "Cases 10")', async () => {

    const SELECTED_WARRANT = { ...mockWarrant };

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <WarrantView />
        </MemoryRouter>
      </Provider>,
    );

    const warrantView = await screen.findByTestId('WarrantView');
    expect(warrantView).toBeVisible();

    fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
    expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

    // Go to Case 3
    fireEvent.click(container.querySelectorAll('.listMenuStyle span.MuiTypography-root')[2] as Element);

    // check all inputs are disabled
    const allInputsDisabled = Array.from(container?.querySelectorAll('input') || [])
      .map((element) => element.getAttribute('disabled'))
      .every((disabled) => disabled === '');

    expect(allInputsDisabled).toBeTruthy();

    expect(await screen.findByDisplayValue('Case 10')).toBeVisible();
    expect(container.querySelector('[name=entryDateTime]')?.getAttribute('value')).toBe(`${mockEntryDateTime.format('MM/DD/YY')} 12:00:00 AM`); // entryDateTime

    // services
    expect(container.querySelectorAll('.wizardCaseContainer .bp3-popover-wrapper .bp3-fill')[0].textContent).toBe('Circuit Switch');

    // test Case options
    fireEvent.click(getByDataTest(container, 'input-caseOptionDetails') as Element);

    const configFormBody = await screen.findByTestId('ConfigFormBody');

    // case Options values
    expect(getByDataTest(configFormBody, 'input-kpi')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-traceLevel')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.traceLevel.toString());
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeMessage')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-packetEnvelopeContent')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-realTimeText')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callPartyNumberDisplay')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-directDigitExtractionData')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-monitoringReplacementPartiesAllowed')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-circuitSwitchedVoiceCombined')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-includeInBandOutBoundSignalling')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-location')).not.toBeChecked();
    expect(getByDataTest(configFormBody, 'input-target')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-sms')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-callIndependentSupplementaryServicesContent')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-cccBillingNumber')?.getAttribute('value')).toBe(SELECTED_WARRANT.caseOptions?.cccBillingNumber.toString());
    expect(getByDataTest(configFormBody, 'input-summaryAF')).toBeChecked();
    expect(getByDataTest(configFormBody, 'input-headersAF')).toBeChecked();

    fireEvent.click(getByDataTest(document.body, 'configFormPopup-CancelOrOk') as Element);

    // Targets Values
    const caseTargets = (await screen.findByTestId('TargetSummaryOfTargetPanelTargetsList')).querySelectorAll('.field');

    expect(caseTargets.length).toBe(4);

    fireEvent.click(caseTargets[0] as Element);
    // Check target input
    expect(caseTargets[0].querySelector('.bp3-label')?.textContent).toBe('MIN:');
    expect(caseTargets[0].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('5555555555');
    // Check target AF
    const case1AccessFunctions = (await screen.findByTestId('AccessFunctionListWithAdditionalInfoButton')).querySelectorAll('.access-function-item');
    expect(case1AccessFunctions.length).toBe(1);
    expect(case1AccessFunctions[0].querySelector('label')?.textContent).toBe('NORTELMTX');

    fireEvent.click(caseTargets[1] as Element);
    expect(caseTargets[1].querySelector('.bp3-label')?.textContent).toBe('Case Identity/MIN:');
    expect(caseTargets[1].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('5555555555');

    fireEvent.click(caseTargets[2] as Element);
    expect(caseTargets[2].querySelector('.bp3-label')?.textContent).toBe('Case Identity/LIID:');
    expect(caseTargets[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('6');

    fireEvent.click(caseTargets[3] as Element);
    expect(caseTargets[3].querySelector('.bp3-label')?.textContent).toBe('MSISDN:');
    expect(caseTargets[3].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('14444444444');
    const case4AccessFunctions = (await screen.findByTestId('AccessFunctionListWithAdditionalInfoButton')).querySelectorAll('.access-function-item');
    expect(case4AccessFunctions.length).toBe(2);
    expect(case4AccessFunctions[0].textContent).toBe('HPHLR');
    expect(case4AccessFunctions[0].querySelector('input')).toBeChecked();
    expect(case4AccessFunctions[1].textContent).toBe('SamsungMSC');
    expect(case4AccessFunctions[1].querySelector('input')).toBeChecked();

  });

  describe('Warrant Actions', () => {
    it('should Move Warrant to Trash', async () => {

      /*
        This "router simulator" was created because for some reason the WarrantGrid component is not receiving
        props from redux as expected, so the test was breaking. As we need it only to check the redirect call
       */
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${WARRANT_ROUTE}/view`]}>
            <Route
              exact
              path={`${WARRANT_ROUTE}/`}
            >
              <div data-testid="WarrantGrid">
                Warrant Grid
              </div>
            </Route>
            <Route
              exact
              path={`${WARRANT_ROUTE}/view`}
            >
              <WarrantView />
            </Route>
          </MemoryRouter>
        </Provider>,
      );

      const warrantView = await screen.findByTestId('WarrantView');
      expect(warrantView).toBeVisible();

      const trashButtonTop = (container?.querySelectorAll('[data-test="delete-button"]') || [])[0];

      fireEvent.click(trashButtonTop as Element);

      const dialog = document.querySelector('.modalDialog');

      const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');

      fireEvent.click(confirmButton as Element);

      const warrantGrid = await screen.findByTestId('WarrantGrid');
      expect(warrantGrid).toBeVisible();

      expect(screen.getByText(`${mockWarrant.name} successfully deleted.`));
    });

    describe('when Reschedule Start, Stop, Pause, Warrant and Provisioning Retry', () => {

      it('should Reschedule Warrant', async () => {

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const newStartDateTime = moment(mockStartDateTime).add(2, 'days');
        const newStopDateTime = moment(mockStopDateTime).add(2, 'days');

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, {
          data: [{
            ...mockWarrant,
            startDateTime: newStartDateTime.toISOString(),
            stopDateTime: newStopDateTime.toISOString(),
          }],
        });

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        const rescheduleButtonTop = (container?.querySelectorAll('[data-test="reschedule-button"]') || [])[0];

        // Open modal
        fireEvent.click(rescheduleButtonTop as Element);

        expect(screen.getByText(`Schedule Warrant: ${mockWarrant.name}`)).toBeVisible();

        const rescheduleDialogFirstOpen = document.querySelector('.configFormPopup');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(rescheduleDialogFirstOpen as HTMLElement, 'configFormPopup-CancelOrOk') as Element);

        // Open modal again
        fireEvent.click(rescheduleButtonTop as Element);

        const rescheduleDialog = document.querySelector('.configFormPopup');
        const saveButton = getByDataTest(rescheduleDialog as HTMLElement, 'configFormPopup-Save');

        let startDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStartDateTime.format('ddd MMM DD YYYY')}"`);
        let stopDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStopDateTime.format('ddd MMM DD YYYY')}"`);

        if (startDateButton == null) {
          fireEvent.click(rescheduleDialog?.querySelectorAll('.DayPicker-NavButton--next')[0] as Element);
          startDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStartDateTime.format('ddd MMM DD YYYY')}"`);
        }

        if (stopDateButton == null) {
          fireEvent.click(rescheduleDialog?.querySelectorAll('.DayPicker-NavButton--next')[1] as Element);
          stopDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStopDateTime.format('ddd MMM DD YYYY')}"`);
        }

        fireEvent.click(startDateButton as Element);
        fireEvent.click(stopDateButton as Element);
        fireEvent.click(saveButton as Element);

        const startDateTime = await screen.findByDisplayValue(`${newStartDateTime.format('MM/DD/YY')} 12:00:00 AM`);
        const stopDateTime = await screen.findByDisplayValue(`${newStopDateTime.format('MM/DD/YY')} 12:00:00 AM`);

        expect(container.querySelector('[name=stopDateTime]')?.getAttribute('value')).toBe(`${newStopDateTime.format('MM/DD/YY')} 12:00:00 AM`); // StopTime

        expect(startDateTime).toBeVisible();
        expect(stopDateTime).toBeVisible();
      });

      it('should Reschedule Warrant from Cases tab', async () => {

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const newStartDateTime = moment(mockStartDateTime).add(2, 'days');
        const newStopDateTime = moment(mockStopDateTime).add(2, 'days');

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, {
          data: [{
            ...mockWarrant,
            startDateTime: newStartDateTime.toISOString(),
            stopDateTime: newStopDateTime.toISOString(),
          }],
        });

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
        expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

        const rescheduleButtonTop = (container?.querySelectorAll('[data-test="reschedule-button"]') || [])[0];

        // Open modal
        fireEvent.click(rescheduleButtonTop as Element);

        expect(screen.getByText(`Schedule Warrant: ${mockWarrant.name}`)).toBeVisible();

        const rescheduleDialogFirstOpen = document.querySelector('.configFormPopup');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(rescheduleDialogFirstOpen as HTMLElement, 'configFormPopup-CancelOrOk') as Element);

        // Open modal again
        fireEvent.click(rescheduleButtonTop as Element);

        const rescheduleDialog = document.querySelector('.configFormPopup');

        let startDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStartDateTime.format('ddd MMM DD YYYY')}"`);
        let stopDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStopDateTime.format('ddd MMM DD YYYY')}"`);

        if (startDateButton == null) {
          fireEvent.click(rescheduleDialog?.querySelectorAll('.DayPicker-NavButton--next')[0] as Element);
          startDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStartDateTime.format('ddd MMM DD YYYY')}"`);
        }

        if (stopDateButton == null) {
          fireEvent.click(rescheduleDialog?.querySelectorAll('.DayPicker-NavButton--next')[1] as Element);
          stopDateButton = rescheduleDialog?.querySelector(`[aria-label="${newStopDateTime.format('ddd MMM DD YYYY')}"`);
        }

        const saveButton = getByDataTest(rescheduleDialog as HTMLElement, 'configFormPopup-Save');

        fireEvent.click(startDateButton as Element);
        fireEvent.click(stopDateButton as Element);
        fireEvent.click(saveButton as Element);

        const startDateTime = await screen.findByDisplayValue(`${newStartDateTime.format('MM/DD/YY')} 12:00:00 AM`);
        const stopDateTime = await screen.findByDisplayValue(`${newStopDateTime.format('MM/DD/YY')} 12:00:00 AM`);

        expect(container.querySelector('[name=stopDateTime]')?.getAttribute('value')).toBe(`${newStopDateTime.format('MM/DD/YY')} 12:00:00 AM`); // StopTime

        expect(startDateTime).toBeVisible();
        expect(stopDateTime).toBeVisible();
      });

      it('should Stop Warrant', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'ACTIVE' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        const stopButtonTop = (container?.querySelectorAll('[data-test="stop-button"]') || [])[0];

        // Open modal
        fireEvent.click(stopButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to stop warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(stopButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'STOPPED' }] });

        const warrantWithNewState = await screen.findByText('STOPPED');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully stopped.`));
      });

      it('should Stop Warrant from Cases tab', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'ACTIVE' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
        expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

        const stopButtonTop = (container?.querySelectorAll('[data-test="stop-button"]') || [])[0];

        // Open modal
        fireEvent.click(stopButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to stop warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(stopButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'STOPPED' }] });

        const warrantWithNewState = await screen.findByText('STOPPED');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully stopped.`));
      });

      it('should Start a stopped Warrant', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'STOPPED' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        const startButtonTop = (container?.querySelectorAll('[data-test="start-button"]') || [])[0];

        // Open modal
        fireEvent.click(startButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to start warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(startButtonTop as Element);

        fireEvent.click(startButtonTop as Element);
        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'ACTIVE' }] });

        const warrantWithNewState = await screen.findByText('ACTIVE');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully started.`));
      });

      it('should Start a stopped Warrant from Cases tab', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'STOPPED' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
        expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

        const startButtonTop = (container?.querySelectorAll('[data-test="start-button"]') || [])[0];

        // Open modal
        fireEvent.click(startButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to start warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(startButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'ACTIVE' }] });

        const warrantWithNewState = await screen.findByText('ACTIVE');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully started.`));
      });

      it('should Pause Warrant', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'ACTIVE' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        const pauseButtonTop = (container?.querySelectorAll('[data-test="pause-button"]') || [])[0];

        // Open modal
        fireEvent.click(pauseButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to pause warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(pauseButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'PAUSED' }] });

        const warrantWithNewState = await screen.findByText('PAUSED');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully paused.`));
      });

      it('should Pause Warrant from Cases tab', async () => {

        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'ACTIVE' }],
        });

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);
        expect(await screen.findByTestId('WarrantCasesPanel')).toBeVisible();

        const pauseButtonTop = (container?.querySelectorAll('[data-test="pause-button"]') || [])[0];

        // Open modal
        fireEvent.click(pauseButtonTop as Element);

        expect(screen.getByText(`Are you sure you want to pause warrant '${mockWarrant.name}' ?`)).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(pauseButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        fireEvent.click(confirmButton as Element);

        axiosMock.onGet('/warrants/0/WARRANT_ID_MANY_CASES').reply(200, { data: [{ ...mockWarrant, state: 'PAUSED' }] });

        const warrantWithNewState = await screen.findByText('PAUSED');
        expect(warrantWithNewState).toBeVisible();

        expect(screen.getByText(`${mockWarrant.name} successfully paused.`));
      });

      it('should dispatch Provisioning Retry for the warrant', async () => {

        const { container } = render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        const reprovisionButtonTop = (container?.querySelectorAll('[data-test="reprovision-button"]') || [])[0];

        // Open modal
        fireEvent.click(reprovisionButtonTop as Element);

        expect(screen.getByText('Do you want to retry provisioning the failed targets again?')).toBeVisible();

        const dialogFirstOpen = document.querySelector('.modalDialog');
        // close modal to check it will be able to open again
        fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Open modal again
        fireEvent.click(reprovisionButtonTop as Element);

        const dialog = document.querySelector('.modalDialog');

        // click Retry
        const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
        act(() => {
          fireEvent.click(confirmButton as Element);
        });

        const viewDetailsButton = await screen.findByText('View Details');

        fireEvent.click(viewDetailsButton as Element);

        expect(screen.getByText('Completed - 17 succeeded and 1 failed')).toBeVisible();

        expect(screen.getByText(mockWarrantRetryResultDetails[0].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[1].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[2].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[3].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[4].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[5].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[6].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[7].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[8].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[9].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[10].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[11].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[12].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[13].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[14].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[15].message)).toBeVisible();
        expect(screen.getByText(mockWarrantRetryResultDetails[16].message)).toBeVisible();

        fireEvent.click(getByDataTest(dialog as HTMLElement, 'modalDialog-button-cancel') as Element);

        // Checking the Devices, because there was an issues what was cleaning the devices services after reprovision
        const targetsHeaders = Array.from(container.querySelectorAll('.devices-form-list-header-data')).map((element) => element.innerHTML);
        expect(targetsHeaders).toEqual(['Device', 'Services', 'MSISDN', 'IMSI', 'IMEI', 'MIN', 'Case Identity/MIN', 'Case Identity/LIID']);

        const devicesFormListBody = container.querySelector('.devices-form-list-body');

        // check all inputs are disabled
        const allInputsDisabled = Array.from(devicesFormListBody?.querySelectorAll('input') || [])
          .map((element) => element.getAttribute('disabled'))
          .every((disabled) => disabled === '');

        expect(allInputsDisabled).toBeTruthy();

        const devices = devicesFormListBody?.querySelectorAll('[data-test="deviceDetail-device"]') || [];
        const firstDeviceTds = devices?.[0].querySelectorAll('td');
        const secondDeviceTds = devices?.[1].querySelectorAll('td');

        const firstDeviceServices = firstDeviceTds[1].querySelectorAll('.bp3-fill');
        const secondDeviceServices = secondDeviceTds[1].querySelectorAll('.bp3-fill');

        expect(devices?.length).toBe(2);

        // Devices Ids
        expect(firstDeviceTds[0].querySelector('.bp3-label')?.innerHTML).toBe('1');
        expect(secondDeviceTds[0].querySelector('.bp3-label')?.innerHTML).toBe('2');

        // Device 1 Services
        expect(firstDeviceServices?.length).toBe(2);
        expect(firstDeviceServices?.[0].innerHTML).toBe('5G data');
        expect(firstDeviceServices?.[1].innerHTML).toBe('4G PD');

        // Device 1 Targets
        expect(firstDeviceTds[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('11111111111');
        expect(firstDeviceTds[3].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('22222222222222');
        expect(firstDeviceTds[4].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('333333333333333');

        // Device 2 Services
        expect(secondDeviceServices?.length).toBe(1);
        expect(secondDeviceServices?.[0].innerHTML).toBe('Circuit Switch');

        // Device 2 Targets
        expect(secondDeviceTds[2].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('14444444444');
        expect(secondDeviceTds[5].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('5555555555');
        expect(secondDeviceTds[7].querySelector('[name="primaryValue"]')?.getAttribute('value')).toBe('6');

      });

      it('should not show the Pause button if setting it not enabled', async () => {
        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'ACTIVE' }],
        });

        const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
        discoverySettings.appSettings.forEach((entry) => {
          if (entry.name === 'isWarrantPauseActionEnabled' || entry.name === 'isWarrantResumeActionEnabled') {
            entry.setting = 'false';
          }
        });

        store.dispatch({
          type: GET_DX_APP_SETTINGS_FULFILLED,
          payload: buildAxiosServerResponse([discoverySettings]),
        });

        render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        expect(screen.queryByText('Pause')).toBeNull();
      });

      it('should not show the Pause button if setting it not enabled', async () => {
        store.dispatch({
          type: warrantActions.BROADCAST_SELECTED_WARRANTS,
          selectedWarrants: [{ ...mockWarrant, state: 'PAUSED' }],
        });

        const discoverySettings = cloneDeep(mockDiscoveryXcipioSettings);
        discoverySettings.appSettings.forEach((entry) => {
          if (entry.name === 'isWarrantPauseActionEnabled' || entry.name === 'isWarrantResumeActionEnabled') {
            entry.setting = 'false';
          }
        });

        store.dispatch({
          type: GET_DX_APP_SETTINGS_FULFILLED,
          payload: buildAxiosServerResponse([discoverySettings]),
        });

        render(
          <Provider store={store}>
            <MemoryRouter>
              <WarrantView />
            </MemoryRouter>
          </Provider>,
        );

        const warrantView = await screen.findByTestId('WarrantView');
        expect(warrantView).toBeVisible();

        expect(screen.queryByText('Resume')).toBeNull();
      });
    });
  });

  describe('Warrant with only one Case', () => {
    it('should not render the cases list menu aside the case form', async () => {
      store.dispatch({
        type: warrantActions.BROADCAST_SELECTED_WARRANTS,
        selectedWarrants: [mockWarrantSingleCase],
      });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantView />
          </MemoryRouter>
        </Provider>,
      );

      const warrantView = await screen.findByTestId('WarrantView');
      expect(warrantView).toBeVisible();

      fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);

      expect(screen.getByTestId('WarrantCasesPanel')).toBeVisible();

      const casesTabs = Array.from(container.querySelectorAll('.listMenuStyle span.MuiTypography-root')).map((element) => element.innerHTML);
      expect(casesTabs).toHaveLength(0);
      expect(screen.getByDisplayValue('Single Case 1')).toBeVisible();
    });
  });

  describe('Warrant with no Cases', () => {
    it('should render the message indicating that there are no cases', async () => {
      store.dispatch({
        type: warrantActions.BROADCAST_SELECTED_WARRANTS,
        selectedWarrants: [mockWarrantNoCases],
      });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <WarrantView />
          </MemoryRouter>
        </Provider>,
      );

      const warrantView = await screen.findByTestId('WarrantView');
      expect(warrantView).toBeVisible();

      fireEvent.click(container.querySelector('[data-tab-id="cases"]') as Element);

      expect(screen.getByTestId('WarrantCasesPanel')).toBeVisible();

      const casesTabs = Array.from(container.querySelectorAll('.listMenuStyle span.MuiTypography-root')).map((element) => element.innerHTML);
      expect(casesTabs).toHaveLength(0);
      expect(screen.getByText('No cases because no devices were entered')).toBeVisible();
    });
  });

});
