import React, { FC } from 'react';
import { Icon, Intent } from '@blueprintjs/core';
import './response-details.scss';

type Response = {
  status: string,
  message: string
}

type Props = {
  responses: Response[],
}

const renderResponseStatus = (status: string) => {
  if (status === 'Succeeded') {
    return <Icon icon="tick" intent={Intent.SUCCESS} />;
  }
  return <Icon icon="cross" intent={Intent.DANGER} />;
};

const ResponseDetails: FC<Props> = ({
  responses,
}: Props) => {
  return (
    <div className="response-details-container">
      {
        responses.map((response: Response) => {
          return (
            <div className="response">
              <span>
                {
                  renderResponseStatus(response.status)
                }
              </span>
              <span className="response-message">
                {response.message}
              </span>
            </div>
          );
        })
      }
    </div>
  );
};

export default ResponseDetails;
