import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { Provider } from 'react-redux';
import { MemoryRouter, Route } from 'react-router-dom';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import Http from 'shared/http';
import store from 'data/store';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as caseActions from 'xcipio/cases/case-actions';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { getByDataTest } from '__test__/customQueries';
import {
  mockWarrant,
  mockWarrantRetryResponse,
  mockWarrantRetryResultDetails,
} from '__test__/mockWarrantsCaseTargets';
import { mockUserPrivileges } from '__test__/mockUserPrivileges';
import moment from 'moment';
import cloneDeep from 'lodash.clonedeep';
import WarrantRetryPopup from './WarrantRetryPopup';
import { WARRANT_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

const WarrantRetryPopupProps = {
  handleConfirm: jest.fn(),
  isOpen: true,
  warrant: {
    id: mockWarrant.id,
    name: mockWarrant.name,
  },
  isCalledfromCase: false,
  onReload: jest.fn(),
  onWarrantAction: jest.fn(),
};

describe('<WarrantRetryPopup />', () => {
  beforeEach(() => {
    axiosMock.onPut('/warrants/0/WARRANT_ID_MANY_CASES/retry').reply(200, mockWarrantRetryResponse);

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      return [200, { data: [] }];
    });
  });

  describe('when Provisioning Retry', () => {
    it('should display generic message when opened from Warrant View page', async () => {

      render(
        <Provider store={store}>
          <WarrantRetryPopup {...WarrantRetryPopupProps} />
        </Provider>,
      );

      const WarrantRetryPopupView = await screen.findByTestId('WarrantRetryPopup');
      expect(WarrantRetryPopupView).toBeVisible();
      expect(screen.getByText('Do you want to retry provisioning the failed targets again?')).toBeVisible();
    });

    it('should display message with warrant name if opened from warrant and case grid actions', async () => {
      const props = cloneDeep(WarrantRetryPopupProps);
      props.isCalledfromCase = true;
      render(
        <Provider store={store}>
          <WarrantRetryPopup {...props} />
        </Provider>,
      );

      const WarrantRetryPopupView = await screen.findByTestId('WarrantRetryPopup');
      expect(WarrantRetryPopupView).toBeVisible();
      expect(screen.getByText('Do you want to retry provisioning of all targets in TEst View?')).toBeVisible();
    });

    it('should call handle confirm on click cancel', async () => {

      render(
        <Provider store={store}>
          <WarrantRetryPopup {...WarrantRetryPopupProps} />
        </Provider>,
      );
      const dialogFirstOpen = document.querySelector('.modalDialog');
      fireEvent.click(getByDataTest(dialogFirstOpen as HTMLElement, 'modalDialog-button-cancel') as Element);
      expect(WarrantRetryPopupProps.handleConfirm).toBeCalled();
    });

    it('should open progress panel and show view details button', async () => {

      render(
        <Provider store={store}>
          <WarrantRetryPopup {...WarrantRetryPopupProps} />
        </Provider>,
      );
      const dialog = document.querySelector('.modalDialog');

      // click Retry
      const confirmButton = getByDataTest(dialog as HTMLElement, 'modalDialog-button-save');
      act(() => {
        fireEvent.click(confirmButton as Element);
      });

      const viewDetailsButton = await screen.findByText('View Details');
      expect(viewDetailsButton).toBeVisible();
      expect(screen.getByText('Completed - 17 succeeded and 1 failed')).toBeVisible();
      fireEvent.click(viewDetailsButton as Element);
      expect(screen.getByText(mockWarrantRetryResultDetails[0].message)).toBeVisible();
      expect(screen.getByText(mockWarrantRetryResultDetails[1].message)).toBeVisible();
      const hideDetailsButton = await screen.findByText('Hide Details');
      expect(hideDetailsButton).toBeVisible();
      expect(screen.queryByText('View Details')).toBeNull();
      fireEvent.click(hideDetailsButton as Element);
      expect(screen.queryByText(mockWarrantRetryResultDetails[0].message)).toBeNull();
      expect(screen.queryByText(mockWarrantRetryResultDetails[1].message)).toBeNull();
    });
  });
});
