import { ProgressBar } from 'components';
import { Button, Dialog, Classes } from '@blueprintjs/core';
import { connect, ConnectedProps } from 'react-redux';
import React, {
  FC, useState, useEffect, Fragment,
} from 'react';
import ModalDialog from 'shared/modal/dialog';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as globalSelectors from 'global/global-selectors';
import { AuditService, AuditActionType } from 'shared/userAudit/audit-constants';
import { AppToaster } from 'shared/toaster';
import { extractErrorMessage } from 'utils';
import { RootState } from 'data/root.reducers';
import ResponseDetails from '../ResponseDetails';
import './warrant-retry-popup.scss';

const mapState = (state: RootState) => ({
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userDBId: authenticationSelectors.getAuthenticationUserName(state),
  dfId: globalSelectors.getSelectedDeliveryFunctionId(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
});

const mapDispatch = {
  warrantReprovision: warrantActions.warrantReprovision,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

interface WarrantRetryPopupProps {
  handleConfirm: () => void,
  isOpen: boolean,
  warrant: {
    id: string,
    name: string
  },
  isCalledfromCase?: boolean,
  onReload?: () => void
  onWarrantAction?: () => void
}

export type Props = PropsFromRedux & WarrantRetryPopupProps;

const WarrantRetryPopup: FC<Props> = ({
  isOpen,
  handleConfirm,
  warrant,
  dfId,
  isAuditEnabled,
  userId,
  userDBId,
  isCalledfromCase,
  onReload,
  warrantReprovision,
  onWarrantAction,
}: Props) => {
  const [inProgress, setInProgress] = useState(true);
  const [displayMessageDetails, setDisplayMessageDetails] = useState(false);
  const [displayMessageButtonText, setDisplayMessageButtonText] = useState('View Details');
  const [isConfirmationPopupOpen, setIsConfirmationPopupOpen] = useState(false);
  const [progressMessage, setProgressMessage] = useState('Reprovisioning targets... Please wait');
  const [result, setResult] = useState([]);
  const [isProgressbarOpen, setIsProgressbarOpen] = useState(false);
  useEffect(() => {
    setIsConfirmationPopupOpen(isOpen);
  }, [isOpen]);

  const renderProgress = () => {
    return (
      <div className="display-message">
        {progressMessage}
      </div>
    );
  };

  const renderActionsButton = () => {
    if (inProgress) return null;
    return (
      <div className="actions-button-container">
        <Button className="action-button" onClick={showOrHideMessageDetails}>{displayMessageButtonText}</Button>
        <Button className="action-button" onClick={onProgressClose} intent="primary" text="OK" />
      </div>
    );
  };

  const renderConfirmationMessage = () => () => {
    if (isCalledfromCase) {
      return (
        <Fragment>
          <span>Do you want to retry provisioning of all targets in {warrant.name}?</span>
          <br />
          <span>
            <b>Note: </b>
            All the targets in the failed and unknown state from this warrant will be provisioned again
          </span>
        </Fragment>
      );
    }
    return (
      <Fragment>
        <span>Do you want to retry provisioning the failed targets again?</span>
        <br />
      </Fragment>
    );
  };

  const showOrHideMessageDetails = () => {
    if (displayMessageDetails) {
      setDisplayMessageButtonText('View Details');
    } else {
      setDisplayMessageButtonText('Hide Details');
    }
    setDisplayMessageDetails(!displayMessageDetails);

  };

  const onSubmit = () => {
    setIsConfirmationPopupOpen(false);
    setIsProgressbarOpen(true);
    const { id, name }: any = warrant;
    const auditDetails = {
      auditEnabled: isAuditEnabled,
      userId,
      userName: userDBId,
      fieldDetails: '',
      recordName: name,
      service: AuditService.WARRANTS,
      actionType: AuditActionType.WARRANT_EDIT,
    };
    const promise: any = warrantReprovision(dfId, id, auditDetails);
    promise.then((response: any) => {
      const { data } = response.value;
      setInProgress(false);
      if (data.error) {
        setProgressMessage(`Completed - ${data.error.message}`);
        setResult(data.resultDetail);
      }
    }).catch((error: any) => {
      AppToaster.show({
        icon: 'error',
        intent: 'danger',
        timeout: 0,
        message: `Error: ${extractErrorMessage(error)}`,
      });
    });
  };

  const onConfirmationClose = () => {
    setIsConfirmationPopupOpen(false);
    handleConfirm();
  };

  const onProgressClose = () => {
    setIsProgressbarOpen(false);
    setInProgress(true);
    setDisplayMessageDetails(false);
    setProgressMessage('Reprovisioning targets... Please wait');
    handleConfirm();
    if (onReload) {
      onReload();
    }
    if (onWarrantAction) {
      onWarrantAction();
    }
  };

  return (
    <div data-testid="WarrantRetryPopup">
      <ModalDialog
        width="500px"
        isOpen={isConfirmationPopupOpen}
        title="Provisioning Retry"
        customComponent={renderConfirmationMessage()}
        actionText="Retry"
        onSubmit={onSubmit}
        onClose={onConfirmationClose}
      />
      <Dialog
        onClose={onProgressClose}
        isOpen={isProgressbarOpen}
        style={{ minWidth: '550px' }}
        canEscapeKeyClose={false}
        canOutsideClickClose={false}
      >
        <div className={Classes.DIALOG_BODY}>
          {renderProgress()}
          <ProgressBar
            value={inProgress ? 1 / 4 : 1}
            animate={inProgress}
          />
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          {renderActionsButton()}
          {
            displayMessageDetails && <ResponseDetails responses={result} />
          }
        </div>
      </Dialog>
    </div>
  );
};

export default connector(WarrantRetryPopup);
