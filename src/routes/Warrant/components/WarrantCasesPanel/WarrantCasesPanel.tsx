import React, { Component } from 'react';
import { CONFIG_FORM_MODE } from 'shared/popup/config-form-popup';
import { AppToaster, showErrors } from 'shared/toaster';
import WarrantStepPanelList from 'shared/wizard/components/WarrantStepPanelList';
import CasePanel, { CasePanel as CasePanelClass } from 'xcipio/cases/forms/case-panel';
import { CaseModel } from 'data/case/case.types';
import './warrant-cases-panel.scss';
import { FormPanel } from 'shared/wizard/with-discovery-wizard-step-panel';

type Props = {
  cases: CaseModel[],
  onDeleteCase?: (caseId: string) => void;
  mode: string
  selectedCaseIndex?: number | null,
}

type State = {
  selectedCaseIndex: number,
}

export default class WarrantCasesPanel extends Component<Props, State> implements FormPanel {
  casePanelRef: React.RefObject<CasePanelClass>;

  constructor(props: Props) {
    super(props);
    this.casePanelRef = React.createRef<CasePanelClass>();

    const {
      selectedCaseIndex,
    } = this.props;

    let caseIndex = 1;
    if (selectedCaseIndex != null) {
      caseIndex = selectedCaseIndex + 1;
    }

    this.state = {
      selectedCaseIndex: caseIndex,
    };
  }

  // TODO: Use a different method
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps = (nextProps: Props) => {
    if (nextProps.selectedCaseIndex !== this.props.selectedCaseIndex) {
      this.setState({
        selectedCaseIndex: nextProps.selectedCaseIndex ? nextProps.selectedCaseIndex + 1 : 1,
      });
    }
  }

  getVisibleCases(): CaseModel[] {
    const {
      cases = [],
    } = this.props;

    const visibleCases = cases.filter(({ hideFromView }) => !hideFromView);

    return visibleCases;
  }

  casePanelClassName = (): string => {
    const visibleCases = this.getVisibleCases();

    if (visibleCases?.length > 1) {
      return 'wizardChildPanelStyle multiple-entries';
    }
    return 'wizardChildPanelStyle';
  };

  isValid(errors: string[]): boolean {
    const isCasePanelValid = this.casePanelRef.current?.isValid(errors) ?? false;

    return isCasePanelValid;
  }

  async submit(): Promise<boolean> {
    const isValid = await this.casePanelRef.current?.submit();

    return isValid || false;
  }

  handleSubStep = (index: number): void => {

    const { mode } = this.props;

    if (mode === CONFIG_FORM_MODE.View) {
      this.setState({
        selectedCaseIndex: index,
      });
      return;
    }

    AppToaster.clear();
    const errors: string[] = [];

    if (mode === CONFIG_FORM_MODE.View) {
      this.setState({
        selectedCaseIndex: index,
      });
      return;
    }

    const isCasePanelValid = this.casePanelRef.current?.isValid(errors) ?? false;

    if (isCasePanelValid) {
      if (this.casePanelRef.current?.submitSubStep) {
        this.casePanelRef?.current?.submitSubStep();
      }
      this.setState({
        selectedCaseIndex: index,
      });
    } else {
      showErrors(errors);
    }
  };

  handleDelete = (caseId: string): void => {
    const { onDeleteCase } = this.props;

    this.setState({
      selectedCaseIndex: 1,
    });

    if (onDeleteCase != null) {
      onDeleteCase(caseId);
    }
  }

  renderCasesListMenu = (): JSX.Element | undefined => {
    const { cases, mode } = this.props;
    const { selectedCaseIndex } = this.state;

    const visibleCases = this.getVisibleCases();

    if (visibleCases.length <= 1) {
      return undefined;
    }

    return (
      <WarrantStepPanelList
        dataArray={cases}
        subStep={selectedCaseIndex}
        mode={mode}
        onSubStep={this.handleSubStep}
      />
    );
  };

  render(): JSX.Element {
    const { selectedCaseIndex } = this.state;
    const { mode } = this.props;
    return (
      <div className="warrant-cases-panel" data-testid="WarrantCasesPanel">
        {this.renderCasesListMenu()}
        <div className={this.casePanelClassName()}>
          <CasePanel
            ref={this.casePanelRef}
            mode={mode}
            index={selectedCaseIndex - 1}
            onDeleteCase={this.handleDelete}
          />
        </div>
      </div>
    );
  }
}
