import React, { FunctionComponent, Fragment } from 'react';
import { Route } from 'react-router-dom';
import WarrantEdit from './containers/WarrantEdit';
import WarrantView from './containers/WarrantView';
import { WARRANT_ROUTE } from '../../constants';

interface Pros {}

const Warrant: FunctionComponent<Pros> = () => {
  return (
    <Fragment>
      <Route exact path={`${WARRANT_ROUTE}/editWizard`}>
        <WarrantEdit />
      </Route>
      <Route exact path={`${WARRANT_ROUTE}/viewWizard`}>
        <WarrantView />
      </Route>
    </Fragment>
  );
};

export default Warrant;
