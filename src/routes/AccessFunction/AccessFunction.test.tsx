import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { Provider } from 'react-redux';
import store from 'data/store';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { MemoryRouter } from 'react-router-dom';
import AccessFunction from './AccessFunction';
import AccessFunctionList from './containers/AccessFunctionList';
import AccessFunctionCreate from './containers/AccessFunctionCreate';
import AccessFunctionEdit from './containers/AccessFunctionEdit';
import AccessFunctionView from './containers/AccessFunctionView';
import AccessFunctionCopy from './containers/AccessFunctionCopy';
import { AF_ROUTE } from '../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunction />', () => {
  let wrapper: ReactWrapper;

  axiosMock.onAny().reply(200, { data: [] });

  it('should render the list for the /afs path', () => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[AF_ROUTE]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    expect(wrapper.find(AccessFunctionList)).toHaveLength(1);
  });

  it('should render the create for the /afs/create path', () => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/create`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    expect(wrapper.find(AccessFunctionCreate)).toHaveLength(1);
  });

  it('should render the edit for the /afs/edit path', () => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/edit`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    expect(wrapper.find(AccessFunctionEdit)).toHaveLength(1);
  });

  it('should render the view for the /afs/view path', () => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/view`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    expect(wrapper.find(AccessFunctionView)).toHaveLength(1);
  });

  it('should render the copy for the /afs/copy path', () => {
    wrapper = mount((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/copy`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    expect(wrapper.find(AccessFunctionCopy)).toHaveLength(1);
  });
});
