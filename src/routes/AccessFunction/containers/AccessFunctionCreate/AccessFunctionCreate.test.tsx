import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import cloneDeep from 'lodash.clonedeep';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getByLabelText,
  getByTestId,
  getAllByLabelText,
  getByText,
  queryByText,
  act,
  getAllByText,
  queryByLabelText,
} from '@testing-library/react';
import AccessFunction from 'routes/AccessFunction/AccessFunction';
import AuthActions from 'data/authentication/authentication.types';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as accessFunctionActions from 'data/accessFunction/accessFunction.types';
import { GET_PROCESS_OPTIONS_FULFILLED } from 'data/processOption/processOption.types';
import { GET_NODES_FULFILLED } from 'data/nodes/nodes.types';
import { SUPPORTED_FEATURE_5GC_VPC } from 'data/supportedFeatures/supportedFeatures.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { locations } from '__test__/locations';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { mockAccessFunctions, mockAccessFunctionsUPF, mockAccessFunctionsSMF } from '__test__/mockAccessFunctions';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { mockNodes } from '__test__/mockNodes';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import { mockDiscoveryXcipioSettings } from '__test__/mockDiscoveryXcipioSettings';
import { GET_DX_APP_SETTINGS_FULFILLED } from 'xcipio/discovery-xcipio-actions';
import AccessFunctionCreate from './AccessFunctionCreate';
import { AF_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionCreate />', () => {
  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['af_view', 'af_write', 'af_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: accessFunctionActions.GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockAccessFunctions),
    });

    store.dispatch({
      type: GET_PROCESS_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockProcessOptions),
    });

    store.dispatch({
      type: GET_NODES_FULFILLED,
      payload: buildAxiosServerResponse(mockNodes),
    });

    store.dispatch({
      type: GET_DX_APP_SETTINGS_FULFILLED,
      payload: buildAxiosServerResponse([mockDiscoveryXcipioSettings]),
    });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('AF Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      if (config.url === '/session/timezones') {
        return [200, {
          data: [{
            zones: [
              { value: 'America/New_York', label: 'US/Eastern' },
              { value: 'America/Los_Angeles', label: 'US/Pacific' },
            ],
          }],
        }];
      }

      if (config.url === '/contacts/0') {
        return [200, { data: [{ id: 'Q29udGFjdCAx', name: 'Contact 1' }, { id: 'Q29udGFjdCAy', name: 'Contact 2' }] }];
      }

      if (config.url === '/distributorConfigs/0?$filter=afType+eq+%27ERK_UPF%27&$view=listeners') {
        return [200, { data: [] }];
      }

      if (config.url === '/locations') {
        return [200, { data: locations }];
      }

      if (config.url === '/templates') {
        return [200, { data: mockTemplates }];
      }

      if (config.url === '/securityInfos/0') {
        return [200, { data: mockSecurityInfos }];
      }

      if (config.url === '/ssh-infos/0') {
        return [200, { data: mockSshSecurityInfos }];
      }

      if (config.url === '/networks/0') {
        return [200, { data: mockNetworks }];
      }

      if (config.url === '/afs/0/configuredAfs') {
        return [200, { data: [JSON.stringify(configuredAfs)] }];
      }

      if (config.url === '/xtps/0') {
        return [200, { data: mockXtps }];
      }

      if (config.url === '/xdps/0') {
        return [200, { data: mockXdps }];
      }

      if ((config.url === '/distributorConfigs/0') &&
          (config.params.$filter === 'afType eq \'ERK_SMF\' or afType eq \'ERK_UPF\'')) {
        return [200, { data: mockDistributorsConfiguration }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );
    expect(screen.getByText('New Access Function')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(1);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(1);
  });

  it('should render the list of access function when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/create`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New Access Function')).toBeVisible();
  });

  it('should show the warning when there is no template', () => {
    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getByText('New Access Function')).toBeVisible();
    expect(screen.getByText('Unable to load Access Function templates')).toBeVisible();
  });

  it('should render the list of access function when clicking in the cancel button and templates are missing', () => {
    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/create`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New Access Function')).toBeVisible();
  });

  it('should set the AF Type with the first option', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent Distributed Soft Switch');
  });

  it('should load the initial data for ERK_UPF', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });

    expect(screen.getByLabelText('Name:*')).toHaveValue('');
    expect(screen.getByLabelText('Description:')).toHaveValue('');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson UPF');
    expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

    expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
    expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
    expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');
  });

  it('should validate if at least one interface is being created for ERK_UPF', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeVisible();

    fireEvent.change(screen.getByLabelText('Name:*'), { target: { value: 'AF Name Test' } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(document.querySelectorAll('.field-error-message')).toHaveLength(0);
    expect(document.querySelectorAll('.bp3-toast.bp3-intent-danger')).toHaveLength(0);

  });

  it('should be possible to set the country, state and city', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Select a country')[0], { target: { value: 'US' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a state/province')[0], { target: { value: 'Alaska' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'Adak' } });

    expect(screen.getByDisplayValue('US')).toBeVisible();
    expect(screen.getByDisplayValue('Alaska')).toBeVisible();
    expect(screen.getByDisplayValue('Adak')).toBeVisible();
  });

  it('should reset the city selection when changing the state and then be able to select new city', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Select a country')[0], { target: { value: 'US' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a state/province')[0], { target: { value: 'Alaska' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'Adak' } });

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: 'California' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());

    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'San Francisco' } });
    expect(screen.getByDisplayValue('San Francisco')).toBeVisible();
  });

  it('should disable the city when unselecting the state', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Select a country')[0], { target: { value: 'US' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a state/province')[0], { target: { value: 'Alaska' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'Adak' } });

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should disable the state and city when unselecting the country', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Select a country')[0], { target: { value: 'US' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a state/province')[0], { target: { value: 'Alaska' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'Adak' } });

    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeDisabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should reset the country, state and city when changing the type', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Select a country')[0], { target: { value: 'US' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a state/province')[0], { target: { value: 'Alaska' } });
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());
    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'Adak' } });

    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });

    expect(screen.getByDisplayValue('Select a country')).toBeVisible();
    expect(screen.getByDisplayValue('Select a country')).toBeEnabled();
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeDisabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should show the security info options', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });

    expect(screen.getByDisplayValue('None')).toBeVisible();
    fireEvent.change(screen.getAllByDisplayValue('None')[0], { target: { value: 'VExTMTJfQ0xJRU5U' } });
    expect(screen.getByDisplayValue('TLS12_CLIENT')).toBeVisible();
  });

  it('should show the list of SS8 Distributors Configuration as an empty table', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')[0]).toBeVisible();
  });

  it('should show success message on Save of a valid form', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name 1' } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a time zone')[0], { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Access Function successfully created.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name to fail' } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a time zone')[0], { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    await waitFor(() => expect(screen.getByText('Unable to create Access Function: Request failed with status code 500. Try again later.')).toBeVisible());
  });

  it('should show error message on Save if new Access Function name is duplicated', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Name:*'), { target: { value: mockAccessFunctionsUPF[1].name } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show error message on Save if new Access Function name is duplicated', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Name:*'), { target: { value: mockAccessFunctionsSMF[1].name } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show error message on Save if new Access Function name is duplicated, even to different types', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCreate />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getByLabelText('Name:*'), { target: { value: mockAccessFunctionsUPF[1].name } });
    fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });
    fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/New_York' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  describe('ERK_SMF', () => {
    it('should filter the X3 list based in the selected nodeName', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SMF');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      expect(getByLabelText(provInterface, 'X3 CCPAG Xcipio Node:*')).toHaveDisplayValue('Select a Node');

      // Should start with the X3 distributors empty
      const contentInterface = screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces');
      await waitFor(() => expect(getByText(contentInterface, 'No Rows To Show')).toBeVisible());

      // And as soon as a node is selected, it should populate the list of ERK UPF distributors
      act(() => {
        fireEvent.change(getByLabelText(provInterface, 'X3 CCPAG Xcipio Node:*'), { target: { value: 'CCPAGNODE' } });
      });

      expect(getByLabelText(provInterface, 'X3 CCPAG Xcipio Node:*')).toHaveDisplayValue('CCPAGNODE');

      await waitFor(() => expect(queryByText(contentInterface, 'No Rows To Show')).toBeNull());
    });

    it('should select the MDF3 Node Name based in the CCPAG selected', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_SMF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SMF');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      expect(getByLabelText(provInterface, 'X3 CCPAG Xcipio Node:*')).toHaveDisplayValue('Select a Node');
      expect(getByLabelText(provInterface, 'MDF3 Node Name:*')).toHaveDisplayValue('Select a Node');

      act(() => {
        fireEvent.change(getByLabelText(provInterface, 'X3 CCPAG Xcipio Node:*'), { target: { value: 'CCPAGNODE' } });
      });

      expect(getByLabelText(provInterface, 'MDF3 Node Name:*')).toHaveDisplayValue('MDF3NODE');
    });
  });

  describe('ALU_MME', () => {
    it('should load the initial data for ALU_MME', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ALU_MME' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent MME');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):*')).toHaveValue('');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('0');

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'AF Port:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(dataInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(dataInterface, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(dataInterface, 'TPKT')).not.toBeChecked();
    });
  });

  describe('ALU_DSS', () => {
    it('should load the initial data for ALU_DSS', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ALU_DSS' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent Distributed Soft Switch');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveDisplayValue('0104');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('SGS-IWF');
      expect(screen.getByLabelText('Serial Number:*')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):')).toHaveValue('');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(provInterface, 'Timeout Interval (secs):*')).toHaveValue('10');

      let dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      fireEvent.click(getByTestId(dataInterface, 'FormSectionAddButton'));

      dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toHaveValue('11550');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'MSC ID:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'Configured State:')).toHaveDisplayValue('Active');
    });
  });

  describe('MV_SMSIWF', () => {
    it('should load the initial data for MV_SMSIWF', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MV_SMSIWF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir SMS IWF');

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Software Version:')).toBeDisabled();
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toBeDisabled();
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('32');

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      fireEvent.click(getByTestId(dataInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(dataInterface, 'DSCP (0-63):*')).toHaveValue('32');
    });
  });

  describe('ERK_PGW', () => {
    it('should load the initial data for ERK_PGW', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SAEGW/PGW');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('without Traffic Manager');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();
      // TODO: fix error on suggestText box
      // expect(screen.getByLabelText('Software Version:')).toHaveDisplayValue([]);
      expect(screen.getByLabelText('Model Number:')).toHaveDisplayValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const contentInterface = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(contentInterface, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(contentInterface, 'TLS Profile (Security Info ID):')).toHaveValue('Tk9ORQ');
      expect(getByLabelText(contentInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      fireEvent.click(getByTestId(contentInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(contentInterface, 'Transport:')).toHaveValue('UDP');

      fireEvent.change(screen.getByLabelText('Configure:'), { target: { value: 'trafficManager' } });
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterfaceWithTM, 'XTP ID:*')).toHaveValue('weq');
      expect(getByLabelText(contentInterfaceWithTM, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(contentInterfaceWithTM, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(contentInterfaceWithTM, 'TLS Profile (Security Info ID):')).toHaveValue('');
      expect(getByLabelText(contentInterfaceWithTM, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(contentInterfaceWithTM, 'CollapsibleFormButton'));
      expect(getByLabelText(contentInterfaceWithTM, 'Delivery Type:')).toHaveValue('ERKPGW_UDP');
      expect(getByLabelText(contentInterfaceWithTM, 'Trace Level:')).toHaveValue('0');
    });
  });

  describe('MVNR_PGW', () => {
    it('should load the initial data for MVNR_PGW', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('without Traffic Manager');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      let provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'FormSectionAddButton'));
      provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11:0');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(provInterface, 'Keepalive Interval:*')).toHaveValue('60');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('HTTP');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol:')).toHaveValue('MVRN_UAG_PGW');
      expect(getByLabelText(provInterface, 'AF Protocol:')).toBeDisabled();

      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveValue('1.0');
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'LI Target Namespace:')).toHaveValue('http://mavenir.net/li/');

      let dataInterface = screen.getByTestId('FormSection-dataInterfaces');
      fireEvent.click(getByTestId(dataInterface, 'FormSectionAddButton'));
      dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(dataInterface, 'AF Port:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11:21990');
      expect(getByLabelText(dataInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      fireEvent.click(getByTestId(dataInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(dataInterface, 'DSCP (0-63):*')).toHaveValue('0');
      expect(getByLabelText(dataInterface, 'IRI Payload Encoding:')).toHaveValue('NONE');
      expect(getByLabelText(dataInterface, 'Transport:')).toHaveValue('TCP');
      expect(getByLabelText(dataInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'AF Protocol:')).toHaveValue('MVRN_UAG_PGW');
      expect(getByLabelText(dataInterface, 'AF Protocol:')).toBeDisabled();

      expect(getByLabelText(dataInterface, 'AF Protocol Version:')).toHaveValue('1.0');
      expect(getByLabelText(dataInterface, 'AF Protocol Version:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'Maximum Concurrent Connections:')).toHaveValue('2');
      expect(getByLabelText(dataInterface, 'Maximum Concurrent Connections:')).toBeDisabled();

      let contentInterface = screen.getByTestId('FormSection-contentInterfaces');
      fireEvent.click(getByTestId(contentInterface, 'FormSectionAddButton'));
      contentInterface = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(contentInterface, 'AF Port:*')).toHaveValue('0');
      expect(getByLabelText(contentInterface, 'Network ID:')).toHaveDisplayValue('Select a Network ID');
      expect(getByLabelText(contentInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(contentInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(contentInterface, 'Transport:')).toHaveValue('UDP');
      expect(getByLabelText(contentInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(contentInterface, 'AF Protocol:')).toHaveValue('MVRN_UAG_PGW');
      expect(getByLabelText(contentInterface, 'AF Protocol:')).toBeDisabled();
      expect(getByLabelText(contentInterface, 'AF Protocol Version:')).toHaveValue('1.0');
      expect(getByLabelText(contentInterface, 'AF Protocol Version:')).toBeDisabled();

      fireEvent.change(screen.getByLabelText('Configure:'), { target: { value: 'trafficManager' } });
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterfaceWithTM, 'XTP ID:')).toHaveDisplayValue(['1']);
      expect(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:')).toHaveDisplayValue('4');
      expect(getByLabelText(contentInterfaceWithTM, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveDisplayValue('0');

      expect(getByLabelText(contentInterfaceWithTM, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11:130');
      expect(getByLabelText(contentInterfaceWithTM, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(contentInterfaceWithTM, 'CollapsibleFormButton'));

      expect(getByLabelText(contentInterfaceWithTM, 'DSCP (0-63):*')).toHaveValue('32');
      expect(getByLabelText(contentInterfaceWithTM, 'Delivery Type:')).toHaveValue('MVNRPGW_TCP');
      expect(getByLabelText(contentInterfaceWithTM, 'Delivery Type:')).toBeDisabled();
      expect(getByLabelText(contentInterfaceWithTM, 'Trace Level:')).toHaveDisplayValue('None');
    });

    it('should set the sec info as required and transport as HTTP when uri is set to https:', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');

      let provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'FormSectionAddButton'));
      provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      fireEvent.change(getByLabelText(provInterface, 'AF URL:*'), { target: { value: 'https://domain.com' } });
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('None');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');
    });

    it('should set the sec info as required and transport as HTTP when uri is set to http:', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');

      let provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'FormSectionAddButton'));
      provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      fireEvent.change(getByLabelText(provInterface, 'AF URL:*'), { target: { value: 'http://domain.com' } });
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');
    });

    it('should set the AF Port to 0 and disabled the field if max connection greater than 4', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');
      fireEvent.change(screen.getByLabelText('Configure:'), { target: { value: 'trafficManager' } });
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:')).toHaveDisplayValue('4');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('0');
      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'AF Port:'), { target: { value: '1,2,3' } });
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('1,2,3');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toBeEnabled();

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:'), { target: { value: '5' } });
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toBeDisabled();
    });

    it('should validate the list of ports for traffic manager', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');
      fireEvent.change(screen.getByLabelText('Configure:'), { target: { value: 'trafficManager' } });
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');
      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'AF IP Address:*'), { target: { value: '1.1.1.1' } });

      expect(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:')).toHaveDisplayValue('4');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('0');
      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(contentInterfaceWithTM.querySelectorAll('.field-error-message')).toHaveLength(0);

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'AF Port:'), { target: { value: '1,2,3,4' } });
      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(contentInterfaceWithTM.querySelectorAll('.field-error-message')).toHaveLength(0);

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'AF Port:'), { target: { value: '1,2' } });
      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(contentInterfaceWithTM.querySelectorAll('.field-error-message')).toHaveLength(1);
      expect(getByText(contentInterfaceWithTM, 'Enter 0 or 4 unique port numbers separated by comma.')).toBeVisible();

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:'), { target: { value: '1' } });
      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(getByText(contentInterfaceWithTM, 'Enter 0 or 1 unique port number.')).toBeVisible();

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'AF Port:'), { target: { value: '1000' } });
      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(contentInterfaceWithTM.querySelectorAll('.field-error-message')).toHaveLength(0);
    });

    it('should validate if at least one interface is being created', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'MVNR_PGW' } });

      expect(screen.getByText('For proper configuration of this Access Function, add at least 1 interface from the options below.')).toBeVisible();

      fireEvent.change(screen.getByLabelText('Name:*'), { target: { value: 'AF Name Test' } });
      fireEvent.change(screen.getByLabelText('Time Zone:*'), { target: { value: 'America/New_York' } });

      fireEvent.click(screen.getAllByText('Save')[0]);

      // show toast error message
      expect(screen.getByText('Unable to create Access Function')).toBeVisible();
      expect(screen.getByText('Add at least 1 interface to the AF before saving.')).toBeVisible();
      expect(document.querySelectorAll('.bp3-toast.bp3-intent-danger')).toHaveLength(1);

      let provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'FormSectionAddButton'));
      provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      fireEvent.change(getByLabelText(provInterface, 'AF URL:*'), { target: { value: 'https://28.28.28.28/mvr' } });
      fireEvent.change(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*'), { target: { value: 'SFRUUFNfU2VydmVyX1NpbQ' } });

      fireEvent.click(screen.getAllByText('Save')[0]);
      expect(document.querySelectorAll('.field-error-message')).toHaveLength(0);
    });

  });

  describe('Cisco Virtual PCRF', () => {
    it('should load the initial data for Cisco Virtual PCRF', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'CISCOVPCRF' } });
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('6443');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('HTTPS_Server_Sim');
      expect(getByLabelText(provInterface, 'Call Data Network ID:*')).toHaveValue('WENQcmNz');
      expect(getByLabelText(provInterface, 'Call Content Destination IP Address:*')).toHaveValue('::');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'XCP IP Address:*')).toHaveValue('0.0.0.0');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):')).toHaveValue('0');
      expect(getByLabelText(provInterface, 'Message Update Interval:')).toHaveValue('30');
      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('0');

      const iriInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(iriInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriInterface, 'AF Port:*')).toHaveValue('0');
      expect(getByLabelText(iriInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('HTTPS_Server_Sim');
      expect(getByLabelText(iriInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(iriInterface, 'XCP IP Address:*')).toHaveValue('0.0.0.0');
      expect(getByLabelText(iriInterface, 'XCP Port (TCP):')).toHaveValue('51500');
      expect(getByLabelText(iriInterface, 'Message Update Interval:')).toHaveValue('30');
      expect(getByLabelText(iriInterface, 'DSCP (0-63):*')).toHaveValue('0');
    });
  });

  describe('ALU_SGW', () => {
    it('should load the initial data for ALU_SGW', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ALU_SGW' } });

      waitFor(() => expect(screen.getByLabelText('AF Type:*')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent Serving Gateway');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('without Traffic Manager');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('22');
      expect(getByLabelText(provInterface, 'Username:*')).toHaveDisplayValue('');
      expect(getByLabelText(provInterface, 'Password:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'DSCP (0-63):')).toHaveValue('0');
      expect(getByLabelText(provInterface, 'Delivery Function ID:')).toHaveValue('1');

      const iriInterface = screen.getByTestId('FormSection-dataInterfaces');
      expect(getByLabelText(iriInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriInterface, 'AF Port:*')).toHaveValue('0');
      expect(getByLabelText(iriInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11:0');
      expect(getByLabelText(iriInterface, 'Configured State:')).toHaveDisplayValue('Active');

      expect(getByLabelText(iriInterface, 'DSCP (0-63):')).toHaveDisplayValue('0');
      expect(getByLabelText(iriInterface, 'TPKT')).not.toBeChecked();

      const contentInterface = screen.getByTestId('FormSection-contentInterfaces');
      expect(getByLabelText(contentInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(contentInterface, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(contentInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(contentInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(contentInterface, 'MultiTabAddButton'));

      fireEvent.click(getByText(contentInterface, 'Interface 2'));

      fireEvent.change(getAllByLabelText(contentInterface, 'Network ID:*')[1], { target: { value: 'WENQdjY' } });

      expect(getByText(contentInterface, 'Network ID must be the same across all Content Interfaces (X3).')).toBeVisible();
      expect(contentInterface.querySelectorAll('.field-error-message')).toHaveLength(1);

      fireEvent.change(getAllByLabelText(contentInterface, 'Network ID:*')[0], { target: { value: 'WENQdjY' } });
    });

    it('should show validation error in Content Interface if all interfaces do not have the same Network ID', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ALU_SGW' } });

      waitFor(() => expect(screen.getByLabelText('AF Type:*')).toBeVisible());

      const contentInterface = screen.getByTestId('FormSection-contentInterfaces');

      const networkValue1 = 'WENQdjY';
      const networkValue2 = 'WENQcmNz';

      // Interface 1
      fireEvent.change(getAllByLabelText(contentInterface, 'Network ID:*')[0], { target: { value: networkValue1 } });
      expect(contentInterface.querySelectorAll('.field-error-message')).toHaveLength(0);

      fireEvent.click(getByTestId(contentInterface, 'MultiTabAddButton'));

      fireEvent.click(getByText(contentInterface, 'Interface 2'));

      // Interface 2
      fireEvent.change(getAllByLabelText(contentInterface, 'Network ID:*')[1], { target: { value: networkValue2 } });

      expect(getByText(contentInterface, 'Network ID must be the same across all Content Interfaces (X3).')).toBeVisible();
      expect(contentInterface.querySelectorAll('.field-error-message')).toHaveLength(1);

      // Interface 2 again
      fireEvent.change(getAllByLabelText(contentInterface, 'Network ID:*')[1], { target: { value: networkValue1 } });
      expect(contentInterface.querySelectorAll('.field-error-message')).toHaveLength(0);

    });
  });

  describe('NKSR', () => {
    it('should load the initial data for NKSR', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'NKSR' } });

      waitFor(() => expect(screen.getByLabelText('AF Type:*')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Nokia SR 7750');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      // expect(screen.getByLabelText('Software Version:')).toHaveDisplayValue('R15');
      expect(screen.getByDisplayValue('NOKIASR7750')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'Service ID:*')).toHaveValue('');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByLabelText(provInterface, 'SSH Security Info ID:*')).toHaveDisplayValue('SSHInfo1');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveDisplayValue('R15');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('SSH');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();

      const contentInterface = screen.getByTestId('FormSection-contentInterfaces');
      expect(getByLabelText(contentInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(contentInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(contentInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(contentInterface, 'AF Protocol Version:')).toHaveDisplayValue('R15');
      expect(getByLabelText(contentInterface, 'Transport:')).toHaveValue('UDP');
      expect(getByLabelText(contentInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(contentInterface, 'AF Protocol:')).toHaveValue('NOKIASR');
      expect(getByLabelText(contentInterface, 'AF Protocol:')).toBeDisabled();
    });
  });

  describe('ETSI103221', () => {
    it('should load the initial data for ETSI103221', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ETSI103221' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('ETSI 103 221');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      // expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      let provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      fireEvent.click(getByTestId(provInterface, 'FormSectionAddButton'));

      provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'DSCP (0-63):')).toHaveDisplayValue('0');
      expect(getByLabelText(provInterface, 'Keepalive Interval (Sec):')).toHaveValue('60');
      expect(getByLabelText(provInterface, 'LI Target Namespace:')).toHaveValue('http://uri.etsi.org/03221/X1/2017/10');
      expect(getByLabelText(provInterface, 'NE Identifier:')).toHaveValue('');

      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('HTTP');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol:')).toHaveValue('ETSI103221');
      expect(getByLabelText(provInterface, 'AF Protocol:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveValue('1.3.1');
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toBeDisabled();

      const dataSectionInterface = screen.getByTestId('SelectionPanel-dataInterfaces');

      expect(getByLabelText(dataSectionInterface, 'SS8 Distributor')).toBeChecked();
      expect(getByLabelText(dataSectionInterface, 'Interface')).not.toBeChecked();

      const dataInterface = screen.getByTestId('FormSectionDistributorConfigs-dataInterfaces');
      expect(getByText(dataInterface, /All AFs of this type are auto-associated with all SS8 Distributors of the same type/)).toBeVisible();

      const contentSectionInterface = screen.getByTestId('SelectionPanel-contentInterfaces');

      expect(getByLabelText(contentSectionInterface, 'SS8 Distributor')).toBeChecked();
      expect(getByLabelText(contentSectionInterface, 'Interface')).not.toBeChecked();

      const contentInterface = screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces');
      expect(getByText(contentInterface, /All AFs of this type are auto-associated with all SS8 Distributors of the same type/)).toBeVisible();
    });

    it('should show the validation error in the interface when saving', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ETSI103221' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('ETSI 103 221');

      let dataInterface = screen.getByTestId('SelectionPanel-dataInterfaces');

      expect(getByLabelText(dataInterface, 'SS8 Distributor')).toBeChecked();
      expect(getByLabelText(dataInterface, 'Interface')).not.toBeChecked();

      act(() => {
        fireEvent.click(getByLabelText(dataInterface, 'Interface'));
      });

      dataInterface = screen.getByTestId('SelectionPanel-dataInterfaces');
      expect(getByLabelText(dataInterface, 'SS8 Distributor')).not.toBeChecked();
      expect(getByLabelText(dataInterface, 'Interface')).toBeChecked();

      [dataInterface] = screen.getAllByTestId('FormSection-dataInterfaces');

      expect(getByText(dataInterface, 'Interface 1')).toBeVisible();

      expect(getByLabelText(dataInterface, 'Delivery Type:')).toHaveDisplayValue('IP/Port');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');

      fireEvent.change(getByLabelText(dataInterface, 'Delivery Type:'), { target: { value: 'HTTP' } });
      expect(getByLabelText(dataInterface, 'Delivery Type:')).toHaveDisplayValue('URI');
      expect(getByLabelText(dataInterface, 'Delivery URI:*')).toHaveValue('');

      fireEvent.click(screen.getAllByText('Save')[0]);

      expect(getAllByText(dataInterface, 'Please enter a value.')).toHaveLength(1);
    });
  });

  describe('ERK_5GAMF', () => {
    it('should load the initial data for ERK_5GAMF', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_5GAMF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson AMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      // expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('SIMPLESERVER1');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toBeDisabled();
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'MDF2 Node Name:*')).toHaveDisplayValue('Select a Node');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'Keepalive Interval (Sec):')).toHaveValue('60');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('HTTP');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol:')).toHaveValue('ERK_5GAMF');
      expect(getByLabelText(provInterface, 'AF Protocol:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveValue('1.7.1');
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toBeDisabled();

      const dataInterface = screen.getByTestId('FormSectionDistributorConfigs-dataInterfaces');
      expect(getByText(dataInterface, /All Points of Interception of this type are auto-associated with all SS8 Distributors of the same type/)).toBeVisible();
    });

    it('should load the initial data for ERK_5GAMF when the 5gcVcpEnabled is not enabled', async () => {
      const clonedMockSupportedFeatures = cloneDeep(mockSupportedFeatures);
      let { features } = clonedMockSupportedFeatures.features.nodes[0];
      features = features.filter((feature) => feature !== SUPPORTED_FEATURE_5GC_VPC);
      clonedMockSupportedFeatures.features.nodes[0].features = features;

      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([clonedMockSupportedFeatures]),
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_5GAMF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson AMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      // expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Select a contact');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Select a state');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Select a city');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('SIMPLESERVER1');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toBeDisabled();
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(queryByLabelText(provInterface, 'MDF2 Node Name:*')).toBeNull();

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'Keepalive Interval (Sec):')).toHaveValue('60');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('HTTP');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol:')).toHaveValue('ERK_5GAMF');
      expect(getByLabelText(provInterface, 'AF Protocol:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveValue('1.7.1');
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toBeDisabled();

      const dataInterface = screen.getByTestId('FormSectionDistributorConfigs-dataInterfaces');
      expect(getByText(dataInterface, /All Points of Interception of this type are auto-associated with all SS8 Distributors of the same type/)).toBeVisible();
    });
  });

  describe('when endeavor deployment', () => {
    beforeEach(() => {
      const settings = cloneDeep(mockDiscoveryXcipioSettings);
      const deploymentSetting = settings.appSettings.find(({ name }) => name === 'deployment');
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      deploymentSetting!.setting = 'endeavor';

      store.dispatch({
        type: GET_DX_APP_SETTINGS_FULFILLED,
        payload: buildAxiosServerResponse([settings]),
      });
    });

    it('should change the template labels for the deployment', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCreate />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('');
      expect(screen.getByLabelText('Description:')).toHaveValue('');
      fireEvent.change(screen.getByLabelText('AF Type:*'), { target: { value: 'ERK_5GAMF' } });
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson AMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('Select a country');
      expect(screen.getByLabelText('County/Region:')).toHaveDisplayValue('Select a County/Region');
      expect(screen.getByLabelText('Town/City:')).toHaveDisplayValue('Select a Town/City');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('Select a time zone');
    });
  });
});
