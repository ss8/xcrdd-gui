import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getByTestId,
  getByLabelText,
} from '@testing-library/react';
import AccessFunction from 'routes/AccessFunction/AccessFunction';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse, buildServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { locations } from '__test__/locations';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import { mockContacts } from '__test__/mockContacts';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import * as types from 'data/accessFunction/accessFunction.types';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { GET_PROCESS_OPTIONS_FULFILLED } from 'data/processOption/processOption.types';
import { UserSettings } from 'data/types';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import AccessFunctionView from './AccessFunctionView';
import { AF_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionView />', () => {
  let accessFunction: types.AccessFunction;
  let accessFunctionSMF: types.AccessFunction;
  let accessFunctionMVNRPGW: types.AccessFunction;
  let accessFunctionSMFInvalidNetworkId: types.AccessFunction;
  let userSettings: UserSettings;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['af_view', 'af_write', 'af_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: GET_PROCESS_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockProcessOptions),
    });

    accessFunction = {
      id: 'ID_TO_GET',
      name: 'AF Name to View',
      description: 'some description',
      contact: { id: 'Q29udGFjdCAy', name: 'Contact 2' },
      insideNat: false,
      timeZone: 'America/Los_Angeles',
      type: 'ERK_UPF',
      tag: 'ERK_UPF',
      country: 'US',
      stateName: 'Alaska',
      city: 'Adak',
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    };

    accessFunctionSMF = {
      ...accessFunction,
      id: 'ID_TO_GET_SMF',
      type: 'ERK_SMF',
      tag: 'ERK_SMF',
      provisioningInterfaces: [{
        id: 'eEFGXzU4NjMwMjY2NiMx',
        name: 'eEFGXzU4NjMwMjY2NiMx',
        options: [
          { key: 'afid', value: 'xAF_586302666' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
          { key: 'ownnetid', value: 'WENQcmNz' },
          { key: 'ifid', value: '1' },
          { key: 'destPort', value: '123' },
          { key: 'dscp', value: '32' },
          { key: 'destIPAddr', value: '1.1.1.1' },
        ],
      }],
    };

    accessFunctionMVNRPGW = {
      ...accessFunction,
      type: 'MVNR_PGW',
      tag: 'MVNR_PGW',
      contentDeliveryVia: 'directFromAF',
      provisioningInterfaces: [{
        id: '1',
        name: 'name1',
        options: [
          { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
          { key: 'ownnetid', value: 'U1M4XzMwNzA' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'dscp', value: '63' },
          { key: 'litype', value: 'INI1' },
          { key: 'keepaliveinterval', value: '60' },
          { key: 'litargetnamespace', value: 'http://mavenir.net/li/' },
          { key: 'secinfoid', value: 'Tk9ORQ' },
          { key: 'transport', value: 'HTTP' },
          { key: 'uri', value: 'http://domain.com' },
          { key: 'version', value: '1.0' },
        ],
      }],
    };

    accessFunctionSMFInvalidNetworkId = {
      ...accessFunction,
      id: 'ID_TO_GET_SMF_INVALID',
      type: 'ERK_SMF',
      tag: 'ERK_SMF',
      provisioningInterfaces: [{
        id: 'eEFGXzU4NjMwMjY2NiMx',
        name: 'eEFGXzU4NjMwMjY2NiMx',
        options: [
          { key: 'afid', value: 'xAF_586302666' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
          { key: 'ownnetid', value: 'XXXXXXXX' },
          { key: 'ifid', value: '1' },
          { key: 'destPort', value: '123' },
          { key: 'dscp', value: '32' },
          { key: 'destIPAddr', value: '1.1.1.1' },
        ],
      }],
    };

    userSettings = {
      dateUpdated: '2020-07-28T17:08:46.000+0000',
      id: 'id1',
      name: 'AccessFunctionSettings',
      setting: '[]',
      type: 'json',
      user: 'userId1',
    };

    const response = buildServerResponse([accessFunction]);
    axiosMock.onGet('/afs/0?$view=options').reply(200, response);

    const userSettingsResponse = buildServerResponse([userSettings]);
    axiosMock.onGet('/users/userId1/settings?name=AccessFunctionSettings').reply(200, userSettingsResponse);

    const accessFunctionResponse = buildServerResponse([accessFunction]);
    axiosMock.onGet('/afs/0/ID_TO_GET').reply(200, accessFunctionResponse);
    axiosMock.onGet('/afs/0/ID_TO_GET_SMF').reply(200, { data: [accessFunctionSMF] });
    axiosMock.onGet('/afs/0/ID_TO_GET_MVNR_PGW').reply(200, { data: [accessFunctionMVNRPGW] });
    axiosMock.onGet('/afs/0/ID_TO_GET_SMF_INVALID').reply(200, { data: [accessFunctionSMFInvalidNetworkId] });

    axiosMock.onGet('/session/timezones').reply(200, { data: [{ zones: mockTimeZones }] });
    axiosMock.onGet('/contacts/0').reply(200, { data: mockContacts });
    axiosMock.onGet('/networks/0').reply(200, { data: mockNetworks });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: mockSecurityInfos });
    axiosMock.onGet('/ssh-infos/0').reply(200, { data: mockSshSecurityInfos });
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet('/distributorConfigs/0').reply(200, { data: [] });
    axiosMock.onGet('/vpn-configs/0').reply(200, { data: [] });
    axiosMock.onGet('/vpn-templates/0').reply(200, { data: [] });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/locations').reply(200, { data: locations });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet('/afs/0/configuredAfs').reply(200, { data: [JSON.stringify(configuredAfs)] });
    axiosMock.onGet('/xtps/0').reply(200, { data: mockXtps });
    axiosMock.onGet('/xdps/0').reply(200, { data: mockXdps });
    axiosMock.onGet('/process-options/0').reply(200, { data: mockProcessOptions });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: mockProcessOptions });
  });

  it('should render the form title with Edit and Back buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionView />
        </MemoryRouter>
      </Provider>,
    );
    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());
    let items = screen.getAllByText('Edit');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Back');
    expect(items).toHaveLength(2);
  });

  it('should render the list of access function when clicking in the Back button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/View`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );
    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());
    fireEvent.click(screen.getAllByText('Back')[0]);
    expect(screen.getByText('New Access Function')).toBeVisible();
  });

  it('should render the access function to edit when clicking in the Edit button', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/View`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Edit')[0]);
    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
  });

  it('should load the data retrieved from the server for ERK_UPF', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());
    expect(screen.getByDisplayValue('AF Name to View')).toBeVisible();
    expect(screen.getByDisplayValue('AF Name to View')).toBeDisabled();
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeVisible();
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeDisabled();
    expect(screen.getByText('US/Pacific', { exact: false })).toBeVisible();
    expect(screen.getByText('US/Pacific', { exact: false })).toBeDisabled();
    expect(screen.getByDisplayValue('Contact 2')).toBeVisible();
    expect(screen.getByDisplayValue('Contact 2')).toBeDisabled();
    expect(screen.getByDisplayValue('US')).toBeVisible();
    expect(screen.getByDisplayValue('US')).toBeDisabled();
    expect(screen.getByDisplayValue('Alaska')).toBeVisible();
    expect(screen.getByDisplayValue('Alaska')).toBeDisabled();
    expect(screen.getByDisplayValue('Adak')).toBeVisible();
    expect(screen.getByDisplayValue('Adak')).toBeDisabled();
  });

  it('should show an error on load if the network id is not valid', async () => {
    store.dispatch({
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [{ id: 'ID_TO_GET_SMF_INVALID' }],
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());

    expect(screen.getByLabelText('Network ID:*')).toHaveDisplayValue('Network ID not found');
    expect(screen.getByText('Please select another network ID.')).toBeVisible();
  });

  describe('ERK_SMF', () => {
    it('should load the data retrieved from the server for ERK_SMF', async () => {
      store.dispatch({
        type: types.SELECT_ACCESS_FUNCTION,
        payload: [{ id: 'ID_TO_GET_SMF' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionView />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to View');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('');
      expect(screen.getByLabelText('Software Version:')).toHaveDisplayValue('1.4');

      expect(screen.getByLabelText('AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(screen.getByLabelText('AF Port:*')).toHaveValue('123');
      expect(screen.getByLabelText('Configured State:')).toHaveDisplayValue('Active');
      expect(screen.getByTestId('FieldCurrentState').textContent).toBe('Active');

      expect(screen.getByLabelText('Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11:0');
      expect(screen.getByLabelText('TLS Profile (Security Info ID):')).toHaveDisplayValue('TLS12_CLIENT');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0 - 63):*')).toHaveValue('32');

      expect(screen.getByTestId('FormSectionDistributorConfigs-dataInterfaces')).toBeVisible();
      expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible();
    });
  });

  it('should show the timezone even if it is not available in the list', async () => {
    accessFunction.timeZone = 'America/Tijuana';

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());

    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/America\/Tijuana/);
  });

  it('should show the list of SS8 Distributors Configuration as an empty table', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionView />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());

    expect(screen.getAllByText('SS8 Distributors Configuration')).toHaveLength(2);
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  describe('MVNR_PGW', () => {
    it('should set the sec info as required and transport as HTTP when uri is set to http:', async () => {
      store.dispatch({
        type: types.SELECT_ACCESS_FUNCTION,
        payload: [{ id: 'ID_TO_GET_MVNR_PGW' }],
      });

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionView />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveDisplayValue('http://domain.com');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');
    });
  });
});
