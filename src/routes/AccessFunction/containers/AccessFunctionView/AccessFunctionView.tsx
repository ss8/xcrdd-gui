import React, {
  FunctionComponent,
  useState,
  useEffect,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { RootState } from 'data/root.reducers';
import { FormTemplate } from 'data/template/template.types';
import * as actions from 'data/accessFunction/accessFunction.actions';
import { AccessFunctionFormData } from 'data/accessFunction/accessFunction.types';
import { getSelectedDeliveryFunctionId } from 'global/global-selectors';
import * as accessFunctionSelectors from 'data/accessFunction/accessFunction.selectors';
import * as userSelectors from 'users/users-selectors';
import * as userActions from 'users/users-actions';
import * as contactSelectors from 'xcipio/contacts/contacts-selectors';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as networkSelectors from 'data/network/network.selectors';
import * as networkActions from 'data/network/network.actions';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as sshSecurityInfoSelectors from 'data/sshSecurityInfo/sshSecurityInfo.selectors';
import * as sshSecurityInfoActions from 'data/sshSecurityInfo/sshSecurityInfo.actions';
import * as locationSelectors from 'data/location/location.selectors';
import * as locationActions from 'data/location/location.actions';
import * as distributorConfigSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorConfigAdapters from 'data/distributorConfiguration/distributorConfiguration.adapter';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as templateSelectors from 'data/template/template.selectors';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { adaptToForm } from 'data/accessFunction/accessFunction.adapter';
import AccessFunctionForm from 'routes/AccessFunction/components/AccessFunctionForm';
import FieldCurrentState from 'components/FieldCurrentState';
import * as xtpActions from 'data/xtp/xtp.actions';
import * as xtpSelectors from 'data/xtp/xtp.selectors';
import * as xdpActions from 'data/xdp/xdp.actions';
import * as xdpSelectors from 'data/xdp/xdp.selectors';
import * as processOptionSelectors from 'data/processOption/processOption.selectors';
import {
  adaptTemplateOptions,
  adaptTemplateVisibilityMode,
  adaptLocationField,
  adaptTemplateInterfaceFields,
  adaptTemplate5GFields,
  adaptTimezoneField,
  adaptTemplateLabelsForDeployment,
} from '../../utils/accessFunctionTemplate.adapter';
import { useGetDistributorConfigurationsByFilter } from '../../hooks/useGetDistributorConfigurationsByFilter';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: getSelectedDeliveryFunctionId(state),
  selectedAccessFunction: accessFunctionSelectors.getSelectedAccessFunction(state),
  accessFunction: accessFunctionSelectors.getAccessFunction(state),
  filteredDistributorConfigurations: distributorConfigSelectors.getFilteredDistributorConfigurations(state),
  timezones: userSelectors.getTimezones(state),
  contacts: contactSelectors.getContacts(state),
  networks: networkSelectors.getNetworks(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  sshSecurityInfos: sshSecurityInfoSelectors.getSshSecurityInfos(state),
  enabledAccessFunctions: supportedFeaturesSelectors.getEnabledAccessFunction(state),
  countries: locationSelectors.getCountryList(state),
  accessFunctionTemplates: templateSelectors.getEnabledAccessFunctionFormTemplatesByKey(state),
  xtps: xtpSelectors.getXtps(state),
  xdps: xdpSelectors.getXdps(state),
  processOptions: processOptionSelectors.getProcessOptions(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  is5gcVcpEnabled: supportedFeaturesSelectors.is5gcVcpEnabled(state),
  deployment: discoverySelectors.getDiscoveryDeployment(state),
});

const mapDispatch = {
  getAccessFunctionWithViewOptions: actions.getAccessFunctionWithViewOptions,
  getDistributorConfigurationsByFilter: distributorConfigActions.getDistributorConfigurationsByFilter,
  goToAccessFunctionList: actions.goToAccessFunctionList,
  goToAccessFunctionEdit: actions.goToAccessFunctionEdit,
  getTimezones: userActions.getTimezones,
  getContacts: contactActions.getAllContacts,
  getNetworks: networkActions.getNetworks,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getSshSecurityInfos: sshSecurityInfoActions.getSshSecurityInfos,
  getXtps: xtpActions.getXtps,
  getXdps: xdpActions.getXdps,
  fetchLocationList: locationActions.fetchLocationList,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

export const AccessFunctionView: FunctionComponent<Props> = ({
  deliveryFunctionId,
  selectedAccessFunction,
  accessFunction,
  filteredDistributorConfigurations,
  timezones,
  contacts,
  networks,
  securityInfos,
  sshSecurityInfos,
  history,
  enabledAccessFunctions,
  countries,
  accessFunctionTemplates,
  xtps,
  xdps,
  processOptions,
  xcipioNodes,
  is5gcVcpEnabled,
  deployment,
  goToAccessFunctionList,
  goToAccessFunctionEdit,
  getAccessFunctionWithViewOptions,
  getTimezones,
  getContacts,
  getNetworks,
  getSecurityInfos,
  getSshSecurityInfos,
  getDistributorConfigurationsByFilter,
  getXtps,
  getXdps,
  fetchLocationList,
}: Props) => {
  const [templates, setTemplates] = useState<{[key: string]: FormTemplate}>({});
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<AccessFunctionFormData>>();
  const [distributorConfigsTableData, setDistributorConfigsTableData] =
    useState<DistributorConfigurationTableData[]>([]);

  const accessFunctionId = selectedAccessFunction[0]?.id;

  const customFieldComponentMap = {
    accessFunctionCurrentState: FieldCurrentState,
  };

  useEffect(() => {
    if (accessFunctionId) {
      getAccessFunctionWithViewOptions(deliveryFunctionId, accessFunctionId);
    }
  }, [accessFunctionId, deliveryFunctionId, getAccessFunctionWithViewOptions]);

  useEffect(() => {
    getTimezones();
    getContacts(deliveryFunctionId);
    getNetworks(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
    getSshSecurityInfos(deliveryFunctionId);
    getXtps(deliveryFunctionId);
    getXdps(deliveryFunctionId);
    fetchLocationList();
  }, [
    deliveryFunctionId,
    getTimezones,
    getContacts,
    getNetworks,
    getSecurityInfos,
    getSshSecurityInfos,
    getXtps,
    getXdps,
    fetchLocationList,
  ]);

  useEffect(() => {
    const availableTemplates = adaptTemplateOptions(
      accessFunctionTemplates,
      countries,
      timezones,
      contacts,
      networks,
      securityInfos,
      sshSecurityInfos,
      xtps,
      xdps,
      processOptions,
      enabledAccessFunctions,
      xcipioNodes,
    );

    setTemplates(availableTemplates);
  }, [
    accessFunctionTemplates,
    countries,
    timezones,
    contacts,
    networks,
    securityInfos,
    sshSecurityInfos,
    xtps,
    xdps,
    processOptions,
    enabledAccessFunctions,
    xcipioNodes,
  ]);

  useEffect(() => {
    if (accessFunction != null && templates != null) {
      const formData = adaptToForm(accessFunction);
      let templateData = adaptTemplateVisibilityMode(templates[formData.tag], 'view');
      templateData = adaptTemplate5GFields(templateData, is5gcVcpEnabled);
      templateData = adaptTemplateLabelsForDeployment(templateData, deployment);
      templateData = adaptTimezoneField(templateData, timezones, formData);

      // As this is the view mode, it is not required to load the list of states or cities
      // as the input will be read only. So it is just inserting the option that should
      // be displayed as selected.
      if (formData.stateName) {
        templateData = adaptLocationField(templateData, [formData.stateName], 'stateName');
      }
      if (formData.city) {
        templateData = adaptLocationField(templateData, [formData.city], 'city');
      }

      templateData = adaptTemplateInterfaceFields(templateData, formData);

      setData(formData);
      setTemplate(templateData);
    }
  }, [
    accessFunction,
    templates,
    is5gcVcpEnabled,
    timezones,
    deployment,
  ]);

  useGetDistributorConfigurationsByFilter(deliveryFunctionId, template, getDistributorConfigurationsByFilter);

  useEffect(() => {
    const tableData = distributorConfigAdapters.adaptToTableData(filteredDistributorConfigurations);
    setDistributorConfigsTableData(tableData);
  }, [filteredDistributorConfigurations]);

  const handleClickEdit = () => goToAccessFunctionEdit(history);
  const handleCancel = () => goToAccessFunctionList(history);

  if (!template || !data) {
    return null;
  }

  return (
    <AccessFunctionForm
      title="View Access Function"
      isEditing={false}
      data={data}
      accessFunctionByType={{}}
      distributorConfigs={distributorConfigsTableData}
      template={template}
      customFieldComponentMap={customFieldComponentMap}
      networks={networks}
      is5gcVcpEnabled={is5gcVcpEnabled}
      shouldValidateOnRender
      onClickEdit={handleClickEdit}
      onClickCancelClose={handleCancel}
    />
  );
};

export default withRouter(connector(AccessFunctionView));
