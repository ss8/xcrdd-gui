import React, {
  FunctionComponent,
  useState,
  useEffect,
  useRef,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import cloneDeep from 'lodash.clonedeep';
import { RootState } from 'data/root.reducers';
import { FormTemplate } from 'data/template/template.types';
import * as actions from 'data/accessFunction/accessFunction.actions';
import {
  AccessFunctionFormData,
  AccessFunctionInterfaceFormData,
  AccessFunctionInterfaceFormSectionType,
  AccessFunctionFormSection,
} from 'data/accessFunction/accessFunction.types';
import { getSelectedDeliveryFunctionId } from 'global/global-selectors';
import * as accessFunctionSelectors from 'data/accessFunction/accessFunction.selectors';
import * as userSelectors from 'users/users-selectors';
import * as userActions from 'users/users-actions';
import * as contactSelectors from 'xcipio/contacts/contacts-selectors';
import * as contactActions from 'xcipio/contacts/contacts-actions';
import * as networkSelectors from 'data/network/network.selectors';
import * as networkActions from 'data/network/network.actions';
import * as vpnConfigTemplateSelectors from 'data/vpnTemplate/vpnTemplate.selectors';
import * as vpnConfigTemplateActions from 'data/vpnTemplate/vpnTemplate.actions';
import * as vpnConfigSelectors from 'data/vpnConfig/vpnConfig.selectors';
import * as vpnConfigActions from 'data/vpnConfig/vpnConfig.actions';
import * as securityInfoSelectors from 'data/securityInfo/securityInfo.selectors';
import * as securityInfoActions from 'data/securityInfo/securityInfo.actions';
import * as sshSecurityInfoSelectors from 'data/sshSecurityInfo/sshSecurityInfo.selectors';
import * as sshSecurityInfoActions from 'data/sshSecurityInfo/sshSecurityInfo.actions';
import * as distributorConfigSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import * as distributorConfigActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorConfigAdapters from 'data/distributorConfiguration/distributorConfiguration.adapter';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as locationSelectors from 'data/location/location.selectors';
import * as locationActions from 'data/location/location.actions';
import * as templateSelectors from 'data/template/template.selectors';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { adaptToForm } from 'data/accessFunction/accessFunction.adapter';
import AccessFunctionForm from 'routes/AccessFunction/components/AccessFunctionForm';
import { FieldUpdateHandlerParam } from 'shared/form';
import { ACCESS_FUNCTION_SECURITY_INFO_FILTER } from 'data/securityInfo/securityInfo.types';
import FieldCurrentState from 'components/FieldCurrentState';
import * as xtpActions from 'data/xtp/xtp.actions';
import * as xtpSelectors from 'data/xtp/xtp.selectors';
import * as xdpActions from 'data/xdp/xdp.actions';
import * as xdpSelectors from 'data/xdp/xdp.selectors';
import * as processOptionSelectors from 'data/processOption/processOption.selectors';
import {
  adaptTemplateOptions,
  adaptTemplateVisibilityMode,
  adaptLocationField,
  adaptSecurityInfoField,
  adaptTemplateForInterfacesFieldUpdate,
  adaptTemplateInterfaceFields,
  adaptTemplate5GFields,
  adaptTimezoneField,
  adaptTemplateLabelsForDeployment,
} from '../../utils/accessFunctionTemplate.adapter';
import getDistributorConfigurationDataToSubmit from '../../utils/getDistributorConfigurationDataToSubmit';
import getAccessFunctionDataToSubmit from '../../utils/getAccessFunctionDataToSubmit';
import updateDistributorConfigs from '../../utils/updateDistributorConfigs';
import { createAccessFunctionBasedOnTemplate } from '../../utils/accessFunctionTemplate.factory';
import { handleInterfacesFieldUpdate } from '../../utils/accessFunctionInterfaceTabFieldUpdate';
import { useGetDistributorConfigurationsByFilter } from '../../hooks/useGetDistributorConfigurationsByFilter';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  deliveryFunctionId: getSelectedDeliveryFunctionId(state),
  selectedAccessFunction: accessFunctionSelectors.getSelectedAccessFunction(state),
  accessFunction: accessFunctionSelectors.getAccessFunction(state),
  filteredDistributorConfigurations: distributorConfigSelectors.getFilteredDistributorConfigurations(state),
  timezones: userSelectors.getTimezones(state),
  contacts: contactSelectors.getContacts(state),
  networks: networkSelectors.getNetworks(state),
  vpnConfigs: vpnConfigSelectors.getVpnConfigs(state),
  vpnConfigTemplates: vpnConfigTemplateSelectors.getVpnConfigTemplates(state),
  securityInfos: securityInfoSelectors.getSecurityInfos(state),
  sshSecurityInfos: sshSecurityInfoSelectors.getSshSecurityInfos(state),
  enabledAccessFunctions: supportedFeaturesSelectors.getEnabledAccessFunction(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  countries: locationSelectors.getCountryList(state),
  stateNameByCountry: locationSelectors.getStateListByCountry(state),
  filteredCities: locationSelectors.getFilteredCityList(state),
  accessFunctionByType: accessFunctionSelectors.getPoiAccessFunctionByType(state),
  accessFunctionTemplates: templateSelectors.getEnabledAccessFunctionFormTemplatesByKey(state),
  xtps: xtpSelectors.getXtps(state),
  xdps: xdpSelectors.getXdps(state),
  processOptions: processOptionSelectors.getProcessOptions(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  is5gcVcpEnabled: supportedFeaturesSelectors.is5gcVcpEnabled(state),
  deployment: discoverySelectors.getDiscoveryDeployment(state),
});

const mapDispatch = {
  getAccessFunctionWithViewOptions: actions.getAccessFunctionWithViewOptions,
  getDistributorConfigurationsByFilter: distributorConfigActions.getDistributorConfigurationsByFilter,
  updateAccessFunction: actions.updateAccessFunction,
  goToAccessFunctionList: actions.goToAccessFunctionList,
  getTimezones: userActions.getTimezones,
  getContacts: contactActions.getAllContacts,
  getNetworks: networkActions.getNetworks,
  getVpnConfigs: vpnConfigActions.getVpnConfigs,
  getVpnConfigTemplates: vpnConfigTemplateActions.GetVpnConfigTemplates,
  getSecurityInfos: securityInfoActions.getSecurityInfos,
  getSshSecurityInfos: sshSecurityInfoActions.getSshSecurityInfos,
  fetchLocationList: locationActions.fetchLocationList,
  fetchLocationByFilterWithViewCities: locationActions.fetchLocationByFilterWithViewCities,
  getXtps: xtpActions.getXtps,
  getXdps: xdpActions.getXdps,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => AccessFunctionFormData
}

export const AccessFunctionEdit: FunctionComponent<Props> = ({
  deliveryFunctionId,
  selectedAccessFunction,
  accessFunction,
  filteredDistributorConfigurations,
  timezones,
  contacts,
  networks,
  securityInfos,
  sshSecurityInfos,
  vpnConfigs,
  vpnConfigTemplates,
  history,
  enabledAccessFunctions,
  isAuditEnabled,
  userId,
  userName,
  countries,
  stateNameByCountry,
  filteredCities,
  accessFunctionByType,
  accessFunctionTemplates,
  xtps,
  xdps,
  processOptions,
  xcipioNodes,
  is5gcVcpEnabled,
  deployment,
  updateAccessFunction,
  goToAccessFunctionList,
  getAccessFunctionWithViewOptions,
  getTimezones,
  getContacts,
  getNetworks,
  getSecurityInfos,
  getSshSecurityInfos,
  getVpnConfigs,
  getVpnConfigTemplates,
  getDistributorConfigurationsByFilter,
  fetchLocationList,
  fetchLocationByFilterWithViewCities,
  getXtps,
  getXdps,
}: Props) => {
  const [templates, setTemplates] = useState<{ [key: string]: FormTemplate }>({});
  const [template, setTemplate] = useState<FormTemplate>();
  const [data, setData] = useState<Partial<AccessFunctionFormData>>({});
  const [distributorConfigsTableData, setDistributorConfigsTableData] =
    useState<DistributorConfigurationTableData[]>([]);
  const accessFunctionFormRef = useRef<Ref>(null);
  const accessFunctionId = selectedAccessFunction[0]?.id;

  const customFieldComponentMap = {
    accessFunctionCurrentState: FieldCurrentState,
  };

  useEffect(() => {
    if (accessFunctionId) {
      getAccessFunctionWithViewOptions(deliveryFunctionId, accessFunctionId);
    }
  }, [accessFunctionId, deliveryFunctionId, getAccessFunctionWithViewOptions]);

  useEffect(() => {
    getTimezones();
    getContacts(deliveryFunctionId);
    getNetworks(deliveryFunctionId);
    getSecurityInfos(deliveryFunctionId);
    getSshSecurityInfos(deliveryFunctionId);
    getVpnConfigs(deliveryFunctionId);
    getVpnConfigTemplates(deliveryFunctionId);
    fetchLocationList();
    getXtps(deliveryFunctionId);
    getXdps(deliveryFunctionId);
  }, [
    deliveryFunctionId,
    getTimezones,
    getContacts,
    getNetworks,
    getSecurityInfos,
    getSshSecurityInfos,
    getVpnConfigs,
    getVpnConfigTemplates,
    fetchLocationList,
    getXtps,
    getXdps,
  ]);

  useEffect(() => {
    const availableTemplates = adaptTemplateOptions(
      accessFunctionTemplates,
      countries,
      timezones,
      contacts,
      networks,
      securityInfos,
      sshSecurityInfos,
      xtps,
      xdps,
      processOptions,
      enabledAccessFunctions,
      xcipioNodes,
    );

    setTemplates(availableTemplates);
  }, [
    accessFunctionTemplates,
    countries,
    timezones,
    contacts,
    networks,
    securityInfos,
    sshSecurityInfos,
    enabledAccessFunctions,
    xtps,
    xdps,
    processOptions,
    xcipioNodes,
  ]);

  useEffect(() => {
    if (accessFunction != null && templates != null) {
      const formData = adaptToForm(accessFunction);
      let templateData = adaptTemplateVisibilityMode(templates[formData.tag], 'edit');

      const securityInfoFilter = ACCESS_FUNCTION_SECURITY_INFO_FILTER[formData.tag] ?? {};
      templateData = adaptSecurityInfoField(templateData, securityInfos, securityInfoFilter);
      templateData = adaptTemplate5GFields(templateData, is5gcVcpEnabled);
      templateData = adaptTemplateLabelsForDeployment(templateData, deployment);
      templateData = adaptTimezoneField(templateData, timezones, formData);

      templateData = adaptTemplateInterfaceFields(templateData, formData);

      const { country, stateName } = formData;

      if (country) {
        const stateNames = stateNameByCountry[country]?.map(({ stateName: entry }) => entry) ?? [];
        templateData = adaptLocationField(templateData, stateNames, 'stateName');
      }

      if (country && stateName) {
        const { stateCode } = stateNameByCountry[country]?.find(({ stateName: entry }) => entry === stateName) ?? {};
        fetchLocationByFilterWithViewCities(`country eq '${country}' and stateCode eq '${stateCode}'`);
      }

      setData(formData);
      setTemplate(templateData);
    }
  }, [
    accessFunction,
    securityInfos,
    stateNameByCountry,
    templates,
    fetchLocationByFilterWithViewCities,
    accessFunctionByType,
    is5gcVcpEnabled,
    timezones,
    deployment,
  ]);

  useGetDistributorConfigurationsByFilter(deliveryFunctionId, template, getDistributorConfigurationsByFilter);

  useEffect(() => {
    const tableData = distributorConfigAdapters.adaptToTableData(filteredDistributorConfigurations);
    setDistributorConfigsTableData(tableData);
  }, [filteredDistributorConfigurations]);

  useEffect(() => {
    if (filteredCities.length > 0 && data?.country && data?.stateName) {
      const templateData = adaptLocationField(template, filteredCities, 'city');
      setTemplate(templateData);
    }
  }, [filteredCities]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleFieldUpdate = (
    { name: fieldName, value }: FieldUpdateHandlerParam,
    formSection: AccessFunctionFormSection,
    interfaceId?: string | number,
  ) => {
    let clonedData = cloneDeep(data);

    if (formSection === 'general' || formSection === 'systemInformation') {
      if (fieldName === 'country') {
        const stateNames = stateNameByCountry[value]?.map(({ stateName }) => stateName) ?? [];

        let templateData = adaptLocationField(template, stateNames, 'stateName');
        templateData = adaptLocationField(templateData, [], 'city');

        setData({
          ...clonedData,
          [fieldName]: value,
          stateName: '',
          city: '',
        });
        setTemplate(templateData);

      } else if (fieldName === 'stateName') {
        const { country = '' } = clonedData ?? {};
        const { stateCode } = stateNameByCountry[country]?.find(({ stateName }) => stateName === value) ?? {};

        if (country && stateCode) {
          fetchLocationByFilterWithViewCities(`country eq '${country}' and stateCode eq '${stateCode}'`);
        }

        const templateData = adaptLocationField(template, [], 'city');

        setData({
          ...clonedData,
          [fieldName]: value,
          city: '',
        });
        setTemplate(templateData);

      } else if (fieldName === 'name') {
        setData({
          ...clonedData,
          [fieldName]: value,
        });

      } else {
        setData({
          ...clonedData,
          [fieldName]: value,
        });
      }

    } else if (formSection === 'provisioningInterfaces' || formSection === 'dataInterfaces' || formSection === 'contentInterfaces') {
      clonedData = handleInterfacesFieldUpdate(
        template,
        clonedData,
        formSection,
        interfaceId as string,
        fieldName,
        value,
        xcipioNodes,
      );

      const adaptedTemplate = adaptTemplateForInterfacesFieldUpdate(
        formSection,
        clonedData,
        template,
        fieldName,
        value,
      );

      setData(clonedData);
      setTemplate(adaptedTemplate);
    }
  };

  const handleDistributorUpdate = (distributorConfigTableData: DistributorConfigurationTableData) => {
    const updatedDistributorConfigsTableData = updateDistributorConfigs(
      distributorConfigTableData,
      distributorConfigsTableData,
    );
    setDistributorConfigsTableData(updatedDistributorConfigsTableData);
  };

  const handleClickSave = () => {
    if (!accessFunctionId) {
      return;
    }

    const isValid = accessFunctionFormRef?.current?.isValid(true, true) ?? true;
    if (!isValid) {
      return;
    }

    const formData = accessFunctionFormRef?.current?.getValues();
    if (!formData) {
      return;
    }

    const accessFunctionBody = getAccessFunctionDataToSubmit(
      formData,
      accessFunction,
      isAuditEnabled,
      userId,
      userName,
      'UPDATE',
    );

    const distributorConfigurationBodies = getDistributorConfigurationDataToSubmit(
      distributorConfigsTableData,
      filteredDistributorConfigurations,
      isAuditEnabled,
      userId,
      userName,
    );

    updateAccessFunction(
      deliveryFunctionId,
      accessFunctionId,
      accessFunctionBody,
      history,
      distributorConfigurationBodies,
    );
  };

  const handleCancel = () => goToAccessFunctionList(history);

  const handleTabAdd = (formSectionName: AccessFunctionInterfaceFormSectionType) => {
    const clonedData = cloneDeep(data);

    const newData = createAccessFunctionBasedOnTemplate(template, true);
    const newTabData = (newData[formSectionName] as AccessFunctionInterfaceFormData[])[0];
    if (clonedData[formSectionName] !== undefined) {
      (clonedData[formSectionName] as AccessFunctionInterfaceFormData[]).push(newTabData);
    } else {
      clonedData[formSectionName] = [newTabData];
    }
    setData(clonedData);
  };

  const handleTabRemove = (tabId: string | number, formSectionName: AccessFunctionInterfaceFormSectionType) => {
    const clonedData = cloneDeep(data);

    clonedData[formSectionName] = clonedData[formSectionName]?.filter(({ id }) => id !== tabId);

    setData(clonedData);
  };

  if (!template || !data) {
    return null;
  }

  return (
    <AccessFunctionForm
      title="Edit Access Function"
      isEditing
      ref={accessFunctionFormRef}
      data={data}
      distributorConfigs={distributorConfigsTableData}
      template={template}
      accessFunctionByType={accessFunctionByType}
      vpnConfigs={vpnConfigs}
      vpnConfigTemplates={vpnConfigTemplates}
      networks={networks}
      is5gcVcpEnabled={is5gcVcpEnabled}
      shouldValidateOnRender
      onFieldUpdate={handleFieldUpdate}
      onDistributorUpdate={handleDistributorUpdate}
      customFieldComponentMap={customFieldComponentMap}
      onClickSave={handleClickSave}
      onClickCancelClose={handleCancel}
      onTabAdd={handleTabAdd}
      onTabRemove={handleTabRemove}
    />
  );
};

export default withRouter(connector(AccessFunctionEdit));
