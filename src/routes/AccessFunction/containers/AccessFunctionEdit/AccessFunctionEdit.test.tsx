import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getByLabelText,
  getByTestId,
} from '@testing-library/react';
import AccessFunction from 'routes/AccessFunction/AccessFunction';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { locations } from '__test__/locations';
import { mockTemplates } from '__test__/mockTemplates';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockContacts } from '__test__/mockContacts';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import * as types from 'data/accessFunction/accessFunction.types';
import { GET_NODES_FULFILLED } from 'data/nodes/nodes.types';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import { GET_PROCESS_OPTIONS_FULFILLED } from 'data/processOption/processOption.types';
import { mockAccessFunctions, mockAccessFunctionsUPF, mockAccessFunctionsSMF } from '__test__/mockAccessFunctions';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockNodes } from '__test__/mockNodes';
import AccessFunctionEdit from './AccessFunctionEdit';
import { AF_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionEdit />', () => {
  let accessFunction: types.AccessFunction;
  let accessFunctionSMFInvalidNetworkId: types.AccessFunction;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['af_view', 'af_write', 'af_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    store.dispatch({
      type: GET_PROCESS_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse(mockProcessOptions),
    });

    store.dispatch({
      type: GET_NODES_FULFILLED,
      payload: buildAxiosServerResponse(mockNodes),
    });

    accessFunction = {
      id: 'ID_TO_GET',
      name: 'AF Name to edit',
      description: 'some description',
      contact: { id: 'Q29udGFjdCAy', name: 'Contact 2' },
      insideNat: false,
      timeZone: 'America/Los_Angeles',
      type: 'ERK_UPF',
      tag: 'ERK_UPF',
      country: 'US',
      stateName: 'Alaska',
      city: 'Adak',
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    };

    accessFunctionSMFInvalidNetworkId = {
      ...accessFunction,
      id: 'ID_TO_GET_SMF_INVALID',
      type: 'ERK_SMF',
      tag: 'ERK_SMF',
      provisioningInterfaces: [{
        id: 'eEFGXzU4NjMwMjY2NiMx',
        name: 'eEFGXzU4NjMwMjY2NiMx',
        options: [
          { key: 'afid', value: 'xAF_586302666' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
          { key: 'ownnetid', value: 'XXXXXXXX' },
          { key: 'ifid', value: '1' },
          { key: 'destPort', value: '123' },
          { key: 'dscp', value: '32' },
          { key: 'destIPAddr', value: '1.1.1.1' },
        ],
      }],
    };

    store.dispatch({
      type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse([...mockAccessFunctions, accessFunction]),
    });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('AF Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      if (config.url === '/afs/0/ID_TO_GET') {
        return [200, { data: [accessFunction] }];
      }

      if (config.url === '/afs/0/ID_TO_GET_SMF_INVALID') {
        return [200, { data: [accessFunctionSMFInvalidNetworkId] }];
      }

      if (config.url === '/session/timezones') {
        return [200, { data: [{ zones: mockTimeZones }] }];
      }

      if (config.url === '/contacts/0') {
        return [200, { data: mockContacts }];
      }

      if (config.url === '/distributorConfigs/0') {
        return [200, { data: [] }];
      }

      if (config.url === '/config/supported-features/0') {
        return [200, { data: [mockSupportedFeatures] }];
      }

      if (config.url === '/locations') {
        return [200, { data: locations }];
      }

      if (config.url === '/templates') {
        return [200, { data: mockTemplates }];
      }

      if (config.url === '/securityInfos/0') {
        return [200, { data: mockSecurityInfos }];
      }

      if (config.url === '/networks/0') {
        return [200, { data: mockNetworks }];
      }

      if (config.url === '/afs/0/configuredAfs') {
        return [200, { data: [JSON.stringify(configuredAfs)] }];
      }

      if (config.url === '/xtps/0') {
        return [200, { data: mockXtps }];
      }

      if (config.url === '/xdps/0') {
        return [200, { data: mockXdps }];
      }

      if (config.url === '/process-options/0') {
        return [200, { data: mockProcessOptions }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Save buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );
    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of access function when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/edit`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New Access Function')).toBeVisible();
  });

  it('should load the data retrieved from the server for ERK_UPF', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
    expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
    expect(screen.getByLabelText('Description:')).toHaveValue('some description');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson UPF');
    expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

    expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
    expect(screen.getByLabelText('Country:')).toBeEnabled();
    expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
    expect(screen.getByLabelText('State/Province:')).toBeEnabled();
    expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
    expect(screen.getByLabelText('City:')).toBeEnabled();
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);
  });

  describe('ERK_SMF', () => {
    it('should load the data retrieved from the server for ERK_SMF', async () => {
      accessFunction = {
        ...accessFunction,
        type: 'ERK_SMF',
        tag: 'ERK_SMF',
        provisioningInterfaces: [{
          id: 'eEFGXzU4NjMwMjY2NiMx',
          name: 'eEFGXzU4NjMwMjY2NiMx',
          options: [
            { key: 'afid', value: 'xAF_586302666' },
            { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'ACTIVE' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'ifid', value: '1' },
            { key: 'destPort', value: '123' },
            { key: 'dscp', value: '32' },
            { key: 'destIPAddr', value: '1.1.1.1' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('Country:')).toBeEnabled();
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('State/Province:')).toBeEnabled();
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('City:')).toBeEnabled();
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('123');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(provInterface, 'FieldCurrentState').textContent).toBe('Active');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('TLS12_CLIENT');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0 - 63):*')).toHaveValue('32');
    });
  });

  it('should reset the city selection when changing the state and then be able to select new city', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: 'California' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());

    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'San Francisco' } });
    expect(screen.getByDisplayValue('San Francisco')).toBeVisible();
  });

  it('should disable the city when unselecting the state', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should disable the state and city when unselecting the country', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeDisabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should show the state as enabled when only country loaded', async () => {
    accessFunction = {
      ...accessFunction,
      stateName: '',
      city: '',
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    expect(screen.getByDisplayValue('US')).toBeVisible();
    expect(screen.getByDisplayValue('US')).toBeEnabled();
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should show the timezone even if it is not available in the list', async () => {
    accessFunction.timeZone = 'America/Tijuana';

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/America\/Tijuana/);
  });

  it('should show the list of SS8 Distributors Configuration as an empty table', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: '' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')[0]).toBeVisible();
  });

  it('should show success message on Save of a valid form', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: 'AF Name 1' } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    expect(screen.getByDisplayValue('AF Name 1')).toBeVisible();
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeVisible();

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Access Function successfully updated.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: 'AF Name to fail' } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    expect(screen.getByDisplayValue('AF Name to fail')).toBeVisible();
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeVisible();

    fireEvent.click(screen.getAllByText('Save')[0]);
    await waitFor(() => expect(screen.getByText('Unable to update Access Function: Request failed with status code 500. Try again later.')).toBeVisible());
  });

  it('should show error message on Save if edit Access Function name is duplicated', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: mockAccessFunctionsUPF[1].name } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show error message on Save if edit Access Function name is duplicated even to different types', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: mockAccessFunctionsSMF[1].name } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show success message on Save of a valid form, not treating current AF own name in list as a duplicated one', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('AF Name to edit')[0], { target: { value: accessFunction.name } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    expect(screen.getByDisplayValue(accessFunction.name)).toBeVisible();
    expect(screen.getByDisplayValue('Ericsson UPF')).toBeVisible();

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Access Function successfully updated.')).toBeVisible());
  });

  it('should show an error message on Save if the Network ID was not found', async () => {
    accessFunction = {
      ...accessFunction,
      version: '',
      model: '',
      serialNumber: '',
      type: 'MVNR_PGW',
      tag: 'MVNR_PGW',
      insideNat: true,
      contentDeliveryVia: 'trafficManager',
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        options: [
          { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
          { key: 'lbid', value: 'MQ' },
          { key: 'destIPAddr', value: '1.1.1.1' },
          { key: 'destPort', value: '1' },
          { key: 'ownnetid', value: 'U1M4XzMwNzA' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'INACTIVE' },
          { key: 'traceLevel', value: '1' },
          { key: 'type', value: 'MVNRPGW_TCP' },
          { key: 'dscp', value: '63' },
        ],
      }],
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

    expect(getByLabelText(contentInterfaceWithTM, 'Network ID:*')).toHaveDisplayValue('Network ID not found');

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(screen.getByText('Please select another network ID.')).toBeVisible();
  });

  it('should render the XDP IP Address drop down with an additional option that is not initially available', async () => {
    accessFunction = {
      ...accessFunction,
      version: '',
      model: '',
      serialNumber: '',
      type: 'ACME_X123',
      tag: 'ACME_X123',
      insideNat: true,
      provisioningInterfaces: [{
        id: 'id1',
        name: '',
        options: [
          { key: 'destIPAddr', value: '1.1.2.2' },
          { key: 'destPort', value: '123' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'ownIPAddr', value: '0.0.0.0' },
          { key: 'ownIPPort', value: '0' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
        ],
      }],
      dataInterfaces: [{
        id: 'id2',
        name: '',
        options: [
          { key: 'destIPAddr', value: '1.2.2.2' },
          { key: 'destPort', value: '122' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
        ],
      }],
      contentInterfaces: [{
        id: 'id3',
        name: '',
        options: [
          { key: 'ownIPAddr', value: '9.9.9.9' },
        ],
      }],
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

    expect(getByLabelText(contentInterfaceWithTM, 'XDP IP Address:*')).toHaveDisplayValue('9.9.9.9');
  });

  it('should show an error on load if the network id is not valid', async () => {
    store.dispatch({
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [{ id: 'ID_TO_GET_SMF_INVALID' }],
    });

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionEdit />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

    expect(screen.getByLabelText('Network ID:*')).toHaveDisplayValue('Network ID not found');
    expect(screen.getByText('Please select another network ID.')).toBeVisible();
  });

  describe('ALU_MME', () => {
    it('should load the initial data for ALU_MME', async () => {
      accessFunction = {
        ...accessFunction,
        version: '1',
        model: '2',
        serialNumber: '3',
        type: 'ALU_MME',
        tag: 'ALU_MME',
        insideNat: true,
        provisioningInterfaces: [{
          id: '3.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '11' },
            { key: 'ownIPPort', value: '111' },
            { key: 'dscp', value: '10' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
          ],
        }],
        dataInterfaces: [{
          id: '2.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '2.2.2.2' },
            { key: 'destPort', value: '22' },
            { key: 'dscp', value: '20' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'FAILED' },
            { key: 'tpkt', value: 'Y' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent MME');
      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('1');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('2');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('3');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('11');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):*')).toHaveValue('111');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(provInterface, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('10');

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(dataInterface, 'AF Port:*')).toHaveValue('22');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(dataInterface, 'FieldCurrentState').textContent).toBe('Failed');

      fireEvent.click(getByTestId(dataInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(dataInterface, 'DSCP (0-63):*')).toHaveValue('20');
      expect(getByLabelText(dataInterface, 'TPKT')).toBeChecked();
    });
  });

  describe('ALU_DSS', () => {
    beforeEach(() => {
      accessFunction = {
        ...accessFunction,
        version: '0302',
        model: 'SGS-IWF',
        serialNumber: '3',
        type: 'ALU_DSS',
        tag: 'ALU_DSS',
        insideNat: true,
        provisioningInterfaces: [{
          id: '3.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '11' },
            { key: 'ownIPPort', value: '111' },
            { key: 'dscp', value: '10' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'imei', value: true },
            { key: 'imsi', value: true },
            { key: 'msisdn', value: true },
            { key: 'timeout', value: '11' },
          ],
        }],
        dataInterfaces: [{
          id: '2.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '2.2.2.2' },
            { key: 'destPort', value: '22' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'FAILED' },
            { key: 'ownIPPort', value: '11550' },
            { key: 'mscid', value: '123' },
          ],
        }],
      };
    });

    it('should load the initial data for ALU_DSS', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Alcatel-Lucent Distributed Soft Switch');
      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('0302');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('SGS-IWF');
      expect(screen.getByLabelText('Serial Number:*')).toHaveValue('3');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('11');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):')).toHaveValue('111');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(provInterface, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('10');

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('22');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toHaveValue('11550');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(dataInterface, 'FieldCurrentState').textContent).toBe('Failed');
      expect(getByLabelText(dataInterface, 'MSC ID:*')).toHaveValue('123');
    });

    it('should be possible to remove and add the delivery interface', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByTestId('FormSectionRemove')).toBeVisible();
      expect(screen.getByTestId('FormSectionRemove')).toHaveTextContent('Remove');

      fireEvent.click(screen.getByTestId('FormSectionRemove'));

      expect(screen.getByTestId('FormSectionAddButton')).toBeVisible();

      fireEvent.click(screen.getByTestId('FormSectionAddButton'));
      expect(screen.getByTestId('FormSectionRemove')).toBeVisible();

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toHaveValue('11550');
      expect(getByLabelText(dataInterface, 'XCP Port:')).toBeDisabled();
      expect(getByLabelText(dataInterface, 'MSC ID:*')).toHaveValue('');
      expect(getByLabelText(dataInterface, 'Configured State:')).toHaveDisplayValue('Active');
    });
  });

  describe('MV_SMSIWF', () => {
    it('should load the initial data for MV_SMSIWF', async () => {
      accessFunction = {
        ...accessFunction,
        version: '',
        model: '',
        serialNumber: '',
        type: 'MV_SMSIWF',
        tag: 'MV_SMSIWF',
        insideNat: true,
        provisioningInterfaces: [{
          id: '3.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '11' },
            { key: 'ownIPPort', value: '111' },
            { key: 'dscp', value: '10' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'secinfoid', value: 'Tk9ORQ' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
          ],
        }],
        dataInterfaces: [{
          id: '2.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '2.2.2.2' },
            { key: 'destPort', value: '22' },
            { key: 'dscp', value: '20' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'secinfoid', value: 'Tk9ORQ' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir SMS IWF');

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Software Version:')).toBeDisabled();
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toBeDisabled();
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(provInterface, 'AF Port:')).toHaveValue('11');
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(provInterface, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('10');

      const dataInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(dataInterface, 'AF IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(dataInterface, 'AF Port:')).toHaveValue('22');
      expect(getByLabelText(dataInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(dataInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');

      fireEvent.click(getByTestId(dataInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(dataInterface, 'DSCP (0-63):*')).toHaveValue('20');
    });
  });

  describe('ERK_PGW', () => {
    beforeEach(() => {
      accessFunction = {
        ...accessFunction,
        version: '0302',
        model: '',
        serialNumber: '3',
        type: 'ERK_PGW',
        tag: 'ERK_PGW',
        insideNat: true,
        contentDeliveryVia: 'directFromAF',
        contentInterfaces: [{
          id: '1.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '2.2.2.2' },
            { key: 'destPort', value: '22' },
            { key: 'secinfoid', value: 'Tk9ORQ' },
            { key: 'netid', value: 'WENQcmNz' },
            { key: 'transport', value: 'UDP' },
          ],
        }],
      };
    });

    it('should load the initial data for ERK_PGW', async () => {
      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SAEGW/PGW');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('without Traffic Manager');
      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();

      // TODO: fix error on suggestText box
      // expect(screen.getByLabelText('Software Version:')).toHaveValue('0302');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('3');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      const contentInterface = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterface, 'AF IP Address:*')).toHaveValue('2.2.2.2');
      expect(getByLabelText(contentInterface, 'AF Port:')).toHaveValue('22');
      expect(getByLabelText(contentInterface, 'TLS Profile (Security Info ID):')).toHaveValue('Tk9ORQ');
      expect(getByLabelText(contentInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      fireEvent.click(getByTestId(contentInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(contentInterface, 'Transport:')).toHaveValue('UDP');
    });
  });

  describe('MVNR_PGW', () => {
    it('should load the initial data for MVNR_PGW', async () => {
      accessFunction = {
        ...accessFunction,
        version: '',
        model: '',
        serialNumber: '',
        type: 'MVNR_PGW',
        tag: 'MVNR_PGW',
        insideNat: true,
        contentDeliveryVia: 'trafficManager',
        contentInterfaces: [{
          id: '1',
          name: 'name1',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'lbid', value: 'MQ' },
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '1' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'traceLevel', value: '1' },
            { key: 'type', value: 'MVNRPGW_TCP' },
            { key: 'dscp', value: '63' },
          ],
        }, {
          id: '2',
          name: 'name2',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'lbid', value: 'MQ' },
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '2' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'traceLevel', value: '1' },
            { key: 'type', value: 'MVNRPGW_TCP' },
            { key: 'dscp', value: '63' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');
      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

      expect(screen.queryAllByTestId('FormSection-provisioningInterfaces')).toHaveLength(0);

      expect(screen.queryAllByTestId('FormSection-dataInterfaces')).toHaveLength(0);

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterfaceWithTM, 'XTP ID:')).toHaveDisplayValue(['1']);
      expect(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:')).toHaveDisplayValue('2');
      expect(getByLabelText(contentInterfaceWithTM, 'AF IP Address:*')).toHaveValue('1.1.1.1');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveDisplayValue('1,2');

      expect(getByLabelText(contentInterfaceWithTM, 'Network ID:*')).toHaveDisplayValue('Network ID not found');
      expect(getByLabelText(contentInterfaceWithTM, 'Configured State:')).toHaveDisplayValue('Active');
      expect(getByTestId(contentInterfaceWithTM, 'FieldCurrentState').textContent).toBe('Inactive');

      fireEvent.click(getByTestId(contentInterfaceWithTM, 'CollapsibleFormButton'));

      expect(getByLabelText(contentInterfaceWithTM, 'DSCP (0-63):*')).toHaveValue('63');
      expect(getByLabelText(contentInterfaceWithTM, 'Delivery Type:')).toHaveValue('MVNRPGW_TCP');
      expect(getByLabelText(contentInterfaceWithTM, 'Delivery Type:')).toBeDisabled();
      expect(getByLabelText(contentInterfaceWithTM, 'Trace Level:')).toHaveDisplayValue('1');
    });

    it('should set the sec info as required and transport as HTTP when uri is set to https:', async () => {
      accessFunction = {
        ...accessFunction,
        type: 'MVNR_PGW',
        tag: 'MVNR_PGW',
        contentDeliveryVia: 'directFromAF',
        provisioningInterfaces: [{
          id: '1',
          name: 'name1',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'dscp', value: '63' },
            { key: 'litype', value: 'INI1' },
            { key: 'keepaliveinterval', value: '60' },
            { key: 'litargetnamespace', value: 'http://mavenir.net/li/' },
            { key: 'secinfoid', value: 'Tk9ORQ' },
            { key: 'transport', value: 'HTTP' },
            { key: 'uri', value: 'http://domain.com' },
            { key: 'version', value: '1.0' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveDisplayValue('http://domain.com');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');

      fireEvent.change(getByLabelText(provInterface, 'AF URL:*'), { target: { value: 'https://domain.com' } });
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');
    });

    it('should leave the AF Port as is and disabled the field if max connection smaller than 4', async () => {
      accessFunction = {
        ...accessFunction,
        version: '',
        model: '',
        serialNumber: '',
        type: 'MVNR_PGW',
        tag: 'MVNR_PGW',
        insideNat: true,
        contentDeliveryVia: 'trafficManager',
        contentInterfaces: [{
          id: '1',
          name: 'name1',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'lbid', value: 'MQ' },
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '1' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'traceLevel', value: '1' },
            { key: 'type', value: 'MVNRPGW_TCP' },
            { key: 'dscp', value: '63' },
          ],
        }, {
          id: '2',
          name: 'name2',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'lbid', value: 'MQ' },
            { key: 'destIPAddr', value: '1.1.1.1' },
            { key: 'destPort', value: '2' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'traceLevel', value: '1' },
            { key: 'type', value: 'MVNRPGW_TCP' },
            { key: 'dscp', value: '63' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');
      expect(screen.getByLabelText('Configure:')).toHaveDisplayValue('with Traffic Manager');

      const contentInterfaceWithTM = screen.getByTestId('FormSection-contentInterfaces');

      expect(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:')).toHaveDisplayValue('2');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('1,2');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toBeEnabled();

      fireEvent.change(getByLabelText(contentInterfaceWithTM, 'Maximum Concurrent Connections:'), { target: { value: '5' } });
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toHaveValue('0');
      expect(getByLabelText(contentInterfaceWithTM, 'AF Port:')).toBeDisabled();
    });
  });

  describe('PCRF', () => {
    it('should load the initial data for Cisco PCRF', async () => {
      accessFunction = {
        ...accessFunction,
        version: '1',
        model: 'CISCOVPCRF',
        tag: 'CISCOVPCRF',
        serialNumber: '3',
        type: 'CISCOVPCRF',
        insideNat: true,
        provisioningInterfaces: [{
          id: '3.accessFunctionInterface',
          name: '',
          options: [
            { key: 'uri', value: 'https://111.111.111.111/cgi-bin/PCRF/PCRF.py' },
            { key: 'destPort', value: '6443' },
            { key: 'secinfoid', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'ccdestip', value: '::' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'FAILED' },
            { key: 'ownipaddr', value: 'ADDR_ANY' },
            { key: 'ownIPPort', value: '1000' },
            { key: 'interval', value: '50' },
            { key: 'dscp', value: '60' },
          ],
        }],
        dataInterfaces: [{
          id: '2.accessFunctionInterface',
          name: '',
          options: [
            { key: 'destIPAddr', value: '111.111.111.111' },
            { key: 'destPort', value: '10' },
            { key: 'secinfoid', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'ownipaddr', value: 'ADDR_ANY' },
            { key: 'ownIPPort', value: '51500' },
            { key: 'interval', value: '30' },
            { key: 'dscp', value: '20' },
            { key: 'afid', value: 'CiscovPCRF' },
            { key: 'ifid', value: '13' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Cisco Virtual PCRF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).toBeChecked();

      expect(screen.getByLabelText('Software Version:')).toHaveValue('1');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('CISCOVPCRF');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('3');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('US/Pacific (GMT-07)');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('https://111.111.111.111/cgi-bin/PCRF/PCRF.py');
      expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('6443');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('HTTPS_Server_Sim');
      expect(getByLabelText(provInterface, 'Call Data Network ID:*')).toHaveValue('WENQcmNz');
      expect(getByLabelText(provInterface, 'Call Content Destination IP Address:*')).toHaveValue('::');
      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'XCP IP Address:*')).toHaveValue('');
      expect(getByLabelText(provInterface, 'XCP Port (TCP):')).toHaveValue('1000');
      expect(getByLabelText(provInterface, 'Message Update Interval:')).toHaveValue('50');
      expect(getByLabelText(provInterface, 'DSCP (0-63):*')).toHaveValue('60');

      const iriInterface = screen.getByTestId('FormSection-dataInterfaces');

      expect(getByLabelText(iriInterface, 'AF IP Address:*')).toHaveValue('111.111.111.111');
      expect(getByLabelText(iriInterface, 'AF Port:*')).toHaveValue('10');
      expect(getByLabelText(iriInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('HTTPS_Server_Sim');
      expect(getByLabelText(iriInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(iriInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(iriInterface, 'XCP IP Address:*')).toHaveValue('');
      expect(getByLabelText(iriInterface, 'XCP Port (TCP):')).toHaveValue('51500');
      expect(getByLabelText(iriInterface, 'Message Update Interval:')).toHaveValue('30');
      expect(getByLabelText(iriInterface, 'DSCP (0-63):*')).toHaveValue('20');
    });
  });

  describe('ERK_5GAMF', () => {
    it('should load the initial data for ERK_5GAMF with 5GC VPC enabled', async () => {
      accessFunction = {
        ...accessFunction,
        model: '',
        tag: 'ERK_5GAMF',
        serialNumber: '',
        type: 'ERK_5GAMF',
        insideNat: false,
        provisioningInterfaces: [{
          id: '3.accessFunctionInterface',
          name: '',
          options: [
            { key: 'role', value: 'CLIENT' },
            { key: 'uri', value: 'https://111.111.111.111' },
            { key: 'keepaliveinterval', value: '61' },
            { key: 'ownnetid', value: 'WENQcmNz' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'destIPAddr', value: '0.0.0.0' },
            { key: 'state', value: 'FAILED' },
            { key: 'appProtocol', value: 'ERK_5GAMF' },
            { key: 'version', value: '1.7.1' },
            { key: 'transport', value: 'HTTP' },
            { key: 'mdf2NodeName', value: 'MDF2NODE' },
          ],
        }],
        dataInterfaces: [],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionEdit />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByLabelText('Name:*')).toHaveValue('AF Name to edit');
      expect(screen.getByLabelText('Description:')).toHaveValue('some description');
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson AMF');
      expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

      // expect(screen.getByLabelText('Software Version:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toHaveValue('');
      expect(screen.getByLabelText('Model Number:')).toBeDisabled();
      expect(screen.getByLabelText('Serial Number:')).toHaveValue('');
      expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

      expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
      expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
      expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
      expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue('US/Pacific (GMT-07)');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveValue('https://111.111.111.111');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('SIMPLESERVER1');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toBeDisabled();
      expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
      expect(getByLabelText(provInterface, 'MDF2 Node Name:*')).toHaveDisplayValue('MDF2NODE');

      expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));
      expect(getByLabelText(provInterface, 'Keepalive Interval (Sec):')).toHaveValue('61');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveValue('HTTP');
      expect(getByLabelText(provInterface, 'Transport:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol:')).toHaveValue('ERK_5GAMF');
      expect(getByLabelText(provInterface, 'AF Protocol:')).toBeDisabled();
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toHaveValue('1.7.1');
      expect(getByLabelText(provInterface, 'AF Protocol Version:')).toBeDisabled();
    });
  });
});
