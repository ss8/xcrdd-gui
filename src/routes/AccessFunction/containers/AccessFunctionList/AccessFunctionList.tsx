import React, {
  FunctionComponent, useEffect, useState, Fragment,
} from 'react';
import { ConnectedProps, connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { SelectionChangedEvent } from '@ag-grid-enterprise/all-modules';
import { RootState } from 'data/root.reducers';
import { GridSettings, WithAuditDetails, AuditDetails } from 'data/types';
import {
  AccessFunctionPrivileges,
  AF_USER_CATEGORY_SETTINGS,
  AccessFunctionDataActionDetails,
  AccessFunctionGridRowData,
} from 'data/accessFunction/accessFunction.types';
import * as actions from 'data/accessFunction/accessFunction.actions';
import * as distributorConfigurationActions from 'data/distributorConfiguration/distributorConfiguration.actions';
import * as distributorConfigurationSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import * as userActions from 'users/users-actions';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as processOptionActions from 'data/processOption/processOption.actions';
import * as networkActions from 'data/network/network.actions';
import * as xdpActions from 'data/xdp/xdp.actions';
import * as nodeActions from 'data/nodes/nodes.actions';
import * as selectors from 'data/accessFunction/accessFunction.selectors';
import * as supportedFeaturesSelectors from 'data/supportedFeatures/supportedFeatures.selectors';
import * as discoverySelectors from 'xcipio/discovery-xcipio-selectors';
import * as authenticationSelectors from 'data/authentication/authentication.selectors';
import * as templateSelectors from 'data/template/template.selectors';
import * as networkSelectors from 'data/network/network.selectors';
import * as xdpSelectors from 'data/xdp/xdp.selectors';
import * as nodeSelectors from 'data/nodes/nodes.selectors';
import { AppToaster } from 'shared/toaster';
import ModalDialog from 'shared/modal/dialog';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';
import useConfirmActionDialog from 'utils/hooks/useConfirmActionDialog';
import { adaptAccessFunctionsGridRowData } from 'routes/AccessFunction/utils/accessFunctionList.adapters';
import { adaptTemplateOptions } from 'routes/AccessFunction/utils/accessFunctionTemplate.adapter';
import AccessFunctionGrid from './components/AccessFunctionGrid';
import {
  accessFunctionsColumnDefs,
  accessFunctionAutoGroupColumnDef,
  AccessFunctionMenuActions,
  accessFunctionsColumnDefsVersion,
} from '../../AccessFunction.types';
import { isAccessFunctionTemplateAvailable } from '../../utils/isAccessFunctionTemplateAvailable';
import {
  CannotTriggerAuditDialogBody,
  DeleteDialogBody,
  MissingTemplateDialogBody,
  TriggerActionDialogBody,
} from './components/DialogBody';
import isActionSupportedForAccessFunction from '../../utils/isActionSupportedForAccessFunction';

type RouterParams = {
  history: string
}

const mapState = (state: RootState) => ({
  beforeLogout: state.authentication.beforeLogout,
  privileges: state.authentication.privileges,
  deliveryFunctionId: state.global.selectedDF,
  data: selectors.getPoiAccessFunctions(state),
  enabledAccessFunctions: supportedFeaturesSelectors.getEnabledAccessFunction(state),
  selectedAccessFunction: selectors.getSelectedAccessFunction(state),
  isDeletingAccessFunction: selectors.isDeletingAccessFunction(state),
  isAuditEnabled: discoverySelectors.isDiscoverySettingAuditEnabled(state),
  userId: authenticationSelectors.getAuthenticationUserId(state),
  userName: authenticationSelectors.getAuthenticationUserName(state),
  accessFunctionTemplates: templateSelectors.getEnabledAccessFunctionFormTemplatesByKey(state),
  networks: networkSelectors.getNetworks(state),
  xdps: xdpSelectors.getXdps(state),
  xcipioNodes: nodeSelectors.getNodes(state),
  distributorConfigurationsByAccessFunctionType:
    distributorConfigurationSelectors.getDistributorConfigurationsByAccessFunctionType(state),
});

const mapDispatch = {
  getAccessFunctions: actions.getAccessFunctionsWithViewOptions,
  goToAccessFunctionCreate: actions.goToAccessFunctionCreate,
  goToAccessFunctionEdit: actions.goToAccessFunctionEdit,
  goToAccessFunctionView: actions.goToAccessFunctionView,
  goToAccessFunctionCopy: actions.goToAccessFunctionCopy,
  getGridSettings: userActions.getUserSettings,
  saveGridSettings: userActions.putUserSettings,
  selectAccessFunction: actions.selectAccessFunction,
  deleteAccessFunction: actions.deleteAccessFunction,
  getSupportedFeature: warrantActions.getWarrantFormConfig,
  auditAccessFunction: actions.auditAccessFunction,
  resetAccessFunction: actions.resetAccessFunction,
  refreshAccessFunction: actions.refreshAccessFunction,
  getTemplates: warrantActions.getAllTemplates,
  getConfiguredAccessFunctions: actions.getConfiguredAccessFunctions,
  getProcessOptions: processOptionActions.getProcessOptions,
  getNetworks: networkActions.getNetworks,
  getXdps: xdpActions.getXdps,
  getXcipioNodes: nodeActions.getNodes,
  getDistributorConfigurationsWithView: distributorConfigurationActions.getDistributorConfigurationsWithView,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type PropsFromRouter = RouteComponentProps<RouterParams>

export type Props = PropsFromRedux & PropsFromRouter;

export const AccessFunctionList: FunctionComponent<Props> = ({
  beforeLogout,
  privileges,
  deliveryFunctionId,
  data = [],
  history,
  selectedAccessFunction,
  enabledAccessFunctions,
  isAuditEnabled,
  userId,
  userName,
  accessFunctionTemplates,
  networks,
  xdps,
  xcipioNodes,
  distributorConfigurationsByAccessFunctionType,
  goToAccessFunctionCreate,
  goToAccessFunctionEdit,
  goToAccessFunctionView,
  goToAccessFunctionCopy,
  getAccessFunctions,
  getGridSettings,
  saveGridSettings,
  selectAccessFunction,
  deleteAccessFunction,
  getSupportedFeature,
  auditAccessFunction,
  resetAccessFunction,
  refreshAccessFunction,
  getTemplates,
  getConfiguredAccessFunctions,
  getProcessOptions,
  getNetworks,
  getXdps,
  getXcipioNodes,
  getDistributorConfigurationsWithView,
}: Props) => {
  const [accessFunctionGridRowData, setAccessFunctionGridRowData] = useState<AccessFunctionGridRowData[]>([]);
  const [lastUpdatedAt, setLastUpdatedAt] = useState<Date>();
  const [confirmActionDialog, setConfirmActionDialog] = useConfirmActionDialog();

  useEffect(() => {
    AppToaster.clear();
    getSupportedFeature(deliveryFunctionId);
    getAccessFunctions(deliveryFunctionId);
    getDistributorConfigurationsWithView(deliveryFunctionId);
    getTemplates();
    getConfiguredAccessFunctions(deliveryFunctionId);
    getProcessOptions(deliveryFunctionId);
    getNetworks(deliveryFunctionId);
    getXdps(deliveryFunctionId);
    getXdps(deliveryFunctionId);
    getXcipioNodes(deliveryFunctionId);
    return () => {
      AppToaster.clear();
    };
  }, [
    deliveryFunctionId,
    getAccessFunctions,
    getDistributorConfigurationsWithView,
    getSupportedFeature,
    getTemplates,
    getConfiguredAccessFunctions,
    getProcessOptions,
    getNetworks,
    getXdps,
    getXcipioNodes,
  ]);

  useEffect(() => {
    setLastUpdatedAt(new Date());
    const availableTemplates = adaptTemplateOptions(
      accessFunctionTemplates,
      [],
      [],
      [],
      networks,
      [],
      [],
      [],
      xdps,
      [],
      enabledAccessFunctions,
      xcipioNodes,
    );

    const gridRowData = adaptAccessFunctionsGridRowData(
      data,
      enabledAccessFunctions,
      availableTemplates,
      networks,
      xdps,
      distributorConfigurationsByAccessFunctionType,
    );

    setAccessFunctionGridRowData(gridRowData);
  }, [
    data,
    enabledAccessFunctions,
    accessFunctionTemplates,
    networks,
    xdps,
    xcipioNodes,
    distributorConfigurationsByAccessFunctionType,
  ]);

  const handleRefresh = () => getAccessFunctions(deliveryFunctionId);

  const handleCreateNew = () => goToAccessFunctionCreate(history);

  const handleSelection = (event: SelectionChangedEvent) => {
    return selectAccessFunction(event.api.getSelectedRows());
  };

  const handleSaveGridSettings = (gridSettings: GridSettings) =>
    saveGridSettings(AF_USER_CATEGORY_SETTINGS, userName, gridSettings);

  const handleGetGridSettings = () => getGridSettings(AF_USER_CATEGORY_SETTINGS, userName);

  const handleShowActionMenuItem = (type: AccessFunctionMenuActions, accessFunctionTag: string) => {
    if (type === AccessFunctionMenuActions.Reset || type === AccessFunctionMenuActions.Refresh) {
      return isActionSupportedForAccessFunction(type, accessFunctionTag, enabledAccessFunctions);
    }

    return true;
  };

  const showPopupForUnavailableTemplate = (title: string, accessFunctionTag: string) => {
    const accessFunctionTitle = enabledAccessFunctions[accessFunctionTag].title;

    setConfirmActionDialog(
      `${title} Access Function - Missing Template`,
      <MissingTemplateDialogBody name={accessFunctionTitle} />,
      'Close',
    );
  };

  const handleSelectItemMenuAction = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const accessFunction = selectedAccessFunction[0];
    if (!accessFunction) {
      return;
    }
    const {
      name,
      tag,
      provisioningInterfaces,
    } = accessFunction;

    if (event?.type === AccessFunctionMenuActions.Delete) {
      setConfirmActionDialog(
        'Delete Access Function',
        <DeleteDialogBody name={name} />,
        'Yes, delete it',
        handleDeleteAccessFunction,
      );

    } else if (event?.type === AccessFunctionMenuActions.Audit) {
      if (provisioningInterfaces && provisioningInterfaces.length > 0) {
        setConfirmActionDialog(
          'Audit Access Function',
          <TriggerActionDialogBody>
            Do you want to trigger an audit on &#39;{name}&#39;?
          </TriggerActionDialogBody>,
          'Yes, audit it',
          handleAuditAccessFunction,
        );

      } else {
        setConfirmActionDialog(
          'Audit Access Function',
          <CannotTriggerAuditDialogBody name={name} />,
          'Close',
        );
      }

    } else if (event?.type === AccessFunctionMenuActions.Reset) {
      setConfirmActionDialog(
        'Reset Access Function',
        <TriggerActionDialogBody>
          Do you want to trigger a reset on the AF &#39;{name}&#39; target database?
        </TriggerActionDialogBody>,
        'Yes, reset it',
        handleResetAccessFunction,
      );

    } else if (event?.type === AccessFunctionMenuActions.Refresh) {
      setConfirmActionDialog(
        'Refresh Access Function',
        <TriggerActionDialogBody>
          Do you want to trigger a refresh on the AF &#39;{name}&#39; database?
        </TriggerActionDialogBody>,
        'Yes, refresh it',
        handleRefreshAccessFunction,
      );

    } else if (event?.type === AccessFunctionMenuActions.Edit) {
      if (isAccessFunctionTemplateAvailable(accessFunctionTemplates, selectedAccessFunction[0])) {
        goToAccessFunctionEdit(history);
      } else {
        showPopupForUnavailableTemplate('Edit', tag);
      }

    } else if (event?.type === AccessFunctionMenuActions.View) {
      if (isAccessFunctionTemplateAvailable(accessFunctionTemplates, selectedAccessFunction[0])) {
        goToAccessFunctionView(history);
      } else {
        showPopupForUnavailableTemplate('View', tag);
      }

    } else if (event?.type === AccessFunctionMenuActions.Copy) {
      if (isAccessFunctionTemplateAvailable(accessFunctionTemplates, selectedAccessFunction[0])) {
        goToAccessFunctionCopy(history);
      } else {
        showPopupForUnavailableTemplate('Copy', tag);
      }
    }
  };

  const handleDeleteAccessFunction = () => {
    const accessFunction = selectedAccessFunction[0];
    if (accessFunction == null || accessFunction.id == null) {
      return;
    }

    const accessFunctionBody: WithAuditDetails<void> = { data: undefined };

    if (isAuditEnabled) {
      const auditDetails = getAuditDetails(
        isAuditEnabled,
        userId,
        userName,
        getAuditFieldInfo(null),
        accessFunction.name,
        AuditService.AF,
        AuditActionType.ACCESS_FUNCTION_DELETE,
      );
      accessFunctionBody.auditDetails = auditDetails as AuditDetails;
    }

    deleteAccessFunction(deliveryFunctionId, accessFunction.id, accessFunctionBody);
  };

  const handleExecuteActionAccessFunction = (
    action: 'AUDIT' | 'RESET' | 'DWNLD',
    auditActionType: AuditActionType,
    executeActionAccessFunction: CallableFunction,
  ) => {
    const accessFunction = selectedAccessFunction[0];
    if (accessFunction == null || accessFunction.id == null) {
      return;
    }

    const accessFunctionBody: WithAuditDetails<AccessFunctionDataActionDetails> = {
      data: { action },
    };

    if (isAuditEnabled) {
      const auditDetails = getAuditDetails(
        isAuditEnabled,
        userId,
        userName,
        getAuditFieldInfo(null),
        accessFunction.name,
        AuditService.AF,
        auditActionType,
      );
      accessFunctionBody.auditDetails = auditDetails as AuditDetails;
    }

    executeActionAccessFunction(deliveryFunctionId, accessFunction.id, accessFunctionBody);
  };

  const handleAuditAccessFunction = () => {
    handleExecuteActionAccessFunction(
      'AUDIT',
      AuditActionType.ACCESS_FUNCTION_AUDIT,
      auditAccessFunction,
    );
  };

  const handleResetAccessFunction = () => {
    handleExecuteActionAccessFunction(
      'RESET',
      AuditActionType.ACCESS_FUNCTION_RESET,
      resetAccessFunction,
    );
  };

  const handleRefreshAccessFunction = () => {
    handleExecuteActionAccessFunction(
      'DWNLD',
      AuditActionType.ACCESS_FUNCTION_REFRESH,
      refreshAccessFunction,
    );
  };

  const hasPrivilegesToCreate = privileges.includes(AccessFunctionPrivileges.Edit);
  const hasPrivilegeToDelete = privileges.includes(AccessFunctionPrivileges.Delete);
  const hasPrivilegeToView = privileges.includes(AccessFunctionPrivileges.View);

  return (
    <Fragment>
      <AccessFunctionGrid
        hasPrivilegesToCreate={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegeToView={hasPrivilegeToView}
        data={accessFunctionGridRowData}
        noResults={false}
        beforeLogout={beforeLogout}
        onRefresh={handleRefresh}
        onCreateNew={handleCreateNew}
        onSelectionChanged={handleSelection}
        onSaveGridSettings={handleSaveGridSettings}
        onGetGridSettings={handleGetGridSettings}
        onSelectItemMenuAction={handleSelectItemMenuAction}
        onShowActionMenuItem={handleShowActionMenuItem}
        lastUpdatedAt={lastUpdatedAt}
        columnDefs={accessFunctionsColumnDefs}
        accessFunctionsColumnDefsVersion={accessFunctionsColumnDefsVersion}
        autoGroupColumnDef={accessFunctionAutoGroupColumnDef}
        suppressAggFuncInHeader
        groupSelectsChildren
        suppressRowClickSelection
        enableGroupSelection
      />
      <ModalDialog
        width="500px"
        isOpen={confirmActionDialog.isOpen}
        title={confirmActionDialog.title}
        customComponent={confirmActionDialog.customComponent}
        actionText={confirmActionDialog.actionText}
        onSubmit={confirmActionDialog.onSubmit}
        onClose={confirmActionDialog.onClose}
        doNotDisplayCancelButton={confirmActionDialog.hideCancel}
      />
    </Fragment>
  );
};

export default withRouter((connector(AccessFunctionList)));
