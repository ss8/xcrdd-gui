import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { MenuItem } from '@blueprintjs/core';
import { AccessFunctionGridRowData } from 'data/accessFunction/accessFunction.types';
import AccessFunctionActionMenu from './AccessFunctionActionMenu';
import { AccessFunctionMenuActions } from '../../../../AccessFunction.types';

describe('<AccessFunctionActionMenu />', () => {
  const data: AccessFunctionGridRowData = {
    afid: '',
    configuredState: '',
    destination: '',
    destinationToolTip: '',
    id: '',
    ipAddress: '',
    name: '',
    port: '',
    state: '',
    subType: '',
    tag: '',
    tagName: '',
    provisioningInterfaces: [],
  };

  const node = {
    allLeafChildren: [{
      data,
    }],
  };

  describe('when user has Privilege to View, Edit, Delete Update, Audit', () => {
    let wrapper: ReactWrapper;
    const context = {
      onActionMenuItemClick: jest.fn(),
      props: {
        showActionMenuItem: () => true,
        hasPrivilegeToCreateNew: true,
        hasPrivilegeToDelete: true,
        hasPrivilegetoView: true,
      },
    };
    beforeEach(() => {
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should render all AccessFunctionActionMenu items', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(7);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Edit');
      expect(menuItems.at(2).text()).toBe('Copy');
      expect(menuItems.at(3).text()).toBe('Delete');
      expect(menuItems.at(4).text()).toBe('Audit');
      expect(menuItems.at(5).text()).toBe('Refresh');
      expect(menuItems.at(6).text()).toBe('Reset');
    });

    it('should click View menu item and call onActionMenuItemClick with View data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(0).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Edit menu item and call onActionMenuItemClick with Edit data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(1).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Copy menu item and call onActionMenuItemClick with Copy data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(2).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Delete menu item and call onActionMenuItemClick with Delete data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(3).find('a').simulate('click');

      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });

    it('should click Audit menu item and call onActionMenuItemClick with Audit data as arguments', () => {
      const menuItems = wrapper.find(MenuItem);
      menuItems.at(4).find('a').simulate('click');
      expect(context.onActionMenuItemClick).toHaveBeenCalled();
    });
  });

  describe('when user has Privilege to View, Edit, Delete Update but AF is not of Ericsson SMF', () => {
    let wrapper: ReactWrapper;
    const context = {
      onActionMenuItemClick: jest.fn(),
      props: {
        showActionMenuItem: () => true,
        hasPrivilegeToCreateNew: true,
        hasPrivilegeToDelete: true,
        hasPrivilegetoView: true,
      },
    };
    beforeEach(() => {
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should render all AccessFunctionActionMenu items', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(7);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Edit');
      expect(menuItems.at(2).text()).toBe('Copy');
      expect(menuItems.at(3).text()).toBe('Delete');
      expect(menuItems.at(4).text()).toBe('Audit');
      expect(menuItems.at(5).text()).toBe('Refresh');
      expect(menuItems.at(6).text()).toBe('Reset');
    });
  });

  describe('when user does not have View Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: () => true,
          hasPrivilegeToCreateNew: true,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: false,
        },
      };
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should render all AccessFunctionActionMenu items but View', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(6);
      expect(menuItems.at(0).text()).toBe('Edit');
      expect(menuItems.at(1).text()).toBe('Copy');
      expect(menuItems.at(2).text()).toBe('Delete');
      expect(menuItems.at(3).text()).toBe('Audit');
      expect(menuItems.at(4).text()).toBe('Refresh');
      expect(menuItems.at(5).text()).toBe('Reset');
    });
  });

  describe('when user does not have Create Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: () => true,
          hasPrivilegeToCreateNew: false,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should render all AccessFunctionActionMenu items but Edit and copy', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(2);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Delete');
    });
  });

  describe('when user has not Delete Privilege', () => {
    let wrapper: ReactWrapper;

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem: () => true,
          hasPrivilegeToCreateNew: true,
          hasPrivilegeToDelete: false,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should render all AccessFunctionActionMenu items but Delete', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(6);
      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Edit');
      expect(menuItems.at(2).text()).toBe('Copy');
      expect(menuItems.at(3).text()).toBe('Audit');
      expect(menuItems.at(4).text()).toBe('Refresh');
      expect(menuItems.at(5).text()).toBe('Reset');
    });
  });

  describe('when showActionMenuItem returns false to some menu items', () => {
    let wrapper: ReactWrapper;
    const showActionMenuItem = jest.fn((type: AccessFunctionMenuActions) => {
      if (type === AccessFunctionMenuActions.Refresh || type === AccessFunctionMenuActions.Reset) {
        return false;
      }

      return true;
    });

    beforeEach(() => {
      const context = {
        onActionMenuItemClick: jest.fn(),
        props: {
          showActionMenuItem,
          hasPrivilegeToCreateNew: false,
          hasPrivilegeToDelete: true,
          hasPrivilegetoView: true,
        },
      };
      wrapper = mount(<AccessFunctionActionMenu node={node} disabled={false} context={context} />);
    });

    it('should not render any menu item', () => {
      const menuItems = wrapper.find(MenuItem);
      expect(menuItems).toHaveLength(2);

      expect(menuItems.at(0).text()).toBe('View');
      expect(menuItems.at(1).text()).toBe('Delete');
    });
  });
});
