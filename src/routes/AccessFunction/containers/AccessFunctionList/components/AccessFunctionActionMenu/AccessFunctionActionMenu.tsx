import React, { FunctionComponent } from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import { AccessFunctionGridRowData } from 'data/accessFunction/accessFunction.types';
import { AccessFunctionMenuActions } from 'routes/AccessFunction/AccessFunction.types';

interface Props {
  disabled: boolean;
  node: {
    allLeafChildren: {
      data: AccessFunctionGridRowData,
    }[]
  },
  context: {
    onActionMenuItemClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    props: {
      showActionMenuItem: (type: AccessFunctionMenuActions, tag: string) => boolean;
      hasPrivilegeToCreateNew: boolean;
      hasPrivilegeToDelete: boolean;
      hasPrivilegetoView: boolean;
    }
  };
}

const AccessFunctionActionMenu: FunctionComponent<Props> = ({
  disabled,
  node,
  context: {
    onActionMenuItemClick,
    props: {
      showActionMenuItem,
      hasPrivilegetoView,
      hasPrivilegeToCreateNew,
      hasPrivilegeToDelete,
    },
  },
}: Props) => {

  const accessFunctionTag = node?.allLeafChildren?.[0].data.tag;

  return (
    <Menu>
      {hasPrivilegetoView && showActionMenuItem(AccessFunctionMenuActions.View, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.View}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="View"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem(AccessFunctionMenuActions.Edit, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Edit}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Edit"
        />
      )}
      {hasPrivilegeToCreateNew && showActionMenuItem(AccessFunctionMenuActions.Copy, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Copy}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Copy"
        />
      )}
      {hasPrivilegeToDelete && showActionMenuItem(AccessFunctionMenuActions.Delete, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Delete}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Delete"
        />
      )}

      {hasPrivilegeToCreateNew && showActionMenuItem(AccessFunctionMenuActions.Audit, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Audit}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Audit"
        />
      )}

      {hasPrivilegeToCreateNew && showActionMenuItem(AccessFunctionMenuActions.Refresh, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Refresh}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Refresh"
        />
      )}

      {hasPrivilegeToCreateNew && showActionMenuItem(AccessFunctionMenuActions.Reset, accessFunctionTag) && (
        <MenuItem
          id={AccessFunctionMenuActions.Reset}
          onClick={onActionMenuItemClick}
          disabled={disabled}
          text="Reset"
        />
      )}
    </Menu>
  );
};

export default AccessFunctionActionMenu;
