import React from 'react';
import { Provider } from 'react-redux';
import { mount, ReactWrapper } from 'enzyme';
import Http from 'shared/http';
import MockAdapter from 'axios-mock-adapter';
import store from 'data/store';
import { ensureGridApiHasBeenSet } from '__test__/utils';
import { accessFunctionsColumnDefs, accessFunctionAutoGroupColumnDef, accessFunctionsColumnDefsVersion } from 'routes/AccessFunction/AccessFunction.types';
import { mockAccessFunctions, mockAccessFunctionsRowData } from '__test__/mockAccessFunctionRowData';
import AccessFunctionGrid from './AccessFunctionGrid';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionGrid />', () => {
  let userSettings: unknown;

  beforeEach(() => {

    userSettings = {
      value: {
        data: {
          data: [
            {
              setting: '[]',
            },
          ],
        },
      },
    };
  });

  describe('When AF list input has data', () => {
    let wrapper: ReactWrapper;

    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      wrapper = mount(
        <Provider store={store}>
          <AccessFunctionGrid
            hasPrivilegesToCreate
            hasPrivilegeToDelete
            hasPrivilegeToView
            data={mockAccessFunctionsRowData}
            noResults={!mockAccessFunctionsRowData.length}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={accessFunctionsColumnDefs}
            accessFunctionsColumnDefsVersion={accessFunctionsColumnDefsVersion}
            autoGroupColumnDef={accessFunctionAutoGroupColumnDef}
            suppressAggFuncInHeader
            groupSelectsChildren
            suppressRowClickSelection
            enableGroupSelection
          />
        </Provider>,
      );

      axiosMock.onAny().reply(200);

      await ensureGridApiHasBeenSet(wrapper);
    });

    it('should click on Create New button and dispatch a the create new function passed in the component', async () => {
      await ensureGridApiHasBeenSet(wrapper);
      wrapper.find('.grid-container button.bp3-button').first().simulate('click');
      expect(handleCreateNew).toHaveBeenCalled();
    });

    it('should click on Refresh button and dispatch a the refresh function passed in the component', async () => {
      await ensureGridApiHasBeenSet(wrapper);
      wrapper.find('.grid-container button.bp3-button').at(1).simulate('click');
      expect(handleRefresh).toHaveBeenCalled();
    });

    it('should filter the AF grid according the input passed', async () => {
      // ALL AFs before filtering
      expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(11);

      const event = {
        currentTarget: {
          value: mockAccessFunctions[0].name,
        },
      } as React.ChangeEvent<HTMLInputElement>;

      const props = wrapper.find('.bp3-input').first().props();

      if (props.onChange != null) {
        props.onChange(event);
      }

      // some AFs after filtering
      expect(wrapper.render().find('.ag-center-cols-container .ag-row')).toHaveLength(1);
    });

    it('should render the last updated at', () => {
      expect(wrapper.render().find('.last-updated-at').text()).toEqual('Last updated at 08/27/20 07:13:39 AM');
    });
  });

  describe('When AF list input has data and user DONT has privileges to create', () => {
    let wrapper: ReactWrapper;

    const handleRefresh = jest.fn();
    const handleCreateNew = jest.fn();
    const handleSelection = jest.fn();
    const handleSaveGridSettings = jest.fn();
    const handleSelectItemMenuAction = jest.fn();
    const handleShowActionMenu = jest.fn();
    const handleGetGridSettingsFromServer = () => Promise.resolve(userSettings);

    beforeEach(async () => {
      wrapper = mount(
        <Provider store={store}>
          <AccessFunctionGrid
            hasPrivilegesToCreate={false}
            hasPrivilegeToDelete={false}
            hasPrivilegeToView
            data={mockAccessFunctionsRowData}
            noResults={!mockAccessFunctionsRowData.length}
            beforeLogout={false}
            onRefresh={handleRefresh}
            onCreateNew={handleCreateNew}
            onSelectionChanged={handleSelection}
            onSaveGridSettings={handleSaveGridSettings}
            onGetGridSettings={handleGetGridSettingsFromServer}
            onSelectItemMenuAction={handleSelectItemMenuAction}
            onShowActionMenuItem={handleShowActionMenu}
            lastUpdatedAt={new Date('Thu Aug 27 2020 11:13:39 GMT-0300')}
            columnDefs={accessFunctionsColumnDefs}
            autoGroupColumnDef={accessFunctionAutoGroupColumnDef}
            accessFunctionsColumnDefsVersion={accessFunctionsColumnDefsVersion}
            suppressAggFuncInHeader
            groupSelectsChildren
            suppressRowClickSelection
            enableGroupSelection
          />
        </Provider>,
      );

      await ensureGridApiHasBeenSet(wrapper);
    });

    it('should not render create new button and the first button in the interface should be the refresh', async () => {
      await ensureGridApiHasBeenSet(wrapper);
      wrapper.find('.grid-container button.bp3-button').first().simulate('click');
      expect(handleCreateNew).not.toHaveBeenCalled();
      expect(handleRefresh).toHaveBeenCalled();
    });
  });
});
