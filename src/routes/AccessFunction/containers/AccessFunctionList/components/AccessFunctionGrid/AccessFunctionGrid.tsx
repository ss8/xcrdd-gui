import React, { FunctionComponent } from 'react';
import { SelectionChangedEvent, ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import CommonGrid from 'shared/commonGrid';
import { ROWSELECTION } from 'shared/commonGrid/grid-enums';
import ActionMenuCellRenderer from 'shared/commonGrid/action-menu-cell-renderer';
import { AccessFunctionGridRowData } from 'data/accessFunction/accessFunction.types';
import { GridSettings } from 'data/types';
import NotApplicableCellRenderer from 'components/NotApplicableCellRenderer';
import { AccessFunctionMenuActions } from 'routes/AccessFunction/AccessFunction.types';
import AccessFunctionStatusCellRenderer from 'routes/AccessFunction/components/AccessFunctionStatusCellRenderer';
import AccessFunctionDestinationCellRenderer from 'routes/AccessFunction/components/AccessFunctionDestinationCellRenderer';
import AccessFunctionConfiguredStatusCellRenderer from 'routes/AccessFunction/components/AccessFunctionConfiguredStatusCellRenderer';
import AccessFunctionActionMenu from '../AccessFunctionActionMenu';

interface Props {
  data: AccessFunctionGridRowData[]
  hasPrivilegesToCreate: boolean
  hasPrivilegeToDelete: boolean
  hasPrivilegeToView: boolean
  noResults: boolean,
  beforeLogout: boolean,
  lastUpdatedAt: Date | undefined,
  columnDefs: (ColGroupDef | ColDef)[],
  autoGroupColumnDef: ColDef,
  suppressAggFuncInHeader: boolean,
  groupSelectsChildren: boolean,
  suppressRowClickSelection: boolean,
  enableGroupSelection: boolean,
  accessFunctionsColumnDefsVersion: string,
  onRefresh: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onCreateNew: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onGetGridSettings: (() => any) | undefined
  onSelectionChanged: (event: SelectionChangedEvent) => void
  onSaveGridSettings: (gridSettings: GridSettings) => void
  onSelectItemMenuAction: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  onShowActionMenuItem: (type: AccessFunctionMenuActions, accessFunctionTag: string) => void
}

const AccessFunctionGrid: FunctionComponent<Props> = ({
  hasPrivilegesToCreate,
  hasPrivilegeToDelete,
  hasPrivilegeToView,
  data = [],
  noResults,
  beforeLogout,
  lastUpdatedAt,
  columnDefs,
  autoGroupColumnDef,
  suppressAggFuncInHeader,
  groupSelectsChildren,
  suppressRowClickSelection,
  enableGroupSelection,
  accessFunctionsColumnDefsVersion,
  onCreateNew,
  onRefresh,
  onSelectionChanged,
  onSaveGridSettings,
  onGetGridSettings,
  onShowActionMenuItem,
  onSelectItemMenuAction,
}: Props) => {
  const frameworkComponents = {
    actionMenuCellRenderer: ActionMenuCellRenderer,
    notApplicableCellRenderer: NotApplicableCellRenderer,
    accessFunctionStatusCellRenderer: AccessFunctionStatusCellRenderer,
    accessFunctionDestinationCellRenderer: AccessFunctionDestinationCellRenderer,
    accessFunctionConfiguredStatusCellRenderer: AccessFunctionConfiguredStatusCellRenderer,
  };

  return (
    <div className="grid-container access-function-grid" style={{ height: 'inherit' }}>
      <CommonGrid
        createNewText="New Access Function"
        pagination
        enableBrowserTooltips
        rowSelection={ROWSELECTION.MULTIPLE}
        hasPrivilegeToCreateNew={hasPrivilegesToCreate}
        hasPrivilegeToDelete={hasPrivilegeToDelete}
        hasPrivilegetoView={hasPrivilegeToView}
        rowData={data}
        noResults={noResults}
        isBeforeLogout={beforeLogout}
        columnDefs={columnDefs}
        columnDefsVersion={accessFunctionsColumnDefsVersion}
        autoGroupColumnDef={autoGroupColumnDef}
        frameworkComponents={frameworkComponents}
        actionMenuClass={AccessFunctionActionMenu}
        handleCreateNew={onCreateNew}
        onRefresh={onRefresh}
        onSelectionChanged={onSelectionChanged}
        showActionMenuItem={onShowActionMenuItem}
        onActionMenuItemClick={onSelectItemMenuAction}
        saveGridSettings={onSaveGridSettings}
        getGridSettingsFromServer={onGetGridSettings}
        lastUpdatedAt={lastUpdatedAt}
        suppressAggFuncInHeader={suppressAggFuncInHeader}
        groupSelectsChildren={groupSelectsChildren}
        suppressRowClickSelection={suppressRowClickSelection}
        enableGroupSelection={enableGroupSelection}
      />
    </div>
  );
};

export default AccessFunctionGrid;
