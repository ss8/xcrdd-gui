import React from 'react';
import { render } from '@testing-library/react';
import TriggerActionDialogBody from './TriggerActionDialogBody';

describe('<TriggerActionDialogBody />', () => {
  it('should render the dialog body', () => {
    const { container } = render(
      <TriggerActionDialogBody>
        Do you want to trigger an audit on &#39;NAME&#39;?
      </TriggerActionDialogBody>,
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        Do you want to trigger an audit on 'NAME'?
        <br />
        <br />
        NOTE: This operation will run in background and the results will be displayed in the Alarm page.
      </div>
    `);
  });
});
