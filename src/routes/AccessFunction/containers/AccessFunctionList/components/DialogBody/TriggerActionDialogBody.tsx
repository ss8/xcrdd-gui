import React, { Fragment, ReactNode } from 'react';

type Props = {
  children: ReactNode,
}

const TriggerActionDialogBody = ({
  children,
}: Props): JSX.Element => (
  <Fragment>
    {children}
    <br />
    <br />
    NOTE: This operation will run in background and the results will be displayed in the Alarm page.
  </Fragment>
);

export default TriggerActionDialogBody;
