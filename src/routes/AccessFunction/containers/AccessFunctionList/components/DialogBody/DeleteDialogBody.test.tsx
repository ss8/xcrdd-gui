import React from 'react';
import { render } from '@testing-library/react';
import DeleteDialogBody from './DeleteDialogBody';

describe('<DeleteDialogBody />', () => {
  it('should render the dialog body', () => {
    const { container } = render(<DeleteDialogBody name="NAME" />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        Are you sure you want to delete Access Function '
        NAME
        '?
        <br />
        All the associated wiretaps will also be deleted.
      </div>
    `);
  });
});
