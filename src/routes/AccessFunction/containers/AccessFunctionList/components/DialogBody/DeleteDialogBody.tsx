import React, { Fragment } from 'react';
import { DialogBodyProps } from './DialogBody.types';

const DeleteDialogBody = ({ name }: DialogBodyProps): JSX.Element => (
  <Fragment>
    Are you sure you want to delete Access Function &#39;{name}&#39;?
    <br />
    All the associated wiretaps will also be deleted.
  </Fragment>
);

export default DeleteDialogBody;
