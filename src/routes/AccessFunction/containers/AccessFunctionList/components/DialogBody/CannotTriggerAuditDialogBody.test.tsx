import React from 'react';
import { render } from '@testing-library/react';
import CannotTriggerAuditDialogBody from './CannotTriggerAuditDialogBody';

describe('<CannotTriggerAuditDialogBody />', () => {
  it('should render the dialog body', () => {
    const { container } = render(<CannotTriggerAuditDialogBody name="NAME" />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        Access function '
        NAME
        ' cannot be audited.
        <br />
        <br />
        - Provisioning Interface (X1) not configured.
      </div>
    `);
  });
});
