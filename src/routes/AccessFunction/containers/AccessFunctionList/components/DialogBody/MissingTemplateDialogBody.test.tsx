import React from 'react';
import { render } from '@testing-library/react';
import MissingTemplateDialogBody from './MissingTemplateDialogBody';

describe('<MissingTemplateDialogBody />', () => {
  it('should render the dialog body', () => {
    const { container } = render(<MissingTemplateDialogBody name="NAME" />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        Unable to find Access Function template.
        <br />
        Please contact system administrator to check Discovery installation environment to ensure that the template for '
        NAME
        ' is there.
      </div>
    `);
  });
});
