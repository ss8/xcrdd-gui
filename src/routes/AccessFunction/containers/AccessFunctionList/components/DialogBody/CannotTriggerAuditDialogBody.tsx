import React, { Fragment } from 'react';
import { DialogBodyProps } from './DialogBody.types';

const CannotTriggerAuditDialogBody = ({ name }: DialogBodyProps): JSX.Element => (
  <Fragment>
    Access function &#39;{name}&#39; cannot be audited.
    <br />
    <br />
    - Provisioning Interface (X1) not configured.
  </Fragment>
);

export default CannotTriggerAuditDialogBody;
