import React, { Fragment } from 'react';
import { DialogBodyProps } from './DialogBody.types';

const MissingTemplateDialogBody = ({ name }: DialogBodyProps): JSX.Element => (
  <Fragment>
    Unable to find Access Function template.
    <br />
    Please contact system administrator to check Discovery installation environment
    to ensure that the template for &#39;{name}&#39; is there.
  </Fragment>
);

export default MissingTemplateDialogBody;
