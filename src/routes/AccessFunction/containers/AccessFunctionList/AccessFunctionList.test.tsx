import React from 'react';
import { Provider } from 'react-redux';
import MockAdapter from 'axios-mock-adapter';
import * as types from 'data/accessFunction/accessFunction.types';
import {
  RenderResult, render, screen, fireEvent, waitFor, act,
} from '@testing-library/react';
import { buildServerResponse, buildAxiosServerResponse } from '__test__/utils';
import cloneDeep from 'lodash.clonedeep';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { locations } from '__test__/locations';
import { mockTemplates } from '__test__/mockTemplates';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import store from 'data/store';
import Http from 'shared/http';
import { UserSettings } from 'data/types';
import AuthActions from 'data/authentication/authentication.types';
import { MemoryRouter } from 'react-router-dom';
import AccessFunction from 'routes/AccessFunction/AccessFunction';
import { mockAccessFunctions } from '__test__/mockAccessFunctionRowData';
import { AF_ROUTE } from '../../../../constants';
import AccessFunctionList from './AccessFunctionList';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionList />', () => {
  let accessFunction1: types.AccessFunction;
  let userSettings: UserSettings;

  beforeEach(() => {

    userSettings = {
      dateUpdated: '2020-07-28T17:08:46.000+0000',
      id: 'id1',
      name: 'AccessFunctionSettings',
      setting: '[]',
      type: 'json',
      user: 'userId1',
    };

    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['af_view', 'af_write', 'af_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse([]),
    });

    const response = buildServerResponse(mockAccessFunctions);
    axiosMock.onGet('/afs/0?$view=options').reply(200, response);

    const userSettingsResponse = buildServerResponse([userSettings]);
    axiosMock.onGet('/users/userId1/settings?name=AccessFunctionSettings').reply(200, userSettingsResponse);

    const accessFunctionResponse = buildServerResponse([mockAccessFunctions[1]]);
    axiosMock.onGet(`/afs/0/${mockAccessFunctions[0].id}`).reply(200, accessFunctionResponse);

    axiosMock.onGet('/session/timezones').reply(200, { data: [] });
    axiosMock.onGet('/contacts/0').reply(200, { data: [] });
    axiosMock.onGet('/networks/0').reply(200, { data: [] });
    axiosMock.onGet('/securityInfos/0').reply(200, { data: [] });
    axiosMock.onGet('/ssh-infos/0').reply(200, { data: [] });
    axiosMock.onGet('/vpn-configs/0').reply(200, { data: [] });
    axiosMock.onGet('/vpn-templates/0').reply(200, { data: [] });
    axiosMock.onPut('/users/userId1/settings').reply(200, { data: [] });
    axiosMock.onGet('/distributorConfigs/0').reply(200, { data: mockDistributorsConfiguration });
    axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    axiosMock.onGet('/locations').reply(200, { data: locations });
    axiosMock.onGet('/templates').reply(200, { data: mockTemplates });
    axiosMock.onGet('/afs/0/configuredAfs').reply(200, { data: [JSON.stringify(configuredAfs)] });
    axiosMock.onGet('/xtps/0').reply(200, { data: [] });
    axiosMock.onGet('/xdps/0').reply(200, { data: [] });
    axiosMock.onGet('/process-options/0').reply(200, { data: [] });
    axiosMock.onGet('/xcipioNodes/0').reply(200, { data: [] });
  });

  it('should render the empty list if no access functions', () => {
    const response = buildServerResponse([]);
    axiosMock.onGet('/afs/0?$view=options').reply(200, response);
    render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should render the list of access functions', async () => {

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionList />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

    expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(11);
  });

  it('should filter the list of access functions', async () => {

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionList />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

    expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(11);

    const filterInput = container.querySelector('[placeholder="Search"]') as HTMLElement;

    fireEvent.change(filterInput as Element, {
      target: {
        value: 'AF_ID_11',
      },
    } as React.ChangeEvent<HTMLInputElement>);

    expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(1);
  });

  it('should refresh the list of access functions', async () => {

    axiosMock.onGet('/afs/0?$view=options').reply(200, buildServerResponse([mockAccessFunctions[0]]));

    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionList />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

    expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(1);

    const refreshIcon = container.querySelector('[icon="refresh"]') as HTMLElement;
    axiosMock.onGet('/afs/0?$view=options').reply(200, buildServerResponse(mockAccessFunctions));

    act(() => {
      fireEvent.click(refreshIcon);
    });

    await waitFor(() => expect(container.querySelectorAll('.ag-group-value')[2]).toBeVisible());

    expect(container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(11);
  });

  it('should render the create access function when clicking in the create new button', async () => {
    const { container } = render((
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>
    ));

    await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

    const newAccessFunctionButton = screen.getByText('New Access Function') as HTMLElement;

    act(() => {
      fireEvent.click(newAccessFunctionButton);
    });

    expect(screen.getByTestId('AccessFunctionForm')).toBeVisible();
  });

  describe('when clicking to edit', () => {
    it('should render the edit access function', async () => {
      const { container } = render((
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
            <AccessFunction />
          </MemoryRouter>
        </Provider>
      ));

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());
      const moreButtons = (await screen.findAllByTitle('View available actions')) as HTMLElement[];

      act(() => {
        fireEvent.click(moreButtons[0]);
      });

      const editMenuItem = await screen.findByText('Edit') as HTMLElement;

      act(() => {
        fireEvent.click(editMenuItem.parentElement as Element);
      });

      await waitFor(() => expect(screen.getByText('Edit Access Function')).toBeVisible());

      expect(screen.getByTestId('AccessFunctionForm')).toBeVisible();
    });

    it('should show a popup if the template is not available', async () => {

      const templates = mockTemplates.filter((template) => template.name !== 'Alcatel-Lucent Distributed Soft Switch');

      axiosMock.onGet('/templates').reply(200, { data: templates });
      const { container } = render((

        <Provider store={store}>
          <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
            <AccessFunction />
          </MemoryRouter>
        </Provider>
      ));

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());
      const moreButtons = (await screen.findAllByTitle('View available actions')) as HTMLElement[];

      act(() => {
        fireEvent.click(moreButtons[0]);
      });

      const editMenuItem = await screen.findByText('Edit') as HTMLElement;

      act(() => {
        fireEvent.click(editMenuItem.parentElement as Element);
      });

      expect(screen.getByText('Edit Access Function - Missing Template')).toBeVisible();
      fireEvent.click(screen.getByText('Close'));
      await waitFor(() => expect(screen.queryAllByText('Edit Access Function - Missing Template')).toHaveLength(0));
    });
  });

  describe('when clicking to view', () => {
    it('should render the view access function', async () => {
      const { container } = render((
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
            <AccessFunction />
          </MemoryRouter>
        </Provider>
      ));

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());
      const moreButtons = await screen.findAllByTitle('View available actions') as HTMLElement[];

      act(() => {
        fireEvent.click(moreButtons[0]);
      });

      const editMenuItem = await screen.findByText('View') as HTMLElement;

      act(() => {
        fireEvent.click(editMenuItem.parentElement as Element);
      });

      await waitFor(() => expect(screen.getByText('View Access Function')).toBeVisible());

      expect(screen.getByTestId('AccessFunctionForm')).toBeVisible();
    });

    it('should show a popup if the template is not available', async () => {
      const templates = mockTemplates.filter((template) => template.name !== 'Alcatel-Lucent Distributed Soft Switch');

      axiosMock.onGet('/templates').reply(200, { data: templates });
      const { container } = render((

        <Provider store={store}>
          <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
            <AccessFunction />
          </MemoryRouter>
        </Provider>
      ));

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());
      const moreButtons = await screen.findAllByTitle('View available actions') as HTMLElement[];

      act(() => {
        fireEvent.click(moreButtons[0]);
      });

      const editMenuItem = await screen.findByText('View') as HTMLElement;

      act(() => {
        fireEvent.click(editMenuItem.parentElement as Element);
      });

      expect(screen.getByText('View Access Function - Missing Template')).toBeVisible();
      fireEvent.click(screen.getByText('Close'));
      await waitFor(() => expect(screen.queryAllByText('View Access Function - Missing Template')).toHaveLength(0));
    });
  });

  describe('when clicking to copy', () => {
    it('should render the copy access function', async () => {
      const { container } = render((
        <Provider store={store}>
          <MemoryRouter initialEntries={[`${AF_ROUTE}`]}>
            <AccessFunction />
          </MemoryRouter>
        </Provider>
      ));

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());
      const moreButtons = await screen.findAllByTitle('View available actions') as HTMLElement[];

      act(() => {
        fireEvent.click(moreButtons[0]);
      });

      const editMenuItem = await screen.findByText('Copy') as HTMLElement;

      act(() => {
        fireEvent.click(editMenuItem.parentElement as Element);
      });

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

      expect(screen.getByTestId('AccessFunctionForm')).toBeVisible();
    });
  });

  describe('Delete AF feature - Successful operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];
    let afterDeleteAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];
      afterDeleteAfList = [mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onDelete(`/afs/0/${initialAfList[0].id}`).reply(200, response);
      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select and delete an Access Function from AF list', async () => {

      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      expect(afListWrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(initialAfList.length);

      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      const actionsButtons = await screen.findAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Delete action menu option
      const deleteMenuButtonEl = await waitFor(() => afListWrapper.getByText('Delete'));

      // Act: Click Delete option in the AF item Action menu
      fireEvent.click(deleteMenuButtonEl);

      // Waits: confirm delete dialog
      expect(screen.getByText('Delete Access Function')).toBeVisible();

      // re Mock afs list response after delete, to simulate a successfully delete operation
      const responseDel = buildServerResponse(afterDeleteAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, responseDel);

      // Act: when confirm delete dialog became rendered, click delete button
      fireEvent.click(screen.getByText('Yes, delete it'));

      // waits delete operation and refresh the AF list from server
      await new Promise((resolve) => setTimeout(resolve, 500));

      // waits for the success feedback component
      const feedbackToastMsg = await waitFor(() => afListWrapper.getAllByText(`${initialAfList[0]?.name} successfully deleted.`));

      // Success feedback component must be rendered
      expect(feedbackToastMsg?.length).toBe(1);
      // AF list must show the new AF list items
      expect(afListWrapper.container.querySelectorAll('.ag-center-cols-container .ag-row').length).toBe(afterDeleteAfList.length);
      expect(afListWrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(afterDeleteAfList.length);
    });
  });

  describe('Delete AF feature - Failure operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onDelete(`/afs/0/${mockAccessFunctions[0].id}`).reply(500, { data: [] });

      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select an Access Function but not delete it and keep the AF list state as the init state', async () => {
      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      expect(afListWrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(initialAfList.length);

      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      const actionsButtons = await screen.findAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Delete action menu option
      const deleteMenuButtonEl = await waitFor(() => afListWrapper.getByText('Delete'));

      // Act: Click Delete option in the AF item Action menu
      fireEvent.click(deleteMenuButtonEl);

      // Waits: confirm delete dialog
      expect(screen.getByText('Delete Access Function')).toBeVisible();

      // Act: when confirm delete dialog became rendered, click delete button
      fireEvent.click(screen.getByText('Yes, delete it'));

      await waitFor(() => expect(screen.getByText(`Unable to delete ${mockAccessFunctions[0]?.name}. Try again later.`)).toBeVisible());
    });
  });

  describe('Delete AF feature - Cancel operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];
    let afterDeleteAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];
      afterDeleteAfList = [mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select an AF to delete but cancel the delete operation by clicking Cancel in the confirmation dialog', async () => {
      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      expect(afListWrapper.container.querySelectorAll('.ag-center-cols-container .ag-row')).toHaveLength(initialAfList.length);

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(initialAfList.length));
      const actionsButtons = screen.getAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Delete action menu option
      const deleteMenuButtonEl = await waitFor(() => afListWrapper.getByText('Delete'));

      // Act: Click Delete option in the AF item Action menu
      fireEvent.click(deleteMenuButtonEl);

      // Waits: confirm delete dialog
      expect(screen.getByText('Delete Access Function')).toBeVisible();

      // re Mock afs list response after delete, to simulate a successfully delete operation
      const responseDel = buildServerResponse(afterDeleteAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, responseDel);

      // Act: when confirm delete dialog became rendered, click Cancel button
      fireEvent.click(screen.getByText('Cancel'));

      expect(afListWrapper.container.querySelectorAll('.bp3-dialog-body').length).toBe(0);
    });
  });

  describe('Audit AF feature - Successful operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onPut(`/afs/0/${mockAccessFunctions[0].id}/action`).reply(200, response);
      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select and Audit an Access Function from AF list', async () => {
      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(initialAfList.length));
      const actionsButtons = screen.getAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Audit action menu option
      const auditMenuButtonEl = await waitFor(() => afListWrapper.getByText('Audit'));

      // Act: Click Audit option in the AF item Action menu
      fireEvent.click(auditMenuButtonEl);

      // Waits: confirm Audit dialog
      const confirmAuditDialogMsgEl = await screen.findByText('Audit Access Function');

      // Test: Check if the confirm Audit dialog is rendered
      expect(confirmAuditDialogMsgEl).toBeVisible();

      // Act: when confirm audit dialog became rendered, click Audit button
      fireEvent.click(screen.getByText('Yes, audit it'));

      // waits Audit operation and refresh the AF list from server
      await new Promise((resolve) => setTimeout(resolve, 500));

      // Success feedback component must be rendered
      expect(await screen.findByText(`Audit on ${mockAccessFunctions[0]?.name} successfully triggered. Check Alarms for result.`)).toBeVisible();
    });
  });

  describe('Audit AF feature - Failure operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onPut(`/afs/0/${mockAccessFunctions[0].id}/action`).reply(500, { data: [] });

      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select an Access Function but not Audit it because server sends an error', async () => {
      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(initialAfList.length));
      const actionsButtons = screen.getAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Audit action menu option
      const auditMenuButtonEl = await waitFor(() => afListWrapper.getByText('Audit'));

      // Act: Click Audit option in the AF item Action menu
      fireEvent.click(auditMenuButtonEl);

      // Waits: confirm Audit dialog
      const confirmAuditDialogMsgEl = await screen.findByText('Audit Access Function');

      // Test: Check if the confirm Audit dialog is rendered
      expect(confirmAuditDialogMsgEl).toBeVisible();

      // Act: when confirm audit dialog became rendered, click Audit button
      fireEvent.click(screen.getByText('Yes, audit it'));

      // waits Audit operation and refresh the AF list from server
      await new Promise((resolve) => setTimeout(resolve, 500));

      // Success feedback component must be rendered
      expect(await screen.findByText(`Unable to trigger audit on ${mockAccessFunctions[0]?.name}. Try again later.`)).toBeVisible();
    });
  });

  describe('Audit AF feature - Cancel operation', () => {
    let afListWrapper: RenderResult;
    let initialAfList: types.AccessFunction[] = [];

    beforeEach(() => {
      initialAfList = [mockAccessFunctions[0], mockAccessFunctions[1], mockAccessFunctions[2]];

      const response = buildServerResponse(initialAfList);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      afListWrapper = render(<Provider store={store}><MemoryRouter><AccessFunctionList /></MemoryRouter></Provider>);
    });

    it('should select an AF to Audit but cancel the Audit operation by clicking Cancel in the confirmation dialog', async () => {
      await waitFor(() => expect(afListWrapper.container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(initialAfList.length));
      const actionsButtons = screen.getAllByTitle('View available actions');

      fireEvent.click(actionsButtons[0]);

      // Wait: Action Menu and select the Audit action menu option
      const auditMenuButtonEl = await waitFor(() => afListWrapper.getByText('Audit'));

      // Act: Click Audit option in the AF item Action menu
      fireEvent.click(auditMenuButtonEl as Element);

      // Waits: confirm Audit dialog
      const confirmAuditDialogMsgEl = await screen.findByText('Audit Access Function');

      // Test: Check if the confirm Audit dialog is rendered
      expect(confirmAuditDialogMsgEl).toBeVisible();

      // Act: when confirm audit dialog became rendered, click Audit button
      fireEvent.click(screen.getByText('Cancel'));

      // Test: Confirm dialog is gone
      expect(afListWrapper.container.querySelectorAll('.bp3-dialog-body').length).toBe(0);
    });
  });

  describe('Reset action', () => {
    beforeEach(() => {
      accessFunction1 = {
        id: 'ID_TO_GET',
        name: 'ALU DSS',
        description: '',
        timeZone: '',
        type: 'ALU_DSS',
        tag: 'ALU_DSS',
        version: '',
        insideNat: false,
        liInterface: 'ALL',
        model: '',
        preprovisioningLead: '000:00',
        serialNumber: '',
        provisioningInterfaces: [{
          id: 'id1',
          name: '',
          options: [],
        }],
        dataInterfaces: [],
      };

      const response = buildServerResponse([accessFunction1, mockAccessFunctions[1]]);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    });

    it('should not show the menu options for all AFs', async () => {
      const newMockSupportedFeatures = cloneDeep(mockSupportedFeatures);
      newMockSupportedFeatures.config.accessFunctions.ALU_DSS.supportedActions = [];

      axiosMock.onGet('/config/supported-features/0').reply(200, { data: [newMockSupportedFeatures] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[1]);

      await waitFor(() => screen.getByText('Audit'));
      expect(screen.queryAllByText('Reset')).toHaveLength(0);

    });

    it('should show a confirmation popup', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );
      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Reset'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Reset Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Cancel'));

      await waitFor(() => expect(screen.queryAllByText('Reset Access Function')).toHaveLength(0));
    });

    it('should show a success message on success', async () => {
      axiosMock.onPut(`/afs/0/${accessFunction1.id}/action`).reply(200, { data: [] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Reset'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Reset Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, reset it'));

      expect(await screen.findByText(`Reset on ${accessFunction1?.name} successfully triggered. Check Alarms for result.`)).toBeVisible();
    });

    it('should show an error message on failure', async () => {
      axiosMock.onPut(`/afs/0/${accessFunction1.id}/action`).reply(500, { data: [] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Reset'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Reset Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, reset it'));

      await waitFor(() => expect(screen.getByText(`Unable to trigger reset on ${accessFunction1?.name}. Try again later.`)).toBeVisible());
    });
  });

  describe('Refresh action', () => {
    beforeEach(() => {
      accessFunction1 = {
        id: 'ID_TO_GET',
        name: 'ALU DSS',
        description: '',
        timeZone: '',
        type: 'ALU_DSS',
        tag: 'ALU_DSS',
        version: '',
        insideNat: false,
        liInterface: 'ALL',
        model: '',
        preprovisioningLead: '000:00',
        serialNumber: '',
        provisioningInterfaces: [{
          id: 'id1',
          name: '',
          options: [],
        }],
        dataInterfaces: [],
      };

      const response = buildServerResponse([accessFunction1, mockAccessFunctions[1]]);
      axiosMock.onGet('/afs/0?$view=options').reply(200, response);
      axiosMock.onGet('/config/supported-features/0').reply(200, { data: [mockSupportedFeatures] });
    });

    it('should not show the menu options for all AFs', async () => {
      const newMockSupportedFeatures = cloneDeep(mockSupportedFeatures);
      newMockSupportedFeatures.config.accessFunctions.ALU_DSS.supportedActions = [];

      axiosMock.onGet('/config/supported-features/0').reply(200, { data: [newMockSupportedFeatures] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[1]);

      await waitFor(() => screen.getByText('Audit'));
      expect(screen.queryAllByText('Refresh')).toHaveLength(0);
    });

    it('should show a confirmation popup', async () => {
      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Refresh'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Refresh Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Cancel'));

      await waitFor(() => expect(screen.queryAllByText('Refresh Access Function')).toHaveLength(0));
    });

    it('should show a success message on success', async () => {
      axiosMock.onPut(`/afs/0/${accessFunction1.id}/action`).reply(200, { data: [] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Refresh'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Refresh Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, refresh it'));

      expect(await screen.findByText(`Refresh on ${accessFunction1?.name} successfully triggered. Check Alarms for result.`)).toBeVisible();
    });

    it('should show an error message on failure', async () => {
      axiosMock.onPut(`/afs/0/${accessFunction1.id}/action`).reply(500, { data: [] });

      const { container } = render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionList />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(container.querySelector('.ag-group-value')).toBeVisible());

      await waitFor(() => expect(screen.getAllByTitle('View available actions')).toHaveLength(2));
      const actionsButtons = screen.getAllByTitle('View available actions');
      fireEvent.click(actionsButtons[0]);

      const auditMenuButtonEl = await waitFor(() => screen.getByText('Refresh'));
      fireEvent.click(auditMenuButtonEl);

      await waitFor(() => expect(screen.getByText('Refresh Access Function')).toBeVisible());

      fireEvent.click(screen.getByText('Yes, refresh it'));

      await waitFor(() => expect(screen.getByText(`Unable to trigger refresh on ${accessFunction1?.name}. Try again later.`)).toBeVisible());
    });

  });
});
