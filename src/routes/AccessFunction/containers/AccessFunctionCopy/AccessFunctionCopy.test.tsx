import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import Http from 'shared/http';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from 'data/store';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  getByTestId,
  getByLabelText,
} from '@testing-library/react';
import AccessFunction from 'routes/AccessFunction/AccessFunction';
import AuthActions from 'data/authentication/authentication.types';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { locations } from '__test__/locations';
import { mockTemplates } from '__test__/mockTemplates';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockNetworks } from '__test__/mockNetworks';
import { configuredAfs } from '__test__/mockConfiguredAccessFunctions';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockAccessFunctions, mockAccessFunctionsUPF, mockAccessFunctionsSMF } from '__test__/mockAccessFunctions';
import * as types from 'data/accessFunction/accessFunction.types';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import AccessFunctionCopy from './AccessFunctionCopy';
import { AF_ROUTE } from '../../../../constants';

const http = Http.getHttp();
const axiosMock = new MockAdapter(http);

describe('<AccessFunctionCopy />', () => {
  let accessFunction: types.AccessFunction;

  beforeEach(() => {
    const checkTokenResponse = {
      userID: 'userId1',
      privileges: ['af_view', 'af_write', 'af_delete'],
    };
    store.dispatch({
      type: AuthActions.CHECK_TOKEN_FULFILLED,
      payload: buildAxiosServerResponse([checkTokenResponse]),
    });

    store.dispatch({
      type: types.SELECT_ACCESS_FUNCTION,
      payload: [{ id: 'ID_TO_GET' }],
    });

    store.dispatch({
      type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
      payload: buildAxiosServerResponse([mockSupportedFeatures]),
    });

    store.dispatch({
      type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
      payload: buildAxiosServerResponse(mockTemplates),
    });

    accessFunction = {
      id: 'ID_TO_GET',
      name: 'AF Name to Copy',
      description: 'some description',
      contact: { id: 'Q29udGFjdCAy', name: 'Contact 2' },
      insideNat: false,
      timeZone: 'America/Los_Angeles',
      type: 'ERK_UPF',
      tag: 'ERK_UPF',
      country: 'US',
      stateName: 'Alaska',
      city: 'Adak',
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
      vpnConfig: [],
    };

    store.dispatch({
      type: types.GET_ALL_AFS_WITH_VIEW_OPTIONS_FULFILLED,
      payload: buildAxiosServerResponse([...mockAccessFunctions, accessFunction]),
    });

    axiosMock.onAny().reply((config) => {
      if (config.data && config.data.indexOf('AF Name to fail') !== -1) {
        return [500, { data: [] }];
      }

      if (config.url === '/afs/0/ID_TO_GET') {
        return [200, { data: [accessFunction] }];
      }

      if (config.url === '/session/timezones') {
        return [200, {
          data: [{
            zones: [
              { value: 'America/New_York', label: 'US/Eastern' },
              { value: 'America/Los_Angeles', label: 'US/Pacific' },
            ],
          }],
        }];
      }

      if (config.url === '/contacts/0') {
        return [200, { data: [{ id: 'Q29udGFjdCAx', name: 'Contact 1' }, { id: 'Q29udGFjdCAy', name: 'Contact 2' }] }];
      }

      if (config.url === '/distributorConfigs/0?$filter=afType+eq+%27ERK_UPF%27&$view=listeners') {
        return [200, { data: [] }];
      }

      if (config.url === '/config/supported-features/0') {
        return [200, { data: [mockSupportedFeatures] }];
      }

      if (config.url === '/locations') {
        return [200, { data: locations }];
      }

      if (config.url === '/templates') {
        return [200, { data: mockTemplates }];
      }

      if (config.url === '/securityInfos/0') {
        return [200, { data: mockSecurityInfos }];
      }

      if (config.url === '/networks/0') {
        return [200, { data: mockNetworks }];
      }

      if (config.url === '/afs/0/configuredAfs') {
        return [200, { data: [JSON.stringify(configuredAfs)] }];
      }

      if (config.url === '/xtps/0') {
        return [200, { data: mockXtps }];
      }

      if (config.url === '/xdps/0') {
        return [200, { data: mockXdps }];
      }

      return [200, { data: [] }];
    });
  });

  it('should render the form title with Cancel and Save buttons', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );
    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the list of access function when clicking in the cancel button', () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[`${AF_ROUTE}/Copy`]}>
          <AccessFunction />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(screen.getByText('New Access Function')).toBeVisible();
  });

  it('should load the data retrieved from the server for ERK_UPF', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    expect(screen.getByLabelText('Name:*')).toHaveValue('Copy of AF Name to Copy');
    expect(screen.getByLabelText('Description:')).toHaveValue('some description');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson UPF');
    expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

    expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
    expect(screen.getByLabelText('Country:')).toBeEnabled();
    expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
    expect(screen.getByLabelText('State/Province:')).toBeEnabled();
    expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
    expect(screen.getByLabelText('City:')).toBeEnabled();
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);
  });

  it('should load the data retrieved from the server for ERK_SMF', async () => {
    accessFunction = {
      ...accessFunction,
      type: 'ERK_SMF',
      tag: 'ERK_SMF',
      provisioningInterfaces: [{
        id: 'eEFGXzU4NjMwMjY2NiMx',
        name: 'eEFGXzU4NjMwMjY2NiMx',
        options: [
          { key: 'afid', value: 'xAF_586302666' },
          { key: 'secinfoid', value: 'VExTMTJfQ0xJRU5U' },
          { key: 'reqState', value: 'ACTIVE' },
          { key: 'state', value: 'ACTIVE' },
          { key: 'ownnetid', value: 'WENQcmNz' },
          { key: 'ifid', value: '1' },
          { key: 'destPort', value: '123' },
          { key: 'dscp', value: '32' },
          { key: 'destIPAddr', value: '1.1.1.1' },
        ],
      }],
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
    expect(screen.getByLabelText('Name:*')).toHaveValue('Copy of AF Name to Copy');
    expect(screen.getByLabelText('Description:')).toHaveValue('some description');
    expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Ericsson SMF');
    expect(screen.getByLabelText('Xcipio is behind NAT')).not.toBeChecked();

    expect(screen.getByLabelText('Contact:')).toHaveDisplayValue('Contact 2');

    expect(screen.getByLabelText('Country:')).toHaveDisplayValue('US');
    expect(screen.getByLabelText('Country:')).toBeEnabled();
    expect(screen.getByLabelText('State/Province:')).toHaveDisplayValue('Alaska');
    expect(screen.getByLabelText('State/Province:')).toBeEnabled();
    expect(screen.getByLabelText('City:')).toHaveDisplayValue('Adak');
    expect(screen.getByLabelText('City:')).toBeEnabled();
    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/US\/Pacific \(GMT-0[78]\)/);

    const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');

    expect(getByLabelText(provInterface, 'AF IP Address:*')).toHaveValue('1.1.1.1');
    expect(getByLabelText(provInterface, 'AF Port:*')).toHaveValue('123');
    expect(getByLabelText(provInterface, 'Configured State:')).toHaveDisplayValue('Active');

    expect(getByLabelText(provInterface, 'Network ID:*')).toHaveDisplayValue('XCPrcs - 11.11.11.11');
    expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('TLS12_CLIENT');

    fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

    expect(getByLabelText(provInterface, 'DSCP (0 - 63):*')).toHaveValue('32');
  });

  it('should reset the city selection when changing the state and then be able to select new city', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: 'California' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    await waitFor(() => expect(screen.getByDisplayValue('Select a city')).toBeEnabled());

    fireEvent.change(screen.getAllByDisplayValue('Select a city')[0], { target: { value: 'San Francisco' } });
    expect(screen.getByDisplayValue('San Francisco')).toBeVisible();
  });

  it('should disable the city when unselecting the state', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('Alaska')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should disable the state and city when unselecting the country', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: '' } });
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeDisabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should show the state as enabled when only country loaded', async () => {
    accessFunction = {
      ...accessFunction,
      stateName: '',
      city: '',
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    expect(screen.getByDisplayValue('US')).toBeVisible();
    expect(screen.getByDisplayValue('US')).toBeEnabled();
    expect(screen.getByDisplayValue('Select a state/province')).toBeVisible();
    expect(screen.getByDisplayValue('Select a state/province')).toBeEnabled();
    expect(screen.getByDisplayValue('Select a city')).toBeVisible();
    expect(screen.getByDisplayValue('Select a city')).toBeDisabled();
  });

  it('should show the timezone even if it is not available in the list', async () => {
    accessFunction.timeZone = 'America/Tijuana';

    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());

    expect(screen.getByLabelText('Time Zone:*')).toHaveDisplayValue(/America\/Tijuana/);
  });

  it('should show the list of SS8 Distributors Configuration as an empty table', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should validate fields on Save', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: '' } });

    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(screen.getAllByText('Please enter a value.')[0]).toBeVisible();
  });

  it('should show success message on Save of a valid form', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: 'AF Name 1' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    expect(screen.getByDisplayValue('AF Name 1')).toBeVisible();
    expect(screen.getByDisplayValue('US')).toBeVisible();

    fireEvent.click(screen.getAllByText('Save')[0]);

    await waitFor(() => expect(screen.getByText('Access Function successfully created.')).toBeVisible());
  });

  it('should show error message on Save if server returns an error', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: 'AF Name to fail' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    expect(screen.getByDisplayValue('AF Name to fail')).toBeVisible();
    expect(screen.getByDisplayValue('US')).toBeVisible();

    fireEvent.click(screen.getAllByText('Save')[0]);
    await waitFor(() => expect(screen.getByText('Unable to create Access Function: Request failed with status code 500. Try again later.')).toBeVisible());
  });

  it('should show error message on Save if copy Access Function name is duplicated', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: mockAccessFunctionsUPF[1].name } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show error message on Save if copy Access Function name is same as original Access Function', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: accessFunction.name } });
    fireEvent.change(screen.getAllByDisplayValue('Ericsson UPF')[0], { target: { value: 'ERK_UPF' } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  it('should show error message on Save if edit Access Function name is duplicated even to different types', async () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <AccessFunctionCopy />
        </MemoryRouter>
      </Provider>,
    );

    fireEvent.change(screen.getAllByDisplayValue('Copy of AF Name to Copy')[0], { target: { value: mockAccessFunctionsSMF[1].name } });
    fireEvent.change(screen.getAllByDisplayValue('US')[0], { target: { value: 'US' } });

    fireEvent.click(screen.getAllByText('Save')[0]);

    expect(await screen.findByText('An AF with this name already exists.')).toBeVisible();
  });

  describe('MVNR_PGW', () => {
    it('should set the sec info as required and transport as HTTP when uri is set to http:', async () => {
      accessFunction = {
        ...accessFunction,
        type: 'MVNR_PGW',
        tag: 'MVNR_PGW',
        contentDeliveryVia: 'directFromAF',
        provisioningInterfaces: [{
          id: '1',
          name: 'name1',
          options: [
            { key: 'appProtocol', value: 'MVRN_UAG_PGW' },
            { key: 'ownnetid', value: 'U1M4XzMwNzA' },
            { key: 'reqState', value: 'ACTIVE' },
            { key: 'state', value: 'INACTIVE' },
            { key: 'dscp', value: '63' },
            { key: 'litype', value: 'INI1' },
            { key: 'keepaliveinterval', value: '60' },
            { key: 'litargetnamespace', value: 'http://mavenir.net/li/' },
            { key: 'secinfoid', value: 'Tk9ORQ' },
            { key: 'transport', value: 'HTTPS' },
            { key: 'uri', value: 'https://domain.com' },
            { key: 'version', value: '1.0' },
          ],
        }],
      };

      render(
        <Provider store={store}>
          <MemoryRouter>
            <AccessFunctionCopy />
          </MemoryRouter>
        </Provider>,
      );

      await waitFor(() => expect(screen.getByText('New Access Function')).toBeVisible());
      expect(screen.getByLabelText('AF Type:*')).toHaveDisplayValue('Mavenir PGW');

      const provInterface = screen.getByTestId('FormSection-provisioningInterfaces');
      fireEvent.click(getByTestId(provInterface, 'CollapsibleFormButton'));

      expect(getByLabelText(provInterface, 'AF URL:*')).toHaveDisplayValue('https://domain.com');
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):*')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTPS');

      fireEvent.change(getByLabelText(provInterface, 'AF URL:*'), { target: { value: 'http://domain.com' } });
      expect(getByLabelText(provInterface, 'TLS Profile (Security Info ID):')).toHaveDisplayValue('None');
      expect(getByLabelText(provInterface, 'Transport:')).toHaveDisplayValue('HTTP');
    });
  });
});
