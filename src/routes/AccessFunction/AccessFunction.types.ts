import {
  ColDef, ValueFormatterParams,
} from '@ag-grid-enterprise/all-modules';
import { COMMON_GRID_COLUMN_FIELD_NAMES } from 'shared/commonGrid/grid-enums';
import { stateValueFormatter } from 'utils/valueFormatters';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import {
  distributorConfigurationGroupFilterValueGetter,
  allowedPoiIpAddressesFilterValueGetter,
} from './utils/accessFunctionValueGetters';
import AllowedAccessFunctionIpCell from './components/DistributorConfigurationTable/AllowedAccessFunctionIpCell';
import AccessFunctionGroupHeader from './components/AccessFunctionGroupHeader';

export const NETWORK_ID_NOT_FOUND = 'NETWORK_ID_NOT_FOUND';

export enum AccessFunctionMenuActions {
  Copy = 'copy',
  Delete = 'delete',
  Edit = 'edit',
  View = 'view',
  Audit = 'audit',
  Reset = 'reset',
  Refresh = 'refresh',
}

export type AccessFunctionIpType = {
  allowedAfIps: string;
  listenerIp: string;
  listenerPort: string;
  name: string;
};

export type AllowedIpType = {
  poiName: string;
  poiIpAddress: string;
  actions: string;
};

/**
 * This value must be set in common-grid columnDefsVersion property. So, updating this value
 * forces common-grid to use the in code columnDefs instead of the saved grid setting.
 */
export const accessFunctionsColumnDefsVersion = '1';

export const accessFunctionsColumnDefs: ColDef[] = [
  {
    headerName: '',
    field: COMMON_GRID_COLUMN_FIELD_NAMES.Action,
    cellRenderer: 'actionMenuCellRenderer',
  },
  {
    field: 'name',
    rowGroup: true,
    hide: true,
    lockVisible: true,
  },
  {
    headerName: 'AF Type',
    field: 'tagName',
    tooltipField: 'tagName',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 204,
    aggFunc: 'first',
    tooltipValueGetter: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }

      return '';
    },
    cellRenderer: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }

      return '';
    },
  },
  {
    headerName: 'AF ID',
    field: 'afid',
    sortable: true,
    filter: 'agTextColumnFilter',
    tooltipField: 'afid',
    resizable: true,
    flex: 1,
    minWidth: 150,
    aggFunc: 'first',
    tooltipValueGetter: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }

      return '';
    },
    cellRenderer: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return params.value;
      }

      return '';
    },
  },
  {
    headerName: 'Status',
    field: 'state',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 132,
    valueFormatter: stateValueFormatter,
    tooltipValueGetter: stateValueFormatter,
    cellRenderer: 'accessFunctionStatusCellRenderer',
  },
  {
    headerName: 'Interface Type',
    field: 'subType',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    tooltipField: 'subType',
    flex: 1,
    minWidth: 120,
  },
  {
    headerName: 'AF IP Address/URL',
    field: 'ipAddress',
    tooltipField: 'ipAddress',
    filter: 'agTextColumnFilter',
    sortable: true,
    resizable: true,
    flex: 1,
    minWidth: 151,
    cellRenderer: 'notApplicableCellRenderer',
  },
  {
    headerName: 'IP Port',
    field: 'port',
    tooltipField: 'port',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 97,
    cellRenderer: 'notApplicableCellRenderer',
  },
  {
    headerName: 'Destination',
    field: 'destination',
    sortable: true,
    filter: 'agTextColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 178,
    tooltipValueGetter: (params: Partial<ValueFormatterParams>): string => {
      if (params.node?.group) {
        return '';
      }

      return params.data?.destinationToolTip;
    },
    cellRenderer: 'accessFunctionDestinationCellRenderer',
  },
  {
    headerName: 'Configured State',
    field: 'configuredState',
    sortable: true,
    filter: 'agSetColumnFilter',
    resizable: true,
    flex: 1,
    minWidth: 200,
    valueFormatter: stateValueFormatter,
    tooltipValueGetter: stateValueFormatter,
    cellRenderer: 'accessFunctionConfiguredStatusCellRenderer',
  },
];

export const accessFunctionAutoGroupColumnDef: ColDef = {
  headerName: 'Name',
  sortable: true,
  filter: 'agTextColumnFilter',
  resizable: true,
  flex: 1,
  minWidth: 220,
  headerComponentFramework: AccessFunctionGroupHeader,
  filterValueGetter: (params: Partial<ValueFormatterParams>): string => {
    return params.data?.name;
  },

};

export const distributorConfigurationTableColumnDefs: ColDef[] = [
  {
    headerName: 'SS8 Distributors Configuration',
    field: 'name',
    flex: 1,
    rowGroup: true,
    hide: true,
    filter: 'agTextColumnFilter',
  },
  {
    headerName: 'Listening IP',
    field: 'ipAddress',
    flex: 1,
    filter: 'agTextColumnFilter',
  },
  {
    headerName: 'Listening Port',
    field: 'port',
    flex: 1,
    filter: 'agTextColumnFilter',
  },
  {
    headerName: 'Allowed AF IP Addresses',
    field: 'afWhitelistFormatted',
    flex: 1,
    cellRendererFramework: AllowedAccessFunctionIpCell,
    filter: 'agTextColumnFilter',
    filterValueGetter: allowedPoiIpAddressesFilterValueGetter,
  },
];

export const distributorConfigurationTableAutoGroupColumnDef: ColDef = {
  filter: 'agTextColumnFilter',
  filterValueGetter: distributorConfigurationGroupFilterValueGetter,
};

export const distributorsIPsWhiteListEditableTableColumnDefs: ColDef[] = [
  {
    headerName: 'AF Name',
    field: 'afName',
    flex: 2,
    filter: 'agTextColumnFilter',
    editable: true,
    cellRenderer: 'ipAFNameCellRenderer',
  },
  {
    headerName: 'AF IP Address',
    field: 'afAddress',
    flex: 2,
    filter: 'agTextColumnFilter',
    editable: true,
    cellRenderer: 'ipAddressCellRenderer',
  },
  {
    headerName: 'Actions',
    field: 'actions',
    filter: false,
    cellClass: 'allowed-ips-update-table-center-cell',
    headerClass: 'allowed-ips-update-table-action-header',
    cellRenderer: 'actionCellRenderer',
  },
];

export const distributorsIPsWhiteListReadOnlyTableColumnDefs: ColDef[] = [
  {
    headerName: 'AF Name',
    field: 'afName',
    flex: 2,
    filter: 'agTextColumnFilter',
  },
  {
    headerName: 'AF IP Address',
    field: 'afAddress',
    flex: 2,
    filter: 'agTextColumnFilter',
  },
];

export type AFWhiteListVersioned = {
  rowKey: number;
  actions?: string;
} & DistributorConfigurationListenerAfWhiteList;
