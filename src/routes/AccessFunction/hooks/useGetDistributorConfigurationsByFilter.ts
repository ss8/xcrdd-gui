import { FormTemplate } from 'data/template/template.types';
import { useEffect, useState } from 'react';
import { getDistributorConfigurationsFilter } from '../utils/getDistributorConfigurationsFilter';

// eslint-disable-next-line import/prefer-default-export
export function useGetDistributorConfigurationsByFilter(
  deliveryFunctionId: string,
  template: FormTemplate | undefined,
  getDistributorConfigurationsByFilter: (deliveryFunctionId: string, filter: string) => void,
): void {
  const [distributorFilter, setDistributorFilter] = useState('');

  useEffect(() => {
    if (template) {
      const filter = getDistributorConfigurationsFilter(template);
      setDistributorFilter(filter);
    }
  }, [template]);

  useEffect(() => {
    if (distributorFilter) {
      getDistributorConfigurationsByFilter(deliveryFunctionId, distributorFilter);
    }
  }, [distributorFilter, deliveryFunctionId, getDistributorConfigurationsByFilter]);
}
