import { renderHook } from '@testing-library/react-hooks';
import { act } from '@testing-library/react';
import { FormTemplate, FormTemplateMapByKey } from 'data/template/template.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { useGetDistributorConfigurationsByFilter } from './useGetDistributorConfigurationsByFilter';

type RenderHookProps = {
  deliveryFunctionId: string,
  template: FormTemplate | undefined,
  getDistributorConfigurationsByFilter: (deliveryFunctionId: string, filter: string) => void,
}

describe('useGetDistributorConfigurationsByFilter()', () => {
  let accessFunctionTemplates: FormTemplateMapByKey;
  let getDistributorConfigurationsByFilter: jest.Mock;

  beforeEach(() => {
    accessFunctionTemplates = {
      ERK_SMF: getMockTemplateByKey('ERK_SMF'),
      NOK_5GSMF: getMockTemplateByKey('NOK_5GSMF'),
    };

    getDistributorConfigurationsByFilter = jest.fn();
  });

  it('should not call getDistributorConfigurationsByFilter if template is not defined', () => {
    renderHook(() => useGetDistributorConfigurationsByFilter('0', undefined, getDistributorConfigurationsByFilter));
    expect(getDistributorConfigurationsByFilter).not.toHaveBeenCalled();
  });

  it('should call getDistributorConfigurationsByFilter if everything is available', () => {
    renderHook(() => useGetDistributorConfigurationsByFilter('0', accessFunctionTemplates.ERK_SMF, getDistributorConfigurationsByFilter));
    expect(getDistributorConfigurationsByFilter).toHaveBeenCalledTimes(1);
  });

  it('should call getDistributorConfigurationsByFilter once if parameters did not change', () => {
    const { rerender } = renderHook(
      ({ deliveryFunction, template, filter }) => {
        return useGetDistributorConfigurationsByFilter(deliveryFunction, template, filter);
      }, {
        initialProps: {
          deliveryFunction: '0',
          template: accessFunctionTemplates.ERK_SMF,
          filter: getDistributorConfigurationsByFilter,
        },
      },
    );

    act(() => {
      rerender({
        deliveryFunction: '0',
        template: accessFunctionTemplates.ERK_SMF,
        filter: getDistributorConfigurationsByFilter,
      });
    });

    expect(getDistributorConfigurationsByFilter).toHaveBeenCalledTimes(1);
  });

  it('should call getDistributorConfigurationsByFilter as soon as everything is available', () => {
    const { rerender } = renderHook<RenderHookProps, void>(
      ({ deliveryFunctionId, template, getDistributorConfigurationsByFilter: filter }) => {
        return useGetDistributorConfigurationsByFilter(deliveryFunctionId, template, filter);
      }, {
        initialProps: {
          deliveryFunctionId: '0',
          template: undefined,
          getDistributorConfigurationsByFilter,
        },
      },
    );

    expect(getDistributorConfigurationsByFilter).not.toHaveBeenCalled();

    act(() => {
      rerender({
        deliveryFunctionId: '0',
        template: accessFunctionTemplates.ERK_SMF,
        getDistributorConfigurationsByFilter,
      });
    });

    expect(getDistributorConfigurationsByFilter).toHaveBeenCalledTimes(1);
  });

  it('should call getDistributorConfigurationsByFilter again if a parameter has changed', () => {
    const { rerender } = renderHook(
      ({ deliveryFunction, template, filter }) => {
        return useGetDistributorConfigurationsByFilter(deliveryFunction, template, filter);
      }, {
        initialProps: {
          deliveryFunction: '0',
          template: accessFunctionTemplates.ERK_SMF,
          filter: getDistributorConfigurationsByFilter,
        },
      },
    );

    expect(getDistributorConfigurationsByFilter).toHaveBeenCalledTimes(1);

    act(() => {
      rerender({
        deliveryFunction: '0',
        template: accessFunctionTemplates.NOK_5GSMF,
        filter: getDistributorConfigurationsByFilter,
      });
    });

    expect(getDistributorConfigurationsByFilter).toHaveBeenCalledTimes(2);
  });
});
