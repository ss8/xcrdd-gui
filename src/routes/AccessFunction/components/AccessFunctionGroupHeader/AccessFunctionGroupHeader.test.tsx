// eslint-disable-next-line import/no-extraneous-dependencies
import { Column, GridApi } from '@ag-grid-community/all-modules';
import {
  act, fireEvent, render, screen,
} from '@testing-library/react';
import React from 'react';
import AccessFunctionGroupHeader from './AccessFunctionGroupHeader';

describe('<AccessFunctionGroupHeader />', () => {
  it('should render the header as collapsed', () => {
    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="access-function-group-header"
          role="presentation"
        >
          <div
            class="ag-header-cell-label"
          >
            <span
              class="collapsed"
              data-testid="AccessFunctionGroupHeaderExpandCollapse"
              role="presentation"
              title="Expand all"
            >
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
            </span>
            <div
              class="ag-header-cell-label"
              role="presentation"
              unselectable="on"
            >
              <span
                class="ag-header-cell-text"
                role="columnheader"
                unselectable="on"
              >
                Name
              </span>
              <span
                unselectable="on"
              />
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('should render the header as expanded after clicking in expand', () => {
    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    act(() => {
      fireEvent.click(
        screen.getByTestId('AccessFunctionGroupHeaderExpandCollapse'),
      );
    });

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="access-function-group-header"
          role="presentation"
        >
          <div
            class="ag-header-cell-label"
          >
            <span
              class="expanded"
              data-testid="AccessFunctionGroupHeaderExpandCollapse"
              role="presentation"
              title="Collapse all"
            >
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
            </span>
            <div
              class="ag-header-cell-label"
              role="presentation"
              unselectable="on"
            >
              <span
                class="ag-header-cell-text"
                role="columnheader"
                unselectable="on"
              >
                Name
              </span>
              <span
                unselectable="on"
              />
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('should call setSort on click in header if enableSorting is true', () => {
    const setSort = jest.fn();
    render(
      <AccessFunctionGroupHeader
        displayName="Name"
        setSort={setSort}
        enableSorting
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    fireEvent.click(screen.getByText('Name'));
    expect(setSort).toBeCalledWith('asc', false);
  });

  it('should not call setSort on click in header if enableSorting is false', () => {
    const setSort = jest.fn();
    render(
      <AccessFunctionGroupHeader
        displayName="Name"
        setSort={setSort}
        enableSorting={false}
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    fireEvent.click(screen.getByText('Name'));
    expect(setSort).not.toBeCalled();
  });

  it('should show the asc icon if column sort is asc', () => {
    const eventTarget: any = new EventTarget();
    eventTarget.getSort = () => 'asc';

    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        enableSorting
        column={(eventTarget as unknown) as Column}
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    act(() => {
      eventTarget.dispatchEvent(new Event('sortChanged'));
    });

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="access-function-group-header"
          role="presentation"
        >
          <div
            class="ag-header-cell-label"
          >
            <span
              class="collapsed"
              data-testid="AccessFunctionGroupHeaderExpandCollapse"
              role="presentation"
              title="Expand all"
            >
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
            </span>
            <div
              class="ag-header-cell-label"
              role="presentation"
              unselectable="on"
            >
              <span
                class="ag-header-cell-text"
                role="columnheader"
                unselectable="on"
              >
                Name
              </span>
              <span
                aria-hidden="true"
                class="ag-header-icon ag-sort-ascending-icon"
              >
                <span
                  class="ag-icon ag-icon-asc"
                  unselectable="on"
                />
              </span>
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('should show the menu if mouse over and enableMenu is true', () => {
    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        enableSorting
        enableMenu
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    act(() => {
      fireEvent.mouseOver(screen.getByText('Name'));
    });

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="access-function-group-header"
          role="presentation"
        >
          <div
            class="ag-header-cell-label"
          >
            <span
              class="collapsed"
              data-testid="AccessFunctionGroupHeaderExpandCollapse"
              role="presentation"
              title="Expand all"
            >
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
            </span>
            <div
              class="ag-header-cell-label"
              role="presentation"
              unselectable="on"
            >
              <span
                class="ag-header-cell-text"
                role="columnheader"
                unselectable="on"
              >
                Name
              </span>
              <span
                unselectable="on"
              />
            </div>
          </div>
          <span
            aria-hidden="true"
            class="ag-header-icon ag-header-cell-menu-button"
          >
            <span
              class="ag-icon ag-icon-menu"
              unselectable="on"
            />
          </span>
        </div>
      </div>
    `);
  });

  it('should not show the menu if mouse over and enableMenu is false', () => {
    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        enableSorting
        enableMenu={false}
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    act(() => {
      fireEvent.mouseOver(screen.getByText('Name'));
    });

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="access-function-group-header"
          role="presentation"
        >
          <div
            class="ag-header-cell-label"
          >
            <span
              class="collapsed"
              data-testid="AccessFunctionGroupHeaderExpandCollapse"
              role="presentation"
              title="Expand all"
            >
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
              <span
                class="ag-icon ag-icon-contracted"
                unselectable="on"
              />
            </span>
            <div
              class="ag-header-cell-label"
              role="presentation"
              unselectable="on"
            >
              <span
                class="ag-header-cell-text"
                role="columnheader"
                unselectable="on"
              >
                Name
              </span>
              <span
                unselectable="on"
              />
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('should call showColumnMenu on click in menu icon', () => {
    const showColumnMenu = jest.fn();
    const { container } = render(
      <AccessFunctionGroupHeader
        displayName="Name"
        enableSorting
        enableMenu
        showColumnMenu={showColumnMenu}
        api={
          ({
            expandAll: jest.fn(),
            collapseAll: jest.fn(),
          } as unknown) as GridApi
        }
      />,
    );

    fireEvent.click(container.querySelector('.ag-icon-menu') as Element);
    expect(showColumnMenu).toBeCalled();
  });
});
