import React, { ReactElement } from 'react';

type Props = {
  collapsed: boolean
  onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export default function AccessFunctionGroupHeaderCollapseExtandIcon({ collapsed, onClick }: Props): ReactElement {
  let title = 'Collapse all';
  let className = 'expanded';

  if (collapsed) {
    title = 'Expand all';
    className = 'collapsed';
  }

  return (
    <span
      data-testid="AccessFunctionGroupHeaderExpandCollapse"
      className={className}
      onClick={onClick}
      title={title}
      role="presentation"
    >
      <span className="ag-icon ag-icon-contracted" unselectable="on" />
      <span className="ag-icon ag-icon-contracted" unselectable="on" />
    </span>
  );
}
