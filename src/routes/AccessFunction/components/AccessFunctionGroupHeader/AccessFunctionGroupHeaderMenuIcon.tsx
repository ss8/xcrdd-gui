import React, { ReactElement } from 'react';

type Props = {
  onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export default function AccessFunctionGroupHeaderMenuIcon({ onClick }: Props): ReactElement {
  return (
    <span className="ag-header-icon ag-header-cell-menu-button" onClick={onClick} aria-hidden="true">
      <span className="ag-icon ag-icon-menu" unselectable="on" />
    </span>
  );
}
