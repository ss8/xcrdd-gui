import React, { useEffect, useState } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { IHeaderParams } from '@ag-grid-community/all-modules';
import AccessFunctionGroupHeaderCollapseExtandIcon from './AccessFunctionGroupHeaderCollapseExtandIcon';
import AccessFunctionGroupHeaderSortIcon from './AccessFunctionGroupHeaderSortIcon';
import AccessFunctionGroupHeaderMenuIcon from './AccessFunctionGroupHeaderMenuIcon';
import './access-function-group-header.scss';

const SORT_NEXT_CONFIGURATION: { [key: string]: string } = {
  none: 'asc',
  asc: 'desc',
  desc: '',
};

type Props = Partial<IHeaderParams>;

export default function AccessFunctionGroupHeader({
  displayName,
  column,
  api,
  enableMenu,
  enableSorting,
  setSort,
  showColumnMenu,
}: Props): JSX.Element {
  const [allCollapsed, setAllCollapsed] = useState(true);
  const [sortOrder, setSortOrder] = useState('none');

  useEffect(() => {
    const sortChangedListener = () => setSortOrder(column?.getSort() ?? 'none');
    column?.addEventListener('sortChanged', sortChangedListener);
    return () => column?.removeEventListener('sortChanged', sortChangedListener);
  }, [column]);

  const handleClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    if (!enableSorting) {
      return;
    }

    const newSort = SORT_NEXT_CONFIGURATION[column?.getSort() ?? 'none'];
    setSort?.(newSort, event.shiftKey);
  };

  const handleExpandCollapseClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.stopPropagation();

    if (allCollapsed) {
      api?.expandAll();
      setAllCollapsed(false);

    } else {
      api?.collapseAll();
      setAllCollapsed(true);
    }
  };

  const handleMenuClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.stopPropagation();
    showColumnMenu?.(event.currentTarget);
  };

  return (
    <div className="access-function-group-header" onClick={handleClick} role="presentation">
      <div className="ag-header-cell-label">
        <AccessFunctionGroupHeaderCollapseExtandIcon onClick={handleExpandCollapseClick} collapsed={allCollapsed} />
        <div className="ag-header-cell-label" role="presentation" unselectable="on">
          <span className="ag-header-cell-text" role="columnheader" unselectable="on">{displayName}</span>
          <AccessFunctionGroupHeaderSortIcon sort={sortOrder} />
        </div>
      </div>
      {enableMenu && <AccessFunctionGroupHeaderMenuIcon onClick={handleMenuClick} />}
    </div>
  );
}
