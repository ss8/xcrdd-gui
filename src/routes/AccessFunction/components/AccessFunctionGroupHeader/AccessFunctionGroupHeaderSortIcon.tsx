import React, { ReactElement } from 'react';

type Props = {
  sort: string
}

export default function AccessFunctionGroupHeaderSortIcon({ sort }: Props): ReactElement {
  if (sort === 'asc') {
    return (
      <span className="ag-header-icon ag-sort-ascending-icon" aria-hidden="true">
        <span className="ag-icon ag-icon-asc" unselectable="on" />
      </span>
    );
  }

  if (sort === 'desc') {
    return (
      <span className="ag-header-icon ag-sort-descending-icon" aria-hidden="true">
        <span className="ag-icon ag-icon-desc" unselectable="on" />
      </span>
    );
  }

  return <span unselectable="on" />;
}
