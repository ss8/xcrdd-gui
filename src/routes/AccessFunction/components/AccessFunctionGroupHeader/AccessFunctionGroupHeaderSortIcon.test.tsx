import React from 'react';
import { render } from '@testing-library/react';
import AccessFunctionGroupHeaderSortIcon from './AccessFunctionGroupHeaderSortIcon';

describe('<AccessFunctionGroupHeaderSortIcon />', () => {
  it('should render the expected icon if sort is asc', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderSortIcon sort="asc" />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          aria-hidden="true"
          class="ag-header-icon ag-sort-ascending-icon"
        >
          <span
            class="ag-icon ag-icon-asc"
            unselectable="on"
          />
        </span>
      </div>
    `);
  });

  it('should render the expected icon if sort is desc', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderSortIcon sort="desc" />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          aria-hidden="true"
          class="ag-header-icon ag-sort-descending-icon"
        >
          <span
            class="ag-icon ag-icon-desc"
            unselectable="on"
          />
        </span>
      </div>
    `);
  });

  it('should render the expected icon if sort is none', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderSortIcon sort="none" />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          unselectable="on"
        />
      </div>
    `);
  });
});
