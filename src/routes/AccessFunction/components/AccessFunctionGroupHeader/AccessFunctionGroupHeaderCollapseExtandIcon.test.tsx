import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import AccessFunctionGroupHeaderCollapseExtandIcon from './AccessFunctionGroupHeaderCollapseExtandIcon';

describe('<AccessFunctionGroupHeaderCollapseExtandIcon />', () => {
  it('should render the expected icon if collapsed', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderCollapseExtandIcon
        collapsed={false}
        onClick={jest.fn()}
      />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="expanded"
          data-testid="AccessFunctionGroupHeaderExpandCollapse"
          role="presentation"
          title="Collapse all"
        >
          <span
            class="ag-icon ag-icon-contracted"
            unselectable="on"
          />
          <span
            class="ag-icon ag-icon-contracted"
            unselectable="on"
          />
        </span>
      </div>
    `);
  });

  it('should render the expected icon if expanded', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderCollapseExtandIcon
        collapsed
        onClick={jest.fn()}
      />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="collapsed"
          data-testid="AccessFunctionGroupHeaderExpandCollapse"
          role="presentation"
          title="Expand all"
        >
          <span
            class="ag-icon ag-icon-contracted"
            unselectable="on"
          />
          <span
            class="ag-icon ag-icon-contracted"
            unselectable="on"
          />
        </span>
      </div>
    `);
  });

  it('should call the onclick callback on click', () => {
    const onClick = jest.fn();

    const { container } = render(
      <AccessFunctionGroupHeaderCollapseExtandIcon
        collapsed
        onClick={onClick}
      />,
    );

    fireEvent.click(container.children[0]);

    expect(onClick).toHaveBeenCalled();
  });
});
