import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import AccessFunctionGroupHeaderMenuIcon from './AccessFunctionGroupHeaderMenuIcon';

describe('<AccessFunctionGroupHeaderMenuIcon />', () => {
  it('should render the expected icon', () => {
    const { container } = render(
      <AccessFunctionGroupHeaderMenuIcon onClick={jest.fn()} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          aria-hidden="true"
          class="ag-header-icon ag-header-cell-menu-button"
        >
          <span
            class="ag-icon ag-icon-menu"
            unselectable="on"
          />
        </span>
      </div>
    `);
  });

  it('should call the onclick callback on click', () => {
    const onClick = jest.fn();

    const { container } = render(
      <AccessFunctionGroupHeaderMenuIcon onClick={onClick} />,
    );

    fireEvent.click(container.children[0]);

    expect(onClick).toHaveBeenCalled();
  });
});
