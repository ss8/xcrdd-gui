import React, {
  FC,
  useEffect,
  useState,
  Fragment,
} from 'react';
import IPAddressValidator from 'shared/form/validators/IPAddressValidator';
import CellValidationAlert from '../CellValidationAlert';

type Props = {
  value: string,
  context: { isDialogSubmitted: boolean }
};

const IpAddressCellRenderer: FC<Props> = ({ value, context }: Props) => {
  const [isIpValid, setIpValid] = useState(true);

  useEffect(() => {
    if (value || context.isDialogSubmitted) {
      setIpValid(IPAddressValidator.isValid(null, null, value, null, null, null));
    }
  }, [value, context]);

  const renderInvalidIPAlert = (isValid: boolean) => {
    if (isValid) {
      return null;
    }

    return (
      <Fragment>
        <CellValidationAlert msg="Please enter a valid IP Address." />&nbsp;
      </Fragment>
    );
  };

  return (
    <div>
      {renderInvalidIPAlert(isIpValid)}{value}
    </div>
  );
};

export default IpAddressCellRenderer;
