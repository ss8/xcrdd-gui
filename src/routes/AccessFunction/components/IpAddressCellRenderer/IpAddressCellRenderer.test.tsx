import React from 'react';
import { render, screen } from '@testing-library/react';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import IpAddressCellRenderer from './IpAddressCellRenderer';

const AFS_WHITELIST_MOCK: DistributorConfigurationListenerAfWhiteList[] = [
  {
    afName: 'afName1',
    afAddress: '1.1.1.1',
    afId: 'id1',
  },
  {
    afName: 'afName2',
    afAddress: '2.2.2.2',
    afId: 'id2',
  },
  {
    afName: '',
    afAddress: '',
    afId: '',
  },
];

describe('<IpAddressCellRenderer />', () => {
  it('should not render error alert when af address value is undefined because dialog is not submitted', () => {
    const value = '';

    const { container } = render(
      <IpAddressCellRenderer
        value={value}
        context={{
          isDialogSubmitted: false,
        }}
      />,
    );

    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(0);
  });

  it('should not render error alert when af address value is valid and dialog is submitted', () => {
    const value = AFS_WHITELIST_MOCK[0].afAddress;

    const { container } = render(
      <IpAddressCellRenderer
        value={value}
        context={{
          isDialogSubmitted: true,
        }}
      />,
    );

    expect(screen.getByText(value)).toBeVisible();
    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(0);
  });

  it('should render error alert when af address value is invalid and dialog is submitted', () => {
    const value = 'abcde';

    const { container } = render(
      <IpAddressCellRenderer
        value={value}
        context={{
          isDialogSubmitted: true,
        }}
      />,
    );

    expect(screen.getByText(value)).toBeVisible();
    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(1);
  });
});
