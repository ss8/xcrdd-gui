import React from 'react';
import {
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import * as types from 'data/accessFunction/accessFunction.types';
import {
  FormTemplate,
  AFFormTemplateFormSectionName,
} from 'data/template/template.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { mockDistributorConfigurationTableData } from '__test__/mockDistributorConfigurationTableData';
import { createAccessFunctionBasedOnTemplate } from 'routes/AccessFunction/utils/accessFunctionTemplate.factory';
import MultifacetedFormSection from './MultifacetedFormSection';

describe('<MultifacetedFormSection/>', () => {
  let accessFunctionNoInterfaceData: Partial<types.AccessFunctionFormData>;
  const title = 'Section title';
  const afTemplates: { [key: string]: FormTemplate } = {};

  beforeEach(() => {
    accessFunctionNoInterfaceData = {
      provisioningInterfaces: [],
      dataInterfaces: [],
      contentInterfaces: [],
    };

    afTemplates.ERK_SMF = getMockTemplateByKey('ERK_SMF');
    afTemplates.STA_PCRF = getMockTemplateByKey('PCRF');
    afTemplates.ERK_UPF = getMockTemplateByKey('ERK_UPF');
    afTemplates.STA_PGW = getMockTemplateByKey('STA_PGW');
    afTemplates.NSN_CSCF = getMockTemplateByKey('NSN_CSCF');
    afTemplates.ERK_MCPTT = getMockTemplateByKey('ERK_MCPTT');
    afTemplates.NSNHLR_SDM_PRGW = getMockTemplateByKey('NSNHLR_SDM_PRGW');
    afTemplates.ALU_MME = getMockTemplateByKey('ALU_MME');
  });

  it('should render FormSection', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_SMF);

    render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.ERK_SMF}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        data={data}
        distributorConfigs={[]}
        isEditing
      />,
    );
    expect(screen.getByTestId('FormSection-provisioningInterfaces')).toBeVisible();
  });

  it('should render AFMultiTabFormSection', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.STA_PCRF);

    render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.STA_PCRF}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId(`FormSection-${AFFormTemplateFormSectionName.provisioningInterfaces}`)).toBeVisible();
  });

  it('should render contentInterfaces DistributorConfigurationFormSection', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_UPF);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_UPF}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible();
  });

  it('should render dataInterfaces DistributorConfigurationFormSection', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_SMF);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_SMF}
        formSectionName={AFFormTemplateFormSectionName.dataInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId('FormSectionDistributorConfigs-dataInterfaces')).toBeVisible();
  });

  it('should render VPN section for dataInterfaces', () => {
    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_SMF}
        formSectionName={AFFormTemplateFormSectionName.dataInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );
    expect(screen.getByTestId('VPNConfiguration-x2')).toBeVisible();
  });

  it('should not render VPN section for ContentInterfaces', () => {
    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_SMF}
        formSectionName={AFFormTemplateFormSectionName.dataInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );

    expect(screen.queryByTestId('VPNConfiguration-x3')).toBeNull();
  });

  it('should render distributor configuration form in content interfaces section ', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.STA_PGW);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.STA_PGW}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName="ss8Distributor"
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible();
  });

  it('should render multitab form in an interface section has 0 to n interfaces ', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.NSNHLR_SDM_PRGW);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.NSNHLR_SDM_PRGW}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId('AFMultiTabFormSection-NSNHLR_SDM_PRGW')).toBeVisible();
  });

  it('should render content interfaces form when template subsection form name cannot be found', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.STA_PGW);

    render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.STA_PGW}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName="directFromAF"
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByTestId('FormSection-contentInterfaces')).toBeVisible();
    expect(screen.getByText('AF IP Address:')).toBeVisible();
  });

  it('should render custom instructional Text for distributor configuration Interface', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.STA_PGW);

    render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.STA_PGW}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName="ss8Distributor"
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByText('All AFs of this type are auto-associated with all SS8 Distributors of the same type.')).toBeVisible();
  });

  it('should render nothing when template section is not in the template', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.STA_PGW);

    const { container } = render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.STA_PGW}
        formSectionName={AFFormTemplateFormSectionName.dataInterfaces}
        formSubsectionName="no_such_subsection"
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(container).toBeEmptyDOMElement();
  });

  it('should use different name for Form when template changes otherwise Form will crash', () => {
    let data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_SMF);

    const { rerender } = render(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.ERK_SMF}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );
    expect(screen.getByText('TLS Profile (Security Info ID):')).toBeVisible();

    data = createAccessFunctionBasedOnTemplate(afTemplates.NSN_CSCF);

    rerender(
      <MultifacetedFormSection
        title={title}
        template={afTemplates.NSN_CSCF}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );

    expect(screen.getByText('AF URL:')).toBeVisible();
  });

  it('should display the add button when the template section can have zero interface and there is no interface', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_MCPTT);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing
      />,
    );

    expect(screen.getByText('Section title')).toBeVisible();
    expect(screen.queryByTestId('FormSectionAddButton')).toBeVisible();
  });

  it('should not show anything if in view mode and data is empty for that form section name', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_MCPTT);

    const { container } = render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing={false}
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

  it('should show distributor config if in view mode and data is empty for that form section name', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ERK_UPF);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing={false}
      />,
    );

    expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible();
  });

  it('should not show the remove button if in view mode', () => {
    const data = createAccessFunctionBasedOnTemplate(afTemplates.ALU_MME);

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={data}
        isEditing={false}
      />,
    );

    expect(screen.getByText('Section title')).toBeVisible();
    expect(screen.queryAllByTestId('FormSectionRemove')).toHaveLength(0);
  });

  it('should not return values if there is no form', () => {
    let ref: any;
    render(
      <MultifacetedFormSection
        ref={(instance) => { ref = instance; }}
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );

    expect(ref?.getValues()).toEqual({
      afInterfaceFormData: null,
      vpnConfigData: null,
    });
  });

  it('should return valid if there is no form', () => {
    let ref: any;
    render(
      <MultifacetedFormSection
        ref={(instance) => { ref = instance; }}
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
        formSubsectionName=""
        distributorConfigs={[]}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );

    expect(ref?.isValid(true, true)).toBeTruthy();
  });

  it('should display distributor config applicable to this section', async () => {
    const distributorConfigurationTableData: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X2,
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X3,
    ];

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName=""
        distributorConfigs={distributorConfigurationTableData}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );

    expect(screen.queryByRole('button', { name: /remove/i })).toBeNull();
    expect(screen.queryByRole('button', { name: /plus/i })).toBeNull();

    await waitFor(() => expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible());

    expect(await screen?.findByText(
      mockDistributorConfigurationTableData.ERK_MCPTT_X3[0].listener.ipAddress
    )).toBeVisible();
    expect(await screen?.findByText(
      mockDistributorConfigurationTableData.ERK_MCPTT_X3[1].listener.ipAddress
    )).toBeVisible();
    expect(screen?.queryByText(
      mockDistributorConfigurationTableData.ERK_MCPTT_X2[0].listener.ipAddress
    )).toBeNull();
  });

  it('should display empty grid when no applicable distributor config to this section', async () => {
    const distributorConfigurationTableData: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X2,
    ];

    render(
      <MultifacetedFormSection
        title="Section title"
        template={afTemplates.ERK_MCPTT}
        formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
        formSubsectionName=""
        distributorConfigs={distributorConfigurationTableData}
        data={accessFunctionNoInterfaceData}
        isEditing
      />,
    );

    await waitFor(() => expect(screen.getByTestId('FormSectionDistributorConfigs-contentInterfaces')).toBeVisible());

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });
});
