import React, {
  Fragment,
  forwardRef,
  useRef,
  useImperativeHandle,
} from 'react';
import { Button } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Form, { CustomFormFieldComponent, CustomValidationFunction } from 'shared/form';
import {
  FormTemplate,
  FormTemplateSection,
  SectionType,
} from 'data/template/template.types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import {
  AccessFunctionFormData,
  AccessFunctionInterfaceCompositeFormData,
  AccessFunctionInterfaceFormSectionType,
  AccessFunctionFieldUpdateCallback,
} from 'data/accessFunction/accessFunction.types';
import { VPNConfigurationSection, VPNConfigurationSectionRef } from 'components/VPNConfiguration';
import { populateLiInterfaceId, populateMappedId } from 'components/VPNConfiguration/utils/vpn.utils';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { VpnConfigTemplate } from 'data/vpnTemplate/vpnTemplate.types';
import { AFMultiTabFormSection, AFMultiTabFormSectionRef } from '../AFMultiTabFormSection';
import FormSection from '../FormSection';
import FormSectionDistributorConfigs from '../FormSectionDistributorConfigs';
import { getInterfaceFormTemplateSection } from '../../utils/accessFunctionTemplate.adapter';
import { isHeaderSelectionPanel } from './MultifacetedForm.utils';

type Props = {
  title: string,
  template: FormTemplate,
  formSectionName: AccessFunctionInterfaceFormSectionType,
  formSubsectionName: string
  data: Partial<AccessFunctionFormData> | undefined,
  distributorConfigs: DistributorConfigurationTableData[],
  vpnConfigTemplates?: VpnConfigTemplate[],
  vpnConfigs?: VpnConfig[],
  isEditing: boolean,
  is5gcVcpEnabled?: boolean,
  customValidationFunctionMap?: { [key: string]: CustomValidationFunction },
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent },
  shouldValidateOnRender?: boolean
  onFieldUpdate?: AccessFunctionFieldUpdateCallback,
  onDistributorUpdate?: (distributorConfiguration: DistributorConfigurationTableData) => void,
  onTabAdd?: (formSectionName: AccessFunctionInterfaceFormSectionType) => void,
  onTabRemove?: (id: string | number, formSectionName: AccessFunctionInterfaceFormSectionType) => void,
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => AccessFunctionInterfaceCompositeFormData | null
}

export const MultifacetedFormSection = forwardRef<Ref, Props>(({
  title,
  template,
  formSectionName,
  formSubsectionName,
  data,
  distributorConfigs,
  vpnConfigTemplates,
  vpnConfigs,
  isEditing,
  is5gcVcpEnabled,
  customValidationFunctionMap,
  customFieldComponentMap,
  shouldValidateOnRender = false,
  onFieldUpdate,
  onDistributorUpdate,
  onTabAdd = () => null,
  onTabRemove = () => null,
}: Props, ref) => {
  const singleFormRef = useRef<Form>(null);
  const multiFormRef = useRef<AFMultiTabFormSectionRef>(null);
  const vpnConfigRef = useRef<VPNConfigurationSectionRef>(null);

  const templateSection = getInterfaceFormTemplateSection(template, formSectionName, formSubsectionName);

  const getValues = (): AccessFunctionInterfaceCompositeFormData | null => {
    let vpnConfigFormData : VpnConfig | null = getConfiguredVPNDataExcludeCopy() ?? null;
    if (vpnConfigRef.current?.getValues() != null) {
      vpnConfigFormData = vpnConfigRef.current.getValues();
    }

    if (templateSection?.metadata?.sectionType === SectionType.distributorConfiguration) {
      return {
        afInterfaceFormData: null,
        vpnConfigData: vpnConfigFormData,
      };
    }

    if (templateSection?.metadata?.max !== undefined && templateSection.metadata.max > 1) {
      return {
        afInterfaceFormData: multiFormRef.current?.getValues() ?? null,
        vpnConfigData: vpnConfigFormData,
      };
    }

    const singleFormValues = singleFormRef.current?.getValues();
    return {
      afInterfaceFormData: singleFormValues ? [singleFormValues] : null,
      vpnConfigData: vpnConfigFormData,
    };
  };

  const isValid = (forceValidation: boolean, forceTouched: boolean): boolean => {
    if (templateSection?.metadata?.sectionType === SectionType.distributorConfiguration) {
      return true;
    }

    if (templateSection?.metadata?.max !== undefined && templateSection.metadata.max > 1) {
      return (multiFormRef.current?.isValid(forceValidation, forceTouched) ?? true);
    }

    return (singleFormRef.current?.isValid(forceValidation, forceTouched) ?? true);
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  function getConfiguredVPNData() {
    const populatedMappedId = populateMappedId(formSectionName);
    return data?.vpnConfig?.filter((vpn) => (vpn.mappedId === populatedMappedId))[0];
  }

  function getConfiguredVPNDataExcludeCopy() {
    const populatedMappedId = populateMappedId(formSectionName);
    // for Copy AF, vpn.id is undefined, filter it out.
    return data?.vpnConfig?.filter((vpn) => (vpn.id != null && vpn.mappedId === populatedMappedId))[0];
  }

  function getNodeName() {
    // The Node Name is used to filter the list of Ericsson UPF distributors when is5gcVcpEnabled is true.
    // The Ericsson UPF distributors are visible inside the Ericsson SMF X3 section.
    return data?.provisioningInterfaces?.[0]?.ccnodename;
  }

  function renderVpnConfig() {
    const populatedLiInterface = populateLiInterfaceId(formSectionName);
    return (
      <Fragment>
        <VPNConfigurationSection
          liInterface={populatedLiInterface}
          ref={vpnConfigRef}
          data={getConfiguredVPNData()}
          isEditing={isEditing}
          vpnConfigTemplates={vpnConfigTemplates}
          vpnConfigs={vpnConfigs}
        />
      </Fragment>
    );
  }

  const handleRemoveButtonClick = () => {
    if (data == null || formSectionName == null) {
      return;
    }

    const interfaceData = data[formSectionName];
    if (interfaceData == null) {
      return;
    }

    onTabRemove(interfaceData[0].id, formSectionName);
  };

  const accessFunctionInterfaces = data?.[formSectionName] ?? [];

  const metadata = templateSection?.metadata;

  const shouldShowDistributorConfiguration = metadata?.sectionType === SectionType.distributorConfiguration;
  const shouldShowAddButton = accessFunctionInterfaces.length === 0;
  const shouldShowMultiTab = metadata?.max !== undefined && metadata.max > 1;
  const shouldShowRemoveButton = metadata?.min === 0 && isEditing;
  const shouldShowHeaderSelectionPanel = isHeaderSelectionPanel(template, formSectionName);

  function renderHeaderSelectionPanel() {
    if (shouldShowHeaderSelectionPanel === false) {
      return null;
    }
    const headerSelectionTemplateSection = template[formSectionName] as FormTemplateSection;
    return (
      <div data-testid={`SelectionPanel-${formSectionName}`}>
        <FormSection
          name={template.formId ?? template.key}
          data={data ?? {}}
          formSectionName={formSectionName}
          templateSection={headerSelectionTemplateSection}
          onFieldUpdate={onFieldUpdate}
        />
      </div>
    );
  }

  function renderForm() {

    if (shouldShowDistributorConfiguration) {
      return (
        <Fragment>
          <h3>{title}</h3>
          {renderHeaderSelectionPanel()}
          <FormSectionDistributorConfigs
            isEditing={isEditing}
            is5gcVcpEnabled={is5gcVcpEnabled}
            data={distributorConfigs}
            formSectionName={formSectionName}
            templateSection={templateSection}
            onDistributorUpdate={onDistributorUpdate}
            instructionalText={metadata?.sectionTypeInstructionalText}
            nodeName={getNodeName()}
          />
          {renderVpnConfig()}
        </Fragment>
      );
    }

    if (shouldShowAddButton) {
      const onAddSection = () => onTabAdd(formSectionName);

      return (
        <div data-testid={`FormSection-${formSectionName}`}>
          <h3>{title}</h3>
          {renderHeaderSelectionPanel()}
          <Button data-testid="FormSectionAddButton" icon={IconNames.PLUS} onClick={onAddSection} className="add-button" />
        </div>
      );
    }

    if (shouldShowMultiTab) {
      return (
        <div data-testid={`FormSection-${formSectionName}`}>
          <h3>{title}</h3>
          {renderHeaderSelectionPanel()}
          <AFMultiTabFormSection
            ref={multiFormRef}
            name={template.formId ?? template.key}
            data={accessFunctionInterfaces}
            templateSection={templateSection}
            onFieldUpdate={onFieldUpdate}
            isEditing={isEditing}
            formSectionName={formSectionName}
            template={template}
            onTabInterfaceAdd={onTabAdd}
            onTabInterfaceRemove={onTabRemove}
            customValidationFunctionMap={customValidationFunctionMap}
            customFieldComponentMap={customFieldComponentMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
          {renderVpnConfig()}
        </div>
      );
    }

    return (
      <Fragment>
        <FormSection
          ref={singleFormRef}
          name={template.formId ?? template.key}
          title={title}
          data={data || {}}
          formSectionName={formSectionName}
          dataSection={`${formSectionName}.0`}
          templateSection={templateSection}
          onFieldUpdate={onFieldUpdate}
          customValidationFunctionMap={customValidationFunctionMap}
          customFieldComponentMap={customFieldComponentMap}
          showRemoveButton={shouldShowRemoveButton}
          onRemoveButtonClick={handleRemoveButtonClick}
          shouldValidateOnRender={shouldValidateOnRender}
        />
        {renderVpnConfig()}
      </Fragment>
    );
  }

  if ((!templateSection) ||
      (!isEditing && shouldShowAddButton && !shouldShowDistributorConfiguration)) {
    return null;
  }

  return (
    <Fragment>
      {renderForm()}
    </Fragment>
  );
});

export default MultifacetedFormSection;
