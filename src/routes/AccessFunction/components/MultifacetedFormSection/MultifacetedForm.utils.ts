import {
  FormTemplate,
  FormTemplateSection,
  SectionType,
} from 'data/template/template.types';
import {
  AccessFunctionInterfaceFormSectionType,
} from 'data/accessFunction/accessFunction.types';

// eslint-disable-next-line import/prefer-default-export
export function isHeaderSelectionPanel(
  template: FormTemplate,
  formSectionName: AccessFunctionInterfaceFormSectionType,
): boolean {
  return ((template?.[formSectionName] as FormTemplateSection)?.metadata?.sectionType ===
    SectionType.headerSelectionPanel);
}
