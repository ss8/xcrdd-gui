import React, { createRef } from 'react';
import {
  render,
  screen,
  fireEvent,
  act,
} from '@testing-library/react';
import { Contact } from 'data/types';
import {
  FormTemplate,
} from 'data/template/template.types';
import { Network } from 'data/network/network.types';
import { SecurityInfo } from 'data/securityInfo/securityInfo.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { getByDataTest } from '__test__/customQueries';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockNodes } from '__test__/mockNodes';
import { mockAccessFunctionsSMF, mockAccessFunctionsUPF } from '__test__/mockAccessFunctions';
import { AccessFunctionFormData, ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID } from 'data/accessFunction/accessFunction.types';
import { mockDistributorConfigurationTableData } from '__test__/mockDistributorConfigurationTableData';
import {
  DistributorConfigurationTableData,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockNetworks } from '__test__/mockNetworks';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { createAccessFunctionBasedOnTemplate } from 'routes/AccessFunction/utils/accessFunctionTemplate.factory';
import { AccessFunctionForm, Ref } from './AccessFunctionForm';
import { adaptTemplateOptions } from '../../utils/accessFunctionTemplate.adapter';

describe('<AccessFunctionForm />', () => {
  let templates: { [key: string]: FormTemplate };
  let contacts: Contact[];
  let networks: Network[];
  let securityInfos: SecurityInfo[];
  let partialAccessFunction: Partial<AccessFunctionFormData>;
  let countries: string[];
  let accessFunctionTemplates: { [key: string]: FormTemplate };

  beforeEach(() => {
    partialAccessFunction = {
      id: 'id1',
    };

    contacts = [{
      id: 'id1',
      name: 'Name 1',
    }, {
      id: 'id2',
      name: 'Name 2',
    }];

    networks = [{
      id: 'id1',
      networkId: 'networkId1',
      ownIPAddress: '12.12.12.12',
      startPortNumber: '1000',
      portNumberRange: '100',
    }];

    securityInfos = [{
      id: 'VExTMTJfQ0xJRU5U',
      name: 'TLS12_CLIENT',
      intfType: 'TLS',
      tlsVersion: 'TLSv1.2',
      tlsType: 'Client',
      authenType: 'MUTUAL',
      privateKeyFile: 'server.key',
      ownCertFile: 'server.crt',
      certAuthority: 'root_cacert.pem',
      certDirectory: '/SS8/Keys_and_Certs',
      description: '',
    }];

    countries = [
      'US',
    ];

    accessFunctionTemplates = {
      ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      ERK_SMF: getMockTemplateByKey('ERK_SMF'),
      STA_PGW: getMockTemplateByKey('STA_PGW'),
      ERK_MCPTT: getMockTemplateByKey('ERK_MCPTT'),
      ERK_SGW: getMockTemplateByKey('ERK_SGW'),
      MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
    };

    templates = adaptTemplateOptions(
      accessFunctionTemplates,
      countries,
      mockTimeZones,
      contacts,
      networks,
      securityInfos,
      mockSshSecurityInfos,
      mockXtps,
      mockXdps,
      mockProcessOptions,
      mockSupportedFeatures.config.accessFunctions,
      mockNodes,
    );
  });

  it('should render the form title with Cancel and Save buttons', () => {
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        onFieldUpdate={jest.fn()}
        template={templates.ERK_UPF}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByText('Access Function form title')).toBeVisible();
    let items = screen.getAllByText('Cancel');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Save');
    expect(items).toHaveLength(2);
  });

  it('should render the form title with Edit and Back buttons', () => {
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing={false}
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        onFieldUpdate={jest.fn()}
        template={templates.ERK_UPF}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByText('Access Function form title')).toBeVisible();
    let items = screen.getAllByText('Edit');
    expect(items).toHaveLength(2);
    items = screen.getAllByText('Back');
    expect(items).toHaveLength(2);
  });

  it('should call the onClickCancelClose callback when clicking in Cancel', () => {
    const onClickCancelClose = jest.fn();
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={onClickCancelClose}
      />,
    );
    fireEvent.click(screen.getAllByText('Cancel')[0]);
    expect(onClickCancelClose).toBeCalled();
  });

  it('should call the onClickCancelClose callback when clicking in Back', () => {
    const onClickCancelClose = jest.fn();
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing={false}
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickEdit={jest.fn()}
        onClickCancelClose={onClickCancelClose}
      />,
    );
    fireEvent.click(screen.getAllByText('Back')[0]);
    expect(onClickCancelClose).toBeCalled();
  });

  it('should call the onClickSave callback when clicking in Save', () => {
    const onClickSave = jest.fn();
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickSave={onClickSave}
        onClickCancelClose={jest.fn()}
      />,
    );
    fireEvent.click(screen.getAllByText('Save')[0]);
    expect(onClickSave).toBeCalled();
  });

  it('should call the onClickEdit callback when clicking in Edit', () => {
    const onClickEdit = jest.fn();
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing={false}
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickEdit={onClickEdit}
        onClickCancelClose={jest.fn()}
      />,
    );
    fireEvent.click(screen.getAllByText('Edit')[0]);
    expect(onClickEdit).toBeCalled();
  });

  it('should call the onFieldUpdate callback when changing an input', () => {
    const onFieldUpdate = jest.fn();
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={onFieldUpdate}
        onClickCancelClose={jest.fn()}
      />,
    );
    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name 1' } });
    expect(onFieldUpdate).toBeCalledWith({
      label: 'Name',
      name: 'name',
      value: 'AF Name 1',
      valueLabel: 'AF Name 1',
    }, 'general', 'id1');
  });

  it('should get the form values', () => {
    let ref: any;

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name 1' } });

    expect(ref?.getValues()).toEqual({
      id: 'id1',
      city: '',
      contact: '',
      country: '',
      description: '',
      insideNat: false,
      name: 'AF Name 1',
      liInterface: 'ALL',
      model: '',
      preprovisioningLead: '000:00',
      stateName: '',
      serialNumber: '',
      timeZone: '',
      type: 'ERK_UPF',
      tag: 'ERK_UPF',
      version: '',
      vpnConfig: [],
    });
  });

  it('should return form values including system and provisioning interfaces', () => {
    let ref: any;

    const data = createAccessFunctionBasedOnTemplate(templates.ERK_SMF);

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={data}
        distributorConfigs={mockDistributorConfigurationTableData.ERK_SMF_X2}
        accessFunctionByType={{}}
        template={templates.ERK_SMF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name 1' } });

    expect(ref?.getValues()).toEqual({
      id: '',
      city: '',
      contact: '',
      country: '',
      description: '',
      insideNat: false,
      name: 'AF Name 1',
      liInterface: 'ALL',
      model: '',
      preprovisioningLead: '000:00',
      stateName: '',
      serialNumber: '',
      timeZone: '',
      type: 'ERK_SMF',
      tag: 'ERK_SMF',
      version: '1.4',
      vpnConfig: [],
      provisioningInterfaces: [
        {
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          destIPAddr: undefined,
          destPort: undefined,
          dscp: '32',
          name: '',
          ownnetid: 'id1',
          reqState: 'ACTIVE',
          secinfoid: 'Tk9ORQ',
          state: '',
          ccnodename: '',
          mdf2NodeName: '',
          mdf3NodeName: '',
        }],
    });
  });

  it('should return form values including system, provisioning interfaces, and data interfaces', () => {
    let ref: any;

    const data = createAccessFunctionBasedOnTemplate(templates.ERK_SGW);

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={data}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_SGW}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(ref?.getValues()).toEqual({
      type: 'ERK_SGW',
      country: '',
      city: '',
      contact: '',
      contentDeliveryVia: 'directFromAF',
      description: '',
      id: undefined,
      insideNat: false,
      liInterface: 'ALL',
      model: '',
      name: '',
      preprovisioningLead: '000:00',
      serialNumber: '',
      stateName: '',
      timeZone: '',
      version: '',
      tag: 'ERK_SGW',
      vpnConfig: [],
      provisioningInterfaces: [{
        destIPAddr: '',
        destPort: '0',
        dscp: '32',
        id: '3.accessFunctionInterface',
        name: undefined,
        ownnetid: 'id1',
        reqState: 'ACTIVE',
        secinfoid: 'Tk9ORQ',
        state: '',
      }],
      dataInterfaces: [{
        destIPAddr: '',
        destPort: '0',
        dscp: '32',
        id: '2.accessFunctionInterface',
        name: undefined,
        ownnetid: 'id1',
        secinfoid: 'Tk9ORQ',
      }],
      contentInterfaces: [{
        destIPAddr: '',
        destPort: '0',
        id: '4.accessFunctionInterface',
        name: undefined,
        ownnetid: 'id1',
        secinfoid: 'Tk9ORQ',
        transport: 'UDP',
      }],
    });
  });

  it('should not return values on sections that do not have a form', () => {
    // Ericsson MCPTT AF has sections with 0 or 1 interfaces
    let ref: any;

    const distributorConfigurations: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X2,
    ];

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={partialAccessFunction}
        distributorConfigs={distributorConfigurations}
        accessFunctionByType={{}}
        template={templates.ERK_MCPTT}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    const formValues = ref?.getValues();
    expect(formValues).toEqual(expect.objectContaining({
      type: 'ERK_MCPTT',
    }));
    expect(formValues.provisioningInterfaces).toBeUndefined();
    expect(formValues.dataInterfaces).toBeUndefined();
    expect(formValues.contentInterfaces).toBeUndefined();
  });

  it('should check if form is valid', () => {
    let ref: any;

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(ref?.isValid(true, true)).toEqual(false);
  });

  it('should be valid when sections have no forms', () => {
    // Ericsson MCPTT AF has sections with 0 or 1 interfaces
    let ref: any;

    const distributorConfigurations: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X2,
    ];

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        ref={(instance) => { ref = instance; }}
        data={partialAccessFunction}
        distributorConfigs={distributorConfigurations}
        accessFunctionByType={{}}
        template={templates.ERK_MCPTT}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    fireEvent.change(screen.getAllByDisplayValue('')[0], { target: { value: 'AF Name 1' } });
    fireEvent.change(screen.getAllByDisplayValue('Select a time zone')[0], { target: { value: 'America/New_York' } });

    expect(ref?.isValid(true, true)).toEqual(true);
  });

  it('should display the values from the data', () => {
    partialAccessFunction.name = 'AF name';

    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByDisplayValue('AF name')).toBeVisible();
  });

  it('should display the SS8 Distributors Configuration as an empty table', () => {
    render(
      <AccessFunctionForm
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        template={templates.ERK_UPF}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should show an error message on Save if the Network ID was not found', () => {
    const formRef = createRef<Ref>();

    partialAccessFunction = {
      ...partialAccessFunction,
      type: 'MVNR_PGW',
      tag: 'MVNR_PGW',
      contentDeliveryVia: 'trafficManager',
      contentInterfaces: [{
        id: '1',
        name: 'name1',
        appProtocol: 'MVRN_UAG_PGW',
        lbid: 'MQ',
        destIPAddr: '1.1.1.1',
        destPort: '1',
        ownnetid: 'U1M4XzMwNzA',
        reqState: 'ACTIVE',
        state: 'INACTIVE',
        traceLevel: '1',
        type: 'MVNRPGW_TCP',
        dscp: '63',
      }],
    };

    render(
      <AccessFunctionForm
        ref={formRef}
        title="Access Function form title"
        isEditing
        data={partialAccessFunction}
        distributorConfigs={[]}
        accessFunctionByType={{}}
        networks={mockNetworks}
        template={templates.MVNR_PGW}
        onFieldUpdate={jest.fn()}
        onClickCancelClose={jest.fn()}
      />,
    );

    expect(screen.getByLabelText('Network ID:*')).toHaveDisplayValue('Network ID not found');

    act(() => {
      expect(formRef.current?.isValid(true, true)).toEqual(false);
    });

    expect(screen.getByText('Please select another network ID.')).toBeVisible();
  });

  describe('when try saving an AF with repeated name', () => {
    const formRef = createRef<Ref>();

    const accessFunctionByType = {
      ERK_UPF: mockAccessFunctionsUPF,
      ERK_SMF: mockAccessFunctionsSMF,
    };

    describe('when access function type is ERK_UPF', () => {
      it('should show error message and not call save onClickSave callback to same type access function', async () => {
        const onClickSave = jest.fn();
        const { container } = render(
          <AccessFunctionForm
            ref={formRef}
            title="Access Function form title"
            isEditing
            data={partialAccessFunction}
            distributorConfigs={[]}
            accessFunctionByType={accessFunctionByType}
            template={templates.ERK_UPF}
            onFieldUpdate={jest.fn()}
            onClickSave={onClickSave}
            onClickCancelClose={jest.fn()}
          />,
        );

        fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: mockAccessFunctionsUPF[0].name } });
        fireEvent.change(getByDataTest(container, 'input-tag'), { target: { value: 'ERK_UPF' } });
        fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

        act(() => {
          expect(formRef.current?.isValid(true, true)).toEqual(false);
        });

        expect(screen.getByText('An AF with this name already exists.')).toBeVisible();
      });

      it('should show error message and not call save onClickSave callback to different type access function', async () => {
        const onClickSave = jest.fn();
        const { container } = render(
          <AccessFunctionForm
            ref={formRef}
            title="Access Function form title"
            isEditing
            data={partialAccessFunction}
            distributorConfigs={[]}
            accessFunctionByType={accessFunctionByType}
            template={templates.ERK_UPF}
            onFieldUpdate={jest.fn()}
            onClickSave={onClickSave}
            onClickCancelClose={jest.fn()}
          />,
        );

        fireEvent.change(getByDataTest(container, 'input-name'), { target: { value: mockAccessFunctionsSMF[0].name } });
        fireEvent.change(getByDataTest(container, 'input-tag'), { target: { value: 'ERK_UPF' } });
        fireEvent.change(getByDataTest(container, 'input-timeZone'), { target: { value: 'America/New_York' } });

        act(() => {
          expect(formRef.current?.isValid(true, true)).toEqual(false);
        });

        expect(screen.getByText('An AF with this name already exists.')).toBeVisible();
      });
    });
  });
});
