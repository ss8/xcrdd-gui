import React, {
  forwardRef, useCallback, useImperativeHandle, useRef,
} from 'react';
import {
  FormTemplate,
  FormTemplateSection,
  FormTemplateValidator,
  AFFormTemplateFormSectionName,
} from 'data/template/template.types';
import * as types from 'data/accessFunction/accessFunction.types';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { VpnConfigTemplate } from 'data/vpnTemplate/vpnTemplate.types';
import { VpnConfig } from 'data/vpnConfig/vpnConfig.types';
import { Network } from 'data/network/network.types';
import { FormLayoutContent, FormLayoutFooter, FormLayout } from 'components/FormLayout';
import FormButtons from 'components/FormButtons';
import FormPanel from 'components/FormPanel';
import Form, { CustomFormFieldComponent, CustomValidationFunction } from 'shared/form';
import { validateNonRepeatedProperty } from 'utils/validators/listValidators';
import { shouldValidateInterfacesQuantity } from 'routes/AccessFunction/utils/accessFunctionTemplate.adapter';
import { AppToaster, showError } from 'shared/toaster';
import FormSection from '../FormSection';
import getExistingAccessFunctionNames from '../../utils/getExistingAccessFunctionNames';
import MultifacetedFormSection from '../MultifacetedFormSection';
import './access-function-form.scss';
import { accessFunctionPortListValidator } from '../../utils/accessFunctionPortListValidator';
import validateSameNetworkIds from '../../utils/validateSameNetworkIds';
import { getSubsectionName } from '../../utils/accessFunctionTemplate.adapter';
import validateUniqueProperty from '../../utils/validateUniqueProperty';

type Props = {
  title: string,
  isEditing: boolean,
  template: FormTemplate,
  data: Partial<types.AccessFunctionFormData>
  distributorConfigs: DistributorConfigurationTableData[],
  accessFunctionByType: { [key: string]: types.AccessFunction[] },
  vpnConfigTemplates?: VpnConfigTemplate[],
  vpnConfigs?: VpnConfig[],
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent },
  networks?: Network[],
  is5gcVcpEnabled?: boolean,
  shouldValidateOnRender?: boolean,
  onClickSave?: () => void
  onClickEdit?: () => void
  onClickCancelClose: () => void
  onFieldUpdate?: types.AccessFunctionFieldUpdateCallback
  onDistributorUpdate?: (distributorConfiguration: DistributorConfigurationTableData) => void
  onTabAdd?: (formSectionName: types.AccessFunctionInterfaceFormSectionType) => void,
  onTabRemove?: (id: string | number, formSectionName: types.AccessFunctionInterfaceFormSectionType) => void,
}

export type Ref = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => types.AccessFunctionFormData
}

export const AccessFunctionForm = forwardRef<Ref, Props>(({
  title,
  isEditing,
  template,
  data,
  distributorConfigs,
  accessFunctionByType,
  vpnConfigTemplates,
  vpnConfigs,
  customFieldComponentMap,
  networks,
  is5gcVcpEnabled,
  shouldValidateOnRender = false,
  onClickSave,
  onClickEdit,
  onClickCancelClose,
  onFieldUpdate = () => null,
  onDistributorUpdate,
  onTabAdd,
  onTabRemove,
}: Props, ref) => {
  const generalRef = useRef<Form>(null);
  const systemInformationRef = useRef<Form>(null);
  const provisioningInterfacesRef = useRef<Form>(null);
  const dataInterfacesRef = useRef<Form>(null);
  const contentInterfacesRef = useRef<Form>(null);

  const provisioningInterfacesSubsectionName = getSubsectionName(data, template, 'provisioningInterfaces');
  const dataInterfacesSubsectionName = getSubsectionName(data, template, 'dataInterfaces');
  const contentInterfacesSubsectionName = getSubsectionName(data, template, 'contentInterfaces');

  const isValid = (forceValidation = false, forceTouched = true) => {
    const isGeneralValid = generalRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isSystemInformationValid = systemInformationRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isProvisioningInterfacesValid =
      provisioningInterfacesRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isDataInterfacesValid =
      dataInterfacesRef.current?.isValid(forceValidation, forceTouched) ?? true;
    const isContentInterfacesValid =
      contentInterfacesRef.current?.isValid(forceValidation, forceTouched) ?? true;

    const hasAtLeastOneInterface =
      dataInterfacesRef.current?.getValues().afInterfaceFormData != null
      || provisioningInterfacesRef.current?.getValues().afInterfaceFormData != null
      || contentInterfacesRef.current?.getValues().afInterfaceFormData != null;

    const isInterfacesQuantityValid = shouldValidateInterfacesQuantity(
      template,
      provisioningInterfacesSubsectionName,
      dataInterfacesSubsectionName,
      contentInterfacesSubsectionName,
    ) ? hasAtLeastOneInterface : true;

    AppToaster.clear();

    if (!isInterfacesQuantityValid) {
      showError(renderRequiredInterfaceErrorMessage(), 5000);
    }

    return isGeneralValid &&
    isSystemInformationValid &&
    isProvisioningInterfacesValid &&
    isDataInterfacesValid &&
    isContentInterfacesValid &&
    isInterfacesQuantityValid;
  };

  const getValues = (): types.AccessFunctionFormData => {
    const formValues: types.AccessFunctionFormData = {
      ...generalRef.current?.getValues(),
      ...systemInformationRef.current?.getValues(),
      vpnConfig: [],
    };

    const provisioningInterfaces = provisioningInterfacesRef.current?.getValues();
    if (provisioningInterfaces != null) {
      if (provisioningInterfaces.afInterfaceFormData) {
        formValues.provisioningInterfaces = provisioningInterfaces.afInterfaceFormData;
      }
      if (provisioningInterfaces.vpnConfigData) {
        formValues.vpnConfig?.push(provisioningInterfaces.vpnConfigData);
      }
    }

    const dataInterfaces = dataInterfacesRef.current?.getValues();
    if (dataInterfaces !== null) {
      if (dataInterfaces.afInterfaceFormData) {
        formValues.dataInterfaces = dataInterfaces.afInterfaceFormData;
      }
      if (dataInterfaces.vpnConfigData) {
        formValues.vpnConfig?.push(dataInterfaces.vpnConfigData);
      }
    }

    const contentInterfaces = contentInterfacesRef.current?.getValues();
    if (contentInterfaces !== null) {
      if (contentInterfaces.afInterfaceFormData) {
        formValues.contentInterfaces = contentInterfaces.afInterfaceFormData;
      }
      if (contentInterfaces.vpnConfigData) {
        formValues.vpnConfig?.push(contentInterfaces.vpnConfigData);
      }
    }

    return formValues;
  };

  const nonRepeatedPropertyValidator = useCallback<CustomValidationFunction>((
    _propFields?: unknown,
    _stateFields?: unknown,
    _value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
    form?: Form | undefined,
  ) => {

    const valid = validateNonRepeatedProperty(
      form?.getValues().name,
      getExistingAccessFunctionNames(accessFunctionByType, form?.getValues()?.id),
    );

    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  }, [accessFunctionByType]);

  const portListValidator = (
    _propFields?: unknown,
    stateFields?: unknown,
    value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
    _form?: Form | undefined,
  ) => {
    const fields = stateFields as { [fieldName: string]: { value: string } };
    const numberOfPorts = fields?.maxx2connsperaf.value ?? '1';

    const valid = accessFunctionPortListValidator(value, +numberOfPorts);

    let message = '';
    if (!valid) {
      if (numberOfPorts === '1') {
        message = validator?.errorMessages?.singlePort ?? '';
      } else {
        message = validator?.errorMessages?.multiplePorts ?? '';
        message = message.replace('{{ports}}', numberOfPorts);
      }
    }

    return { isValid: valid, errorMessage: message };
  };

  const sameNetworkIdPropertyValidator = (
    _propFields?: unknown,
    _stateFields?: unknown,
    _value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
  ) => {

    const formSectionName = validator?.formSectionName as AFFormTemplateFormSectionName;
    const interfaces = getValues()[formSectionName] || [];

    const valid = validateSameNetworkIds(interfaces);

    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  };

  const uniquePropertyValidator = (
    _propFields?: unknown,
    _stateFields?: unknown,
    _value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
  ) => {
    const formSectionName = validator?.formSectionName as AFFormTemplateFormSectionName;
    const interfaces = getValues()[formSectionName] || [];

    const valid = validateUniqueProperty('ipduid', interfaces);

    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  };

  const networkIdNotFoundValidator = (
    _propFields?: unknown,
    _stateFields?: unknown,
    value?: string,
    _lastSavedValue?: string,
    validator?: FormTemplateValidator,
  ) => {
    const network = networks?.find(({ id }) => id === value);
    const valid = network != null;

    let message = '';
    if (!valid) {
      message = validator?.errorMessage || '';
    }

    return { isValid: valid, errorMessage: message };
  };

  const customValidationFunctionMap = {
    nonRepeatedPropertyValidator,
    portListValidator,
    sameNetworkIdPropertyValidator,
    uniquePropertyValidator,
    networkIdNotFoundValidator,
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const renderRequiredInterfacesInstructionalText = (): JSX.Element | null => {
    if (shouldValidateInterfacesQuantity(
      template,
      provisioningInterfacesSubsectionName,
      dataInterfacesSubsectionName,
      contentInterfacesSubsectionName,
    )) {
      return (
        <p className="access-function-form-required-interfaces-instructional-text">
          For proper configuration of this Access Function, add at least 1 interface from the options below.
        </p>
      );
    }

    return null;
  };

  const renderRequiredInterfaceErrorMessage = (): JSX.Element => {
    return (
      <React.Fragment>
        <p><strong>Unable to create Access Function</strong></p>
        <p>Add at least 1 interface to the AF before saving.</p>
      </React.Fragment>
    );
  };

  const renderFormPanel = (formTitle: string | null = null) => (
    <FormPanel title={formTitle}>
      <FormButtons
        isEditing={isEditing}
        onClickCancel={onClickCancelClose}
        onClickSave={onClickSave}
        onClickEdit={onClickEdit}
        onClickClose={onClickCancelClose}
      />
    </FormPanel>
  );

  return (
    <div className="access-function-form" data-testid="AccessFunctionForm">
      <FormLayout>
        <FormLayoutContent>
          {renderFormPanel(title)}
          <FormSection
            ref={generalRef}
            name={template.formId ?? template.key}
            data={data}
            formSectionName="general"
            templateSection={template.general as FormTemplateSection}
            onFieldUpdate={onFieldUpdate}
            customValidationFunctionMap={customValidationFunctionMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
          <FormSection
            ref={systemInformationRef}
            name={template.formId ?? template.key}
            title="System Information"
            data={data}
            formSectionName="systemInformation"
            templateSection={template.systemInformation as FormTemplateSection}
            onFieldUpdate={onFieldUpdate}
            customValidationFunctionMap={customValidationFunctionMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
          {renderRequiredInterfacesInstructionalText()}
          <MultifacetedFormSection
            ref={provisioningInterfacesRef}
            title="Provisioning Interface (X1)"
            template={template}
            formSectionName={AFFormTemplateFormSectionName.provisioningInterfaces}
            formSubsectionName={provisioningInterfacesSubsectionName}
            distributorConfigs={distributorConfigs}
            vpnConfigTemplates={vpnConfigTemplates}
            vpnConfigs={vpnConfigs}
            data={data}
            customFieldComponentMap={customFieldComponentMap}
            isEditing={isEditing}
            is5gcVcpEnabled={is5gcVcpEnabled}
            onDistributorUpdate={onDistributorUpdate}
            onFieldUpdate={onFieldUpdate}
            onTabAdd={onTabAdd}
            onTabRemove={onTabRemove}
            customValidationFunctionMap={customValidationFunctionMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
          <MultifacetedFormSection
            ref={dataInterfacesRef}
            title="Delivery Interface (X2)"
            template={template}
            formSectionName={AFFormTemplateFormSectionName.dataInterfaces}
            formSubsectionName={dataInterfacesSubsectionName}
            distributorConfigs={distributorConfigs}
            vpnConfigTemplates={vpnConfigTemplates}
            vpnConfigs={vpnConfigs}
            data={data}
            customFieldComponentMap={customFieldComponentMap}
            isEditing={isEditing}
            is5gcVcpEnabled={is5gcVcpEnabled}
            onDistributorUpdate={onDistributorUpdate}
            onFieldUpdate={onFieldUpdate}
            onTabAdd={onTabAdd}
            onTabRemove={onTabRemove}
            customValidationFunctionMap={customValidationFunctionMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
          <MultifacetedFormSection
            ref={contentInterfacesRef}
            title="Content Interface (X3)"
            template={template}
            formSectionName={AFFormTemplateFormSectionName.contentInterfaces}
            formSubsectionName={contentInterfacesSubsectionName}
            distributorConfigs={distributorConfigs}
            vpnConfigTemplates={vpnConfigTemplates}
            vpnConfigs={vpnConfigs}
            data={data}
            customFieldComponentMap={customFieldComponentMap}
            isEditing={isEditing}
            is5gcVcpEnabled={is5gcVcpEnabled}
            onDistributorUpdate={onDistributorUpdate}
            onFieldUpdate={onFieldUpdate}
            onTabAdd={onTabAdd}
            onTabRemove={onTabRemove}
            customValidationFunctionMap={customValidationFunctionMap}
            shouldValidateOnRender={shouldValidateOnRender}
          />
        </FormLayoutContent>
        <FormLayoutFooter>
          {renderFormPanel()}
        </FormLayoutFooter>
      </FormLayout>
    </div>
  );
});

export default AccessFunctionForm;
