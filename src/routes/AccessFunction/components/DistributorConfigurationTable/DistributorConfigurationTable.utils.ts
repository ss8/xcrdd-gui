import cloneDeep from 'lodash.clonedeep';
import { ColDef } from '@ag-grid-enterprise/all-modules';
import * as types from 'data/distributorConfiguration/distributorConfiguration.types';
import { toDistributorConfigurationListenerWhiteList } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { FormTemplateSection } from 'data/template/template.types';

export const findDistributorById = (
  rowData: types.DistributorConfigurationTableData[],
  id: string,
):types.DistributorConfigurationTableData | undefined => {
  return rowData.find((data) => data.id === id);
};

export const updateDistributorWhiteListIps = (
  distributorConfigs: types.DistributorConfigurationTableData,
  newAFIpsList: types.DistributorConfigurationListenerAfWhiteList[],
): types.DistributorConfigurationTableData => {
  const clonedDistributorConfigs = cloneDeep(distributorConfigs);
  return {
    ...clonedDistributorConfigs,
    listener: {
      ...clonedDistributorConfigs?.listener,
      afWhitelist: newAFIpsList.map(toDistributorConfigurationListenerWhiteList),
    },
  };
};

export const adaptColumnDefsBasedInTemplate = (
  distributorConfigurationTableColumnDefs: ColDef[],
  templateSection: FormTemplateSection,
) : ColDef[] => {
  const hideAFWhitelist = (templateSection?.fields.afWhitelist?.hide === 'true');
  if (hideAFWhitelist) {
    return distributorConfigurationTableColumnDefs.filter((colDef) => (
      colDef.field !== 'afWhitelistFormatted'));
  }
  return distributorConfigurationTableColumnDefs;

};
