import React, { useEffect, useState, FC } from 'react';
import { AgGridReact } from '@ag-grid-community/react';
import './distributor-configuration-table.scss';
import {
  distributorConfigurationTableAutoGroupColumnDef,
  distributorConfigurationTableColumnDefs,
  distributorsIPsWhiteListEditableTableColumnDefs,
  distributorsIPsWhiteListReadOnlyTableColumnDefs,
} from 'routes/AccessFunction/AccessFunction.types';
import {
  DistributorConfigurationListenerAfWhiteList,
  DistributorConfigurationTableData,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import { FormTemplateSection } from 'data/template/template.types';
import EditAllowedIPsDialog from '../../../../components/EditAllowedIPsDialog';
import * as utils from './DistributorConfigurationTable.utils';

type Props = {
  onDistributorUpdate?: (distributorConfiguration: DistributorConfigurationTableData) => void,
  isEditing: boolean,
  rowData: DistributorConfigurationTableData[],
  templateSection: FormTemplateSection,
}

const DistributorConfigurationTable: FC<Props> = ({
  rowData,
  templateSection,
  isEditing,
  onDistributorUpdate,
}: Props) => {
  const [selectedDistributor, setSelectedDistributor] = useState<DistributorConfigurationTableData>();
  const [selectedDistributorId, setSelectedDistributorId] = useState<string>('');

  const adaptedDistributorConfigurationTableColDefs = utils.adaptColumnDefsBasedInTemplate(
    distributorConfigurationTableColumnDefs,
    templateSection,
  );

  useEffect(() => {
    if (selectedDistributorId) {
      setSelectedDistributor(utils.findDistributorById(rowData, selectedDistributorId));
    }
  }, [selectedDistributorId, rowData]);

  const handleSelectAfIp = (params: DistributorConfigurationTableData) => {
    setSelectedDistributorId(params.id);
  };

  const handleUpdateIpList = (newAFIpsList: DistributorConfigurationListenerAfWhiteList[]) => {
    if (!selectedDistributor || !onDistributorUpdate) { return; }

    onDistributorUpdate(utils.updateDistributorWhiteListIps(selectedDistributor, newAFIpsList));

    setSelectedDistributor(undefined);
    setSelectedDistributorId('');
  };

  const handleCloseEditIPsDialog = () => {
    setSelectedDistributor(undefined);
    setSelectedDistributorId('');
  };

  const handleGridReady = (event: any) => {
    event.api.sizeColumnsToFit();
  };

  let ipTableColumnDefs = distributorsIPsWhiteListReadOnlyTableColumnDefs;
  if (isEditing) {
    ipTableColumnDefs = distributorsIPsWhiteListEditableTableColumnDefs;
  }

  return (
    <div className="ag-theme-balham distributor-configuration-table-theme">
      <AgGridReact
        onGridReady={handleGridReady}
        floatingFilter
        groupHideOpenParents
        groupDefaultExpanded={1}
        autoGroupColumnDef={distributorConfigurationTableAutoGroupColumnDef}
        columnDefs={adaptedDistributorConfigurationTableColDefs}
        rowData={rowData}
        gridOptions={{
          context: {
            onSelectAfIp: handleSelectAfIp,
          },
        }}
      />
      <EditAllowedIPsDialog
        isOpen={!!selectedDistributor}
        ipsWhiteList={selectedDistributor?.listener?.afWhitelist || []}
        isEditing={isEditing}
        ipTableColumnDefs={ipTableColumnDefs}
        distributorName={selectedDistributor?.name}
        listeningIpAddress={selectedDistributor?.ipAddress}
        listeningPort={selectedDistributor?.port}
        onSubmit={handleUpdateIpList}
        onCancel={handleCloseEditIPsDialog}
      />
    </div>
  );
};

export default DistributorConfigurationTable;
