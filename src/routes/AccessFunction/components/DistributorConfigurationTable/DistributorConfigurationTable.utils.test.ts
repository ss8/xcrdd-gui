import {
  AccessFunction,
} from 'data/accessFunction/accessFunction.types';
import {
  distributorConfigurationTableColumnDefs,
} from 'routes/AccessFunction/AccessFunction.types';
import {
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  FormTemplate,
  FormTemplateSection,
} from 'data/template/template.types';
import { mockTemplates, findMockTemplates } from '__test__/mockTemplates';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import {
  findDistributorById,
  updateDistributorWhiteListIps,
  adaptColumnDefsBasedInTemplate,
} from './DistributorConfigurationTable.utils';

const DISTRIBUTORS_MOCK: DistributorConfigurationTableData[] = [
  {
    afType: 'ERK_UPF',
    afProtocol: 'ERKUPF_X3',
    ipAddress: '172.31.11.38',
    port: '1111',
    destinations: [
      {
        id: '1',
        ipAddress: '172.31.11.48',
        keepaliveInterval: '60',
        keepaliveRetries: '6',
        numOfConnections: '1',
        port: '2222',
        status: 'INACTIVE',
        requiredState: 'ACTIVE',
        secInfoId: '',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      },
    ],
    id: '0.MQ',
    instanceId: '1',
    listener: {
      afProtocol: 'ERKUPF_X3',
      id: '1',
      ipAddress: '172.11.11.38',
      keepaliveTimeout: '30',
      maxConcurrentConnection: '4096',
      port: '22222',
      requiredState: 'ACTIVE',
      secInfoId: 'RFRMU1NlcnZlcg',
      status: 'INACTIVE',
      synchronizationHeader: 'NONE',
      numberOfParsers: '2',
      transport: 'UDP',
      afWhitelist: [
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_1',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_2',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_3',
        },
      ],
    },
    moduleName: 'ERKUPFX3IM',
    name: 'DC-UPF-X3-3',
  },
  {
    afType: 'ERK_UPF',
    destinations: [
      {
        id: '1',
        ipAddress: '172.31.11.48',
        keepaliveInterval: '60',
        keepaliveRetries: '6',
        numOfConnections: '1',
        port: '3333',
        status: 'INACTIVE',
        requiredState: 'ACTIVE',
        secInfoId: '',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      },
    ],
    id: '1.MQ',
    instanceId: '1',
    afProtocol: 'ERKUPF_X3',
    ipAddress: '172.33.11.38',
    port: '45165',
    listener: {
      afProtocol: 'ERKUPF_X3',
      id: '1',
      ipAddress: '172.44.11.38',
      keepaliveTimeout: '30',
      maxConcurrentConnection: '4096',
      port: '45165',
      requiredState: 'ACTIVE',
      secInfoId: 'RFRMU1NlcnZlcg',
      status: 'INACTIVE',
      synchronizationHeader: 'NONE',
      numberOfParsers: '2',
      transport: 'UDP',
      afWhitelist: [
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_1',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_2',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_3',
        },
      ],
    },
    moduleName: 'ERKUPFX3IM',
    name: 'DC-UPF-X3-3',
  },
  {
    afType: 'ERK_UPF',
    destinations: [
      {
        id: '1',
        ipAddress: '172.31.11.48',
        keepaliveInterval: '60',
        keepaliveRetries: '6',
        numOfConnections: '1',
        port: '5555',
        status: 'INACTIVE',
        requiredState: 'ACTIVE',
        secInfoId: '',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      },
    ],
    id: '2.MQ',
    instanceId: '1',
    afProtocol: 'ERKUPF_X3',
    ipAddress: '172.55.11.38',
    port: '45165',
    listener: {
      afProtocol: 'ERKUPF_X3',
      id: '1',
      ipAddress: '172.66.11.38',
      keepaliveTimeout: '30',
      maxConcurrentConnection: '4096',
      port: '45165',
      requiredState: 'ACTIVE',
      secInfoId: 'RFRMU1NlcnZlcg',
      status: 'INACTIVE',
      synchronizationHeader: 'NONE',
      numberOfParsers: '2',
      transport: 'UDP',
      afWhitelist: [
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_1',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_2',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_3',
        },
      ],
    },
    moduleName: 'ERKUPFX3IM',
    name: 'DC-UPF-X3-3',
  },
  {
    afType: 'ERK_UPF',
    destinations: [
      {
        id: '1',
        ipAddress: '172.31.11.48',
        keepaliveInterval: '60',
        keepaliveRetries: '6',
        numOfConnections: '1',
        port: '6666',
        status: 'INACTIVE',
        requiredState: 'ACTIVE',
        secInfoId: '',
        transport: 'TCP',
        protocol: 'PASSTHRU',
      },
    ],
    id: '3.MQ',
    instanceId: '1',
    afProtocol: 'ERKUPF_X3',
    ipAddress: '172.77.11.38',
    port: '45165',
    listener: {
      afProtocol: 'ERKUPF_X3',
      id: '1',
      ipAddress: '172.88.11.38',
      keepaliveTimeout: '30',
      maxConcurrentConnection: '4096',
      port: '45165',
      requiredState: 'ACTIVE',
      secInfoId: 'RFRMU1NlcnZlcg',
      status: 'INACTIVE',
      synchronizationHeader: 'NONE',
      numberOfParsers: '2',
      transport: 'UDP',
      afWhitelist: [
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_1',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_2',
        },
        {
          afAddress: '10.0.0.1',
          afId: 'eEFGXzEyMTU5NDIxODk',
          afName: 'ERK_UPF_3',
        },
      ],
    },
    moduleName: 'ERKUPFX3IM',
    name: 'DC-UPF-X3-3',
  },
];

const ACCESS_FUNCTIONS_MOCK: AccessFunction[] = [
  {
    id: 'id1',
    tag: 'tag1',
    dxOwner: 'dxOwner1',
    dxAccess: 'dxAccess1',
    name: 'afName1',
    contact: { id: 'contactId1', name: 'contact1' },
    insideNat: false,
    liInterface: 'INI1',
    model: 'model1',
    preprovisioningLead: 'preprovisioningLead1',
    serialNumber: 'serialNumber1',
    timeZone: 'timeZone1',
    type: 'ERK_UPF',
    version: 'version1',
    latitude: 'latitude1',
    longitude: 'longitude1',
    status: 'ACTIVE',
  },
  {
    id: 'id2',
    tag: 'tag1',
    dxOwner: 'dxOwner1',
    dxAccess: 'dxAccess1',
    name: 'afName2',
    contact: { id: 'contactId1', name: 'contact1' },
    insideNat: false,
    liInterface: 'INI1',
    model: 'model1',
    preprovisioningLead: 'preprovisioningLead1',
    serialNumber: 'serialNumber1',
    timeZone: 'timeZone1',
    type: 'ERK_SMF',
    version: 'version1',
    latitude: 'latitude1',
    longitude: 'longitude1',
    status: 'ACTIVE',
  },
];

describe('DistributorConfigurationTable.utils', () => {
  describe('findDistributorById()', () => {
    it('should find an distributor by Id', () => {
      const distributorFound = findDistributorById(DISTRIBUTORS_MOCK, '1.MQ');
      expect(distributorFound).toEqual(DISTRIBUTORS_MOCK[1]);
    });

    it('should not find an distributor by Id', () => {
      const distributorFound = findDistributorById(
        DISTRIBUTORS_MOCK,
        'invalidID',
      );
      expect(distributorFound).toEqual(undefined);
    });
  });

  describe('updateDistributorWhiteListIps()', () => {
    it('should return a Distributor with whitelist ips updated', () => {
      const distributorBase = DISTRIBUTORS_MOCK[0];
      const newAfIpWhitelist = DISTRIBUTORS_MOCK[1].listener.afWhitelist;
      const expectedDistributorWithNewAfIpWhitelist = {
        ...distributorBase,
        listener: {
          ...distributorBase.listener,
          afWhitelist: newAfIpWhitelist,
        },
      };

      const distributorWithNewAfIpWhitelist = updateDistributorWhiteListIps(
        distributorBase,
        newAfIpWhitelist,
      );

      expect(distributorWithNewAfIpWhitelist).toEqual(
        expectedDistributorWithNewAfIpWhitelist,
      );
    });
  });

  describe('adaptColumnDefsBasedInTemplate()', () => {
    it('should return table definition with afWhitelist when template does not have afWhitelist.hide=true', () => {
      const rawTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson MCPTT');
      const afTemplate = JSON.parse(rawTemplate[0].template) as FormTemplate;

      const distributorConfigurationColDefs = adaptColumnDefsBasedInTemplate(
        distributorConfigurationTableColumnDefs,
        afTemplate.dataInterfaces as FormTemplateSection,
      );
      expect(distributorConfigurationColDefs).toEqual(distributorConfigurationTableColumnDefs);
    });

    it('should return table definition without afWhitelist when template has afWhitelist.hide=true', () => {
      const rawTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson MCPTT');
      const afTemplate = JSON.parse(rawTemplate[0].template) as FormTemplate;

      const distributorConfigurationColDefs = adaptColumnDefsBasedInTemplate(
        distributorConfigurationTableColumnDefs,
        afTemplate.contentInterfaces as FormTemplateSection,
      );

      const expected = [
        distributorConfigurationTableColumnDefs[0],
        distributorConfigurationTableColumnDefs[1],
        distributorConfigurationTableColumnDefs[2],
      ];
      expect(distributorConfigurationColDefs).toEqual(expected);
    });

  });
});
