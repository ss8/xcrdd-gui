import React from 'react';
import {
  render, fireEvent, screen,
} from '@testing-library/react';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import {
  Column, ColumnApi, GridApi, RowNode,
} from '@ag-grid-enterprise/all-modules';
import AllowedAccessFunctionIpCell from './AllowedAccessFunctionIpCell';

const AFS_WHITELIST_MOCK: DistributorConfigurationListenerAfWhiteList[] = [
  {
    afName: 'afName1',
    afAddress: '1.1.1.1',
    afId: 'id1',
  },
  {
    afName: 'afName2',
    afAddress: '2.2.2.2',
    afId: 'id2',
  },
  {
    afName: 'afName3',
    afAddress: '3.3.3.3',
    afId: 'id3',
  },
  {
    afName: 'afName4',
    afAddress: '4.4.4.4',
    afId: 'id4',
  },
  {
    afName: 'afName5',
    afAddress: '5.5.5.5',
    afId: 'id5',
  },
];

describe('<AllowedAccessFunctionIpCell />', () => {
  describe('when data is undefined', () => {
    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={undefined}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp: jest.fn(),
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(document.querySelectorAll('span').length).toBe(1);
    });
  });

  describe('when data has afWhitelist has length = 0', () => {
    const onSelectAfIp = jest.fn();

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist: [],
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('all')).toBeVisible();
    });
    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));
      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist: [],
        },
      });
    });
  });

  describe('when data has afWhitelist has length = 1', () => {
    const onSelectAfIp = jest.fn();
    const afWhitelist = [AFS_WHITELIST_MOCK[0]];

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist,
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('1.1.1.1')).toBeVisible();
    });

    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));
      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist,
        },
      });
    });
  });

  describe('when data has afWhitelist has length = 2', () => {
    const onSelectAfIp = jest.fn();
    const afWhitelist = [AFS_WHITELIST_MOCK[0], AFS_WHITELIST_MOCK[1]];

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist,
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('1.1.1.1, 2.2.2.2')).toBeVisible();
    });

    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));
      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist,
        },
      });
    });
  });

  describe('when data has afWhitelist has length = 3', () => {
    const onSelectAfIp = jest.fn();
    const afWhitelist = [AFS_WHITELIST_MOCK[0], AFS_WHITELIST_MOCK[1], AFS_WHITELIST_MOCK[2]];

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist,
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('1.1.1.1, 2.2.2.2, 3.3.3.3')).toBeVisible();
    });
    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));
      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist,
        },
      });
    });
  });

  describe('when data has afWhitelist has length = 4', () => {
    const onSelectAfIp = jest.fn();
    const afWhitelist = [AFS_WHITELIST_MOCK[0], AFS_WHITELIST_MOCK[1], AFS_WHITELIST_MOCK[2], AFS_WHITELIST_MOCK[3]];

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist,
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('1.1.1.1, 2.2.2.2, 3.3.3.3 and 1 other')).toBeVisible();
    });

    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));
      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist,
        },
      });
    });
  });

  describe('when data has afWhitelist has length = 5', () => {
    const onSelectAfIp = jest.fn();

    beforeEach(async () => {
      render(
        <AllowedAccessFunctionIpCell
          data={{
            listener: {
              afWhitelist: AFS_WHITELIST_MOCK,
            },
          }}
          value="value"
          colDef={{}}
          column={{} as Column}
          node={{} as RowNode}
          rowIndex={1}
          context={{
            onSelectAfIp,
          }}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          valueFormatted={{}}
          formatValue={jest.fn}
          getValue={jest.fn}
          setValue={jest.fn}
          api={{} as GridApi}
          columnApi={{} as ColumnApi}
          refreshCell={jest.fn}
          eParentOfValue={{} as HTMLElement}
          addRenderedRowListener={jest.fn}
        />,
      );
    });

    it('should render the empty version of the AllowedAccessFunctionIpCell', async () => {
      expect(await screen.findByText('1.1.1.1, 2.2.2.2, 3.3.3.3 and 2 others')).toBeVisible();
    });

    it('should click AllowedAccessFunctionIpCell button and call context.onSelectAfIp', async () => {
      fireEvent.click(await screen.findByRole('button'));

      expect(onSelectAfIp).toHaveBeenCalledWith({
        listener: {
          afWhitelist: AFS_WHITELIST_MOCK,
        },
      });
    });
  });
});
