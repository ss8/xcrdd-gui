import React from 'react';
import {
  render,
  RenderResult,
  screen,
  waitFor,
} from '@testing-library/react';
import {
  FormTemplateSection,
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
} from 'data/template/template.types';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { mockDistributorConfigurationTableData } from '__test__/mockDistributorConfigurationTableData';
import { mockTemplates, findMockTemplates } from '__test__/mockTemplates';
import DistributorConfigurationTable from './DistributorConfigurationTable';

const DISTRIBUTORS_MOCK: DistributorConfigurationTableData[] = mockDistributorConfigurationTableData.ERK_UPF_X3;

/*
 having difficulties to test behavior like, "collapse row",
 "open whitelist dialog" and "perform whitelist ip dialog actions"
 because for some reason some ag-grid columns are not being rendered
*/
describe('<DistributorConfigurationTable />', () => {
  let afTemplateSection: FormTemplateSection;
  const onDistributorUpdateFn = jest.fn();

  beforeEach(() => {
    const rawMockTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson SMF');
    const afTemplate = JSON.parse(rawMockTemplate[0].template);
    afTemplateSection = afTemplate.dataInterfaces;
  });

  describe('when DistributorConfigurationTable rowData is an empty', () => {
    let component: RenderResult;
    beforeEach(async () => {
      component = render(
        <DistributorConfigurationTable
          rowData={[]}
          templateSection={afTemplateSection}
          isEditing={false}
          onDistributorUpdate={onDistributorUpdateFn}
        />,
      );
    });

    it('should render a message informing that the table has no data ', () => {
      expect(component.getByText('No Rows To Show')).toBeTruthy();
    });

    it('should has no rows rendered', () => {
      const tableRows = component?.container.querySelectorAll('.ag-center-cols-container .ag-row');
      expect(tableRows?.length).toBe(0);
    });
  });

  describe('when DistributorConfigurationTable rowData is an array with data', () => {
    let component: RenderResult;
    beforeEach(async () => {
      component = render(
        <DistributorConfigurationTable
          isEditing
          rowData={DISTRIBUTORS_MOCK}
          templateSection={afTemplateSection}
          onDistributorUpdate={onDistributorUpdateFn}
        />,
      );
      await waitFor(() => screen.getByRole('grid'));
    });

    it('should render a table which the number of rows is the same as the rowData length', async () => {
      const tableRows = component?.container.querySelectorAll('.ag-center-cols-container .ag-row');
      expect(tableRows?.length).toBe(4);
    });

    it('should render distributors configuration name', async () => {
      expect(await screen?.findByText(DISTRIBUTORS_MOCK[0].ipAddress)).toBeVisible();
      expect(await screen?.findByText(DISTRIBUTORS_MOCK[1].ipAddress)).toBeVisible();
      expect(await screen?.findByText(DISTRIBUTORS_MOCK[2].ipAddress)).toBeVisible();
      expect(await screen?.findByText(DISTRIBUTORS_MOCK[3].ipAddress)).toBeVisible();
    });
  });
});
