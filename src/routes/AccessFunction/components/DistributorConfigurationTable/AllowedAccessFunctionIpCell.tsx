import React, { FC } from 'react';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';
import { AccessFunctionIpType } from 'routes/AccessFunction/AccessFunction.types';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import { afWhiteListToGridLabel } from 'routes/AccessFunction/utils/accessFunctionValueGetters';

type Props = {
  context: {
    onSelectAfIp: (data: AccessFunctionIpType) => void;
  }
} & ICellRendererParams

const formatAFWhitelist = (afWhitelist: DistributorConfigurationListenerAfWhiteList[]): string => {
  return afWhiteListToGridLabel(afWhitelist);
};

const AllowedAccessFunctionIpCell: FC<Props> = ({ data, context }: Props) => {

  const handleSelectAfIp = () => context.onSelectAfIp(data);

  if (!data) {
    return <span />;
  }

  return (
    <a type="button" role="button" tabIndex={0} onClick={handleSelectAfIp}>
      {formatAFWhitelist(data?.listener?.afWhitelist)}
    </a>
  );
};

export default AllowedAccessFunctionIpCell;
