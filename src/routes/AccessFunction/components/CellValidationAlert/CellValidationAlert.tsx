import React, { FC } from 'react';
import { Icon, Tooltip, Position } from '@blueprintjs/core';

type Props = {
  msg: string,
}

const CellValidationAlert: FC<Props> = ({ msg }: Props) => (
  <Tooltip intent="danger" content={msg} position={Position.TOP}>
    <Icon intent="danger" icon="error" iconSize={12} />
  </Tooltip>
);

export default CellValidationAlert;
