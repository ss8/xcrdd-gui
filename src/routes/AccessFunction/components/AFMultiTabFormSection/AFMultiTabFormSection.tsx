import React, {
  useState,
  forwardRef,
  useRef,
  RefObject,
  useImperativeHandle,
  MutableRefObject,
} from 'react';
import isEqual from 'lodash.isequal';
import Form, { CustomFormFieldComponent, CustomValidationFunction, FieldUpdateHandlerParam } from 'shared/form';
import { FormTemplate, FormTemplateSection } from 'data/template/template.types';
import { AccessFunctionFieldUpdateCallback, AccessFunctionInterfaceFormData, AccessFunctionInterfaceFormSectionType } from 'data/accessFunction/accessFunction.types';
import MultiTab from 'shared/multi-tab';
import { TabProp } from 'shared/multi-tab/MultiTab';
import { adaptNetworkIdField, adaptXDPIpAddress } from '../../utils/accessFunctionTemplate.adapter';

type Props = {
  name: string,
  templateSection: FormTemplateSection | undefined,
  data: AccessFunctionInterfaceFormData[] | undefined,
  onFieldUpdate?: AccessFunctionFieldUpdateCallback
  isEditing: boolean,
  formSectionName: AccessFunctionInterfaceFormSectionType,
  template: FormTemplate,
  customValidationFunctionMap?: { [key: string]: CustomValidationFunction },
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent },
  shouldValidateOnRender?: boolean
  onTabInterfaceAdd?: (formSectionName: AccessFunctionInterfaceFormSectionType) => void
  onTabInterfaceRemove?: (id: string | number, formSectionName: AccessFunctionInterfaceFormSectionType) => void
}

export type AFMultiTabFormSectionRef = {
  isValid: (forceValidation: boolean, forceTouched: boolean) => boolean
  getValues: () => AccessFunctionInterfaceFormData[]
}

export const AFMultiTabFormSection = forwardRef<AFMultiTabFormSectionRef, Props>(({
  name,
  templateSection,
  data,
  onFieldUpdate = () => null,
  isEditing,
  formSectionName,
  template,
  customValidationFunctionMap,
  customFieldComponentMap,
  shouldValidateOnRender = false,
  onTabInterfaceAdd = () => null,
  onTabInterfaceRemove = () => null,
}: Props, ref) => {
  const [invalidTabs, setInvalidTabs] = useState<{ [id: string]: boolean }>({});
  const tabComponentRefs = useRef<(RefObject<Form>)[]>([]);
  const adaptedTemplateSections = useRef<{ [id: string]: FormTemplateSection }>({});
  const adaptedTemplateKeys = useRef<{ [id: string]: string }>({});

  const getValues = (): AccessFunctionInterfaceFormData[] => {
    const returnValues : AccessFunctionInterfaceFormData[] = [];
    tabComponentRefs.current.forEach((tabComponentRef) => {
      if (tabComponentRef.current) {
        returnValues.push(tabComponentRef.current.getValues());
      }
    });
    return returnValues;
  };

  const getValidTabs = (
    tabFormRefs: MutableRefObject<RefObject<Form>[]>,
    forceValidation = false,
    forceTouched = true,
  ) => {
    const validFormTabs = tabFormRefs.current
      .filter((formRef) => !!formRef.current)
      .map((formRef) => formRef.current?.isValid(forceValidation, forceTouched) ?? true);

    return validFormTabs;
  };

  const isValid = (forceValidation = false, forceTouched = true) => {
    const validInterfaces = getValidTabs(tabComponentRefs, forceValidation, forceTouched);
    const allInterfacesValid = validInterfaces.every((valid) => valid === true);

    const invalidInterfaces = validInterfaces.reduce((previousValue, entry, index) => {
      const interfaceData = data;

      if (interfaceData == null) {
        return previousValue;
      }
      return {
        ...previousValue,
        [interfaceData[index].id]: !entry,
      };
    }, {});

    setInvalidTabs(invalidInterfaces);

    return allInterfacesValid;
  };

  useImperativeHandle(ref, () => ({
    isValid,
    getValues,
  }));

  const createTabComponent = (
    tabData: Partial<AccessFunctionInterfaceFormData> = {},
    index: number,
  ): TabProp | undefined => {
    if (tabData.id == null) {
      return;
    }

    const tabComponentRef: { current: Form | null } = {
      current: null,
    };

    tabComponentRefs.current.push(tabComponentRef);

    const refCallback = (node: Form | null) => {
      tabComponentRef.current = node;
    };

    const handleFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, formSectionName, tabData.id);

    let templateKey = adaptedTemplateKeys.current[tabData.id] ?? template.formId ?? template.key;
    let adaptedTemplateSection = adaptedTemplateSections.current[tabData.id] ?? templateSection;

    if ((adaptedTemplateSection != null) &&
        ((formSectionName === 'provisioningInterfaces') ||
         (formSectionName === 'dataInterfaces') ||
         (formSectionName === 'contentInterfaces'))) {
      adaptedTemplateSection = adaptNetworkIdField(adaptedTemplateSection, tabData);
      adaptedTemplateSection = adaptXDPIpAddress(adaptedTemplateSection, tabData);
    }

    // The adaptXXX above may always change the adaptedTemplateSection But there is no need to force
    // the form update if the returned adaptedTemplateSection is the same as the previous one.
    // In order to prevent the form re-rendered and then losing any field error indication,
    // it is required to check if the new adaptedTemplateSection is the same as the current
    // one.
    if (!isEqual(adaptedTemplateSections.current[tabData.id], adaptedTemplateSection)) {
      templateKey = Date.now().toString();
      adaptedTemplateKeys.current[tabData.id] = templateKey;
      adaptedTemplateSections.current[tabData.id] = adaptedTemplateSection;
    }

    const panel = (
      <Form
        ref={refCallback}
        name={templateKey}
        defaults={tabData}
        fields={adaptedTemplateSection.fields}
        layout={adaptedTemplateSection.metadata.layout?.rows}
        collapsibleLayout={adaptedTemplateSection.metadata.layout?.collapsible}
        fieldUpdateHandler={handleFieldUpdate}
        customValidationFunctionMap={customValidationFunctionMap}
        customFieldComponentMap={customFieldComponentMap}
        shouldValidateOnRender={shouldValidateOnRender}
      />
    );

    const tabTitle = adaptedTemplateSection.metadata.tabTitle || 'Interface';

    return {
      id: tabData.id,
      title: `${tabTitle} ${index + 1}`,
      panel,
      hasErrors: !!invalidTabs?.[tabData.id] ?? false,
    };
  };

  const onTabAdd = () => onTabInterfaceAdd(formSectionName);
  const onTabRemove = (tabId: string | number) => onTabInterfaceRemove(tabId, formSectionName);

  const tabs = data?.map(createTabComponent).filter((entry): entry is TabProp => entry != null) ?? [];

  return (
    <div className="multi-tab-form-section" data-testid={`AFMultiTabFormSection-${name}`}>
      <MultiTab
        tabContainerId={name}
        tabs={tabs}
        addTab={onTabAdd}
        removeTab={onTabRemove}
        minNumberOfTabs={templateSection?.metadata?.min ?? 1}
        maxNumberOfTabs={templateSection?.metadata?.max ?? 1}
        isEditing={isEditing}
      />
    </div>
  );
});

export default AFMultiTabFormSection;
