import React from 'react';
import {
  fireEvent,
  getAllByTestId,
  getByRole,
  getByTestId,
  getByText,
  render,
  screen,
} from '@testing-library/react';
import {
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  FormTemplate,
  FormTemplateSection,
} from 'data/template/template.types';
import { mockTemplates, findMockTemplates } from '__test__/mockTemplates';
import { AccessFunctionFormData } from 'data/accessFunction/accessFunction.types';
import AFMultiTabFormSection from './AFMultiTabFormSection';

describe('<AFMultiTabFormSection />', () => {
  let afData: Partial<AccessFunctionFormData>;
  let afTemplate: FormTemplate;

  beforeEach(() => {
    const rawTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Alcatel-Lucent Distributed Soft Switch');
    afTemplate = JSON.parse(rawTemplate[0].template);
    afData = {
      provisioningInterfaces: [{
        id: '1',
        name: '1',
        destIPAddr: '12.12.23.34',
        destPort: '2343',
        ownnetid: 'ownnetid',
        reqState: 'ACTIVE',
        secinfoid: 'secinfo',
        state: 'ACTIVE',
      }, {
        id: '2',
        name: '2',
        destIPAddr: '13.13.13.13',
        destPort: '2346',
        ownnetid: 'ownnetid',
        reqState: 'ACTIVE',
        secinfoid: 'secinfo',
        state: 'ACTIVE',
      }],
    };
  });

  it('should not render add/remove tab buttons if not in edit mode', () => {
    render(
      <AFMultiTabFormSection
        name="Form name"
        isEditing={false}
        data={afData.provisioningInterfaces}
        templateSection={afTemplate.provisioningInterfaces as FormTemplateSection}
        onFieldUpdate={() => undefined}
        template={afTemplate}
        formSectionName="provisioningInterfaces"
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(0);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(0);
  });

  it('should render add and remove tab buttons if in edit mode', () => {
    render(
      <AFMultiTabFormSection
        name="Form name"
        isEditing
        data={afData.provisioningInterfaces}
        templateSection={afTemplate.provisioningInterfaces as FormTemplateSection}
        onFieldUpdate={() => undefined}
        template={afTemplate}
        formSectionName="provisioningInterfaces"
      />,
    );

    expect(screen.queryAllByTestId('MultiTabAddButton')).toHaveLength(1);
    expect(screen.queryAllByTestId('MultiTabRemoveIcon')).toHaveLength(2);
  });

  it('should render new tab when add is clicked', () => {
    render(
      <AFMultiTabFormSection
        name="PI"
        isEditing
        data={afData.provisioningInterfaces}
        templateSection={afTemplate.provisioningInterfaces as FormTemplateSection}
        onFieldUpdate={() => undefined}
        template={afTemplate}
        formSectionName="provisioningInterfaces"
      />,
    );
    const listeners = screen.getByTestId('AFMultiTabFormSection-PI');
    fireEvent.click(getByTestId(listeners, 'MultiTabAddButton'));

    expect(getByText(listeners, 'Interface 1')).toBeVisible();

    fireEvent.click(getAllByTestId(listeners, 'CollapsibleFormButton')[0]);

    const tab = getByRole(listeners, 'tabpanel');

    expect(getByText(tab, 'AF IP Address:')).toBeVisible();
    expect(getByText(tab, 'AF Port:')).toBeVisible();
    expect(getByText(tab, 'Configured State:')).toBeVisible();
    expect(getByText(tab, 'Timeout Interval (secs):')).toBeVisible();
    expect(getByText(tab, 'DSCP (0-63):')).toBeVisible();
  });

  it('should render the drop down with option with Network ID not found', () => {
    const formTemplateSection: FormTemplateSection = {
      metadata: {
        layout: {
          rows: [['ownnetid']],
        },
      },
      fields: {
        ownnetid: {
          label: 'Network ID',
          type: 'select',
          options: [{ label: 'net id 1', value: 'id1' }],
        },
      },
    };

    const interfaceFormData: Partial<AccessFunctionFormData> = {
      contentInterfaces: [{
        id: 'id',
        name: 'name',
        ownnetid: 'idx',
      }],
    };

    render(
      <AFMultiTabFormSection
        name="form1"
        isEditing
        data={interfaceFormData.contentInterfaces}
        templateSection={formTemplateSection}
        template={afTemplate}
        formSectionName="contentInterfaces"
      />,
    );

    expect(screen.getByLabelText('Network ID:')).toHaveValue('idx');
    expect(screen.getByLabelText('Network ID:')).toHaveDisplayValue('Network ID not found');
  });

  it('should render the XDP IP Address drop down with an additional option that is not initially available', () => {
    const formTemplateSection: FormTemplateSection = {
      metadata: {
        layout: {
          rows: [['ownIPAddr']],
        },
      },
      fields: {
        ownIPAddr: {
          label: 'XDP IP Address',
          type: 'select',
          initial: '',
          options: [
            {
              label: 'Select a XDP IP Address',
              value: '',
            },
            {
              label: '1.1.1.1',
              value: '1.1.1.1',
            },
          ],
        },
      },
    };

    const interfaceFormData: Partial<AccessFunctionFormData> = {
      contentInterfaces: [{
        id: 'id',
        name: 'name',
        ownIPAddr: '1.1.2.2',
      }],
    };

    render(
      <AFMultiTabFormSection
        name="form1"
        isEditing
        data={interfaceFormData.contentInterfaces}
        templateSection={formTemplateSection}
        template={afTemplate}
        formSectionName="contentInterfaces"
      />,
    );

    expect(screen.getByLabelText('XDP IP Address:')).toHaveValue('1.1.2.2');
    expect(screen.getByLabelText('XDP IP Address:')).toHaveDisplayValue('1.1.2.2');
  });
});
