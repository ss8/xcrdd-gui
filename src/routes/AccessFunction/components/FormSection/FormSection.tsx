import React, { forwardRef } from 'react';
import lodashGet from 'lodash.get';
import { Button, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Form, { CustomFormFieldComponent, CustomValidationFunction, FieldUpdateHandlerParam } from 'shared/form';
import { FormTemplateSection } from 'data/template/template.types';
import {
  AccessFunctionFieldUpdateCallback,
  AccessFunctionFormData,
  AccessFunctionFormSection,
  AccessFunctionInterfaceFormData,
} from 'data/accessFunction/accessFunction.types';
import './form-section.scss';
import { adaptNetworkIdField, adaptXDPIpAddress } from 'routes/AccessFunction/utils/accessFunctionTemplate.adapter';

type Props = {
  name: string,
  title?: string,
  templateSection: FormTemplateSection | undefined,
  data: Partial<AccessFunctionFormData>,
  formSectionName: AccessFunctionFormSection,
  dataSection?: string | undefined,
  customValidationFunctionMap?: { [key: string]: CustomValidationFunction }
  customFieldComponentMap?: { [key: string]: CustomFormFieldComponent },
  showRemoveButton?: boolean,
  shouldValidateOnRender?: boolean
  onFieldUpdate?: AccessFunctionFieldUpdateCallback,
  onRemoveButtonClick?: () => void,
}

export const FormSection = forwardRef<Form, Props>(({
  name,
  title,
  templateSection,
  data,
  formSectionName,
  dataSection = undefined,
  customValidationFunctionMap,
  customFieldComponentMap,
  showRemoveButton = false,
  shouldValidateOnRender = false,
  onFieldUpdate = () => null,
  onRemoveButtonClick,
}: Props, ref) => {
  if (!templateSection) {
    return null;
  }

  const renderTitle = () => {
    if (!title) {
      return null;
    }

    return (
      <div className="form-section-header">
        <h3 className="form-section-title">{title}</h3>
        {showRemoveButton && (
          <Button data-testid="FormSectionRemove" icon={IconNames.REMOVE} minimal intent={Intent.PRIMARY} onClick={onRemoveButtonClick}>
            Remove
          </Button>
        )}
      </div>
    );
  };

  let formData = data;
  if (dataSection != null) {
    formData = lodashGet(data, dataSection);
  }

  let adaptedTemplateSection = templateSection;
  if ((formSectionName === 'provisioningInterfaces') ||
      (formSectionName === 'dataInterfaces') ||
      (formSectionName === 'contentInterfaces')) {
    adaptedTemplateSection = adaptNetworkIdField(adaptedTemplateSection, formData as AccessFunctionInterfaceFormData);
    adaptedTemplateSection = adaptXDPIpAddress(adaptedTemplateSection, formData as AccessFunctionInterfaceFormData);
  }

  const handleFieldUpdate = (param: FieldUpdateHandlerParam) => onFieldUpdate(param, formSectionName, formData.id);

  return (
    <div data-testid={`FormSection-${formSectionName}`}>
      {renderTitle()}
      <Form
        ref={ref}
        name={name}
        defaults={formData}
        fields={adaptedTemplateSection.fields}
        layout={adaptedTemplateSection.metadata.layout?.rows}
        collapsibleLayout={adaptedTemplateSection.metadata.layout?.collapsible}
        fieldUpdateHandler={handleFieldUpdate}
        customValidationFunctionMap={customValidationFunctionMap}
        customFieldComponentMap={customFieldComponentMap}
        shouldValidateOnRender={shouldValidateOnRender}
      />
    </div>
  );
});

export default FormSection;
