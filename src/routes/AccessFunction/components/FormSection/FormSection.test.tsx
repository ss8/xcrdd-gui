import React from 'react';
import { FormTemplateSection } from 'data/template/template.types';
import { AccessFunctionFormData } from 'data/accessFunction/accessFunction.types';
import {
  render,
  fireEvent,
  screen,
} from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import FormSection from './FormSection';

describe('<FormSection />', () => {
  let formData: Partial<AccessFunctionFormData>;
  let templateSection: FormTemplateSection;
  let handleFieldUpdate: jest.Mock;

  beforeEach(() => {
    formData = {
      id: 'id1',
    };

    templateSection = {
      metadata: {
        layout: {
          rows: [['name']],
        },
      },
      fields: {
        name: {
          label: 'Label 1',
          type: 'text',
        },
      },
    };

    handleFieldUpdate = jest.fn();
  });

  it('should not render anything if templateSection is not defined', () => {
    const { container } = render(
      <FormSection
        data={formData}
        name="form1"
        templateSection={undefined}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

  it('should render the form as in templateSection', () => {
    render(
      <FormSection
        data={formData}
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    expect(screen.getByText('Section title')).toBeVisible();
    expect(screen.getByText('Label 1:')).toBeVisible();
    expect(screen.getByDisplayValue('')).toBeVisible();
  });

  it('should call handleFieldUpdate when changing the field value', () => {
    render(
      <FormSection
        data={formData}
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    fireEvent.change(screen.getByDisplayValue(''), { target: { value: 'input text' } });
    expect(handleFieldUpdate).toBeCalledWith(
      {
        label: 'Label 1',
        name: 'name',
        value: 'input text',
        valueLabel: 'input text',
      },
      'general',
      'id1',
    );
  });

  it('should load the values in data', () => {
    formData.name = 'some data';

    render(
      <FormSection
        data={formData}
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    expect(screen.getByDisplayValue('some data')).toBeVisible();
  });

  it('should load the values in data from the dataSection', () => {
    formData.contentInterfaces = [{
      id: 'id1',
      name: 'content interface name',
    }];

    render(
      <FormSection
        data={formData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    expect(screen.getByDisplayValue('content interface name')).toBeVisible();
  });

  it('should not show the remove button if prop is false', () => {
    render(
      <FormSection
        data={formData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
      />,
    );

    expect(screen.queryAllByTestId('FormSectionRemove')).toHaveLength(0);
  });

  it('should show the remove button when prop is true', () => {
    render(
      <FormSection
        data={formData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={templateSection}
        formSectionName="general"
        onFieldUpdate={handleFieldUpdate}
        showRemoveButton
      />,
    );

    expect(screen.getByTestId('FormSectionRemove')).toBeVisible();
    expect(screen.getByTestId('FormSectionRemove')).toHaveTextContent('Remove');
  });

  it('should call the onRemoveButtonClick if Remove button is clicked', () => {
    const onRemoveButtonClick = jest.fn();

    render(
      <FormSection
        data={formData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={templateSection}
        onFieldUpdate={handleFieldUpdate}
        formSectionName="general"
        showRemoveButton
        onRemoveButtonClick={onRemoveButtonClick}
      />,
    );

    act(() => {
      fireEvent.click(screen.getByTestId('FormSectionRemove'));
    });

    expect(onRemoveButtonClick).toHaveBeenCalled();
  });

  it('should render the Network ID drop down with option with Network ID not found', () => {
    const formTemplateSection: FormTemplateSection = {
      metadata: {
        layout: {
          rows: [['ownnetid']],
        },
      },
      fields: {
        ownnetid: {
          label: 'Network ID',
          type: 'select',
          options: [{ label: 'net id 1', value: 'id1' }],
        },
      },
    };

    const interfaceFormData: Partial<AccessFunctionFormData> = {
      contentInterfaces: [{
        id: 'id',
        name: 'name',
        ownnetid: 'idx',
      }],
    };

    render(
      <FormSection
        data={interfaceFormData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={formTemplateSection}
        formSectionName="contentInterfaces"
      />,
    );

    expect(screen.getByLabelText('Network ID:')).toHaveValue('idx');
    expect(screen.getByLabelText('Network ID:')).toHaveDisplayValue('Network ID not found');
  });

  it('should render the XDP IP Address drop down with an additional option that is not initially available', () => {
    const formTemplateSection: FormTemplateSection = {
      metadata: {
        layout: {
          rows: [['ownIPAddr']],
        },
      },
      fields: {
        ownIPAddr: {
          label: 'XDP IP Address',
          type: 'select',
          initial: '',
          options: [
            {
              label: 'Select a XDP IP Address',
              value: '',
            },
            {
              label: '1.1.1.1',
              value: '1.1.1.1',
            },
          ],
        },
      },
    };

    const interfaceFormData: Partial<AccessFunctionFormData> = {
      contentInterfaces: [{
        id: 'id',
        name: 'name',
        ownIPAddr: '1.1.2.2',
      }],
    };

    render(
      <FormSection
        data={interfaceFormData}
        dataSection="contentInterfaces.0"
        name="form1"
        title="Section title"
        templateSection={formTemplateSection}
        formSectionName="contentInterfaces"
      />,
    );

    expect(screen.getByLabelText('XDP IP Address:')).toHaveValue('1.1.2.2');
    expect(screen.getByLabelText('XDP IP Address:')).toHaveDisplayValue('1.1.2.2');
  });
});
