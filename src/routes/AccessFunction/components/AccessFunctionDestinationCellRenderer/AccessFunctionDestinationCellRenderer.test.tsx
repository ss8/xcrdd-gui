import React from 'react';
import { render, screen } from '@testing-library/react';
import AccessFunctionDestinationCellRenderer from './AccessFunctionDestinationCellRenderer';
import { NETWORK_ID_NOT_FOUND } from '../../AccessFunction.types';

describe('<AccessFunctionDestinationCellRenderer />', () => {
  it('should render empty if in group row', () => {
    const { container } = render(
      <AccessFunctionDestinationCellRenderer node={{ group: true }} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span />
      </div>
    `);
  });

  it('should render as not applicable if no value', () => {
    render(<AccessFunctionDestinationCellRenderer />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if empty', () => {
    render(<AccessFunctionDestinationCellRenderer value="" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if dash', () => {
    render(<AccessFunctionDestinationCellRenderer value="-" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render the destination value', () => {
    render(<AccessFunctionDestinationCellRenderer value="destination" />);

    expect(screen.getByText('destination')).toBeVisible();
  });

  it('should render as network id not found', () => {
    const { container } = render(
      <AccessFunctionDestinationCellRenderer value={NETWORK_ID_NOT_FOUND} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="access-function-destination-cell-renderer-network-not-found"
        >
          Network ID not found
        </span>
      </div>
    `);
  });
});
