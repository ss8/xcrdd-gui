import React from 'react';
import NotApplicableCellRenderer from 'components/NotApplicableCellRenderer';
import { NETWORK_ID_NOT_FOUND } from '../../AccessFunction.types';
import './access-function-destination-cell-renderer.scss';

type Props = {
  value?: string,
  node?: { group: boolean }
};

export default function AccessFunctionDestinationCellRenderer({
  value,
  node,
}: Props): JSX.Element {

  if (node?.group) {
    return <span />;
  }

  if (value === NETWORK_ID_NOT_FOUND) {
    return (
      <span className="access-function-destination-cell-renderer-network-not-found">
        Network ID not found
      </span>
    );
  }

  if (value == null || value === '-' || value === '') {
    return (
      <NotApplicableCellRenderer />
    );
  }

  return (
    <span>
      {value}
    </span>
  );
}
