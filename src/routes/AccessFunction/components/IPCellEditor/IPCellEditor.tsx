import React, { Component, ReactNode } from 'react';
import IPAddressValidator from 'shared/form/validators/IPAddressValidator';
import { ICellEditorParams } from '@ag-grid-enterprise/all-modules';
import CellValidationAlert from '../CellValidationAlert';
import './ip-cell-editor.scss';

const KEY_BACKSPACE = 8;
const KEY_DELETE = 46;
const KEY_F2 = 113;
const KEY_ENTER = 13;
const KEY_TAB = 9;

type Props = {
  keyPress: typeof KEY_BACKSPACE | typeof KEY_DELETE | typeof KEY_F2 | typeof KEY_ENTER | typeof KEY_TAB,
  value: string
  charPress: string,
} & ICellEditorParams;

type State = {
  value: string,
  isValid: boolean,
  isPristine: boolean,
}

export default class IPCellEditor extends Component<Props, State> {
  private ipAddressInputRef = React.createRef<HTMLInputElement>();

  constructor(props: Props) {
    super(props);
    this.state = this.createInitialState(props);
  }

  // override
  afterGuiAttached = (): void => {
    if (this.ipAddressInputRef?.current) {
      this.ipAddressInputRef.current.focus();
    }
  }

  // override
  getValue(): string {
    const { value } = this.state;
    return value;
  }

  // util
  createInitialState = (props: Props): State => {
    return {
      value: props.value,
      isValid: false,
      isPristine: true,
    };
  }

  // util
  getWrapperClass = (isValid: boolean, isPristine: boolean): string => {
    return (!isValid && !isPristine)
      ? 'ag-input-wrapper ag-input-wrapper-invalid'
      : 'ag-input-wrapper';
  }

  // util
  getInputClass = (isValid: boolean, isPristine: boolean): string => {
    return (!isValid && !isPristine) ? 'ag-cell-edit-input invalid-field' : 'ag-cell-edit-input';
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const ipAddress = event.target.value;
    const isValid = IPAddressValidator.isValid(null, null, ipAddress, null, null, null);
    this.setState({
      value: ipAddress,
      isPristine: false,
      isValid,
    });
  }

  renderInvalidIPAlert = (isValid: boolean, isPristine: boolean): ReactNode => {
    if (isValid || isPristine) {
      return null;
    }

    return (
      <span className="ip-cell-editor-input-icon">
        <CellValidationAlert msg="Please enter a valid IP Address." />
      </span>
    );
  }

  render(): ReactNode {
    const { value, isValid, isPristine } = this.state;
    const inputWrapperClassName = this.getWrapperClass(isValid, isPristine);
    const inputClassName = this.getInputClass(isValid, isPristine);

    return (
      <div className={inputWrapperClassName} role="presentation">
        {this.renderInvalidIPAlert(isValid, isPristine)}
        <input
          type="text"
          ref={this.ipAddressInputRef}
          value={value}
          className={inputClassName}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
