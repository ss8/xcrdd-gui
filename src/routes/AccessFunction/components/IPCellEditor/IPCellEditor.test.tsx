import React from 'react';
import {
  render,
  waitForElement,
  fireEvent,
  screen,
} from '@testing-library/react';

import { Column, RowNode } from '@ag-grid-enterprise/all-modules';
import IPCellEditor from './IPCellEditor';

describe('<IPCellEditor />', () => {

  describe('when cellEditor isValid=true', () => {

    const onKeyDownFn = jest.fn();
    const stopEditingFn = jest.fn();
    const parseValueFn = jest.fn();
    const formatValueFn = jest.fn();

    const value = 'myValue';
    beforeEach(async () => {

      render(
        <IPCellEditor
          value={value}
          keyPress={13}
          charPress="1"
          colDef={{}}
          cellStartedEdit={false}
          column={{} as Column}
          node={{} as RowNode}
          data={{}}
          rowIndex={1}
          api={null}
          columnApi={null}
          context={{}}
          $scope={{}}
          eGridCell={{} as HTMLElement}
          onKeyDown={onKeyDownFn}
          stopEditing={stopEditingFn}
          parseValue={parseValueFn}
          formatValue={formatValueFn}
        />,
      );
      await waitForElement(() => screen?.getByRole('presentation'));
    });

    it('should not render validation alert...', async () => {
      expect((await screen?.findByRole('presentation')).querySelectorAll('.ip-cell-editor-input-icon').length).toBe(0);
    });

    it('should render validation alert when IP Address informed is invalid', async () => {
      const inputElement = (await screen?.findByRole('presentation')).querySelector('input');
      fireEvent.change(inputElement as Element, {
        target: {
          value: 'abcd',
        },
      });
      expect((await screen?.findByRole('presentation')).querySelectorAll('.ip-cell-editor-input-icon').length).toBe(1);
    });
  });

});
