import React from 'react';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';
import { stateValueFormatter } from 'utils/valueFormatters';
import { IconCycle } from 'utils/icons';
import NotApplicableCellRenderer from 'components/NotApplicableCellRenderer';
import './access-function-status-cell-renderer.scss';

type Props = {
  value?: string,
  node?: { group: boolean }
};

export default function AccessFunctionStatusCellRenderer({
  value,
  node,
  ...rest
}: Props): JSX.Element {
  if (node?.group) {
    return <span />;
  }

  if (value == null || value === '-' || value === '') {
    return (
      <NotApplicableCellRenderer />
    );
  }

  let className = 'status ';

  if (value === 'ACTIVE') {
    className += 'active';
  } else if (value === 'INACTIVE') {
    className += 'inactive';
  } else if (value === 'PARTIALLY_ACTIVE') {
    className += 'partially-active';
  } else if (value === 'FAILED') {
    className += 'failed';
  }

  value = stateValueFormatter({ ...(rest as ICellRendererParams), value });

  return (
    <span className="access-function-status-cell-renderer">
      <span className={className} title={value}><IconCycle /></span>
      {value}
    </span>
  );
}
