import React from 'react';
import { render, screen } from '@testing-library/react';
import AccessFunctionStatusCellRenderer from './AccessFunctionStatusCellRenderer';

describe('<AccessFunctionStatusCellRenderer />', () => {
  it('should render empty if in group row', () => {
    const { container } = render(
      <AccessFunctionStatusCellRenderer node={{ group: true }} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span />
      </div>
    `);
  });

  it('should render as not applicable if no value', () => {
    render(<AccessFunctionStatusCellRenderer />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if empty', () => {
    render(<AccessFunctionStatusCellRenderer value="" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if dash', () => {
    render(<AccessFunctionStatusCellRenderer value="-" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render the icon and the status for ACTIVE', () => {
    render(
      <AccessFunctionStatusCellRenderer
        value="ACTIVE"
      />,
    );

    expect(screen.getByText('Active')).toBeVisible();
    expect(screen.getByTitle('Active')).toBeVisible();
  });

  it('should render the icon and status for INACTIVE', () => {
    render(
      <AccessFunctionStatusCellRenderer
        value="INACTIVE"
      />,
    );

    expect(screen.getByText('Inactive')).toBeVisible();
    expect(screen.getByTitle('Inactive')).toBeVisible();
  });

  it('should render the icon and the status for PARTIALLY_ACTIVE', () => {
    render(
      <AccessFunctionStatusCellRenderer
        value="PARTIALLY_ACTIVE"
      />,
    );

    expect(screen.getByText('Partially Active')).toBeVisible();
    expect(screen.getByTitle('Partially Active')).toBeVisible();
  });
});
