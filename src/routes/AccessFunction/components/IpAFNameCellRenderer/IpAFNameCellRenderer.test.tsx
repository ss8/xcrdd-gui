import React from 'react';
import { render, screen } from '@testing-library/react';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import IpAFNameCellRenderer from './IpAFNameCellRenderer';

const AFS_WHITELIST_MOCK: DistributorConfigurationListenerAfWhiteList[] = [
  {
    afName: 'afName1',
    afAddress: '1.1.1.1',
    afId: 'id1',
  },
  {
    afName: 'afName2',
    afAddress: '2.2.2.2',
    afId: 'id2',
  },
  {
    afName: '',
    afAddress: '',
    afId: '',
  },
];

const AFS_OCCURRENCES_MOCK: { [key: string]: number } = {
  afName1: 0,
  afName2: 2,
};

describe('<IpAFNameCellRenderer />', () => {
  it('should render whitelist alert component when af name value is undefined', () => {
    const value = '';

    const { container } = render(
      <IpAFNameCellRenderer
        value={value}
        context={{
          accessFunctionOccurrences: AFS_OCCURRENCES_MOCK,
          isDialogSubmitted: true,
        }}
      />,
    );

    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(1);
  });

  it('should render whitelist value but not render the alert component when af name value is not used by another whitelist item', () => {
    const value = AFS_WHITELIST_MOCK[0].afName;

    const { container } = render(
      <IpAFNameCellRenderer
        value={value}
        context={{
          accessFunctionOccurrences: AFS_OCCURRENCES_MOCK,
          isDialogSubmitted: true,
        }}
      />,
    );

    expect(screen.getByText(value)).toBeVisible();
    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(0);
  });

  it('should render whitelist value and the alert component when af name value is used by another whitelist item', () => {
    const value = AFS_WHITELIST_MOCK[1].afName;

    const { container } = render(
      <IpAFNameCellRenderer
        value={value}
        context={{
          accessFunctionOccurrences: AFS_OCCURRENCES_MOCK,
          isDialogSubmitted: true,
        }}
      />,
    );

    expect(screen.getByText(value)).toBeVisible();
    expect(container.querySelectorAll('.bp3-icon.bp3-icon-error.bp3-intent-danger').length).toBe(1);
  });
});
