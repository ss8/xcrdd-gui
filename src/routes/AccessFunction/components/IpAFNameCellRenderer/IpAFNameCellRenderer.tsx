import React, {
  FC,
  Fragment,
  useEffect,
  useState,
} from 'react';
import CellValidationAlert from '../CellValidationAlert';

type Props = {
  value: string,
  context: {
    accessFunctionOccurrences: { [key: string]: number },
    isDialogSubmitted: boolean
  },
};

const IpAFNameCellRenderer: FC<Props> = ({ value, context }: Props) => {
  const [isNameValid, setNameValid] = useState(true);

  useEffect(() => {
    if (value || context.isDialogSubmitted) {
      setNameValid(context?.accessFunctionOccurrences[value] <= 1 && value.length > 0 && value.length < 128);
    }
  }, [value, context]);

  const renderInvalidNameAlert = (isValid: boolean) => {
    if (isValid) {
      return null;
    }

    return (
      <Fragment>
        <CellValidationAlert msg="Please enter a valid AF Name." />&nbsp;
      </Fragment>
    );
  };

  return (
    <div>
      {renderInvalidNameAlert(isNameValid)}{value}
    </div>
  );
};

export default IpAFNameCellRenderer;
