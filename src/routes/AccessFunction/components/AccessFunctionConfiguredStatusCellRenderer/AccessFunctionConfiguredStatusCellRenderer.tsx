import React from 'react';
import NotApplicableCellRenderer from 'components/NotApplicableCellRenderer';
import { stateValueFormatter } from 'utils/valueFormatters';

type Props = {
  value?: string,
  node?: { group: boolean }
};

export default function AccessFunctionDestinationCellRenderer({
  value,
  node,
}: Props): JSX.Element {

  if (node?.group) {
    return <span />;
  }

  if (value == null || value === '-' || value === '') {
    return (
      <NotApplicableCellRenderer />
    );
  }

  return (
    <span>
      {stateValueFormatter({ value })}
    </span>
  );
}
