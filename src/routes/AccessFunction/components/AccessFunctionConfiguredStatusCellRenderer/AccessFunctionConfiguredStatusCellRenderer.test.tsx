import React from 'react';
import { render, screen } from '@testing-library/react';
import AccessFunctionConfiguredStatusCellRenderer from './AccessFunctionConfiguredStatusCellRenderer';

describe('<AccessFunctionConfiguredStatusCellRenderer />', () => {
  it('should render empty if in group row', () => {
    const { container } = render(
      <AccessFunctionConfiguredStatusCellRenderer node={{ group: true }} />,
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span />
      </div>
    `);
  });

  it('should render as not applicable if no value', () => {
    render(
      <AccessFunctionConfiguredStatusCellRenderer />,
    );

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if empty', () => {
    render(<AccessFunctionConfiguredStatusCellRenderer value="" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render as not applicable if dash', () => {
    render(<AccessFunctionConfiguredStatusCellRenderer value="-" />);

    expect(screen.getByText('not applicable')).toBeVisible();
  });

  it('should render the icon and the status for ACTIVE', () => {
    render(
      <AccessFunctionConfiguredStatusCellRenderer
        value="ACTIVE"
      />,
    );

    expect(screen.getByText('Active')).toBeVisible();
  });

  it('should render the icon and status for INACTIVE', () => {
    render(
      <AccessFunctionConfiguredStatusCellRenderer
        value="INACTIVE"
      />,
    );

    expect(screen.getByText('Inactive')).toBeVisible();
  });

  it('should render the icon and the status for PARTIALLY_ACTIVE', () => {
    render(
      <AccessFunctionConfiguredStatusCellRenderer
        value="PARTIALLY_ACTIVE"
      />,
    );

    expect(screen.getByText('Partially Active')).toBeVisible();
  });
});
