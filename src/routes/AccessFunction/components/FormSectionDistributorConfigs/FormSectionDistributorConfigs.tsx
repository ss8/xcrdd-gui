import React, {
  FunctionComponent,
  useEffect,
  useState,
} from 'react';
import { FormTemplateSection } from 'data/template/template.types';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { FieldUpdateHandlerParam } from 'shared/form';
import DistributorConfigurationTable from '../DistributorConfigurationTable';
import { filterDistributorConfigurations } from './FormSectionDistributorConfigs.utils';

type Props = {
  isEditing: boolean,
  data: DistributorConfigurationTableData[],
  formSectionName: string,
  templateSection: FormTemplateSection | undefined,
  nodeName?: string
  is5gcVcpEnabled?: boolean
  instructionalText?: string
  onDistributorUpdate?: (distributorConfiguration: DistributorConfigurationTableData) => void
  onFieldUpdate?: (param: FieldUpdateHandlerParam) => void
}

export const FormSectionDistributorConfigs: FunctionComponent<Props> = ({
  isEditing,
  data,
  formSectionName,
  templateSection,
  nodeName,
  is5gcVcpEnabled,
  instructionalText,
  onDistributorUpdate,
}: Props) => {
  const [distributorConfigsTableData, setDistributorConfigsTableData] =
    useState<DistributorConfigurationTableData[]>([]);

  useEffect(() => {
    const filteredData = filterDistributorConfigurations(
      data,
      templateSection,
      nodeName,
      is5gcVcpEnabled,
    );

    setDistributorConfigsTableData(filteredData);
  }, [is5gcVcpEnabled, data, templateSection, nodeName]);

  const renderInstructionalText = ():JSX.Element => {
    if (instructionalText && instructionalText !== '') {
      return <p dangerouslySetInnerHTML={{ __html: instructionalText }} />;
    }

    return (
      <p>
        All Points of Interception of this type are auto-associated with all SS8 Distributors
        of the same type.
        <br />
        The listener <b>Allowed AF IPs list</b> will determine if an AF will be listened or
        not. Empty lists will listen to all points of interception of the same type.
        Click on the link to view or edit the list.
      </p>
    );
  };

  if (!templateSection) {
    return null;
  }

  return (
    <div data-testid={`FormSectionDistributorConfigs-${formSectionName}`}>
      {renderInstructionalText()}
      <DistributorConfigurationTable
        isEditing={isEditing}
        rowData={distributorConfigsTableData}
        templateSection={templateSection}
        onDistributorUpdate={onDistributorUpdate}
      />
    </div>
  );
};

export default FormSectionDistributorConfigs;
