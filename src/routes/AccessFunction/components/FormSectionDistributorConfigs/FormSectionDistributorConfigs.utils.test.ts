import cloneDeep from 'lodash.clonedeep';
import { FormTemplateMapByKey, FormTemplateSection } from 'data/template/template.types';
import { adaptToTableData } from 'data/distributorConfiguration/distributorConfiguration.adapter';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { filterDistributorConfigurations } from './FormSectionDistributorConfigs.utils';

describe('FormSectionDistributorConfigs.utils', () => {
  describe('filterDistributorConfigurationsBasedTemplateSection()', () => {
    let accessFunctionTemplates: FormTemplateMapByKey;
    let distributors: DistributorConfiguration[];

    beforeEach(() => {
      distributors = [{
        id: 'MQ',
        name: 'smf-1ed',
        moduleName: 'ERKSMFX2IM',
        instanceId: '1',
        afType: 'ERK_SMF',
        nodeName: 'other',
        listeners: [{
          id: '1',
          afProtocol: 'ERKSMF_X2',
          ipAddress: '27.0.0.1',
          port: '12115',
          keepaliveTimeout: '330',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          synchronizationHeader: 'NONE',
          numberOfParsers: '5',
          afWhitelist: [],
        }],
        destinations: [],
      }, {
        id: 'MjA',
        name: 'upf-2ed',
        moduleName: 'ERKUPFX3IM',
        instanceId: '1',
        afType: 'ERK_UPF',
        nodeName: 'CCPAGNODE',
        listeners: [{
          id: '1',
          afProtocol: 'ERKSMF_X2',
          ipAddress: '27.0.0.1',
          port: '12115',
          keepaliveTimeout: '330',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          synchronizationHeader: 'NONE',
          numberOfParsers: '5',
          afWhitelist: [],
        }],
        destinations: [],
      }, {
        id: 'MjE',
        name: 'smf-2ed',
        moduleName: 'ERKSMFX2IM',
        instanceId: '1',
        afType: 'ERK_SMF',
        nodeName: 'CCPAGNODE',
        listeners: [{
          id: '1',
          afProtocol: 'ERKSMF_X2',
          ipAddress: '27.0.0.1',
          port: '12115',
          keepaliveTimeout: '330',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          synchronizationHeader: 'NONE',
          numberOfParsers: '5',
          afWhitelist: [],
        }],
        destinations: [],
      }, {
        id: 'MjA',
        name: 'upf-2ed',
        moduleName: '5GXCF3',
        instanceId: '1',
        afType: 'ERK_UPF',
        nodeName: 'CCPAGNODE',
        listeners: [{
          id: '1',
          afProtocol: 'ERKSMF_X2',
          ipAddress: '27.0.0.1',
          port: '12115',
          keepaliveTimeout: '330',
          maxConcurrentConnection: '128',
          secInfoId: 'Tk9ORQ',
          transport: 'TCP',
          requiredState: 'ACTIVE',
          status: 'FAILED',
          synchronizationHeader: 'NONE',
          numberOfParsers: '5',
          afWhitelist: [],
        }],
        destinations: [],
      }];

      accessFunctionTemplates = {
        ERK_SMF: getMockTemplateByKey('ERK_SMF'),
        NOK_5GSMF: getMockTemplateByKey('NOK_5GSMF'),
      };
    });

    it('should return the distributor data as is if no template section', () => {
      const data = adaptToTableData(distributors);
      expect(filterDistributorConfigurations(data, undefined, undefined, undefined)).toEqual(data);
    });

    it('should return the distributor data filtered by moduleName', () => {
      const templateSection = accessFunctionTemplates.ERK_SMF.dataInterfaces as FormTemplateSection;
      const data = adaptToTableData(distributors);

      expect(filterDistributorConfigurations(data, templateSection, undefined, undefined)).toEqual([
        data[0],
        data[2],
      ]);
    });

    it('should return the distributor data filtered by nodeName', () => {
      const templateSection = cloneDeep(accessFunctionTemplates.ERK_SMF.contentInterfaces as FormTemplateSection);
      delete templateSection.fields.moduleName;
      delete templateSection.fields.moduleNameIf5gcVcpEnabled;
      const data = adaptToTableData(distributors);

      expect(filterDistributorConfigurations(data, templateSection, 'CCPAGNODE', true)).toEqual([
        data[1],
        data[2],
        data[3],
      ]);
    });

    it('should return the distributor data filtered by moduleName and nodeName', () => {
      const templateSection = cloneDeep(accessFunctionTemplates.ERK_SMF.contentInterfaces as FormTemplateSection);
      delete templateSection.fields.moduleNameIf5gcVcpEnabled;
      const data = adaptToTableData(distributors);

      expect(filterDistributorConfigurations(data, templateSection, 'CCPAGNODE', true)).toEqual([
        data[1],
      ]);
    });

    it('should return an empty distributor data if is5gcVcpEnabled and nodeName not defined', () => {
      const templateSection = cloneDeep(accessFunctionTemplates.ERK_SMF.contentInterfaces as FormTemplateSection);
      delete templateSection.fields.moduleNameIf5gcVcpEnabled;
      const data = adaptToTableData(distributors);
      expect(filterDistributorConfigurations(data, templateSection, '', true)).toEqual([]);
    });

    it('should return the distributor data filtered by moduleName and nodeName when moduleNameIf5gcVcpEnabled', () => {
      const templateSection = cloneDeep(accessFunctionTemplates.ERK_SMF.contentInterfaces as FormTemplateSection);
      delete templateSection.fields.moduleName;
      const data = adaptToTableData(distributors);

      expect(filterDistributorConfigurations(data, templateSection, 'CCPAGNODE', true)).toEqual([
        data[3],
      ]);
    });

    it('should return the distributor data filtered by moduleName if not is5gcVcpEnabled when moduleNameIf5gcVcpEnabled', () => {
      const templateSection = accessFunctionTemplates.ERK_SMF.contentInterfaces as FormTemplateSection;
      const data = adaptToTableData(distributors);

      expect(filterDistributorConfigurations(data, templateSection, 'CCPAGNODE', false)).toEqual([
        data[1],
      ]);
    });
  });
});
