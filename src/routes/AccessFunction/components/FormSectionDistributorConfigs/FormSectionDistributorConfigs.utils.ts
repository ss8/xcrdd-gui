import { FormTemplateOption, FormTemplateSection } from 'data/template/template.types';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';

// eslint-disable-next-line import/prefer-default-export
export function filterDistributorConfigurations(
  distributorConfigsTableData: DistributorConfigurationTableData[],
  templateSection: FormTemplateSection | undefined,
  nodeName?: string,
  is5gcVcpEnabled?: boolean,
): DistributorConfigurationTableData[] {
  let filteredDistributorConfigsTableData = distributorConfigsTableData;

  const moduleNames = getModuleNames(templateSection, is5gcVcpEnabled);
  if (moduleNames.length > 0) {
    filteredDistributorConfigsTableData = filterDistributorConfigurationsBasedOnModuleName(
      filteredDistributorConfigsTableData,
      moduleNames,
    );
  }

  const filterIf5gcVcpEnabled = templateSection?.fields.distributorsFilterIf5gcVcpEnabled?.initial;
  if (filterIf5gcVcpEnabled === true && is5gcVcpEnabled && !nodeName) {
    // The nodeName is not available yet, so the list should be empty
    filteredDistributorConfigsTableData = [];

  } else if (filterIf5gcVcpEnabled === true && is5gcVcpEnabled && nodeName != null) {
    filteredDistributorConfigsTableData = filterDistributorConfigurationsBasedOnNodeName(
      filteredDistributorConfigsTableData,
      nodeName,
    );
  }

  return filteredDistributorConfigsTableData;
}

function getModuleNames(
  templateSection: FormTemplateSection | undefined,
  is5gcVcpEnabled?: boolean,
): string[] {
  let moduleNameOptions: FormTemplateOption[] = [];

  if (is5gcVcpEnabled) {
    // 5GC VCP is enabled, try to get the module name for it
    moduleNameOptions = templateSection?.fields.moduleNameIf5gcVcpEnabled?.options ?? [];
  }

  if (moduleNameOptions.length === 0) {
    moduleNameOptions = templateSection?.fields.moduleName?.options ?? [];
  }

  return moduleNameOptions.map(({ value }) => value);
}

function filterDistributorConfigurationsBasedOnModuleName(
  distributorConfigsTableData: DistributorConfigurationTableData[],
  moduleNames: string[],
): DistributorConfigurationTableData[] {
  return distributorConfigsTableData.filter((distributorConfigTableData) =>
    (moduleNames.includes(distributorConfigTableData.moduleName)));
}

function filterDistributorConfigurationsBasedOnNodeName(
  distributorConfigsTableData: DistributorConfigurationTableData[],
  nodeName: string,
): DistributorConfigurationTableData[] {

  const filteredDistributorConfigsTableData = distributorConfigsTableData.filter((distributorConfigTableData) => {
    return distributorConfigTableData.nodeName === nodeName;
  });

  return filteredDistributorConfigsTableData;
}
