import React from 'react';
import {
  FormTemplateSection,
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
} from 'data/template/template.types';
import {
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { mockTemplates, findMockTemplates, getMockTemplateByKey } from '__test__/mockTemplates';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import { mockDistributorConfigurationTableData } from '__test__/mockDistributorConfigurationTableData';
import FormSectionDistributorConfigs from './FormSectionDistributorConfigs';

describe('<FormSectionDistributorConfigs />', () => {
  let templateSection: FormTemplateSection;
  let handleFieldUpdate: jest.Mock;

  beforeEach(() => {
    templateSection = {
      metadata: {},
      fields: {},
    };

    handleFieldUpdate = jest.fn();
  });

  it('should not render anything if templateSection is not defined', () => {
    const { container } = render(
      <FormSectionDistributorConfigs
        isEditing
        data={[]}
        formSectionName="section"
        templateSection={undefined}
        onDistributorUpdate={handleFieldUpdate}
      />,
    );

    expect(container).toBeEmptyDOMElement();
  });

  it('should render the distributor config list as an empty table', () => {
    render(
      <FormSectionDistributorConfigs
        isEditing
        data={[]}
        formSectionName="section"
        templateSection={templateSection}
        onDistributorUpdate={handleFieldUpdate}
      />,
    );

    expect(screen.getAllByText('SS8 Distributors Configuration')[0]).toBeVisible();
    expect(screen.getByText('No Rows To Show')).toBeVisible();
  });

  it('should display distributor config applicable to this section', async () => {
    const rawMockTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson MCPTT');
    const afTemplate = JSON.parse(rawMockTemplate[0].template);

    const distributorConfigurationTableData: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X2,
      ...mockDistributorConfigurationTableData.ERK_MCPTT_X3,
    ];

    render(
      <FormSectionDistributorConfigs
        isEditing
        data={distributorConfigurationTableData}
        formSectionName="contentInterfaces"
        templateSection={afTemplate.contentInterfaces}
        onDistributorUpdate={handleFieldUpdate}
      />,
    );
    await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    expect(await screen?.findByText(
      mockDistributorConfigurationTableData.ERK_MCPTT_X3[0].listener.ipAddress
    )).toBeVisible();
    expect(await screen?.findByText(
      mockDistributorConfigurationTableData.ERK_MCPTT_X3[1].listener.ipAddress
    )).toBeVisible();
  });

  it('should filter the list based in the moduleName for ERK_SMF X2', async () => {
    const template = getMockTemplateByKey('ERK_SMF');

    const data: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_SMF_X2,
      ...mockDistributorConfigurationTableData.ERK_UPF_X3,
    ];

    render(
      <FormSectionDistributorConfigs
        isEditing
        data={data}
        formSectionName="dataInterfaces"
        templateSection={(template.dataInterfaces as FormTemplateSection)}
        onDistributorUpdate={handleFieldUpdate}
      />,
    );

    await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    expect(screen.queryByText(data[0].ipAddress)).toBeVisible();
    expect(screen.queryByText(data[1].ipAddress)).toBeVisible();
    expect(screen.queryByText(data[2].ipAddress)).toBeNull();
    expect(screen.queryByText(data[3].ipAddress)).toBeNull();
    expect(screen.queryByText(data[4].ipAddress)).toBeNull();
    expect(screen.queryByText(data[5].ipAddress)).toBeNull();
  });

  it('should filter the list based in the moduleName for ERK_SMF X3', async () => {
    const template = getMockTemplateByKey('ERK_SMF');

    const data: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_SMF_X2,
      ...mockDistributorConfigurationTableData.ERK_UPF_X3,
    ];

    render(
      <FormSectionDistributorConfigs
        isEditing
        data={data}
        formSectionName="contentInterfaces"
        templateSection={(template.contentInterfaces as FormTemplateSection)}
        onDistributorUpdate={handleFieldUpdate}
        nodeName="CCPAGNODE"
        is5gcVcpEnabled={false}
      />,
    );

    await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    expect(screen.queryByText(data[0].ipAddress)).toBeNull();
    expect(screen.queryByText(data[1].ipAddress)).toBeNull();
    expect(screen.queryByText(data[2].ipAddress)).toBeNull();
    expect(screen.queryByText(data[3].ipAddress)).toBeVisible();
    expect(screen.queryByText(data[4].ipAddress)).toBeVisible();
    expect(screen.queryByText(data[5].ipAddress)).toBeVisible();
  });

  it('should filter the list based in the moduleName and nodeName for ERK_SMF X3', async () => {
    const template = getMockTemplateByKey('ERK_SMF');

    const data: DistributorConfigurationTableData[] = [
      ...mockDistributorConfigurationTableData.ERK_SMF_X2,
      ...mockDistributorConfigurationTableData.ERK_UPF_X3,
    ];

    render(
      <FormSectionDistributorConfigs
        isEditing
        data={data}
        formSectionName="contentInterfaces"
        templateSection={(template.contentInterfaces as FormTemplateSection)}
        onDistributorUpdate={handleFieldUpdate}
        nodeName="CCPAGNODE"
        is5gcVcpEnabled
      />,
    );

    await waitFor(() => expect(screen.getByRole('grid')).toBeVisible());

    expect(screen.queryByText(data[0].ipAddress)).toBeNull();
    expect(screen.queryByText(data[1].ipAddress)).toBeNull();
    expect(screen.queryByText(data[2].ipAddress)).toBeVisible();
    expect(screen.queryByText(data[3].ipAddress)).toBeNull();
    expect(screen.queryByText(data[4].ipAddress)).toBeNull();
    expect(screen.queryByText(data[5].ipAddress)).toBeNull();
  });

  it('should render default Instructional Text', () => {
    const rawMockTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson MCPTT');
    const afTemplate = JSON.parse(rawMockTemplate[0].template);
    render(
      <FormSectionDistributorConfigs
        isEditing
        data={[]}
        formSectionName="contentInterfaces"
        templateSection={afTemplate.contentInterfaces}
        onDistributorUpdate={handleFieldUpdate}
      />,
    );
    expect(screen.getByText('All Points of Interception of this type are auto-associated with all SS8 Distributors of the same type.', { exact: false })).toBeVisible();
    expect(screen.getByText('will determine if an AF will be listened or not. Empty lists will listen to all points of interception of the same type. Click on the link to view or edit the list', { exact: false })).toBeVisible();
  });

  it('should render a custom instructional text', () => {
    const rawMockTemplate = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson MCPTT');
    const afTemplate = JSON.parse(rawMockTemplate[0].template);
    render(
      <FormSectionDistributorConfigs
        isEditing
        data={[]}
        formSectionName="contentInterfaces"
        templateSection={afTemplate.contentInterfaces}
        onDistributorUpdate={handleFieldUpdate}
        instructionalText="All AFs of this type are auto-associated with all SS8 Distributors of the same type."
      />,
    );
    expect(screen.getByText('All AFs of this type are auto-associated with all SS8 Distributors of the same type.')).toBeVisible();
  });
});
