import React, { FunctionComponent, Fragment } from 'react';
import { Route } from 'react-router-dom';
import AccessFunctionList from './containers/AccessFunctionList';
import AccessFunctionCreate from './containers/AccessFunctionCreate';
import AccessFunctionEdit from './containers/AccessFunctionEdit';
import AccessFunctionView from './containers/AccessFunctionView';
import AccessFunctionCopy from './containers/AccessFunctionCopy';
import { AF_ROUTE } from '../../constants';

interface Pros {}

const AccessFunction: FunctionComponent<Pros> = () => {
  return (
    <Fragment>
      <Route exact path={AF_ROUTE}>
        <AccessFunctionList />
      </Route>
      <Route exact path={`${AF_ROUTE}/create`}>
        <AccessFunctionCreate />
      </Route>
      <Route exact path={`${AF_ROUTE}/edit`}>
        <AccessFunctionEdit />
      </Route>
      <Route exact path={`${AF_ROUTE}/view`}>
        <AccessFunctionView />
      </Route>
      <Route exact path={`${AF_ROUTE}/copy`}>
        <AccessFunctionCopy />
      </Route>
    </Fragment>
  );
};

export default AccessFunction;
