import { WithAuditDetails, AuditDetails } from 'data/types';
import { DistributorConfiguration, DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import * as distributorConfigAdapters from 'data/distributorConfiguration/distributorConfiguration.adapter';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';

export default function getDistributorConfigurationDataToSubmit(
  distributorConfigsTableData: DistributorConfigurationTableData[] | undefined,
  originalDistributorConfigurations: DistributorConfiguration[],
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
): WithAuditDetails<DistributorConfiguration>[] {
  const distributorConfigurationBodies: WithAuditDetails<DistributorConfiguration>[] = [];
  if (!distributorConfigsTableData) {
    return distributorConfigurationBodies;
  }

  const adaptedDistributorConfigs = distributorConfigAdapters.adaptToSubmitFromTableData(
    distributorConfigsTableData,
    originalDistributorConfigurations,
  );

  if (adaptedDistributorConfigs.length > 0) {
    const afTypeAndProtocolMap: {[afTypeAndProtocol: string]: boolean} = {};
    adaptedDistributorConfigs.filter(({ afType, listeners }) => {
      const { afProtocol } = listeners[0] ?? {};
      const afTypeAndProtocol = `${afType}:${afProtocol}`;
      const isAlreadyMapped = !!afTypeAndProtocolMap[afTypeAndProtocol];
      afTypeAndProtocolMap[afTypeAndProtocol] = true;
      return !isAlreadyMapped;
    }).forEach((adaptedDistributorConfig) => {
      const originalDistributorConfiguration =
        originalDistributorConfigurations.find((entry) => entry.id === adaptedDistributorConfig.id);

      const distributorConfigToSubmit =
        distributorConfigAdapters.setDistributorConfigurationOperationValuesInAccessFunction(
          adaptedDistributorConfig,
          originalDistributorConfiguration,
        );

      const distributorConfigurationBody: WithAuditDetails<DistributorConfiguration> = {
        data: distributorConfigToSubmit,
      };

      if (isAuditEnabled) {
        const auditDetails = getAuditDetails(
          isAuditEnabled,
          userId,
          userName,
          getAuditFieldInfo(adaptedDistributorConfig, null, true, originalDistributorConfiguration),
          distributorConfigToSubmit.name,
          AuditService.AF,
          AuditActionType.DISTRIBUTOR_CONFIGURATION_EDIT,
        );
        distributorConfigurationBody.auditDetails = auditDetails as AuditDetails;
      }

      distributorConfigurationBodies.push(distributorConfigurationBody);
    });
  }

  return distributorConfigurationBodies;
}
