import { FormTemplateMapByKey, FormTemplateSection } from 'data/template/template.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { getDistributorConfigurationsFilter } from './getDistributorConfigurationsFilter';

describe('getDistributorConfigurationsFilter()', () => {
  let accessFunctionTemplates: FormTemplateMapByKey;

  beforeEach(() => {
    accessFunctionTemplates = {
      ERK_SMF: getMockTemplateByKey('ERK_SMF'),
      NOK_5GSMF: getMockTemplateByKey('NOK_5GSMF'),
    };
  });

  it('should return the filter with the type passed as parameter', () => {
    expect(getDistributorConfigurationsFilter(accessFunctionTemplates.NOK_5GSMF)).toEqual('afType eq \'NOK_5GSMF\' or afType eq \'NOK_5GUPF\'');
  });

  it('should return the filter with the type and the template distributorsType', () => {
    expect(getDistributorConfigurationsFilter(accessFunctionTemplates.ERK_SMF)).toEqual('afType eq \'ERK_SMF\' or afType eq \'ERK_UPF\'');
  });

  it('should return the filter with the type and the template distributorsType without duplicates', () => {
    (accessFunctionTemplates.ERK_SMF.dataInterfaces as FormTemplateSection).fields.distributorsType = {
      type: 'text',
      initial: 'ERK_SMF',
      hide: 'true',
    };

    expect(getDistributorConfigurationsFilter(accessFunctionTemplates.ERK_SMF)).toEqual('afType eq \'ERK_SMF\' or afType eq \'ERK_UPF\'');
  });
});
