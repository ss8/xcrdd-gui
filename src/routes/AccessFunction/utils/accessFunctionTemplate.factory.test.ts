import {
  FormTemplate, FormTemplateSection,
} from 'data/template/template.types';
import { AccessFunctionFormData, ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID } from 'data/accessFunction/accessFunction.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { createAccessFunctionBasedOnTemplate } from './accessFunctionTemplate.factory';

describe('accessFunctionTemplate.factory', () => {
  describe('createAccessFunctionBasedOnTemplate', () => {
    let accessFunctionTemplates: {[key: string]: FormTemplate};

    beforeEach(() => {
      accessFunctionTemplates = {
        ERK_UPF: getMockTemplateByKey('ERK_UPF'),
        ERK_SMF: getMockTemplateByKey('ERK_SMF'),
        ALU_MME: getMockTemplateByKey('ALU_MME'),
        ALU_DSS: getMockTemplateByKey('ALU_DSS'),
        ERK_PGW: getMockTemplateByKey('ERK_PGW'),
        MV_SMSIWF: getMockTemplateByKey('MV_SMSIWF'),
        ACME_X123: getMockTemplateByKey('ACME_X123'),
        ICS_RCS: getMockTemplateByKey('ICS_RCS'),
        MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
        NSNHLR_SDM_PRGW: getMockTemplateByKey('NSNHLR_SDM_PRGW'),
        NSNHLR: getMockTemplateByKey('NSNHLR'),
        NSN_SAEGW_PGW: getMockTemplateByKey('NSN_SAEGW_PGW'),
        ERK_VMME: getMockTemplateByKey('ERK_VMME'),
        NSN_SAEGW: getMockTemplateByKey('NSN_SAEGW'),
        MVNR_SBC: getMockTemplateByKey('MVNR_SBC'),
        MVNR_CSCF: getMockTemplateByKey('MVNR_CSCF'),
        GIZMO: getMockTemplateByKey('GIZMO'),
        ERK_5GAMF: getMockTemplateByKey('ERK_5GAMF'),
        NOK_5GUDM: getMockTemplateByKey('NOK_5GUDM'),
        NSN_SDM: getMockTemplateByKey('NSN_SDM'),
        NOK_5GAMF: getMockTemplateByKey('NOK_5GAMF'),
        NOK_5GSMF: getMockTemplateByKey('NOK_5GSMF'),
        NOK_5GSMSF: getMockTemplateByKey('NOK_5GSMSF'),
        MVNR_5GSMSC: getMockTemplateByKey('MVNR_5GSMSC'),
        NKSR: getMockTemplateByKey('NKSR'),
        ETSI103221: getMockTemplateByKey('ETSI103221'),
      };
    });

    describe('ERK_UPF', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ERK_UPF',
          tag: 'ERK_UPF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentInterfaces: [],
        };

        const template = accessFunctionTemplates.ERK_UPF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ERK_SMF', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ERK_SMF',
          tag: 'ERK_SMF',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          dataInterfaces: [],
          provisioningInterfaces: [{
            id: '1.accessFunctionInterface',
            name: '',
            dscp: '32',
            reqState: 'ACTIVE',
            secinfoid: '',
            state: '',
            ccnodename: '',
            mdf2NodeName: '',
            mdf3NodeName: '',
          }],
          contentInterfaces: [],
        };

        const template = accessFunctionTemplates.ERK_SMF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ACME_X123', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ACME_X123',
          tag: 'ACME_X123',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            secinfoid: '',
            ownIPAddr: '0.0.0.0',
            ownIPPort: '',
            reqState: 'ACTIVE',
            state: '',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            secinfoid: '',
            reqState: 'ACTIVE',
            state: '',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            ownIPAddr: '',
          }],
        };

        const template = accessFunctionTemplates.ACME_X123;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ALU_MME', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ALU_MME',
          tag: 'ALU_MME',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            destIPAddr: '',
            destPort: '',
            ownIPPort: '',
            dscp: '0',
            name: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            dscp: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            tpkt: false,
          }],
        };

        const template = accessFunctionTemplates.ALU_MME;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ALU_DSS', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ALU_DSS',
          tag: 'ALU_DSS',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            destIPAddr: '',
            destPort: '',
            ownIPPort: '',
            dscp: '0',
            name: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            imei: true,
            imsi: true,
            msisdn: true,
            timeout: '10',
          }],
          dataInterfaces: [],
        };

        const template = accessFunctionTemplates.ALU_DSS;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      describe('ERK_VMME', () => {
        it('should return a AccessFunctionFormData instance based in the template', () => {
          const expected: AccessFunctionFormData = {
            name: '',
            description: '',
            timeZone: '',
            type: 'ERK_VMME',
            tag: 'ERK_VMME',
            version: '',
            insideNat: false,
            country: '',
            stateName: '',
            city: '',
            liInterface: 'ALL',
            model: '',
            preprovisioningLead: '000:00',
            serialNumber: '',
            contact: '',
            provisioningInterfaces: [{
              id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
              destIPAddr: '',
              destPort: '4002',
              dscp: '0',
              name: '',
              secinfoid: '',
              ownnetid: '',
              tpkt: false,
              appProtocol: 'ERK_SGSN_MME',
              version: '1.1',
              keepaliveinterval: '70',
              transport: 'TCP',
              reqState: 'ACTIVE',
              state: '',
            }],
            dataInterfaces: [{
              id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
              name: '',
              destIPAddr: '',
              destPort: '0',
              dscp: '0',
              maxx2connections: '1',
              ownnetid: '',
              secinfoid: '',
              reqState: 'ACTIVE',
              state: '',
              tpkt: true,
              x224headerpresent: true,
            }],
          };

          const template = accessFunctionTemplates.ERK_VMME;
          expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
        });
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ALU_DSS',
          tag: 'ALU_DSS',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            destIPAddr: '',
            destPort: '',
            ownIPPort: '',
            dscp: '0',
            name: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            imei: true,
            imsi: true,
            msisdn: true,
            timeout: '10',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            ownIPPort: '11550',
            mscid: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
          }],
        };

        const template = accessFunctionTemplates.ALU_DSS;
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('ICS_RCS', () => {

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
        const expected: AccessFunctionFormData = {
          name: '',
          description: '',
          timeZone: '',
          type: 'ICS_RCS',
          tag: 'ICS_RCS',
          version: '',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            dscp: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            secinfoid: '',
            uri: '',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            ownnetid: '',
            dscp: '0',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            ownnetid: '',
            dscp: '0',
          }],
        };

        const template = accessFunctionTemplates.ICS_RCS;
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces and contentDeliveryVia is trafficManager', () => {
        const expected: AccessFunctionFormData = {
          name: '',
          description: '',
          timeZone: '',
          type: 'ICS_RCS',
          tag: 'ICS_RCS',
          version: '',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'trafficManager',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            ownnetid: '',
            secinfoid: '',
            reqState: 'ACTIVE',
            state: '',
            dscp: '32',
            lbid: '',
            traceLevel: '0',
            type: 'ICS_RCS',
          }],
        };

        const template = accessFunctionTemplates.ICS_RCS;
        (template.general as FormTemplateSection).fields.contentDeliveryVia.initial = 'trafficManager';
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('MV_SMSIWF', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MV_SMSIWF',
          tag: 'MV_SMSIWF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            dscp: '32',
            ownnetid: '',
            secinfoid: 'Tk9ORQ',
            reqState: 'ACTIVE',
            state: '',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            dscp: '32',
            ownnetid: '',
            secinfoid: 'Tk9ORQ',
          }],
        };

        const template = accessFunctionTemplates.MV_SMSIWF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ERK_PGW', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ERK_PGW',
          tag: 'ERK_PGW',
          version: '',
          insideNat: false,
          contentDeliveryVia: 'directFromAF',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            secinfoid: '',
            ownnetid: '',
            transport: 'UDP',
            destPort: '0',
          }],
        };

        const template = accessFunctionTemplates.ERK_PGW;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('MVNR_PGW', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_PGW',
          tag: 'MVNR_PGW',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        };

        const template = accessFunctionTemplates.MVNR_PGW;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_PGW',
          tag: 'MVNR_PGW',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            dscp: '0',
            ownnetid: '',
            litype: 'INI1',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVRN_UAG_PGW',
            destIPAddr: '0.0.0.0',
            keepaliveinterval: '60',
            litargetnamespace: 'http://mavenir.net/li/',
            secinfoid: '',
            transport: 'HTTP',
            uri: '',
            version: '1.0',
            role: 'CLIENT',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI2',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVRN_UAG_PGW',
            dscp: '0',
            iri_payload_encoding: 'NONE',
            maxx2connsperaf: '4',
            secinfoid: 'Tk9ORQ',
            transport: 'TCP',
            version: '1.0',
            role: 'SERVER',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            appProtocol: 'MVRN_UAG_PGW',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI3',
            reqState: 'ACTIVE',
            state: '',
            transport: 'TCP',
            version: '1.0',
            role: 'SERVER',
          }],
        };

        const template = accessFunctionTemplates.MVNR_PGW;
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces and contentDeliveryVia is trafficManager', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_PGW',
          tag: 'MVNR_PGW',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'trafficManager',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            dscp: '32',
            lbid: '',
            maxx2connsperaf: '4',
            traceLevel: '0',
            type: 'MVNRPGW_TCP',
          }],
        };

        const template = accessFunctionTemplates.MVNR_PGW;
        (template.general as FormTemplateSection).fields.contentDeliveryVia.initial = 'trafficManager';
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('MVNR_SBC', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_SBC',
          tag: 'MVNR_SBC',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        };

        const template = accessFunctionTemplates.MVNR_SBC;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_SBC',
          tag: 'MVNR_SBC',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            dscp: '0',
            ownnetid: '',
            litype: 'INI1',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVNR_UAG_SBC',
            destIPAddr: '0.0.0.0',
            keepaliveinterval: '60',
            litargetnamespace: 'http://mavenir.net/li/',
            secinfoid: '',
            transport: 'HTTP',
            uri: '',
            version: '1.1',
            role: 'CLIENT',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI2',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVNR_UAG_SBC',
            dscp: '0',
            iri_payload_encoding: 'NONE',
            maxx2connsperaf: '4',
            secinfoid: 'Tk9ORQ',
            transport: 'TCP',
            version: '1.1',
            role: 'SERVER',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            appProtocol: 'MVNR_UAG_SBC',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI3',
            reqState: 'ACTIVE',
            state: '',
            transport: 'TCP',
            version: '1.1',
            role: 'SERVER',
          }],
        };

        const template = accessFunctionTemplates.MVNR_SBC;
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces and contentDeliveryVia is trafficManager', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_SBC',
          tag: 'MVNR_SBC',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'trafficManager',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            dscp: '32',
            lbid: '',
            maxx2connsperaf: '4',
            traceLevel: '0',
            type: 'MVNRSBC_TCP',
          }],
        };

        const template = accessFunctionTemplates.MVNR_SBC;
        (template.general as FormTemplateSection).fields.contentDeliveryVia.initial = 'trafficManager';
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('MVNR_CSCF', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_CSCF',
          tag: 'MVNR_CSCF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [],
          dataInterfaces: [],
          contentInterfaces: [],
        };

        const template = accessFunctionTemplates.MVNR_CSCF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_CSCF',
          tag: 'MVNR_CSCF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            dscp: '0',
            ownnetid: '',
            litype: 'INI1',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVNR_UAG_SBC',
            destIPAddr: '0.0.0.0',
            keepaliveinterval: '60',
            litargetnamespace: 'http://mavenir.net/li/',
            secinfoid: '',
            transport: 'HTTP',
            uri: '',
            version: '1.1',
            role: 'CLIENT',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI2',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'MVNR_UAG_SBC',
            dscp: '0',
            iri_payload_encoding: 'NONE',
            maxx2connsperaf: '4',
            secinfoid: 'Tk9ORQ',
            transport: 'TCP',
            version: '1.1',
            role: 'SERVER',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            appProtocol: 'MVNR_UAG_SBC',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            litype: 'INI3',
            reqState: 'ACTIVE',
            state: '',
            transport: 'TCP',
            version: '1.1',
            role: 'SERVER',
          }],
        };

        const template = accessFunctionTemplates.MVNR_CSCF;
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces and contentDeliveryVia is trafficManager', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_CSCF',
          tag: 'MVNR_CSCF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'trafficManager',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            dscp: '32',
            lbid: '',
            maxx2connsperaf: '4',
            traceLevel: '0',
            type: 'MVNRCSCF_TCP',
          }],
        };

        const template = accessFunctionTemplates.MVNR_CSCF;
        (template.general as FormTemplateSection).fields.contentDeliveryVia.initial = 'trafficManager';
        expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
      });
    });

    describe('NSNHLR_SDM_PRGW', () => {
      it('should return a AccessFunctionFormData instance based in the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NSNHLR',
          tag: 'NSNHLR_SDM_PRGW',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            cdnetid: '',
            cdport: '',
            interval: '20',
            ownIPPort: '',
            dscp: '32',
            ownnetid: '',
            secinfoid: '',
            reqState: 'ACTIVE',
            state: '',
          }],
        };

        const template = accessFunctionTemplates.NSNHLR_SDM_PRGW;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NSNHLR', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NSNHLR',
          tag: 'NSNHLR',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '',
            dscp: '32',
            reqState: 'ACTIVE',
            secinfoid: '',
            state: '',
          }],
        };

        const template = accessFunctionTemplates.NSNHLR;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NSN_SAEGW_PGW', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'STA_PGW',
          tag: 'NSN_SAEGW_PGW',
          version: '',
          insideNat: false,
          contentDeliveryVia: 'directFromAF',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            ipduid: '',
          }],
        };

        const template = accessFunctionTemplates.NSN_SAEGW_PGW;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NSN_SAEGW', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ALU_SGW',
          tag: 'NSN_SAEGW',
          version: '',
          insideNat: false,
          contentDeliveryVia: 'directFromAF',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            command: '0',
            destIPAddr: '',
            destPort: '22',
            dfid: '1',
            dscp: '0',
            reqState: 'ACTIVE',
            state: '',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            dscp: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            tpkt: false,
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '0',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            tpkt: false,
          }],
        };

        const template = accessFunctionTemplates.NSN_SAEGW;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });

      it('should return a AccessFunctionFormData instance based on the template when contentDeliveryVia is trafficManager', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ALU_SGW',
          tag: 'NSN_SAEGW',
          version: '',
          insideNat: false,
          contentDeliveryVia: 'trafficManager',
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            destIPAddr: '',
            destPort: '45092',
            lbid: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            traceLevel: '0',
            type: 'ALUSGW',
          }],
        };

        const template = accessFunctionTemplates.NSN_SAEGW;
        (template.general as FormTemplateSection).fields.contentDeliveryVia.initial = 'trafficManager';
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('GIZMO', () => {
      it('should return a AccessFunctionFormData instance based on the template expandByDefault= true - display interfaces', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'GIZMO',
          tag: 'GIZMO',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'directFromAF',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            secinfoid: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            appProtocol: 'GIZMO',
            dscp: '0',
            litargetnamespace: 'http://gizmo.net/li/03221/x1/2017/10',
            version: '1.3.1',
            transport: 'HTTP',
          }],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'SERVER',
            appProtocol: 'GIZMO',
            version: '1.3.1',
            destIPAddr: '',
            destPort: '0',
            secinfoid: 'Tk9ORQ',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            transport: 'TCP',
            dscp: '0',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'SERVER',
            appProtocol: 'GIZMO',
            version: '1.3.1',
            destIPAddr: '',
            destPort: '0',
            secinfoid: 'Tk9ORQ',
            ownnetid: '',
            reqState: 'ACTIVE',
            state: '',
            transport: 'TCP',
            dscp: '0',
          }],
        };
        const template = accessFunctionTemplates.GIZMO;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ERK_5GAMF', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ERK_5GAMF',
          tag: 'ERK_5GAMF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            httpversion: 'HTTP/2',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'ERK_5GAMF',
            version: '1.7.1',
            transport: 'HTTP',
            processOptionSecInfo: 'None',
            mdf2NodeName: '',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.ERK_5GAMF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NOK_5GUDM', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NOK_5GUDM',
          tag: 'NOK_5GUDM',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'NOK_5GUDM',
            version: '1.7.1',
            transport: 'HTTP',
            httpversion: 'HTTP/2',
            processOptionSecInfo: 'None',
            mdf2NodeName: '',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.NOK_5GUDM;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NSN_SDM', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NSN_SDM',
          tag: 'NSN_SDM',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: 'SDM_PRGW',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            uri: '',
            ifid: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            secinfoid: '',
            cdnetid: '',
            cdport: '',
            state: '',
            mdf2NodeName: '',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.NSN_SDM;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NOK_5GAMF', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NOK_5GAMF',
          tag: 'NOK_5GAMF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            httpversion: 'HTTP/1.1',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'NOK_5GAMF',
            version: '1.4.1',
            transport: 'HTTP',
            processOptionSecInfo: 'None',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.NOK_5GAMF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NOK_5GSMF', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NOK_5GSMF',
          tag: 'NOK_5GSMF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            httpversion: 'HTTP/2',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'NOK_5GSMF',
            version: '1.6.1',
            transport: 'HTTP',
            processOptionSecInfo: 'None',
          }],
          dataInterfaces: [],
          contentInterfaces: [],
        };
        const template = accessFunctionTemplates.NOK_5GSMF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NOK_5GSMSF', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NOK_5GSMSF',
          tag: 'NOK_5GSMSF',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            httpversion: 'HTTP/2',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'NOK_5GSMSF',
            version: '1.5.1',
            transport: 'HTTP',
            processOptionSecInfo: 'None',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.NOK_5GSMSF;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('MVNR_5GSMSC', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'MVNR_5GSMSC',
          tag: 'MVNR_5GSMSC',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            uri: '',
            ifid: '',
            keepaliveinterval: '60',
            httpversion: 'HTTP/1.1',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '0.0.0.0',
            state: '',
            appProtocol: 'MVNR_5GSMSC',
            version: '1.1.1',
            transport: 'HTTP',
            processOptionSecInfo: 'None',
            connection: 'SHORT',
          }],
          dataInterfaces: [],
        };
        const template = accessFunctionTemplates.MVNR_5GSMSC;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('NKSR', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'NKSR',
          tag: 'NKSR',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: 'NOKIASR7750',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          provisioningInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            role: 'CLIENT',
            ifid: '',
            ownnetid: '',
            reqState: 'ACTIVE',
            destIPAddr: '',
            destPort: '',
            state: '',
            version: 'R15',
            transport: 'SSH',
            serviceid: '',
            sshInfoId: '',
            appProtocol: 'CCLI',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            name: '',
            ifid: '',
            appProtocol: 'NOKIASR',
            ownnetid: '',
            role: 'SERVER',
            transport: 'UDP',
            version: 'R15',
            reqState: 'ACTIVE',
            state: '',
          }],
        };
        const template = accessFunctionTemplates.NKSR;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    describe('ETSI103221', () => {
      it('should return a AccessFunctionFormData instance based on the template', () => {
        const expected: AccessFunctionFormData = {
          id: '',
          name: '',
          description: '',
          timeZone: '',
          type: 'ETSI103221',
          tag: 'ETSI103221',
          version: '',
          insideNat: false,
          country: '',
          stateName: '',
          city: '',
          liInterface: 'ALL',
          model: '',
          preprovisioningLead: '000:00',
          serialNumber: '',
          contact: '',
          contentDeliveryVia: 'ss8Distributor',
          dataDeliveryVia: 'ss8Distributor',
          provisioningInterfaces: [],
          dataInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            dataDeliveryVia: 'ss8Distributor',
          }],
          contentInterfaces: [{
            id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
            contentDeliveryVia: 'ss8Distributor',
          }],
        };
        const template = accessFunctionTemplates.ETSI103221;
        expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
      });
    });

    it('should return a AccessFunctionFormData instance based in the template when forcing all interfaces', () => {
      const expected: AccessFunctionFormData = {
        id: '',
        name: '',
        description: '',
        timeZone: '',
        type: 'ETSI103221',
        tag: 'ETSI103221',
        version: '',
        insideNat: false,
        country: '',
        stateName: '',
        city: '',
        liInterface: 'ALL',
        model: '',
        preprovisioningLead: '000:00',
        serialNumber: '',
        contact: '',
        contentDeliveryVia: 'ss8Distributor',
        dataDeliveryVia: 'ss8Distributor',
        provisioningInterfaces: [{
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          appProtocol: 'ETSI103221',
          dscp: '0',
          keepaliveinterval: '60',
          litargetnamespace: 'http://uri.etsi.org/03221/X1/2017/10',
          ownnetid: '',
          reqState: 'ACTIVE',
          secinfoid: '',
          state: '',
          transport: 'HTTP',
          uri: '',
          version: '1.3.1',
        }],
        dataInterfaces: [{
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          dataDeliveryVia: 'ss8Distributor',
        }],
        contentInterfaces: [{
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          contentDeliveryVia: 'ss8Distributor',
        }],
      };
      const template = accessFunctionTemplates.ETSI103221;
      expect(createAccessFunctionBasedOnTemplate(template, true)).toEqual(expected);
    });

    it('should return a AccessFunctionFormData instance based on the template when contentDeliveryVia is directFromAF', () => {
      const expected: AccessFunctionFormData = {
        id: '',
        name: '',
        description: '',
        timeZone: '',
        type: 'ETSI103221',
        tag: 'ETSI103221',
        version: '',
        insideNat: false,
        country: '',
        stateName: '',
        city: '',
        liInterface: 'ALL',
        model: '',
        preprovisioningLead: '000:00',
        serialNumber: '',
        contact: '',
        contentDeliveryVia: 'ss8Distributor',
        dataDeliveryVia: 'ss8Distributor',
        provisioningInterfaces: [],
        dataInterfaces: [{
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          dataDeliveryVia: 'directFromAF',
        }],
        contentInterfaces: [{
          id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
          contentDeliveryVia: 'directFromAF',
        }],
      };
      const template = accessFunctionTemplates.ETSI103221;
      (template.contentInterfaces as FormTemplateSection).fields.contentDeliveryVia.initial = 'directFromAF';
      (template.dataInterfaces as FormTemplateSection).fields.dataDeliveryVia.initial = 'directFromAF';
      expect(createAccessFunctionBasedOnTemplate(template)).toEqual(expected);
    });
  });
});
