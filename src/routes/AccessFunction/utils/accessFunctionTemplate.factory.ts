import { FormTemplate, FormTemplateSection, SectionType } from 'data/template/template.types';
import isNil from 'lodash.isnil';
import omitBy from 'lodash.omitby';
import {
  AccessFunctionFormData,
  ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID,
  ContentInterfaceFormData,
  ProvisioningInterfaceFormData,
  DataInterfaceFormData,
  AccessFunctionInterfaceFormSectionType,
} from 'data/accessFunction/accessFunction.types';

import { getInterfaceFormTemplateSectionName } from './accessFunctionTemplate.adapter';

let nextId = 1;

// eslint-disable-next-line import/prefer-default-export
export function createAccessFunctionBasedOnTemplate(
  template?: FormTemplate,
  forceInterfaceCreation = false,
): Partial<AccessFunctionFormData> {
  if (template == null) {
    return {};
  }

  const accessFunctionForm: Partial<AccessFunctionFormData> = {
    ...getFieldValuesFromSection(template.general as FormTemplateSection),
    ...getFieldValuesFromSection(template.systemInformation as FormTemplateSection),
    ...getFieldValuesFromSubsection(template),
    dataInterfaces: getInterfaceFieldValuesFromSection(
      template,
      'dataInterfaces',
      forceInterfaceCreation,
    ),
    provisioningInterfaces: getInterfaceFieldValuesFromSection(
      template,
      'provisioningInterfaces',
      forceInterfaceCreation,
    ),
    contentInterfaces: getInterfaceFieldValuesFromSection(
      template,
      'contentInterfaces',
      forceInterfaceCreation,
    ),
  };
  const formData = omitBy(accessFunctionForm, isNil) as unknown as AccessFunctionFormData;
  return formData;
}

function getFieldValuesFromSection(
  templateSection: FormTemplateSection,
): Partial<AccessFunctionFormData> {
  return Object.entries(templateSection?.fields)
    .filter(([_fieldName, field]) => field.initial != null)
    .reduce((previousValue, [fieldName, field]) => ({
      ...previousValue,
      [fieldName]: field.initial,
    }), {});
}

function getFieldValuesFromSubsection(
  template: FormTemplate,
) : Partial<AccessFunctionFormData> {
  if (template.key === 'ETSI103221') {
    return {
      contentDeliveryVia: 'ss8Distributor',
      dataDeliveryVia: 'ss8Distributor',
    };
  }
  return {};
}

function getInterfaceFieldValuesFromSection(
  template: FormTemplate,
  interfaceName: AccessFunctionInterfaceFormSectionType,
  forceInterfaceCreation = false,
): (ContentInterfaceFormData | ProvisioningInterfaceFormData | DataInterfaceFormData)[] | undefined {

  const generalSection = getFieldValuesFromSection(template.general as FormTemplateSection);
  const contentDeliveryViaType = generalSection?.contentDeliveryVia ?? '';

  const templateSectionName = getInterfaceFormTemplateSectionName(template, interfaceName, contentDeliveryViaType);
  const templateSection = template[templateSectionName] as FormTemplateSection;

  if (templateSection == null) {
    return;
  }

  const expandByDefault = (template[templateSectionName] as FormTemplateSection).metadata.expandByDefault || false;
  if (!forceInterfaceCreation && (
    ((template[templateSectionName] as FormTemplateSection).metadata.min === 0 && expandByDefault !== true) ||
      templateSection?.metadata?.sectionType === SectionType.distributorConfiguration
  )) {
    return [];
  }

  const fields = {
    ...getFieldValuesFromSection(template[templateSectionName] as FormTemplateSection),
    id: `${nextId}.${ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID}`,
  };

  nextId += 1;

  return [fields as ContentInterfaceFormData | ProvisioningInterfaceFormData | DataInterfaceFormData];
}
