import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import getExistingAccessFunctionNames from './getExistingAccessFunctionNames';

describe('getExistingAccessFunctionNames', () => {
  const accessFunctionsType1: AccessFunction[] = [
    {
      name: 'name1 type1',
      id: 'id1_type1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      type: 'type1',
      timeZone: 'timeZone1',
    },
    {
      id: 'id2_type1',
      name: 'name2 type1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      timeZone: 'timeZone1',
      type: 'type1',
    },
  ];

  const accessFunctionsType2: AccessFunction[] = [
    {
      name: 'name1 type2',
      id: 'id1_type2',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      type: 'type2',
      timeZone: 'timeZone1',
    },
    {
      name: 'name2 type2',
      id: 'id2_type2',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      timeZone: 'timeZone1',
      type: 'type2',
    },
  ];

  const accessFunctionsType3: AccessFunction[] = [
    {
      id: 'id1_type3',
      tag: 'tag1',
      name: 'name1 type3',
      type: 'type3',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      timeZone: 'timeZone1',
    },
    {
      name: 'name2 type3',
      id: 'id2_type3',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      timeZone: 'timeZone1',
      type: 'type3',
    },
  ];

  const accessFunctionsByType = {
    type1: accessFunctionsType1,
    type2: accessFunctionsType2,
    type3: accessFunctionsType3,
  };

  it('should return a list of AccessFunction names avoid including the creating and placeholder access function', () => {
    expect(getExistingAccessFunctionNames(accessFunctionsByType)).toEqual([
      'name1 type1',
      'name2 type1',
      'name1 type2',
      'name2 type2',
      'name1 type3',
      'name2 type3',
    ]);
  });

  it('should return a list of AccessFunction names avoid including the editing Access Function', () => {
    expect(getExistingAccessFunctionNames(accessFunctionsByType, 'id1_type2')).toEqual([
      'name1 type1',
      'name2 type1',
      'name2 type2',
      'name1 type3',
      'name2 type3',
    ]);
  });

});
