import { ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID } from 'data/accessFunction/accessFunction.types';
import { FormTemplate } from 'data/template/template.types';
import { mockContacts } from '__test__/mockContacts';
import { mockNetworks } from '__test__/mockNetworks';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockXdps } from '__test__/mockXdps';
import { mockXtps } from '__test__/mockXtps';
import { mockNodes } from '__test__/mockNodes';
import { adaptContentDeliveryViaField, adaptTemplateOptions } from './accessFunctionTemplate.adapter';
import { createAccessFunctionBasedOnTemplate } from './accessFunctionTemplate.factory';
import { handleContentDeliveryViaFieldUpdate, handleInterfacesSectionDeliveryViaFieldUpdate } from './handleContentDeliveryViaFieldUpdate';

describe('handleContentDeliveryViaFieldUpdate()', () => {
  let templates: { [key: string]: FormTemplate };

  beforeEach(() => {
    const accessFunctionTemplates = {
      MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
      LU3G_CSCF: getMockTemplateByKey('LU3G_CSCF'),
    };

    const countries = ['US'];

    templates = adaptTemplateOptions(
      accessFunctionTemplates,
      countries,
      mockTimeZones,
      mockContacts,
      mockNetworks,
      mockSecurityInfos,
      mockSshSecurityInfos,
      mockXtps,
      mockXdps,
      mockProcessOptions,
      mockSupportedFeatures.config.accessFunctions,
      mockNodes,
    );
  });

  it('should return the data if template is undefined', () => {
    expect(handleContentDeliveryViaFieldUpdate(undefined, {}, 'trafficManager')).toEqual({});
  });

  it('should return the data with the contentInterfaces redefined for LU3G_CSCF', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.LU3G_CSCF, false);
    const adaptedTemplate = adaptContentDeliveryViaField(templates.LU3G_CSCF, 'trafficManager');
    expect(handleContentDeliveryViaFieldUpdate(adaptedTemplate, data, 'trafficManager')).toEqual({
      ...data,
      contentInterfaces: [{
        id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
        lbid: 'weq',
        ownnetid: 'WENQcmNz',
        reqState: 'ACTIVE',
        destIPAddr: '',
        destPort: '',
        state: '',
        traceLevel: '0',
        type: 'LU3GCSCF',
      }],
    });
  });

  it('should return the data with the contentInterfaces redefined for MVNR_PGW', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.MVNR_PGW, true);
    const adaptedTemplate = adaptContentDeliveryViaField(templates.MVNR_PGW, 'trafficManager');
    expect(handleContentDeliveryViaFieldUpdate(adaptedTemplate, data, 'trafficManager')).toEqual({
      ...data,
      contentInterfaces: [{
        id: expect.stringContaining(ACCESS_FUNCTION_INTERFACE_TEMPORARY_ID),
        name: '',
        destIPAddr: '',
        destPort: '0',
        dscp: '32',
        lbid: 'weq',
        maxx2connsperaf: '4',
        ownnetid: 'WENQcmNz',
        reqState: 'ACTIVE',
        state: '',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }],
    });
  });

  it('should return the data as is for MVNR_PGW if no change in contentDeliveryVia', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.MVNR_PGW, true);
    const adaptedTemplate = adaptContentDeliveryViaField(templates.MVNR_PGW, 'directFromAF');
    expect(handleContentDeliveryViaFieldUpdate(adaptedTemplate, data, 'directFromAF')).toEqual(data);
  });
});

describe('handleDeliveryViaFieldUpdate', () => {
  let templates: { [key: string]: FormTemplate };

  beforeEach(() => {
    const accessFunctionTemplates = {
      ETSI103221: getMockTemplateByKey('ETSI103221'),
    };

    const countries = ['US'];

    templates = adaptTemplateOptions(
      accessFunctionTemplates,
      countries,
      mockTimeZones,
      mockContacts,
      mockNetworks,
      mockSecurityInfos,
      mockSshSecurityInfos,
      mockXtps,
      mockXdps,
      mockProcessOptions,
      mockSupportedFeatures.config.accessFunctions,
      mockNodes,
    );
  });

  it('should populate contentDeliveryVia when content interface section\'s \'Deliver via\' is changed to \'SS8 Distributor\'', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.ETSI103221, true);
    expect(handleInterfacesSectionDeliveryViaFieldUpdate(
      templates.ETSI103221,
      'contentInterfaces',
      data,
      'contentDeliveryVia',
      'ss8Distributor',
    )).toEqual(expect.objectContaining({ contentDeliveryVia: 'ss8Distributor' }));
  });

  it('should populate contentDeliveryVia when content interface section\'s \'Deliver via\' is changed to \'directFromAF\'', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.ETSI103221, true);
    expect(handleInterfacesSectionDeliveryViaFieldUpdate(
      templates.ETSI103221,
      'contentInterfaces',
      data,
      'contentDeliveryVia',
      'directFromAF',
    )).toEqual(expect.objectContaining({ contentDeliveryVia: 'directFromAF' }));
  });

  it('should populate dataDeliveryVia when data interface section\'s \'Deliver via\' is changed is changed to \'SS8 Distributor\'', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.ETSI103221, true);
    expect(handleInterfacesSectionDeliveryViaFieldUpdate(
      templates.ETSI103221,
      'dataInterfaces',
      data,
      'dataDeliveryVia',
      'ss8Distributor',
    )).toEqual(expect.objectContaining({ dataDeliveryVia: 'ss8Distributor' }));
  });

  it('should populate dataDeliveryVia when data interface section\'s \'Deliver via\' is changed is changed to \'directFromAF\'', () => {
    const data = createAccessFunctionBasedOnTemplate(templates.ETSI103221, true);
    expect(handleInterfacesSectionDeliveryViaFieldUpdate(
      templates.ETSI103221,
      'dataInterfaces',
      data,
      'dataDeliveryVia',
      'directFromAF',
    )).toEqual(expect.objectContaining({ dataDeliveryVia: 'directFromAF' }));
  });

});
