import { FormTemplate, FormTemplateSection } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export function getDistributorConfigurationsFilter(
  template: FormTemplate,
): string {
  const types = getTypesFromTemplate(template);

  return types.map((type) => `afType eq '${type}'`).join(' or ');
}

function getTypesFromTemplate(template: FormTemplate): string[] {
  const types: string[] = [];

  const templateType = (template.general as FormTemplateSection).fields?.type?.initial;
  if (templateType && typeof templateType === 'string') {
    types.push(templateType);
  }

  Object.values(template).forEach((section) => {
    if (typeof section === 'string' || section.metadata.sectionType !== 'distributorConfiguration') {
      return;
    }

    const distributorsType = section?.fields?.distributorsType?.initial;
    if (distributorsType != null && typeof distributorsType === 'string') {
      types.push(distributorsType);
    }
  });

  return [...new Set(types)];
}
