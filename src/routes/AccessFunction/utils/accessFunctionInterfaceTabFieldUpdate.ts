import cloneDeep from 'lodash.clonedeep';
import { AccessFunctionFormData, AccessFunctionInterfaceFormData, AccessFunctionInterfaceFormSectionType } from 'data/accessFunction/accessFunction.types';
import { FormTemplate, FormTemplateField } from 'data/template/template.types';
import { NodeData } from 'data/nodes/nodes.types';
import { getInterfaceFormTemplateSection, getSubsectionName } from './accessFunctionTemplate.adapter';
import { handleInterfacesSectionDeliveryViaFieldUpdate } from './handleContentDeliveryViaFieldUpdate';

// eslint-disable-next-line import/prefer-default-export
export function handleInterfacesFieldUpdate(
  template: FormTemplate | undefined,
  data: Partial<AccessFunctionFormData>,
  formSection: AccessFunctionInterfaceFormSectionType,
  interfaceId: string | undefined,
  fieldName: keyof AccessFunctionInterfaceFormData,
  fieldValue: unknown,
  xcipioNodes: NodeData[],
): Partial<AccessFunctionFormData> {
  if (template == null) {
    return data;
  }

  let clonedData = cloneDeep(data);
  clonedData = handleInterfacesSectionDeliveryViaFieldUpdate(template, formSection, clonedData, fieldName, fieldValue);

  const formSubsectionName = getSubsectionName(data, template, formSection);

  clonedData[formSection] = clonedData[formSection]?.map((interfaceEntry) => {
    if (interfaceEntry.id === interfaceId) {
      return handleInterfaceFieldUpdate(
        template,
        formSection,
        formSubsectionName,
        interfaceEntry,
        fieldName,
        fieldValue,
        xcipioNodes,
      );
    }

    return interfaceEntry;
  });

  return clonedData;
}

export function fieldAffectsOtherFieldsOnChange(formTemplateField: FormTemplateField | undefined): boolean {
  if (formTemplateField == null || formTemplateField.affectedFieldsOnChange == null) {
    return false;
  }

  return formTemplateField.affectedFieldsOnChange.length > 0;
}

/**
 * Sometimes when a field is changed, other field data needs to be set to a certain value.
 * This function set the data of dependent fields when value of a controlling field is changed.
 */
function handleInterfaceFieldUpdate(
  template: FormTemplate,
  formSection: AccessFunctionInterfaceFormSectionType,
  formSubsectionName: string,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldName: keyof AccessFunctionInterfaceFormData,
  fieldValue: unknown,
  xcipioNodes: NodeData[],
) {
  setInterfaceFieldValue(interfaceFormData, fieldName, fieldValue);

  const templateSection = getInterfaceFormTemplateSection(template, formSection, formSubsectionName);
  const templateField = templateSection?.fields[fieldName];

  if (formSection === 'provisioningInterfaces' && fieldName === 'uri') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      interfaceFormData = setInterfaceValuesForUriValue(
        templateField?.affectedFieldsOnChange,
        interfaceFormData,
        fieldValue as string,
      );
    }
  }

  if (formSection === 'provisioningInterfaces' && fieldName === 'ccnodename') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      interfaceFormData = setInterfaceValuesForCcNodeNameValue(
        templateField?.affectedFieldsOnChange,
        interfaceFormData,
        fieldValue as string,
        xcipioNodes,
      );
    }
  }

  if (formSection === 'contentInterfaces' && fieldName === 'maxx2connsperaf') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      interfaceFormData = setInterfaceValuesForMaxx2ConnValue(
        templateField?.affectedFieldsOnChange,
        interfaceFormData,
        fieldValue as string,
      );
    }
  }

  if (formSection === 'dataInterfaces' && fieldName === 'tpkt') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      interfaceFormData = setInterfaceValuesForTPKTValue(
        templateField?.affectedFieldsOnChange,
        interfaceFormData,
        fieldValue as boolean,
      );
    }
  }

  if (formSection === 'dataInterfaces' && fieldName === 'ftpType') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      interfaceFormData = setInterfaceValuesForFtpTypeValue(
        templateField?.affectedFieldsOnChange,
        interfaceFormData,
        fieldValue as string,
      );
    }
  }

  return interfaceFormData;
}

function setInterfaceFieldValue(
  interfaceFormData: any,
  fieldName: string,
  value: unknown,
): AccessFunctionInterfaceFormData {
  interfaceFormData[fieldName] = value;

  return interfaceFormData;
}

function setInterfaceValuesForUriValue(
  affectedFieldsOnChange: string[] | undefined,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldValue: string,
): AccessFunctionInterfaceFormData {
  if (affectedFieldsOnChange == null) {
    return interfaceFormData;
  }

  if (affectedFieldsOnChange.includes('secinfoid')) {
    if (fieldValue.toLocaleLowerCase().startsWith('http:') && interfaceFormData.secinfoid === '') {
      interfaceFormData.secinfoid = 'Tk9ORQ';

    } else if (fieldValue.toLocaleLowerCase().startsWith('https:') && interfaceFormData.secinfoid === 'Tk9ORQ') {
      interfaceFormData.secinfoid = '';
    }
  }

  return interfaceFormData;
}

function setInterfaceValuesForCcNodeNameValue(
  affectedFieldsOnChange: string[] | undefined,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldValue: string,
  xcipioNodes: NodeData[],
): AccessFunctionInterfaceFormData {
  if (affectedFieldsOnChange == null) {
    return interfaceFormData;
  }

  if (affectedFieldsOnChange.includes('mdf3NodeName')) {
    const ccPagNode = xcipioNodes.find(({ nodeName }) => nodeName === fieldValue);

    if (ccPagNode != null && ccPagNode.destNodeName != null && ccPagNode.destNodeName !== '') {
      interfaceFormData.mdf3NodeName = ccPagNode.destNodeName;
    }
  }

  return interfaceFormData;
}

function setInterfaceValuesForTPKTValue(
  affectedFieldsOnChange: string[] | undefined,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldValue: boolean | undefined,
): AccessFunctionInterfaceFormData {
  if (affectedFieldsOnChange == null) {
    return interfaceFormData;
  }

  if (affectedFieldsOnChange.includes('x224headerpresent')) {
    if (fieldValue ?? true) {
      interfaceFormData.x224headerpresent = true;
    } else {
      interfaceFormData.x224headerpresent = false;
    }
  }

  return interfaceFormData;
}

function setInterfaceValuesForMaxx2ConnValue(
  affectedFieldsOnChange: string[] | undefined,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldValue: string,
): AccessFunctionInterfaceFormData {
  if (affectedFieldsOnChange == null) {
    return interfaceFormData;
  }

  if (affectedFieldsOnChange.includes('destPort')) {
    if (+fieldValue > 4) {
      interfaceFormData.destPort = '0';
    }
  }

  return interfaceFormData;
}

function setInterfaceValuesForFtpTypeValue(
  affectedFieldsOnChange: string[] | undefined,
  interfaceFormData: AccessFunctionInterfaceFormData,
  fieldValue: string,
): AccessFunctionInterfaceFormData {
  if (affectedFieldsOnChange == null) {
    return interfaceFormData;
  }

  if (affectedFieldsOnChange.includes('secinfoid')) {
    if (fieldValue === 'FTP' && interfaceFormData.secinfoid === '') {
      interfaceFormData.secinfoid = 'Tk9ORQ';

    } else if (fieldValue === 'FTPS' && interfaceFormData.secinfoid === 'Tk9ORQ') {
      interfaceFormData.secinfoid = '';
    }
  }
  return interfaceFormData;
}
