import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import { FormTemplateMapByKey } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export function isAccessFunctionTemplateAvailable(
  templates: FormTemplateMapByKey,
  accessFunction?: AccessFunction,
): boolean {
  if (accessFunction == null) {
    return false;
  }

  const accessFunctionTag = accessFunction.tag;

  return templates[accessFunctionTag] != null;
}
