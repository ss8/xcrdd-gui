import { AccessFunctionInterfaceFormData } from 'data/accessFunction/accessFunction.types';
import validateSameNetworkIds from './validateSameNetworkIds';

describe('validateSameNetworkIds', () => {
  it('should return true if there is no interfaces in the list', () => {
    expect(validateSameNetworkIds([])).toBeTruthy();
  });

  it('should return true if there is no just one interface in the list', () => {
    const interfaces:AccessFunctionInterfaceFormData[] = [
      { id: '1', name: 'name 1', ownnetid: 'ownnetid' },
    ];

    expect(validateSameNetworkIds(interfaces)).toBeTruthy();
  });

  it('should return true if all interfaces have the same ownnetid ', () => {
    const interfaces:AccessFunctionInterfaceFormData[] = [
      { id: '1', name: 'name 1', ownnetid: 'ownnetid' },
      { id: '2', name: 'name 2', ownnetid: 'ownnetid' },
      { id: '3', name: 'name 3', ownnetid: 'ownnetid' },
    ];

    expect(validateSameNetworkIds(interfaces)).toBeTruthy();
  });

  it('should return false if not all interfaces have the same ownnetid ', () => {
    const interfaces:AccessFunctionInterfaceFormData[] = [
      { id: '1', name: 'name 1', ownnetid: 'ownnetid' },
      { id: '2', name: 'name 2', ownnetid: 'ownnetid' },
      { id: '3', name: 'name 3', ownnetid: 'ownnetid3' },
    ];

    expect(validateSameNetworkIds(interfaces)).toBeFalsy();
  });

});
