import ipRegex from 'ip-regex';
import cloneDeep from 'lodash.clonedeep';
import lodashTemplate from 'lodash.template';
import {
  FormTemplate,
  FormTemplateField,
  FormTemplateOption,
  FormTemplateSection,
} from 'data/template/template.types';
import {
  Contact, Deployment, ValueLabelPair,
} from 'data/types';
import { getSortedTimeZoneValueLabels } from 'shared/timeZoneUtils/timeZone.utils';
import { Network } from 'data/network/network.types';
import { SecurityInfo, SecurityInfoFilter, SecurityInfoFilterByInterface } from 'data/securityInfo/securityInfo.types';
import { SupportedFeaturesAccessFunctions, SupportedFeaturesAccessFunction } from 'data/supportedFeatures/supportedFeatures.types';
import {
  AccessFunctionFormData,
  AccessFunctionInterfaceFormData,
  AccessFunctionInterfaceFormSectionType,
  ACCESS_FUNCTION_REQ_STATE_FIELD,
  ACCESS_FUNCTION_STATE_FIELD,
} from 'data/accessFunction/accessFunction.types';
import { Xtp } from 'data/xtp/xtp.types';
import { Xdp } from 'data/xdp/xdp.types';
import { ProcessOption } from 'data/processOption/processOption.types';
import { SshSecurityInfo } from 'data/sshSecurityInfo/sshSecurityInfo.types';
import { isHeaderSelectionPanel } from 'routes/AccessFunction/components/MultifacetedFormSection/MultifacetedForm.utils';
import { NodeData } from 'data/nodes/nodes.types';
import { EMPTY_FIELD } from 'shared/form';
import { fieldAffectsOtherFieldsOnChange } from './accessFunctionInterfaceTabFieldUpdate';

const POSSIBLE_INTERFACES = [
  'provisioningInterfaces',
  'provisioningInterfaces_directFromAF',
  'provisioningInterfaces_trafficManager',
  'dataInterfaces',
  'dataInterfaces_directFromAF',
  'dataInterfaces_trafficManager',
  'contentInterfaces',
  'contentInterfaces_directFromAF',
  'contentInterfaces_trafficManager',
];

const TEMPLATE_SETTINGS = {
  interpolate: /\{\{(.+?)\}\}/g,
};

type FormTemplateOptionMap = {
  default: FormTemplateOption[]
  [key: string]: FormTemplateOption[]
}

export function adaptTemplateOptions(
  templates: { [key: string]: FormTemplate },
  countries: string[],
  timezones: ValueLabelPair[],
  contacts: Contact[],
  networks: Network[],
  securityInfos: SecurityInfo[],
  sshSecurityInfos: SshSecurityInfo[],
  xtps: Xtp[],
  xdps: Xdp[],
  processOptions: ProcessOption[],
  enabledAccessFunctions: SupportedFeaturesAccessFunctions,
  xcipioNodes: NodeData[],
): { [key: string]: FormTemplate } {
  const adaptedTemplates: { [key: string]: FormTemplate } = {};

  const tagOptions = getTagOptions(templates, enabledAccessFunctions);
  const countryOptions = getFieldOptions(countries);
  const timezoneOptions = getTimeZoneFieldOptions(timezones);
  const contactOptions = getContactsOptions(contacts);

  const networkIdOptionsMap = getNetworkOptionsMap(networks);
  const securityInfoOptions = getSecurityInfoOptions(securityInfos, []);
  const sshSecurityInfoOptions = getSshSecurityInfoOptions(sshSecurityInfos);
  const xtpIdOptions = getXtpIdOptions(xtps);
  const xdpIdOptions = getXdpIdOptions(xdps);
  const ccpagOptions = getXcipioNodesByType(xcipioNodes, 'CCPAG');
  const mdf2Options = getXcipioNodesByType(xcipioNodes, 'MDF2');
  const mdf3Options = getXcipioNodesByType(xcipioNodes, 'MDF3');

  Object.keys(templates).forEach((templateName) => {
    const template = cloneDeep(templates[templateName]);

    if (enabledAccessFunctions[template.key] == null) {
      return;
    }

    const versionOptions = getSoftwareVersionOptions(enabledAccessFunctions[template.key]);
    const model = getSoftwareModel(enabledAccessFunctions[template.key]);
    const httpVersions = getHTTPVersions(enabledAccessFunctions[template.key]);
    const httpDefaultVersion = getHTTPDefaultVersion(enabledAccessFunctions[template.key]);

    setInitialValueFromProcessOptions(template, processOptions);

    setTagOptions(template, tagOptions);
    setLocationFieldOptions(template, countryOptions, 'country');
    setTimezoneOptions(template, timezoneOptions);
    setContactOptions(template, contactOptions);
    setOptionsForFields(template, networkIdOptionsMap, ['cdnetid', 'netid', 'ownnetid', 'ccnetid']);
    setOptionsForFields(template, securityInfoOptions, ['secinfoid']);
    setOptionsForFields(template, sshSecurityInfoOptions, ['sshInfoId']);
    setOptionsForFields(template, xdpIdOptions, ['ipduid']);
    setOptionsForFields(template, xtpIdOptions, ['lbid']);
    setOptionForTemplateSection(template.systemInformation as FormTemplateSection, versionOptions, ['version']);
    setFieldInitialValueForTemplateSection(template.systemInformation as FormTemplateSection, model, ['model']);
    setOptionForOwnIpAddr(template, xdps);
    setOptionsForFields(template, ccpagOptions, ['ccnodename']);
    setOptionsForFields(template, mdf2Options, ['mdf2NodeName']);
    setOptionsForFields(template, mdf3Options, ['mdf3NodeName']);
    setOptionsForFields(template, httpVersions, ['httpversion']);
    setFieldInitialValueForTemplateSection(template.provisioningInterfaces as FormTemplateSection, httpDefaultVersion, ['httpversion']);

    adaptedTemplates[template.key] = template;
  });

  return adaptedTemplates;
}

export function adaptTemplate5GFields(
  template: FormTemplate | undefined,
  is5GCVcpEnabled: boolean,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  if (is5GCVcpEnabled) {
    return template;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  const provisioningInterfaces = (clonedTemplate.provisioningInterfaces as FormTemplateSection);
  if (provisioningInterfaces != null) {
    clonedTemplate.provisioningInterfaces = adaptTemplateSection5GField(provisioningInterfaces, 'ccnodename');
    clonedTemplate.provisioningInterfaces = adaptTemplateSection5GField(provisioningInterfaces, 'mdf2NodeName');
    clonedTemplate.provisioningInterfaces = adaptTemplateSection5GField(provisioningInterfaces, 'mdf3NodeName');
  }

  return clonedTemplate;
}

function adaptTemplateSection5GField(formTemplateSection: FormTemplateSection, fieldName: string): FormTemplateSection {

  const field = formTemplateSection?.fields?.[fieldName];
  if (field == null) {
    return formTemplateSection;
  }

  field.hide = 'true';

  const { layout } = formTemplateSection.metadata;
  if (layout != null) {
    layout.rows = removeFieldFromLayoutRows(layout.rows, fieldName);
  }

  return formTemplateSection;
}

/**
 * This function is used to remove a field from the form template layout.
 * This handles the following cases:
 * - Add a '$empty' field at the end of the same row if that is tha last row in the layout
 * - Move a field from the next row to a previous row and add '$empty' to the last row
 * - Adjust the state field to be in required position depending on reqState
 *
 * NOTE: This method was made public so it is possible to add UT to it.
 * However it should not be used outside the context of this file.
 *
 * Please check the Unit Tests for documented scenarios.
 *
 * @param rows the row to remove the field
 * @param fieldToRemove the field to be removed
 * @returns the new rows with the field removed
 */
export function removeFieldFromLayoutRows(
  rows: string[][],
  fieldToRemove: string,
): string[][] {
  if (rows == null) {
    return rows;
  }

  // Assume the column size for the form is 4, but get the real number below
  let columnsSize = 4;
  // Get the row index for the field to be removed. Any previous row will
  // remain the same
  const rowIndex = rows.findIndex((row) => {
    const found = row.find((field) => field === fieldToRemove);

    if (found) {
      columnsSize = row.length;
    }

    return found;
  });

  if (rowIndex === -1) {
    // Nothing to be done, the field was not found
    return rows;
  }

  // Rows that will not be changed
  const unchangedRows = rows.slice(0, rowIndex);

  let remainingFields = rows.slice(rowIndex, rows.length).reduce((fields, row) => {
    fields.push(...row);
    return fields;
  }, []);

  // Remove the field to be removed
  remainingFields = remainingFields.filter((field) => field !== fieldToRemove);

  // Remove $empty - they will be added back few lines below
  remainingFields = remainingFields.filter((field) => field !== EMPTY_FIELD);

  // Check if there is state field, remove it to be added at the end
  const hasStateField = remainingFields.find((field) => field === ACCESS_FUNCTION_STATE_FIELD);
  if (hasStateField) {
    // Remove the state field for now
    remainingFields = remainingFields.filter((field) => field !== ACCESS_FUNCTION_STATE_FIELD);
  }

  const changedRows: string[][] = [];
  // Restore each row based in the columnsSize
  for (let i = 0; i <= remainingFields.length; i += columnsSize) {
    const newRow = [];
    for (let j = 0; j < columnsSize; j += 1) {
      newRow.push(remainingFields[i + j] ?? EMPTY_FIELD);
    }

    const isRowEmpty = newRow.every((field) => field === EMPTY_FIELD);
    if (!isRowEmpty) {
      changedRows.push(newRow);
    }
  }

  if (hasStateField) {
    // Restore the state field back
    let reqStatePosition = remainingFields.findIndex((field) => field === ACCESS_FUNCTION_REQ_STATE_FIELD);
    reqStatePosition %= columnsSize;
    const newRow = Array(columnsSize).fill(EMPTY_FIELD);
    newRow[reqStatePosition] = ACCESS_FUNCTION_STATE_FIELD;
    changedRows.push(newRow);
  }

  return [...unchangedRows, ...changedRows];
}

export function adaptTemplateVisibilityMode(
  template: FormTemplate | undefined,
  mode: 'create' | 'edit' | 'view' | 'copy',
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);

  if (mode === 'copy') {
    adaptTemplateForCopyMode(clonedTemplate);

  } else if (mode === 'edit') {
    adaptTemplateForEditMode(clonedTemplate);

  } else if (mode === 'view') {
    adaptTemplateForViewMode(clonedTemplate);
  }

  return clonedTemplate;
}

export function adaptLocationField(
  template: FormTemplate | undefined,
  list: string[],
  field: 'stateName' | 'city',
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  const options: FormTemplateOption[] = getFieldOptions(list);
  setLocationFieldOptions(clonedTemplate, options, field);

  return clonedTemplate;
}

export function adaptTimezoneField(
  template: FormTemplate | undefined,
  timezones: ValueLabelPair[],
  accessFunction: AccessFunctionFormData,
): FormTemplate | undefined {
  if (template == null) {
    return template;
  }

  const accessFunctionTimezoneExists = timezones.some(({ value }) => value === accessFunction.timeZone);
  if (accessFunctionTimezoneExists) {
    return template;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  const newTimezones = [...timezones, { value: accessFunction.timeZone, label: accessFunction.timeZone }];
  const timezoneOptions = getTimeZoneFieldOptions(newTimezones);
  resetTimezoneOptions(clonedTemplate);
  setTimezoneOptions(clonedTemplate, timezoneOptions);

  return clonedTemplate;
}

export function adaptSecurityInfoField(
  template: FormTemplate | undefined,
  securityInfos: SecurityInfo[],
  securityInfoFilterByInterface: SecurityInfoFilterByInterface,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  Object.entries(clonedTemplate).forEach(([sectionName, templateSection]) => {
    if (typeof templateSection === 'string') {
      return;
    }

    cleanupPreviousSecurityInfoOption(templateSection, 'secinfoid');

    const securityInfoFilter = securityInfoFilterByInterface[sectionName] ?? [];
    const securityInfoOptions = getSecurityInfoOptions(securityInfos, securityInfoFilter);

    setOptionForTemplateSection(templateSection, securityInfoOptions, ['secinfoid']);
  });

  return clonedTemplate;
}

export function adaptNetworkIdField(
  templateSection: FormTemplateSection,
  interfaceFormData: Partial<AccessFunctionInterfaceFormData>,
): FormTemplateSection {

  let adaptedTemplate = templateSection;

  if (interfaceFormData.ownnetid != null) {
    adaptedTemplate = adaptNetworkIdFieldByFieldName(
      adaptedTemplate,
      interfaceFormData.ownnetid,
      'ownnetid',
    );
  }

  if (interfaceFormData.cdnetid != null) {
    adaptedTemplate = adaptNetworkIdFieldByFieldName(
      adaptedTemplate,
      interfaceFormData.cdnetid,
      'cdnetid',
    );
  }

  return adaptedTemplate;
}

export function adaptXDPIpAddress(
  templateSection: FormTemplateSection,
  interfaceFormData: Partial<AccessFunctionInterfaceFormData>,
): FormTemplateSection {
  const formTemplateField = templateSection.fields.ownIPAddr;

  if (formTemplateField == null || formTemplateField.type !== 'select') {
    return templateSection;
  }

  const fieldValue = interfaceFormData.ownIPAddr;
  const isOptionAvailable = formTemplateField.options?.some(({ value }) => value === fieldValue);
  if (fieldValue == null || fieldValue === '' || isOptionAvailable) {
    return templateSection;
  }

  const clonedTemplateSection = cloneDeep(templateSection);

  clonedTemplateSection.fields.ownIPAddr.options?.push({
    label: fieldValue,
    value: fieldValue,
  });

  return clonedTemplateSection;
}

export function adaptTemplateLabelsForDeployment(
  template: FormTemplate | undefined,
  deployment: Deployment,
): FormTemplate | undefined {
  if (template == null) {
    return template;
  }

  const clonedTemplate = cloneDeep(template);

  if (deployment === 'endeavor') {
    clonedTemplate.formId = Date.now().toString();
    setLabelForField(clonedTemplate, 'stateName', 'County/Region');
    setLabelForField(clonedTemplate, 'city', 'Town/City');
  }

  return clonedTemplate;
}

function setLabelForField(template: FormTemplate, fieldName: string, fieldLabel: string) {
  Object.keys(template).forEach((section: string) => {
    const templateSection = template[section];

    if (typeof templateSection === 'string') {
      return;
    }

    const { fields } = templateSection;
    if (fields == null || fields[fieldName] == null) {
      return;
    }

    const templateField = fields[fieldName];
    const originalLabel = templateField.label;
    templateField.label = fieldLabel;

    if (originalLabel != null && templateField.options != null) {
      const { options } = templateField;
      options[0].label = options[0].label.replace(new RegExp(originalLabel, 'i'), fieldLabel);
    }
  });
}

export function adaptTemplateInterfaceFields(
  template: FormTemplate | undefined,
  formData: AccessFunctionFormData,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  const provisioningInterfacesSubsectionName = getSubsectionName(formData, template, 'provisioningInterfaces');
  const dataInterfacesSubsectionName = getSubsectionName(formData, template, 'dataInterfaces');
  const contentInterfacesSubsectionName = getSubsectionName(formData, template, 'contentInterfaces');

  formData.provisioningInterfaces?.forEach((interfaceFormData) => {
    if (interfaceFormData.uri != null) {
      const templateSection = getInterfaceFormTemplateSection(clonedTemplate, 'provisioningInterfaces', provisioningInterfacesSubsectionName);
      const templateField = templateSection?.fields.uri;

      if (fieldAffectsOtherFieldsOnChange(templateField)) {
        adaptTemplateInterfaceForUri(
          templateField?.affectedFieldsOnChange,
          templateSection,
          interfaceFormData.uri,
        );
      }
    }
  });

  formData.dataInterfaces?.forEach((interfaceFormData) => {
    const templateSection = getInterfaceFormTemplateSection(clonedTemplate, 'dataInterfaces', dataInterfacesSubsectionName);
    if (interfaceFormData.uri != null) {
      const templateField = templateSection?.fields.tpkt;
      if (fieldAffectsOtherFieldsOnChange(templateField)) {
        adaptTemplateForTPKT(
          templateField?.affectedFieldsOnChange,
          templateSection,
          interfaceFormData.tpkt,
        );
        adaptTemplateForFtpType(
          templateField?.affectedFieldsOnChange,
          templateSection,
          interfaceFormData.ftpType,
        );
      }
    }

    if (interfaceFormData.deliveryVia != null) {
      const templateField = templateSection?.fields.deliveryVia;
      if (fieldAffectsOtherFieldsOnChange(templateField)) {
        adaptTemplateInterfaceForDeliveryVia(
          templateSection,
          interfaceFormData.deliveryVia,
        );
      }
    }
  });

  formData.contentInterfaces?.forEach((interfaceFormData) => {
    if (interfaceFormData.maxx2connsperaf != null) {
      const templateSection = getInterfaceFormTemplateSection(clonedTemplate, 'contentInterfaces', contentInterfacesSubsectionName);
      const templateField = templateSection?.fields.maxx2connsperaf;

      if (fieldAffectsOtherFieldsOnChange(templateField)) {
        adaptTemplateInterfaceForMaxx2Conn(
          templateField?.affectedFieldsOnChange,
          templateSection,
          interfaceFormData.maxx2connsperaf,
        );
      }
    }
  });

  return clonedTemplate;
}

export function getSubsectionName(
  data: Partial<AccessFunctionFormData>,
  template: FormTemplate,
  interfaceName: AccessFunctionInterfaceFormSectionType,
): string {
  let formSubsectionName = data.contentDeliveryVia ?? '';
  if (isHeaderSelectionPanel(template, interfaceName)) {
    if (interfaceName === 'dataInterfaces') {
      formSubsectionName = data.dataDeliveryVia ?? '';
    } else if (interfaceName === 'contentInterfaces') {
      formSubsectionName = data.contentDeliveryVia ?? '';
    } else {
      formSubsectionName = '';
    }
  }
  return formSubsectionName;
}

export function adaptContentDeliveryViaField(
  template: FormTemplate | undefined,
  contentDeliveryVia: string,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  clonedTemplate.formId = Date.now().toString();

  (clonedTemplate.general as FormTemplateSection).fields.contentDeliveryVia.initial = contentDeliveryVia;

  return clonedTemplate;
}

export function adaptTemplateForInterfacesFieldUpdate(
  interfaceName: AccessFunctionInterfaceFormSectionType,
  data: Partial<AccessFunctionFormData>,
  template: FormTemplate | undefined,
  fieldName: keyof AccessFunctionInterfaceFormData,
  fieldValue: unknown,
): FormTemplate | undefined {
  if (template == null) {
    return;
  }

  const clonedTemplate = cloneDeep(template);
  const formSubsectionName = getSubsectionName(data, template, interfaceName);

  const templateSection = getInterfaceFormTemplateSection(clonedTemplate, interfaceName, formSubsectionName);
  const templateField = templateSection?.fields[fieldName];

  if (fieldName === 'ccnodename') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();
    }
  }

  if (fieldName === 'uri') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();

      adaptTemplateInterfaceForUri(
        templateField?.affectedFieldsOnChange,
        templateSection,
        fieldValue as string,
      );
    }
  }

  if (fieldName === 'maxx2connsperaf') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();

      adaptTemplateInterfaceForMaxx2Conn(
        templateField?.affectedFieldsOnChange,
        templateSection,
        fieldValue as string,
      );
    }
  }

  if (fieldName === 'tpkt') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();

      adaptTemplateForTPKT(
        templateField?.affectedFieldsOnChange,
        templateSection,
        fieldValue as boolean,
      );
    }
  }

  if (fieldName === 'deliveryVia') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();

      adaptTemplateInterfaceForDeliveryVia(
        templateSection,
        fieldValue as string,
      );
    }
  }

  if (fieldName === 'ftpType') {
    if (fieldAffectsOtherFieldsOnChange(templateField)) {
      clonedTemplate.formId = Date.now().toString();

      adaptTemplateForFtpType(
        templateField?.affectedFieldsOnChange,
        templateSection,
        fieldValue as string,
      );
    }
  }

  return clonedTemplate;
}

export function getInterfaceFormTemplateSection(
  template: FormTemplate,
  interfaceName: AccessFunctionInterfaceFormSectionType,
  interfaceConfigureType: string,
): FormTemplateSection | undefined {
  const sectionName = getInterfaceFormTemplateSectionName(template, interfaceName, interfaceConfigureType);
  return template[sectionName] as FormTemplateSection;
}

export function getInterfaceFormTemplateSectionName(
  template: FormTemplate,
  interfaceName: AccessFunctionInterfaceFormSectionType,
  interfaceConfigureType: string,
): string {
  if (interfaceConfigureType === '') {
    return `${interfaceName}`;
  }

  if (template?.[`${interfaceName}_${interfaceConfigureType}`] == null) {
    return `${interfaceName}`;
  }

  return `${interfaceName}_${interfaceConfigureType}`;
}

export function shouldValidateInterfacesQuantity(
  template: FormTemplate,
  provisioningInterfacesSubsectionName: string,
  dataInterfacesSubsectionName: string,
  contentInterfacesSubsectionName: string,
): boolean {
  const provisioningInterfacesTemplateSection = getInterfaceFormTemplateSection(template, 'provisioningInterfaces', provisioningInterfacesSubsectionName);
  const dataInterfacesTemplateSection = getInterfaceFormTemplateSection(template, 'dataInterfaces', dataInterfacesSubsectionName);
  const contentInterfacesTemplateSection = getInterfaceFormTemplateSection(template, 'contentInterfaces', contentInterfacesSubsectionName);

  const shouldValidateEmptyInterfaces = [
    provisioningInterfacesTemplateSection,
    dataInterfacesTemplateSection,
    contentInterfacesTemplateSection,
  ]
    .filter((entry:FormTemplateSection | undefined) => entry != null)
    .map((entry:FormTemplateSection | undefined) => entry?.metadata.min === 0)
    .every((entry: boolean) => entry === true);

  return shouldValidateEmptyInterfaces;

}

function getTagOptions(
  templates: { [key: string]: FormTemplate },
  enabledAccessFunctions: SupportedFeaturesAccessFunctions,
): FormTemplateOption[] {
  return Object.keys(templates).map((key) => ({
    label: enabledAccessFunctions[key]?.title ?? templates[key].name,
    value: templates[key].key,
  }))
    .sort((a, b) => a.label.toLocaleLowerCase().localeCompare(b.label.toLocaleLowerCase()));
}

function setInitialValueFromProcessOptions(template: FormTemplate, processOptions: ProcessOption[]): void {
  const { processOptionProcName } = template;

  if (processOptionProcName == null || typeof processOptionProcName !== 'string') {
    return;
  }

  const filteredProcessOptions = processOptions.filter(({ procName }) => procName === processOptionProcName);

  Object.values(template).forEach((formTemplateSection) => {
    if (typeof formTemplateSection === 'string') {
      return;
    }

    const { fields } = formTemplateSection;
    if (fields == null) {
      return;
    }

    Object.values(fields).forEach((formTemplateField) => {
      const {
        processOptionParamName,
        processOptionLoadParamValueTo,
      } = formTemplateField;

      if (processOptionParamName == null) {
        return;
      }

      const processOption = filteredProcessOptions.find(({ paramName }) => paramName === processOptionParamName);
      if (processOption == null || processOption.paramValue == null) {
        return;
      }

      if (processOptionLoadParamValueTo != null) {
        formTemplateField[processOptionLoadParamValueTo] = processOption.paramValue;

      } else {
        formTemplateField.initial = processOption.paramValue;
      }
    });
  });
}

function setTagOptions(template: FormTemplate, options: FormTemplateOption[]): void {
  (template.general as FormTemplateSection).fields.tag?.options?.push(...options);
}

function getFieldOptions(list: string[]): FormTemplateOption[] {
  return list.map((entry) => ({
    label: entry,
    value: entry,
  }));
}

function getTimeZoneFieldOptions(timezones: ValueLabelPair[]): FormTemplateOption[] {
  const date = new Date();
  return getSortedTimeZoneValueLabels(date, timezones);
}

function setLocationFieldOptions(
  template: FormTemplate,
  options: FormTemplateOption[],
  field: 'country' | 'stateName' | 'city',
): void {
  const templateSection = (template.systemInformation as FormTemplateSection);

  const firstOption = templateSection.fields[field].options?.[0];
  if (firstOption) {
    templateSection.fields[field].options = [firstOption];
  }

  if (options.length > 0) {
    templateSection.fields[field].options?.push(...options);
    templateSection.fields[field].disabled = false;
  } else {
    templateSection.fields[field].disabled = true;
  }
}

function resetTimezoneOptions(template: FormTemplate): void {
  const { options } = (template.systemInformation as FormTemplateSection).fields.timeZone;

  if (options != null && options.length > 0) {
    (template.systemInformation as FormTemplateSection).fields.timeZone.options = [options[0]];
  }
}

function setTimezoneOptions(template: FormTemplate, options: FormTemplateOption[]): void {
  if (options.length > 0) {
    (template.systemInformation as FormTemplateSection).fields.timeZone.type = 'select';
    (template.systemInformation as FormTemplateSection).fields.timeZone.options?.push(...options);

    if (options.length === 1) {
      (template.systemInformation as FormTemplateSection).fields.timeZone.initial = options[0].value;
    }
  }
}

function getContactsOptions(contacts: Contact[]): FormTemplateOption[] {
  return contacts.map((contact) => ({
    label: contact.name,
    value: contact.id,
  }));
}

function setContactOptions(template: FormTemplate, options: FormTemplateOption[]): void {
  (template.systemInformation as FormTemplateSection).fields.contact.options?.push(...options);
}

function getNetworkOptionsMap(networks: Network[]): FormTemplateOptionMap {
  return {
    default: getNetworkOptions(networks, 'default'),
    networkIdAndIpAddress: getNetworkOptions(networks, 'networkIdAndIpAddress'),
    networkIdIPAddressAndCustomPort: getNetworkOptions(networks, 'networkIdIPAddressAndCustomPort'),
    networkIdIPAddressAndPortIncrement: getNetworkOptions(networks, 'networkIdIPAddressAndPortIncrement'),
  };
}

function getNetworkOptions(
  networks: Network[],
  type: 'networkIdAndIpAddress' | 'networkIdIPAddressAndCustomPort' | 'networkIdIPAddressAndPortIncrement' | 'default',
): FormTemplateOption[] {
  return networks.map((network) => {
    const name = network.networkId;

    let ip = network.ownIPAddress;
    if (ipRegex.v6({ exact: true }).test(ip)) {
      ip = `[${ip}]`;
    }

    const port = network.startPortNumber;

    let label = '';
    switch (type) {
      case 'networkIdAndIpAddress':
        label = `${name} - ${ip}`;
        break;

      case 'networkIdIPAddressAndCustomPort':
        label = `${name} - ${ip}:{{value}}`;
        break;

      case 'networkIdIPAddressAndPortIncrement':
        label = `${name} - ${ip}:<% print(+${port} + (+value)); %>`;
        break;

      default:
        label = `${name} - ${ip}:${port}`;
    }

    const value = network.id;

    return {
      label,
      value,
    };
  });
}

function getSecurityInfoOptions(
  securityInfos: SecurityInfo[],
  securityInfoFilter: SecurityInfoFilter[],
): FormTemplateOption[] {
  return securityInfos.filter((securityInfo) => {
    let result = true;
    securityInfoFilter.forEach(({ field, value }) => {
      if (securityInfo[field] === value) {
        result = result && true;
      } else {
        result = false;
      }
    });
    return result;
  }).map((securityInfo) => ({
    label: securityInfo.name,
    value: securityInfo.id,
  }));
}

function getSshSecurityInfoOptions(sshSecurityInfos: SshSecurityInfo[]): FormTemplateOption[] {
  return sshSecurityInfos.map(({ id }) => ({
    label: atob(id),
    value: id,
  }));
}

function getXtpIdOptions(xtpIdInfos: Xtp[]): FormTemplateOption[] {
  return xtpIdInfos.map((xtpIdInfo) => {
    const label = `${xtpIdInfo.lbId} - [${xtpIdInfo.ipAddr}]`;
    const value = xtpIdInfo.id;

    return {
      label,
      value,
    };
  });
}

function getXdpIdOptions(xdpIdInfos: Xdp[]): FormTemplateOption[] {
  return xdpIdInfos.map((xdpIdInfo) => {
    const label = `${xdpIdInfo.ipduId} - [${xdpIdInfo.ipAddr}]`;
    const value = xdpIdInfo.id;

    return {
      label,
      value,
    };
  });
}

function cleanupPreviousSecurityInfoOption(templateSection: FormTemplateSection, fieldName: string): void {
  if (templateSection.fields[fieldName] == null) {
    return;
  }

  const { options } = templateSection.fields[fieldName];

  if (options == null || options.length === 0) {
    return;
  }

  const firstOptionValue = options[0].value;
  if (firstOptionValue === '' || firstOptionValue === 'Tk9ORQ') {
    templateSection.fields[fieldName].options = [options[0]];
  } else {
    templateSection.fields[fieldName].options = [];
  }
}

function setOptionsForFields(
  template: FormTemplate,
  options: FormTemplateOption[] | FormTemplateOptionMap,
  fieldNames: string[],
): void {
  Object.keys(template).forEach((section: string) => {
    const templateSection = template[section];

    if (typeof templateSection === 'string') {
      return;
    }

    setOptionForTemplateSection(templateSection, options, fieldNames);
  });
}

function setOptionForTemplateSection(
  templateSection: FormTemplateSection,
  options: FormTemplateOption[] | FormTemplateOptionMap,
  fieldNames: string[],
): void {
  const { fields } = templateSection;

  if (!fields) {
    return;
  }

  fieldNames.forEach((fieldName) => {
    const templateField = fields[fieldName];
    if (templateField == null || templateField?.options == null) {
      return;
    }

    const properOptions = getProperOptions(templateField, options);

    setOptionFormTemplateField(templateField, properOptions);
  });
}

function getProperOptions(
  templateField: FormTemplateField,
  options: FormTemplateOption[] | FormTemplateOptionMap,
): FormTemplateOption[] {
  let properOption: FormTemplateOption[] = [];

  if (Array.isArray(options)) {
    properOption = options;

  } else {
    properOption = getProperOptionsFromOptionsMap(templateField, options);
  }

  return properOption;
}

function getProperOptionsFromOptionsMap(
  templateField: FormTemplateField,
  options: FormTemplateOptionMap,
): FormTemplateOption[] {
  const defaultOptions = options.default ?? [];

  const properOption = options[templateField.optionsFormat ?? 'default'] ?? defaultOptions;

  if (templateField.optionsFormatValue == null) {
    return properOption;
  }

  const clonedOptions = cloneDeep(properOption);

  clonedOptions.forEach((option) => {
    const compiledLabelTemplate = lodashTemplate(option.label, TEMPLATE_SETTINGS);
    option.label = compiledLabelTemplate({
      value: templateField.optionsFormatValue,
    });
  });

  return clonedOptions;
}

function setOptionFormTemplateField(
  templateField: FormTemplateField,
  options: FormTemplateOption[],
) {
  const firstOptions = templateField.options?.[0];
  if (firstOptions) {
    templateField.options = [firstOptions];
  }

  if (options.length > 0) {
    templateField.options?.push(...options);
    templateField.initial = templateField.options?.[0].value ?? '';

  } else {
    templateField.initial = '';
  }
}

function setFieldInitialValueForTemplateSection(
  templateSection: FormTemplateSection | undefined,
  initial: string,
  fieldNames: string[],
): void {
  if (!templateSection) {
    return;
  }

  const { fields } = templateSection;

  if (!fields || !initial) {
    return;
  }

  fieldNames.forEach((fieldName) => {
    const templateField = fields[fieldName];
    if (templateField == null) {
      return;
    }

    templateField.initial = initial;
  });
}

function setOptionForOwnIpAddr(template: FormTemplate, xdps: Xdp[]) {
  Object.keys(template).forEach((section: string) => {
    const templateSection = template[section];

    if (typeof templateSection === 'string') {
      return;
    }

    const { fields } = templateSection;
    const options = xdps.map((xdp) => {
      return {
        label: xdp.ipAddr,
        value: xdp.ipAddr,
      };
    });

    if (!fields) {
      return;
    }

    const templateField = fields.ownIPAddr;
    if (templateField == null || templateField?.options == null || templateField?.optionsBasedOn == null) {
      return;
    }
    const properOptions = getProperOptions(templateField, options);
    setOptionFormTemplateField(templateField, properOptions);
  });
}

function adaptTemplateForCopyMode(template: FormTemplate): FormTemplate {
  (template.general as FormTemplateSection).fields.tag.readOnly = true;

  return template;
}

function adaptTemplateForEditMode(template: FormTemplate): FormTemplate {
  attemptToMakeFieldReadOnlyFromSection(template, 'general', 'tag');
  attemptToMakeFieldReadOnlyFromSection(template, 'general', 'contentDeliveryVia');

  POSSIBLE_INTERFACES.forEach((interfaceName) => {
    attemptToShowFieldFromSection(template, interfaceName, 'state');
    attemptToMakeFieldReadOnlyFromSection(template, interfaceName, 'lbid');
  });

  if (template.key === 'ALU_DSS') {
    attemptToMakeFieldReadOnlyFromSection(template, 'systemInformation', 'model');
  }

  if (template.key === 'SONUS_SBC') {
    attemptToMakeFieldReadOnlyFromSection(template, 'provisioningInterfaces_directFromAF', 'secinfoid');
  }

  return template;
}

function adaptTemplateForViewMode(template: FormTemplate): FormTemplate {
  setAllFieldsAsReadOnly(template);

  POSSIBLE_INTERFACES.forEach((interfaceName) => {
    attemptToShowFieldFromSection(template, interfaceName, 'state');
  });

  return template;
}

function setAllFieldsAsReadOnly(template: FormTemplate): void {
  Object.entries(template).forEach(([_templateKey, templateEntry]) => {
    if (typeof templateEntry === 'string') {
      return;
    }

    Object.entries(templateEntry.fields).forEach(([_fieldKey, fieldEntry]) => {
      fieldEntry.readOnly = true;
    });
  });
}

function attemptToShowFieldFromSection(template: FormTemplate, templateSection: string, fieldName: string) {
  const formSection = (template[templateSection] as FormTemplateSection);
  if (formSection == null) {
    return;
  }

  const field = formSection.fields[fieldName];
  if (field == null) {
    return;
  }

  field.hide = 'false';
}

function attemptToMakeFieldReadOnlyFromSection(template: FormTemplate, templateSection: string, fieldName: string) {
  const formSection = (template[templateSection] as FormTemplateSection);
  if (formSection == null) {
    return;
  }

  const field = formSection.fields[fieldName];
  if (field == null) {
    return;
  }

  field.readOnly = true;
}

function getSoftwareVersionOptions(accessFunction: SupportedFeaturesAccessFunction): FormTemplateOption[] {
  return accessFunction.versions.map((version) => ({
    label: version,
    value: version,
  }));
}

function getSoftwareModel(accessFunction: SupportedFeaturesAccessFunction): string {
  return accessFunction.models?.[0] ?? '';
}

function getHTTPVersions(accessFunction: SupportedFeaturesAccessFunction): FormTemplateOption[] {
  return accessFunction.httpVersions?.map((version) => ({
    label: version,
    value: version,
  })) ?? [];
}

function getHTTPDefaultVersion(accessFunction: SupportedFeaturesAccessFunction): string {
  return accessFunction.httpDefaultVersion ?? '';
}

function adaptNetworkIdFieldByFieldName(
  templateSection: FormTemplateSection,
  netId: string,
  fieldName: 'ownnetid' | 'cdnetid',
): FormTemplateSection {
  const options = templateSection.fields[fieldName]?.options;
  if (options == null) {
    return templateSection;
  }

  const netIdOption = options.find(({ value }) => value === netId);
  if (netIdOption != null) {
    return templateSection;
  }

  const clonedTemplateSection = cloneDeep(templateSection);

  clonedTemplateSection.fields[fieldName].options?.push({
    label: 'Network ID not found',
    value: netId,
  });

  return clonedTemplateSection;
}

function adaptTemplateInterfaceForUri(
  affectedFieldsOnChange: string[] | undefined,
  templateSection: FormTemplateSection | undefined,
  fieldValue: string,
): void {
  if (templateSection == null || affectedFieldsOnChange == null) {
    return;
  }

  if (!affectedFieldsOnChange.includes('secinfoid')) {
    return;
  }

  const {
    secinfoid,
  } = templateSection.fields;

  if (secinfoid == null) {
    return;
  }

  if (fieldValue.toLocaleLowerCase().startsWith('http:')) {
    secinfoid.validation = {
      ...secinfoid.validation,
      required: false,
    };

    if (secinfoid.options != null) {
      secinfoid.initial = 'Tk9ORQ';
      secinfoid.options[0] = {
        label: 'None',
        value: 'Tk9ORQ',
      };
    }

  } else {
    secinfoid.validation = {
      ...secinfoid.validation,
      required: true,
    };

    if (secinfoid.options != null) {
      secinfoid.initial = '';
      secinfoid.options[0] = {
        label: 'None',
        value: '',
      };
    }
  }
}

function adaptTemplateInterfaceForMaxx2Conn(
  affectedFieldsOnChange: string[] | undefined,
  templateSection: FormTemplateSection | undefined,
  fieldValue: string,
): void {
  if (templateSection == null || affectedFieldsOnChange == null) {
    return;
  }

  if (!affectedFieldsOnChange.includes('destPort')) {
    return;
  }

  const {
    destPort,
  } = templateSection.fields;

  if (destPort == null) {
    return;
  }

  if (+fieldValue > 4) {
    destPort.disabled = true;
  } else {
    destPort.disabled = false;
  }
}

function adaptTemplateForTPKT(
  affectedFieldsOnChange: string[] | undefined,
  templateSection: FormTemplateSection | undefined,
  fieldValue: boolean | undefined,
): void {
  if (templateSection == null || affectedFieldsOnChange == null) {
    return;
  }

  if (!affectedFieldsOnChange.includes('x224headerpresent')) {
    return;
  }

  const {
    x224headerpresent,
  } = templateSection.fields;

  if (x224headerpresent == null) {
    return;
  }

  if (fieldValue ?? true) {
    x224headerpresent.readOnly = false;
  } else {
    x224headerpresent.readOnly = true;
  }
}
function adaptTemplateInterfaceForDeliveryVia(
  templateSection: FormTemplateSection | undefined,
  fieldValue: string,
): void {

  if (templateSection == null) {
    return;
  }

  const {
    deliveryVia,
    sharedsecret,
    version,
    dscp,
    secinfoid,
  } = templateSection.fields;

  if (deliveryVia == null) {
    return;
  }

  if (fieldValue === 'VoIP') {
    sharedsecret.hide = 'true';
    version.hide = 'true';
    dscp.hide = 'false';
    secinfoid.hide = 'false';
  } else if (fieldValue === 'PacketCable') {
    sharedsecret.hide = 'false';
    version.hide = 'false';
    dscp.hide = 'true';
    secinfoid.hide = 'true';
  }
}

function adaptTemplateForFtpType(
  affectedFieldsOnChange: string[] | undefined,
  templateSection: FormTemplateSection | undefined,
  fieldValue: string | undefined,
): void {
  if (templateSection == null || affectedFieldsOnChange == null) {
    return;
  }

  if (!affectedFieldsOnChange.includes('secinfoid')) {
    return;
  }

  const {
    secinfoid,
  } = templateSection.fields;

  if (secinfoid == null) {
    return;
  }

  if (fieldValue === 'FTP') {
    secinfoid.validation = {
      ...secinfoid.validation,
      required: false,
    };

    if (secinfoid.options != null) {
      secinfoid.initial = 'Tk9ORQ';
      secinfoid.options[0] = {
        label: 'None',
        value: 'Tk9ORQ',
      };
    }

  } else {
    secinfoid.validation = {
      ...secinfoid.validation,
      required: true,
    };

    if (secinfoid.options != null) {
      secinfoid.initial = '';
      secinfoid.options[0] = {
        label: 'None',
        value: '',
      };
    }
  }
}

function getXcipioNodesByType(
  xcipioNodes: NodeData[],
  type: string,
): FormTemplateOption[] {
  return xcipioNodes?.filter(({ nodeType }) => nodeType === type).map((field) => ({
    label: field.nodeName,
    value: field.nodeName,
  }));
}
