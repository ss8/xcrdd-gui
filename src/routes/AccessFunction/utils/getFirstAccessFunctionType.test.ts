import { FormTemplateMapByKey } from 'data/template/template.types';
import { mockContacts } from '__test__/mockContacts';
import { mockNetworks } from '__test__/mockNetworks';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockXdps } from '__test__/mockXdps';
import { mockXtps } from '__test__/mockXtps';
import { mockNodes } from '__test__/mockNodes';
import { adaptTemplateOptions } from './accessFunctionTemplate.adapter';
import { getFirstAccessFunctionType } from './getFirstAccessFunctionType';

describe('getFirstAccessFunctionType()', () => {
  let accessFunctionTemplates: FormTemplateMapByKey;
  let templates: FormTemplateMapByKey;

  beforeEach(() => {
    accessFunctionTemplates = {
      ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      ALU_DSS: getMockTemplateByKey('ALU_DSS'),
      SONUS_SBC: getMockTemplateByKey('SONUS_SBC'),
    };

    templates = adaptTemplateOptions(
      accessFunctionTemplates,
      [],
      mockTimeZones,
      mockContacts,
      mockNetworks,
      mockSecurityInfos,
      mockSshSecurityInfos,
      mockXtps,
      mockXdps,
      mockProcessOptions,
      mockSupportedFeatures.config.accessFunctions,
      mockNodes,
    );
  });

  it('should return empty string if no template available', () => {
    expect(getFirstAccessFunctionType({})).toEqual('');
  });

  it('should return the first access function type', () => {
    expect(getFirstAccessFunctionType(templates)).toEqual('ALU_DSS');
  });
});
