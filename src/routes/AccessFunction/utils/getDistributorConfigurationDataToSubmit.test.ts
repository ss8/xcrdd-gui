import { DistributorConfigurationTableData, DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import getDistributorConfigurationDataToSubmit from './getDistributorConfigurationDataToSubmit';

describe('getDistributorConfigurationDataToSubmit()', () => {
  let distributorConfigurationsTableData: DistributorConfigurationTableData[];
  let originalDistributorConfigurations: DistributorConfiguration[];
  let expectedDistributorConfigurationUPF: DistributorConfiguration;
  let expectedDistributorConfigurationUPF2: DistributorConfiguration;
  let expectedDistributorConfigurationSMF: DistributorConfiguration;
  const userId = 'userId1';
  const userName = 'userName1';

  beforeEach(() => {
    distributorConfigurationsTableData = [{
      id: '0.id1',
      name: 'name1',
      afType: 'ERK_UPF',
      instanceId: '1',
      moduleName: 'ERKUPFX3IM',
      afProtocol: 'ERKUPF_X3',
      ipAddress: '12.12.12.12',
      port: '1234',
      listener: {
        id: 'listener1',
        afProtocol: 'ERKUPF_X3',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '10',
        maxConcurrentConnection: '10',
        port: '123',
        requiredState: 'ACTIVE',
        secInfoId: '123',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [{
          afAddress: '12.12.12.12',
          afId: 'afId1',
          afName: 'afName1',
        }, {
          afAddress: '12.12.12.12',
          afId: 'afId2',
          afName: 'afName2',
        }, {
          afAddress: '13.13.13.13',
          afId: 'afId3',
          afName: 'afName3',
        }],
      },
      destinations: [],
    }];

    originalDistributorConfigurations = [{
      id: 'id1',
      name: 'name1',
      afType: 'ERK_UPF',
      instanceId: '1',
      moduleName: 'ERKUPFX3IM',
      listeners: [{
        id: 'listeners1',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '10',
        maxConcurrentConnection: '10',
        port: '123',
        requiredState: 'ACTIVE',
        secInfoId: '123',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afProtocol: 'ERKUPF_X3',
        afWhitelist: [{
          afAddress: '11.11.11.11',
          afId: 'afId1',
          afName: 'afName1',
        }, {
          afAddress: '12.12.12.12',
          afId: 'afId2',
          afName: 'afName2',
        }, {
          afAddress: '14.14.14.14',
          afId: 'afId4',
          afName: 'afName4',
        }],
      }],
      destinations: [],
    }];

    expectedDistributorConfigurationUPF = {
      id: 'id1',
      name: 'name1',
      afType: 'ERK_UPF',
      instanceId: '1',
      moduleName: 'ERKUPFX3IM',
      listeners: [{
        id: 'listener1',
        ipAddress: '12.12.12.12',
        keepaliveTimeout: '10',
        maxConcurrentConnection: '10',
        port: '123',
        requiredState: 'ACTIVE',
        secInfoId: '123',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afProtocol: 'ERKUPF_X3',
        operation: 'UPDATE',
        afWhitelist: [{
          afAddress: '12.12.12.12',
          afId: 'afId1',
          afName: 'afName1',
          operation: 'UPDATE',
        }, {
          afAddress: '12.12.12.12',
          afId: 'afId2',
          afName: 'afName2',
          operation: 'NO_OPERATION',
        }, {
          afAddress: '13.13.13.13',
          afId: 'afId3',
          afName: 'afName3',
          operation: 'CREATE',
        }, {
          afAddress: '14.14.14.14',
          afId: 'afId4',
          afName: 'afName4',
          operation: 'DELETE',
        }],
      }],
      destinations: [],
    };
  });

  describe('when operation is UPDATE', () => {
    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getDistributorConfigurationDataToSubmit(
          distributorConfigurationsTableData,
          originalDistributorConfigurations,
          false,
          userId,
          userName,
        ),
      ).toEqual([{
        data: expectedDistributorConfigurationUPF,
      }]);
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getDistributorConfigurationDataToSubmit(
          distributorConfigurationsTableData,
          originalDistributorConfigurations,
          true,
          userId,
          userName,
        ),
      ).toEqual([{
        data: expectedDistributorConfigurationUPF,
        auditDetails: {
          actionType: 'Distributor Configuration Edit',
          auditEnabled: true,
          fieldDetails: '[{"field":"listeners","before":[{"id":"listeners1","ipAddress":"12.12.12.12","keepaliveTimeout":"10","maxConcurrentConnection":"10","port":"123","requiredState":"ACTIVE","secInfoId":"123","numberOfParsers":"10","synchronizationHeader":"NONE","transport":"TCP","afProtocol":"ERKUPF_X3","afWhitelist":[{"afAddress":"11.11.11.11","afId":"afId1","afName":"afName1"},{"afAddress":"12.12.12.12","afId":"afId2","afName":"afName2"},{"afAddress":"14.14.14.14","afId":"afId4","afName":"afName4"}]}],"after":[{"id":"listener1","afProtocol":"ERKUPF_X3","ipAddress":"12.12.12.12","keepaliveTimeout":"10","maxConcurrentConnection":"10","port":"123","requiredState":"ACTIVE","secInfoId":"123","numberOfParsers":"10","synchronizationHeader":"NONE","transport":"TCP","afWhitelist":[{"afAddress":"12.12.12.12","afId":"afId1","afName":"afName1"},{"afAddress":"12.12.12.12","afId":"afId2","afName":"afName2"},{"afAddress":"13.13.13.13","afId":"afId3","afName":"afName3"}]}]}]',
          recordName: 'name1',
          service: 'AFs',
          userId: 'userId1',
          userName: 'userName1',
        },
      }]);
    });
  });

  describe('when operation is UPDATE for more than one distributor type', () => {
    beforeEach(() => {
      distributorConfigurationsTableData = [
        ...distributorConfigurationsTableData,
        {
          id: '0.id2',
          name: 'name2',
          afType: 'ERK_SMF',
          instanceId: '1',
          moduleName: 'ERKSMFX2IM',
          afProtocol: 'ERKSMF_X2',
          ipAddress: '12.12.12.12',
          port: '1234',
          listener: {
            id: 'listener1',
            afProtocol: 'ERKSMF_X2',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afWhitelist: [{
              afAddress: '22.22.22.22',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '23.23.23.23',
              afId: 'afId3',
              afName: 'afName3',
            }],
          },
          destinations: [],
        },
      ];

      originalDistributorConfigurations = [
        ...originalDistributorConfigurations,
        {
          id: 'id2',
          name: 'name2',
          afType: 'ERK_SMF',
          instanceId: '1',
          moduleName: 'ERKSMFX2IM',
          listeners: [{
            id: 'listeners1',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afProtocol: 'ERKSMF_X2',
            afWhitelist: [{
              afAddress: '21.21.21.21',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '24.24.24.24',
              afId: 'afId4',
              afName: 'afName4',
            }],
          }],
          destinations: [],
        },
      ];

      expectedDistributorConfigurationSMF = {
        id: 'id2',
        name: 'name2',
        afType: 'ERK_SMF',
        instanceId: '1',
        moduleName: 'ERKSMFX2IM',
        listeners: [{
          id: 'listener1',
          ipAddress: '12.12.12.12',
          keepaliveTimeout: '10',
          maxConcurrentConnection: '10',
          port: '123',
          requiredState: 'ACTIVE',
          secInfoId: '123',
          numberOfParsers: '10',
          synchronizationHeader: 'NONE',
          transport: 'TCP',
          afProtocol: 'ERKSMF_X2',
          operation: 'UPDATE',
          afWhitelist: [{
            afAddress: '22.22.22.22',
            afId: 'afId1',
            afName: 'afName1',
            operation: 'UPDATE',
          }, {
            afAddress: '22.22.22.22',
            afId: 'afId2',
            afName: 'afName2',
            operation: 'NO_OPERATION',
          }, {
            afAddress: '23.23.23.23',
            afId: 'afId3',
            afName: 'afName3',
            operation: 'CREATE',
          }, {
            afAddress: '24.24.24.24',
            afId: 'afId4',
            afName: 'afName4',
            operation: 'DELETE',
          }],
        }],
        destinations: [],
      };
    });

    it('should return the data to submit with all distributor types', () => {
      expect(
        getDistributorConfigurationDataToSubmit(
          distributorConfigurationsTableData,
          originalDistributorConfigurations,
          false,
          userId,
          userName,
        ),
      ).toEqual([{
        data: expectedDistributorConfigurationUPF,
      }, {
        data: expectedDistributorConfigurationSMF,
      }]);
    });
  });

  describe('when operation is UPDATE for the distributor type but different module name and af protocol', () => {
    beforeEach(() => {
      distributorConfigurationsTableData = [
        ...distributorConfigurationsTableData,
        {
          id: '0.id2',
          name: 'name2',
          afType: 'ERK_UPF',
          instanceId: '1',
          moduleName: '5GXCF3',
          afProtocol: 'ERKUPF_XXXXXXXX',
          ipAddress: '12.12.12.12',
          port: '1234',
          listener: {
            id: 'listener1',
            afProtocol: 'ERKUPF_XXXXXXXX',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afWhitelist: [{
              afAddress: '22.22.22.22',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '23.23.23.23',
              afId: 'afId3',
              afName: 'afName3',
            }],
          },
          destinations: [],
        },
      ];

      originalDistributorConfigurations = [
        ...originalDistributorConfigurations,
        {
          id: 'id2',
          name: 'name2',
          afType: 'ERK_UPF',
          instanceId: '1',
          moduleName: '5GXCF3',
          listeners: [{
            id: 'listeners1',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afProtocol: 'ERKUPF_XXXXXXXX',
            afWhitelist: [{
              afAddress: '21.21.21.21',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '24.24.24.24',
              afId: 'afId4',
              afName: 'afName4',
            }],
          }],
          destinations: [],
        },
      ];

      expectedDistributorConfigurationUPF2 = {
        id: 'id2',
        name: 'name2',
        afType: 'ERK_UPF',
        instanceId: '1',
        moduleName: '5GXCF3',
        listeners: [{
          id: 'listener1',
          ipAddress: '12.12.12.12',
          keepaliveTimeout: '10',
          maxConcurrentConnection: '10',
          port: '123',
          requiredState: 'ACTIVE',
          secInfoId: '123',
          numberOfParsers: '10',
          synchronizationHeader: 'NONE',
          transport: 'TCP',
          afProtocol: 'ERKUPF_XXXXXXXX',
          operation: 'UPDATE',
          afWhitelist: [{
            afAddress: '22.22.22.22',
            afId: 'afId1',
            afName: 'afName1',
            operation: 'UPDATE',
          }, {
            afAddress: '22.22.22.22',
            afId: 'afId2',
            afName: 'afName2',
            operation: 'NO_OPERATION',
          }, {
            afAddress: '23.23.23.23',
            afId: 'afId3',
            afName: 'afName3',
            operation: 'CREATE',
          }, {
            afAddress: '24.24.24.24',
            afId: 'afId4',
            afName: 'afName4',
            operation: 'DELETE',
          }],
        }],
        destinations: [],
      };
    });

    it('should return the data to submit with all distributor with same type and different protocol', () => {
      expect(
        getDistributorConfigurationDataToSubmit(
          distributorConfigurationsTableData,
          originalDistributorConfigurations,
          false,
          userId,
          userName,
        ),
      ).toEqual([{
        data: expectedDistributorConfigurationUPF,
      }, {
        data: expectedDistributorConfigurationUPF2,
      }]);
    });
  });

  describe('when operation is UPDATE for the distributor type, same protocol but different module name', () => {
    beforeEach(() => {
      distributorConfigurationsTableData = [
        ...distributorConfigurationsTableData,
        {
          id: '0.id2',
          name: 'name2',
          afType: 'ERK_UPF',
          instanceId: '1',
          moduleName: '5GXCF3',
          afProtocol: 'ERKUPF_X3',
          ipAddress: '12.12.12.12',
          port: '1234',
          listener: {
            id: 'listener1',
            afProtocol: 'ERKUPF_X3',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afWhitelist: [{
              afAddress: '22.22.22.22',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '23.23.23.23',
              afId: 'afId3',
              afName: 'afName3',
            }],
          },
          destinations: [],
        },
      ];

      originalDistributorConfigurations = [
        ...originalDistributorConfigurations,
        {
          id: 'id2',
          name: 'name2',
          afType: 'ERK_UPF',
          instanceId: '1',
          moduleName: '5GXCF3',
          listeners: [{
            id: 'listeners1',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '10',
            maxConcurrentConnection: '10',
            port: '123',
            requiredState: 'ACTIVE',
            secInfoId: '123',
            numberOfParsers: '10',
            synchronizationHeader: 'NONE',
            transport: 'TCP',
            afProtocol: 'ERKUPF_X3',
            afWhitelist: [{
              afAddress: '21.21.21.21',
              afId: 'afId1',
              afName: 'afName1',
            }, {
              afAddress: '22.22.22.22',
              afId: 'afId2',
              afName: 'afName2',
            }, {
              afAddress: '24.24.24.24',
              afId: 'afId4',
              afName: 'afName4',
            }],
          }],
          destinations: [],
        },
      ];
    });

    it('should return the data to submit with all distributor with same type and different module name', () => {
      expect(
        getDistributorConfigurationDataToSubmit(
          distributorConfigurationsTableData,
          originalDistributorConfigurations,
          false,
          userId,
          userName,
        ),
      ).toEqual([{
        data: expectedDistributorConfigurationUPF,
      }]);
    });
  });
});
