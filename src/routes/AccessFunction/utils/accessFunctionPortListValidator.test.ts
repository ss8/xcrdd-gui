import { accessFunctionPortListValidator } from './accessFunctionPortListValidator';

describe('accessFunctionPortListValidator()', () => {

  it('should return true if undefined', () => {
    expect(accessFunctionPortListValidator(undefined)).toEqual(true);
  });

  it('should return true if empty', () => {
    expect(accessFunctionPortListValidator('')).toEqual(true);
  });

  it('should return true if port value is valid', () => {
    expect(accessFunctionPortListValidator('0')).toEqual(true);
    expect(accessFunctionPortListValidator('1')).toEqual(true);
    expect(accessFunctionPortListValidator('2000')).toEqual(true);
    expect(accessFunctionPortListValidator('65535')).toEqual(true);
  });

  it('should return false if port value is invalid', () => {
    expect(accessFunctionPortListValidator('-1')).toEqual(false);
    expect(accessFunctionPortListValidator('65536')).toEqual(false);
    expect(accessFunctionPortListValidator('a')).toEqual(false);
    expect(accessFunctionPortListValidator('123a')).toEqual(false);
    expect(accessFunctionPortListValidator('a123')).toEqual(false);
    expect(accessFunctionPortListValidator('123 a')).toEqual(false);
  });

  it('should return true if the list of ports are valid', () => {
    expect(accessFunctionPortListValidator(' 1000 , 1, 65535,22 ', 4)).toEqual(true);
  });

  it('should return false if the list of ports contain one invalid port', () => {
    expect(accessFunctionPortListValidator('1000,1,65536,22', 4)).toEqual(false);
    expect(accessFunctionPortListValidator('a,1', 2)).toEqual(false);
    expect(accessFunctionPortListValidator('1,-1', 2)).toEqual(false);
  });

  it('should return false if 0 is in the list', () => {
    expect(accessFunctionPortListValidator('1,0,3', 3)).toEqual(false);
  });

  it('should return false if there are more ports than specified', () => {
    expect(accessFunctionPortListValidator('1,2,3', 2)).toEqual(false);
  });

  it('should return false if there are less ports than specified - expect for 0', () => {
    expect(accessFunctionPortListValidator('2,3', 3)).toEqual(false);
  });

  it('should return true if more ports are expected by only 0 passed', () => {
    expect(accessFunctionPortListValidator('0', 3)).toEqual(true);
  });

  it('should return false if list of ports has empty port value', () => {
    expect(accessFunctionPortListValidator('1,2,', 3)).toEqual(false);
  });

  it('should return false if a port is repeated', () => {
    expect(accessFunctionPortListValidator('1,2,1', 3)).toEqual(false);
  });

  it('should return false if list of ports is greater than 4', () => {
    expect(accessFunctionPortListValidator('1,2,3,4,5', 5)).toEqual(false);
  });

  it('should return true if port is 0 and number of port is greater than 4', () => {
    expect(accessFunctionPortListValidator('0', 5)).toEqual(true);
  });

  it('should return false if port is not 0 and number of port is greater than 4', () => {
    expect(accessFunctionPortListValidator('2', 5)).toEqual(false);
  });
});
