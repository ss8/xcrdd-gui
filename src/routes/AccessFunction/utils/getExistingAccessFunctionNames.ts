import { AccessFunction } from 'data/accessFunction/accessFunction.types';

export default function getExistingAccessFunctionNames(
  accessFunctionByType: {[key: string]: AccessFunction[]},
  currentAccessFunctionId?: string,
): string[] {
  return Object.keys(accessFunctionByType)
    .map((type) => accessFunctionByType[type])
    .reduce((previousValue, currentValue) => [...previousValue, ...currentValue], [])
    .filter((accessFunction) => accessFunction.id !== currentAccessFunctionId)
    .map((accessFunction) => accessFunction.name);
}
