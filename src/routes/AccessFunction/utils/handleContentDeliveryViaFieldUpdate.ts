import cloneDeep from 'lodash.clonedeep';
import {
  AccessFunctionFormData,
  ContentDeliveryVia,
  DataDeliveryVia,
  AccessFunctionInterfaceFormSectionType,
  POSSIBLE_CONTENT_DELIVERY_VIA,
  POSSIBLE_DATA_DELIVERY_VIA,
} from 'data/accessFunction/accessFunction.types';
import { FormTemplate } from 'data/template/template.types';
import { createAccessFunctionBasedOnTemplate } from './accessFunctionTemplate.factory';

const POSSIBLE_INTERFACES: AccessFunctionInterfaceFormSectionType[] = [
  'provisioningInterfaces',
  'dataInterfaces',
  'contentInterfaces',
];

/**
 * @returns the updated AccessFunctionFormData with the relevent interface section changed based
 * on the contentDeliveryVia field change at the general section
 */
export function handleContentDeliveryViaFieldUpdate(
  template: FormTemplate | undefined,
  data: Partial<AccessFunctionFormData>,
  contentDeliveryVia: ContentDeliveryVia,
): Partial<AccessFunctionFormData> {
  if (template == null || data.contentDeliveryVia === contentDeliveryVia) {
    return data;
  }

  const clonedData = cloneDeep(data);

  const affectedInterfaces = getAffectedInterfacesByContentDeliveryViaChange(template, contentDeliveryVia) ?? [];

  affectedInterfaces?.forEach((interfaceName) => {
    if (interfaceName === 'provisioningInterfaces' || interfaceName === 'contentInterfaces' || interfaceName === 'dataInterfaces') {
      let forceInterfaceCreation = false;

      const interfaceData = clonedData[interfaceName];
      if (interfaceData != null && interfaceData.length > 0) {
        forceInterfaceCreation = true;
      }

      const initialData = createAccessFunctionBasedOnTemplate(template, forceInterfaceCreation);
      clonedData[interfaceName] = initialData[interfaceName];
    }
  });

  return clonedData;
}

function getAffectedInterfacesByContentDeliveryViaChange(
  template: FormTemplate | undefined,
  contentDeliveryVia: ContentDeliveryVia,
): AccessFunctionInterfaceFormSectionType[] | undefined {
  if (template == null) {
    return;
  }

  const affectedInterfaces = POSSIBLE_INTERFACES.filter((interfaceName) => {
    return template[`${interfaceName}_${contentDeliveryVia}`] != null;
  });

  return affectedInterfaces;
}

/**
 * @returns the updated contentDeliveryVia and dataDeliveryVia when the 'Deliver via' changes at
 * the dataInterfaces or contentInterfaces section
 */
export function handleInterfacesSectionDeliveryViaFieldUpdate(
  template: FormTemplate | undefined,
  interfaceName: AccessFunctionInterfaceFormSectionType,
  clonedData: Partial<AccessFunctionFormData>,
  fieldName: string,
  fieldValue: unknown,
): Partial<AccessFunctionFormData> {

  // This function is only for fields, contentDeliveryVia and dataDeliveryVia
  if (template == null || (fieldName !== 'contentDeliveryVia' && fieldName !== 'dataDeliveryVia')) {
    return clonedData;
  }

  // no change to contentDeliveryVia field value
  if (fieldName === 'contentDeliveryVia' && clonedData.contentDeliveryVia === fieldValue) {
    return clonedData;
  }
  if (fieldName === 'dataDeliveryVia' && clonedData.dataDeliveryVia === fieldValue) {
    return clonedData;
  }

  // unrecognized contentDeliveryVia value
  if (fieldName === 'contentDeliveryVia' && POSSIBLE_CONTENT_DELIVERY_VIA.includes(fieldValue as string) === false) {
    return clonedData;
  }
  if (fieldName === 'dataDeliveryVia' && POSSIBLE_DATA_DELIVERY_VIA.includes(fieldValue as string) === false) {
    return clonedData;
  }

  if (fieldName === 'contentDeliveryVia') {
    clonedData.contentDeliveryVia = fieldValue as ContentDeliveryVia;
  } else if (fieldName === 'dataDeliveryVia') {
    clonedData.dataDeliveryVia = fieldValue as DataDeliveryVia;
  }

  return clonedData;
}
