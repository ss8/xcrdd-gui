import {
  ColDef, Column, RowNode, ValueGetterParams,
} from '@ag-grid-enterprise/all-modules';
import { AccessFunction, AccessFunctionInterface } from 'data/accessFunction/accessFunction.types';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';
import getFieldOptionFromFirstInterfaceNameValueGetter from 'utils/valueGetters/getFieldOptionFromFirstInterfaceNameValueGetter';
import getNumberOfItemsFromFieldNameValueGetter from 'utils/valueGetters/getNumberOfItemsFromFieldNameValueGetter';
import {
  distributorConfigurationGroupFilterValueGetter,
  allowedPoiIpAddressesFilterValueGetter,
  afWhiteListToGridLabel,
  interfacesAfIdValueGetter,
} from './accessFunctionValueGetters';

describe('accessFunctionValueGetter', () => {
  let accessFunction: AccessFunction;
  let params: Partial<ValueGetterParams>;

  beforeEach(() => {
    accessFunction = {
      id: 'id1',
      tag: 'tag1',
      dxOwner: 'dxOwner1',
      dxAccess: 'dxAccess1',
      name: 'name1',
      contact: { id: 'Y29udGFjdDE=', name: 'contact1' },
      insideNat: false,
      liInterface: 'INI1',
      model: 'model1',
      preprovisioningLead: 'preprovisioningLead1',
      serialNumber: 'serialNumber1',
      timeZone: 'timeZone1',
      type: 'type1',
      version: 'version1',
      latitude: 'latitude1',
      longitude: 'longitude1',
      status: 'ACTIVE',
      provisioningInterfaces: [{
        id: 'provisioningInterfacesId1',
        name: 'provisioningInterfacesName1',
        options: [
          {
            key: 'destIPAddr',
            value: '12.12.12.12',
          },
          {
            key: 'destPort',
            value: 'destPort1',
          },
          {
            key: 'dscp',
            value: 'dscp1',
          },
          {
            key: 'ownnetid',
            value: 'ownnetid1',
          },
          {
            key: 'reqState',
            value: 'reqState1',
          },
          {
            key: 'secinfoid',
            value: 'secinfoid1',
          },
        ],
      }],
      dataInterfaces: [],
      contentInterfaces: [{
        id: 'contentInterfacesId1',
        name: 'contentInterfacesName1',
        options: [],
      }],
    };

    params = {
      colDef: {
        field: 'destIPAddr',
      },
      data: accessFunction,
    };
  });

  describe('getFieldOptionFromFirstInterfaceNameValueGetter()', () => {
    it('should return a value getter function', () => {
      expect(getFieldOptionFromFirstInterfaceNameValueGetter<AccessFunction, AccessFunctionInterface>('provisioningInterfaces')).toEqual(expect.any(Function));
    });

    it('should return "-" in the value getter if no interface defined', () => {
      const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<AccessFunction, AccessFunctionInterface>('dataInterfaces');
      expect(valueGetter(params)).toEqual('-');
    });

    it('should return "-" in the value getter if field not defined in the interface', () => {
      const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<AccessFunction, AccessFunctionInterface>('contentInterfaces');
      expect(valueGetter(params)).toEqual('-');
    });

    it('should return the field value in the value getter', () => {
      const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<AccessFunction, AccessFunctionInterface>('provisioningInterfaces');
      expect(valueGetter(params)).toEqual('12.12.12.12');
    });
  });

  describe('get Number Of Interfaces', () => {
    it('should return a value getter function', () => {
      expect(getNumberOfItemsFromFieldNameValueGetter<AccessFunction, AccessFunctionInterface>('provisioningInterfaces')).toEqual(expect.any(Function));
    });

    it('should return the length of interfaces as 0 if no interfaces', () => {
      const valueGetter = getNumberOfItemsFromFieldNameValueGetter<AccessFunction, AccessFunctionInterface>('dataInterfaces');
      expect(valueGetter(params)).toEqual('0');
    });

    it('should return the length of interfaces when list is not empty', () => {
      const valueGetter = getNumberOfItemsFromFieldNameValueGetter<AccessFunction, AccessFunctionInterface>('provisioningInterfaces');
      expect(valueGetter(params)).toEqual('1');
    });
  });

  describe('distributorConfigurationGroupFilterValueGetter()', () => {
    it('should return empty string if showRowGroup is undefined', () => {
      const distributorColParams = {
        api: undefined,
        data: undefined,
        colDef: {} as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,

      };

      expect(distributorConfigurationGroupFilterValueGetter(distributorColParams)).toBe('');
    });

    it('should return empty string if showRowGroup type is not string', () => {
      const distributorColParams = {
        api: undefined,
        data: {
          showRowGroup: true,
        },
        colDef: {
          showRowGroup: true,
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };

      expect(distributorConfigurationGroupFilterValueGetter(distributorColParams)).toBe('');
    });

    it('should return a string according coldDef showRowGroup', () => {
      const distributorColParams = {
        api: undefined,
        data: {
          id: 'idValue',
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };

      expect(distributorConfigurationGroupFilterValueGetter(distributorColParams)).toBe('idValue');
    });

    it('should return a string according coldDef showRowGroup as number', () => {
      const distributorColParams = {
        api: undefined,
        data: {
          1: 'idValue',
        },
        colDef: {
          showRowGroup: '1',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };

      expect(distributorConfigurationGroupFilterValueGetter(distributorColParams)).toBe('idValue');
    });

  });

  describe('afWhiteListToGridLabel()', () => {

    it('should all if afWhitelist is empty', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [];
      expect(afWhiteListToGridLabel(afWhiteList)).toBe('all');
    });

    it('should merge afWhitelist afAddress to afWhitelist.length < 3', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },

      ];
      expect(afWhiteListToGridLabel(afWhiteList)).toBe('1.1.1.1, 2.2.2.2');
    });

    it('should merge afWhitelist afAddress to afWhitelist.length = 3', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
      ];
      expect(afWhiteListToGridLabel(afWhiteList)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11');
    });

    it('should merge afWhitelist afAddress to afWhitelist.length = 4', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
        {
          afAddress: '22.22.22.22',
          afId: 'id_4',
          afName: 'name_4',
        },
      ];
      expect(afWhiteListToGridLabel(afWhiteList)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11 and 1 other');
    });

    it('should merge afWhitelist afAddress to afWhitelist.length > 4', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
        {
          afAddress: '22.22.22.22',
          afId: 'id_4',
          afName: 'name_4',
        },
        {
          afAddress: '22.22.22.11',
          afId: 'id_5',
          afName: 'name_5',
        },
      ];
      expect(afWhiteListToGridLabel(afWhiteList)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11 and 2 others');
    });

  });

  describe('allowedPoiIpAddressesFilterValueGetter()', () => {

    it('should all if afWhitelist is empty', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [];
      const allowedPoiIpAddressesColParams = {
        api: undefined,
        data: {
          listener: {
            afWhitelist: afWhiteList,
          },
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };

      expect(allowedPoiIpAddressesFilterValueGetter(allowedPoiIpAddressesColParams)).toBe('all');
    });

    it('should set value to afWhitelist.length < 3', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
      ];
      const allowedPoiIpAddressesColParams = {
        api: undefined,
        data: {
          listener: {
            afWhitelist: afWhiteList,
          },
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };
      expect(allowedPoiIpAddressesFilterValueGetter(allowedPoiIpAddressesColParams)).toBe('1.1.1.1, 2.2.2.2');
    });

    it('should set value to afWhitelist.length = 3', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
      ];
      const allowedPoiIpAddressesColParams = {
        api: undefined,
        data: {
          listener: {
            afWhitelist: afWhiteList,
          },
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };
      expect(allowedPoiIpAddressesFilterValueGetter(allowedPoiIpAddressesColParams)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11');
    });

    it('should set value to afWhitelist.length = 4', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
        {
          afAddress: '22.22.22.22',
          afId: 'id_4',
          afName: 'name_4',
        },
      ];
      const allowedPoiIpAddressesColParams = {
        api: undefined,
        data: {
          listener: {
            afWhitelist: afWhiteList,
          },
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };
      expect(allowedPoiIpAddressesFilterValueGetter(allowedPoiIpAddressesColParams)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11 and 1 other');
    });

    it('should set value to afWhitelist.length > 4', () => {
      const afWhiteList:DistributorConfigurationListenerAfWhiteList[] = [
        {
          afAddress: '1.1.1.1',
          afId: 'id_1',
          afName: 'name_1',
        },
        {
          afAddress: '2.2.2.2',
          afId: 'id_2',
          afName: 'name_2',
        },
        {
          afAddress: '11.11.11.11',
          afId: 'id_3',
          afName: 'name_3',
        },
        {
          afAddress: '22.22.22.22',
          afId: 'id_4',
          afName: 'name_4',
        },
        {
          afAddress: '22.22.22.11',
          afId: 'id_5',
          afName: 'name_5',
        },
      ];
      const allowedPoiIpAddressesColParams = {
        api: undefined,
        data: {
          listener: {
            afWhitelist: afWhiteList,
          },
        },
        colDef: {
          showRowGroup: 'id',
        } as ColDef,
        column: {} as Column,
        columnApi: undefined,
        context: undefined,
        getValue: (field: string) => field,
        node: {} as RowNode,
      };
      expect(allowedPoiIpAddressesFilterValueGetter(allowedPoiIpAddressesColParams)).toBe('1.1.1.1, 2.2.2.2, 11.11.11.11 and 2 others');
    });

  });

  describe('interfacesAfIdValueGetter()', () => {
    let interfacesAfIdParams:Partial<ValueGetterParams>;

    beforeEach(() => {
      interfacesAfIdParams = {
        colDef: {
          field: 'afid',
        },
        data: {
          contentInterfaces: [],
          dataInterfaces: [],
          provisioningInterfaces: [],
        },
      };

    });

    it('should return empty value if AF has no interfaces', () => {
      const value = interfacesAfIdValueGetter(interfacesAfIdParams);
      expect(value).toBe('-');
    });

    it('should return the Content Interface afId', () => {
      interfacesAfIdParams.data.contentInterfaces = [{ options: [{ key: 'afid', value: '1' }] }];
      const value = interfacesAfIdValueGetter(interfacesAfIdParams);
      expect(value).toBe('1');
    });

    it('should return the Data Interface afId', () => {
      interfacesAfIdParams.data.dataInterfaces = [{ options: [{ key: 'afid', value: '2' }] }];
      const value = interfacesAfIdValueGetter(interfacesAfIdParams);
      expect(value).toBe('2');
    });

    it('should return the Provisioning Interface afId', () => {
      interfacesAfIdParams.data.provisioningInterfaces = [{ options: [{ key: 'afid', value: '3' }] }];
      const value = interfacesAfIdValueGetter(interfacesAfIdParams);
      expect(value).toBe('3');
    });
  });
});
