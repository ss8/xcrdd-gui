import cloneDeep from 'lodash.clonedeep';
import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';

export default function updateDistributorConfigs(
  distributorConfigTableData: DistributorConfigurationTableData,
  distributorConfigsTableData: DistributorConfigurationTableData[],
): DistributorConfigurationTableData[] {
  const clonedDistributorConfigsTableData = cloneDeep(distributorConfigsTableData);
  const updatedEntryProtocol = distributorConfigTableData.afProtocol;
  const updatedAfWhiteList = distributorConfigTableData.listener.afWhitelist;

  // The list of allowed IPs is the same for all listeners with the same protocol.
  // So we will update all entries with the same protocol.
  clonedDistributorConfigsTableData.forEach((entry) => {
    if (entry.afProtocol === updatedEntryProtocol) {
      entry.listener.afWhitelist = cloneDeep(updatedAfWhiteList);
    }
  });

  return clonedDistributorConfigsTableData;
}
