import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { AccessFunctionMenuActions } from '../AccessFunction.types';
import isActionSupportedForAccessFunction from './isActionSupportedForAccessFunction';

describe('isActionSupportedForAccessFunction()', () => {
  it('should return false for Refresh in ERK_UPF', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Refresh,
      'ERK_UPF',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(false);
  });

  it('should return true for Refresh in ALU_DSS', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Refresh,
      'ALU_DSS',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(true);
  });

  it('should return true for Reset in ALU_DSS', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Reset,
      'ALU_DSS',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(true);
  });

  it('should return true for Refresh in ALU_MME', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Refresh,
      'ALU_MME',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(true);
  });

  it('should return false for Reset in ALU_MME', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Reset,
      'ALU_MME',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(false);
  });

  it('should return false for invalid tag', () => {
    expect(isActionSupportedForAccessFunction(
      AccessFunctionMenuActions.Reset,
      'INVALID',
      mockSupportedFeatures.config.accessFunctions,
    )).toBe(false);
  });
});
