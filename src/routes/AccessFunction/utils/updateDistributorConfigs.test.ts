import { DistributorConfigurationTableData } from 'data/distributorConfiguration/distributorConfiguration.types';
import updateDistributorConfigs from './updateDistributorConfigs';

describe('updateDistributorConfigs()', () => {
  let distributorConfigTableData: DistributorConfigurationTableData;
  let distributorConfigsTableData: DistributorConfigurationTableData[] = [];

  beforeEach(() => {
    distributorConfigTableData = {
      id: 'id1',
      name: 'name1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      afProtocol: 'afProtocol1',
      ipAddress: '11.11.11.11',
      port: '1111',
      listener: {
        id: 'listeners1',
        afProtocol: 'afProtocol1',
        ipAddress: '11.11.11.11',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '1111',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId1',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [{
          afId: 'afId1',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }, {
          afId: 'afId2',
          afAddress: '12.12.12.12',
          afName: 'afName',
        }],
      },
      destinations: [],
    };

    distributorConfigsTableData = [{
      id: 'id1',
      name: 'name1',
      instanceId: 'instanceId1',
      moduleName: 'moduleName1',
      afType: 'afType1',
      afProtocol: 'afProtocol1',
      ipAddress: '11.11.11.11',
      port: '1111',
      listener: {
        id: 'listeners1',
        afProtocol: 'afProtocol1',
        ipAddress: '11.11.11.11',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '1111',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId1',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [],
      },
      destinations: [],
    }, {
      id: 'id2',
      name: 'name2',
      instanceId: 'instanceId2',
      moduleName: 'moduleName2',
      afType: 'afType2',
      afProtocol: 'afProtocol2',
      ipAddress: '22.22.22.22',
      port: '2222',
      listener: {
        id: 'listeners2',
        afProtocol: 'afProtocol2',
        ipAddress: '22.22.22.22',
        keepaliveTimeout: '30',
        maxConcurrentConnection: '30',
        port: '2222',
        requiredState: 'ACTIVE',
        secInfoId: 'secInfoId2',
        status: 'ACTIVE',
        numberOfParsers: '10',
        synchronizationHeader: 'NONE',
        transport: 'TCP',
        afWhitelist: [],
      },
      destinations: [],
    }];
  });

  it('should return an empty array if the list of distributor config is empty', () => {
    expect(updateDistributorConfigs(distributorConfigTableData, [])).toEqual([]);
  });

  it('should update the white list in all distributors with the same protocol', () => {
    expect(updateDistributorConfigs(distributorConfigTableData, distributorConfigsTableData)).toEqual([{
      ...distributorConfigsTableData[0],
      listener: {
        ...distributorConfigsTableData[0].listener,
        afWhitelist: distributorConfigTableData.listener.afWhitelist,
      },
    }, {
      ...distributorConfigsTableData[1],
    }]);
  });
});
