import { AccessFunction } from 'data/accessFunction/accessFunction.types';
import { FormTemplate } from 'data/template/template.types';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { isAccessFunctionTemplateAvailable } from './isAccessFunctionTemplateAvailable';

describe('isAccessFunctionTemplateAvailable()', () => {
  let accessFunctionTemplates: { [key: string]: FormTemplate };
  let accessFunction: AccessFunction;

  beforeEach(() => {
    accessFunctionTemplates = {
      ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
    };

    accessFunction = {
      name: 'AF 1',
      tag: 'MVNR_PGW',
      timeZone: 'America/Los_Angeles',
      type: 'MVNR_PGW',
    };
  });

  it('should return false if access function is not defined', () => {
    expect(isAccessFunctionTemplateAvailable(accessFunctionTemplates)).toEqual(false);
  });

  it('should return false if the access function tag is not available as a template', () => {
    accessFunction.tag = 'ALU_MME';
    accessFunction.type = 'ALU_MME';
    expect(isAccessFunctionTemplateAvailable(accessFunctionTemplates, accessFunction)).toEqual(false);
  });

  it('should return true if the access function tag is available as a template', () => {
    expect(isAccessFunctionTemplateAvailable(accessFunctionTemplates, accessFunction)).toEqual(true);
  });
});
