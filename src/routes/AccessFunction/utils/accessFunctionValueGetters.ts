import { ITooltipParams, ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import { DistributorConfigurationListenerAfWhiteList } from 'data/distributorConfiguration/distributorConfiguration.types';

export function distributorConfigurationGroupFilterValueGetter(params: ValueGetterParams): string {
  const rowGroupKey = params.colDef.showRowGroup;

  if (!rowGroupKey || (typeof rowGroupKey !== 'string' && typeof rowGroupKey !== 'number')) {
    return '';
  }

  return params.data[rowGroupKey];
}

export const afWhiteListToGridLabel = (afWhitelist:DistributorConfigurationListenerAfWhiteList[]): string => {

  if (!afWhitelist || !afWhitelist.length) {
    return 'all';
  }

  if (afWhitelist.length <= 3) {
    return afWhitelist.map((ip:DistributorConfigurationListenerAfWhiteList) => ip.afAddress).join(', ').trim();
  }

  const [first, second, third, ...rest] = afWhitelist;

  if (rest.length === 1) {
    return `${first.afAddress}, ${second.afAddress}, ${third.afAddress} and ${rest?.length} other`;
  }

  return `${first.afAddress}, ${second.afAddress}, ${third.afAddress} and ${rest?.length} others`;
};

export const allowedPoiIpAddressesFilterValueGetter = (params:ValueGetterParams): string => {
  const { afWhitelist } = params.data.listener;
  return afWhiteListToGridLabel(afWhitelist);
};

export const interfacesAfIdValueGetter = (params: Partial<ValueGetterParams> | Partial<ITooltipParams>): string => {
  const { colDef: { field } } = params;
  const { contentInterfaces, dataInterfaces, provisioningInterfaces } = params.data;
  const contentInterfacesOptions = contentInterfaces?.[0]?.options || [];
  const dataInterfacesOptions = dataInterfaces?.[0]?.options || [];
  const provisioningInterfacesOptions = provisioningInterfaces?.[0]?.options || [];

  return contentInterfacesOptions.find(findInterfaceOptionByKey(field))?.value ||
  dataInterfacesOptions.find(findInterfaceOptionByKey(field))?.value ||
  provisioningInterfacesOptions.find(findInterfaceOptionByKey(field))?.value || '-';
};

const findInterfaceOptionByKey = (key:string) => (entry: {key: string, value: string}) => entry.key === key;
