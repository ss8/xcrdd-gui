import { FormTemplateMapByKey, FormTemplateSection } from 'data/template/template.types';

// eslint-disable-next-line import/prefer-default-export
export function getFirstAccessFunctionType(templates: FormTemplateMapByKey): string {
  const templateKeys = Object.keys(templates);

  if (templateKeys.length === 0) {
    return '';
  }
  const template = templates[templateKeys[0]];

  const templateSection = (template.general as FormTemplateSection);
  return templateSection.fields?.tag?.options?.[0].value ?? templateKeys[0];
}
