import {
  AccessFunction,
  AccessFunctionInterface,
  AccessFunctionGridRowData,
  AccessFunctionInterfaceFormSectionType,
  ANY_ADDRESSES,
} from 'data/accessFunction/accessFunction.types';
import { DistributorConfiguration } from 'data/distributorConfiguration/distributorConfiguration.types';
import { Network } from 'data/network/network.types';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { FormTemplateMapByKey, FormTemplateSection } from 'data/template/template.types';
import { Xdp } from 'data/xdp/xdp.types';
import { NETWORK_ID_NOT_FOUND } from '../AccessFunction.types';
import { getSubsectionName, getInterfaceFormTemplateSection } from './accessFunctionTemplate.adapter';

type Destination = {
  destinationValue: string,
  destinationToolTip: string,
}

const DISTRIBUTOR_LISTENER_LIMIT = 1;

// eslint-disable-next-line import/prefer-default-export
export const adaptAccessFunctionsGridRowData = (
  accessFunctions: AccessFunction[],
  enabledAccessFunctions: SupportedFeaturesAccessFunctions,
  accessFunctionTemplates: FormTemplateMapByKey,
  networks: Network[],
  xdps: Xdp[],
  distributorConfigurationsByAccessFunctionType: { [key: string]: DistributorConfiguration[] },
): AccessFunctionGridRowData[] => {
  const interfaces:AccessFunctionGridRowData[] = [];

  accessFunctions.forEach((accessFunction) => {

    const provisioningInterfaces = adaptInterfaceToRowData(
      'provisioningInterfaces',
      accessFunction,
      enabledAccessFunctions,
      accessFunctionTemplates,
      networks,
      xdps,
      distributorConfigurationsByAccessFunctionType,
    );

    const dataInterfaces = adaptInterfaceToRowData(
      'dataInterfaces',
      accessFunction,
      enabledAccessFunctions,
      accessFunctionTemplates,
      networks,
      xdps,
      distributorConfigurationsByAccessFunctionType,
    );

    const contentInterfaces = adaptInterfaceToRowData(
      'contentInterfaces',
      accessFunction,
      enabledAccessFunctions,
      accessFunctionTemplates,
      networks,
      xdps,
      distributorConfigurationsByAccessFunctionType,
    );

    interfaces.push(...provisioningInterfaces);
    interfaces.push(...dataInterfaces);
    interfaces.push(...contentInterfaces);

  });

  return interfaces;
};

const optionByKey = (key:string) => (option: {key: string, value: string | boolean}) => {
  return option.key === key;
};

const interfaceOptionsKeyValue = (key:string, interfaceEntry: AccessFunctionInterface) => {
  return interfaceEntry.options?.find(optionByKey(key))?.value.toString() || '';
};

const oneOfInterfaceOptionsKeyValue = (keys:string[], interfaceEntry: AccessFunctionInterface) => {
  const keysValues = keys.map((key) => interfaceEntry.options?.find(optionByKey(key))?.value.toString());

  const value = keysValues
    .find((keyValue) => keyValue != null && keyValue !== '' && keyValue !== '0.0.0.0') || '';

  if (ANY_ADDRESSES.includes(value)) {
    return '0.0.0.0';
  }

  return value;
};

const getSubTypeValue = (interfaceSectionType: AccessFunctionInterfaceFormSectionType): string => {
  if (interfaceSectionType === 'provisioningInterfaces') {
    return 'X1';
  } if (interfaceSectionType === 'dataInterfaces') {
    return 'X2';
  }

  return 'X3';
};

const getIpAddress = (interfaceOptions: { [key: string]: string; }, anyAddresses:string[]):string => {
  const ipAddress = interfaceOptions.ownIPAddr || interfaceOptions.ownipaddr;

  const isAnyAddress = anyAddresses.includes(ipAddress);

  if (isAnyAddress) {
    return '0.0.0.0';
  }

  return ipAddress;
};

function getDistributorInterfaceDestination(
  distributors: DistributorConfiguration[] | undefined,
): Destination {
  if (distributors == null || distributors.length === 0) {
    return { destinationValue: '', destinationToolTip: '' };
  }

  const destinationSet = new Set<string>();
  distributors.forEach(({ listeners }) => {
    listeners.forEach(({ ipAddress }) => {
      destinationSet.add(`${ipAddress}`);
    });
  });

  const destinations = Array.from(destinationSet);

  let destinationValue = destinations.join(', ');
  const destinationToolTip = destinationValue;

  if (destinations.length > DISTRIBUTOR_LISTENER_LIMIT) {
    destinationValue = destinations.slice(0, DISTRIBUTOR_LISTENER_LIMIT).join(', ');
    const remaining = destinations.slice(DISTRIBUTOR_LISTENER_LIMIT, destinations.length);

    if (remaining.length === 1) {
      destinationValue = `${destinationValue} and 1 other`;

    } else {
      destinationValue = `${destinationValue} and ${remaining.length} others`;
    }
  }

  return {
    destinationValue,
    destinationToolTip,
  };
}

const getInterfaceDestination = (
  accessFunctionTag: string,
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
  interfaceEntry: AccessFunctionInterface,
  interfaceTemplateSection: FormTemplateSection | undefined,
  networks: Network[],
  xdps: Xdp[],
): Destination => {
  let destinationValue = '';

  const interfaceOptions:{[key:string]: string} = interfaceEntry.options
    .reduce((total, option) => ({ ...total, [option.key]: option.value }), {});

  let destinationToolTip = '';
  const ipAddress = getIpAddress(interfaceOptions, ANY_ADDRESSES);

  if (ipAddress && interfaceOptions.ownIPPort) {
    destinationValue = `${ipAddress}:${interfaceOptions.ownIPPort}`;
    destinationToolTip = `${ipAddress}:${interfaceOptions.ownIPPort}`;

  } else if (ipAddress) {
    destinationValue = ipAddress;

  } else if (isOwnNetIdInUse(accessFunctionTag, interfaceSectionKey, interfaceOptions)) {
    destinationValue = getNetworkNameValue(networks, interfaceOptions.ownnetid);
    destinationToolTip = netIdToDestinationToolTip(
      'ownnetid',
      interfaceEntry,
      interfaceTemplateSection,
    );

  } else if (interfaceOptions.ipduid) {
    destinationValue = xdps.find((xdp) => xdp.id === interfaceOptions.ipduid)?.ipduId || '';

  } else if (interfaceOptions.cdnetid) {
    destinationValue = getNetworkNameValue(networks, interfaceOptions.cdnetid);
    destinationToolTip = netIdToDestinationToolTip(
      'cdnetid',
      interfaceEntry,
      interfaceTemplateSection,
    );
  }

  return {
    destinationValue: destinationValue || '',
    destinationToolTip,
  };
};

function isOwnNetIdInUse(
  accessFunctionTag: string,
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
  interfaceOptions: { [key: string]: string },
): boolean {
  const {
    ownnetid,
    transport,
  } = interfaceOptions;

  if ((accessFunctionTag === 'ETSI103221') &&
      (interfaceSectionKey === 'dataInterfaces' || interfaceSectionKey === 'contentInterfaces')) {
    // In this case, the ownnetid is only in use if the transport is TCP
    return !!(ownnetid && transport === 'TCP');
  }

  return !!ownnetid;
}

const netIdToDestinationToolTip = (
  netIdKey: string,
  interfaceEntry: AccessFunctionInterface,
  interfaceTemplateSection: FormTemplateSection | undefined,
): string => {
  const fields = interfaceTemplateSection?.fields;
  const netValue = getInterfaceEntryOptionValue(interfaceEntry, netIdKey);

  return fields
    ?.[netIdKey]
    ?.options?.find(({ value }) => value === netValue)?.label || '';
};

const getInterfaceEntryOptionValue = (
  interfaceEntry: AccessFunctionInterface,
  optionKey: string,
): string => {
  return interfaceEntry?.options.find((option) => option.key === optionKey)?.value.toString() || '';
};

const getNetworkNameValue = (
  networks: Network[],
  netId: string,
): string => {
  let networkId = networks.find(({ id }) => id === netId)?.networkId;

  if (networkId == null) {
    networkId = NETWORK_ID_NOT_FOUND;
  }

  return networkId;
};

const adaptInterfaceToRowData = (
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
  accessFunction: AccessFunction,
  enabledAccessFunctions: SupportedFeaturesAccessFunctions,
  accessFunctionTemplates: FormTemplateMapByKey,
  networks: Network[],
  xdps: Xdp[],
  distributorConfigurationsByAccessFunctionType: { [key: string]: DistributorConfiguration[] },
): AccessFunctionGridRowData[] => {
  const {
    id = '',
    name = '',
    tag = '',
  } = accessFunction;

  const interfaceTemplateSection = getInterfaceTemplateSection(
    accessFunction,
    accessFunctionTemplates,
    interfaceSectionKey,
  );

  const accessFunctionInterfaces = getAccessFunctionInterfaces(
    accessFunction,
    interfaceSectionKey,
    interfaceTemplateSection,
  );

  return accessFunctionInterfaces.map((entry) => {

    let destination: Destination;

    if (isDistributorConfigurationInterface(accessFunction, interfaceSectionKey, interfaceTemplateSection)) {
      const distributors = getDistributorsForInterfaceTemplateSection(
        accessFunction,
        interfaceTemplateSection,
        distributorConfigurationsByAccessFunctionType,
      );

      destination = getDistributorInterfaceDestination(distributors);

    } else {
      destination = getInterfaceDestination(
        accessFunction.tag,
        interfaceSectionKey,
        entry,
        interfaceTemplateSection,
        networks,
        xdps,
      );
    }

    return ({
      id,
      name,
      tag,
      tagName: enabledAccessFunctions[tag]?.title || '',
      subType: getSubTypeValue(interfaceSectionKey),
      ipAddress: oneOfInterfaceOptionsKeyValue(['destIPAddr', 'uri'], entry),
      port: interfaceOptionsKeyValue('destPort', entry),
      configuredState: interfaceOptionsKeyValue('reqState', entry),
      state: interfaceOptionsKeyValue('state', entry),
      destination: destination.destinationValue,
      destinationToolTip: destination.destinationToolTip,
      provisioningInterfaces: accessFunction.provisioningInterfaces,
      afid: interfaceOptionsKeyValue('afid', entry),
    });
  });
};

function getInterfaceTemplateSection(
  accessFunction: AccessFunction,
  accessFunctionTemplates: FormTemplateMapByKey,
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
): FormTemplateSection | undefined {
  const {
    tag,
    contentDeliveryVia,
  } = accessFunction;

  const template = accessFunctionTemplates[tag];

  if (template == null) {
    return;
  }

  const interfaceSuffix = getSubsectionName(
    { contentDeliveryVia },
    template,
    interfaceSectionKey,
  );

  return getInterfaceFormTemplateSection(
    template,
    interfaceSectionKey,
    interfaceSuffix,
  );
}

function getAccessFunctionInterfaces(
  accessFunction: AccessFunction,
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
  interfaceTemplateSection: FormTemplateSection | undefined,
): AccessFunctionInterface[] {
  const accessFunctionInterfaces = accessFunction[interfaceSectionKey] || [];

  if (accessFunctionInterfaces.length === 0) {
    if (isDistributorConfigurationInterface(accessFunction, interfaceSectionKey, interfaceTemplateSection)) {
      accessFunctionInterfaces.push({ id: '', name: '', options: [] });
    }
  }

  return accessFunctionInterfaces;
}

function isDistributorConfigurationInterface(
  accessFunction: AccessFunction,
  interfaceSectionKey: AccessFunctionInterfaceFormSectionType,
  interfaceTemplateSection: FormTemplateSection | undefined,
): boolean {
  if (interfaceTemplateSection?.metadata.sectionType === 'distributorConfiguration') {
    return true;
  }

  if ((accessFunction.tag === 'ETSI103221') &&
      (accessFunction[interfaceSectionKey]?.length === 0) &&
      (interfaceSectionKey === 'dataInterfaces' || interfaceSectionKey === 'contentInterfaces')) {
    // For the ETSI103221 AF, the dataInterfaces and contentInterfaces are considered to be distributorConfiguration
    // if they are empty.
    return true;
  }

  return false;
}

function getDistributorsForInterfaceTemplateSection(
  accessFunction: AccessFunction,
  interfaceTemplateSection: FormTemplateSection | undefined,
  distributorConfigurationsByAccessFunctionType: { [key: string]: DistributorConfiguration[] },
): DistributorConfiguration[] {
  let accessFunctionType = accessFunction.type;

  const distributorsType = interfaceTemplateSection?.fields?.distributorsType?.initial;
  if (distributorsType != null && typeof distributorsType === 'string') {
    accessFunctionType = distributorsType;
  }

  return distributorConfigurationsByAccessFunctionType[accessFunctionType] ?? [];
}
