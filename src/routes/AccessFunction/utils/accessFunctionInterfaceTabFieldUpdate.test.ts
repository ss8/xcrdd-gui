import { AccessFunctionFormData } from 'data/accessFunction/accessFunction.types';
import { FormTemplate } from 'data/template/template.types';
import { mockNodes } from '__test__/mockNodes';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { handleInterfacesFieldUpdate } from './accessFunctionInterfaceTabFieldUpdate';

describe('handleInterfacesFieldUpdate()', () => {
  let accessFunctionTemplates: { [key: string]: FormTemplate };
  let accessFunction: AccessFunctionFormData;
  let accessFunctionMVNRPGW: AccessFunctionFormData;
  let accessFunctionERKVMME: AccessFunctionFormData;
  let accessFunctionSTREAMWIDE: AccessFunctionFormData;
  let accessFunctionERKSMF: AccessFunctionFormData;

  beforeEach(() => {
    accessFunction = {
      name: 'AF 1',
      tag: 'ERK_UPF',
      timeZone: '',
      type: 'ERK_UPF',
      provisioningInterfaces: [{
        id: 'id1',
        name: 'name1',
        destIPAddr: '1.1.1.1',
        destPort: '1',
        dscp: '10',
        reqState: 'INACTIVE',
        state: '',
        tpkt: false,
        version: '3.15.1',
        uri: 'https://domain.com',
        transport: 'HTTPS',
        secinfoid: '',
      }, {
        id: 'id2',
        name: 'name2',
        destIPAddr: '2.2.2.2',
        destPort: '2',
        dscp: '20',
        tpkt: false,
        uri: 'http://domain.com',
        transport: 'HTTP',
        secinfoid: 'Tk9ORQ',
      }],
    };

    accessFunctionMVNRPGW = {
      ...accessFunction,
      tag: 'MVNR_PGW',
      type: 'MVNR_PGW',
      contentDeliveryVia: 'directFromAF',
      contentInterfaces: [{
        id: 'id3',
        name: 'name3',
        destIPAddr: '1.1.1.1',
        destPort: '1,2,3,4',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        dscp: '32',
        lbid: '',
        maxx2connsperaf: '4',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }],
    };

    accessFunctionERKVMME = {
      ...accessFunction,
      tag: 'ERK_VMME',
      type: 'ERK_VMME',
      contentDeliveryVia: 'directFromAF',
      dataInterfaces: [{
        id: 'id2',
        name: 'name2',
        destIPAddr: '1.1.1.1',
        destPort: '1,2,3,4',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        secinfoid: '',
        dscp: '32',
        tpkt: true,
        x224headerpresent: true,
      }],
    };

    accessFunctionSTREAMWIDE = {
      ...accessFunction,
      tag: 'STREAMWIDE',
      type: 'STREAMWIDE',
      dataInterfaces: [{
        id: 'id3',
        name: 'name3',
        destIPAddr: '1.1.1.1',
        destPort: '11',
        ftpType: 'FTP',
        secinfoid: '',
        userName: '',
        userPassword: '',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        serverName: '',
        destPath: '',
        appProtocol: 'StreamWIDE',
        version: '1.0',
      }, {
        id: 'id4',
        name: 'name4',
        destIPAddr: '2.2.2.2',
        destPort: '22',
        ftpType: 'FTPS',
        secinfoid: '',
        userName: '',
        userPassword: '',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        serverName: '',
        destPath: '',
        appProtocol: 'StreamWIDE',
        version: '1.0',
      }],
    };

    accessFunctionERKSMF = {
      ...accessFunction,
      tag: 'ERK_SMF',
      type: 'ERK_SMF',
      provisioningInterfaces: [{
        id: 'id1',
        dscp: '32',
        reqState: 'ACTIVE',
        secinfoid: 'Tk9ORQ',
        state: 'ACTIVE',
        ccnodename: '',
        mdf2NodeName: 'MDF2NODE',
        mdf3NodeName: '',
      }, {
        id: 'id2',
        name: 'name2',
        dscp: '32',
        reqState: 'ACTIVE',
        secinfoid: 'Tk9ORQ',
        state: 'ACTIVE',
        ccnodename: 'CCPAGNODE',
        mdf2NodeName: 'MDF2NODE',
        mdf3NodeName: 'MDF3NODE',
      }],
    };

    accessFunctionTemplates = {
      ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      ERK_SMF: getMockTemplateByKey('ERK_SMF'),
      MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
      ERK_VMME: getMockTemplateByKey('ERK_VMME'),
      STREAMWIDE: getMockTemplateByKey('STREAMWIDE'),
    };
  });

  it('should return the data if no template', () => {
    expect(handleInterfacesFieldUpdate(undefined, accessFunction, 'provisioningInterfaces', 'id1', 'destIPAddr', '', mockNodes)).toEqual(accessFunction);
  });

  it('should return the data as is if interfaces are empty', () => {
    const template = accessFunctionTemplates.ERK_UPF;
    accessFunction.provisioningInterfaces = [];
    expect(handleInterfacesFieldUpdate(template, accessFunction, 'provisioningInterfaces', 'id1', 'destIPAddr', '', mockNodes)).toEqual(accessFunction);
  });

  it('should return the interface list unchanged if no matching interface id', () => {
    const template = accessFunctionTemplates.ERK_UPF;
    expect(handleInterfacesFieldUpdate(template, accessFunction, 'provisioningInterfaces', 'idx', 'destIPAddr', '', mockNodes)).toEqual(accessFunction);
  });

  it('should set the secinfoid field if it is empty when uri starts with http', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'provisioningInterfaces', 'id1', 'uri', 'http://test.com', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      provisioningInterfaces: [
        {
          ...accessFunctionMVNRPGW.provisioningInterfaces?.[0],
          secinfoid: 'Tk9ORQ',
          uri: 'http://test.com',
        },
        accessFunctionMVNRPGW.provisioningInterfaces?.[1],
      ],
    });
  });

  it('should not set the secinfoid field if it is not empty when uri starts with http', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    accessFunctionMVNRPGW.provisioningInterfaces = [
      {
        ...accessFunctionMVNRPGW.provisioningInterfaces?.[0],
        id: 'id1',
        name: 'name1',
        secinfoid: 'something',
      },
      {
        ...accessFunctionMVNRPGW.provisioningInterfaces?.[1],
        id: 'id2',
        name: 'name2',
      },
    ];
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'provisioningInterfaces', 'id1', 'uri', 'http://test.com', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      provisioningInterfaces: [
        {
          ...accessFunctionMVNRPGW.provisioningInterfaces?.[0],
          secinfoid: 'something',
          uri: 'http://test.com',
        },
        accessFunctionMVNRPGW.provisioningInterfaces?.[1],
      ],
    });
  });

  it('should set the secinfoid field if it is Tk9ORQ when uri starts with https', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'provisioningInterfaces', 'id2', 'uri', 'https://test.com', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      provisioningInterfaces: [
        accessFunctionMVNRPGW.provisioningInterfaces?.[0],
        {
          ...accessFunctionMVNRPGW.provisioningInterfaces?.[1],
          secinfoid: '',
          uri: 'https://test.com',
        },
      ],
    });
  });

  it('should not set the secinfoid field if it is not Tk9ORQ when uri starts with https', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    accessFunctionMVNRPGW.provisioningInterfaces = [
      {
        ...accessFunctionMVNRPGW.provisioningInterfaces?.[0],
        id: 'id1',
        name: 'name1',
      },
      {
        ...accessFunctionMVNRPGW.provisioningInterfaces?.[1],
        id: 'id2',
        name: 'name2',
        secinfoid: 'something',
      },
    ];
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'provisioningInterfaces', 'id2', 'uri', 'https://test.com', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      provisioningInterfaces: [
        accessFunctionMVNRPGW.provisioningInterfaces?.[0],
        {
          ...accessFunctionMVNRPGW.provisioningInterfaces?.[1],
          secinfoid: 'something',
          uri: 'https://test.com',
        },
      ],
    });
  });

  it('should not set the secinfoid and transport fields when changing uri for another access function tag', () => {
    const template = accessFunctionTemplates.ERK_UPF;
    expect(handleInterfacesFieldUpdate(template, accessFunction, 'provisioningInterfaces', 'id2', 'uri', 'https://test.com', mockNodes)).toEqual({
      ...accessFunction,
      provisioningInterfaces: [
        accessFunction.provisioningInterfaces?.[0],
        {
          ...accessFunction.provisioningInterfaces?.[1],
          uri: 'https://test.com',
        },
      ],
    });
  });

  it('should set the destPort to 0 when maxx2connsperaf is greater than 4', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    accessFunctionMVNRPGW.contentDeliveryVia = 'trafficManager';
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'contentInterfaces', 'id3', 'maxx2connsperaf', '5', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      contentInterfaces: [
        {
          ...accessFunctionMVNRPGW.contentInterfaces?.[0],
          destPort: '0',
          maxx2connsperaf: '5',
        },
      ],
    });
  });

  it('should leave destPort as is when maxx2connsperaf is smaller or equal to 4', () => {
    const template = accessFunctionTemplates.MVNR_PGW;
    expect(handleInterfacesFieldUpdate(template, accessFunctionMVNRPGW, 'contentInterfaces', 'id3', 'maxx2connsperaf', '4', mockNodes)).toEqual({
      ...accessFunctionMVNRPGW,
      contentInterfaces: [
        {
          ...accessFunctionMVNRPGW.contentInterfaces?.[0],
          destPort: '1,2,3,4',
          maxx2connsperaf: '4',
        },
      ],
    });
  });
  it('should leave x224headerpresent as true when tpkt is changed as true', () => {
    const template = accessFunctionTemplates.ERK_VMME;
    expect(handleInterfacesFieldUpdate(template, accessFunctionERKVMME, 'dataInterfaces', 'id2', 'tpkt', true, mockNodes)).toEqual({
      ...accessFunctionERKVMME,
      dataInterfaces: [{
        ...accessFunctionERKVMME.dataInterfaces?.[0],
        tpkt: true,
        x224headerpresent: true,
      }],
    });
  });

  it('should leave x224headerpresent as false when tpkt is changed as false', () => {
    const template = accessFunctionTemplates.ERK_VMME;
    expect(handleInterfacesFieldUpdate(template, accessFunctionERKVMME, 'dataInterfaces', 'id2', 'tpkt', false, mockNodes)).toEqual({
      ...accessFunctionERKVMME,
      dataInterfaces: [{
        ...accessFunctionERKVMME.dataInterfaces?.[0],
        tpkt: false,
        x224headerpresent: false,
      }],
    });
  });

  it('should set the secinfoid field if it is empty when ftptype equals FTP', () => {
    const template = accessFunctionTemplates.STREAMWIDE;
    expect(handleInterfacesFieldUpdate(template, accessFunctionSTREAMWIDE, 'dataInterfaces', 'id1', 'ftpType', 'FTP', mockNodes)).toEqual({
      ...accessFunctionSTREAMWIDE,
      dataInterfaces: [
        {
          ...accessFunctionSTREAMWIDE.dataInterfaces?.[0],
          secinfoid: '',
          ftpType: 'FTP',
        },
        accessFunctionSTREAMWIDE.dataInterfaces?.[1],
      ],
    });
  });

  it('should not set the secinfoid field if it is not empty when ftptype equals FTP', () => {
    const template = accessFunctionTemplates.STREAMWIDE;
    accessFunctionSTREAMWIDE.dataInterfaces = [
      {
        ...accessFunctionSTREAMWIDE.dataInterfaces?.[0],
        id: 'id1',
        name: 'name1',
        secinfoid: 'something',
      },
      {
        ...accessFunctionSTREAMWIDE.dataInterfaces?.[1],
        id: 'id2',
        name: 'name2',
      },
    ];
    expect(handleInterfacesFieldUpdate(template, accessFunctionSTREAMWIDE, 'dataInterfaces', 'id1', 'ftpType', 'FTP', mockNodes)).toEqual({
      ...accessFunctionSTREAMWIDE,
      dataInterfaces: [
        {
          ...accessFunctionSTREAMWIDE.dataInterfaces?.[0],
          secinfoid: 'something',
          ftpType: 'FTP',
        },
        accessFunctionSTREAMWIDE.dataInterfaces?.[1],
      ],
    });
  });

  it('should set the secinfoid field if it is Tk9ORQ when ftpType equals FTPS', () => {
    const template = accessFunctionTemplates.STREAMWIDE;
    expect(handleInterfacesFieldUpdate(template, accessFunctionSTREAMWIDE, 'dataInterfaces', 'id2', 'ftpType', 'FTPS', mockNodes)).toEqual({
      ...accessFunctionSTREAMWIDE,
      dataInterfaces: [
        accessFunctionSTREAMWIDE.dataInterfaces?.[0],
        {
          ...accessFunctionSTREAMWIDE.dataInterfaces?.[1],
          secinfoid: '',
          ftpType: 'FTPS',
        },
      ],
    });
  });

  it('should not set the secinfoid field if it is not Tk9ORQ when ftpType equals FTPS', () => {
    const template = accessFunctionTemplates.STREAMWIDE;
    accessFunctionSTREAMWIDE.dataInterfaces = [
      {
        ...accessFunctionSTREAMWIDE.dataInterfaces?.[0],
        id: 'id1',
        name: 'name1',
      },
      {
        ...accessFunctionSTREAMWIDE.dataInterfaces?.[1],
        id: 'id2',
        name: 'name2',
        secinfoid: 'something',
      },
    ];
    expect(handleInterfacesFieldUpdate(template, accessFunctionSTREAMWIDE, 'dataInterfaces', 'id2', 'ftpType', 'FTPS', mockNodes)).toEqual({
      ...accessFunctionSTREAMWIDE,
      dataInterfaces: [
        accessFunctionSTREAMWIDE.dataInterfaces?.[0],
        {
          ...accessFunctionSTREAMWIDE.dataInterfaces?.[1],
          secinfoid: 'something',
          ftpType: 'FTPS',
        },
      ],
    });
  });

  it('should set the mdf3NodeName value when ccnodename is changed', () => {
    const template = accessFunctionTemplates.ERK_SMF;
    const actual = handleInterfacesFieldUpdate(
      template,
      accessFunctionERKSMF,
      'provisioningInterfaces',
      'id1',
      'ccnodename',
      'CCPAGNODE',
      mockNodes,
    );

    expect(actual).toEqual({
      ...accessFunctionERKSMF,
      provisioningInterfaces: [
        {
          ...accessFunctionERKSMF.provisioningInterfaces?.[0],
          ccnodename: 'CCPAGNODE',
          mdf3NodeName: 'MDF3NODE',
        },
        accessFunctionERKSMF.provisioningInterfaces?.[1],
      ],
    });
  });
});
