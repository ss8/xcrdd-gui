import { AccessFunction, AccessFunctionFormData } from 'data/accessFunction/accessFunction.types';
import { Operation, WithAuditDetails, AuditDetails } from 'data/types';
import { adaptToSubmit, setAccessFunctionOperationValues } from 'data/accessFunction/accessFunction.adapter';
import {
  getAuditDetails,
  getAuditFieldInfo,
  AuditService,
  AuditActionType,
} from 'shared/userAudit/audit-constants';

export default function getAccessFunctionDataToSubmit(
  accessFunctionFormData: AccessFunctionFormData,
  originalAccessFunction: AccessFunction | null,
  isAuditEnabled: boolean,
  userId: string,
  userName: string,
  operation: Operation,
): WithAuditDetails<AccessFunction> {
  const adaptedFormData = adaptToSubmit(accessFunctionFormData);
  const dataToSubmit = setAccessFunctionOperationValues(adaptedFormData, originalAccessFunction, operation);

  const accessFunctionBody: WithAuditDetails<AccessFunction> = {
    data: dataToSubmit,
  };

  let actionType = AuditActionType.ACCESS_FUNCTION_ADD;
  let isEdit = false;
  if (operation === 'UPDATE') {
    actionType = AuditActionType.ACCESS_FUNCTION_EDIT;
    isEdit = true;
  }

  if (isAuditEnabled) {
    const auditDetails = getAuditDetails(
      isAuditEnabled,
      userId,
      userName,
      getAuditFieldInfo(adaptedFormData, null, isEdit, originalAccessFunction),
      adaptedFormData.name,
      AuditService.AF,
      actionType,
    );
    accessFunctionBody.auditDetails = auditDetails as AuditDetails;
  }

  return accessFunctionBody;
}
