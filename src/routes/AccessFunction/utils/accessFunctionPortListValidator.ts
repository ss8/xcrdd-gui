// eslint-disable-next-line import/prefer-default-export
export function accessFunctionPortListValidator(value: string | undefined, numberOfPorts = 1): boolean {
  if (value == null || value.trim() === '' || value.trim() === '0') {
    return true;
  }

  if (numberOfPorts > 4) {
    // Only allowed value is 0
    return false;
  }

  const ports = value.split(',').map((port) => port.trim());

  if (ports.length !== numberOfPorts) {
    return false;
  }

  const allPortsHasValue = ports.every((port) => port !== '');
  if (!allPortsHasValue) {
    return false;
  }

  const numericPorts = ports.map((port) => +port);
  const allPortsValid = numericPorts.every((port) => port >= 1 && port <= 65535);
  if (!allPortsValid) {
    return false;
  }

  const hasRepeatedPort = Array.from(new Set(numericPorts)).length !== numericPorts.length;
  if (hasRepeatedPort) {
    return false;
  }

  return true;
}
