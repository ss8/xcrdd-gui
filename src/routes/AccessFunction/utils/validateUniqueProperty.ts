import { AccessFunctionInterfaceFormData } from 'data/accessFunction/accessFunction.types';

export default function validateUniqueProperty(
  field: keyof AccessFunctionInterfaceFormData,
  interfaces: AccessFunctionInterfaceFormData[],
): boolean {
  const items = interfaces.map((entry) => entry[field]);
  return [...new Set(items)].length === items.length;
}
