import { Contact, ValueLabelPair } from 'data/types';
import { FormTemplateSection, FormTemplate } from 'data/template/template.types';
import { AccessFunctionFormData, AccessFunctionInterfaceFormData } from 'data/accessFunction/accessFunction.types';
import { Network } from 'data/network/network.types';
import { ACCESS_FUNCTION_SECURITY_INFO_FILTER, SecurityInfoFilterByInterface } from 'data/securityInfo/securityInfo.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { getMockTemplateByKey } from '__test__/mockTemplates';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockXtps } from '__test__/mockXtps';
import { mockXdps } from '__test__/mockXdps';
import { mockNodes } from '__test__/mockNodes';
import cloneDeep from 'lodash.clonedeep';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import {
  adaptTemplateOptions,
  adaptTemplateVisibilityMode,
  adaptLocationField,
  adaptSecurityInfoField,
  getInterfaceFormTemplateSectionName,
  adaptContentDeliveryViaField,
  getInterfaceFormTemplateSection,
  adaptTemplateInterfaceFields,
  adaptTemplateForInterfacesFieldUpdate,
  adaptNetworkIdField,
  adaptXDPIpAddress,
  removeFieldFromLayoutRows,
  adaptTemplate5GFields,
  adaptTemplateLabelsForDeployment,
} from './accessFunctionTemplate.adapter';

describe('adaptTemplateOptions()', () => {
  let timezones: ValueLabelPair[];
  let contacts: Contact[];
  let networks: Network[];
  let countries: string[];
  let accessFunctionTemplates: { [key: string]: FormTemplate };

  beforeEach(() => {
    timezones = [
      { value: 'America/Los_Angeles', label: 'US/Pacific' },
      { value: 'America/New_York', label: 'US/Eastern' },
    ];

    contacts = [{
      id: 'id1',
      name: 'Name 1',
    }, {
      id: 'id2',
      name: 'Name 2',
    }];

    networks = [{
      id: 'id1',
      networkId: 'networkId1',
      ownIPAddress: '12.12.12.12',
      startPortNumber: '1000',
      portNumberRange: '100',
    }, {
      id: 'id2',
      networkId: 'networkId2',
      ownIPAddress: 'cdef:cdef:cdef:cdef::cdef',
      startPortNumber: '2000',
      portNumberRange: '200',
    }];

    countries = ['US'];

    accessFunctionTemplates = {
      ERK_UPF: getMockTemplateByKey('ERK_UPF'),
      ERK_SMF: getMockTemplateByKey('ERK_SMF'),
      NSN_CSCF: getMockTemplateByKey('NSN_CSCF'),
      NSNHLR_SDM_PRGW: getMockTemplateByKey('NSNHLR_SDM_PRGW'),
      ALU_MME: getMockTemplateByKey('ALU_MME'),
      ALU_DSS: getMockTemplateByKey('ALU_DSS'),
      ERK_PGW: getMockTemplateByKey('ERK_PGW'),
      MV_SMSIWF: getMockTemplateByKey('MV_SMSIWF'),
      MVNR_PGW: getMockTemplateByKey('MVNR_PGW'),
      LU3G_CSCF: getMockTemplateByKey('LU3G_CSCF'),
      ERK_VMME: getMockTemplateByKey('ERK_VMME'),
      SONUS_SBC: getMockTemplateByKey('SONUS_SBC'),
      ACME_X123: getMockTemplateByKey('ACME_X123'),
      ERK_5GAMF: getMockTemplateByKey('ERK_5GAMF'),
      NOK_5GUDM: getMockTemplateByKey('NOK_5GUDM'),
      NSN_SDM: getMockTemplateByKey('NSN_SDM'),
      NKSR: getMockTemplateByKey('NKSR'),
      GPVRADIUS: getMockTemplateByKey('GPVRADIUS'),
      MVNR_5GSMSC: getMockTemplateByKey('MVNR_5GSMSC'),
      NOK_5GSMF: getMockTemplateByKey('NOK_5GSMF'),
      NOK_5GAMF: getMockTemplateByKey('NOK_5GAMF'),
      NOK_5GSMSF: getMockTemplateByKey('NOK_5GSMSF'),
    };
  });

  describe('adaptTemplateOptions()', () => {
    it('should set the tag based in the templates', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Alcatel-Lucent Distributed Soft Switch',
        value: 'ALU_DSS',
      }, {
        label: 'Alcatel-Lucent MME',
        value: 'ALU_MME',
      }, {
        label: 'Alcatel-Lucent Web Gateway',
        value: 'LU3G_CSCF',
      }, {
        label: 'Ericsson AMF',
        value: 'ERK_5GAMF',
      }, {
        label: 'Ericsson SAEGW/PGW',
        value: 'ERK_PGW',
      }, {
        label: 'Ericsson SMF',
        value: 'ERK_SMF',
      }, {
        label: 'Ericsson UPF',
        value: 'ERK_UPF',
      }, {
        label: 'Ericsson vMME',
        value: 'ERK_VMME',
      }, {
        label: 'Mavenir PGW',
        value: 'MVNR_PGW',
      }, {
        label: 'Mavenir SMS Center',
        value: 'MVNR_5GSMSC',
      }, {
        label: 'Mavenir SMS IWF',
        value: 'MV_SMSIWF',
      }, {
        label: 'Nokia AMF',
        value: 'NOK_5GAMF',
      }, {
        label: 'Nokia HSS/SDM',
        value: 'NSN_SDM',
      }, {
        label: 'Nokia SMF',
        value: 'NOK_5GSMF',
      }, {
        label: 'Nokia SMSF',
        value: 'NOK_5GSMSF',
      }, {
        label: 'Nokia SR 7750',
        value: 'NKSR',
      }, {
        label: 'Nokia UDM',
        value: 'NOK_5GUDM',
      }, {
        label: 'Nokia-Siemens CSCF',
        value: 'NSN_CSCF',
      }, {
        label: 'Nokia-Siemens HLR/HSS (PrGW)',
        value: 'NSNHLR_SDM_PRGW',
      }, {
        label: 'Oracle X123',
        value: 'ACME_X123',
      }, {
        label: 'Sensor Virtual Radius',
        value: 'GPVRADIUS',
      }, {
        label: 'Sonus SBC',
        value: 'SONUS_SBC',
      }];

      expect((templates.ERK_UPF.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.ERK_SMF.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.ERK_PGW.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.ALU_MME.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.ALU_DSS.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.MV_SMSIWF.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.MVNR_PGW.general as FormTemplateSection).fields.tag.options).toEqual(expected);
      expect((templates.ACME_X123.general as FormTemplateSection).fields.tag.options).toEqual(expected);
    });

    it('should set the country', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a country',
        value: '',
      }, {
        label: 'US',
        value: 'US',
      }];

      expect((templates.ERK_UPF.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.ERK_SMF.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.ERK_PGW.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.ALU_MME.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.ALU_DSS.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.MV_SMSIWF.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.MVNR_PGW.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
      expect((templates.ACME_X123.systemInformation as FormTemplateSection).fields.country.options).toEqual(expected);
    });

    it('should set the timezones', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = expect.arrayContaining([{
        label: 'Select a time zone',
        value: '',
      }, {
        label: expect.stringMatching(/(US\/Pacific \(GMT-08\)|US\/Pacific \(GMT-07\))/),
        value: 'America/Los_Angeles',
      }, {
        label: expect.stringMatching(/(US\/Eastern \(GMT-05\)|US\/Eastern \(GMT-04\))/),
        value: 'America/New_York',
      }]);

      expect((templates.ERK_UPF.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ERK_SMF.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ERK_PGW.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ALU_MME.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ALU_DSS.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.MV_SMSIWF.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.MVNR_PGW.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ACME_X123.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
    });

    it('should set the timezone if one option only', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        [timezones[0]],
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = expect.arrayContaining([{
        label: 'Select a time zone',
        value: '',
      }, {
        label: expect.stringMatching(/(US\/Pacific \(GMT-08\)|US\/Pacific \(GMT-07\))/),
        value: 'America/Los_Angeles',
      }]);

      expect((templates.ERK_UPF.systemInformation as FormTemplateSection).fields.timeZone.options).toEqual(expected);
      expect((templates.ERK_UPF.systemInformation as FormTemplateSection).fields.timeZone.initial).toEqual(
        timezones[0].value,
      );
    });

    it('should set the contacts', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a contact',
        value: '',
      }, {
        label: 'Name 1',
        value: 'id1',
      }, {
        label: 'Name 2',
        value: 'id2',
      }];

      expect((templates.ERK_UPF.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.ERK_SMF.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.ERK_PGW.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.ALU_MME.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.ALU_DSS.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.MV_SMSIWF.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
      expect((templates.MVNR_PGW.systemInformation as FormTemplateSection).fields.contact.options).toEqual(expected);
    });

    it('should set the network options for fields - cdnetid, netid and ownnetid present on any AF template', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expectedDefault = [
        { label: 'networkId1 - 12.12.12.12:1000', value: 'id1' },
        { label: 'networkId2 - [cdef:cdef:cdef:cdef::cdef]:2000', value: 'id2' },
      ];

      const expectedNameAndIP = [
        { label: 'networkId1 - 12.12.12.12', value: 'id1' },
        { label: 'networkId2 - [cdef:cdef:cdef:cdef::cdef]', value: 'id2' },
      ];

      const expectedNameIPAndCustomPortMVNRPGW1 = [
        { label: 'networkId1 - 12.12.12.12:21990', value: 'id1' },
        { label: 'networkId2 - [cdef:cdef:cdef:cdef::cdef]:21990', value: 'id2' },
      ];

      const expectedNameIPAndCustomPortMVNRPGW2 = [
        { label: 'Select a Network ID', value: '' },
        { label: 'networkId1 - 12.12.12.12:55131', value: 'id1' },
        { label: 'networkId2 - [cdef:cdef:cdef:cdef::cdef]:55131', value: 'id2' },
      ];

      const expectedNameIPAndPortIncrementMVNRPGW = [
        { label: 'networkId1 - 12.12.12.12:1130', value: 'id1' },
        { label: 'networkId2 - [cdef:cdef:cdef:cdef::cdef]:2130', value: 'id2' },
      ];

      expect((templates.ERK_SMF.provisioningInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedDefault);

      expect((templates.NSN_CSCF.provisioningInterfaces as FormTemplateSection).fields.cdnetid.options)
        .toEqual(expectedNameAndIP);
      expect((templates.NSN_CSCF.dataInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);

      expect((templates.ALU_MME.provisioningInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);
      expect((templates.ALU_MME.dataInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);

      expect((templates.ALU_DSS.provisioningInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);
      expect((templates.ALU_DSS.dataInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);

      expect((templates.MV_SMSIWF.provisioningInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);
      expect((templates.MV_SMSIWF.dataInterfaces as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameAndIP);

      expect((templates.MVNR_PGW.provisioningInterfaces_directFromAF as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedDefault);
      expect((templates.MVNR_PGW.dataInterfaces_directFromAF as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameIPAndCustomPortMVNRPGW1);
      expect((templates.MVNR_PGW.contentInterfaces_directFromAF as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameIPAndCustomPortMVNRPGW2);
      expect((templates.MVNR_PGW.contentInterfaces_trafficManager as FormTemplateSection).fields.ownnetid.options)
        .toEqual(expectedNameIPAndPortIncrementMVNRPGW);
    });

    it('should set the security info for field - secinfoid present on any AF template', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'None',
        value: 'Tk9ORQ',
      }, {
        label: 'HTTPS_Server_Sim',
        value: 'SFRUUFNfU2VydmVyX1NpbQ',
      }, {
        label: 'TLS12_CLIENT',
        value: 'VExTMTJfQ0xJRU5U',
      }, {
        label: 'TLS_Server_Simpl',
        value: 'VExTX0NsaWVudF9TaW1wbA',
      }, {
        label: 'TLSv12_Server',
        value: 'VExTdjEyX1NlcnZlcg',
      }];
      const expectedWithEmpty = cloneDeep(expected);
      expectedWithEmpty[0].value = '';

      expect((templates.ERK_SMF.provisioningInterfaces as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expected);
      expect((templates.NSN_CSCF.provisioningInterfaces as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expectedWithEmpty);
      expect((templates.MV_SMSIWF.provisioningInterfaces as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expected);
      expect((templates.MV_SMSIWF.dataInterfaces as FormTemplateSection).fields.secinfoid.options)
        .toEqual(expected);
    });

    it('should set the ssh security infos for field - sshInfoId present on any AF template', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      const expected = [{
        label: 'SSHInfo1',
        value: 'U1NISW5mbzE',
      }];

      expect((templates.NKSR.provisioningInterfaces as FormTemplateSection).fields.sshInfoId.options)
        .toEqual(expected);
    });

    it('should populate version dropdown present on any Af', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: '1.4',
        value: '1.4',
      }];

      expect((templates.ERK_SMF.systemInformation as FormTemplateSection).fields.version.options)
        .toEqual(expected);
      expect((templates.NSN_CSCF.systemInformation as FormTemplateSection).fields.version.options)
        .toEqual([]);
    });

    it('should populate model initial value present on any Af', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      expect((templates.ALU_DSS.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('SGS-IWF');
      expect((templates.NSNHLR_SDM_PRGW.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('SDM_PRGW');
      expect((templates.ERK_VMME.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('MULTIPLE_X1');
      expect((templates.SONUS_SBC.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('SONUS_SBC');
      expect((templates.NKSR.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('NOKIASR7750');

      expect((templates.NSN_CSCF.systemInformation as FormTemplateSection).fields.model.initial)
        .toEqual('');
    });

    it('should set the initial value from process options', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      expect((templates.MVNR_PGW.dataInterfaces_directFromAF as FormTemplateSection).fields.maxx2connsperaf.initial)
        .toEqual('2');
      expect((templates.MVNR_PGW.contentInterfaces_directFromAF as FormTemplateSection).fields.transport.initial)
        .toEqual('UDP');
      expect((templates.ERK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.processOptionSecInfo.initial)
        .toEqual('SIMPLESERVER1');
    });

    it('should set the OwnIPAddr to XDP IP address as options if optionsBasedOn !== null', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      const expectedIPAdressList = [
        { label: 'Select a XDP IP Address', value: '' },
        { label: '49.49.49.45', value: '49.49.49.45' },
        { label: '110.110.110.110', value: '110.110.110.110' },
      ];

      expect((templates.ACME_X123.contentInterfaces as FormTemplateSection).fields.ownIPAddr.options)
        .toEqual(expectedIPAdressList);
    });

    it('should set the ccnodename options', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      const expectedOptions = [
        { label: 'Select a Node', value: '' },
        { label: 'CCPAGNODE', value: 'CCPAGNODE' },
      ];

      expect((templates.ERK_SMF.provisioningInterfaces as FormTemplateSection).fields.ccnodename.options)
        .toEqual(expectedOptions);
    });

    it('should set the mdf2NodeName options', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      const expectedOptions = [
        { label: 'Select a Node', value: '' },
        { label: 'MDF2NODE', value: 'MDF2NODE' },
      ];

      expect((templates.ERK_SMF.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName.options)
        .toEqual(expectedOptions);
      expect((templates.ERK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName.options)
        .toEqual(expectedOptions);
      expect((templates.NOK_5GUDM.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName.options)
        .toEqual(expectedOptions);
      expect((templates.NSN_SDM.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName.options)
        .toEqual(expectedOptions);
    });

    it('should set the mdf3NodeName options', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      const expectedOptions = [
        { label: 'Select a Node', value: '' },
        { label: 'MDF3NODE', value: 'MDF3NODE' },
      ];

      expect((templates.ERK_SMF.provisioningInterfaces as FormTemplateSection).fields.mdf3NodeName.options)
        .toEqual(expectedOptions);
    });

    it('should set the http version', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'HTTP/1.1',
        value: 'HTTP/1.1',
      }, {
        label: 'HTTP/2',
        value: 'HTTP/2',
      }];

      expect((templates.ERK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);
      expect((templates.MVNR_5GSMSC.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);
      expect((templates.NOK_5GSMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);
      expect((templates.NOK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);
      expect((templates.NOK_5GSMSF.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);
      expect((templates.NOK_5GUDM.provisioningInterfaces as FormTemplateSection).fields.httpversion.options)
        .toEqual(expected);

      expect((templates.NOK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/1.1');
      expect((templates.MVNR_5GSMSC.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/1.1');
      expect((templates.ERK_5GAMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/2');
      expect((templates.NOK_5GSMF.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/2');
      expect((templates.NOK_5GSMSF.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/2');
      expect((templates.NOK_5GUDM.provisioningInterfaces as FormTemplateSection).fields.httpversion.initial)
        .toEqual('HTTP/2');
    });
  });

  describe('adaptTemplate5GFields()', () => {
    it('should return undefined if the template is not defined', () => {
      expect(adaptTemplate5GFields(undefined, true)).toBeUndefined();
    });

    it('should return the template as is if 5G is enabled', () => {
      expect(adaptTemplate5GFields(accessFunctionTemplates.ERK_5GAMF, true)).toEqual(
        accessFunctionTemplates.ERK_5GAMF,
      );
    });

    it('should return the template as is if no 5g field in the template', () => {
      const template = accessFunctionTemplates.NKSR;
      expect(adaptTemplate5GFields(accessFunctionTemplates.NKSR, false)).toEqual({
        ...template,
        formId: expect.any(String),
      });
    });

    it('should return the template as is if provisioning interface is not defined', () => {
      const template = accessFunctionTemplates.GPVRADIUS;
      expect(adaptTemplate5GFields(accessFunctionTemplates.GPVRADIUS, false)).toEqual({
        ...template,
        formId: expect.any(String),
      });
    });

    it('should return the ERK_SMF template adapted removing the 5G fields', () => {
      const template = accessFunctionTemplates.ERK_SMF;
      expect(adaptTemplate5GFields(template, false)).toEqual({
        ...template,
        formId: expect.any(String),
        provisioningInterfaces: {
          ...(template.provisioningInterfaces as FormTemplateSection),
          metadata: {
            ...(template.provisioningInterfaces as FormTemplateSection).metadata,
            layout: {
              ...(template.provisioningInterfaces as FormTemplateSection).metadata.layout,
              rows: [
                [
                  'id',
                  'name',
                ],
                [
                  'destIPAddr',
                  'destPort',
                  'secinfoid',
                  'ownnetid',
                ],
                [
                  'reqState',
                  '$empty',
                  '$empty',
                  '$empty',
                ],
                [
                  'state',
                  '$empty',
                  '$empty',
                  '$empty',
                ],
              ],
            },
          },
          fields: {
            ...(template.provisioningInterfaces as FormTemplateSection).fields,
            ccnodename: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.ccnodename,
              hide: 'true',
            },
            mdf2NodeName: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName,
              hide: 'true',
            },
            mdf3NodeName: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf3NodeName,
              hide: 'true',
            },
          },
        },
      });
    });

    it('should return the ERK_5GAMF template adapted removing the 5G fields', () => {
      const template = accessFunctionTemplates.ERK_5GAMF;
      expect(adaptTemplate5GFields(template, false)).toEqual({
        ...template,
        formId: expect.any(String),
        provisioningInterfaces: {
          ...(template.provisioningInterfaces as FormTemplateSection),
          metadata: {
            ...(template.provisioningInterfaces as FormTemplateSection).metadata,
            layout: {
              ...(template.provisioningInterfaces as FormTemplateSection).metadata.layout,
              rows: [
                [
                  'id',
                  'ifid',
                  'name',
                  'destIPAddr',
                  'role',
                ],
                [
                  'uri',
                  'processOptionSecInfo',
                  'ownnetid',
                  'reqState',
                ],
                [
                  '$empty',
                  '$empty',
                  '$empty',
                  'state',
                ],
              ],
            },
          },
          fields: {
            ...(template.provisioningInterfaces as FormTemplateSection).fields,
            mdf2NodeName: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName,
              hide: 'true',
            },
          },
        },
      });
    });

    it('should return the NOK_5GUDM template adapted removing the 5G fields', () => {
      const template = accessFunctionTemplates.NOK_5GUDM;
      expect(adaptTemplate5GFields(template, false)).toEqual({
        ...template,
        formId: expect.any(String),
        provisioningInterfaces: {
          ...(template.provisioningInterfaces as FormTemplateSection),
          metadata: {
            ...(template.provisioningInterfaces as FormTemplateSection).metadata,
            layout: {
              ...(template.provisioningInterfaces as FormTemplateSection).metadata.layout,
              rows: [
                [
                  'id',
                  'ifid',
                  'name',
                  'destIPAddr',
                  'role',
                ],
                [
                  'uri',
                  'processOptionSecInfo',
                  'ownnetid',
                  'reqState',
                ],
                [
                  '$empty',
                  '$empty',
                  '$empty',
                  'state',
                ],
              ],
            },
          },
          fields: {
            ...(template.provisioningInterfaces as FormTemplateSection).fields,
            mdf2NodeName: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName,
              hide: 'true',
            },
          },
        },
      });
    });

    it('should return the NSN_SDM template adapted removing the 5H fields', () => {
      const template = accessFunctionTemplates.NSN_SDM;
      expect(adaptTemplate5GFields(template, false)).toEqual({
        ...template,
        formId: expect.any(String),
        provisioningInterfaces: {
          ...(template.provisioningInterfaces as FormTemplateSection),
          metadata: {
            ...(template.provisioningInterfaces as FormTemplateSection).metadata,
            layout: {
              ...(template.provisioningInterfaces as FormTemplateSection).metadata.layout,
              rows: [
                [
                  'id',
                  'ifid',
                  'name',
                  '$empty',
                ],
                [
                  'uri',
                  'secinfoid',
                  'ownnetid',
                  'cdnetid',
                ],
                [
                  'cdport',
                  'reqState',
                  '$empty',
                  '$empty',
                ],
                [
                  '$empty',
                  'state',
                  '$empty',
                  '$empty',
                ],
              ],
            },
          },
          fields: {
            ...(template.provisioningInterfaces as FormTemplateSection).fields,
            mdf2NodeName: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName,
              hide: 'true',
            },
          },
        },
      });
    });
  });

  describe('removeFieldFromLayoutRows()', () => {
    it('should return the row as is if it is empty', () => {
      expect(removeFieldFromLayoutRows([], 'field')).toEqual([]);
    });

    it('should return the row as is if it is empty', () => {
      const rows = [[]];
      expect(removeFieldFromLayoutRows(rows, 'field')).toEqual(rows);
    });

    it('should return the row as is if the field is not defined', () => {
      const rows = [['field1', 'field2', 'field3']];
      expect(removeFieldFromLayoutRows(rows, 'field')).toEqual(rows);
    });

    it('should remove the field and add an $empty to the end of the column section', () => {
      const rows = [['field1', 'field2', 'field3', 'field4'], ['field5', 'field6', 'reqState', '$empty']];
      expect(removeFieldFromLayoutRows(rows, 'field6')).toEqual([
        ['field1', 'field2', 'field3', 'field4'],
        ['field5', 'reqState', '$empty', '$empty'],
      ]);
    });

    it('should remove the field, move the other ones and add an $empty to the end of the column section', () => {
      const rows = [['field1', 'field2', 'field3', 'field4'], ['field5', 'field6', 'reqState', '$empty']];
      expect(removeFieldFromLayoutRows(rows, 'field3')).toEqual([
        ['field1', 'field2', 'field4', 'field5'],
        ['field6', 'reqState', '$empty', '$empty'],
      ]);
    });

    it('should remove the row when moving the fields', () => {
      const rows = [['field1', 'field2', 'field3', 'field4'], ['reqState', '$empty', '$empty', '$empty']];
      expect(removeFieldFromLayoutRows(rows, 'field3')).toEqual([
        ['field1', 'field2', 'field4', 'reqState'],
      ]);
    });

    it('should remove the field, move the other ones and fix the state position', () => {
      const rows = [
        ['field1', 'field2', 'field3', 'field4'],
        ['field5', 'field6', 'reqState', '$empty'],
        ['$empty', '$empty', 'state', '$empty'],
      ];
      expect(removeFieldFromLayoutRows(rows, 'field6')).toEqual([
        ['field1', 'field2', 'field3', 'field4'],
        ['field5', 'reqState', '$empty', '$empty'],
        ['$empty', 'state', '$empty', '$empty'],
      ]);
    });

    it('should remove the field, move the other ones, fix the state position and remove a row', () => {
      const rows = [
        ['field1', 'field2', 'field3', 'field4'],
        ['reqState', '$empty', '$empty', '$empty'],
        ['state', '$empty', '$empty', '$empty'],
      ];
      expect(removeFieldFromLayoutRows(rows, 'field4')).toEqual([
        ['field1', 'field2', 'field3', 'reqState'],
        ['$empty', '$empty', '$empty', 'state'],
      ]);
    });
  });

  describe('adaptTemplateVisibilityMode()', () => {
    describe('ERK_UPF', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          accessFunctionTemplates,
          countries,
          timezones,
          contacts,
          networks,
          mockSecurityInfos,
          mockSshSecurityInfos,
          mockXtps,
          mockXdps,
          mockProcessOptions,
          mockSupportedFeatures.config.accessFunctions,
          mockNodes,
        );
        template = templates.ERK_UPF;
      });

      it('should return undefined if template is not defined', () => {
        expect(adaptTemplateVisibilityMode(undefined, 'create')).toEqual(undefined);
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return the template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
        });
      });

      it('should return the template adapted to view mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'view')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              id: {
                ...(template.general as FormTemplateSection).fields.id,
                readOnly: true,
              },
              liInterface: {
                ...(template.general as FormTemplateSection).fields.liInterface,
                readOnly: true,
              },
              model: {
                ...(template.general as FormTemplateSection).fields.model,
                readOnly: true,
              },
              preprovisioningLead: {
                ...(template.general as FormTemplateSection).fields.preprovisioningLead,
                readOnly: true,
              },
              serialNumber: {
                ...(template.general as FormTemplateSection).fields.serialNumber,
                readOnly: true,
              },
              version: {
                ...(template.general as FormTemplateSection).fields.version,
                readOnly: true,
              },
              type: {
                ...(template.general as FormTemplateSection).fields.type,
                readOnly: true,
              },
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.general as FormTemplateSection).fields.name,
                readOnly: true,
              },
              description: {
                ...(template.general as FormTemplateSection).fields.description,
                readOnly: true,
              },
              insideNat: {
                ...(template.general as FormTemplateSection).fields.insideNat,
                readOnly: true,
              },
            },
          },
          systemInformation: {
            ...(template.systemInformation as FormTemplateSection),
            fields: {
              ...(template.systemInformation as FormTemplateSection).fields,
              timeZone: {
                ...(template.systemInformation as FormTemplateSection).fields.timeZone,
                readOnly: true,
              },
              contact: {
                ...(template.systemInformation as FormTemplateSection).fields.contact,
                readOnly: true,
              },
              country: {
                ...(template.systemInformation as FormTemplateSection).fields.country,
                readOnly: true,
              },
              stateName: {
                ...(template.systemInformation as FormTemplateSection).fields.stateName,
                readOnly: true,
              },
              city: {
                ...(template.systemInformation as FormTemplateSection).fields.city,
                readOnly: true,
              },
            },
          },
          contentInterfaces: {
            ...(template.contentInterfaces as FormTemplateSection),
            fields: {
              ...(template.contentInterfaces as FormTemplateSection).fields,
              moduleName: {
                ...(template.contentInterfaces as FormTemplateSection).fields.moduleName,
                readOnly: true,
              },
            },
          },
        });
      });

      it('should return the template adapted to copy mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'copy')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
        });
      });
    });

    describe('ERK_SMF', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          accessFunctionTemplates,
          countries,
          timezones,
          contacts,
          networks,
          mockSecurityInfos,
          mockSshSecurityInfos,
          mockXtps,
          mockXdps,
          mockProcessOptions,
          mockSupportedFeatures.config.accessFunctions,
          mockNodes,
        );
        template = templates.ERK_SMF;
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return the template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
          provisioningInterfaces: {
            ...(template.provisioningInterfaces as FormTemplateSection),
            fields: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields,
              state: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });

      it('should return the template adapted to view mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'view')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              id: {
                ...(template.general as FormTemplateSection).fields.id,
                readOnly: true,
              },
              liInterface: {
                ...(template.general as FormTemplateSection).fields.liInterface,
                readOnly: true,
              },
              model: {
                ...(template.general as FormTemplateSection).fields.model,
                readOnly: true,
              },
              preprovisioningLead: {
                ...(template.general as FormTemplateSection).fields.preprovisioningLead,
                readOnly: true,
              },
              serialNumber: {
                ...(template.general as FormTemplateSection).fields.serialNumber,
                readOnly: true,
              },
              type: {
                ...(template.general as FormTemplateSection).fields.type,
                readOnly: true,
              },
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              name: {
                ...(template.general as FormTemplateSection).fields.name,
                readOnly: true,
              },
              description: {
                ...(template.general as FormTemplateSection).fields.description,
                readOnly: true,
              },
              insideNat: {
                ...(template.general as FormTemplateSection).fields.insideNat,
                readOnly: true,
              },
            },
          },
          systemInformation: {
            ...(template.systemInformation as FormTemplateSection),
            fields: {
              ...(template.systemInformation as FormTemplateSection).fields,
              timeZone: {
                ...(template.systemInformation as FormTemplateSection).fields.timeZone,
                readOnly: true,
              },
              contact: {
                ...(template.systemInformation as FormTemplateSection).fields.contact,
                readOnly: true,
              },
              country: {
                ...(template.systemInformation as FormTemplateSection).fields.country,
                readOnly: true,
              },
              stateName: {
                ...(template.systemInformation as FormTemplateSection).fields.stateName,
                readOnly: true,
              },
              city: {
                ...(template.systemInformation as FormTemplateSection).fields.city,
                readOnly: true,
              },
              version: {
                ...(template.systemInformation as FormTemplateSection).fields.version,
                readOnly: true,
              },
            },
          },
          provisioningInterfaces: {
            ...(template.provisioningInterfaces as FormTemplateSection),
            fields: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields,
              id: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.id,
                readOnly: true,
              },
              name: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.name,
                readOnly: true,
              },
              destIPAddr: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.destIPAddr,
                readOnly: true,
              },
              destPort: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.destPort,
                readOnly: true,
              },
              reqState: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.reqState,
                readOnly: true,
              },
              state: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.state,
                readOnly: true,
                hide: 'false',
              },
              ownnetid: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.ownnetid,
                readOnly: true,
              },
              secinfoid: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.secinfoid,
                readOnly: true,
              },
              dscp: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.dscp,
                readOnly: true,
              },
              ccnodename: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.ccnodename,
                readOnly: true,
              },
              mdf2NodeName: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf2NodeName,
                readOnly: true,
              },
              mdf3NodeName: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.mdf3NodeName,
                readOnly: true,
              },
            },
          },
          dataInterfaces: {
            ...(template.dataInterfaces as FormTemplateSection),
            fields: {
              ...(template.dataInterfaces as FormTemplateSection).fields,
              moduleName: {
                ...(template.dataInterfaces as FormTemplateSection
                ).fields.moduleName,
                readOnly: true,
              },
              afWhitelist: {
                ...(template.dataInterfaces as FormTemplateSection
                ).fields.afWhitelist,
                readOnly: true,
              },
            },
          },
          contentInterfaces: {
            ...(template.contentInterfaces as FormTemplateSection),
            fields: {
              ...(template.contentInterfaces as FormTemplateSection).fields,
              moduleName: {
                ...(template.contentInterfaces as FormTemplateSection
                ).fields.moduleName,
                readOnly: true,
              },
              afWhitelist: {
                ...(template.contentInterfaces as FormTemplateSection
                ).fields.afWhitelist,
                readOnly: true,
              },
            },
          },
        });
      });

      it('should return the template adapted to copy mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'copy')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
        });
      });
    });

    describe('ALU_DSS', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          accessFunctionTemplates,
          countries,
          timezones,
          contacts,
          networks,
          mockSecurityInfos,
          mockSshSecurityInfos,
          mockXtps,
          mockXdps,
          mockProcessOptions,
          mockSupportedFeatures.config.accessFunctions,
          mockNodes,
        );
        template = templates.ALU_DSS;
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return the template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
          systemInformation: {
            ...(template.systemInformation as FormTemplateSection),
            fields: {
              ...(template.systemInformation as FormTemplateSection).fields,
              model: {
                ...(template.systemInformation as FormTemplateSection).fields.model,
                readOnly: true,
              },
            },
          },
          provisioningInterfaces: {
            ...(template.provisioningInterfaces as FormTemplateSection),
            fields: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields,
              state: {
                ...(template.provisioningInterfaces as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          dataInterfaces: {
            ...(template.dataInterfaces as FormTemplateSection),
            fields: {
              ...(template.dataInterfaces as FormTemplateSection).fields,
              state: {
                ...(template.dataInterfaces as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });

      it('should return the template adapted to copy mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'copy')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
        });
      });
    });

    describe('MVNR_PGW', () => {
      let template: FormTemplate;

      beforeEach(() => {
        const templates = adaptTemplateOptions(
          accessFunctionTemplates,
          countries,
          timezones,
          contacts,
          networks,
          mockSecurityInfos,
          mockSshSecurityInfos,
          mockXtps,
          mockXdps,
          mockProcessOptions,
          mockSupportedFeatures.config.accessFunctions,
          mockNodes,
        );
        template = templates.MVNR_PGW;
      });

      it('should return template adapted to create mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'create')).toEqual(template);
      });

      it('should return the template adapted to edit mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'edit')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
              contentDeliveryVia: {
                ...(template.general as FormTemplateSection).fields.contentDeliveryVia,
                readOnly: true,
              },
            },
          },
          provisioningInterfaces_directFromAF: {
            ...(template.provisioningInterfaces_directFromAF as FormTemplateSection),
            fields: {
              ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields,
              state: {
                ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          dataInterfaces_directFromAF: {
            ...(template.dataInterfaces_directFromAF as FormTemplateSection),
            fields: {
              ...(template.dataInterfaces_directFromAF as FormTemplateSection).fields,
              state: {
                ...(template.dataInterfaces_directFromAF as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          contentInterfaces_directFromAF: {
            ...(template.contentInterfaces_directFromAF as FormTemplateSection),
            fields: {
              ...(template.contentInterfaces_directFromAF as FormTemplateSection).fields,
              state: {
                ...(template.contentInterfaces_directFromAF as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
          contentInterfaces_trafficManager: {
            ...(template.contentInterfaces_trafficManager as FormTemplateSection),
            fields: {
              ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields,
              lbid: {
                ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.lbid,
                readOnly: true,
              },
              state: {
                ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.state,
                hide: 'false',
              },
            },
          },
        });
      });

      it('should return the template adapted to copy mode', () => {
        expect(adaptTemplateVisibilityMode(template, 'copy')).toEqual({
          ...template,
          general: {
            ...(template.general as FormTemplateSection),
            fields: {
              ...(template.general as FormTemplateSection).fields,
              tag: {
                ...(template.general as FormTemplateSection).fields.tag,
                readOnly: true,
              },
            },
          },
        });
      });
    });
  });

  describe('adaptLocationField()', () => {
    it('should set the state name', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a state/province',
        value: '',
      }, {
        label: 'California',
        value: 'California',
      }];

      let adaptedTemplate = adaptLocationField(templates.ERK_UPF, ['California'], 'stateName');
      expect(((adaptedTemplate as FormTemplate).systemInformation as FormTemplateSection).fields.stateName.options)
        .toEqual(expected);

      adaptedTemplate = adaptLocationField(templates.ERK_SMF, ['California'], 'stateName');
      expect(((adaptedTemplate as FormTemplate).systemInformation as FormTemplateSection).fields.stateName.options)
        .toEqual(expected);
    });

    it('should set the city', () => {
      const templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
      const expected = [{
        label: 'Select a city',
        value: '',
      }, {
        label: 'San Francisco',
        value: 'San Francisco',
      }];

      let adaptedTemplate = adaptLocationField(templates.ERK_UPF, ['San Francisco'], 'city');
      expect(((adaptedTemplate as FormTemplate).systemInformation as FormTemplateSection).fields.city.options)
        .toEqual(expected);

      adaptedTemplate = adaptLocationField(templates.ERK_SMF, ['San Francisco'], 'city');
      expect(((adaptedTemplate as FormTemplate).systemInformation as FormTemplateSection).fields.city.options)
        .toEqual(expected);
    });
  });

  describe('adaptSecurityInfoField()', () => {
    let templates: { [key: string]: FormTemplate };
    let securityInfoFilter: SecurityInfoFilterByInterface;

    beforeEach(() => {
      templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
    });

    it('should adapt the security info options', () => {
      securityInfoFilter = ACCESS_FUNCTION_SECURITY_INFO_FILTER.ERK_SMF;
      const adaptedTemplate = adaptSecurityInfoField(templates.ERK_SMF, mockSecurityInfos, securityInfoFilter);
      const formSection = ((adaptedTemplate as FormTemplate).provisioningInterfaces as FormTemplateSection);
      expect(formSection.fields.secinfoid.options).toEqual([
        {
          label: 'None',
          value: 'Tk9ORQ',
        },
        {
          label: 'TLS12_CLIENT',
          value: 'VExTMTJfQ0xJRU5U',
        },
      ]);
    });

    it('should adapt the security info options for NSNHLR', () => {
      securityInfoFilter = {};
      const adaptedTemplate = adaptSecurityInfoField(templates.NSNHLR_SDM_PRGW, mockSecurityInfos, securityInfoFilter);
      const formSection = ((adaptedTemplate as FormTemplate).provisioningInterfaces as FormTemplateSection);
      expect(formSection.fields.secinfoid.options).toEqual([
        { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
        { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
        { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
        { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
      ]);
    });

    it('should adapt the security info options for MV_SMSIWF', () => {
      securityInfoFilter = ACCESS_FUNCTION_SECURITY_INFO_FILTER.MV_SMSIWF;
      const template = templates.MV_SMSIWF;
      const adaptedTemplate = adaptSecurityInfoField(template, mockSecurityInfos, securityInfoFilter);
      expect(adaptedTemplate).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        provisioningInterfaces: {
          ...(template.provisioningInterfaces as FormTemplateSection),
          fields: {
            ...(template.provisioningInterfaces as FormTemplateSection).fields,
            secinfoid: {
              ...(template.provisioningInterfaces as FormTemplateSection).fields.secinfoid,
              options: [
                { label: 'None', value: 'Tk9ORQ' },
                { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
              ],
            },
          },
        },
        dataInterfaces: {
          ...(template.dataInterfaces as FormTemplateSection),
          fields: {
            ...(template.dataInterfaces as FormTemplateSection).fields,
            secinfoid: {
              ...(template.dataInterfaces as FormTemplateSection).fields.secinfoid,
              options: [
                { label: 'None', value: 'Tk9ORQ' },
                { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
              ],
            },
          },
        },
      });
    });

    it('should not adapt if security info is not available', () => {
      securityInfoFilter = {};
      expect(adaptSecurityInfoField(templates.ERK_UPF, mockSecurityInfos, securityInfoFilter)).toEqual({
        ...templates.ERK_UPF,
        formId: expect.stringMatching(/\d+/),
        key: expect.any(String),
      });
    });
  });

  describe('adaptNetworkIdField()', () => {
    let formTemplateSection: FormTemplateSection;
    let interfaceFormData: Partial<AccessFunctionInterfaceFormData>;

    beforeEach(() => {
      formTemplateSection = {
        metadata: {},
        fields: {
          ownnetid: {
            label: 'Network ID',
            type: 'select',
            initial: '',
            options: [{
              label: 'net id 1',
              value: 'id1',
            }],
            validation: {
              required: true,
            },
          },
          cdnetid: {
            type: 'select',
            options: [{
              label: 'net id 2',
              value: 'id2',
            }],
          },
        },
      };
      interfaceFormData = {
        ownnetid: 'id1',
      };
    });

    it('should return the template section as is if netid is not defined', () => {
      interfaceFormData = {};
      expect(adaptNetworkIdField(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
      });
    });

    it('should return the template section as is if options is not defined', () => {
      formTemplateSection.fields.ownnetid.options = undefined;
      expect(adaptNetworkIdField(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
      });
    });

    it('should return the template section as is if network id found in the options', () => {
      expect(adaptNetworkIdField(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
      });
    });

    it('should return the template section with the network id not found option for ownnetid', () => {
      interfaceFormData.ownnetid = 'idx';
      expect(adaptNetworkIdField(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
        fields: {
          ...formTemplateSection.fields,
          ownnetid: {
            ...formTemplateSection.fields.ownnetid,
            options: [{
              label: 'net id 1',
              value: 'id1',
            }, {
              label: 'Network ID not found',
              value: 'idx',
            }],
          },
        },
      });
    });

    it('should return the template section with the network id not found option for both ownnetid and cdnetid', () => {
      interfaceFormData.ownnetid = 'idx';
      interfaceFormData.cdnetid = 'idy';
      expect(adaptNetworkIdField(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
        fields: {
          ...formTemplateSection.fields,
          ownnetid: {
            ...formTemplateSection.fields.ownnetid,
            options: [{
              label: 'net id 1',
              value: 'id1',
            }, {
              label: 'Network ID not found',
              value: 'idx',
            }],
          },
          cdnetid: {
            ...formTemplateSection.fields.cdnetid,
            options: [{
              label: 'net id 2',
              value: 'id2',
            }, {
              label: 'Network ID not found',
              value: 'idy',
            }],
          },
        },
      });
    });
  });

  describe('adaptXDPIpAddress()', () => {
    let formTemplateSection: FormTemplateSection;
    let interfaceFormData: Partial<AccessFunctionInterfaceFormData>;

    beforeEach(() => {
      formTemplateSection = {
        metadata: {},
        fields: {
          ownIPAddr: {
            label: 'XDP IP Address',
            type: 'select',
            initial: '',
            options: [
              {
                label: 'Select a XDP IP Address',
                value: '',
              },
              {
                label: '1.1.1.1',
                value: '1.1.1.1',
              },
            ],
          },
        },
      };
      interfaceFormData = {
        ownIPAddr: '1.1.2.2',
      };
    });

    it('should return the template section as is if ownIPAddr does not exists', () => {
      delete formTemplateSection.fields.ownIPAddr;
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual(formTemplateSection);
    });

    it('should return the template section as is if ownIPAddr type is not select', () => {
      formTemplateSection.fields.ownIPAddr.type = 'text';
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual(formTemplateSection);
    });

    it('should return the template section as is if ownIPAddr not defined in data', () => {
      interfaceFormData.ownIPAddr = undefined;
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual(formTemplateSection);

      interfaceFormData.ownIPAddr = '';
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual(formTemplateSection);
    });

    it('should return the template section as if if the option is already available', () => {
      formTemplateSection.fields.ownIPAddr.options?.push({ label: '1.1.2.2', value: '1.1.2.2' });
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual(formTemplateSection);
    });

    it('should return the template section with the new option if it is not available', () => {
      expect(adaptXDPIpAddress(formTemplateSection, interfaceFormData)).toEqual({
        ...formTemplateSection,
        fields: {
          ...formTemplateSection.fields,
          ownIPAddr: {
            ...formTemplateSection.fields.ownIPAddr,
            options: [{
              label: 'Select a XDP IP Address',
              value: '',
            }, {
              label: '1.1.1.1',
              value: '1.1.1.1',
            }, {
              label: '1.1.2.2',
              value: '1.1.2.2',
            }],
          },
        },
      });
    });
  });

  describe('adaptTemplateLabelsForDeployment()', () => {
    let templates: { [key: string]: FormTemplate };

    beforeEach(() => {
      templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
    });

    it('should return undefined if template is not defined', () => {
      expect(adaptTemplateLabelsForDeployment(undefined, 'andromeda')).toBeUndefined();
    });

    it('should return the template as is if deployment is andromeda', () => {
      expect(adaptTemplateLabelsForDeployment(templates.ERK_SMF, 'andromeda')).toEqual(templates.ERK_SMF);
    });

    it('should return the template labels adapted if deployment is endeavor', () => {
      const template = templates.ERK_SMF;
      expect(adaptTemplateLabelsForDeployment(template, 'endeavor')).toEqual({
        ...template,
        formId: expect.any(String),
        systemInformation: {
          ...(template.systemInformation as FormTemplateSection),
          fields: {
            ...(template.systemInformation as FormTemplateSection).fields,
            city: {
              ...(template.systemInformation as FormTemplateSection).fields.city,
              label: 'Town/City',
              options: [{
                label: 'Select a Town/City',
                value: '',
              }],
            },
            stateName: {
              ...(template.systemInformation as FormTemplateSection).fields.stateName,
              label: 'County/Region',
              options: [{
                label: 'Select a County/Region',
                value: '',
              }],
            },
          },
        },
      });
    });
  });

  describe('adaptTemplateInterfaceFields()', () => {
    let accessFunction: AccessFunctionFormData;
    let templates: { [key: string]: FormTemplate };

    beforeEach(() => {
      templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      accessFunction = {
        id: 'id1',
        name: 'AF name',
        tag: 'MVNR_PGW',
        type: 'MVNR_PGW',
        timeZone: 'America/Los_Angeles',
        contentDeliveryVia: 'directFromAF',
        provisioningInterfaces: [{
          id: 'id2',
          name: '',
          dscp: '0',
          ownnetid: '',
          litype: 'INI1',
          reqState: 'ACTIVE',
          state: 'ACTIVE',
          appProtocol: 'MVRN_UAG_PGW',
          keepaliveinterval: '60',
          litargetnamespace: 'http://mavenir.net/li/',
          secinfoid: 'Tk9ORQ',
          transport: 'HTTPS',
          uri: 'https://domain.com',
          version: '1.0',
        }],
        dataInterfaces: [],
        contentInterfaces: [],
      };
    });

    it('should return undefined if template is not defined', () => {
      expect(adaptTemplateInterfaceFields(undefined, accessFunction)).toBeUndefined();
    });

    it('should set the secinfo fields as expected for MVNR_PGW when uri is HTTPS', () => {
      const template = templates.MVNR_PGW;
      expect(adaptTemplateInterfaceFields(template, accessFunction)).toEqual({
        ...template,
        provisioningInterfaces_directFromAF: {
          ...(template.provisioningInterfaces_directFromAF as FormTemplateSection),
          fields: {
            ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields,
            secinfoid: {
              ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields.secinfoid,
              initial: '',
              options: [
                { label: 'None', value: '' },
                { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
                { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
                { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
                { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
              ],
              validation: {
                required: true,
              },
            },
          },
        },
      });
    });

    it('should set the secinfo fields as expected for MVNR_PGW when uri is HTTP', () => {
      const template = templates.MVNR_PGW;
      accessFunction.provisioningInterfaces = [{
        id: 'id2',
        name: '',
        secinfoid: '',
        transport: 'HTTP',
        uri: 'http://domain.com',
      }];

      expect(adaptTemplateInterfaceFields(template, accessFunction)).toEqual({
        ...template,
        provisioningInterfaces_directFromAF: {
          ...(template.provisioningInterfaces_directFromAF as FormTemplateSection),
          fields: {
            ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields,
            secinfoid: {
              ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields.secinfoid,
              initial: 'Tk9ORQ',
              options: [
                { label: 'None', value: 'Tk9ORQ' },
                { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
                { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
                { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
                { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
              ],
              validation: {
                required: false,
              },
            },
          },
        },
      });
    });

    it('should set the destPort as disabled for MVNR_PGW when maxx2conn is greater than 4', () => {
      const template = templates.MVNR_PGW;
      accessFunction.contentDeliveryVia = 'trafficManager';
      accessFunction.provisioningInterfaces = [];
      accessFunction.contentInterfaces = [{
        id: 'id2',
        name: '',
        destIPAddr: '1.1.1.1',
        destPort: '0',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        dscp: '32',
        lbid: '',
        maxx2connsperaf: '5',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }];

      expect(adaptTemplateInterfaceFields(template, accessFunction)).toEqual({
        ...template,
        contentInterfaces_trafficManager: {
          ...(template.contentInterfaces_trafficManager as FormTemplateSection),
          fields: {
            ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields,
            destPort: {
              ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.destPort,
              disabled: true,
            },
          },
        },
      });
    });

    it('should set the destPort as enabled for MVNR_PGW when maxx2conn is smaller than 4', () => {
      const template = templates.MVNR_PGW;
      accessFunction.contentDeliveryVia = 'trafficManager';
      accessFunction.provisioningInterfaces = [];
      accessFunction.contentInterfaces = [{
        id: 'id2',
        name: '',
        destIPAddr: '1.1.1.1',
        destPort: '1,2,3,4',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        dscp: '32',
        lbid: '',
        maxx2connsperaf: '4',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }];

      expect(adaptTemplateInterfaceFields(template, accessFunction)).toEqual({
        ...template,
        contentInterfaces_trafficManager: {
          ...(template.contentInterfaces_trafficManager as FormTemplateSection),
          fields: {
            ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields,
            destPort: {
              ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.destPort,
              disabled: false,
            },
          },
        },
      });
    });
  });

  describe('adaptContentDeliveryViaField()', () => {
    let templates: { [key: string]: FormTemplate };

    beforeEach(() => {
      templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
    });

    it('should return undefined if template is undefined', () => {
      expect(adaptContentDeliveryViaField(undefined, 'trafficManager')).toBeUndefined();
    });

    it('should adapt the contentDeliveryVia field', () => {
      const template = templates.MVNR_PGW;
      const adaptedTemplate = adaptContentDeliveryViaField(template, 'trafficManager');
      expect(adaptedTemplate).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        general: {
          ...(template.general as FormTemplateSection),
          fields: {
            ...(template.general as FormTemplateSection).fields,
            contentDeliveryVia: {
              ...(template.general as FormTemplateSection).fields.contentDeliveryVia,
              initial: 'trafficManager',
            },
          },
        },
      });
    });
  });

  describe('getInterfaceFormTemplateSection()', () => {
    it('should return the template section', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSection(template, 'provisioningInterfaces', '')).toEqual(
        template.provisioningInterfaces,
      );
    });

    it('should return the default template section if configure type is not defined', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSection(template, 'provisioningInterfaces', 'trafficManager')).toEqual(
        template.provisioningInterfaces,
      );
    });

    it('should return the template section if configure type is defined', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSection(template, 'contentInterfaces', 'trafficManager')).toEqual(
        template.contentInterfaces_trafficManager,
      );
    });
  });

  describe('getInterfaceFormTemplateSectionName()', () => {
    it('should return the interface name if configure type is not set', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSectionName(template, 'provisioningInterfaces', '')).toEqual('provisioningInterfaces');
    });

    it('should return the interface name if the form section with the configure type is not defined', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSectionName(template, 'provisioningInterfaces', 'trafficManager')).toEqual('provisioningInterfaces');
    });

    it('should return the interface name with the configure type it it exists', () => {
      const template = accessFunctionTemplates.MVNR_PGW;
      expect(getInterfaceFormTemplateSectionName(template, 'contentInterfaces', 'trafficManager')).toEqual('contentInterfaces_trafficManager');
    });
  });

  describe('adaptTemplateForInterfacesFieldUpdate()', () => {
    let accessFunction: AccessFunctionFormData;
    let accessFunctionVMME: AccessFunctionFormData;
    let templates: { [key: string]: FormTemplate };

    beforeEach(() => {
      templates = adaptTemplateOptions(
        accessFunctionTemplates,
        countries,
        timezones,
        contacts,
        networks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );

      accessFunction = {
        id: 'id1',
        name: 'AF name',
        tag: 'MVNR_PGW',
        type: 'MVNR_PGW',
        timeZone: 'America/Los_Angeles',
        contentDeliveryVia: 'directFromAF',
        provisioningInterfaces: [{
          id: 'id2',
          name: '',
          dscp: '0',
          ownnetid: '',
          litype: 'INI1',
          reqState: 'ACTIVE',
          state: 'ACTIVE',
          appProtocol: 'MVRN_UAG_PGW',
          keepaliveinterval: '60',
          litargetnamespace: 'http://mavenir.net/li/',
          secinfoid: 'Tk9ORQ',
          transport: 'HTTPS',
          uri: 'https://domain.com',
          version: '1.0',
        }],
        dataInterfaces: [],
        contentInterfaces: [],
      };

      accessFunctionVMME = {
        id: 'id1',
        name: 'AF name',
        tag: 'ERK_VMME',
        type: 'ERK_VMME',
        timeZone: 'America/Los_Angeles',
        contentDeliveryVia: 'directFromAF',
        provisioningInterfaces: [],
        dataInterfaces: [{
          id: 'id2',
          name: '',
          dscp: '0',
          ownnetid: '',
          secinfoid: 'Tk9ORQ',
          reqState: 'ACTIVE',
          state: 'ACTIVE',
          tpkt: true,
          x224headerpresent: true,
        }],
        contentInterfaces: [],
      };
    });

    it('should return undefined if template is not defined', () => {
      expect(adaptTemplateForInterfacesFieldUpdate('provisioningInterfaces', accessFunction, undefined, 'uri', 'http://domain.com')).toBeUndefined();
    });

    it('should update the form id when ccnodename is changed', () => {
      const template = templates.ERK_SMF;
      expect(adaptTemplateForInterfacesFieldUpdate('provisioningInterfaces', accessFunction, template, 'ccnodename', '')).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
      });
    });

    it('should set the secinfoid fields as expected for MVNR_PGW when uri is HTTPS', () => {
      const template = templates.MVNR_PGW;
      expect(adaptTemplateForInterfacesFieldUpdate('provisioningInterfaces', accessFunction, template, 'uri', 'https://domain.com')).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        provisioningInterfaces_directFromAF: {
          ...(template.provisioningInterfaces_directFromAF as FormTemplateSection),
          fields: {
            ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields,
            secinfoid: {
              ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields.secinfoid,
              initial: '',
              options: [
                { label: 'None', value: '' },
                { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
                { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
                { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
                { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
              ],
              validation: {
                required: true,
              },
            },
          },
        },
      });
    });

    it('should set the secinfo fields as expected for MVNR_PGW when uri is HTTP', () => {
      const template = templates.MVNR_PGW;
      accessFunction.provisioningInterfaces = [{
        id: 'id2',
        name: '',
        secinfoid: '',
        transport: 'HTTP',
        uri: 'http://domain.com',
      }];

      expect(adaptTemplateForInterfacesFieldUpdate('provisioningInterfaces', accessFunction, template, 'uri', 'http://domain.com')).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        provisioningInterfaces_directFromAF: {
          ...(template.provisioningInterfaces_directFromAF as FormTemplateSection),
          fields: {
            ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields,
            secinfoid: {
              ...(template.provisioningInterfaces_directFromAF as FormTemplateSection).fields.secinfoid,
              initial: 'Tk9ORQ',
              options: [
                { label: 'None', value: 'Tk9ORQ' },
                { label: 'HTTPS_Server_Sim', value: 'SFRUUFNfU2VydmVyX1NpbQ' },
                { label: 'TLS12_CLIENT', value: 'VExTMTJfQ0xJRU5U' },
                { label: 'TLS_Server_Simpl', value: 'VExTX0NsaWVudF9TaW1wbA' },
                { label: 'TLSv12_Server', value: 'VExTdjEyX1NlcnZlcg' },
              ],
              validation: {
                required: false,
              },
            },
          },
        },
      });
    });

    it('should hide dscp and secinfoid field and display sharedsecret and version field for Sonus SBC when deliver over packet cable', () => {
      const template = templates.SONUS_SBC;
      const clonedAcessFunction = cloneDeep(accessFunction);
      clonedAcessFunction.tag = 'SONUS_SBC';
      clonedAcessFunction.contentDeliveryVia = 'directFromAF';

      const adaptedDataInterfaceSectionTemplate: FormTemplateSection = (adaptTemplateForInterfacesFieldUpdate('dataInterfaces', clonedAcessFunction, template, 'deliveryVia', 'PacketCable') as FormTemplate)
        .dataInterfaces_directFromAF as FormTemplateSection;

      expect(adaptedDataInterfaceSectionTemplate.fields.dscp.hide).toEqual('true');
      expect(adaptedDataInterfaceSectionTemplate.fields.secinfoid.hide).toEqual('true');
      expect(adaptedDataInterfaceSectionTemplate.fields.sharedsecret.hide).toEqual('false');
      expect(adaptedDataInterfaceSectionTemplate.fields.version.hide).toEqual('false');
    });

    it('should hide sharedsecret and version field and display dscp and secinfoid field for Sonus SBC when deliver over VoIP', () => {
      const template = templates.SONUS_SBC;
      const clonedAcessFunction = cloneDeep(accessFunction);
      clonedAcessFunction.tag = 'SONUS_SBC';
      clonedAcessFunction.contentDeliveryVia = 'directFromAF';

      const adaptedDataInterfaceSectionTemplate: FormTemplateSection = (adaptTemplateForInterfacesFieldUpdate('dataInterfaces', clonedAcessFunction, template, 'deliveryVia', 'VoIP') as FormTemplate)
        .dataInterfaces_directFromAF as FormTemplateSection;

      expect(adaptedDataInterfaceSectionTemplate.fields.dscp.hide).toEqual('false');
      expect(adaptedDataInterfaceSectionTemplate.fields.secinfoid.hide).toEqual('false');
      expect(adaptedDataInterfaceSectionTemplate.fields.sharedsecret.hide).toEqual('true');
      expect(adaptedDataInterfaceSectionTemplate.fields.version.hide).toEqual('true');
    });

    it('should set the destPort as disabled for MVNR_PGW when maxx2conn is greater than 4', () => {
      const template = templates.MVNR_PGW;
      accessFunction.contentDeliveryVia = 'trafficManager';
      accessFunction.provisioningInterfaces = [];
      accessFunction.contentInterfaces = [{
        id: 'id2',
        name: '',
        destIPAddr: '1.1.1.1',
        destPort: '0',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        dscp: '32',
        lbid: '',
        maxx2connsperaf: '4',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }];

      expect(adaptTemplateForInterfacesFieldUpdate('contentInterfaces', accessFunction, template, 'maxx2connsperaf', '5')).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        contentInterfaces_trafficManager: {
          ...(template.contentInterfaces_trafficManager as FormTemplateSection),
          fields: {
            ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields,
            destPort: {
              ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.destPort,
              disabled: true,
            },
          },
        },
      });
    });

    it('should set the destPort as enabled for MVNR_PGW when maxx2conn is smaller than 4', () => {
      const template = templates.MVNR_PGW;
      accessFunction.contentDeliveryVia = 'trafficManager';
      accessFunction.provisioningInterfaces = [];
      accessFunction.contentInterfaces = [{
        id: 'id2',
        name: '',
        destIPAddr: '1.1.1.1',
        destPort: '0',
        ownnetid: '',
        reqState: 'ACTIVE',
        state: '',
        dscp: '32',
        lbid: '',
        maxx2connsperaf: '5',
        traceLevel: '0',
        type: 'MVNRPGW_TCP',
      }];

      expect(adaptTemplateForInterfacesFieldUpdate('contentInterfaces', accessFunction, template, 'maxx2connsperaf', '4')).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        contentInterfaces_trafficManager: {
          ...(template.contentInterfaces_trafficManager as FormTemplateSection),
          fields: {
            ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields,
            destPort: {
              ...(template.contentInterfaces_trafficManager as FormTemplateSection).fields.destPort,
              disabled: false,
            },
          },
        },
      });
    });

    it('should set the x224headerpresent as read-only for ERK_VMME when tpkt is changed to false', () => {
      const template = templates.ERK_VMME;

      expect(adaptTemplateForInterfacesFieldUpdate('dataInterfaces', accessFunctionVMME, template, 'tpkt', false)).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        dataInterfaces: {
          ...(template.dataInterfaces as FormTemplateSection),
          fields: {
            ...(template.dataInterfaces as FormTemplateSection).fields,
            x224headerpresent: {
              ...(template.dataInterfaces as FormTemplateSection).fields.x224headerpresent,
              readOnly: true,
            },
          },
        },
      });
    });

    it('should set the x224headerpresent as editable for ERK_VMME when tpkt is changed to true', () => {
      const template = templates.ERK_VMME;

      expect(adaptTemplateForInterfacesFieldUpdate('dataInterfaces', accessFunctionVMME, template, 'tpkt', true)).toEqual({
        ...template,
        formId: expect.stringMatching(/\d+/),
        dataInterfaces: {
          ...(template.dataInterfaces as FormTemplateSection),
          fields: {
            ...(template.dataInterfaces as FormTemplateSection).fields,
            x224headerpresent: {
              ...(template.dataInterfaces as FormTemplateSection).fields.x224headerpresent,
              readOnly: false,
            },
          },
        },
      });
    });
  });
});
