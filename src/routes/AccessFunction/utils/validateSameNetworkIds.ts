import { AccessFunctionInterfaceFormData } from 'data/accessFunction/accessFunction.types';

const validateSameNetworkIds = (
  interfaces: AccessFunctionInterfaceFormData[],
): boolean => {

  if (interfaces.length <= 1) {
    return true;
  }

  const [firstInterfaceEntry, ...restInterfaceEntries] = interfaces;

  const valid = restInterfaceEntries.every((entry) => entry.ownnetid === firstInterfaceEntry.ownnetid);

  return valid;
};

export default validateSameNetworkIds;
