import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { AccessFunctionMenuActions } from '../AccessFunction.types';

export default function isActionSupportedForAccessFunction(
  type: AccessFunctionMenuActions.Refresh | AccessFunctionMenuActions.Reset,
  accessFunctionTag: string,
  enabledAccessFunctions: SupportedFeaturesAccessFunctions,
): boolean {
  return enabledAccessFunctions[accessFunctionTag]?.supportedActions?.includes(type) ?? false;
}
