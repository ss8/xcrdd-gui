import { AccessFunctionInterfaceFormData } from 'data/accessFunction/accessFunction.types';
import validateUniqueProperty from './validateUniqueProperty';

describe('validateUniqueProperty()', () => {
  let interfaces: AccessFunctionInterfaceFormData[];

  beforeEach(() => {
    interfaces = [{
      id: 'id1',
      name: 'name1',
      ipduid: 'ipduid1',
    }, {
      id: 'id2',
      name: 'name2',
      ipduid: 'ipduid2',
    }, {
      id: 'id3',
      name: 'name3',
      ipduid: 'ipduid3',
    }];
  });

  it('should return true if interfaces are empty', () => {
    expect(validateUniqueProperty('ipduid', [])).toBe(true);
  });

  it('should return true if there is only one interface', () => {
    expect(validateUniqueProperty('ipduid', [interfaces[0]])).toBe(true);
  });

  it('should return true if field is unique among all interfaces', () => {
    expect(validateUniqueProperty('ipduid', interfaces)).toBe(true);
  });

  it('should return false if field is not unique among all interfaces', () => {
    interfaces[2].ipduid = interfaces[1].ipduid;
    expect(validateUniqueProperty('ipduid', interfaces)).toBe(false);
  });
});
