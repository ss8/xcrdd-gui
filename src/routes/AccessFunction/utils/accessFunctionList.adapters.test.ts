import store from 'data/store';
import { getEnabledAccessFunctionFormTemplatesByKey } from 'data/template/template.selectors';
import { FormTemplateMapByKey } from 'data/template/template.types';
import * as warrantActions from 'xcipio/warrants/warrant-actions';
import * as distributorConfigurationTypes from 'data/distributorConfiguration/distributorConfiguration.types';
import * as distributorConfigurationSelectors from 'data/distributorConfiguration/distributorConfiguration.selectors';
import { mockAccessFunctions, mockAccessFunctionsRowData } from '__test__/mockAccessFunctionRowData';
import { mockContacts } from '__test__/mockContacts';
import { mockNetworks } from '__test__/mockNetworks';
import { mockProcessOptions } from '__test__/mockProcessOptions';
import { mockSecurityInfos } from '__test__/mockSecurityInfos';
import { mockSshSecurityInfos } from '__test__/mockSshSecurityInfos';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import { mockTemplates } from '__test__/mockTemplates';
import { mockTimeZones } from '__test__/mockTimeZones';
import { mockXdps } from '__test__/mockXdps';
import { mockXtps } from '__test__/mockXtps';
import { mockNodes } from '__test__/mockNodes';
import { buildAxiosServerResponse } from '__test__/utils';
import { mockDistributorsConfiguration } from '__test__/mockDistributorsConfiguration';
import { adaptAccessFunctionsGridRowData } from './accessFunctionList.adapters';
import { adaptTemplateOptions } from './accessFunctionTemplate.adapter';

describe('Access Functions Adapters', () => {
  describe('adaptAccessFunctionsGridRowData()', () => {
    let availableTemplates: FormTemplateMapByKey;

    beforeEach(() => {
      store.dispatch({
        type: warrantActions.GET_WARRANT_FORM_CONFIG_FULFILLED,
        payload: buildAxiosServerResponse([mockSupportedFeatures]),
      });

      store.dispatch({
        type: warrantActions.GET_ALL_TEMPLATES_FULFILLED,
        payload: buildAxiosServerResponse(mockTemplates),
      });

      store.dispatch({
        type: distributorConfigurationTypes.GET_DISTRIBUTOR_CONFIGURATIONS_FULFILLED,
        payload: buildAxiosServerResponse(mockDistributorsConfiguration),
      });

      const accessFunctionTemplates = getEnabledAccessFunctionFormTemplatesByKey(store.getState());

      availableTemplates = adaptTemplateOptions(
        accessFunctionTemplates,
        [],
        mockTimeZones,
        mockContacts,
        mockNetworks,
        mockSecurityInfos,
        mockSshSecurityInfos,
        mockXtps,
        mockXdps,
        mockProcessOptions,
        mockSupportedFeatures.config.accessFunctions,
        mockNodes,
      );
    });

    it('should adapt Access Functions to fit the expected Row Data', () => {
      const input = adaptAccessFunctionsGridRowData(
        mockAccessFunctions,
        mockSupportedFeatures.config.accessFunctions,
        availableTemplates,
        mockNetworks,
        mockXdps,
        distributorConfigurationSelectors.getDistributorConfigurationsByAccessFunctionType(store.getState()),
      );

      expect(input).toEqual(mockAccessFunctionsRowData);
    });
  });
});
