import { AccessFunctionFormData, AccessFunction } from 'data/accessFunction/accessFunction.types';
import getAccessFunctionDataToSubmit from './getAccessFunctionDataToSubmit';

describe('getAccessFunctionDataToSubmit()', () => {
  let accessFunctionFormData: AccessFunctionFormData;
  let originalAccessFunction: AccessFunction;
  let expectedAccessFunction: AccessFunction;
  const userId = 'userId1';
  const userName = 'userName1';

  beforeEach(() => {
    accessFunctionFormData = {
      id: 'id1',
      name: 'new name',
      contact: 'Y29udGFjdDE=',
      insideNat: false,
      timeZone: 'timeZone1',
      type: 'type1',
      tag: '',
      options: [],
      provisioningInterfaces: [],
      contentInterfaces: [],
      dataInterfaces: [],
    };

    originalAccessFunction = {
      id: 'id1',
      name: 'name1',
      contact: { id: '', name: '' },
      insideNat: false,
      timeZone: 'timeZone1',
      type: 'type1',
      tag: '',
      options: [],
      provisioningInterfaces: [],
      contentInterfaces: [],
      dataInterfaces: [],
    };

    expectedAccessFunction = {
      id: 'id1',
      name: 'new name',
      contact: { id: 'Y29udGFjdDE=', name: 'contact1' },
      insideNat: false,
      timeZone: 'timeZone1',
      type: 'type1',
      tag: '',
      options: [],
      provisioningInterfaces: [],
      contentInterfaces: [],
      dataInterfaces: [],
    };
  });

  describe('when operation is CREATE', () => {
    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getAccessFunctionDataToSubmit(
          accessFunctionFormData,
          null,
          false,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedAccessFunction,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getAccessFunctionDataToSubmit(
          accessFunctionFormData,
          null,
          true,
          userId,
          userName,
          'CREATE',
        ),
      ).toEqual({
        data: expectedAccessFunction,
        auditDetails: {
          actionType: 'Access Function Add',
          auditEnabled: true,
          fieldDetails: '[{"field":"id","value":"id1"},{"field":"name","value":"new name"},{"field":"contact","value":{"id":"Y29udGFjdDE=","name":"contact1"}},{"field":"insideNat","value":false},{"field":"timeZone","value":"timeZone1"},{"field":"type","value":"type1"},{"field":"tag","value":""},{"field":"options","value":[]},{"field":"provisioningInterfaces","value":[]},{"field":"contentInterfaces","value":[]},{"field":"dataInterfaces","value":[]}]',
          recordName: 'new name',
          service: 'AFs',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });

  describe('when operation is UPDATE', () => {
    beforeEach(() => {
      expectedAccessFunction.operation = 'UPDATE';
    });

    it('should return the data to submit when audit is not enabled', () => {
      expect(
        getAccessFunctionDataToSubmit(
          accessFunctionFormData,
          originalAccessFunction,
          false,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedAccessFunction,
      });
    });

    it('should return the data to submit when audit is enabled', () => {
      expect(
        getAccessFunctionDataToSubmit(
          accessFunctionFormData,
          originalAccessFunction,
          true,
          userId,
          userName,
          'UPDATE',
        ),
      ).toEqual({
        data: expectedAccessFunction,
        auditDetails: {
          actionType: 'Access Function Edit',
          auditEnabled: true,
          fieldDetails: '[{"field":"name","before":"name1","after":"new name"},{"field":"contact","before":{"id":"","name":""},"after":{"id":"Y29udGFjdDE=","name":"contact1"}}]',
          recordName: 'new name',
          service: 'AFs',
          userId: 'userId1',
          userName: 'userName1',
        },
      });
    });
  });
});
