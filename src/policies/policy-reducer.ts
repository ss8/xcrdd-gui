import {
  GET_PASSWORD_POLICY_FULFILLED,
  LOGIN_REJECTED,
  PASSWORD_POLICY_CLEAR,
} from './policy-actions';

const defaultState = {
  passwordPolicy: {},
  error: {
    errorCode: null,
    errorMessage: null,
  },
};

const moduleReducer = (state = defaultState, action: any) => {
  if (/POLICY.*_REJECTED/.test(action.type)) {
    let errorCode = action.payload.response.status;
    let errorMessage = action.payload.response.statusText;
    if (action.payload.response.data && action.payload.response.data !== ''
        && action.payload.response.data.error) {
      errorCode = action.payload.response.data.error.status;
      errorMessage = action.payload.response.data.error.message;
    }
    return { ...state, error: { errorMessage, errorCode } };
  }
  switch (action.type) {
    case GET_PASSWORD_POLICY_FULFILLED:
      return (action.payload.data.data.length > 0) ?
        ({ ...state, passwordPolicy: action.payload.data.data[0] }) : state;
    case LOGIN_REJECTED:
      const errorState = action.payload.message && action.payload.message.match(/\d+/) ? action.payload.message.match(/\d+/)[0] : '';
      if (errorState === '489') {
        return { ...state, passwordPolicy: action.payload.response.data };
      } return state;
    case PASSWORD_POLICY_CLEAR:
      return { ...state, passwordPolicy: {} };
    default:
      return state;
  }
};
export default moduleReducer;
