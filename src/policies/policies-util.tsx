/**
 * This function returns a description of the password policy
 * @param passwordPolicy
 */
// eslint-disable-next-line import/prefer-default-export
export const toPasswordPolicyDescription = (passwordPolicy: any) => {
  if (!passwordPolicy) return '';
  // Description sample: Password must have a minimum of X characters with at least X uppercase,
  // lowercase, number and symbols (), and cannot be the same as the last X passwords.
  let passwordPolicyDescription = 'Password must have';
  passwordPolicyDescription = `${passwordPolicyDescription} a minimum of ${passwordPolicy.minPasswordLength} characters`;

  if (passwordPolicy.minCharsEachType) {
    passwordPolicyDescription = `${passwordPolicyDescription} with at least`;
  }

  const characterTypes = [];
  if (passwordPolicy.passwordUppercase && passwordPolicy.passwordUppercase.trim().length > 0) {
    characterTypes.push((passwordPolicy.minCharsEachType > 1) ? ` ${passwordPolicy.minCharsEachType} uppercases` : ` ${passwordPolicy.minCharsEachType} uppercase`);
  }
  if (passwordPolicy.passwordLowercase && passwordPolicy.passwordLowercase.trim().length > 0) {
    characterTypes.push((passwordPolicy.minCharsEachType > 1) ? ` ${passwordPolicy.minCharsEachType} lowercases` : ` ${passwordPolicy.minCharsEachType} lowercase`);
  }
  if (passwordPolicy.passwordNumber && passwordPolicy.passwordNumber.trim().length > 0) {
    characterTypes.push((passwordPolicy.minCharsEachType > 1) ? ` ${passwordPolicy.minCharsEachType} numbers` : ` ${passwordPolicy.minCharsEachType} number`);
  }
  if (passwordPolicy.passwordSymbol && passwordPolicy.passwordSymbol.trim().length > 0) {
    characterTypes.push((passwordPolicy.minCharsEachType > 1) ? ` ${passwordPolicy.minCharsEachType} symbols (${passwordPolicy.passwordSymbol})` : ` ${passwordPolicy.minCharsEachType} symbol (${passwordPolicy.passwordSymbol})`);
  }

  if (characterTypes.length > 1) {
    passwordPolicyDescription = `${passwordPolicyDescription}${characterTypes.slice(0, characterTypes.length - 1).join()} and${characterTypes[characterTypes.length - 1]}`;
  } else {
    passwordPolicyDescription = `${passwordPolicyDescription}${characterTypes[0]}`;
  }

  if (passwordPolicy.passwordHistory && passwordPolicy.passwordHistory > 0) {
    passwordPolicyDescription = `${passwordPolicyDescription}, and cannot be the same as the last ${passwordPolicy.passwordHistory} ${passwordPolicy.passwordHistory > 1 ? 'passwords' : 'password'}`;
  }
  passwordPolicyDescription = `${passwordPolicyDescription}.`;
  return passwordPolicyDescription;
};
