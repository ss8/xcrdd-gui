import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  getPasswordPolicy,
} from './policy-actions';
import ModuleRoutes from './policy-routes';

const Policies = (props: any) => (
  <ModuleRoutes {...props} />
);

const mapStateToProps = (state: any) => ({
  passwordPolicy: state.passwordPolicy,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getPasswordPolicy,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Policies));
