/* eslint-disable max-len */
import Http from '../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_PASSWORD_POLICY = 'GET_PASSWORD_POLICY';
export const LOGIN_REJECTED = 'LOGIN_REJECTED';
export const GET_PASSWORD_POLICY_FULFILLED = 'GET_PASSWORD_POLICY_FULFILLED';
export const PASSWORD_POLICY_CLEAR = 'PASSWORD_POLICY_CLEAR';

// export const GET_ALL_POLICIES = 'GET_ALL_POLICIES';
// export const GET_POLICY_BY_ID = 'GET_POLICY_BY_ID';
// export const UPDATE_POLICY = 'UPDATE_POLICY';
// export const PATCH_POLICY = 'PATCH_POLICY';
// export const DELETE_POLICY = 'DELETE_POLICY';
// export const CREATE_POLICY = 'CREATE_POLICY';
// export const GET_ALL_POLICIES_FULFILLED = 'GET_ALL_POLICIES_FULFILLED';
// export const GET_POLICY_BY_ID_FULFILLED = 'GET_POLICY_BY_ID_FULFILLED';
// export const UPDATE_POLICY_FULFILLED = 'UPDATE_POLICY_FULFILLED';
// export const PATCH_POLICY_FULFILLED = 'PATCH_POLICY_FULFILLED';
// export const DELETE_POLICY_FULFILLED = 'DELETE_POLICY_FULFILLED';
// export const CREATE_POLICY_FULFILLED = 'CREATE_POLICY_FULFILLED';

// action creators
export const getPasswordPolicy = () => ({ type: GET_PASSWORD_POLICY, payload: http.get('/policy') });
export const getPasswordAndGoToPage = (history: any, path: any) => {
  return async (dispatch: any) => {
    await dispatch(getPasswordPolicy());
    history.push(path);
  };
};
export const clearPasswordPolicy = () => ({ type: PASSWORD_POLICY_CLEAR });
// export const getAllPolicies = () => ({ type: GET_ALL_POLICIES, payload: http.get('/surveillance') });
// export const getPolicyById = (id: number) => ({ type: GET_POLICY_BY_ID, payload: http.get(`/policies/${id}`) });
// export const createPolicy = (body: any) => ({ type: CREATE_POLICY, payload: http.post('/policies', body) });
// export const updatePolicy = (id: number, body: any) => ({ type: UPDATE_POLICY, payload: http.put(`/policies/${id}`, body) });
// export const patchPolicy = (id: number, body: any) => ({ type: PATCH_POLICY, payload: http.patch(`/policies/${id}`, body) });
// export const deletePolicy = (id: number) => ({ type: DELETE_POLICY, payload: http.delete(`/policies/${id}`) });
