import React from 'react';

export default function IconAccessFunction(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" style={{ fill: 'currentColor' }}>
        <path fillRule="evenodd" clipRule="evenodd" d="M15 0H1C0.45 0 0 0.192857 0 0.428571V5.57143C0 5.80714 0.45 6 1 6H15C15.55 6 16 5.80714 16 5.57143V0.428571C16 0.192857 15.55 0 15 0ZM8.5 3H13.5C13.78 3 14 2.78 14 2.5C14 2.22 13.78 2 13.5 2H8.5C8.22 2 8 2.22 8 2.5C8 2.78 8.22 3 8.5 3ZM2 2.5C2 2.22386 2.22386 2 2.5 2H3.5C3.77614 2 4 2.22386 4 2.5C4 2.77614 3.77614 3 3.5 3H2.5C2.22386 3 2 2.77614 2 2.5Z" />
        <path fillRule="evenodd" clipRule="evenodd" d="M7.5 6L7.5 14C7.5 14.56 7.72 15 8 15C8.28 15 8.5 14.56 8.5 14L8.5 6C8.5 5.44 8.28 5 8 5C7.72 5 7.5 5.44 7.5 6Z" />
        <path fillRule="evenodd" clipRule="evenodd" d="M11.5 13H4.5C4.225 13 4 13.0964 4 13.2143V15.7857C4 15.9036 4.225 16 4.5 16H11.5C11.775 16 12 15.9036 12 15.7857V13.2143C12 13.0964 11.775 13 11.5 13Z" />
        <line x1="0.5" y1="14.5" x2="15.5" y2="14.5" strokeLinecap="round" strokeLinejoin="round" />
        <path fillRule="evenodd" clipRule="evenodd" d="M9.75 9H6.25C6.1125 9 6 9.16071 6 9.35714V13.6429C6 13.8393 6.1125 14 6.25 14H9.75C9.8875 14 10 13.8393 10 13.6429V9.35714C10 9.16071 9.8875 9 9.75 9Z" />
      </svg>
    </span>
  );
}
