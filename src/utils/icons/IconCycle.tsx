import React from 'react';

export default function IconCycle(): JSX.Element {
  return (
    <svg width="16" height="17" viewBox="0 0 16 17" xmlns="http://www.w3.org/2000/svg" style={{ fill: 'currentColor' }}>
      <path fillRule="evenodd" clipRule="evenodd" d="M8 13.81C10.7614 13.81 13 11.5714 13 8.80998C13 6.04856 10.7614 3.80998 8 3.80998C5.23858 3.80998 3 6.04856 3 8.80998C3 11.5714 5.23858 13.81 8 13.81Z" />
    </svg>
  );
}
