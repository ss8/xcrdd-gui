import React from 'react';

export default function IconConnectionStatus(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M15 7.5H13.54L11.84 4.95L11.82 4.96C11.64 4.69 11.35 4.5 11 4.5C10.57 4.5 10.21 4.77 10.07 5.15H10.06L8.37 9.66L6.99 1.34H6.97C6.89 0.87 6.5 0.5 6 0.5C5.59 0.5 5.23 0.75 5.08 1.11L2.34 7.5H1C0.45 7.5 0 7.95 0 8.5C0 9.05 0.45 9.5 1 9.5H3C3.41 9.5 3.77 9.25 3.92 8.89L5.57 5.03L7.01 13.66H7.03C7.11 14.13 7.5 14.5 8 14.5C8.43 14.5 8.79 14.23 8.93 13.85H8.94L11.25 7.68L12.17 9.06L12.19 9.05C12.36 9.31 12.65 9.5 13 9.5H15C15.55 9.5 16 9.05 16 8.5C16 7.95 15.55 7.5 15 7.5Z" fill="#BFCCD6" />
      </svg>
    </span>
  );
}
