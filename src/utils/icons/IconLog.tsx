import React from 'react';

export default function IconLog(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M3.5 6.5H10.5C10.78 6.5 11 6.28 11 6C11 5.72 10.78 5.5 10.5 5.5H3.5C3.22 5.5 3 5.72 3 6C3 6.28 3.22 6.5 3.5 6.5ZM15 0.5H1C0.45 0.5 0 0.95 0 1.5V13.5C0 14.05 0.45 14.5 1 14.5H15C15.55 14.5 16 14.05 16 13.5V1.5C16 0.95 15.55 0.5 15 0.5ZM14 12.5H2V4.5H14V12.5ZM3.5 8.5H7.5C7.78 8.5 8 8.28 8 8C8 7.72 7.78 7.5 7.5 7.5H3.5C3.22 7.5 3 7.72 3 8C3 8.28 3.22 8.5 3.5 8.5ZM3.5 10.5H8.5C8.78 10.5 9 10.28 9 10C9 9.72 8.78 9.5 8.5 9.5H3.5C3.22 9.5 3 9.72 3 10C3 10.28 3.22 10.5 3.5 10.5Z" fill="#BFCCD6" />
      </svg>
    </span>
  );
}
