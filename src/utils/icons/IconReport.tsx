import React from 'react';

export default function IconReport(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" style={{ fill: 'currentColor' }}>
        <path fillRule="evenodd" clipRule="evenodd" d="M9 0H3C2.45 0 2 0.45 2 1V15C2 15.55 2.45 16 3 16H13C13.55 16 14 15.55 14 15V5L9 0ZM12 14H4V2H8V6H12V14Z" />
        <path fillRule="evenodd" clipRule="evenodd" d="M10.5714 11H5.42857C5.19286 11 5 11.225 5 11.5C5 11.775 5.19286 12 5.42857 12H10.5714C10.8071 12 11 11.775 11 11.5C11 11.225 10.8071 11 10.5714 11ZM5.375 8H10.625C10.8313 8 11 7.775 11 7.5C11 7.225 10.8313 7 10.625 7H5.375C5.16875 7 5 7.225 5 7.5C5 7.775 5.16875 8 5.375 8ZM10.625 9H5.375C5.16875 9 5 9.225 5 9.5C5 9.775 5.16875 10 5.375 10H10.625C10.8313 10 11 9.775 11 9.5C11 9.225 10.8313 9 10.625 9Z" />
      </svg>
    </span>
  );
}
