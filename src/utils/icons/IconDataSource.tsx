import React from 'react';

export default function IconDataSource(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="13" height="17" viewBox="0 0 13 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M6.5 4.5C9.81 4.5 12.5 3.6 12.5 2.5C12.5 1.4 9.81 0.5 6.5 0.5C3.18 0.5 0.5 1.4 0.5 2.5C0.5 3.6 3.18 4.5 6.5 4.5ZM0.5 4.02V8.5C0.5 9.6 3.19 10.5 6.5 10.5C9.81 10.5 12.5 9.6 12.5 8.5V4.02C11.28 4.9 9.06 5.5 6.5 5.5C3.94 5.5 1.72 4.9 0.5 4.02ZM0.5 10.02V14.5C0.5 15.6 3.19 16.5 6.5 16.5C9.81 16.5 12.5 15.6 12.5 14.5V10.02C11.28 10.9 9.06 11.5 6.5 11.5C3.94 11.5 1.72 10.9 0.5 10.02Z" fill="#BFCCD6" />
      </svg>
    </span>
  );
}
