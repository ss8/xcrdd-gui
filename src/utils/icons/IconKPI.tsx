import React from 'react';

export default function IconKPI(): JSX.Element {
  return (
    <span className="bp3-icon">
      <svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M8.5 5.5C9.05 5.5 9.5 5.05 9.5 4.5C9.5 3.95 9.05 3.5 8.5 3.5C7.95 3.5 7.5 3.95 7.5 4.5C7.5 5.05 7.95 5.5 8.5 5.5ZM4.5 9.5H5.5C6.05 9.5 6.5 9.05 6.5 8.5V6.5C6.5 5.95 6.05 5.5 5.5 5.5H4.5C3.95 5.5 3.5 5.95 3.5 6.5V8.5C3.5 9.05 3.95 9.5 4.5 9.5ZM11.5 3.5C12.05 3.5 12.5 3.05 12.5 2.5V1.5C12.5 0.95 12.05 0.5 11.5 0.5C10.95 0.5 10.5 0.95 10.5 1.5V2.5C10.5 3.05 10.95 3.5 11.5 3.5ZM15.5 0.5H14.5C13.95 0.5 13.5 0.95 13.5 1.5V8.5C13.5 9.05 13.95 9.5 14.5 9.5H15.5C16.05 9.5 16.5 9.05 16.5 8.5V1.5C16.5 0.95 16.05 0.5 15.5 0.5ZM15.5 10.5H2.5V1.5C2.5 0.95 2.05 0.5 1.5 0.5C0.95 0.5 0.5 0.95 0.5 1.5V11.5C0.5 12.05 0.95 12.5 1.5 12.5H15.5C16.05 12.5 16.5 12.05 16.5 11.5C16.5 10.95 16.05 10.5 15.5 10.5Z" fill="#BFCCD6" />
      </svg>
    </span>
  );
}
