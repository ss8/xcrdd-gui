import yesNoValueFormatter from './yesNoValueFormatter';

describe('yesNoValueFormatter()', () => {
  it('should return yes for true', () => {
    expect(yesNoValueFormatter({ value: true })).toEqual('yes');
  });

  it('should return no otherwise', () => {
    expect(yesNoValueFormatter({ value: false })).toEqual('no');
    expect(yesNoValueFormatter({ value: 'something else' })).toEqual('no');
    expect(yesNoValueFormatter({ value: null })).toEqual('no');
  });
});
