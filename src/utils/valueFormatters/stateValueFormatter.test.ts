import stateValueFormatter from './stateValueFormatter';

describe('stateValueFormatter()', () => {
  it('should return Active for ACTIVE', () => {
    expect(stateValueFormatter({ value: 'ACTIVE' })).toEqual('Active');
  });

  it('should return Inactive for INACTIVE', () => {
    expect(stateValueFormatter({ value: 'INACTIVE' })).toEqual('Inactive');
  });

  it('should return Partially Active for PARTIALLY_ACTIVE', () => {
    expect(stateValueFormatter({ value: 'PARTIALLY_ACTIVE' })).toEqual('Partially Active');
  });

  it('should return "-" otherwise', () => {
    expect(stateValueFormatter({ value: null })).toEqual('-');
    expect(stateValueFormatter({ value: 'something else' })).toEqual('-');
  });
});
