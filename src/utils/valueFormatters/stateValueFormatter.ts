import { ValueFormatterParams } from '@ag-grid-enterprise/all-modules';

export function stateValue(value:string): string {
  if (value === 'ACTIVE') {
    return 'Active';
  }

  if (value === 'INACTIVE') {
    return 'Inactive';
  }

  if (value === 'PARTIALLY_ACTIVE') {
    return 'Partially Active';
  }

  if (value === 'FAILED') {
    return 'Failed';
  }

  return '-';
}

export default function stateValueFormatter({ value }: Partial<ValueFormatterParams>): string {
  return stateValue(value);
}
