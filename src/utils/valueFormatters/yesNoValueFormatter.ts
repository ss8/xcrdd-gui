import { ValueFormatterParams } from '@ag-grid-enterprise/all-modules';

export default function yesNoValueFormatter({ value }: Partial<ValueFormatterParams>): string {
  if (value === true) {
    return 'yes';
  }

  return 'no';
}
