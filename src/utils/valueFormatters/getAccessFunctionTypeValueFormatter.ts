import { ValueFormatterParams, ITooltipParams } from '@ag-grid-enterprise/all-modules';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';

export default function getAccessFunctionTypeValueFormatter(
  availableAccessFunctions: SupportedFeaturesAccessFunctions,
): (params: Partial<ValueFormatterParams> | Partial<ITooltipParams>) => string {
  return ({ value }: Partial<ValueFormatterParams> | Partial<ITooltipParams>): string => {
    return availableAccessFunctions[value]?.title ?? value;
  };
}
