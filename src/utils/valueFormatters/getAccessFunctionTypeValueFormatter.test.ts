import { ValueFormatterParams } from '@ag-grid-enterprise/all-modules';
import { SupportedFeaturesAccessFunctions } from 'data/supportedFeatures/supportedFeatures.types';
import { mockSupportedFeatures } from '__test__/mockSupportedFeatures';
import getAccessFunctionTypeValueFormatter from './getAccessFunctionTypeValueFormatter';

describe('getAccessFunctionTypeValueFormatter()', () => {
  let accessFunctions: SupportedFeaturesAccessFunctions;
  let params: Partial<ValueFormatterParams>;

  beforeEach(() => {
    accessFunctions = mockSupportedFeatures.config.accessFunctions;

    params = {
      value: 'ERK_UPF',
    };
  });

  describe('getAccessFunctionTypeValueFormatter()', () => {
    it('should return a value formatter function', () => {
      expect(getAccessFunctionTypeValueFormatter(accessFunctions)).toEqual(expect.any(Function));
    });

    it('should return the access function title', () => {
      const valueFormatter = getAccessFunctionTypeValueFormatter(accessFunctions);
      expect(valueFormatter(params)).toEqual('Ericsson UPF');
    });

    it('should return the value if access function does not exist', () => {
      params.value = 'SOME_AF';
      const valueFormatter = getAccessFunctionTypeValueFormatter(accessFunctions);
      expect(valueFormatter(params)).toEqual('SOME_AF');
    });
  });
});
