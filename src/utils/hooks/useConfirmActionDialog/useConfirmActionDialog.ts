import { FunctionComponent, useState } from 'react';

type SetConfirmActionDialog = (
  title: string,
  customComponent: JSX.Element,
  actionText: string,
  onSubmit?: CallableFunction,
) => void;

type ConfirmActionDialog = [
  {
    isOpen: boolean
    title: string
    customComponent: FunctionComponent
    onClose: CallableFunction
    onSubmit: CallableFunction
    actionText: string
    hideCancel: boolean
  },
  SetConfirmActionDialog
]

export default function useConfirmActionDialog(): ConfirmActionDialog {
  const [isOpen, setOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [customComponent, setCustomComponent] = useState<FunctionComponent>(() => () => null);
  const [onSubmit, setOnSubmit] = useState<CallableFunction>(() => () => null);
  const [actionText, setActionText] = useState('');
  const [isCancelHidden, setCancelHidden] = useState(false);

  function setConfirmActionDialog(
    dialogTitle: string,
    dialogComponent: JSX.Element,
    dialogActionText: string,
    submitCallback?: CallableFunction,
  ): void {
    setOpen(true);
    setTitle(dialogTitle);
    setCustomComponent(() => () => dialogComponent);
    setActionText(dialogActionText);

    if (submitCallback != null) {
      setOnSubmit(() => submitCallback);
      setCancelHidden(false);

    } else {
      setOnSubmit(() => onClose);
      setCancelHidden(true);
    }
  }

  function onClose() {
    setOpen(false);
  }

  return [
    {
      isOpen,
      title,
      onClose,
      onSubmit,
      customComponent,
      actionText,
      hideCancel: isCancelHidden,
    },
    setConfirmActionDialog,
  ];
}
