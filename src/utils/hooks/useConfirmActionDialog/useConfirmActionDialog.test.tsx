import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import useConfirmActionDialog from './useConfirmActionDialog';

describe('useConfirmActionDialog', () => {
  it('should set the default values', () => {
    const { result } = renderHook(() => useConfirmActionDialog());

    const [confirmActionDialog] = result.current;

    expect(confirmActionDialog.isOpen).toBe(false);
    expect(confirmActionDialog.title).toEqual('');
    expect(confirmActionDialog.customComponent).toEqual(expect.any(Function));
    expect(confirmActionDialog.actionText).toEqual('');
    expect(confirmActionDialog.onSubmit).toEqual(expect.any(Function));
    expect(confirmActionDialog.onClose).toEqual(expect.any(Function));
    expect(confirmActionDialog.hideCancel).toBe(false);
  });

  it('should configured the dialog to be open and show the cancel button', () => {
    const { result } = renderHook(() => useConfirmActionDialog());

    let [confirmActionDialog, setConfirmActionDialog] = result.current;

    const onSubmit = jest.fn();

    act(() => {
      setConfirmActionDialog(
        'title',
        <div>content</div>,
        'OK',
        onSubmit,
      );
    });

    [confirmActionDialog, setConfirmActionDialog] = result.current;

    expect(confirmActionDialog.isOpen).toBe(true);
    expect(confirmActionDialog.title).toEqual('title');
    expect(confirmActionDialog.customComponent).toEqual(expect.any(Function));
    expect(confirmActionDialog.actionText).toEqual('OK');
    expect(confirmActionDialog.onSubmit).toEqual(onSubmit);
    expect(confirmActionDialog.onClose).toEqual(expect.any(Function));
    expect(confirmActionDialog.hideCancel).toBe(false);
  });

  it('should configured the dialog to be open and hide the cancel button', () => {
    const { result } = renderHook(() => useConfirmActionDialog());

    let [confirmActionDialog, setConfirmActionDialog] = result.current;

    act(() => {
      setConfirmActionDialog(
        'title',
        <div>content</div>,
        'OK',
      );
    });

    [confirmActionDialog, setConfirmActionDialog] = result.current;

    expect(confirmActionDialog.isOpen).toBe(true);
    expect(confirmActionDialog.title).toEqual('title');
    expect(confirmActionDialog.customComponent).toEqual(expect.any(Function));
    expect(confirmActionDialog.actionText).toEqual('OK');
    expect(confirmActionDialog.onSubmit).toEqual(expect.any(Function));
    expect(confirmActionDialog.onClose).toEqual(expect.any(Function));
    expect(confirmActionDialog.hideCancel).toBe(true);
  });

  it('should set the dialog to close when calling the onClose callback', () => {
    const { result } = renderHook(() => useConfirmActionDialog());

    let [confirmActionDialog, setConfirmActionDialog] = result.current;

    act(() => {
      setConfirmActionDialog(
        'title',
        <div>content</div>,
        'OK',
      );
    });

    [confirmActionDialog, setConfirmActionDialog] = result.current;

    expect(confirmActionDialog.isOpen).toBe(true);

    act(() => {
      confirmActionDialog.onClose();
    });

    [confirmActionDialog, setConfirmActionDialog] = result.current;

    expect(confirmActionDialog.isOpen).toBe(false);
  });
});
