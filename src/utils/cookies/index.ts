export enum COOKIE {
  JSESSIONID = 'JSESSIONID',
  USERNAME = 'UserName',
  LOGINFROM = 'LoginFrom',
  USEPOLICY = 'UsePolicy'
}

export const getCookie = (cname: string) => {
  const name = `${cname}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
};

export const setCookie = (name: string, value: any) => {
  document.cookie = `${name}=${value};path=/`;
};

export const removeCookie = (name: string) => {
  document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/`;
};
