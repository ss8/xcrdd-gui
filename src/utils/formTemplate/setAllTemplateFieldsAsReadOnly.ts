import { FormTemplateFieldMap } from 'data/template/template.types';

const setAllTemplateFieldsAsReadOnly = (
  formTemplateFields: FormTemplateFieldMap,
): void => {
  Object.entries(formTemplateFields).forEach(([_fieldKey, fieldData]) => {
    fieldData.readOnly = true;
    fieldData.disabled = true;
  });
};

export default setAllTemplateFieldsAsReadOnly;
