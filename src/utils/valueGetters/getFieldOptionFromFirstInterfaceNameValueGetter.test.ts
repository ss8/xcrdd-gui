import { ColDef, ValueGetterParams } from '@ag-grid-community/core';
import getFieldOptionFromFirstInterfaceNameValueGetter from './getFieldOptionFromFirstInterfaceNameValueGetter';

type Option = {
  key:string,
  value:string
};

interface Children1 {
  name: string;
  options: Option[]
}

interface Children2 {
  id: string;
  options: Option[]
}

interface GenericInterface {
  name: string;
  children1: Children1[];
  children2: Children2[];
}

describe('getFieldOptionFromFirstInterfaceNameValueGetter()', () => {

  let params: Partial<ValueGetterParams>;

  let genericInterfaceData: GenericInterface;

  beforeEach(() => {
    genericInterfaceData = {
      name: 'name1',
      children1: [
        {
          name: 'name children1 1',
          options: [{ key: 'optId', value: 'id child 1.1' }, { key: 'optName', value: 'Name child 1.1' }],
        },
        {
          name: 'name children1 2',
          options: [{ key: 'optId', value: 'id child 1.1' }, { key: 'optName', value: 'Name child 1.1' }],
        },
      ],
      children2: [
        {
          id: 'name children2 1',
          options: [{ key: 'optId', value: 'id child 2.1' }, { key: 'optName', value: 'Name child 2.1' }],
        },
        {
          id: 'name children2 2',
          options: [{ key: 'optId', value: 'id child 2.1' }, { key: 'optName', value: 'Name child 2.1' }],
        },
      ],
    };

    params = {
      colDef: {
        field: 'optId',
      },
      data: genericInterfaceData,
    };
  });

  it('should return a value getter function', () => {
    expect(
      getFieldOptionFromFirstInterfaceNameValueGetter<GenericInterface, Children1>(
        'children1',
      ),
    ).toEqual(expect.any(Function));
  });

  it('should return "-" in the value getter if field not defined in the interface', () => {
    (params.colDef as ColDef).field = 'something else';
    const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<
      GenericInterface,
      Children1
    >('children1');
    expect(valueGetter(params)).toEqual('-');
  });

  it('should return the field value in the value getter - children1', () => {
    const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<
      GenericInterface,
      Children1
    >('children1');
    expect(valueGetter(params)).toEqual(genericInterfaceData.children1[0].options[0].value);
  });

  it('should return the field value in the value getter - children1', () => {
    const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<
      GenericInterface,
      Children2
    >('children1');
    expect(valueGetter(params)).toEqual(genericInterfaceData.children1[0].options[0].value);
  });

  it('should return the field value in the value getter - children2', () => {
    (params.colDef as ColDef).field = 'optName';
    const valueGetter = getFieldOptionFromFirstInterfaceNameValueGetter<
      GenericInterface,
      Children2
    >('children2');
    expect(valueGetter(params)).toEqual(genericInterfaceData.children2[0].options[1].value);
  });
});
