import { ValueGetterParams } from '@ag-grid-community/core';
import getFieldFromPropertyPathValueGetter from './getFieldFromPropertyPathValueGetter';

describe('getFieldFromPropertyPathValueGetter()', () => {
  interface NestedChild {
    name: string;
  }

  interface NestedChild2 {
    data: {
      name: string
    };
  }

  interface GenericInterface {
    name: string;
    child1: NestedChild;
    child2: NestedChild2;
  }

  let genericInterfaceData: GenericInterface;
  let params: Partial<ValueGetterParams>;
  let params2: Partial<ValueGetterParams>;
  let params3: Partial<ValueGetterParams>;

  beforeEach(() => {
    genericInterfaceData = {
      name: 'name1',
      child1: {
        name: 'name child1',
      },
      child2: {
        data: {
          name: 'name child2',
        },
      },
    };

    params = {
      colDef: {
        field: 'child1',
      },
      data: genericInterfaceData,
    };

    params2 = {
      colDef: {
        field: 'child2',
      },
      data: genericInterfaceData,
    };

    params3 = {
      colDef: {
        field: 'not_found',
      },
      data: genericInterfaceData,
    };

  });

  it('should return a value getter function', () => {
    expect(getFieldFromPropertyPathValueGetter(['name'])).toEqual(expect.any(Function));
  });

  it('should return "-" if property path is empty', () => {
    const valueGetter = getFieldFromPropertyPathValueGetter([]);
    expect(valueGetter(params)).toEqual('-');
  });

  it('should return "-"  if property path value is not defined', () => {
    const valueGetter = getFieldFromPropertyPathValueGetter(['not_found']);
    expect(valueGetter(params2)).toEqual('-');
  });

  it('should return "-" if colDef field is not defined in data param', () => {
    const valueGetter = getFieldFromPropertyPathValueGetter(['name']);
    expect(valueGetter(params3)).toEqual('-');
  });

  it('should return filed value according property path', () => {
    const valueGetter = getFieldFromPropertyPathValueGetter(['name']);
    expect(valueGetter(params)).toEqual('name child1');
  });

  it('should return filed value according property path - 2', () => {
    const valueGetter = getFieldFromPropertyPathValueGetter(['data', 'name']);
    expect(valueGetter(params2)).toEqual('name child2');
  });
});
