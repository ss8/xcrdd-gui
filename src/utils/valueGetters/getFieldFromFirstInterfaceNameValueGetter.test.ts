import { ColDef, ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import {
  DistributorConfiguration,
  DistributorConfigurationDestination,
  DistributorConfigurationListener,
} from 'data/distributorConfiguration/distributorConfiguration.types';
import getFieldFromFirstInterfaceNameValueGetter from './getFieldFromFirstInterfaceNameValueGetter';

describe('getFieldFromFirstInterfaceNameValueGetter', () => {
  describe('DistributorValueGetters scope', () => {
    let distributorConfiguration: DistributorConfiguration;
    let params: Partial<ValueGetterParams>;
    let destinations: DistributorConfigurationDestination[];

    beforeEach(() => {
      distributorConfiguration = {
        id: 'id1',
        name: 'name1',
        instanceId: '1',
        moduleName: 'moduleName1',
        afType: 'type1',
        listeners: [
          {
            id: '2',
            afProtocol: 'afProtocol',
            ipAddress: '12.12.12.12',
            keepaliveTimeout: '20',
            maxConcurrentConnection: '30',
            port: '1234',
            requiredState: 'ACTIVE',
            secInfoId: 'Tk9ORQ',
            status: 'ACTIVE',
            synchronizationHeader: 'NONE',
            numberOfParsers: '2',
            transport: 'TCP',
            afWhitelist: [
              {
                afId: 'afId1',
                afAddress: '12.12.12.12',
                afName: 'afName',
              },
            ],
          },
          {
            id: '3',
            afProtocol: 'afProtocol',
            ipAddress: '22.22.22.22',
            keepaliveTimeout: '20',
            maxConcurrentConnection: '30',
            port: '2234',
            requiredState: 'ACTIVE',
            secInfoId: 'Tk9ORQ',
            status: 'INACTIVE',
            synchronizationHeader: 'NONE',
            numberOfParsers: '2',
            transport: 'TCP',
            afWhitelist: [
              {
                afId: 'afId2',
                afAddress: '22.22.22.22',
                afName: 'afName',
              },
            ],
          },
        ],
        destinations: [],
      };

      destinations = [
        {
          id: 'id1',
          ipAddress: '100.100.100.100',
          port: '1234',
          keepaliveInterval: 'true',
          keepaliveRetries: 'true',
          numOfConnections: '100',
          requiredState: 'ACTIVE',
          secInfoId: '1234',
          transport: '1234',
          protocol: '1234',
        },
        {
          id: 'id2',
          ipAddress: '200.200.200.200',
          port: '1234',
          keepaliveInterval: 'true',
          keepaliveRetries: 'true',
          numOfConnections: '100',
          requiredState: 'ACTIVE',
          secInfoId: '1234',
          transport: '1234',
          protocol: '1234',
        },
      ];

      params = {
        colDef: {
          field: 'ipAddress',
        },
        data: distributorConfiguration,
      };
    });

    it('should return a value getter function', () => {
      expect(
        getFieldFromFirstInterfaceNameValueGetter<
          DistributorConfiguration,
          DistributorConfigurationListener
        >('listeners'),
      ).toEqual(expect.any(Function));
    });

    it('should return "-" in the value getter if no interface defined', () => {
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        DistributorConfiguration,
        DistributorConfigurationDestination
      >('destinations');
      expect(valueGetter(params)).toEqual('-');
    });

    it('should return "-" in the value getter if field not defined in the interface', () => {
      (params.colDef as ColDef).field = 'something else';
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        DistributorConfiguration,
        DistributorConfigurationListener
      >('listeners');
      expect(valueGetter(params)).toEqual('-');
    });

    it('should return the field value in the value getter - listeners', () => {
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        DistributorConfiguration,
        DistributorConfigurationListener
      >('listeners');
      expect(valueGetter(params)).toEqual('12.12.12.12');
    });

    it('should return the field value in the value getter - destinations', () => {
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        DistributorConfiguration,
        DistributorConfigurationDestination
      >('destinations');

      const paramsWithDestinations = {
        ...params,
        data: {
          ...distributorConfiguration,
          destinations,
        },
      };

      expect(valueGetter(paramsWithDestinations)).toEqual('100.100.100.100');
    });
  });

  describe('GenericInterface scope', () => {
    interface Children1 {
      name: string;
    }

    interface Children2 {
      id: string;
    }

    interface GenericInterface {
      name: string;
      children1: Children1[];
      children2: Children2[];
    }

    let genericInterfaceData: GenericInterface;
    let params: Partial<ValueGetterParams>;

    beforeEach(() => {
      genericInterfaceData = {
        name: 'name1',
        children1: [
          {
            name: 'name children1 1',
          },
          {
            name: 'name children1 2',
          },
        ],
        children2: [
          {
            id: 'name children2 1',
          },
          {
            id: 'name children2 2',
          },
        ],
      };

      params = {
        colDef: {
          field: 'name',
        },
        data: genericInterfaceData,
      };
    });

    it('should return a value getter function', () => {
      expect(
        getFieldFromFirstInterfaceNameValueGetter<GenericInterface, Children1>(
          'children1',
        ),
      ).toEqual(expect.any(Function));
    });

    it('should return "-" in the value getter if field not defined in the interface', () => {
      (params.colDef as ColDef).field = 'something else';
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        GenericInterface,
        Children1
      >('children1');
      expect(valueGetter(params)).toEqual('-');
    });

    it('should return the field value in the value getter - children1', () => {
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        GenericInterface,
        Children1
      >('children1');
      expect(valueGetter(params)).toEqual(genericInterfaceData.children1[0].name);
    });

    it('should return the field value in the value getter - children1', () => {
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        GenericInterface,
        Children1
      >('children1');
      expect(valueGetter(params)).toEqual(genericInterfaceData.children1[0].name);
    });

    it('should return the field value in the value getter - children2', () => {
      (params.colDef as ColDef).field = 'id';
      const valueGetter = getFieldFromFirstInterfaceNameValueGetter<
        GenericInterface,
        Children2
      >('children2');
      expect(valueGetter(params)).toEqual(genericInterfaceData.children2[0].id);
    });
  });
});
