import lodashGet from 'lodash.get';
import { ITooltipParams, ValueGetterParams } from '@ag-grid-community/core';

export default function getFieldFromPropertyPathValueGetter<T, U>(
  propertyPath: string[],
): (params: Partial<ValueGetterParams> | Partial<ITooltipParams>) => string {
  return ({ data, colDef }: Partial<ValueGetterParams> | Partial<ITooltipParams>): string => {
    const value = lodashGet(data[colDef.field], propertyPath);

    if (value == null) {
      return '-';
    }

    return value;
  };
}
