import { ValueGetterParams } from '@ag-grid-community/core';
import getNumberOfItemsFromFieldNameValueGetter from './getNumberOfItemsFromFieldNameValueGetter';

describe('getNumberOfItemsFromFieldNameValueGetter()', () => {
  interface Children1 {
    name: string;
  }

  interface Children2 {
    id: string;
  }

  interface GenericInterface {
    name: string;
    children1: Children1[];
    children2: Children2[];
  }

  let genericInterfaceData: GenericInterface;
  let params: Partial<ValueGetterParams>;

  beforeEach(() => {
    genericInterfaceData = {
      name: 'name1',
      children1: [
        {
          name: 'name children1 1',
        },
        {
          name: 'name children1 2',
        },
      ],
      children2: [
        {
          id: 'name children2 1',
        },
      ],
    };

    params = {
      colDef: {
        field: 'name',
      },
      data: genericInterfaceData,
    };
  });

  it('should return a value getter function', () => {
    expect(getNumberOfItemsFromFieldNameValueGetter<GenericInterface, Children1>('children1')).toEqual(expect.any(Function));
  });

  it('should return the length of interfaces as 0 if no interfaces', () => {
    const valueGetter = getNumberOfItemsFromFieldNameValueGetter<GenericInterface, Children1>('children1');
    expect(valueGetter(params)).toEqual('2');
  });

  it('should return the length of interfaces when list is not empty', () => {
    const valueGetter = getNumberOfItemsFromFieldNameValueGetter<GenericInterface, Children2>('children2');
    expect(valueGetter(params)).toEqual('1');
  });

});
