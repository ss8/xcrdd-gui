import {
  ValueGetterParams,
  ITooltipParams,
} from '@ag-grid-enterprise/all-modules';

export default function getFieldFromFirstInterfaceNameValueGetter<T, U>(
  interfaceName: keyof T,
): (params: Partial<ValueGetterParams> | Partial<ITooltipParams>) => string {
  return ({ data, colDef }: Partial<ValueGetterParams> | Partial<ITooltipParams>): string => {

    const interfaceConfig = data as T;
    const [interfaceData] = (interfaceConfig[interfaceName] || []) as U[];
    const field = colDef.field as keyof U;

    if (!interfaceData) {
      return '-';
    }

    return (`${interfaceData[field] || '-'}`);
  };
}
