import { ITooltipParams, ValueGetterParams } from '@ag-grid-community/core';

export default function getNumberOfItemsFromFieldNameValueGetter<T, U>(
  interfaceName: keyof T,
): (params: Partial<ValueGetterParams> | Partial<ITooltipParams>) => string {
  return ({ data }: Partial<ValueGetterParams> | Partial<ITooltipParams>): string => {
    const interfaceData = data as T;
    return ((interfaceData[interfaceName] || []) as U[]).length.toString();
  };
}
