import {
  ValueGetterParams,
  ITooltipParams,
} from '@ag-grid-enterprise/all-modules';

interface OptionValue {
  key:string,
  value:string | boolean | number
}

interface Field {
  options?: OptionValue[]
}

export default function getFieldOptionFromFirstInterfaceNameValueGetter<T, U extends Field>(
  interfaceName: keyof T,
): (params: Partial<ValueGetterParams> | Partial<ITooltipParams>) => string {
  return ({ data, colDef }: Partial<ValueGetterParams> | Partial<ITooltipParams>): string => {
    const accessFunction = data as T;
    const [interfaceData] = (accessFunction[interfaceName] || []) as U[];

    if (!interfaceData) {
      return '-';
    }

    const { options } = interfaceData;

    if (!options) {
      return '-';
    }

    const option = options.find(({ key }) => key === colDef.field);
    return option?.value.toString() ?? '-';
  };
}
