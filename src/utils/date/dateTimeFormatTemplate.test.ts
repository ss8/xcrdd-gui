import getDateTimeFormatTemplate, { setLanguage } from './dateTimeFormatTemplate';
import {
  LANGUAGE_EN_US,
  LANGUAGE_EN_GB,
  DATE_TIME_FORMAT_US,
  DATE_TIME_FORMAT_GB,
} from '../../constants';

describe('getDateTimeFormatTemplate()', () => {
  it('should return US template if language not set', () => {
    expect(getDateTimeFormatTemplate()).toEqual(DATE_TIME_FORMAT_US);
  });

  it('should return US template for en-US language', () => {
    setLanguage(LANGUAGE_EN_US);
    expect(getDateTimeFormatTemplate()).toEqual(DATE_TIME_FORMAT_US);
  });

  it('should return GB template for en-GB language', () => {
    setLanguage(LANGUAGE_EN_GB);
    expect(getDateTimeFormatTemplate()).toEqual(DATE_TIME_FORMAT_GB);
  });

  it('should return US template if language is invalid', () => {
    setLanguage('invalid');
    expect(getDateTimeFormatTemplate()).toEqual(DATE_TIME_FORMAT_US);
  });
});
