import moment from 'moment';

const ISOFormat = (data: any) => moment(data).toISOString();

export default ISOFormat;
