import {
  LANGUAGE_EN_US,
  LANGUAGE_EN_GB,
  DATE_TIME_FORMAT_US,
  DATE_TIME_FORMAT_GB,
} from '../../constants';

// Date formats should be available in i18n lib in the future
const DATE_TIME_FORMAT_MAP : { [key: string]: string; } = {
  [LANGUAGE_EN_US]: DATE_TIME_FORMAT_US,
  [LANGUAGE_EN_GB]: DATE_TIME_FORMAT_GB,
};

// Language setting should be available in i18n lib in the future
let language: string = LANGUAGE_EN_US;
export function setLanguage(applicationLanguage: string) {
  language = applicationLanguage;
}

export default function getDateTimeFormatTemplate() {
  if (DATE_TIME_FORMAT_MAP[language] == null) {
    return DATE_TIME_FORMAT_MAP[LANGUAGE_EN_US];
  }

  return DATE_TIME_FORMAT_MAP[language];
}
