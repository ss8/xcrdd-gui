import ISOFormat from './isoFormat';

describe('ISOFormat', () => {
  it('should return the date as ISO format', () => {
    const date = '2020-06-17T13:10:10.590Z';
    expect(ISOFormat(new Date(date))).toEqual(date);
  });
});
