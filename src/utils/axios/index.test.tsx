import { AxiosError } from 'axios';
import { ServerResponse } from 'data/types';
import { extractErrorMessage } from '..';

describe('extractErrorMessage()', () => {
  it('should return error.message if not an axios error', () => {
    const error: AxiosError<ServerResponse<void>> = {
      config: {},
      name: 'erro1',
      message: 'some error message',
      isAxiosError: false,
      toJSON: jest.fn(),
    };

    expect(extractErrorMessage(error)).toEqual('some error message');
  });

  it('should return error.message if an axios error but response empty', () => {
    const error: AxiosError<ServerResponse<void>> = {
      config: {},
      name: 'erro1',
      message: 'some error message',
      isAxiosError: true,
      toJSON: jest.fn(),
    };

    expect(extractErrorMessage(error)).toEqual('some error message');
  });

  it('should return the server response error', () => {
    const error: AxiosError<ServerResponse<void>> = {
      config: {},
      name: 'erro1',
      message: 'some error message',
      isAxiosError: true,
      toJSON: jest.fn(),
      response: {
        status: 500,
        statusText: '',
        headers: [],
        config: {},
        data: {
          data: null,
          metadata: null,
          success: false,
          error: {
            status: 500,
            code: 'INTERNAL_SERVER_ERROR',
            message: 'some server error message',
          },
          errors: [],
          message: '',
          status: '',
        },
      },
    };

    expect(extractErrorMessage(error)).toEqual('some server error message');
  });
});
