import { AxiosError } from 'axios';
import { ServerResponse } from 'data/types';

const extractErrorMessage = (error: AxiosError<ServerResponse<void>>): string => {
  if (!error.isAxiosError) {
    return error.message;
  }

  return error?.response?.data?.error?.message || error.message;
};

export default extractErrorMessage;
