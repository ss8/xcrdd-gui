import { validateNonRepeatedProperty } from './listValidators';

describe('validateNonRepeatedProperty()', () => {

  const MOCK = [
    'name1',
    'name2',
    'name3',
  ];

  it('should return valid=true when target key value is not in use by controlList items', () => {
    expect(validateNonRepeatedProperty('name0', MOCK)).toEqual(true);
  });

  it('should return valid=false when target key value is already in use by controlList items', () => {
    expect(validateNonRepeatedProperty('name2', MOCK)).toEqual(false);
  });
});
