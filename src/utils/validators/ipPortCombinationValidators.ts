export type Validatable = {
  ipAddress: string,
  port: string
}

export function validateIpPortCombinations<T extends Validatable, U extends Validatable>(
  listeners: T[] | undefined,
  destinations: U[] | undefined,
): boolean {
  const listenersIpPort = listeners?.map(trim).filter(filterNotEmpty).map(mapIpPort) ?? [];
  const destinationsIpPort = destinations?.map(trim).filter(filterNotEmpty).map(mapIpPort) ?? [];

  const combinedIpPortList = [...listenersIpPort, ...destinationsIpPort];
  const hasDuplicatedIpPort = (new Set(combinedIpPortList)).size !== combinedIpPortList.length;

  return !hasDuplicatedIpPort;
}

export function validateCurrentIpPortCombination<T extends Validatable, U extends Validatable>(
  current: T | U | undefined,
  listeners: T[] | undefined,
  destinations: U[] | undefined,
): boolean {
  if (current == null) {
    return true;
  }

  const { ipAddress, port } = trim(current);
  if (ipAddress === '' || port === '') {
    return true;
  }

  const currentIpPort = mapIpPort(trim(current));

  const listenersIpPort = listeners?.map(trim).filter(filterNotEmpty).map(mapIpPort) ?? [];
  const destinationsIpPort = destinations?.map(trim).filter(filterNotEmpty).map(mapIpPort) ?? [];

  const combinedIpPortList = [...listenersIpPort, ...destinationsIpPort];
  const matchedIpPort = combinedIpPortList.filter((entry) => entry === currentIpPort);

  // The combinedIpPortList will also include the current interface being validated,
  // so it will be a problem only if there are multiple entries for the same port:ip
  const hasDuplicatedIpPort = matchedIpPort.length > 1;

  return !hasDuplicatedIpPort;
}

function trim<T extends Validatable>(entry: T): T {
  return {
    ...entry,
    ipAddress: entry.ipAddress?.trim(),
    port: entry.port?.trim(),
  };
}

function filterNotEmpty<T extends Validatable>({ ipAddress, port }: T): boolean {
  return ipAddress !== '' && port !== '';
}

function mapIpPort<T extends Validatable>({ ipAddress, port }: T): string {
  return `${ipAddress}:${port}`;
}
