import { validateIpPortCombinations, Validatable, validateCurrentIpPortCombination } from './ipPortCombinationValidators';

describe('ipPortCombinationValidators', () => {
  describe('validateIpPortCombinations()', () => {
    let listeners: Validatable[];
    let destinations: Validatable[];

    beforeEach(() => {
      listeners = [{
        ipAddress: '1.1.1.1',
        port: '1',
      }, {
        ipAddress: '2.2.2.2',
        port: '1',
      }];

      destinations = [{
        ipAddress: '3.3.3.3',
        port: '1',
      }, {
        ipAddress: '4.4.4.4',
        port: '1',
      }];
    });

    it('should return true if lists are empty', () => {
      expect(validateIpPortCombinations([], [])).toEqual(true);
    });

    it('should return true if only listeners', () => {
      expect(validateIpPortCombinations(listeners, [])).toEqual(true);
    });

    it('should return true if only destinations', () => {
      expect(validateIpPortCombinations([], destinations)).toEqual(true);
    });

    it('should return true if no duplicated ip:port', () => {
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(true);
    });

    it('should return true if same ip but different port', () => {
      listeners[1].ipAddress = listeners[0].ipAddress;
      listeners[1].port = '2';
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(true);
    });

    it('should return true if ip or port empty', () => {
      listeners[0].ipAddress = '';
      listeners[0].port = '';
      destinations[0].ipAddress = '';
      destinations[0].port = '';
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(true);
    });

    it('should return false if duplicated ip:port in a listener', () => {
      listeners[1].ipAddress = listeners[0].ipAddress;
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(false);
    });

    it('should return false if duplicated ip:port in a destination', () => {
      destinations[1].ipAddress = destinations[0].ipAddress;
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(false);
    });

    it('should return false if duplicated ip:port between listener and destination', () => {
      destinations[0].ipAddress = listeners[0].ipAddress;
      expect(validateIpPortCombinations(listeners, destinations)).toEqual(false);
    });
  });

  describe('validateCurrentIpPortCombination()', () => {
    let current: Validatable;
    let listeners: Validatable[];
    let destinations: Validatable[];

    beforeEach(() => {
      current = {
        ipAddress: '1.1.1.1',
        port: '1',
      };

      listeners = [{
        ipAddress: '1.1.1.1',
        port: '1',
      }, {
        ipAddress: '2.2.2.2',
        port: '1',
      }];

      destinations = [{
        ipAddress: '3.3.3.3',
        port: '1',
      }, {
        ipAddress: '4.4.4.4',
        port: '1',
      }];
    });

    it('should return true if current is null', () => {
      expect(validateCurrentIpPortCombination(undefined, listeners, destinations)).toEqual(true);
    });

    it('should return true if ipAddress is empty', () => {
      current.ipAddress = '';
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(true);
    });

    it('should return true if port is empty', () => {
      current.port = '';
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(true);
    });

    it('should return true if no duplicated ip:port', () => {
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(true);
    });

    it('should return true if same ip but different port', () => {
      listeners[1].ipAddress = current.ipAddress;
      listeners[1].port = '2';
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(true);
    });

    it('should return false if duplicated ip:port in a listener', () => {
      listeners[1].ipAddress = current.ipAddress;
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(false);
    });

    it('should return false if duplicated ip:port in a destination', () => {
      destinations[1].ipAddress = current.ipAddress;
      expect(validateCurrentIpPortCombination(current, listeners, destinations)).toEqual(false);
    });
  });

});
