// eslint-disable-next-line import/prefer-default-export
export function validateNonRepeatedProperty(
  property: string,
  controlList: string[],
): boolean {

  const isAfNameValid = !controlList.find((controlListItem) => {
    return (
      controlListItem?.trim() === property?.trim()
    );
  });

  return isAfNameValid;
}
