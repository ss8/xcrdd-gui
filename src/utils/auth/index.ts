import axios from 'axios';

import {
  groupsMap,
} from '../../shared/constants';

// The import throws error with decrypt toString conversion so using require for now (and it works as expected)
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CryptoJS = require('crypto-js');

export enum AUTH {
  PRIVILEGES = 'privileges',
  TOKEN = 'token',
  USERID = 'userID',
  USERDBID = 'userDBId',
  JWTTOKEN = 'jwtToken',
}

interface LogiGroupIdsAndRolesArgs {
  externalDashboardIP: string
  logiAdminAccount: string
  logiAdminPswd: string
  logiAdminUserName: string
  privileges: string[]
}

interface LogiTokenAuthArgs {
  user: string;
  externalDashboardIP: string;
  logiOAuthClientId: string;
  logiOAuthClientName: string;
  logiSupervisorUserName: string;
  logiSupervisorPswd: string;
}

export const decryptText = (encryptedText: string): string => {
  const byteText = CryptoJS.AES.decrypt(encryptedText, 'This is Awesome Dashboard');
  return byteText.toString(CryptoJS.enc.Utf8);
};

export const setAuth = (name: AUTH, value: any, stringify?: boolean) => {
  localStorage.setItem(name, stringify ? JSON.stringify(value) : value);
};

export const getAuth = (name: AUTH) => {
  return localStorage.getItem(name);
};

export const removeAuth = (name: AUTH) => {
  localStorage.removeItem(name);
};

export const clearAuth = () => {
  localStorage.clear();
};

export const getLogiGroupIdsAndRoles = async ({
  externalDashboardIP,
  logiAdminAccount,
  logiAdminPswd,
  logiAdminUserName,
  privileges,
}: LogiGroupIdsAndRolesArgs) => {
  // const roles = basicRolesSet;
  const groups = [] as string[];
  const groupIds = [] as string[];

  // Always add regular user group by default
  groups.push(groupsMap.dashboard_user);

  privileges.forEach((privilege: string) => {
    if (groupsMap[privilege]) {
      groups.push(groupsMap[privilege]);
    }
  });

  try {
    // TODO: Need to remove accountId before upgrading Logi instance from 5.9
    const logiGroupsUrl = `${externalDashboardIP}/zoomdata/api/groups?accountId=${logiAdminAccount}&limit=-1&offset=0`;

    const { data: groupObjs } = await axios.get(logiGroupsUrl, {
      headers: {
        Accept: 'application/vnd.zoomdata.v2+json',
      },
      auth: {
        username: decryptText(logiAdminUserName),
        password: decryptText(logiAdminPswd),
      },
    });

    groupObjs.forEach((groupObj: any) => {
      if (groups.includes(groupObj.label)) {
        groupIds.push(groupObj.id);
      }
    });
  } catch (err) {
    // TODO: Log the error
    // console.log('Error while getting Logi group IDs and roles: ', err);
  }

  return {
    groupIds,
    // roles,
  };
};

export const generateLogiToken = async ({
  user,
  externalDashboardIP,
  logiOAuthClientId,
  logiOAuthClientName,
  logiSupervisorUserName,
  logiSupervisorPswd,
}: LogiTokenAuthArgs) => {
  const logiTokenUrl = `${externalDashboardIP}/zoomdata/api/oauth/token?username=${user}`;

  const {
    data: { tokenValue, expiration },
  } = await axios.post(
    logiTokenUrl,
    {},
    {
      headers: {
        Accept: 'application/vnd.zoomdata.v2+json',
        'Content-Type': 'application/vnd.zoomdata.v2+json',
      },
      auth: {
        username: decryptText(logiSupervisorUserName),
        password: decryptText(logiSupervisorPswd),
      },
      data: {
        clientId: logiOAuthClientId,
        clientName: logiOAuthClientName,
      },
    },
  );

  return {
    tokenValue,
    expiration,
  };
};
