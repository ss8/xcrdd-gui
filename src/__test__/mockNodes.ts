import { NodeData } from 'data/nodes/nodes.types';

// eslint-disable-next-line import/prefer-default-export
export const mockNodes: NodeData[] = [
  {
    id: 'Tm9kZS1EdW1teS0x',
    nodeName: 'node-1',
    nodeType: 'XCF3',
    topology: 'HAPAIR',
    ipAddress: '10.0.1.11',
  },
  {
    id: 'Tm9kZS1EdW1teS0y',
    nodeName: 'node-2',
    nodeType: 'XCF3',
    topology: 'STANDALONE',
    ipAddress: '10.0.2.22',
  },
  {
    id: 'Q0NQQUdOT0RF',
    nodeName: 'CCPAGNODE',
    nodeType: 'CCPAG',
    topology: 'STANDALONE',
    ipAddress: '172.31.9.149',
    destNodeName: 'MDF3NODE',
  },
  {
    id: 'TURGMk5PREU',
    nodeName: 'MDF2NODE',
    nodeType: 'MDF2',
    topology: 'STANDALONE',
    ipAddress: '172.31.9.149',
  },
  {
    id: 'TURGM05PREU',
    nodeName: 'MDF3NODE',
    nodeType: 'MDF3',
    topology: 'STANDALONE',
    ipAddress: '172.31.9.149',
  },
];
