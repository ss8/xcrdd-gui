import { FtpInfo } from 'data/ftpInfo/ftpInfo.types';

// eslint-disable-next-line import/prefer-default-export
export const mockFtpInfos: FtpInfo[] = [
  {
    id: 'ZnRwMQ',
    ftpId: 'ftp1',
    destPath: '/test/dir1',
    fileMode: 'CASE',
    ftpFileTransferTimeout: '60',
    ftpFileSize: '2048',
    ftpType: 'SFTP',
    ipAddress: '1.1.1.1',
    operatorId: '1',
    port: '11',
    username: 'test-1',
    userPassword: '*****',
  },
  {
    id: 'ZnRwMg',
    ftpId: 'ftp2',
    destPath: '/test/dir2',
    fileMode: 'ALLINONE',
    ftpFileTransferTimeout: '60',
    ftpFileSize: '2048',
    ftpType: 'SFTP',
    ipAddress: '2.2.2.2',
    operatorId: '2',
    port: '22',
    username: 'test-2',
    userPassword: '*****',
  },
];
