import { SupportedFeatures } from 'data/supportedFeatures/supportedFeatures.types';

// eslint-disable-next-line import/prefer-default-export
export const mockSupportedFeatures: SupportedFeatures = {
  id: 'XCP',
  version: '8.12.0',
  description: 'This file defines categories, properties, and relations between Xcipio concepts (services, access functions, collection functions, targets, and features)',
  features: {
    customer: 'CI',
    nodes: [
      {
        ip: '172.30.2.35',
        type: 'XCP',
        features: [
          'AF-NOKIA-SBC',
          'AF-ERK-5GAMF',
          'AF-NKSR',
          'AF-GPVRADIUS',
          'AF-NOK-5GAMF',
          'AF-NOK-5GSMF',
          'AF-NOK-5GSMSF',
          'AF-NOK-5GUDM',
          'AF-NSN-SDM',
          'AF-ETSI-GIZMO',
          'AF-MVNR-5GSMSC',
          'XC-CNFVCP',
          'AF-ACM-SBC',
          'AF-NOKIA-SAEGW',
          'AF-ERK-UPF',
          'AF-ERK-SMF-X2DIST',
          'AF-ERK-SMF-X1',
          'AF-ACM-SBCX123',
          'AF-BRW-AAA',
          'AF-STARENT-EHA',
          'AF-STARENT-PDSN',
          'AF-ERK-VMME',
          'AF-GIZMO',
          'AF-NT-GSM',
          'AF-NT-MTX',
          'AF-SONUS-SBC',
          'AF-ALU-DSS',
          'AF-ALU-HLR',
          'AF-ALU-MME',
          'AF-ALU-SGSN',
          'AF-ALU-SGW',
          'AF-CAMIANT-PCRF',
          'ciscovpcrf',
          'AF-CISCO-VPRCF',
          'AF-EPS',
          'AF-ERK-SCEF',
          'AF-ERK-SGW',
          'AF-HP-AAA',
          'AF-HP-HLR',
          'AF-JSTD025',
          'AF-KODK-PTT',
          'AF-MITEL-TAS',
          'AF-MV-CSCF',
          'AF-MV-HSS',
          'AF-MV-MSTORE',
          'AF-MV-PGW',
          'AF-MV-RCS',
          'AF-MV-SBC',
          'AF-MV-SMS',
          'AF-NSN-CSCF',
          'AF-NSN-HLR',
          'AF-RCS',
          'AF-SMSG-UBCL',
          'AF-STARENT-GGSN',
          'AF-STARENT-PGW',
          'AF-SYLN-FS411',
          'CF-ATIS-0700005',
          'CF-ATIS-1000678',
          'CF-ETSI-102232-TCP',
          'CF-3GPP-33108',
          'CF-JSTD025',
          'CF-TIA-1066',
          'XC-CORE',
          'XC-IPDU',
          'XC-UIB',
          'XC-VPN',
          'dde',
          'dr',
          'erkvmme',
          'etsi103221',
          'gizmo',
          'kpi',
          'lboam',
          'nsnsaegw',
          'sl',
          'sonussbc',
          'x2x',
          'cs2k',
          'miteluag',
          'erkmcptt',
          'xccfg',
          'XC-ALARM',
          'AF-ALU-CSCF',
          'AF-GP',
          'AF-XCAP',
          'AF-PVOIP',
          'AF-STREAMWIDE',
          'AF-ACM-SBCX123',
          'CF-ETSI-102232-TCP',
          'CF-ETSI-201671',
          'XC-CORE',
          'XC-UIB',
          'XC-VPN',
          'kpi',
          'xccfg',
          'dfaudit',
          'monotifier',
          'XC-ALARM',
          'AF-ERK-5GAMF',
          'AF-NKSR',
          'AF-NOK-5GAMF',
          'AF-NOK-5GSMF',
          'AF-NOK-5GSMSF',
          'AF-NOK-5GUDM',
          'AF-NSN-SDM',
          'AF-ETSI-GIZMO',
          'AF-MVNR-5GSMSC',
          'XC-CNFVCP',
        ],
      },
      {
        ip: '172.30.1.221',
        type: 'XTP',
        features: [
          'XC-CORE',
          'XC-XTP',
          'XC-UIB',
          'sx3dist',
        ],
      },
      {
        ip: '172.30.2.187',
        type: 'XDP',
        features: [
          'XC-CORE',
          'XC-IPDU',
        ],
      },
    ],
    version: 1,
  },
  config: {
    services: {
      PTT: {
        description: 'Push-to-Talk over mobile network',
        title: 'Push-to-Talk (PTT)',
        deployments: ['andromeda'],
      },
      'VOIP-VOICE': {
        description: 'Voice Over IP (VoIP)',
        title: 'Voice over packet data',
        deployments: ['andromeda'],
      },
      'LTE-IOT': {
        description: 'Internet of Things (IoT) over mobile network',
        title: 'IoT',
        deployments: ['andromeda'],
      },
      'GSMCDMA-2G-DATA': {
        description: 'CDMA/GSM 2G circuit-switched mobile data services',
        title: '2G data',
        deployments: ['andromeda'],
      },
      'VOLTE-4G-VOICE': {
        description: 'Voice Over LTE (VoLTE)',
        title: 'VoLTE 4G Voice',
        deployments: ['andromeda'],
      },
      IP: {
        description: 'IP data',
        title: 'Internet data',
        deployments: ['andromeda'],
      },
      'VZ-VISIBLE': {
        description: 'Visible service',
        title: 'Visible',
        deployments: ['andromeda'],
      },
      'VOLTE-RCS': {
        description: 'Rich Communication Services (RCS) over mobile network',
        title: 'Rich Communication Services (RCS)',
        deployments: ['andromeda'],
      },
      'CDMA2000-3G-DATA': {
        description: 'cdma2000 3G data',
        title: '3G PD',
        deployments: ['andromeda'],
      },
      EMAIL: {
        description: 'Email services: SMTP, IMAP, POP3',
        title: 'Email',
        deployments: ['andromeda'],
      },
      'CDMA2000-3G-VOICE': {
        description: 'cdma2000 3G voice',
        title: 'cdma2000 voice',
        deployments: ['andromeda'],
      },
      MCPTT: {
        description: 'Mission-critical Push-to-Talk (MCPTT)',
        title: 'Mission-critical Push-to-Talk (MCPTT)',
        deployments: ['andromeda'],
      },
      'LTE-4G-DATA': {
        description: '4G LTE data',
        title: '4G PD',
        deployments: ['andromeda'],
      },
      CELLCRYPT: {
        description: 'Encrypted Content Delivery using CellCrypt',
        title: 'CellCrypt',
        deployments: ['andromeda'],
      },
      '5G-DATA': {
        description: '5G mobile data',
        title: '5G data',
        deployments: ['andromeda'],
      },
      'LTE-4G-DATA-ROAMING': {
        description: '4G LTE data roaming',
        title: 'Roaming 4G PD',
        deployments: ['andromeda'],
      },
      'UMTS-3G-VOICE': {
        description: 'UMTS 3G voice',
        title: 'UMTS voice',
        deployments: ['andromeda'],
      },
      'GSMCDMA-2G-VOICE': {
        description: 'CDMA/GSM 2G circuit-switched mobile voice services',
        title: 'Circuit Switch',
        deployments: ['andromeda'],
      },
      'LTE-GIZMO': {
        description: 'Gizmo watch communications',
        title: 'Gizmo',
        deployments: ['andromeda'],
      },
      'ETSI-GIZMO-SERVICE': {
        description: 'ETSI Gizmo watch communications',
        deployments: [
          'andromeda',
        ],
        title: 'Gizmo ETSI',
      },
      VOICE: {
        description: 'Voice used',
        deployments: ['Endeavor'],
        title: 'Voice',
      },
      MOBILE: {
        description: 'Mobile',
        deployments: ['Endeavor'],
        title: 'Mobile',
      },
      'VOICE-AND-IP-SMS': {
        description: 'Voice and IP SMS service',
        deployments: ['Endeavor'],
        title: 'Voice & IP SMS',
      },
      DATA: {
        description: 'Data',
        deployments: ['Endeavor'],
        title: 'Data',
      },
      SMS: {
        description: 'SMS',
        deployments: ['Endeavor'],
        title: 'SMS',
      },
      ROAMING: {
        description: 'Roaming',
        deployments: ['Endeavor'],
        title: 'Roaming',
      },
      SUPPLEMENTARY: {
        description: 'Supplementary',
        deployments: ['Endeavor'],
        title: 'Supplementary',
      },
      'OFFNET-VOICE': {
        description: 'Offnet Voice',
        deployments: ['Endeavor'],
        title: 'Offnet Voice',
      },
      VOICEMAIL: {
        description: 'Voicemail',
        deployments: ['Endeavor'],
        title: 'Voicemail',
      },
      'MOBILE-DATA': {
        description: 'Mobile Data',
        deployments: ['Endeavor'],
        title: 'Mobile Data',
      },
      BROADBAND: {
        description: 'Broadband',
        deployments: ['Endeavor'],
        title: 'Broadband',
      },
    },
    accessFunctions: {
      LU3G_CSCF: {
        requiredTargets: [
          'SIP-URI',
          'TEL-URI',
        ],
        collectionFunctions: [
          'CF-TIA-1066',
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-ALU-CSCF',
        ],
        autowire: true,
        title: 'Alcatel-Lucent Web Gateway',
        models: [],
        versions: [
          '4.0',
          '11.0',
        ],
        genccc: false,
        services: [
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'LU3G_CSCF',
        description: 'Alcatel-Lucent Web Gateway',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      ALU_DSS: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ALU-DSS',
        ],
        autowire: true,
        title: 'Alcatel-Lucent Distributed Soft Switch',
        models: [
          'SGS-IWF',
        ],
        versions: [
          '0104',
          '0302',
        ],
        genccc: false,
        services: [
          'UMTS-3G-VOICE',
        ],
        optionalTargets: [
          'IMEI',
          'MSISDN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_DSS',
        description: 'Alcatel-Lucent (ALU) Digital Soft Switch (DSS) Access Function',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      NORTEL_MTX: {
        requiredTargets: [
          'MIN',
          'CASEIDMIN',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-NT-MTX',
        ],
        autowire: false,
        title: 'Nortel/Ericsson MTX MSC (CDMA)',
        models: [],
        versions: [
          '12',
          '13',
          '14',
          '15',
          '16',
          '18',
        ],
        genccc: true,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        x3TargetAssociations: {
          targetInfoType: 'MTXCFCI',
          targets: [
            'MIN',
          ],
        },
        xcipioAFType: 'NORTEL_MTX',
        description: 'Nortel/Ericsson MTX Mobile Switching Center (MSC) for CDMA network',
      },
      NORTEL_HLR: {
        requiredTargets: [
          'IMEI',
          'IMSI',
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
          'CF-NORTEL',
        ],
        features: [
          'AF-NT-GSM',
        ],
        autowire: false,
        title: 'Nortel GSM HLR',
        models: [],
        versions: [
          'Pre-GSM18',
          'GSM18',
          'GSM19',
        ],
        genccc: false,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'NORTEL_HLR',
        description: 'Nortel GSM HLR',
      },
      MVNR_SBC: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-MV-SBC',
        ],
        autowire: true,
        title: 'Mavenir P-SBC',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VZ-VISIBLE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_SBC',
        description: 'Mavenir Session Border Controller (SBC) Access Function',
      },
      EPS: {
        requiredTargets: [
          'CASEID',
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-EPS',
        ],
        autowire: false,
        title: 'EPS',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'EPS',
        description: '3GPP 33.108 Evolved Packet System (EPS) Access Function',
      },
      ERK_MCPTT: {
        title: 'Ericsson MCPTT',
        description: 'Ericsson Mission-critical Push-to-Talk (MCPTT)',
        xcipioAFType: 'ERK_MCPTT',
        features: [
          'erkmcptt',
        ],
        services: [
        ],
        requiredTargets: [
        ],
        optionalTargets: [
        ],
        collectionFunctions: [
        ],
        x3deliveryOptions: null,
        versions: [],
        models: [
        ],
        autowire: false,
        genccc: false,
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      ERK_SMF: {
        features: [
          'AF-ERK-SMF-X1',
          'AF-ERK-SMF-X2DIST',
        ],
        autowire: true,
        title: 'Ericsson SMF',
        models: [],
        versions: [
          '1.4',
        ],
        genccc: false,
        services: {
          '5G-DATA': {
            requiredTargets: [
              'MSISDN',
            ],
            optionalTargets: [
              'IMEI',
            ],
            collectionFunctions: [
              'CF-ETSI-102232',
            ],
          },
          'LTE-4G-DATA': {
            requiredTargets: [
              'IMSI',
            ],
            collectionFunctions: [
              'CF-3GPP-33108',
            ],
          },
        },
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_SMF',
        description: 'Ericsson Session Management Function Point of Interception',
      },
      ETSI103221: {
        title: 'ETSI 103 221',
        description: 'ETSI 103 221',
        xcipioAFType: 'ETSI103221',
        features: ['etsi103221'],
        services: [],
        requiredTargets: ['IMSI'],
        optionalTargets: [],
        collectionFunctions: ['CF-3GPP-33108'],
        x3deliveryOptions: null,
        versions: [],
        models: [],
        autowire: true,
        genccc: false,
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      MVNR_5GSMSC: {
        requiredTargets: [
          'GPSIMSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-MVNR-5GSMSC',
        ],
        autowire: false,
        title: 'Mavenir SMS Center',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/1.1',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'GPSINAI',
          'SUPINAI',
          'SUCI',
          'PEIIMEI',
          'PEIIMEI_CHECKDIGIT',
          'PEIIMEISV',
        ],
        x3deliveryOptions: null,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
        xcipioAFType: 'MVNR_5GSMSC',
        description: 'Mavenir Short Message Service Center (SMSC)',
      },
      MVNR_RCS_MSTORE: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-MV-MSTORE',
        ],
        autowire: true,
        title: 'Mavenir RCS mStore',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-RCS',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_RCS_MSTORE',
        description: 'Mavenir Rich Communications Services (RCS) Message Store (mStore) Access Function',
      },
      ERK_VMME: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ERK-VMME',
        ],
        autowire: true,
        title: 'Ericsson vMME',
        models: [
          'MULTIPLE_X1',
        ],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
          'LTE-4G-DATA-ROAMING',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_VMME',
        description: 'Ericsson Virtual Mobility Management Entity (vMME) Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      ICS_RCS: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-RCS',
        ],
        autowire: true,
        title: 'ICS RCS Server',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-RCS',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ICS_RCS',
        description: 'IMS Centralized Services (ICS) Rich Communications Services (RCS) Access Function. Legacy RCS implementation',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      STA_PDSN: {
        requiredTargets: [
          'LOGIN',
          'MDN',
        ],
        collectionFunctions: [
          'CF-JSTD025B',
          'CF-TIA-1072',
        ],
        features: [
          'AF-STARENT-PDSN',
        ],
        autowire: false,
        title: 'Cisco/Starent PDSN',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'CDMA2000-3G-DATA',
        ],
        optionalTargets: [
          'IP4',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'STA_PDSN',
        description: 'Cisco/Starent Packet Data Serving Node (PDSN) Access Function',
      },
      MVNR_RCS_AS: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-MV-RCS',
        ],
        autowire: true,
        title: 'Mavenir RCS Application Server',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-RCS',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_RCS_AS',
        description: 'Mavenir Rich Communications Services (RCS) Application Server (AS) Access Function',
      },
      VIETTEL_MD: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-VIETTEL-MD',
        ],
        autowire: false,
        title: 'Viettel MD',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'VIETTEL_MD',
        description: 'Viettel MD',
      },
      ALU_MME: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ALU-MME',
        ],
        autowire: true,
        title: 'Alcatel-Lucent MME',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
          'LTE-4G-DATA-ROAMING',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_MME',
        description: 'Alcatel-Lucent (ALU) Mobility Management Entity (MME) Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      ERK_SCEF: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ERK-SCEF',
        ],
        autowire: true,
        title: 'Ericsson SCEF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-IOT',
        ],
        optionalTargets: [
          'EXTERNAL_ID',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_SCEF',
        description: 'Ericsson Service Capability Exposure Function (SCEF) Access Function',
      },
      NOKIA_SBC: {
        title: 'Nokia SBC',
        description: 'Nokia SBC',
        xcipioAFType: 'NOKIA_SBC',
        features: [
          'AF-NOKIA-SBC',
        ],
        services: [
          '5G-VOICE',
        ],
        requiredTargets: [
          'SIP-URI',
          'TEL-URI',
        ],
        optionalTargets: [],
        collectionFunctions: [
          'CF-TIA-1066',
        ],
        x3deliveryOptions: null,
        versions: [],
        models: [
          'I_SBC',
        ],
        autowire: true,
        genccc: false,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
      },
      NOK_5GAMF: {
        requiredTargets: [
          'GPSIMSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-NOK-5GAMF',
        ],
        autowire: false,
        title: 'Nokia AMF',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/1.1',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'GPSINAI',
          'SUPINAI',
          'SUCI',
          'PEIIMEI',
          'PEIIMEI_CHECKDIGIT',
          'PEIIMEISV',
        ],
        x3deliveryOptions: null,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
        xcipioAFType: 'NOK_5GAMF',
        description: 'Nokia Access and Mobility Management Function',
      },
      PCRF: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-JSTD025B',
          'CF-3GPP-33108',
        ],
        features: [
          'AF-CAMIANT-PCRF',
        ],
        autowire: true,
        title: 'Oracle/Camiant PCRF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'PCRF',
        description: 'Oracle/Camiant Policy and Charging Function (PCRF) Access Function',
      },
      NOK_5GSMSF: {
        requiredTargets: [
          'GPSIMSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-NOK-5GSMSF',
        ],
        autowire: false,
        title: 'Nokia SMSF',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/2',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'GPSINAI',
          'SUPINAI',
          'SUCI',
          'PEIIMEI',
          'PEIIMEI_CHECKDIGIT',
          'PEIIMEISV',
        ],
        x3deliveryOptions: null,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
        xcipioAFType: 'NOK_5GSMSF',
        description: 'Nokia SMS Function',
      },
      CISCOVPCRF: {
        title: 'Cisco Virtual PCRF',
        description: 'Cisco Virtual PCRF',
        requiredTargets: ['IMSI'],
        xcipioAFType: 'PCRF',
        features: ['ciscovpcrf'],
        services: ['LTE-4G-DATA'],
        optionalTargets: [],
        collectionFunctions: ['CF-JSTD025B', 'CF-3GPP-33108'],
        x3deliveryOptions: null,
        versions: [],
        models: ['CISCOVPCRF'],
        autowire: true,
        genccc: false,
      },
      ACME_X123: {
        title: 'Oracle X123',
        description: 'Oracle Acme Packet Session Border Controller (SBC) Access Function with Acme X123 interface',
        xcipioAFType: 'ACME_X123',
        features: [
          'AF-ACM-SBCX123',
        ],
        services: {
          'VOIP-VOICE': {
            requiredTargets: [
              'SIP-URI',
            ],
            optionalTargets: [
              'DN',
              'PARTIAL_DN',
              'TEL-URI',
              'SIP-URI',
              'IMSI',
              'IMEI',
            ],
          },
          'VOLTE-4G-VOICE': {
            requiredTargets: [
              'SIP-URI',
            ],
            optionalTargets: [
              'DN',
              'PARTIAL_DN',
              'TEL-URI',
              'SIP-URI',
              'IMSI',
              'IMEI',
            ],
          },
          VOICE: {
            requiredTargets: [
              'DN',
            ],
          },
        },
        collectionFunctions: [
          'CF-ATIS-1000678',
          'CF-ETSI-102232',
        ],
        x3deliveryOptions: null,
        versions: [
          '1.0',
          '1.1',
          '1.4',
        ],
        models: [],
        autowire: false,
        genccc: false,
        supportedActions: [
          'refresh',
        ],
      },
      ERK_UPF: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-3GPP-33108',
          'CF-ETSI-102232',
        ],
        features: [
          'AF-ERK-UPF',
        ],
        autowire: true,
        title: 'Ericsson UPF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
          '5G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_UPF',
        description: 'Ericsson User Plane Function Point of Interception',
      },
      SS8_DF: {
        requiredTargets: [
          'CASEID',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-JSTD025',
        ],
        autowire: false,
        title: 'SS8 J-STD-025A Delivery Function',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [
          'IMSI',
          'MIN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'SS8_DF',
        description: 'SS8 J-STD-025A Delivery Function',
      },
      MVNR_HSS: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-MV-HSS',
        ],
        autowire: true,
        title: 'Mavenir HSS',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VZ-VISIBLE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_HSS',
        description: 'Mavenir Home Subscriber Server (HSS) Access Function',
      },
      NSN_SDM: {
        requiredTargets: [
          'IMSI',
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-NSN-SDM',
        ],
        autowire: false,
        title: 'Nokia HSS/SDM',
        models: [
          'SDM_PRGW',
        ],
        versions: [],
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
        xcipioAFType: 'NSN_SDM',
        description: 'Nokia Networks Home Subscriber Server/Subscriber Data Management (HLR/SDM) POI',
      },
      GENVOIP: {
        requiredTargets: [
          'SIP-URI',
          'TEL-URI',
        ],
        collectionFunctions: [
          'CF-TIA-1066',
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-ACM-SBC',
        ],
        autowire: true,
        title: 'AcmePacket SBC with SS8 VoIP',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'GENVOIP',
        description: 'Oracle Acme Packet Session Border Controller (SBC) with SS8 VoIP interface',
      },
      HP_AAA: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-JSTD025B',
          'CF-3GPP-33108',
        ],
        features: [
          'AF-HP-AAA',
        ],
        autowire: true,
        title: 'HP AAA/SPC',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [
          'IMSI',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'HP_AAA',
        description: 'HP Authentication Authorization and Accounting (AAA) SPC Access Function',
      },
      ALU_HLR: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ALU-HLR',
        ],
        autowire: true,
        title: 'Alcatel-Lucent HLR/HSS SDM',
        models: [],
        versions: [
          'HSS3.1',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_HLR',
        description: 'Alcatel-Lucent (ALU) Home Subscriber Server (HSS)/Subscriber Data Manager (SDM) Access Function',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      STA_PGW: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-TIA-1066',
          'CF-ATIS-0700005',
          'CF-3GPP-33108',
        ],
        features: [
          'AF-STARENT-PGW',
        ],
        autowire: false,
        title: 'Cisco/Starent PGW',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'STA_PGW',
        description: 'Cisco/Starent Packet Data Network Gateway (PGW) Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      NORTEL_MSC: {
        requiredTargets: [
          'IMEI',
          'IMSI',
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
          'CF-NORTEL',
        ],
        features: [
          'AF-NT-GSM',
        ],
        autowire: false,
        title: 'Nortel GSM MSC',
        models: [],
        versions: [
          'GSM18',
          'GSM19',
        ],
        genccc: false,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [
          'TRUNK-CLLI',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'NORTEL_MSC',
        description: 'Nortel GSM MSC',
      },
      NSN_CSCF: {
        requiredTargets: [
          'SIP-URI',
          'TEL-URI',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
          'CF-TIA-1066',
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-NSN-CSCF',
        ],
        autowire: true,
        title: 'Nokia-Siemens CSCF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-RCS',
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'NSN_CSCF',
        description: 'Nokia-Siemens Networks Call Session Control Function (CSCF) Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      NOK_5GUDM: {
        requiredTargets: [
          'SUCI',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-NOK-5GUDM',
        ],
        autowire: true,
        title: 'Nokia UDM',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/2',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'PEIIMEI',
          'SUPINAI',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'NOK_5GUDM',
        description: 'Nokia Unified Data Management',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      NORTEL_DF: {
        requiredTargets: [
          'CASEID',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-NT-GSM',
        ],
        autowire: false,
        title: 'Nortel MSC DF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'NORTEL_DF',
        description: 'Nortel MSC Delivery Function',
      },
      ALU_SDHLR: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-ALU-SDHLR',
        ],
        autowire: false,
        title: 'Alcatel-Lucent SDHLR',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'UMTS-3G-VOICE',
        ],
        optionalTargets: [
          'IMSI',
          'MIN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_SDHLR',
        description: 'Alcatel-Lucent (ALU) Super Distributed Home Location Register (SD-HLR)',
      },
      ALU_SGW: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ALU-SGW',
        ],
        autowire: true,
        title: 'Alcatel-Lucent Serving Gateway',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA-ROAMING',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_SGW',
        description: 'Alcatel-Lucent (ALU) Serving Gateway (SGW)',
        supportedActions: [
          'refresh',
        ],
      },
      NSNHLR: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-NSN-HLR',
        ],
        autowire: true,
        title: 'Nokia-Siemens HLR/HSS (FE)',
        models: [
          'SDM_FE',
        ],
        versions: [
          '6.01',
          '7.3',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'NSNHLR',
        description: 'Nokia-Siemens Networks Home Location Register (HLR) FE Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      HP_HLR: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-HP-HLR',
        ],
        autowire: false,
        title: 'HP HLR',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'HP_HLR',
        description: 'HP Home Location Register (HLR) Access Function',
      },
      NOK_5GSMF: {
        requiredTargets: [
          'GPSIMSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-NOK-5GSMF',
        ],
        autowire: false,
        title: 'Nokia SMF',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/2',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'GPSINAI',
          'SUPINAI',
          'SUCI',
          'PEIIMEI',
          'PEIIMEI_CHECKDIGIT',
          'PEIIMEISV',
        ],
        x3deliveryOptions: null,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
        xcipioAFType: 'NOK_5GSMF',
        description: 'Nokia Session Management Function',
      },
      MITEL_TAS: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-MITEL-TAS',
        ],
        autowire: true,
        title: 'Mitel/Mavenir Telephony Application Server',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VZ-VISIBLE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MITEL_TAS',
        description: 'Mitel Telephony Application Server (TAS)',
      },
      SONUS_SBC: {
        requiredTargets: [
          'SIP-URI',
          'TEL-URI',
        ],
        collectionFunctions: [
          'CF-TIA-1066',
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-SONUS-SBC',
        ],
        autowire: true,
        title: 'Sonus SBC',
        models: [
          'SONUS_SBC',
        ],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'GENVOIP',
        description: 'Sonus SBC',
        supportedActions: [
          'refresh',
        ],
      },
      AXSGP: {
        title: 'SS8 Generic Probe Access Function',
        description: 'SS8 Generic Probe Access Function',
        xcipioAFType: 'AXSGP',
        features: [
          'AF-GP',
        ],
        services: {
          MOBILE: {
            optionalTargets: [
              'IMSI',
              'MSISDN',
              'IMEI',
            ],
          },
          'VOICE-AND-IP-SMS': {
            requiredTargets: [
              'MSISDN',
            ],
          },
          DATA: {
            optionalTargets: [
              'IMSI',
              'MSISDN',
              'IMEI',
            ],
          },
          SMS: {
            optionalTargets: [
              'IMSI',
              'MSISDN',
            ],
          },
          ROAMING: {
            optionalTargets: [
              'IMSI',
              'MSISDN',
            ],
          },
          SUPPLEMENTARY: {
            requiredTargets: [
              'MSISDN',
            ],
          },
          'OFFNET-VOICE': {
            requiredTargets: [
              'MSISDN',
            ],
          },
        },
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        x3TargetAssociations: {
          targetInfoType: 'GPTARGETINFO',
          targets: [
            'IMSI',
            'MSISDN',
            'IMEI',
          ],
        },
        x3deliveryOptions: null,
        versions: [],
        models: [],
        autowire: false,
        genccc: false,
      },
      MVNR_CSCF: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ATIS-0700005',
        ],
        features: [
          'AF-MV-CSCF',
        ],
        autowire: true,
        title: 'Mavenir P-CSCF/A-SBC',
        models: [
          'INI1',
        ],
        versions: [],
        genccc: false,
        services: [
          'VZ-VISIBLE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_CSCF',
        description: 'Mavenir Call Session Control Function (CSCF) Access Function',
      },
      NSN_SAEGW: {
        requiredTargets: [
          'IMSI',
          'IMEI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'nsnsaegw',
        ],
        autowire: true,
        title: 'Nokia SAEGW/SGW',
        models: [
          'NSN_SAEGW',
        ],
        versions: [
          '7.02',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [
          'MSISDN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ALU_SGW',
        description: 'Nokia SAEGW/SGW',
        supportedActions: [
          'refresh',
        ],
      },
      GPVRADIUS: {
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-GPVRADIUS',
        ],
        autowire: false,
        title: 'Sensor Virtual Radius',
        models: [],
        versions: [],
        genccc: false,
        services: {
          BROADBAND: {
            optionalTargets: [
              'DHCP-MAC',
              'SDSI',
            ],
          },
          'MOBILE-DATA': {
            requiredTargets: [
              'DHCP-MAC',
            ],
          },
        },
        x3deliveryOptions: null,
        xcipioAFType: 'GPVRADIUS',
        description: 'Sensor Virtual Radius',
      },
      NKSR: {
        title: 'Nokia SR 7750',
        description: 'Nokia SR 7750',
        xcipioAFType: 'NKSR',
        features: [
          'AF-NKSR',
        ],
        services: {
          'MOBILE-DATA': {
            requiredTargets: [
              'DHCP-MAC',
            ],
          },
          BROADBAND: {
            requiredTargets: [
              'DHCP-MAC',
              'SDSI',
            ],
          },
        },
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        x3deliveryOptions: null,
        versions: [],
        models: [
          'NOKIASR7750',
        ],
        autowire: true,
        genccc: false,
        supportedActions: [
          'audit',
        ],
      },
      SAMSUNG_MSC: {
        requiredTargets: [
          'MSISDN',
          'CASEIDMDN',
        ],
        collectionFunctions: [
          'CF-JSTD025A',
        ],
        features: [
          'AF-SMSG-UBCL',
        ],
        autowire: false,
        title: 'Samsung UbiCell',
        models: [],
        versions: [],
        genccc: true,
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        optionalTargets: [
          'CASEIDMIN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'SAMSUNG_MSC',
        description: 'Samsung UbiCell CDMA MSC and Wireless Softswitch (WSS) Access Function',
      },
      MV_SMSIWF: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-MV-SMS',
        ],
        autowire: true,
        title: 'Mavenir SMS IWF',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [
          'MSISDN',
          'IMEI',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'MV_SMSIWF',
        description: 'Mavenir Short Message Server (SMS) Interworking Function (IWF) Access Function',
      },
      ERK_SGW: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ERK-SGW',
        ],
        autowire: true,
        title: 'Ericsson SAEGW/SGW',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA-ROAMING',
        ],
        optionalTargets: [
          'IMEI',
          'MSISDN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_SGW',
        description: 'Ericsson System Architecture Evolution (SAE) SGW/PGW Access Function',
      },
      KODIAK_PTT: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-TIA-1072',
        ],
        features: [
          'AF-KODK-PTT',
        ],
        autowire: true,
        title: 'Kodiak Push-To-Talk',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'PTT',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'KODIAK_PTT',
        description: 'Kodiak Broadband 4G Push-To-Talk (PTT) Access Function',
      },
      ERK_PGW: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-ERK-SGW',
        ],
        autowire: false,
        title: 'Ericsson SAEGW/PGW',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_PGW',
        description: 'Ericsson System Architecture Evolution (SAE) SGW/PGW Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      ERK_5GAMF: {
        requiredTargets: [
          'GPSIMSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-ERK-5GAMF',
        ],
        autowire: true,
        title: 'Ericsson AMF',
        models: [],
        versions: [],
        httpVersions: ['HTTP/1.1', 'HTTP/2'],
        httpDefaultVersion: 'HTTP/2',
        genccc: false,
        services: [
          '5G-DATA',
        ],
        optionalTargets: [
          'SUPIIMSI',
          'GPSINAI',
          'SUPINAI',
          'SUCI',
          'PEIIMEI',
          'PEIIMEI_CHECKDIGIT',
          'PEIIMEISV',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'ERK_5GAMF',
        description: 'Ericsson Access and Mobility Management Function',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      PVOIP: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        features: [
          'AF-PVOIP',
        ],
        autowire: false,
        title: 'SS8 Passive VOIP Access Function',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VOLTE-4G-VOICE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'PVOIP',
        description: 'SS8 Passive VOIP Access Function',
      },
      NSN_SAEGW_PGW: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-ATIS-1000678',
        ],
        features: [
          'nsnsaegw',
        ],
        autowire: false,
        title: 'Nokia SAEGW/PGW',
        models: [
          'NSN_SAEGW',
        ],
        versions: [
          '7.02',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'STA_PGW',
        description: 'Nokia SAEGW/PGW',
        supportedActions: [
          'refresh',
        ],
      },
      STARENT_EHA: {
        requiredTargets: [],
        collectionFunctions: [
          'CF-JSTD025B',
          'CF-TIA-1072',
        ],
        features: [
          'AF-STARENT-EHA',
        ],
        autowire: false,
        title: 'Cisco/Starent EHA',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'CDMA2000-3G-DATA',
        ],
        optionalTargets: [
          'MDN',
          'LOGIN',
          'IP4',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'STARENT_EHA',
        description: 'Cisco/Starent Enterprise Home Agent (EHA) Access Function',
      },
      STA_GGSN: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-STARENT-GGSN',
        ],
        autowire: true,
        title: 'Cisco/Starent GGSN',
        models: [],
        versions: [
          '1.X',
          'R14',
          'R17',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [
          'IMEI',
          'MSISDN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'STA_GGSN',
        description: 'Cisco/Starent Gateway GPRS Support Node (GGSN) Access Function',
      },
      BRW_AAA: {
        requiredTargets: [
          'LOGIN',
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-JSTD025B',
        ],
        features: [
          'AF-BRW-AAA',
        ],
        autowire: false,
        title: 'Bridgewater AAA',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'CDMA2000-3G-DATA',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'BRW_AAA',
        description: 'Bridgewater AAA Server Access Function',
      },
      NSNHLR_SDM_PRGW: {
        requiredTargets: [
          'IMSI',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-NSN-HLR',
        ],
        autowire: true,
        title: 'Nokia-Siemens HLR/HSS (PrGW)',
        models: [
          'SDM_PRGW',
        ],
        versions: [
          '6.01',
          '7.3',
        ],
        genccc: false,
        services: [
          'LTE-4G-DATA',
        ],
        optionalTargets: [
          'MSISDN',
        ],
        x3deliveryOptions: null,
        xcipioAFType: 'NSNHLR',
        description: 'Nokia-Siemens Networks Home Location Register (HLR) PrGW Access Function',
        supportedActions: [
          'refresh',
        ],
      },
      MVNR_PGW: {
        requiredTargets: [
          'MSISDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-MV-PGW',
        ],
        autowire: true,
        title: 'Mavenir PGW',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'VZ-VISIBLE',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'MVNR_PGW',
        description: 'Mavenir Packet Data Network Gateway (PGW) Access Function',
      },
      GIZMO: {
        requiredTargets: [
          'MDN',
        ],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        features: [
          'AF-GIZMO',
        ],
        autowire: true,
        title: 'Gizmo',
        models: [],
        versions: [],
        genccc: false,
        services: [
          'LTE-GIZMO',
        ],
        optionalTargets: [],
        x3deliveryOptions: null,
        xcipioAFType: 'GIZMO',
        description: 'Gizmo Access Function',
        supportedActions: [
          'reset',
          'refresh',
        ],
      },
      STREAMWIDE: {
        title: 'Streamwide WM Server',
        description: 'Streamwide WM Server',
        xcipioAFType: 'STREAMWIDE',
        features: [
          'AF-STREAMWIDE',
        ],
        services: [
          'VOICEMAIL',
        ],
        requiredTargets: [
          'DN',
        ],
        optionalTargets: [],
        collectionFunctions: [
          'CF-ETSI-102232',
        ],
        x3deliveryOptions: null,
        versions: [],
        models: [],
        autowire: false,
        genccc: false,
      },
      ETSI_GIZMO: {
        title: 'ETSI Gizmo',
        description: 'ETSI Gizmo Access Function',
        xcipioAFType: 'ETSI_GIZMO',
        features: [
          'AF-ETSI-GIZMO',
        ],
        services: [
          'ETSI-GIZMO-SERVICE',
        ],
        requiredTargets: [
          'MDNwService',
        ],
        optionalTargets: [],
        collectionFunctions: [
          'CF-3GPP-33108',
        ],
        x3deliveryOptions: null,
        versions: [],
        models: [],
        autowire: true,
        genccc: false,
        supportedActions: [
          'audit',
          'reset',
          'refresh',
        ],
      },
    },
    collectionFunctions: {
      'CF-TIA-1072': {
        services: [
          'CDMA2000-3G-DATA',
          'PTT',
        ],
        versions: [
          'TIA1072',
        ],
        features: [
          'CF-ATIS-1000678',
        ],
        description: 'TIA-1072 Push-To-Talk over Cellular Collection Function',
        title: 'US cdma2000 PoC (TIA-1072)',
      },
      'CF-NORTEL': {
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        versions: [],
        features: [
          'CF-JSTD025',
        ],
        description: 'Delivery from Nortel to Collection Function',
        title: 'Delivery from Nortel',
      },
      'CF-3GPP-33108ROSE': {
        services: [
          'GSMCDMA-2G-VOICE',
          'GSMCDMA-2G-DATA',
          'LTE-4G-DATA',
          'LTE-GIZMO',
          'VZ-VISIBLE',
          'MCPTT',
          'LTE-IOT',
        ],
        versions: [],
        features: [
          'CF-3GPP-33108',
        ],
        description: 'ETSI 133.108 Collection Function with ROSE interface',
        title: 'ETSI LI (TS 133.108) over ROSE',
      },
      'CF-TIA-1066': {
        services: [
          'CDMA2000-3G-VOICE',
          'VOLTE-4G-VOICE',
        ],
        versions: [
          '2',
        ],
        features: [
          'CF-TIA-1066',
        ],
        description: 'TIA-1066 for cdma2000 VoIP Collection Function',
        title: 'US cdma2000 VoIP (TIA-1066)',
      },
      'CF-ATIS-0700005': {
        features: [
          'CF-ATIS-0700005',
        ],
        versions: [
          '0700005V1',
          '0700005V2',
        ],
        title: 'ATIS-0700005',
        services: [
          'VZ-VISIBLE',
          'VOLTE-4G-VOICE',
        ],
        supersedes: [
          'CF-TIA-1066',
        ],
        description: 'ATIS-0700005 Collection Function',
      },
      'CF-3GPP-33128': {
        services: [],
        versions: [
          '16.2',
        ],
        features: [
          'CF-3GPP-33128',
        ],
        description: '3GPP TS 33.128 Collection Function with TCP interfaces',
        title: '3GPP LI (TS 33.128) over TCP',
      },
      'CF-JSTD025B': {
        services: [
          'CDMA2000-3G-DATA',
        ],
        versions: [],
        features: [
          'CF-JSTD025',
        ],
        description: 'J-STD-025B Collection Function',
        title: 'US cdma2000 Wireless (J-STD-025B)',
      },
      'CF-JSTD025A': {
        services: [
          'GSMCDMA-2G-VOICE',
          'UMTS-3G-VOICE',
          'GSMCDMA-2G-DATA',
        ],
        versions: [],
        features: [
          'CF-JSTD025',
        ],
        description: 'J-STD-025A Collection Function',
        title: 'US Wireless/Wireline (J-STD-025A)',
      },
      'CF-3GPP-33108': {
        services: [
          'VOLTE-RCS',
          'VOLTE-4G-VOICE',
          'GSMCDMA-2G-VOICE',
          'GSMCDMA-2G-DATA',
          'LTE-4G-DATA',
          'LTE-4G-DATA-ROAMING',
          'LTE-GIZMO',
          'ETSI-GIZMO-SERVICE',
          'VZ-VISIBLE',
          'MCPTT',
          'LTE-IOT',
          '5G-DATA',
        ],
        versions: [
          '11.1',
          '12.7',
          '12.8',
          '12.12',
          '15.4',
          '15.6',
        ],
        features: [
          'CF-3GPP-33108',
        ],
        description: '3GPP 33.108 Collection Function with TCP or FTP interfaces',
        title: 'US 3GPP LI (TS 33.108) over TCP or FTP',
      },
      'CF-ETSI-102232': {
        services: [
          'VOLTE-RCS',
          'IP',
          'VOLTE-4G-VOICE',
          '5G-LTE-ADVANCED',
          '5G-DATA',
          'LTE-4G-DATA',
          'BROADBAND',
          'MOBILE-DATA',
          'VOICEMAIL',
          'OFFNET-VOICE',
          'SUPPLEMENTARY',
          'ROAMING',
          'SMS',
          'DATA',
          'VOICE-AND-IP-SMS',
          'MOBILE',
          'VOICE',
        ],
        versions: [
          '1.4.1',
          '2.2.1',
          '3.1.1',
          '3.3.1',
          '3.10.1',
          '3.15.1',
          '3.21.1',
        ],
        features: [
          'CF-ETSI-102232-TCP',
          'CF-ETSI-102232-FTP',
        ],
        description: 'ETSI 102 232 Collection Function',
        title: 'ETSI IP (TS 102 232)',
      },
      'CF-ATIS-1000013': {
        services: [
          'IP',
        ],
        versions: [
          '1',
          '2',
          '2015',
        ],
        features: [
          'CF-ATIS-1000013',
        ],
        description: 'ATIS-1000013',
        title: 'ATIS-1000013',
      },
      'CF-ATIS-1000678': {
        services: [
          'VOIP-VOICE',
        ],
        versions: [
          '2',
          '3',
          '4',
        ],
        features: [
          'CF-ATIS-1000678',
        ],
        description: 'ATIS-1000678 (T1.678) Collection Function',
        title: 'US Wireline VoIP (T1.678)',
      },
      'CF-AF-DIRECT': {
        services: [
          'GSMCDMA-2G-VOICE',
        ],
        versions: [],
        features: [
          'CF-JSTD025',
        ],
        description: 'Direct Delivery from Access Function',
        title: 'Direct Delivery from Access Function',
      },
    },
    targets: {
      CASEIDMIN: {
        description: 'Unique string identifying the intercept and its results for Xcipio to match the X2 and X3 data to the surveillance (based on MIN)',
        title: 'Case Identity/MIN',
        pattern: '^[^\\x00-\\x1f]{1,25}$',
        maxLength: 25,
        type: 'string',
        minLength: 1,
      },
      DN: {
        description: 'Telephone Directory Number',
        title: 'DN',
        pattern: '^[0-9]{1,15}$',
        maxLength: 15,
        type: 'string',
        minLength: 1,
      },
      MIN: {
        description: 'Mobile Identification Number',
        title: 'MIN',
        pattern: '^[0-9]{10}$',
        maxLength: 10,
        minLength: 10,
        type: 'string',
        example: '4085551212',
      },
      MSISDN: {
        description: 'Mobile Station ISDN Number as defined in ITU E.164',
        title: 'MSISDN',
        pattern: '^[1-9]{1}[0-9]{2,14}$',
        maxLength: 15,
        type: 'string',
        minLength: 3,
      },
      IP6: {
        description: 'IP address version 6 as defined in RFC 2460',
        title: 'IPv6 address',
        minLength: 2,
        maxLength: 39,
        type: 'string',
        example: '2601:198:0:1677::5',
      },
      IP4: {
        description: 'IP address version 4 as defined in RFC 791',
        title: 'IP4 address',
        pattern: '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$',
        maxLength: 15,
        type: 'string',
        example: '10.0.1.2',
        minLength: 7,
      },
      LOGIN: {
        items: {
          user: {
            type: 'string',
            title: 'User Name',
          },
        },
        type: 'array',
        description: 'Login name',
        title: 'Login',
      },
      EMAIL: {
        description: 'Email address as defined in RFC 5332',
        title: 'Email address',
        minLength: 6,
        maxLength: 256,
        type: 'string',
        example: 'john@ss8.com',
      },
      MDN: {
        items: {
          type: {
            enum: [
              {
                value: 'VoiceOnly',
                title: 'Voice Only',
              },
              {
                value: 'ChatOnly',
                title: 'Chat Only',
              },
              {
                value: 'VoiceandChat',
                title: 'Voice and Chat',
              },
            ],
            type: 'enum',
            title: 'Traffic Type',
          },
          value: {
            title: 'Value',
            pattern: '^[0-9]{10}$',
            maxLength: 10,
            type: 'string',
            example: '4085551212',
            minLength: 10,
          },
        },
        type: 'array',
        description: 'Mobile Directory Number',
        title: 'MDN',
      },
      CASEIDMDN: {
        description: 'Unique string identifying the intercept and its results for Xcipio to match the X2 and X3 data to the surveillance (based on MDN)',
        title: 'Case Identity/MDN',
        pattern: '^[^\\x00-\\x1f]{1,25}$',
        maxLength: 25,
        type: 'string',
        minLength: 1,
      },
      'RADIUS-CSI': {
        minLength: 3,
        maxLength: 256,
        type: 'string',
        description: 'RADIUS Calling-Station-Id Attribute per RFC 2865',
        title: 'RADIUS Calling Station ID',
      },
      'TEL-URI': {
        description: 'TEL URI as defined in RFC 3966',
        title: 'TEL URI',
        pattern: '^tel:\\+[1-9]{1}[0-9]{2,14}$',
        maxLength: 256,
        minLength: 5,
        type: 'string',
        example: 'tel:+18005551212',
      },
      TRUNK_CLLI: {
        description: 'Trunk Common Language Location Identifier',
        title: 'Trunk CCLI',
        pattern: '^[0-9a-zA-Z]{0,16}:[0-9]{0,15}$',
        maxLength: 30,
        minLength: 1,
        type: 'string',
        example: 'HSTNTXMOCG0:5103331212',
      },
      'DHCP-MAC': {
        description: '48-bit hardware address represented by six groups of two hexadecimal digits separated by dashes',
        title: 'MAC address',
        pattern: '^([0-9a-fA-F][0-9a-fA-F]-){5}([0-9a-fA-F][0-9a-fA-F])$',
        maxLength: 17,
        type: 'string',
        minLength: 17,
      },
      IMEI: {
        description: 'International Mobile Equipment Identity (IMEI) as defined in 3GPP TS 23.003',
        title: 'IMEI',
        pattern: '^[0-9]{8}[0-9]{7,8}$',
        maxLength: 16,
        type: 'string',
        example: 'IMEI(15 digits): 352066060926230 or IMEISV(16 digits): 3520660609262327',
        minLength: 15,
      },
      ESN: {
        description: 'Uniquely identifies a CDMA phone.  It is 11-digit decimal numbers. The first three digits are the decimal representation of the first eight bits (between 00 and 255 inclusive) and the next eight digits are derived from the remaining 24 bits and will be between 0000000 and 16777215 inclusive',
        title: 'Electronic Serial Number',
        pattern: '^[0-9]{11}$',
        maxLength: 11,
        type: 'string',
        minLength: 11,
      },
      'SIP-URI': {
        description: 'SIP URI as defined in RFC 3261',
        title: 'SIP URI',
        pattern: '^sip:\\S+@\\S+$',
        maxLength: 256,
        minLength: 5,
        type: 'string',
        example: 'sip:+18005551212@vzims.com',
      },
      MDNwService: {
        items: {
          type: {
            enum: [
              {
                value: 'voice',
                title: 'Voice Only',
              },
              {
                value: 'data',
                title: 'Data Only',
              },
              {
                value: 'voice|data',
                title: 'Voice and Data',
              },
            ],
            type: 'enum',
            title: 'Service Type',
          },
          value: {
            title: 'Value',
            pattern: '^[0-9]{10}$',
            maxLength: 10,
            type: 'string',
            example: '4085551212',
            minLength: 10,
          },
        },
        type: 'array',
        description: 'Mobile Directory Number',
        title: 'MDN',
      },
      CASEID: {
        description: 'Unique string identifying the intercept and its results for Xcipio to match the X2 and X3 data to the surveillance',
        title: 'Case Identity/LIID',
        pattern: '^[^\\x00-\\x1f]{1,25}$',
        maxLength: 25,
        type: 'string',
        minLength: 1,
      },
      PARTIAL_DN: {
        description: 'Partial Telephone Directory Number',
        title: 'Partial DN',
        pattern: '^[0-9]{1,12}$',
        maxLength: 12,
        type: 'string',
        minLength: 1,
      },
      'SIPS-URI': {
        description: 'SIP URI as defined in RFC 3261',
        title: 'SIPS URI',
        pattern: '^sips:\\S+@\\S+$',
        maxLength: 256,
        minLength: 5,
        type: 'string',
        example: 'sip:+18005551212@vzims.com',
      },
      EXTERNAL_ID: {
        description: 'External identifier used to facilitate communications with packet data networks and applications. Identifies a subscription associated with IMSI for UE in the form username@realm as specified in RFC 7542.',
        title: 'External ID',
        minLength: 6,
        maxLength: 256,
        type: 'string',
        example: '123456789@domain.com',
      },
      IMSI: {
        description: 'International Mobile Subscriber Identity (IMSI) as defined in E.212',
        title: 'IMSI',
        pattern: '^[1-9]{1}[0-9]{2}[0-9]{2,3}[0-9]{1,9}$',
        maxLength: 15,
        type: 'string',
        minLength: 6,
      },
    },
  },
};
