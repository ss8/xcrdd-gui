import { CaseModel } from 'data/case/case.types';

// eslint-disable-next-line import/prefer-default-export
export const mockCases: CaseModel[] = [{
  id: 'caseId1',
  dxOwner: 'T2',
  dxAccess: '',
  name: 't65',
  warrantId: {
    id: 'ID_TO_GET',
    name: 'warrant name',
  },
  liId: 'test li id',
  coId: 't65',
  device: '',
  services: [
    '5G-DATA',
  ],
  cfId: {
    id: 'Q0EtRE9KQk5FLTEwMDA2',
    name: 'CA-DOJBNE-1000678',
    cfId: 'CA-DOJBNE-1000678',
  },
  entryDateTime: '2020-12-01T13:47:00Z',
  hi2Interfaces: [
    {
      id: 'ZXRzaWlwLXYzLjIxLjEjMQ',
      name: 'ZXRzaWlwLXYzLjIxLjEjMQ',
    },
  ],
  hi3Interfaces: [],
  genccc: {
    leaphone1: '',
    leaphone2: '',
  },
  type: 'CC',
  startDateTime: '2020-12-02T05:00:00Z',
  stopDateTime: '2020-12-05T05:00:00Z',
  status: 'INACTIVE',
  subStatus: 'EXPIRED',
  timeZone: 'America/New_York',
  templateUsed: '',
  options: {
    kpi: false,
    traceLevel: 0,
    configureOptions: false,
    packetEnvelopeMessage: true,
    packetEnvelopeContent: true,
    realTimeText: true,
    callPartyNumberDisplay: true,
    directDigitExtractionData: true,
    monitoringReplacementPartiesAllowed: true,
    circuitSwitchedVoiceCombined: true,
    includeInBandOutBoundSignalling: true,
    encryptionType: 'NONE',
    encryptionKey: '',
    provideTargetIdentityInCcChannel: 'EMPTY',
    cccBillingType: 'FLATRATE',
    cccBillingNumber: '9999999999',
    deactivationMode: 'NORMAL',
    location: true,
    target: true,
    sms: true,
    callIndependentSupplementaryServicesContent: true,
    packetSummaryCount: 0,
    packetSummaryTimer: 0,
    generatePacketDataHeaderReport: false,
    packetHeaderInformationReportFilter: 'ALL',
    callTraceLogging: 0,
    featureStatusInterval: 15,
    surveillanceStatusInterval: 60,
  },
  targets: [
    {
      id: 'targetId1',
      name: 'targetId1',
    },
  ],
  reporting: {},
}, {
  id: 'caseId1',
  dxOwner: 'T2',
  dxAccess: '',
  name: 't65',
  warrantId: {
    id: 'ID_TO_GET',
    name: 'warrant name',
  },
  liId: 'test li id',
  coId: 't65',
  device: '',
  services: [
    '5G-DATA',
  ],
  cfId: {
    id: 'Q29va0NvdW50eS0xMDAw',
    name: 'CookCounty-1000678',
    cfId: 'CookCounty-1000678',
  },
  entryDateTime: '2020-12-01T13:47:00Z',
  hi2Interfaces: [
    {
      id: 'ZXRzaWlwLXYzLjIxLjEjMQ',
      name: 'ZXRzaWlwLXYzLjIxLjEjMQ',
    },
  ],
  hi3Interfaces: [],
  genccc: {
    leaphone1: '',
    leaphone2: '',
  },
  type: 'CC',
  startDateTime: '2020-12-02T05:00:00Z',
  stopDateTime: '2020-12-05T05:00:00Z',
  status: 'INACTIVE',
  subStatus: 'EXPIRED',
  timeZone: 'America/New_York',
  templateUsed: '',
  options: {
    kpi: false,
    traceLevel: 0,
    configureOptions: false,
    packetEnvelopeMessage: true,
    packetEnvelopeContent: true,
    realTimeText: true,
    callPartyNumberDisplay: true,
    directDigitExtractionData: true,
    monitoringReplacementPartiesAllowed: true,
    circuitSwitchedVoiceCombined: true,
    includeInBandOutBoundSignalling: true,
    encryptionType: 'NONE',
    encryptionKey: '',
    provideTargetIdentityInCcChannel: 'EMPTY',
    cccBillingType: 'FLATRATE',
    cccBillingNumber: '9999999999',
    deactivationMode: 'NORMAL',
    location: true,
    target: true,
    sms: true,
    callIndependentSupplementaryServicesContent: true,
    packetSummaryCount: 0,
    packetSummaryTimer: 0,
    generatePacketDataHeaderReport: false,
    packetHeaderInformationReportFilter: 'ALL',
    callTraceLogging: 0,
    featureStatusInterval: 15,
    surveillanceStatusInterval: 60,
  },
  targets: [
    {
      id: 'targetId2',
      name: 'targetId2',
    },
  ],
  reporting: {},
}];
