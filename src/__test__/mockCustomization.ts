import { Customization } from 'data/customization/customization.types';

// eslint-disable-next-line import/prefer-default-export
export const mockCustomization: Customization = {
  derivedTargets: {
    CASEIDMDN: {
      'GSMCDMA-2G-VOICE': { pattern: '{MDN}', parent: 'MDN', target: 'CASEID' },
    },
    CASEIDMIN: {
      'GSMCDMA-2G-VOICE': { pattern: '{MIN}', parent: 'MIN', target: 'CASEID' },
    },
    'SIP-URI': {
      '*': { pattern: 'sip:+1{MDN}@vzims.com', parent: 'MDN' },
    },
    'TEL-URI': {
      '*': { pattern: 'tel:+1{MDN}', parent: 'MDN' },
    },
    MSISDN: {
      '*': { pattern: '1{MDN}', parent: 'MDN' },
    },
  },
  collectionFunctionName: {
    template: '{{cfGroup}}-{{tagMap}}',
    tagMap: {
      'CF-3GPP-33108': '33108',
      'CF-ETSI-102232': 'ETSI',
      'CF-TIA-1072': '1072',
      'CF-TIA-1066': '1066',
      'CF-JSTD025A': 'J25A',
      'CF-JSTD025B': 'J25B',
      'CF-ATIS-1000678': 'ATIS',
    },
  },
  targetDefaultAFSelection: {
    'GSMCDMA-2G-VOICE': {
      CASEIDMDN: {
        selectionMethods: [{ method: 'selectNone' }],
        selectionRequired: false,
      },
      CASEIDMIN: {
        selectionMethods: [{ method: 'selectNone' }],
        selectionRequired: false,
      },
    },
  },
};
