import {
  FormTemplate,
  RawTemplate,
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
} from 'data/template/template.types';
import templateWarrant from '../../deployment/templates/warrant-create-template-andromeda.json';
import templateWarrantWizard from '../../deployment/templates/warrantWizard-create-template-andromeda.json';
import templateCaseOptions from '../../deployment/templates/caseOption-default-template-endeavor.json';
import templateCaseOptionsWithLocation from '../../deployment/templates/caseOption-options-with-location-template.json';
import templateCaseOptionsWithoutLocation from '../../deployment/templates/caseOption-options-without-location-template.json';
import templateEricssonUPF from '../../deployment/templates/accessFunction/access-function-erk-upf-template.json';
import templateEricssonSMF from '../../deployment/templates/accessFunction/access-function-erk-smf-template.json';
import templateMockAFMultiTab from '../../deployment/templates/accessFunction/access-function-pcrf-template.json';
import templateCiscoStarentPGW from '../../deployment/templates/accessFunction/access-function-sta-pgw-template.json';
import templateNokiaSimensCSCF from '../../deployment/templates/accessFunction/access-function-nsn-cscf-template.json';
import templateEricssonMCPTT from '../../deployment/templates/accessFunction/access-function-erk-mcptt-template.json';
import templateEricssonSGW from '../../deployment/templates/accessFunction/access-function-erk-sgw-template.json';
import templateAlcatelLucentMME from '../../deployment/templates/accessFunction/access-function-alu-mme-template.json';
import templateNokiaSimensHLRHSSPrGW from '../../deployment/templates/accessFunction/access-function-nsnhlr-sdm-prgw-template.json';
import templateMavenirPGW from '../../deployment/templates/accessFunction/access-function-mvnr-pgw-template.json';
import templateNokiaSimensHLRHSSFE from '../../deployment/templates/accessFunction/access-function-nsnhlr-template.json';
import templateNokiaSiemensSAEGWPGW from '../../deployment/templates/accessFunction/access-function-nsn-saegw-pgw-template.json';
import templateNokiaSiemensSAEGWSGW from '../../deployment/templates/accessFunction/access-function-nsn-saegw-template.json';
import templateAlcatelLucentDSS from '../../deployment/templates/accessFunction/access-function-alu-dss-template.json';
import templateMavenirSMSIFW from '../../deployment/templates/accessFunction/access-function-mv-smsiwf-template.json';
import templateCiscoPCRF from '../../deployment/templates/accessFunction/access-function-ciscovpcrf-template.json';
import template3GPP33108 from '../../deployment/templates/collectionFunction/collection-function-3gpp-33108-template.json';
import templateETSI102232 from '../../deployment/templates/collectionFunction/collection-function-etsi-102232-template.json';
import templateTIA1072 from '../../deployment/templates/collectionFunction/collection-function-tia-1072-template.json';
import templateTIA1066 from '../../deployment/templates/collectionFunction/collection-function-tia-1066-template.json';
import templateJSTDA from '../../deployment/templates/collectionFunction/collection-function-jstd025a-template.json';
import templateJSTDB from '../../deployment/templates/collectionFunction/collection-function-jstd025b-template.json';
import templateAtis1000678 from '../../deployment/templates/collectionFunction/collection-function-atis-1000678-template.json';
import templateAlcatelLucentWebGateway from '../../deployment/templates/accessFunction/access-function-lu3g-cscf-template.json';
import templateEricssonPGW from '../../deployment/templates/accessFunction/access-function-erk-pgw-template.json';
import templateOracleX123 from '../../deployment/templates/accessFunction/access-function-acme-x123-template.json';
import templateEricssonVMME from '../../deployment/templates/accessFunction/access-function-erk-vmme-template.json';
import templateMavenirSBC from '../../deployment/templates/accessFunction/access-function-mvnr-sbc-template.json';
import templateICSRCS from '../../deployment/templates/accessFunction/access-function-ics-rcs-template.json';
import templateMavenirCSCF from '../../deployment/templates/accessFunction/access-function-mvnr-cscf-template.json';
import templateMavenirAluSGW from '../../deployment/templates/accessFunction/access-function-alu-sgw-template.json';
import templateSonusSBC from '../../deployment/templates/accessFunction/access-function-sonus-sbc-template.json';
import templateETSI103221 from '../../deployment/templates/accessFunction/access-function-etsi103221-template.json';
import templateGizmo from '../../deployment/templates/accessFunction/access-function-gizmo-template.json';
import templateStreamwide from '../../deployment/templates/accessFunction/access-function-streamwide-template.json';
import templateEtsiGizmo from '../../deployment/templates/accessFunction/access-function-etsi-gizmo-template.json';
import templateEricssonAMF from '../../deployment/templates/accessFunction/access-function-erk-5gamf-template.json';
import templateNokiaUDM from '../../deployment/templates/accessFunction/access-function-nok-5gudm-template.json';
import templateNokiaSDM from '../../deployment/templates/accessFunction/access-function-nsn-sdm-template.json';
import templateNokiaAMF from '../../deployment/templates/accessFunction/access-function-nok-5gamf-template.json';
import templateNokiaSMF from '../../deployment/templates/accessFunction/access-function-nok-5gsmf-template.json';
import templateNokiaSMSF from '../../deployment/templates/accessFunction/access-function-nok-5gsmsf-template.json';
import templateMavenirSMSC from '../../deployment/templates/accessFunction/access-function-mvnr-5gsmsc-template.json';
import templateNokiaSR from '../../deployment/templates/accessFunction/access-function-nksr-template.json';
import templateNokiaSBC from '../../deployment/templates/accessFunction/access-function-nokia-sbc-template.json';
import templateGPVRADIUS from '../../deployment/templates/accessFunction/access-function-gpvradius-template.json';

export function findMockTemplates(
  mockTemplates: RawTemplate[],
  category: string,
  name: string | null = null,
): RawTemplate[] {
  return mockTemplates?.filter((rawTemplate) => (
    name === null ?
      rawTemplate.category === category :
      rawTemplate.category === category && rawTemplate.name === name));
}

let mockTemplateMapByKey: { [templateKey: string]: FormTemplate } = {};
export function getMockTemplateByKey(key: string): FormTemplate {
  if (Object.keys(mockTemplateMapByKey).length === 0) {
    mockTemplateMapByKey = mockTemplates.reduce((previousValue, { template }) => {
      const parsedTemplate = JSON.parse(template);

      return {
        ...previousValue,
        [parsedTemplate.key]: parsedTemplate,
      };
    }, {});
  }

  return mockTemplateMapByKey[key];
}

export function getMockTemplatesAccessFunctionStartIndex(): number {
  return mockTemplates.findIndex(({ category }) => category === TEMPLATE_CATEGORY_ACCESS_FUNCTION);
}

export function getMockTemplatesAccessFunctionEndIndex(): number {
  const accessFunctionTemplates = mockTemplates.filter(
    ({ category }) => category === TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  );
  return getMockTemplatesAccessFunctionStartIndex() + accessFunctionTemplates.length - 1;
}

export function getMockTemplatesCollectionFunctionStartIndex(): number {
  return mockTemplates.findIndex(({ category }) => category === TEMPLATE_CATEGORY_COLLECTION_FUNCTION);
}

export function getMockTemplatesCollectionFunctionEndIndex(): number {
  const collectionFunctionTemplates = mockTemplates.filter(
    ({ category }) => category === TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  );
  return getMockTemplatesCollectionFunctionStartIndex() + collectionFunctionTemplates.length - 1;
}

let nextTemplateId = 0;

/**
 * Attention!!!
 *
 * To make tests easier, please group Access Function and Collection Function templates
 */
export const mockTemplates: RawTemplate[] = [{
  category: 'warrants',
  categoryDefault: false,
  dateUpdated: '2020-08-21T07:19:42.000+0000',
  id: (nextTemplateId += 1).toString(),
  name: 'Default',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateWarrant),
}, {
  category: 'warrants-wizard',
  categoryDefault: false,
  dateUpdated: '2020-12-01T18:21:51.000+0000',
  id: (nextTemplateId += 1).toString(),
  name: 'Default',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateWarrantWizard),
}, {
  category: 'CaseOptions',
  categoryDefault: false,
  dateUpdated: '2020-08-21T07:19:43.000+0000',
  id: (nextTemplateId += 1).toString(),
  name: 'Default',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateCaseOptions),
}, {
  category: 'CaseOptions',
  categoryDefault: false,
  dateUpdated: '2020-08-21T07:19:43.000+0000',
  id: (nextTemplateId += 1).toString(),
  name: 'With Location',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateCaseOptionsWithLocation),
}, {
  category: 'CaseOptions',
  categoryDefault: false,
  dateUpdated: '2020-08-21T07:21:35.000+0000',
  id: (nextTemplateId += 1).toString(),
  name: 'Without Location',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateCaseOptionsWithoutLocation),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson UPF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonUPF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson SMF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonSMF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Oracle/Camiant PCRF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMockAFMultiTab),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Cisco/Starent PGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateCiscoStarentPGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia-Siemens CSCF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSimensCSCF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson MCPTT',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonMCPTT),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson SGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonSGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Alcatel-Lucent MME',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateAlcatelLucentMME),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia-Siemens HLR/HSS (PrGW)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSimensHLRHSSPrGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Alcatel-Lucent Distributed Soft Switch',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateAlcatelLucentDSS),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson SAEGW/PGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonPGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Mavenir SMS IWF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirSMSIFW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Oracle X123',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateOracleX123),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Mavenir PGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirPGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia-Siemens HLR/HSS (FE)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSimensHLRHSSFE),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SAEGW/PGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSiemensSAEGWPGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Alcatel-Lucent Web Gateway',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateAlcatelLucentWebGateway),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Cisco Virtual PCRF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateCiscoPCRF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SAEGW/SGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSiemensSAEGWSGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Mavenir P-SBC',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirSBC),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson vMME',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonVMME),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Mavenir P-CSCF/A-SBC',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirCSCF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'ICS RCS Server',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateICSRCS),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Alcatel-Lucent SGW',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirAluSGW),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Sonus SBC',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateSonusSBC),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: templateETSI103221.name,
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateETSI103221),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Gizmo',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateGizmo),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'STREAMWIDE',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateStreamwide),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Ericsson AMF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEricssonAMF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia UDM',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaUDM),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia HSS/SDM',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSDM),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia AMF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaAMF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SMF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSMF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SMSF',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSMSF),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Mavenir SMS Center',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateMavenirSMSC),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'ETSI_GIZMO',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateEtsiGizmo),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SR 7750',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSR),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Nokia SBC',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateNokiaSBC),
}, {
  category: TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'Virtual Radius',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateGPVRADIUS),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US 3GPP LI (TS 33.108)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(template3GPP33108),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'ETSI IP (TS 102 232)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateETSI102232),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US cdma2000 PoC (TIA-1072)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateTIA1072),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US cdma2000 VoIP (TIA-1066)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateTIA1066),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US Wireless/Wireline (J-STD-025-A)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateJSTDA),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US Wireless/Wireline (J-STD-025-B)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateJSTDB),
}, {
  category: TEMPLATE_CATEGORY_COLLECTION_FUNCTION,
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'US Wireline VoIP (ATIS-1000678)',
  owner: null,
  publicTemplate: true,
  template: JSON.stringify(templateAtis1000678),
}, {
  category: 'other category',
  categoryDefault: false,
  dateUpdated: '',
  id: (nextTemplateId += 1).toString(),
  name: 'other',
  owner: null,
  publicTemplate: false,
  template: '[]',
}];
