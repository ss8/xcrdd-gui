import { TlsProxy } from 'data/tlsProxy/tlsProxy.types';

// eslint-disable-next-line import/prefer-default-export
export const mockTlsProxies: TlsProxy[] = [
  {
    id: 'id1',
    name: 'TLS Name 1',
    nodeName: 'node-1',
    instanceId: '1',
    listeners: [{
      id: '1',
      ipAddress: '11.11.11.11',
      port: '1111',
      requiredState: 'INACTIVE',
      keepaliveTimeout: '300',
      maxConcurrentConnection: '256',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
      routeRule: 'INTERCEPT',
    }],
    destinations: [{
      id: '1',
      ipAddress: '22.22.22.22',
      port: '2222',
      requiredState: 'ACTIVE',
      keepaliveInterval: '60',
      numOfConnections: '5',
      secInfoId: 'Tk9ORQ',
      transport: 'TCP',
    }],
  },
  {
    id: 'id2',
    name: 'TLS Name 2',
    nodeName: 'node-2',
    instanceId: '1',
    listeners: [],
    destinations: [],
  },
];
