import { Ss8ProxyDefault } from 'data/ss8ProxyDefault/ss8ProxyDefault.types';

// eslint-disable-next-line import/prefer-default-export
export const mockSs8ProxyDefaults: Ss8ProxyDefault[] = [
  {
    resource: 'SS8_PROXY_DEFAULTS',
    globalDefaultValues: {
      listeners: [
        { field: 'keepaliveTimeout', defaultvalue: 300 },
        { field: 'maxConcurrentConnection', defaultvalue: 128 },
        { field: 'transport', defaultvalue: 'TCP' },
        { field: 'requiredState', defaultvalue: 'ACTIVE' },
        { field: 'routeRule', defaultvalue: 'SRCADDR' },
      ],
      destinations: [
        { field: 'keepaliveInterval', defaultvalue: 60 },
        { field: 'keepaliveRetries', defaultvalue: 6 },
        { field: 'numOfConnections', defaultvalue: 1 },
        { field: 'transport', defaultvalue: 'TCP' },
      ],
    },
    ss8Proxies: [{
      instanceId: 'instanceId1',
    }, {
      instanceId: 'instanceId2',
    }],
  },
];
