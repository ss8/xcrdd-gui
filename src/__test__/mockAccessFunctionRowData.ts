import { AccessFunction, AccessFunctionGridRowData } from 'data/accessFunction/accessFunction.types';

export const mockAccessFunctions: AccessFunction[] = [
  {
    id: 'ID_0',
    name: 'AF_ID_0',
    tag: 'ALU_DSS',
    timeZone: '',
    type: 'ALU_DSS',
    provisioningInterfaces: [
      {
        id: 'MSMx',
        name: 'MSMx',
        options: [{
          key: 'afid',
          value: '1',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'ownnetid',
          value: 'WENQLWNvbnRhaW5lcg',
        }, {
          key: 'ownIPPort',
          value: '111',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.1',
        }, {
          key: 'destPort',
          value: '2211',
        }, {
          key: 'timeout',
          value: '10',
        }, {
          key: 'imsi',
          value: 'Y',
        }, {
          key: 'imei',
          value: 'Y',
        }, {
          key: 'msisdn',
          value: 'Y',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'lis',
        }, {
          key: 'group',
          value: '',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
      {
        id: 'MSMx',
        name: 'MSMx',
        options: [{
          key: 'afid',
          value: '1',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'ownnetid',
          value: 'WENQLWNvbnRhaW5lcg',
        }, {
          key: 'ownIPPort',
          value: '222',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.2',
        }, {
          key: 'destPort',
          value: '2211',
        }, {
          key: 'timeout',
          value: '10',
        }, {
          key: 'imsi',
          value: 'Y',
        }, {
          key: 'imei',
          value: 'Y',
        }, {
          key: 'msisdn',
          value: 'Y',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'lis',
        }, {
          key: 'group',
          value: '',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [],
    contentInterfaces: [],
  },
  {
    id: 'ID_1',
    name: 'AF_ID_1',
    tag: 'ALU_DSS',
    timeZone: '',
    type: 'ALU_DSS',
    provisioningInterfaces: [
      {
        id: 'MiMx',
        name: 'MiMx',
        options: [{
          key: 'afid',
          value: '2',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'ownnetid',
          value: 'WENQcmNz',
        }, {
          key: 'ownIPPort',
          value: '333',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.4',
        }, {
          key: 'destPort',
          value: '11',
        }, {
          key: 'timeout',
          value: '10',
        }, {
          key: 'imsi',
          value: 'Y',
        }, {
          key: 'imei',
          value: 'Y',
        }, {
          key: 'msisdn',
          value: 'Y',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'dscp',
          value: '61',
        }, {
          key: 'owner',
          value: 'sysadm',
        }, {
          key: 'group',
          value: 'lis',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [
      {
        id: 'MiMx',
        name: 'MiMx',
        options: [{
          key: 'afid',
          value: '2',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'mscid',
          value: '123',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.5',
        }, {
          key: 'destPort',
          value: '0',
        }, {
          key: 'ownnetid',
          value: 'WENQLWNvbnRhaW5lcg',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'INACTIVE',
        }, {
          key: 'owner',
          value: 'lis',
        }, {
          key: 'group',
          value: '',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    contentInterfaces: [],
  },
  {
    id: 'ID_2',
    name: 'AF_ID_2',
    tag: 'ALU_SGW',
    timeZone: '',
    type: 'ALU_SGW',
    contentDeliveryVia: 'directFromAF',
    provisioningInterfaces: [
      {
        id: 'QUxVX1NHVyMx',
        name: 'QUxVX1NHVyMx',
        options: [{
          key: 'afid',
          value: 'ALU_SGW',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.6',
        }, {
          key: 'destPort',
          value: '22',
        }, {
          key: 'conntype',
          value: 'SSH',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'dfid',
          value: '1',
        }, {
          key: 'userName',
          value: 'test',
        }, {
          key: 'password',
          value: '*****',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'command',
          value: 'NONE',
        }, {
          key: 'respfilename',
          value: '',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [
      {
        id: 'QUxVX1NHVyMx',
        name: 'QUxVX1NHVyMx',
        options: [{
          key: 'afid',
          value: 'ALU_SGW',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.7',
        }, {
          key: 'destPort',
          value: '0',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'INACTIVE',
        }, {
          key: 'ownnetid',
          value: 'WENQcmNz',
        }, {
          key: 'tpkt',
          value: 'N',
        }, {
          key: 'secinfoid',
          value: 'Tk9ORQ',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    contentInterfaces: [
      {
        id: 'QUxVX1NHVyMx',
        name: 'QUxVX1NHVyMx',
        options: [{
          key: 'afid',
          value: 'ALU_SGW',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.9',
        }, {
          key: 'destPort',
          value: '0',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'INACTIVE',
        }, {
          key: 'ownnetid',
          value: 'WENQcmNz',
        }, {
          key: 'tpkt',
          value: 'N',
        }, {
          key: 'keepalive',
          value: 'Y',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_3',
    name: 'AF_ID_3',
    insideNat: false,
    timeZone: 'America/Tijuana',
    type: 'ERK_UPF',
    contentDeliveryVia: 'ss8Distributor',
    tag: 'ERK_UPF',
    provisioningInterfaces: [],
    dataInterfaces: [],
    contentInterfaces: [],
  },
  {
    id: 'ID_4',
    name: 'AF_ID_4',
    insideNat: false,
    model: '',
    timeZone: 'America/Tijuana',
    type: 'ERK_SMF',
    contentDeliveryVia: 'ss8Distributor',
    tag: 'ERK_SMF',
    provisioningInterfaces: [
      {
        id: 'ZXJrc21mX2FmMSMx',
        name: 'ZXJrc21mX2FmMSMx',
        options: [{
          key: 'afid',
          value: 'Ericsson SMF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'destIPAddr',
          value: '83.83.83.83',
        }, {
          key: 'destPort',
          value: '33310',
        }, {
          key: 'ownnetid',
          value: 'WENQcmNz',
        }, {
          key: 'secinfoid',
          value: 'Q2xpZW50U2ltcGxl',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'dscp',
          value: '32',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [],
    contentInterfaces: [],
  },
  {
    id: 'ID_5',
    name: 'AF_ID_5',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'uri',
          value: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
        }, {
          key: 'destPort',
          value: '6443',
        }, {
          key: 'ownipaddr',
          value: '1.2.3.4',
        }, {
          key: 'ownIPPort',
          value: '0',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'cdnetid',
          value: 'WENQcmNz',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'INACTIVE',
        }, {
          key: 'destIPAddr',
          value: '17.17.17.17',
        }, {
          key: 'destPort',
          value: '0',
        }, {
          key: 'ownipaddr',
          value: '1.2.3.4',
        }, {
          key: 'ownIPPort',
          value: '51500',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    contentInterfaces: [],
  },
  {
    id: 'ID_6',
    name: 'AF_ID_6',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'uri',
          value: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
        }, {
          key: 'destPort',
          value: '6443',
        }, {
          key: 'ownIPAddr',
          value: '2.2.3.4',
        }, {
          key: 'ownIPPort',
          value: '1000',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_7',
    name: 'AF_ID_7',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'ownipaddr',
          value: '2.2.2.4',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_8',
    name: 'AF_ID_8',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'ipduid',
          value: 'MTU',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_9',
    name: 'AF_ID_9',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'cdnetid',
          value: 'WENQdjY',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_10',
    name: 'AF_ID_10',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'destIPAddr',
          value: 'ANY_ADDR',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_11',
    name: 'AF_ID_11',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'uri',
          value: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
        }, {
          key: 'destPort',
          value: '6443',
        }, {
          key: 'ownIPAddr',
          value: 'ANY_ADDR',
        }, {
          key: 'ownIPPort',
          value: '1000',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_12',
    name: 'AF_ID_12',
    insideNat: false,
    model: 'CISCOVPCRF',
    timeZone: 'America/Tijuana',
    type: 'PCRF',
    contentDeliveryVia: 'directFromAF',
    tag: 'CISCOVPCRF',
    provisioningInterfaces: [
      {
        id: 'Q2lzY292UENSRiMx',
        name: 'Q2lzY292UENSRiMx',
        options: [{
          key: 'afid',
          value: 'CiscovPCRF',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'secinfoid',
          value: 'SFRUUFNfU2VydmVyX1NpbQ',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'uri',
          value: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
        }, {
          key: 'destPort',
          value: '6443',
        }, {
          key: 'ownipaddr',
          value: 'ADDR_ANY',
        }, {
          key: 'ownIPPort',
          value: '1000',
        }, {
          key: 'interval',
          value: '30',
        }, {
          key: 'traceLevel',
          value: '0',
        }, {
          key: 'ccdestip',
          value: '::',
        }, {
          key: 'ccsourceaftype',
          value: 'STA_PGW',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'ou1',
        }, {
          key: 'group',
          value: 'og1',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
  },
  {
    id: 'ID_13',
    name: 'AF_ID_13',
    tag: 'ALU_DSS',
    timeZone: '',
    type: 'ALU_DSS',
    provisioningInterfaces: [
      {
        id: 'MSMx',
        name: 'MSMx',
        options: [{
          key: 'afid',
          value: '1',
        }, {
          key: 'ifid',
          value: '1',
        }, {
          key: 'ownnetid',
          value: 'xxxxxxxxx',
        }, {
          key: 'ownIPPort',
          value: '111',
        }, {
          key: 'destIPAddr',
          value: '1.1.1.1',
        }, {
          key: 'destPort',
          value: '2211',
        }, {
          key: 'timeout',
          value: '10',
        }, {
          key: 'imsi',
          value: 'Y',
        }, {
          key: 'imei',
          value: 'Y',
        }, {
          key: 'msisdn',
          value: 'Y',
        }, {
          key: 'reqState',
          value: 'ACTIVE',
        }, {
          key: 'state',
          value: 'FAILED',
        }, {
          key: 'dscp',
          value: '0',
        }, {
          key: 'owner',
          value: 'lis',
        }, {
          key: 'group',
          value: '',
        }, {
          key: 'access',
          value: 'GG',
        }],
      },
    ],
    dataInterfaces: [],
    contentInterfaces: [],
  },
  {
    id: 'ID_14',
    name: 'AF_ID_14',
    timeZone: 'America/Tijuana',
    type: 'ETSI103221',
    tag: 'ETSI103221',
    provisioningInterfaces: [
      {
        id: 'RVRTSTEwMzIyMVgxQiMxI0lOSTE',
        name: 'RVRTSTEwMzIyMVgxQiMxI0lOSTE',
        options: [
          {
            key: 'afid',
            value: 'ETSI103221X1B',
          }, {
            key: 'ifid',
            value: '1',
          }, {
            key: 'litype',
            value: 'INI1',
          }, {
            key: 'reqState',
            value: 'ACTIVE',
          }, {
            key: 'state',
            value: 'FAILED',
          }, {
            key: 'ownnetid',
            value: 'U1M4XzMwNzA',
          }, {
            key: 'appProtocol',
            value: 'ETSI103221',
          }, {
            key: 'version',
            value: '1.3.1',
          }, {
            key: 'traceLevel',
            value: '1',
          }, {
            key: 'destIPAddr',
            value: '',
          }, {
            key: 'destPort',
            value: '0',
          }, {
            key: 'role',
            value: 'CLIENT',
          }, {
            key: 'dscp',
            value: '0',
          }, {
            key: 'transport',
            value: 'HTTP',
          }, {
            key: 'uri',
            value: 'https://34.45.45.45/li',
          }, {
            key: 'tpkt',
            value: 'N',
          }, {
            key: 'secinfoid',
            value: 'Q2xpZW50U2ltcGxl',
          }, {
            key: 'encryptKey',
            value: '*****',
          }, {
            key: 'encryptMethod',
            value: 'NONE',
          }, {
            key: 'sshInfoId',
            value: '',
          }, {
            key: 'errormsg',
            value: '',
          }, {
            key: 'owner',
            value: 'ou1',
          }, {
            key: 'group',
            value: 'og1',
          }, {
            key: 'access',
            value: 'GG',
          }, {
            key: 'keepaliveinterval',
            value: '60',
          }, {
            key: 'litargetnamespace',
            value: 'http://uri.etsi.org/03221/X1/2017/10',
          },
        ],
      },
    ],
    dataInterfaces: [
      {
        id: 'RVRTSTEwMzIyMVgxQiMxI0lOSTI',
        name: 'RVRTSTEwMzIyMVgxQiMxI0lOSTI',
        options: [
          {
            key: 'afid',
            value: 'ETSI103221X1B',
          }, {
            key: 'ifid',
            value: '1',
          }, {
            key: 'litype',
            value: 'INI2',
          }, {
            key: 'reqState',
            value: 'ACTIVE',
          }, {
            key: 'state',
            value: 'INACTIVE',
          }, {
            key: 'ownnetid',
            value: 'U1M4XzMwNzA',
          }, {
            key: 'appProtocol',
            value: '',
          }, {
            key: 'version',
            value: '',
          }, {
            key: 'traceLevel',
            value: '1',
          }, {
            key: 'destIPAddr',
            value: '',
          }, {
            key: 'destPort',
            value: '0',
          }, {
            key: 'role',
            value: 'SERVER',
          }, {
            key: 'dscp',
            value: '0',
          }, {
            key: 'transport',
            value: 'E164',
          }, {
            key: 'uri',
            value: '1234567890',
          }, {
            key: 'tpkt',
            value: 'N',
          }, {
            key: 'secinfoid',
            value: '',
          }, {
            key: 'encryptKey',
            value: '*****',
          }, {
            key: 'encryptMethod',
            value: 'NONE',
          }, {
            key: 'sshInfoId',
            value: '',
          }, {
            key: 'errormsg',
            value: '',
          }, {
            key: 'owner',
            value: 'ou1',
          }, {
            key: 'group',
            value: 'og1',
          }, {
            key: 'access',
            value: 'GG',
          },
        ],
      },
    ],
    contentInterfaces: [
      {
        id: 'RVRTSTEwMzIyMVgxQiMxI0lOSTM',
        name: 'RVRTSTEwMzIyMVgxQiMxI0lOSTM',
        options: [
          {
            key: 'afid',
            value: 'ETSI103221X1B',
          }, {
            key: 'ifid',
            value: '1',
          }, {
            key: 'litype',
            value: 'INI3',
          }, {
            key: 'reqState',
            value: 'ACTIVE',
          }, {
            key: 'state',
            value: 'INACTIVE',
          }, {
            key: 'ownnetid',
            value: 'U1M4XzMwNzA',
          }, {
            key: 'appProtocol',
            value: '',
          }, {
            key: 'version',
            value: '',
          }, {
            key: 'traceLevel',
            value: '1',
          }, {
            key: 'destIPAddr',
            value: '',
          }, {
            key: 'destPort',
            value: '0',
          }, {
            key: 'role',
            value: 'SERVER',
          }, {
            key: 'dscp',
            value: '0',
          }, {
            key: 'transport',
            value: 'TCP',
          }, {
            key: 'uri',
            value: '',
          }, {
            key: 'tpkt',
            value: 'N',
          }, {
            key: 'secinfoid',
            value: '',
          }, {
            key: 'encryptKey',
            value: '*****',
          }, {
            key: 'encryptMethod',
            value: 'NONE',
          }, {
            key: 'sshInfoId',
            value: '',
          }, {
            key: 'errormsg',
            value: '',
          }, {
            key: 'owner',
            value: 'ou1',
          }, {
            key: 'group',
            value: 'og1',
          }, {
            key: 'access',
            value: 'GG',
          },
        ],
      },
    ],
  },
  {
    id: 'ID_15',
    name: 'AF_ID_15',
    timeZone: 'America/Tijuana',
    type: 'ETSI103221',
    tag: 'ETSI103221',
    contentDeliveryVia: 'directFromAF',
    provisioningInterfaces: [],
    dataInterfaces: [],
    contentInterfaces: [],
  },
];

export const mockAccessFunctionsRowData: AccessFunctionGridRowData[] = [
  {
    id: 'ID_0',
    name: 'AF_ID_0',
    tag: 'ALU_DSS',
    subType: 'X1',
    ipAddress: '1.1.1.1',
    port: '2211',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    destination: 'XCP-container',
    destinationToolTip: 'XCP-container - 0.0.0.0',
    tagName: 'Alcatel-Lucent Distributed Soft Switch',
    provisioningInterfaces: mockAccessFunctions[0]?.provisioningInterfaces,
    afid: '1',
  },
  {
    id: 'ID_0',
    name: 'AF_ID_0',
    tag: 'ALU_DSS',
    subType: 'X1',
    ipAddress: '1.1.1.2',
    port: '2211',
    destination: 'XCP-container',
    destinationToolTip: 'XCP-container - 0.0.0.0',
    tagName: 'Alcatel-Lucent Distributed Soft Switch',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[0]?.provisioningInterfaces,
    afid: '1',
  },
  {
    id: 'ID_1',
    name: 'AF_ID_1',
    tag: 'ALU_DSS',
    subType: 'X1',
    ipAddress: '1.1.1.4',
    port: '11',
    destination: 'XCPrcs',
    destinationToolTip: 'XCPrcs - 11.11.11.11',
    tagName: 'Alcatel-Lucent Distributed Soft Switch',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[1]?.provisioningInterfaces,
    afid: '2',
  },
  {
    id: 'ID_1',
    name: 'AF_ID_1',
    tag: 'ALU_DSS',
    subType: 'X2',
    ipAddress: '1.1.1.5',
    port: '0',
    destination: 'XCP-container',
    destinationToolTip: 'XCP-container - 0.0.0.0',
    tagName: 'Alcatel-Lucent Distributed Soft Switch',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[1]?.provisioningInterfaces,
    afid: '2',
  },
  {
    id: 'ID_2',
    name: 'AF_ID_2',
    tag: 'ALU_SGW',
    subType: 'X1',
    ipAddress: '1.1.1.6',
    port: '22',
    destination: '',
    destinationToolTip: '',
    tagName: 'Alcatel-Lucent Serving Gateway',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[2]?.provisioningInterfaces,
    afid: 'ALU_SGW',
  },
  {
    id: 'ID_2',
    name: 'AF_ID_2',
    tag: 'ALU_SGW',
    subType: 'X2',
    ipAddress: '1.1.1.7',
    port: '0',
    destination: 'XCPrcs',
    destinationToolTip: 'XCPrcs - 11.11.11.11:0',
    tagName: 'Alcatel-Lucent Serving Gateway',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[2]?.provisioningInterfaces,
    afid: 'ALU_SGW',
  },
  {
    id: 'ID_2',
    name: 'AF_ID_2',
    tag: 'ALU_SGW',
    subType: 'X3',
    ipAddress: '1.1.1.9',
    port: '0',
    destination: 'XCPrcs',
    destinationToolTip: 'XCPrcs - 11.11.11.11',
    tagName: 'Alcatel-Lucent Serving Gateway',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[2]?.provisioningInterfaces,
    afid: 'ALU_SGW',
  },
  {
    id: 'ID_3',
    name: 'AF_ID_3',
    tag: 'ERK_UPF',
    subType: 'X3',
    ipAddress: '',
    port: '',
    destination: '102.2.2.2 and 1 other',
    destinationToolTip: '102.2.2.2, 103.3.3.3',
    tagName: 'Ericsson UPF',
    configuredState: '',
    state: '',
    provisioningInterfaces: [],
    afid: '',
  },
  {
    id: 'ID_4',
    name: 'AF_ID_4',
    tag: 'ERK_SMF',
    subType: 'X1',
    ipAddress: '83.83.83.83',
    port: '33310',
    destination: 'XCPrcs',
    destinationToolTip: 'XCPrcs - 11.11.11.11:0',
    tagName: 'Ericsson SMF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[4]?.provisioningInterfaces,
    afid: 'Ericsson SMF',
  },
  {
    id: 'ID_4',
    name: 'AF_ID_4',
    tag: 'ERK_SMF',
    subType: 'X2',
    ipAddress: '',
    port: '',
    destination: '27.0.0.1',
    destinationToolTip: '27.0.0.1',
    tagName: 'Ericsson SMF',
    configuredState: '',
    state: '',
    provisioningInterfaces: mockAccessFunctions[4]?.provisioningInterfaces,
    afid: '',
  },
  {
    id: 'ID_4',
    name: 'AF_ID_4',
    tag: 'ERK_SMF',
    subType: 'X3',
    ipAddress: '',
    port: '',
    destination: '102.2.2.2 and 1 other',
    destinationToolTip: '102.2.2.2, 103.3.3.3',
    tagName: 'Ericsson SMF',
    configuredState: '',
    state: '',
    provisioningInterfaces: mockAccessFunctions[4]?.provisioningInterfaces,
    afid: '',
  },
  {
    id: 'ID_5',
    name: 'AF_ID_5',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
    port: '6443',
    destinationToolTip: '1.2.3.4:0',
    destination: '1.2.3.4:0',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[5]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_5',
    name: 'AF_ID_5',
    tag: 'CISCOVPCRF',
    subType: 'X2',
    ipAddress: '17.17.17.17',
    port: '0',
    destinationToolTip: '1.2.3.4:51500',
    destination: '1.2.3.4:51500',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[5]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_6',
    name: 'AF_ID_6',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
    port: '6443',
    destinationToolTip: '2.2.3.4:1000',
    destination: '2.2.3.4:1000',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[6]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_7',
    name: 'AF_ID_7',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: '',
    port: '',
    destinationToolTip: '',
    destination: '2.2.2.4',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[7]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_8',
    name: 'AF_ID_8',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: '',
    port: '',
    destinationToolTip: '',
    destination: '15',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[8]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_9',
    name: 'AF_ID_9',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: '',
    port: '',
    destinationToolTip: 'XCPv6 - [abcd:abcd:abcd:abcd::abcd]',
    destination: 'XCPv6',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[9]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_10',
    name: 'AF_ID_10',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: '0.0.0.0',
    port: '',
    destinationToolTip: '',
    destination: '',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[10]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_11',
    name: 'AF_ID_11',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
    port: '6443',
    destinationToolTip: '0.0.0.0:1000',
    destination: '0.0.0.0:1000',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[11]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_12',
    name: 'AF_ID_12',
    tag: 'CISCOVPCRF',
    subType: 'X1',
    ipAddress: 'https://17.17.17.17/cgi-bin/PCRF/PCRF.py',
    port: '6443',
    destinationToolTip: '0.0.0.0:1000',
    destination: '0.0.0.0:1000',
    tagName: 'Cisco Virtual PCRF',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[12]?.provisioningInterfaces,
    afid: 'CiscovPCRF',
  },
  {
    id: 'ID_13',
    name: 'AF_ID_13',
    tag: 'ALU_DSS',
    subType: 'X1',
    ipAddress: '1.1.1.1',
    port: '2211',
    destinationToolTip: '',
    destination: 'NETWORK_ID_NOT_FOUND',
    tagName: 'Alcatel-Lucent Distributed Soft Switch',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[13]?.provisioningInterfaces,
    afid: '1',
  },
  {
    id: 'ID_14',
    name: 'AF_ID_14',
    tag: 'ETSI103221',
    subType: 'X1',
    ipAddress: 'https://34.45.45.45/li',
    port: '0',
    destinationToolTip: '',
    destination: 'NETWORK_ID_NOT_FOUND',
    tagName: 'ETSI 103 221',
    configuredState: 'ACTIVE',
    state: 'FAILED',
    provisioningInterfaces: mockAccessFunctions[14]?.provisioningInterfaces,
    afid: 'ETSI103221X1B',
  },
  {
    id: 'ID_14',
    name: 'AF_ID_14',
    tag: 'ETSI103221',
    subType: 'X2',
    ipAddress: '1234567890',
    port: '0',
    destinationToolTip: '',
    destination: '',
    tagName: 'ETSI 103 221',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[14]?.provisioningInterfaces,
    afid: 'ETSI103221X1B',
  },
  {
    id: 'ID_14',
    name: 'AF_ID_14',
    tag: 'ETSI103221',
    subType: 'X3',
    ipAddress: '',
    port: '0',
    destinationToolTip: '',
    destination: 'NETWORK_ID_NOT_FOUND',
    tagName: 'ETSI 103 221',
    configuredState: 'ACTIVE',
    state: 'INACTIVE',
    provisioningInterfaces: mockAccessFunctions[14]?.provisioningInterfaces,
    afid: 'ETSI103221X1B',
  },
  {
    id: 'ID_15',
    name: 'AF_ID_15',
    tag: 'ETSI103221',
    subType: 'X2',
    ipAddress: '',
    port: '',
    destinationToolTip: '',
    destination: '',
    tagName: 'ETSI 103 221',
    configuredState: '',
    state: '',
    provisioningInterfaces: mockAccessFunctions[15]?.provisioningInterfaces,
    afid: '',
  },
  {
    id: 'ID_15',
    name: 'AF_ID_15',
    tag: 'ETSI103221',
    subType: 'X3',
    ipAddress: '',
    port: '',
    destinationToolTip: '',
    destination: '',
    tagName: 'ETSI 103 221',
    configuredState: '',
    state: '',
    provisioningInterfaces: mockAccessFunctions[15]?.provisioningInterfaces,
    afid: '',
  },
];
