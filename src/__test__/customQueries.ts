import {
  queryHelpers,
  buildQueries,
  Matcher,
  MatcherOptions,
  AllByBoundAttribute,
} from '@testing-library/react';

/**
 * The usual 'data-test*' attribute to be used in components is the
 * 'data-testid', which is supported by default in the testing-library.
 * However the current codebase already make use of 'data-test', so in
 * order to avoid changing that now, these custom queries were implemented.
 * In the future 'data-test' should be replaced by 'data-testid' and
 * the following custom queries should be removed to make use of *ByTestId.
 */
const queryAllByDataTest: AllByBoundAttribute = (
  container: HTMLElement,
  id: Matcher,
  options?: MatcherOptions | undefined,
) => queryHelpers.queryAllByAttribute('data-test', container, id, options);

const getMultipleError = (_container: HTMLElement, dataValue: unknown) =>
  `Found multiple elements with the data-test attribute of: ${dataValue}`;
const getMissingError = (_container: HTMLElement, dataValue: unknown) =>
  `Unable to find an element with the data-test attribute of: ${dataValue}`;

const [
  queryByDataTest,
  getAllByDataTest,
  getByDataTest,
  findAllByDataTest,
  findByDataTest,
] = buildQueries(queryAllByDataTest, getMultipleError, getMissingError);

const queryAllByName: AllByBoundAttribute = (
  container: HTMLElement,
  id: Matcher,
  options?: MatcherOptions | undefined,
) => queryHelpers.queryAllByAttribute('name', container, id, options);

const getByNameMultipleError = (_container: HTMLElement, dataValue: unknown) =>
  `Found multiple elements with the name attribute of: ${dataValue}`;
const getByNameMissingError = (_container: HTMLElement, dataValue: unknown) =>
  `Unable to find an element with the name attribute of: ${dataValue}`;

const [
  queryByName,
  getAllByName,
  getByName,
  findAllByName,
  findByName,
] = buildQueries(queryAllByName, getByNameMultipleError, getByNameMissingError);

export {
  queryByDataTest,
  getAllByDataTest,
  getByDataTest,
  findAllByDataTest,
  findByDataTest,
  queryByName,
  getAllByName,
  getByName,
  findAllByName,
  findByName,
};
