import { Xtp } from 'data/xtp/xtp.types';

// eslint-disable-next-line import/prefer-default-export
export const mockXtps: Xtp[] = [
  {
    id: 'weq',
    lbId: '1',
    ipAddr: '12.3.2.1',
  },
  {
    id: 'awef',
    lbId: '2',
    ipAddr: '1.33.21.13',
  },
  {
    id: 'lkh',
    lbId: '3',
    ipAddr: '92.23.5.9',
  },
];
