import { AxiosResponse } from 'axios';
import { ServerResponse } from 'data/types';
import CommonGrid from 'shared/commonGrid';
import { ReactWrapper } from 'enzyme';

export function buildAxiosServerResponse<T>(data: T[] | null = null): AxiosResponse<ServerResponse<T>> {
  return {
    data: buildServerResponse(data),
    status: 200,
    statusText: '',
    headers: {},
    config: {},
  };
}

export function buildServerResponse<T>(data: T[] | null = null): ServerResponse<T> {
  return {
    success: true,
    metadata: {
      recordCount: data?.length || 0,
      recordsInView: data?.length || 0,
      idCreated: '-1',
      uriStatus: null,
    },
    data,
    error: null,
  };
}

export function ensureGridApiHasBeenSet(wrapper: ReactWrapper, AgGridWrapper = CommonGrid) {
  return new Promise((resolve) => {
    (function waitForGridReady() {
      if ((wrapper.find(AgGridWrapper).instance() as any).gridApi) {
        resolve(wrapper);
        return;
      }
      setTimeout(waitForGridReady, 100);
    }());
  });
}
