import { ProcessOption } from 'data/processOption/processOption.types';

// eslint-disable-next-line import/prefer-default-export
export const mockProcessOptions: ProcessOption[] = [
  {
    id: 'TVZQR1dBRklNI0NDTElTVEVOUE9SVA',
    procName: 'MVPGWAFIM',
    paramName: 'CCLISTENPORT',
    paramValue: '55131',
    comments: 'Server port for receiving CC PDUs from Mavenir PGW AF (change requires restart)',
  },
  {
    id: 'TVZQR1dBRklNI0NDVFJBTlNQT1JU',
    procName: 'MVPGWAFIM',
    paramName: 'CCTRANSPORT',
    paramValue: 'UDP',
    comments: 'Transport used by the Mavenir PGW AFs to deliver CC (change requires restart)',
  },
  {
    id: 'TVZQR1dBRklNI0lSSUxJU1RFTlBPUlQ',
    procName: 'MVPGWAFIM',
    paramName: 'IRILISTENPORT',
    paramValue: '21990',
    comments: 'Server port for receiving IRI from Mavenir PGW AF (change requires restart)',
  },
  {
    id: 'TVZQR1dBRklNI01BWFgyQ09OTlNQRVJBRg',
    procName: 'MVPGWAFIM',
    paramName: 'MAXX2CONNSPERAF',
    paramValue: '2',
    comments: 'Maximum number of simultaneous X2 connections allowed from each Mavenir PGW AF (change requires restart)',
  },
  {
    id: 'RVJLNUdBTUZBRlBJTSNTRVJWRVJfU0VDSU5GTw',
    procName: 'ERK5GAMFAFPIM',
    paramName: 'SERVER_SECINFO',
    paramValue: 'SIMPLESERVER1',
    comments: 'TLS SECURITY CREDENTIALS FOR RECEIVING X1 MESSAGES FROM 5G ERK_5GAMF AFS',
  },
];
