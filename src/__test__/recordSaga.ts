import { AnyAction } from 'redux';
import { runSaga, Saga } from 'redux-saga';

// eslint-disable-next-line import/prefer-default-export
export async function recordSaga(saga: Saga<any[]>, initialAction: AnyAction): Promise<AnyAction[]> {
  const dispatched: AnyAction[] = [];

  await runSaga(
    { dispatch: (action: AnyAction) => dispatched.push(action) },
    saga,
    initialAction,
  ).toPromise();

  return dispatched;
}
