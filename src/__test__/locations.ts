import { Location } from 'data/location/location.types';

// eslint-disable-next-line import/prefer-default-export
export const locations: Location[] = [{
  country: 'US',
  id: '2',
  stateCode: 'AK',
  stateName: 'Alaska',
  cities: [{
    id: '1',
    city: 'Adak',
    latitude: 55.999722,
    longitude: -161.207778,
  }],
}, {
  id: '5',
  country: 'US',
  stateCode: 'CA',
  stateName: 'California',
  cities: [{
    id: '3',
    city: 'San Francisco',
    latitude: 37.7749,
    longitude: 122.4194,
  }],
}];
