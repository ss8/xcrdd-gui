import { FqdnGroup } from 'data/fqdnGroup/fqdnGroup.types';

// eslint-disable-next-line import/prefer-default-export
export const mockFqdnGroups: FqdnGroup[] = [
  {
    id: 'ZG9tYWluMQ',
    groupName: 'domain1',
    fqdn: [{
      fId: '1',
      domainName: 'domain1.com',
      state: 'ACTIVE',
    }],
  },
  {
    id: 'ZG9tYWluMg',
    groupName: 'domain2',
    fqdn: [{
      fId: '1',
      domainName: 'domain2.com',
      state: 'ACTIVE',
    }],
  },
];
