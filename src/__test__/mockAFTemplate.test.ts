import {
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
  FormTemplate,
  FormTemplateSection,
} from 'data/template/template.types';
import {
  mockSupportedFeatures,
} from './mockSupportedFeatures';

import { mockTemplates, findMockTemplates } from './mockTemplates';

describe('Validate AF template formats', () => {
  const templates:FormTemplate[] = [];

  beforeAll(() => {
    // Set afName to a specific AF name if you want to test that AF template only,
    // example: const afName = 'Ericsson MCPTT'
    const afName = null;
    const mockRawTemplates = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, afName);
    mockRawTemplates?.forEach((rawTemplate) => {
      templates.push(JSON.parse(rawTemplate.template));
    });
  });

  function getAllFields(rows:string[][]): string[] {
    if (!rows) {
      return [];
    }
    const fields:string[] = [];
    rows.forEach((row:string[]) => {
      row.forEach((fieldName:string) => {
        if (fieldName !== '$empty') {
          fields.push(fieldName);
        }
      });
    });
    return fields;
  }

  it('the fields used in layout.rows should be defined in the fields section', () => {
    templates.forEach((template) => {
      const sections = Object.keys(template);
      sections.forEach((section:string) => {
        if (typeof template[section] === 'string') return;
        const templateSection = (template[section] as FormTemplateSection);
        const fieldsInRows = [];
        fieldsInRows.push(...getAllFields(templateSection?.metadata?.layout?.rows || []));
        fieldsInRows.push(...getAllFields(templateSection?.metadata?.layout?.collapsible?.rows || []));
        expect(templateSection?.fields).toBeDefined();
        const definedFields = Object.keys(templateSection?.fields);
        definedFields.push('$empty'); // there could be $empty in rows, so add $empty as a defined field.
        // include afTag in the expect and received so when there is an error we know which template has a problem
        expect(
          {
            afTag: template.key,
            templateSection: section,
            definedFields,
          },
        ).toEqual(
          {
            afTag: template.key,
            templateSection: section,
            definedFields: expect.arrayContaining(fieldsInRows),
          },
        );
      });
    });
  });

  it('should have the fields.tag.initial the same as template.key', () => {
    templates.forEach((template) => {
      const templateSection = (template.general as FormTemplateSection);
      expect(templateSection.fields.tag.initial).toBeDefined();
      expect(typeof templateSection.fields.tag.initial === 'string').toBeTruthy();
      if (templateSection.fields.tag.initial && typeof templateSection.fields.tag.initial === 'string') {
        expect(template.key).toEqual(expect.stringMatching(templateSection.fields.tag.initial));
      }
    });
  });

  it('should have tag in ontology file', () => {
    templates.forEach((template) => {
      const accessFunctionTags = Object.keys(mockSupportedFeatures.config.accessFunctions);
      const templateSection = (template.general as FormTemplateSection);
      expect(templateSection.fields.tag.initial).toBeDefined();
      expect(typeof templateSection.fields.tag.initial === 'string').toBeTruthy();
      if (templateSection.fields.tag.initial && typeof templateSection.fields.tag.initial === 'string') {
        const expected = [templateSection.fields.tag.initial];
        // include afTag in the expect and received so when there is an error we know which template has a problem
        expect({
          afTag: template.key,
          accessFunctionTags,
        }).toEqual({
          afTag: template.key,
          accessFunctionTags: expect.arrayContaining(expected),
        });
      }
      const expectedTag = [template.key];
      expect(accessFunctionTags).toEqual(expect.arrayContaining(expectedTag));
    });
  });

  it('should have type in ontology file', () => {
    templates.forEach((template) => {
      const templateSection = (template.general as FormTemplateSection);
      expect({
        templateKey: template.key,
        field: templateSection.fields.type.initial,
      })
        .toEqual({
          templateKey: template.key,
          field: mockSupportedFeatures.config.accessFunctions[template.key].xcipioAFType,
        });
    });
  });

  it('should be licensed', () => {
    templates.forEach((template) => {
      const licensedFeatures = mockSupportedFeatures.features.nodes[0].features;
      const accessFunctionLicense = mockSupportedFeatures.config.accessFunctions[template.key].features;
      expect({
        afTag: template.key,
        licensedFeatures,
      }).toEqual({
        afTag: template.key,
        licensedFeatures: expect.arrayContaining(accessFunctionLicense),
      });
    });
  });
});
