import { Team } from 'data/team/team.types';

// eslint-disable-next-line import/prefer-default-export
export const mockTeams: Team[] = [
  {
    id: '',
    name: 'Unrestricted',
    description: 'description 1',
    members: [],
  },
  {
    id: 'T1',
    name: 'Team1',
    description: 'description 2',
    members: [],
  },
  {
    id: 'T2',
    name: 'Team2',
    description: 'description 2',
    members: [],
  },
];
