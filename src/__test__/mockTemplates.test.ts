import {
  TEMPLATE_CATEGORY_ACCESS_FUNCTION,
} from 'data/template/template.types';

import { mockTemplates, findMockTemplates } from './mockTemplates';

describe('findMockTemplates', () => {
  it('findMockTemplates', () => {
    const mockRawTemplates = findMockTemplates(mockTemplates, TEMPLATE_CATEGORY_ACCESS_FUNCTION, 'Ericsson UPF');
    expect(mockRawTemplates ? mockRawTemplates[0] : {}).toEqual(mockTemplates[5]);
  });
});
