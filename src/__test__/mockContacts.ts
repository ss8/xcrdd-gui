import { Contact } from 'data/types';

// eslint-disable-next-line import/prefer-default-export
export const mockContacts: Contact[] = [
  {
    id: 'Q29udGFjdCAx',
    name: 'Contact 1',
  },
  {
    id: 'Q29udGFjdCAy',
    name: 'Contact 2',
  },
];
