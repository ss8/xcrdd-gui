import { TargetModel } from 'data/target/target.types';

// eslint-disable-next-line import/prefer-default-export
export const mockTargets: TargetModel[] = [{
  id: 'targetId1',
  dxOwner: 'T2',
  dxAccess: '',
  targetInfoHash: [],
  afs: {
    afIds: [],
    afccc: [],
    afGroups: [],
    afAutowireTypes: [
      {
        id: 'ERK_SMF',
        name: 'ERK_SMF',
      },
    ],
    afAutowireGroups: [],
  },
  caseId: {
    id: 'dDY1',
    name: 't65',
  },
  options: [],
  primaryType: 'IMSI',
  primaryValue: '5555555',
  secondaryType: '',
  secondaryValue: '',
}];
