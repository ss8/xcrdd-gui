import { ValueLabelPair } from 'data/types';

// eslint-disable-next-line import/prefer-default-export
export const mockTimeZones: ValueLabelPair[] = [
  {
    value: 'America/Los_Angeles',
    label: 'US/Pacific',
  },
  {
    value: 'America/Phoenix',
    label: 'US/Arizona',
  },
  {
    value: 'America/Denver',
    label: 'US/Mountain',
  },
  {
    value: 'America/Chicago',
    label: 'US/Central',
  },
  {
    value: 'America/New_York',
    label: 'US/Eastern',
  },
  {
    value: 'America/Anchorage',
    label: 'US/Alaska',
  },
  {
    value: 'Pacific/Honolulu',
    label: 'US/Hawaii',
  },
];
