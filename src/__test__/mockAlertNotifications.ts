import { AlertNotificationModel } from 'data/alertNotification/alertNotification.types';

// eslint-disable-next-line import/prefer-default-export
export const mockAlertNotifications: AlertNotificationModel[] = [{
  id: '1',
  message: 'Alert notification 1',
  alarmType: 'AUDITDISC',
  procName: 'proc1',
  utcDateTime: '',
}];
