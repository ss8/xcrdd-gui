import { SshSecurityInfo } from 'data/sshSecurityInfo/sshSecurityInfo.types';

// eslint-disable-next-line import/prefer-default-export
export const mockSshSecurityInfos: SshSecurityInfo[] = [
  {
    id: 'U1NISW5mbzE',
    sshType: 'SERVER',
    authType: 'SSHKEYS',
    userName: 'username1',
    password: '*****',
    privKeyFile: '3423434',
    passphrase: '*****',
    pubKeyFile: '2342343',
    sshDir: '/SS8/users/lis/.ssh',
    hostKeyType: 'RSA',
  },
];
