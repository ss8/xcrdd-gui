import { CollectionFunctionGroup } from 'data/collectionFunction/collectionFunction.types';

// eslint-disable-next-line import/prefer-default-export
export const mockCollectionFunctionGroups: CollectionFunctionGroup[] = [
  {
    id: 'id1',
    dxOwner: 'dxOwner',
    dxAccess: 'dxAccess',
    name: 'CA-DOJBNE',
    contact: {
      id: '',
      name: '',
    },
    cfs: [{
      id: 'Q0EtRE9KQk5FLTEwMDA2',
      name: 'CA-DOJBNE-1000678',
    }],
    description: '',
  },
  {
    id: 'id2',
    dxOwner: 'dxOwner',
    dxAccess: 'dxAccess',
    name: 'CookCounty',
    contact: {
      id: '',
      name: '',
    },
    cfs: [
      { id: 'Q29va0NvdW50eS0xMDAw', name: 'CookCounty-1000678' },
      { id: 'Q29va0NvdW50eS0xMDY2', name: 'CookCounty-1066' },
    ],
    description: '',
  },
  {
    id: 'id3',
    dxOwner: '',
    dxAccess: '',
    name: 'CA-DOJBNE-TIA1072',
    description: 'CF-Group CA-DOJBNE-TIA1072 auto-generated',
    contact: { id: '', name: '' },
    cfs: [{ id: 'Q0EtRE9KQk5FLVRJQTEw', name: 'CA-DOJBNE-TIA1072' }],
  },
  {
    id: 'id4',
    dxOwner: '',
    dxAccess: '',
    name: 'CA-DOJBNE-5GCF',
    description: 'CF-Group CA-DOJBNE-5GCF auto-generated',
    contact: { id: '', name: '' },
    cfs: [{ id: 'Q0EtRE9KQk5FLTVHQ0Y', name: 'CA-DOJBNE-5GCF' }],
  },
  {
    id: 'id5',
    dxOwner: '',
    dxAccess: '',
    name: 'Flavia',
    description: 'CF-Group Flavia auto-generated',
    contact: { id: '', name: '' },
    cfs: [{ id: 'RmxhdmlhLUpTVEQwMjVB', name: 'Flavia-JSTD025A' }],
  },
];
