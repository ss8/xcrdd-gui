import { DeviceModel } from 'xcipio/devices/device.types';

// eslint-disable-next-line import/prefer-default-export
export const nortelMtxSamsungMSCDeviceModel:DeviceModel = {
  device: '1',
  caseIds: [
    'Ui0yZw',
  ],
  allTargetAfs: [],
  services: [
    'GSMCDMA-2G-VOICE',
  ],
  types: {
    MSISDN: [
      {
        target: {
          id: 'Ui0yZyMx',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'SFBITFI',
                name: 'HPHLR',
                tag: 'HP_HLR',
              },
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MSISDN',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: false,
        ids: [
          'Ui0yZyMx',
        ],
      },
    ],
    CASEID: [
      {
        target: {
          id: 'Ui0yZyMy',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyMy',
        ],
      },
      {
        target: {
          id: 'Ui0yZyMz',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '1333333333',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyMz',
        ],
      },
      {
        target: {
          id: 'Ui0yZyM0',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tm9ydGVsREY',
                name: 'NortelDF',
                tag: 'NORTEL_DF',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '122222222222222222',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM0',
        ],
      },
    ],
    MIN: [
      {
        target: {
          id: 'Ui0yZyM1',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tk9SVEVMTVRY',
                name: 'NORTELMTX',
                tag: 'NORTEL_MTX',
              },
              {
                id: 'bXR4LTEx',
                name: 'mtx-11',
                tag: 'NORTEL_MTX',
              },
            ],
            afccc: [
              {
                afType: 'NORTEL_MTX',
                afSettings: [
                  {
                    autowired: false,
                    afids: [
                      'Tk9SVEVMTVRY',
                    ],
                    options: [
                      {
                        key: 'CDCID',
                        value: '1',
                      },
                    ],
                  },
                  {
                    autowired: false,
                    afids: [
                      'bXR4LTEx',
                    ],
                    options: [
                      {
                        key: 'CDCID',
                        value: '1',
                      },
                    ],
                  },
                ],
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MIN',
          primaryValue: '1333333333',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM1',
        ],
      },
    ],
    MDN: [
      {
        target: {
          id: null,
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MDN',
          primaryValue: '111111111',
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      },
    ],
  },
};

export const samsungMSCDeviceModel:DeviceModel = {
  device: '1',
  caseIds: [
    'Ui0yZw',
  ],
  allTargetAfs: [],
  services: [
    'GSMCDMA-2G-VOICE',
  ],
  types: {
    MSISDN: [
      {
        target: {
          id: 'Ui0yZyMx',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'SFBITFI',
                name: 'HPHLR',
                tag: 'HP_HLR',
              },
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MSISDN',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: false,
        ids: [
          'Ui0yZyMx',
        ],
      },
    ],
    CASEID: [
      {
        target: {
          id: 'Ui0yZyMy',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyMy',
        ],
      },
      {
        target: {
          id: 'Ui0yZyMz',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '1333333333',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyMz',
        ],
      },
      {
        target: {
          id: 'Ui0yZyM0',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tm9ydGVsREY',
                name: 'NortelDF',
                tag: 'NORTEL_DF',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '122222222222222222',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM0',
        ],
      },
    ],
    MDN: [
      {
        target: {
          id: null,
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MDN',
          primaryValue: '111111111',
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      },
    ],
  },
};

export const nortelMtxDeviceModel:DeviceModel = {
  device: '1',
  caseIds: [
    'Ui0yZw',
  ],
  allTargetAfs: [],
  services: [
    'GSMCDMA-2G-VOICE',
  ],
  types: {
    MSISDN: [
      {
        target: {
          id: 'Ui0yZyMx',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'SFBITFI',
                name: 'HPHLR',
                tag: 'HP_HLR',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MSISDN',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: false,
        ids: [
          'Ui0yZyMx',
        ],
      },
    ],
    CASEID: [
      {
        target: {
          id: 'Ui0yZyMz',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [{
              id: 'Tk9SVEVMTVRY',
              name: 'NORTELMTX',
              tag: 'NORTEL_MTX',
            }],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '1333333333',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyMz',
        ],
      },
      {
        target: {
          id: 'Ui0yZyM0',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tm9ydGVsREY',
                name: 'NortelDF',
                tag: 'NORTEL_DF',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '122222222222222222',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM0',
        ],
      },
    ],
    MIN: [
      {
        target: {
          id: 'Ui0yZyM1',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tk9SVEVMTVRY',
                name: 'NORTELMTX',
                tag: 'NORTEL_MTX',
              },
            ],
            afccc: [
              {
                afType: 'NORTEL_MTX',
                afSettings: [
                  {
                    autowired: false,
                    afids: [
                      'Tk9SVEVMTVRY',
                    ],
                    options: [
                      {
                        key: 'CDCID',
                        value: '1',
                      },
                    ],
                  },
                  {
                    autowired: false,
                    afids: [
                      'bXR4LTEx',
                    ],
                    options: [
                      {
                        key: 'CDCID',
                        value: '1',
                      },
                    ],
                  },
                ],
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MIN',
          primaryValue: '1333333333',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM1',
        ],
      },
    ],
    MDN: [
      {
        target: {
          id: null,
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MDN',
          primaryValue: '111111111',
          secondaryType: '',
          secondaryValue: '',
          isRequired: false,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      },
    ],
  },
};

export const noNortelMtxAndSamsungMSCDeviceModel:DeviceModel = {
  device: '1',
  caseIds: [
    'Ui0yZw',
  ],
  allTargetAfs: [],
  services: [
    'GSMCDMA-2G-VOICE',
  ],
  types: {
    MSISDN: [
      {
        target: {
          id: 'Ui0yZyMx',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'SFBITFI',
                name: 'HPHLR',
                tag: 'HP_HLR',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'MSISDN',
          primaryValue: '1111111111',
          secondaryType: '',
          secondaryValue: '',
        },
        display: false,
        ids: [
          'Ui0yZyMx',
        ],
      },
    ],
    CASEID: [
      {
        target: {
          id: 'Ui0yZyM0',
          dxOwner: '',
          dxAccess: '',
          targetInfoHash: [],
          afs: {
            afIds: [
              {
                id: 'Tm9ydGVsREY',
                name: 'NortelDF',
                tag: 'NORTEL_DF',
              },
            ],
            afccc: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
          },
          caseId: {
            id: 'Ui0yZw',
            name: 'R-2g',
          },
          options: [],
          primaryType: 'CASEID',
          primaryValue: '122222222222222222',
          secondaryType: '',
          secondaryValue: '',
        },
        display: true,
        ids: [
          'Ui0yZyM0',
        ],
      },
    ],
  },
};

export const caseIDNotMatchCASEIDMDNOrCASEIDMINDeviceModel:DeviceModel = {
  device: 'DX1',
  caseIds: ['YS0yZy1jbGFzc2ljby1pbg'],
  allTargetAfs: [],
  services: ['GSMCDMA-2G-VOICE'],
  types: {
    CASEID: [{
      target: {
        id: 'YS0yZy1jbGFzc2ljby1pbiMx',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'Tm9ydGVsREY', name: 'NortelDF', tag: 'NORTEL_DF' }], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '2222222222',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy1jbGFzc2ljby1pbiMx'],
    }, {
      target: {
        id: 'YS0yZy1jbGFzc2ljby1pbiMz',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'U2Ftc3VuZ01TQw', name: 'SamsungMSC', tag: 'SAMSUNG_MSC' }], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '4444444444',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy1jbGFzc2ljby1pbiMz'],
    }, {
      target: {
        id: 'YS0yZy1jbGFzc2ljby1pbiM0',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'bXR4LTEx', name: 'mtx-11', tag: 'NORTEL_MTX' }], afccc: [{ afType: 'NORTEL_MTX', afSettings: [{ autowired: false, afids: ['bXR4LTEx'], options: [{ key: 'CDCID', value: '1' }] }] }], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '5555555555',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy1jbGFzc2ljby1pbiM0'],
    }],
    MSISDN: [{
      target: {
        id: 'YS0yZy1jbGFzc2ljby1pbiMy',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'SFBITFI', name: 'HPHLR', tag: 'HP_HLR' }, { id: 'U2Ftc3VuZ01TQw', name: 'SamsungMSC', tag: 'SAMSUNG_MSC' }], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' },
        options: [],
        primaryType: 'MSISDN',
        primaryValue: '3333333333',
        secondaryType: '',
        secondaryValue: '',
      },
      display: false,
      ids: ['YS0yZy1jbGFzc2ljby1pbiMy'],
    }],
    MIN: [{
      target: {
        id: 'YS0yZy1jbGFzc2ljby1pbiM1',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'bXR4LTEx', name: 'mtx-11', tag: 'NORTEL_MTX' }], afccc: [{ afType: 'NORTEL_MTX', afSettings: [{ autowired: false, afids: ['bXR4LTEx'], options: [{ key: 'CDCID', value: '1' }] }] }], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' },
        options: [],
        primaryType: 'MIN',
        primaryValue: '6666666666',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy1jbGFzc2ljby1pbiM1'],
    }],
    MDN: [{
      target: {
        id: null, caseId: { id: 'YS0yZy1jbGFzc2ljby1pbg', name: 'a-2g-classico-in' }, options: [], primaryType: 'MDN', primaryValue: '3333333333', secondaryType: '', secondaryValue: '', isRequired: false, synthesized: true,
      },
      display: true,
      synthesized: true,
      ids: [],
    }],
  },
};

export const alreadyHasCASEIDMINAndCASEIDMDNDeviceModel :DeviceModel = {
  device: '1',
  caseId: '',
  services: [
    'GSMCDMA-2G-VOICE',
    'LTE-4G-DATA',
  ],
  allTargetAfs: [],
  types: {
    MDN: [
      {
        target: {
          id: '',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'MDN',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
          synthesized: true,
        },
        display: true,
        synthesized: true,
        ids: [],
      },
    ],
    MSISDN: [
      {
        target: {
          id: 'MSISDN_1625686478488',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [
              {
                id: 'SFBITFI',
                name: 'HPHLR',
                tag: 'HP_HLR',
              },
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'MSISDN',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
        },
        display: false,
        ids: [],
      },
    ],
    CASEIDMDN: [
      {
        target: {
          id: 'CASEIDMDN_1625686478488',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'CASEIDMDN',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
        },
        display: false,
        ids: [],
      },
    ],
    CASEIDMIN: [
      {
        target: {
          id: 'CASEIDMIN_1625686478488',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [
              {
                id: 'U2Ftc3VuZ01TQw',
                name: 'SamsungMSC',
                tag: 'SAMSUNG_MSC',
              },
              {
                id: 'Tk9SVEVMTVRY',
                name: 'NORTELMTX',
                type: 'NORTEL_MTX',
                tag: 'NORTEL_MTX',
                model: '',
                x3TargetAssociations: null,
                bAFAssociation: false,
                contentInterfaces: [
                  {
                    id: 'Tk9SVEVMTVRY',
                    name: 'Tk9SVEVMTVRY',
                    options: [],
                  },
                ],
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'CASEIDMIN',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
        },
        display: false,
        ids: [],
      },
    ],
    CASEID: [
      {
        target: {
          id: 'CASEID_1625686478488',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [
              {
                id: 'Tm9ydGVsREY',
                name: 'NortelDF',
                tag: 'NORTEL_DF',
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'CASEID',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
        },
        display: true,
        ids: [],
      },
    ],
    MIN: [
      {
        target: {
          id: 'MIN_1625686478488',
          dxOwner: '',
          dxAccess: '',
          name: '',
          afs: {
            afIds: [
              {
                id: 'Tk9SVEVMTVRY',
                name: 'NORTELMTX',
                tag: 'NORTEL_MTX',
              },
            ],
            afGroups: [],
            afAutowireTypes: [],
            afAutowireGroups: [],
            afccc: [],
          },
          primaryType: 'MIN',
          primaryValue: '',
          secondaryType: '',
          secondaryValue: '',
          options: [],
          isRequired: true,
        },
        display: true,
        ids: [],
      },
    ],
  },
};

/**
 * This deviceModel has an extra CASEID that is not associated to an AF
 * CASEID[1] has the same value as MIN and MIN has Nortel MTX AF.  So CASEID[1] is CASEIDMIN
 * CASEID[2] has associated AF
 * CASEID[0] is the extra CASEID because it does not have an AF and although its value matches MSISDN, but MSISDN
 * does not have Samsung MSC
 */
export const extraCaseIDDeviceModel:DeviceModel = {
  device: '1',
  caseIds: ['YS0yZy00Zy13', 'YS0yZy00Zy13MDA'],
  allTargetAfs: [],
  services: ['LTE-4G-DATA', 'GSMCDMA-2G-VOICE'],
  types: {
    MSISDN: [{
      target: {
        id: 'YS0yZy00Zy13IzE',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'SFBITFI', name: 'HPHLR', tag: 'HP_HLR' }], afccc: [], afGroups: [], afAutowireTypes: [{ id: 'ERK_SMF', name: 'ERK_SMF', tag: 'ERK_SMF' }, { id: 'MV_SMSIWF', name: 'MV_SMSIWF', tag: 'MV_SMSIWF' }, { id: 'NSNHLR_SDM_PRGW', name: 'NSNHLR_SDM_PRGW', tag: 'NSNHLR_SDM_PRGW' }, { id: 'NSN_SAEGW', name: 'NSN_SAEGW', tag: 'NSN_SAEGW' }, { id: 'STA_GGSN', name: 'STA_GGSN', tag: 'STA_GGSN' }], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13', name: 'a-2g-4g-w' },
        options: [],
        primaryType: 'MSISDN',
        primaryValue: '1111111111',
        secondaryType: '',
        secondaryValue: '',
      },
      display: false,
      ids: ['YS0yZy00Zy13IzE', 'YS0yZy00Zy13MDAjMQ'],
    }],
    IMSI: [{
      target: {
        id: 'YS0yZy00Zy13IzI',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [], afccc: [], afGroups: [], afAutowireTypes: [{ id: 'ALU_HLR', name: 'ALU_HLR', tag: 'ALU_HLR' }, { id: 'ALU_MME', name: 'ALU_MME', tag: 'ALU_MME' }, { id: 'CISCOVPCRF', name: 'CISCOVPCRF', tag: 'CISCOVPCRF' }, { id: 'ERK_SMF', name: 'ERK_SMF', tag: 'ERK_SMF' }, { id: 'ERK_VMME', name: 'ERK_VMME', tag: 'ERK_VMME' }, { id: 'ETSI103221', name: 'ETSI103221', tag: 'ETSI103221' }, { id: 'HP_AAA', name: 'HP_AAA', tag: 'HP_AAA' }, { id: 'MV_SMSIWF', name: 'MV_SMSIWF', tag: 'MV_SMSIWF' }, { id: 'NSNHLR_SDM_PRGW', name: 'NSNHLR_SDM_PRGW', tag: 'NSNHLR_SDM_PRGW' }, { id: 'NSN_SAEGW', name: 'NSN_SAEGW', tag: 'NSN_SAEGW' }, { id: 'PCRF', name: 'PCRF', tag: 'PCRF' }, { id: 'STA_GGSN', name: 'STA_GGSN', tag: 'STA_GGSN' }], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13', name: 'a-2g-4g-w' },
        options: [],
        primaryType: 'IMSI',
        primaryValue: '44444444444',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13IzI'],
    }],
    IMEI: [{
      target: {
        id: 'YS0yZy00Zy13IzM',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [], afccc: [], afGroups: [], afAutowireTypes: [{ id: 'ERK_SMF', name: 'ERK_SMF', tag: 'ERK_SMF' }, { id: 'MV_SMSIWF', name: 'MV_SMSIWF', tag: 'MV_SMSIWF' }, { id: 'NSN_SAEGW', name: 'NSN_SAEGW', tag: 'NSN_SAEGW' }, { id: 'STA_GGSN', name: 'STA_GGSN', tag: 'STA_GGSN' }], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13', name: 'a-2g-4g-w' },
        options: [],
        primaryType: 'IMEI',
        primaryValue: '3520660609262327',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13IzM'],
    }],
    CASEID: [{
      target: {
        id: 'YS0yZy00Zy13MDAjMg',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13MDA', name: 'a-2g-4g-w00' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '1111111111',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13MDAjMg'],
    }, {
      target: {
        id: 'YS0yZy00Zy13MDAjMw',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13MDA', name: 'a-2g-4g-w00' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '4444444444',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13MDAjMw'],
    }, {
      target: {
        id: 'YS0yZy00Zy13MDAjNA',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'Tm9ydGVsREY', name: 'NortelDF', tag: 'NORTEL_DF' }], afccc: [], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13MDA', name: 'a-2g-4g-w00' },
        options: [],
        primaryType: 'CASEID',
        primaryValue: '3333333333',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13MDAjNA'],
    }],
    MIN: [{
      target: {
        id: 'YS0yZy00Zy13MDAjNQ',
        dxOwner: '',
        dxAccess: '',
        targetInfoHash: [],
        afs: {
          afIds: [{ id: 'bXR4LTEx', name: 'mtx-11', tag: 'NORTEL_MTX' }], afccc: [{ afType: 'NORTEL_MTX', afSettings: [{ autowired: false, afids: ['bXR4LTEx'], options: [{ key: 'CDCID', value: '1' }] }] }], afGroups: [], afAutowireTypes: [], afAutowireGroups: [],
        },
        caseId: { id: 'YS0yZy00Zy13MDA', name: 'a-2g-4g-w00' },
        options: [],
        primaryType: 'MIN',
        primaryValue: '4444444444',
        secondaryType: '',
        secondaryValue: '',
      },
      display: true,
      ids: ['YS0yZy00Zy13MDAjNQ'],
    }],
    MDN: [{
      target: {
        id: null, caseId: { id: 'YS0yZy00Zy13', name: 'a-2g-4g-w' }, options: [], primaryType: 'MDN', primaryValue: '1111111111', secondaryType: '', secondaryValue: '', isRequired: false, synthesized: true,
      },
      display: true,
      synthesized: true,
      ids: [],
    }],
  },
};
