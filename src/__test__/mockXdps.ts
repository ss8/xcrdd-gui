import { Xdp } from 'data/xdp/xdp.types';

// eslint-disable-next-line import/prefer-default-export
export const mockXdps: Xdp[] = [{
  id: 'Mg',
  inPortEnd: '45128',
  inPortStart: '45001',
  insideNat: true,
  ipAddr: '49.49.49.45',
  ipduId: '2',
  outPortEnd: '45512',
  outPortStart: '45129',
  port: '15001',
  state: 'INACTIVE',
  switchTech: 'X',
},
{
  id: 'MTU',
  inPortEnd: '8898',
  inPortStart: '8898',
  insideNat: true,
  ipAddr: '110.110.110.110',
  ipduId: '15',
  outPortEnd: '8898',
  outPortStart: '7888',
  port: '8898',
  state: 'INACTIVE',
  switchTech: 'F',
}];
