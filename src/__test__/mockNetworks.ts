import { Network } from 'data/network/network.types';

// eslint-disable-next-line import/prefer-default-export
export const mockNetworks: Network[] = [
  {
    id: 'WENQcmNz',
    networkId: 'XCPrcs',
    ownIPAddress: '11.11.11.11',
    startPortNumber: '0',
    portNumberRange: '1',
  },
  {
    id: 'WENQdjY',
    networkId: 'XCPv6',
    ownIPAddress: 'abcd:abcd:abcd:abcd::abcd',
    startPortNumber: '0',
    portNumberRange: '1',
  },
  {
    id: 'WFRQMTI4djRfV0dX',
    networkId: 'XTP128v4_WGW',
    ownIPAddress: '101.101.101.101',
    startPortNumber: '15581',
    portNumberRange: '1',
  },
  {
    id: 'WFRQMTI4djZfV0dX',
    networkId: 'XTP128v6_WGW',
    ownIPAddress: 'bcde:bcde:bcde:bcde::bcde',
    startPortNumber: '15581',
    portNumberRange: '1',
  },
  {
    id: 'WENQLWNvbnRhaW5lcg',
    networkId: 'XCP-container',
    ownIPAddress: '0.0.0.0',
    startPortNumber: '0',
    portNumberRange: '1',
  },
];
