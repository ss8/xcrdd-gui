import { DistributorData } from 'data/distributor/distributor.types';

// eslint-disable-next-line import/prefer-default-export
export const mockDistributors: DistributorData[] = [
  {
    resource: 'DISTRIBUTORS',
    comments: 'These are the default values for the listeners and destinations. NOTE: THE afTags SPECIFIED MUST MATCH THE accessFunction TAGS CONTAINED IN THE xcipio_ontology.json file.',
    globalDefaultValues: {
      listeners: [
        {
          field: 'keepaliveTimeout',
          defaultvalue: 300,
        },
        {
          field: 'maxConcurrentConnection',
          defaultvalue: 128,
        },
        {
          field: 'transport',
          defaultvalue: 'TCP',
        },
        {
          field: 'requiredState',
          defaultvalue: 'ACTIVE',
        },
        {
          field: 'numberOfParsers',
          defaultvalue: 2,
        },
        {
          field: 'synchronizationHeader',
          defaultvalue: 'NONE',
        },
      ],
      destinations: [
        {
          field: 'keepaliveInterval',
          defaultvalue: 60,
        },
        {
          field: 'keepaliveRetries',
          defaultvalue: 6,
        },
        {
          field: 'numOfConnections',
          defaultvalue: 2,
        },
        {
          field: 'requiredState',
          defaultvalue: 'ACTIVE',
        },
        {
          field: 'transport',
          defaultvalue: 'TCP',
        },
        {
          field: 'protocol',
          defaultvalue: 'PASSTHRU',
        },
      ],
    },
    ss8Distributors: [
      {
        moduleName: 'RADX2IM',
        instanceId: 1,
        afTags: [
          'GPVRADIUS',
        ],
      },
      {
        moduleName: 'RADAFIM',
        instanceId: 1,
        afTags: [
          'GPVRADIUS',
        ],
      },
      {
        moduleName: 'NOKIASRX3IM',
        instanceId: 1,
        afTags: [
          'NKSR',
        ],
      },
      {
        moduleName: 'NOK5GUPFX3IM',
        instanceId: 1,
        afTags: [
          'NOK_5GUPF',
        ],
      },
      {
        moduleName: 'NOK5GSMSFX2IM',
        instanceId: 1,
        afTags: [
          'NOK_5GSMSF',
        ],
      },
      {
        moduleName: 'NOK5GSMFX2IM',
        instanceId: 1,
        afTags: [
          'NOK_5GSMF',
        ],
      },
      {
        moduleName: 'NOK5GUDMX2IM',
        instanceId: 1,
        afTags: [
          'NOK_5GUDM',
        ],
      },
      {
        moduleName: 'NOK5GAMFX2IM',
        instanceId: 1,
        afTags: [
          'NOK_5GAMF',
        ],
      },
      {
        moduleName: 'ERK5GAMFX2IM',
        instanceId: 1,
        afTags: [
          'ERK_5GAMF',
        ],
      },
      {
        moduleName: 'MV5GSMSCX2IM',
        instanceId: 1,
        afTags: [
          'MVNR_5GSMSC',
        ],
      },
      {
        moduleName: 'ETSIGIZMOX2IM',
        instanceId: 1,
        afTags: [
          'ETSI_GIZMO',
        ],
      },
      {
        moduleName: 'ETSIGIZMOX3IM',
        instanceId: 1,
        afTags: [
          'ETSI_GIZMO',
        ],
      },
      {
        moduleName: 'NSNSDMX2IM',
        instanceId: 1,
        afTags: [
          'NSN_SDM',
        ],
      },
      {
        moduleName: 'AUDCSBCX2IM',
        instanceId: 1,
        afTags: [
          'AUDIOCODES_SBC',
        ],
      },
      {
        moduleName: 'AUDCSBCX3IM',
        instanceId: 1,
        afTags: [
          'AUDIOCODES_SBC',
        ],
      },
      {
        moduleName: 'ERKSMFX2IM',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_SMF',
        ],
      },
      {
        moduleName: '5GXCF3',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'ERK_UPF',
        ],
      },
      {
        moduleName: 'ERKUPFX3IM',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_UPF',
        ],
      },
      {
        moduleName: 'ERKUPFX3IM',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_UPF',
        ],
      },
      {
        moduleName: 'ERKUPFX3IM',
        instanceId: 2,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_UPF',
        ],
      },
      {
        moduleName: 'ERKUPFX3IM',
        instanceId: 3,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_UPF',
        ],
      },
      {
        moduleName: 'CSCOPGWX3IM',
        instanceId: 1,
        listenerIpHint: 'SS8PROXY.destinations',
        destinationIpHint: 'none',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'E103221X2IM',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_MCPTT',
        ],
      },
      {
        moduleName: 'E103221X3IM',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ERK_MCPTT',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 2,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 3,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 4,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 5,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 6,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 7,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'XCF3',
        instanceId: 8,
        listenerIpHint: 'none',
        destinationIpHint: 'SS8PROXY.listeners',
        afTags: [
          'STA_PGW',
        ],
      },
      {
        moduleName: 'INF3',
        instanceId: 1,
        listenerIpHint: 'none',
        destinationIpHint: 'none',
        afTags: [
          'ETSI102232',
          'TS33108',
        ],
      },
    ],
    afTagsToProtocolMap: {
      'GPVRADIUS#RADX2IM': {
        afProtocols: [
          'GPRADIUS',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13070,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13065,
          },
          {
            field: 'transport',
            defaultValue: 'UDP',
          },
        ],
      },
      'GPVRADIUS#RADAFIM': {
        afProtocols: [
          'GPRADIUS_W_SX3HDR',
        ],
        destinationDefaultValues: [],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13070,
          },
          {
            field: 'transport',
            defaultValue: 'ZPULL',
          },
        ],
      },
      NKSR: {
        afProtocols: [
          'NOKIASR_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45166,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45166,
          },
        ],
      },
      NOK_5GUPF: {
        afProtocols: [
          'ETSI103221_2_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45152,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45152,
          },
        ],
      },
      NOK_5GSMSF: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13040,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13035,
          },
        ],
      },
      NOK_5GSMF: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13010,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13000,
          },
        ],
      },
      NOK_5GUDM: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13020,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13015,
          },
        ],
      },
      NOK_5GAMF: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13025,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13030,
          },
        ],
      },
      ERK_5GAMF: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13045,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13050,
          },
        ],
      },
      MVNR_5GSMSC: {
        afProtocols: [
          'ETSI103221_2_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 13055,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 13060,
          },
        ],
      },
      'ETSI_GIZMO#ETSIGIZMOX2IM': {
        afProtocols: [
          'ETSI_GIZMO_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 12133,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 12132,
          },
        ],
      },
      'ETSI_GIZMO#ETSIGIZMOX3IM': {
        afProtocols: [
          'ETSI_GIZMO_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45170,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45170,
          },
        ],
      },
      NSN_SDM: {
        afProtocols: [
          'NSNSDM',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 12160,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 12150,
          },
        ],
      },
      'AUDIOCODES_SBC#AUDCSBCX2IM': {
        afProtocols: [
          'AUDIOCODES_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 12130,
          },
          {
            field: 'transport',
            defaultValue: 'TCP',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 12125,
          },
        ],
      },
      'AUDIOCODES_SBC#AUDCSBCX3IM': {
        afProtocols: [
          'AUDIOCODES_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45165,
          },
          {
            field: 'transport',
            defaultValue: 'UDP',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45200,
          },
        ],
      },
      ERK_SMF: {
        afProtocols: [
          'ERKSMF_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 12120,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 12115,
          },
        ],
      },
      'ERK_UPF#5GXCF3': {
        afProtocols: [
          'ERKUPF_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45160,
          },
          {
            field: 'transport',
            defaultValue: 'TCP',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45160,
          },
        ],
      },
      'ERK_UPF#ERKUPFX3IM': {
        afProtocols: [
          'ERKUPF_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45160,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45160,
          },
        ],
      },
      STA_PGW: {
        afProtocols: [
          'CISCO_PGW',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45064,
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45064,
          },
        ],
      },
      'ERK_MCPTT#E103221X2IM': {
        afProtocols: [
          'ERKMCPTT_X2',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 12075,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 12065,
          },
        ],
      },
      'ERK_MCPTT#E103221X3IM': {
        afProtocols: [
          'ERKMCPTT_X3',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45149,
          },
          {
            field: 'transport',
            defaultValue: 'ZPUSH',
          },
          {
            field: 'protocol',
            defaultValue: 'PASSTHRU_W_SX3HDR',
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45149,
          },
        ],
      },
      ETSI102232: {
        afProtocols: [
          'ETSI102232',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45116,
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45116,
          },
        ],
      },
      TS33108: {
        afProtocols: [
          'TS33108',
        ],
        destinationDefaultValues: [
          {
            field: 'port',
            defaultValue: 45157,
          },
        ],
        listenerDefaultValues: [
          {
            field: 'port',
            defaultValue: 45157,
          },
        ],
      },
    },
  },
];
