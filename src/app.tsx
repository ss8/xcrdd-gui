import React, { FunctionComponent, ReactElement } from 'react';
import { Provider } from 'react-redux';
import { FocusStyleManager } from '@blueprintjs/core';
import { BrowserRouter } from 'react-router-dom';
import { LicenseManager, ModuleRegistry, AllModules } from '@ag-grid-enterprise/all-modules';
import AlertNotification from 'containers/AlertNotification';
import SessionExpirationNotification from 'containers/SessionExpirationNotification';
import store from './data/store';
import Http from './shared/http';
import RootRoutes from './root-routes';
import Navbar from './shared/navbar';
import ScrollTop from './shared/scroll-top';

import './vendor/flexboxgrid.css';
import './app.scss';
import './vendor.d';
import '../node_modules/normalize.css/normalize.css';
import '../node_modules/@blueprintjs/core/lib/css/blueprint.css';
import '../node_modules/@blueprintjs/icons/lib/css/blueprint-icons.css';
import '../node_modules/@blueprintjs/datetime/lib/css/blueprint-datetime.css';

import '@ag-grid-enterprise/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-enterprise/all-modules/dist/styles/ag-theme-balham.css';

LicenseManager.setLicenseKey('SS8_Networks_MultiApp_1Devs17_July_2020__MTU5NDk0MDQwMDAwMA==dbf856c42e67d7b8eaad22a5e449b61a');
FocusStyleManager.onlyShowFocusOnTabs();

ModuleRegistry.registerModules(AllModules);

const App: FunctionComponent = (): ReactElement => {
  Http.init(store);
  return (
    <Provider store={store}>
      <BrowserRouter>
        <ScrollTop>
          <React.Fragment>
            <Navbar />
            <AlertNotification />
            <SessionExpirationNotification />
            <RootRoutes />
          </React.Fragment>
        </ScrollTop>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
