import {
  GET_ALL_ENTITIES_FULFILLED,
  // GET_ENTITY_BY_ID_FULFILLED,
  // CREATE_ENTITY_FULFILLED,
  // PATCH_ENTITY_FULFILLED,
  // UPDATE_ENTITY_FULFILLED,
  // DELETE_ENTITY_FULFILLED,
} from './dashboard-actions';

const moduleReducer = (state = {}, action: any) => {
  // todo: handle error for _REJECTED
  switch (action.type) {
    case GET_ALL_ENTITIES_FULFILLED:
      return state;
    default:
      return state;
  }
};
export default moduleReducer;
