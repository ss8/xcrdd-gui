import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  getAllEntities,
  getEntityById,
  createEntity,
  updateEntity,
  patchEntity,
  deleteEntity,
} from './dashboard-actions';
import ModuleRoutes from './dashboard-routes';

const Module = () => (
  <ModuleRoutes />
);

const mapStateToProps = (state: any) => ({
  module: state.module,
});

const mapDispatchToProps = () => ({
  getAllEntities,
  getEntityById,
  createEntity,
  updateEntity,
  patchEntity,
  deleteEntity,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Module));
