import Http from '../shared/http';

const http = Http.getHttp();

// action export constants
export const GET_ALL_ENTITIES = 'GET_ALL_ENTITIES';
export const GET_ENTITY_BY_ID = 'GET_ENTITY_BY_ID';
export const UPDATE_ENTITY = 'UPDATE_DATA';
export const PATCH_ENTITY = 'PATCH_ENTITY';
export const DELETE_ENTITY = 'DELETE_DATA';
export const CREATE_ENTITY = 'CREATE_ENTITY';
export const GET_ALL_ENTITIES_FULFILLED = 'GET_ALL_ENTITIES_FULFILLED';
export const GET_ENTITY_BY_ID_FULFILLED = 'GET_ENTITY_BY_ID_FULFILLED';
export const UPDATE_ENTITY_FULFILLED = 'UPDATE_DATA_FULFILLED';
export const PATCH_ENTITY_FULFILLED = 'PATCH_ENTITY_FULFILLED';
export const DELETE_ENTITY_FULFILLED = 'DELETE_DATA_FULFILLED';
export const CREATE_ENTITY_FULFILLED = 'CREATE_ENTITY_FULFILLED';

// action creators
export const getAllEntities = () => ({ type: GET_ALL_ENTITIES, payload: http.get('/resource') });
export const getEntityById = (id: number) => ({ type: GET_ENTITY_BY_ID, payload: http.get(`/resource/${id}`) });
export const createEntity = (body: any) => ({ type: CREATE_ENTITY, payload: http.post('/resource', body) });
export const updateEntity = (id: number, body: any) => ({ type: UPDATE_ENTITY, payload: http.put(`/resource/${id}`, body) });
export const patchEntity = (id: number, body: any) => ({ type: PATCH_ENTITY, payload: http.patch(`/resource/${id}`, body) });
export const deleteEntity = (id: number) => ({ type: DELETE_ENTITY, payload: http.delete(`/resource/${id}`) });
