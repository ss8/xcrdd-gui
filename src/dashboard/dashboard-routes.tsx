import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import DashboardFrame from './dashboard-frame';
// import { ModuleAll } from './components/module-all';
// import { ModuleView } from './components/module-view';
// import { ModuleCreate } from './components/module-create';
// import { ModuleEdit } from './components/module-edit';

const moduleRoot = '/dashboard';

const DashboardRoutes = (props: any) => (
  <Fragment>
    <Route
      path={`${moduleRoot}/`}
      render={() => <DashboardFrame {...props} />}
    />
  </Fragment>
);

export default DashboardRoutes;
