import React from 'react';
import Muuri from 'muuri';
import Widget from '../../shared/widget';

class Dashboard extends React.Component {
  grid = null;

  componentDidMount() {
    this.grid = new Muuri('.grid', {
      dragEnabled: true,
    });
  }

  handleRefresh = () => {

  }

  render() {
    return (
      <div className="app">
        <div
          className="grid"
          style={{ width: '100%', height: '100%', position: 'relative' }}
        >
          {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((x, i) => (
            <Widget
              key={i}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Dashboard;
