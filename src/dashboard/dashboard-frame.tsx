import React, { Fragment } from 'react';
import Dashboard from './components/dashboard';
import './dashboard.scss';

class InterceptFrame extends React.Component<any, any> {
  render() {
    return (
      <Fragment>
        <div className="">
          <div className="row">
            <div className="col-md-12">
              <Dashboard {...this.props} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default InterceptFrame;
